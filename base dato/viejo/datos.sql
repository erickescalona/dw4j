

insert into CONFIGURACION (id_configuracion, calidad, ruta_temporal, archivo_tif, archivo_cod, log, foliatura, server_name, database_name, port, userbd, password, ficha, fabrica, elimina)
values (1, '1', 'temp', 'documento.tiff', 'codificado.cod', '/lib/log4j-config.properties', '1', '192.168.0.157', 'dw4j', 5432, 'b3JhZHc0ag==', 'RGV2ZWxjb20wMQ==', '1', '1', '1');
commit;

insert into ROL (id_rol, rol) values (1, 'ADMINISTRADOR');
insert into ROL (id_rol, rol) values (2, 'APROBADOR');
insert into ROL (id_rol, rol) values (3, 'DIGITALIZADOR');
insert into ROL (id_rol, rol) values (4, 'CONSULTAR');
insert into ROL (id_rol, rol) values (5, 'IMPRIMIR');
insert into ROL (id_rol, rol) values (6, 'REPORTES');
insert into ROL (id_rol, rol) values (7, 'CONFIGURADOR');
insert into ROL (id_rol, rol) values (8, 'MANTENIMIENTO');
insert into ROL (id_rol, rol) values (9, 'ELIMINAR');
commit;

insert into ESTATUS (id_estatus, estatus) values (1, 'Activo');
insert into ESTATUS (id_estatus, estatus) values (2, 'Inactivo');
commit;

insert into USUARIO (id_usuario, nombre, apellido, cedula, sexo, id_estatus, password) values ('dw4jconf', 'Usuario', 'Configurador', null, null, 1, 'RGV2ZWxjb21HRA==');
commit;

insert into PERFIL (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) values (1, null, null, 'dw4jconf', 7);
insert into PERFIL (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) values (2, null, null, 'dw4jconf', 8);
commit;

insert into ESTATUS_DOCUMENTO (id_estatus_documento, estatus_documento) values (0, 'Pendiente');
insert into ESTATUS_DOCUMENTO (id_estatus_documento, estatus_documento) values (1, 'Aprobado');
insert into ESTATUS_DOCUMENTO (id_estatus_documento, estatus_documento) values (2, 'Rechazado');
commit;

insert into CAUSA (id_causa, causa) values (1, 'Mala Calidad en la Imagen');
insert into CAUSA (id_causa, causa) values (2, 'Mala tipificación del Documento');
insert into CAUSA (id_causa, causa) values (3, 'Los Índices no concuerdan con el Documento');
insert into CAUSA (id_causa, causa) values (4, 'Mala Orientación del Documento');
insert into CAUSA (id_causa, causa) values (5, 'Visualización Nula del Documento');
insert into CAUSA (id_causa, causa) values (6, 'Falla Técnica del Sistema');
insert into CAUSA (id_causa, causa) values (7, 'Eliminacion de Documento');
commit;