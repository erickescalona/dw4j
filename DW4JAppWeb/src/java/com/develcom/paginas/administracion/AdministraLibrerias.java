/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Libreria;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.trazas.Traza;
import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;

/**
 *
 * @author develcom
 */
public class AdministraLibrerias extends SelectorComposer<Component> {

    @Wire
    private Listbox lstLibreriasExistentes;

    @Wire
    private Textbox txtLibreriasExistentes;

    @Wire
    private Radio rdActivo;

    @Wire
    private Radio rdInactivo;

    @Wire
    private Listbox lstLibreriasNuevas;

    private static final long serialVersionUID = 5717061681864512458L;

    private List<Libreria> listaLibrerias;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraLibrerias.class);
    private Session session;
    private ToolsFiles toolsFile = new ToolsFiles();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarTablaLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }

    }

    private void llenarTablaLibreria() {

        Listitem item;
        Listcell cell;

        try {

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {

                    item = new Listitem();

                    cell = new Listcell(lib.getDescripcion());
                    item.appendChild(cell);

                    cell = new Listcell(lib.getEstatus());
                    item.appendChild(cell);

                    item.setParent(lstLibreriasExistentes);

                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las librerias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las librerias", ex);
        }
    }

    @Listen("onClick = #btnAgregar")
    public void agregarLibreria() {

        String libreria, estatus = "";
        boolean banLista = true, banTabla = true;
        List<Listitem> LibreriasExistentes = new ArrayList<>();
        Listitem item;
        Listcell cell;

        try {

            libreria = txtLibreriasExistentes.getText();

            if (rdActivo.isSelected()) {
                estatus = rdActivo.getLabel();
            } else if (rdInactivo.isSelected()) {
                estatus = rdInactivo.getLabel();
            }

            if (!libreria.equalsIgnoreCase("")) {

                for (Libreria lib : listaLibrerias) {
                    String li = lib.getDescripcion();
                    if (li.equalsIgnoreCase(libreria)) {
                        banLista = false;
                        break;
                    }
                }

                LibreriasExistentes = lstLibreriasNuevas.getItems();

                for (Listitem libex : LibreriasExistentes) {
                    List<Listcell> cells = libex.getChildren();
                    String lib = cells.get(0).getLabel();
                    if (lib.equalsIgnoreCase(libreria)) {
                        banTabla = false;
                        break;
                    }
                }

                if (banTabla) {
                    if (banLista) {

                        item = new Listitem();

                        cell = new Listcell(libreria);
                        item.appendChild(cell);

                        cell = new Listcell(estatus);
                        item.appendChild(cell);

                        item.setParent(lstLibreriasNuevas);

                        txtLibreriasExistentes.setValue("");
                        txtLibreriasExistentes.setDisabled(false);

                    } else {

                        int n = Messagebox.show("Ya la libreria se encuentra en la lista de existentes \n ¿desea modificarlo?",
                                "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                                new org.zkoss.zk.ui.event.EventListener() {

                                    @Override
                                    public void onEvent(Event evt) throws Exception {
                                    }

                                });

                        if (n == Messagebox.YES) {

                            item = new Listitem();

                            cell = new Listcell(libreria);
                            item.appendChild(cell);

                            cell = new Listcell(estatus);
                            item.appendChild(cell);

                            item.setParent(lstLibreriasNuevas);

                            txtLibreriasExistentes.setValue("");
                            txtLibreriasExistentes.setDisabled(false);

                        } else {
                            txtLibreriasExistentes.setValue("");
                            txtLibreriasExistentes.setDisabled(false);
                        }
                    }
                } else {
                    herramientas.warn("Ya la Libreria se encuentra en la lista");
                }

            } else {
                herramientas.warn("Debe colocar un nombre de Libreria");
            }
        } catch (Exception e) {
            traza.trace("problemas al agregar la libreria", Level.ERROR, e);
            herramientas.error("Problemas al agregar la Libreria", e);
        }
    }

    @Listen("onClick = #btnCambiarEstatus")
    public void cambiarEstatus() {

        List<Listcell> cells = new ArrayList<>();
        Listitem item;

        item = lstLibreriasExistentes.getSelectedItem();
        cells = item.getChildren();

        txtLibreriasExistentes.setValue(cells.get(0).getLabel());
        txtLibreriasExistentes.setDisabled(true);
    }

    @Listen("onClick = #btnGuardar")
    public void guardarLibreria() {
        List<Libreria> librerias = new ArrayList<>();
        Libreria libreria;
        List<Listitem> items;
        List<Listcell> cells;
        boolean resp = false;

        try {

            int n = Messagebox.show("Seguro que desea guardar las librerias",
                    "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event evt) throws Exception {
                        }

                    });

            if (n == Messagebox.YES) {

                items = lstLibreriasNuevas.getItems();

                if (!items.isEmpty()) {
                    for (Listitem item : items) {
                        cells = item.getChildren();
                        libreria = new Libreria();

                        libreria.setDescripcion(cells.get(0).getLabel());

                        if (cells.get(1).getLabel().equalsIgnoreCase(rdActivo.getLabel())) {
                            libreria.setEstatus("1");
                        } else if (cells.get(1).getLabel().equalsIgnoreCase(rdInactivo.getLabel())) {
                            libreria.setEstatus("2");
                        }

                        for (Libreria lib : listaLibrerias) {
                            if (libreria.getDescripcion().equalsIgnoreCase(lib.getDescripcion())) {
                                libreria.setIdLibreria(lib.getIdLibreria());
                            }
                        }

                        librerias.add(libreria);
                    }

                    resp = new AdministracionAgregar().insertarLibreria(librerias);

                    if (resp) {
                        herramientas.info("Librerias guardadas con exito");
                    } else {
                        herramientas.warn("Problemas al guardar las Librerias");
                    }
                    limpiarComponentes();
                }else{
                    herramientas.info("La tabla de Librerias esta vacia");
                }

            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }

    }

    public void limpiarComponentes() {
        txtLibreriasExistentes.setText("");
        rdActivo.setSelected(false);
        rdInactivo.setSelected(false);
        lstLibreriasExistentes.getItems().clear();
        llenarTablaLibreria();
    }

}
