/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ve.net.develcom.tool;

import org.apache.log4j.Level;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Separator;
import ve.net.develcom.log.Traza;

/**
 *
 * @author develcom
 */
public class Herramientas {

    private Traza traza = new Traza(Herramientas.class);
    private Session session;
    public Div divInfo;

    public Session crearSesion() {

        if (session == null) {
            session = Sessions.getCurrent();
        }

        return session;
    }

    public void cerrarSesion() {
        session.invalidate();
    }

    public String navegador() {

        String userAgent = Executions.getCurrent().getBrowser();
        traza.trace("userAgent "+Executions.getCurrent().getUserAgent(), Level.INFO);
        return userAgent;
    }

    public void showError(String error, Exception exception) {
        Messagebox.show(error + "\n" + exception, "Error", Messagebox.OK,
                Messagebox.ERROR);

    }

    public void showInfo(String message) {
        Messagebox.show(message, "Informacion", Messagebox.OK,
                Messagebox.INFORMATION);
    }

    public void showWarn(String message) {
        Messagebox.show(message, "Informacion", Messagebox.OK,
                Messagebox.EXCLAMATION);
    }

    public void pageRedirection(String url) {
        Executions.sendRedirect(url);
    }
}
