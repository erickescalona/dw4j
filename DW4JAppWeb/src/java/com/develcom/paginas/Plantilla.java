/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas;

import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Caption;
import org.zkoss.zul.North;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Timer;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Window;

/**
 *
 * @author develcom
 */
public class Plantilla extends SelectorComposer<Component> {
    
    @Wire
    private Toolbarbutton tbUsuario;
    
    @Wire
    private Toolbarbutton tbFechaHora;
    
    @Wire
    private Caption capTitulo;
    
    @Wire
    private North norte;

    private static final long serialVersionUID = 2751412156421889796L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(MuestraDocumento.class);
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        iniciar();
    }
    
    private void iniciar(){
        
        String login, fecha, hora;
        SimpleDateFormat sdFecha = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm");
        Calendar calendar = new GregorianCalendar();
        
        try {
            
            session = herramientas.crearSesion();
            
            if(session != null){
                login = (String) session.getAttribute("login");
                fecha = sdFecha.format(calendar.getTime());
                hora = sdfHora.format(calendar.getTime());
                
//                norte.setTitle("Usuario: "+login+" - Fecha y Hora de inicio de Sesión: "+fecha+" ~ "+hora);
                
//                capTitulo.setLabel("Usuario: "+login);
//                capTitulo.setTooltiptext(login);
//                tbUsuario.setLabel("Usuario: "+login);
                tbUsuario.setImage("/img/user1111.png");
                tbUsuario.setLabel(login);
                tbUsuario.setTooltiptext(login);
                tbFechaHora.setLabel("Hora y Fecha de inicio de Sesión: "+hora+" - "+fecha);
                tbFechaHora.setTooltiptext(hora+" - "+fecha);
            }
        } catch (Exception e) {
            traza.trace("problema en la plantilla web", Level.ERROR, e);
            herramientas.error("Problemas general en la plantilla", e);
        }
        
    }

}
