/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.modal;

import com.develcom.administracion.Combo;
import com.develcom.dao.Expediente;
import com.develcom.tools.Herramientas;
import com.develcom.tools.excepcion.DW4JException;
import com.develcom.tools.trazas.Traza;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.xel.VariableResolver;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import ve.com.develcom.administracion.AdministracionBusqueda;
import ve.com.develcom.expediente.GestionDocumentos;
import ve.com.develcom.expediente.LLenarListaDesplegable;

/**
 *
 * @author develcom
 */
public class DatoAdicional extends SelectorComposer<Component> {

    private static final long serialVersionUID = -2498355629459915583L;

    @Wire
    private Label lblExpediente;

    @Wire
    private Label lblLibreria;

    @Wire
    private Label lblCategoria;

    @Wire
    private Label lblSubCategoria;

    @Wire
    private Label lblDocumento;

    @Wire
    private Window winDatoAdicional;

    @Wire
    private Grid panelDatosAdicionales;

    @Wire
    private Button btnGuardar;

    @Wire
    private Panel panelDatos;

    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(DatoAdicional.class);
    private Session session;
    private Expediente expediente;
    private List<com.develcom.administracion.DatoAdicional> lstDatosAdicionales;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {
        String accion;
        session = herramientas.crearSesion();

        if (session != null) {

            expediente = (Expediente) session.getAttribute("expediente");
            accion = expediente.getAccion();
            
            traza.trace("accion a realizar "+accion, Level.INFO);

            lblExpediente.setValue("Expediente: " + expediente.getIdExpediente());
            lblLibreria.setValue("Libreria: " + expediente.getLibreria());
            lblCategoria.setValue("Categoria: " + expediente.getCategoria());
            lblSubCategoria.setValue("SubCategoria: " + expediente.getSubCategoria());
            lblDocumento.setValue("Tipo de Documento: " + expediente.getTipoDocumento());

            if (accion.equalsIgnoreCase("guardar")) {
                panelDatos.setTitle("Agregar Datos Adicionales");
                crearDatosAdicionales();
            } else if (accion.equalsIgnoreCase("mostrar")) {
                panelDatos.setTitle("Vizualizar Datos Adicionales");
                btnGuardar.setVisible(false);
                mostrarDatosAdicionales();
            } else if (accion.equalsIgnoreCase("actualizar")) {
                panelDatos.setTitle("Agregar nuevos Datos Adicionales");
                actualizarDatosAdicionales();
            }

        } else {

            herramientas.warn("Problemas con la Sesión, posiblemente se venció");
            herramientas.navegarPagina("index.zul");
        }
    }

    private void crearDatosAdicionales() {

        Rows rows;
        Row row = null;
        Label label;
        Textbox textbox;
        Combobox combobox;
        int cont = 2;

        try {

            if (session != null) {

                lstDatosAdicionales = new AdministracionBusqueda().buscarIndDatoAdicional(expediente.getIdTipoDocumento());

                rows = panelDatosAdicionales.getRows();

                for (com.develcom.administracion.DatoAdicional ind : lstDatosAdicionales) {

                    if (cont == 2) {
                        cont = 0;
                        row = new Row();
                    }

                    if (ind.getTipo().equalsIgnoreCase("NUMERO")) {

                        traza.trace("id etiqueta numero " + ind.getIndiceDatoAdicional().replace(" ", "_"), Level.INFO);

                        label = new Label(ind.getIndiceDatoAdicional());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("TEXTO")) {

                        traza.trace("id etiqueta texto " + ind.getIndiceDatoAdicional().replace(" ", "_"), Level.INFO);

                        label = new Label(ind.getIndiceDatoAdicional());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("AREA")) {

                        traza.trace("id etiqueta area " + ind.getIndiceDatoAdicional().replace(" ", "_"), Level.INFO);

                        label = new Label(ind.getIndiceDatoAdicional());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        textbox.setRows(6);

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("COMBO")) {

                        traza.trace("id etiqueta combo " + ind.getIndiceDatoAdicional().replace(" ", "_"), Level.INFO);
                        Comboitem item;

                        label = new Label(ind.getIndiceDatoAdicional());

                        combobox = new Combobox();
                        combobox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));

                        List<Combo> datosCombo = new LLenarListaDesplegable().buscarData(ind.getCodigo(), false);

                        for (Combo combo : datosCombo) {
                            item = new Comboitem();
                            item.setValue(combo.getIdCombo());
                            item.setLabel(combo.getDatoCombo());
                            item.setParent(combobox);
                        }

                        row.appendChild(label);
                        row.appendChild(combobox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("FECHA")) {

                        traza.trace("id etiqueta fecha " + ind.getIndiceDatoAdicional().replace(" ", "_"), Level.INFO);

                        Label labelDesde = new Label(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        Datebox dateboxDesde = new Datebox();
                        dateboxDesde.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        dateboxDesde.setFormat("dd/MM/yyyy");

                        row.appendChild(labelDesde);
                        row.appendChild(dateboxDesde);

                        rows.appendChild(row);

                        cont++;
                    }

                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas creando los datos adicionales", Level.ERROR, ex);
        }
    }

    private void mostrarDatosAdicionales() {
        Rows rows = null;
        Row row = null;
        Label label;
        List<com.develcom.documento.DatoAdicional> lsDatosAdicionales;
        int numeroDocumento = 0, version = 0, cont = 2;

        try {

            if (session != null) {

                numeroDocumento = expediente.getNumeroDocumento();
                version = expediente.getVersionDocumento();

                lsDatosAdicionales = new GestionDocumentos().encontrarValorDatoAdicional(expediente.getIdTipoDocumento(), expediente.getIdExpediente(), numeroDocumento, version);

                rows = panelDatosAdicionales.getRows();

                for (com.develcom.documento.DatoAdicional da : lsDatosAdicionales) {

                    if (cont == 2) {
                        cont = 0;
                        row = new Row();
                    }
                    String arg = da.getIndiceDatoAdicional() + ": ";
                    String dato = da.getValor().toString().trim() + " ";

                    label = new Label(arg + dato);

                    row.appendChild(label);
                    rows.appendChild(row);

                    cont++;
                }

            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas mostrando los datos adicionales", Level.ERROR, ex);
        }
    }

    private Object buscarValorDatoAdicional(String datoAdicional, List<com.develcom.documento.DatoAdicional> lsIndDatosAdicionales) {
        Object valor = null;
        for (com.develcom.documento.DatoAdicional da : lsIndDatosAdicionales) {
            if (da.getIndiceDatoAdicional().equalsIgnoreCase(datoAdicional)) {
                valor = da.getValor();
            }
        }
        return valor;
    }

    private void actualizarDatosAdicionales() {

        List<com.develcom.documento.DatoAdicional> lsDatosAdicionales;
        Rows rows;
        Row row = null;
        Label label;
        Textbox textbox;
        Combobox combobox;
        int numeroDocumento, version, cont = 2;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        try {

            if (session != null) {

                numeroDocumento = expediente.getNumeroDocumento();
                version = expediente.getVersionDocumento();

                lsDatosAdicionales = new GestionDocumentos().encontrarValorDatoAdicional(expediente.getIdTipoDocumento(), expediente.getIdExpediente(), numeroDocumento, version);

                rows = panelDatosAdicionales.getRows();

                for (com.develcom.documento.DatoAdicional ind : lsDatosAdicionales) {

                    if (cont == 2) {
                        cont = 0;
                        row = new Row();
                    }

                    if (ind.getTipo().equalsIgnoreCase("NUMERO")) {

                        label = new Label(ind.getIndiceDatoAdicional());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        textbox.setValue(buscarValorDatoAdicional(ind.getIndiceDatoAdicional(), lsDatosAdicionales).toString());

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("TEXTO")) {

                        label = new Label(ind.getIndiceDatoAdicional());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        textbox.setValue(buscarValorDatoAdicional(ind.getIndiceDatoAdicional(), lsDatosAdicionales).toString());

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("AREA")) {

                        label = new Label(ind.getIndiceDatoAdicional());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        textbox.setRows(6);
                        textbox.setValue(buscarValorDatoAdicional(ind.getIndiceDatoAdicional(), lsDatosAdicionales).toString());

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("COMBO")) {
                        Comboitem item;

                        label = new Label(ind.getIndiceDatoAdicional());

                        combobox = new Combobox();
                        combobox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));

                        List<Combo> datosCombo = new LLenarListaDesplegable().buscarData(ind.getCodigo(), false);

                        for (Combo combo : datosCombo) {
                            item = new Comboitem();
                            item.setValue(combo.getIdCombo());
                            item.setLabel(combo.getDatoCombo());
                            item.setParent(combobox);
                        }
                        combobox.getSelectedItem().setValue(buscarValorDatoAdicional(ind.getIndiceDatoAdicional(), lsDatosAdicionales).toString());

                        row.appendChild(label);
                        row.appendChild(combobox);

                        rows.appendChild(row);

                        cont++;

                    } else if (ind.getTipo().equalsIgnoreCase("FECHA")) {

                        Label labelDesde = new Label(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        Datebox datebox = new Datebox();
                        datebox.setId(ind.getIndiceDatoAdicional().replace(" ", "_"));
                        datebox.setFormat("dd/MM/yyyy");
                        
                        datebox.setValue((Date) sdf.parse(buscarValorDatoAdicional(ind.getIndiceDatoAdicional(), lsDatosAdicionales).toString()));

                        row.appendChild(labelDesde);
                        row.appendChild(datebox);

                        rows.appendChild(row);

                        cont++;
                    }

                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas creando los datos adicionales", Level.ERROR, ex);
            herramientas.error("Problemas generando los Datos Adicionales", ex);
        } catch (ParseException ex) {
            traza.trace("problemas al colocal la fecha", Level.ERROR, ex);
            herramientas.error("Problemas al colocar la fecha (Dato Adicional)", ex);
        }
    }

    private List<com.develcom.administracion.DatoAdicional> convertir(List<com.develcom.documento.DatoAdicional> lsDatosAdicionales) {
        List<com.develcom.administracion.DatoAdicional> datosAdicionales = new ArrayList<>();
        com.develcom.administracion.DatoAdicional datoAdicional;

        for (com.develcom.documento.DatoAdicional da : lsDatosAdicionales) {
            datoAdicional = new com.develcom.administracion.DatoAdicional();

            datoAdicional.setCodigo(da.getCodigo());
            datoAdicional.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
            datoAdicional.setIdDatoAdicional(da.getIdDatoAdicional());
            datoAdicional.setIdTipoDocumento(da.getIdTipoDocumento());
            datoAdicional.setIdValor(da.getIdValor());
            datoAdicional.setNumeroDocumento(da.getNumeroDocumento());
            datoAdicional.setTipo(da.getTipo());
            datoAdicional.setValor(da.getValor());

            datosAdicionales.add(datoAdicional);
        }

        return datosAdicionales;
    }

    @Listen("onClick = #btnGuardar")
    public void guardarDatosAdicionales() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        try {

            if (session != null) {

                for (int i = 0; i < lstDatosAdicionales.size(); i++) {
//                for (com.develcom.administracion.DatoAdicional das : lstdDatosAdicionales) {

                    com.develcom.administracion.DatoAdicional das = lstDatosAdicionales.get(i);

                    if (panelDatosAdicionales.getFellowIfAny(das.getIndiceDatoAdicional().replace(" ", "_")) instanceof Textbox) {

                        Textbox textbox = (Textbox) panelDatosAdicionales.getFellowIfAny(das.getIndiceDatoAdicional().replace(" ", "_"));

                        traza.trace("dato adicional " + das.getIndiceDatoAdicional() + " valor " + textbox.getValue(), Level.INFO);

                        if (textbox.getValue() != null || !textbox.getValue().equalsIgnoreCase("")) {

                            lstDatosAdicionales.get(i).setValor(textbox.getValue());

                        } else {
                            String arg = das.getIndiceDatoAdicional().replace("_", " ");
                            arg = arg.toLowerCase();
                            char[] cs = arg.toCharArray();
                            char ch = cs[0];
                            cs[0] = Character.toUpperCase(ch);
                            arg = String.valueOf(cs);

                            throw new DW4JException("El " + arg + " no debe estar vacio");
                        }

                    } else if (panelDatosAdicionales.getFellowIfAny(das.getIndiceDatoAdicional().replace(" ", "_")) instanceof Combobox) {

                        Combobox combobox = (Combobox) panelDatosAdicionales.getFellowIfAny(das.getIndiceDatoAdicional().replace(" ", "_"));

                        traza.trace("dato adicional " + das.getIndiceDatoAdicional() + " valor " + combobox.getValue(), Level.INFO);

                        if (combobox.getValue() != null || !combobox.getValue().equalsIgnoreCase("")) {

                            lstDatosAdicionales.get(i).setValor(combobox.getSelectedItem().getValue());

                        } else {
                            String arg = das.getIndiceDatoAdicional().replace("_", " ");
                            arg = arg.toLowerCase();
                            char[] cs = arg.toCharArray();
                            char ch = cs[0];
                            cs[0] = Character.toUpperCase(ch);
                            arg = String.valueOf(cs);

                            throw new DW4JException("El " + arg + " no debe estar vacio");
                        }

                    } else if (panelDatosAdicionales.getFellowIfAny(das.getIndiceDatoAdicional().replace(" ", "_")) instanceof Datebox) {

                        Datebox datebox = (Datebox) panelDatosAdicionales.getFellowIfAny(das.getIndiceDatoAdicional().replace(" ", "_"));

                        traza.trace("argumento " + das.getIndiceDatoAdicional() + " valor " + datebox.getValue(), Level.INFO);

                        if (datebox.getValue() != null) {

                            lstDatosAdicionales.get(i).setValor(sdf.format(datebox.getValue()));

                        } else {
                            String arg = das.getIndiceDatoAdicional().replace("_", " ");
                            arg = arg.toLowerCase();
                            char[] cs = arg.toCharArray();
                            char ch = cs[0];
                            cs[0] = Character.toUpperCase(ch);
                            arg = String.valueOf(cs);

                            throw new DW4JException("El " + arg + " no debe estar vacio");
                        }

                    }

                }

                session.setAttribute("lstDatosAdicionales", lstDatosAdicionales);
                winDatoAdicional.setVisible(false);
                winDatoAdicional.detach();

            } else {
                herramientas.info("Sesión finalizada");
                herramientas.navegarPagina("index.zul");
            }

        } catch (DW4JException e) {
            herramientas.warn(e.getMessage());
        }

    }

    @Listen("onClick = #btnCerrar")
    public void showModal(Event e) {
        winDatoAdicional.detach();
    }

}
