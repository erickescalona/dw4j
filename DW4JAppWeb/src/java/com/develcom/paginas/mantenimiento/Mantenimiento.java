/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.mantenimiento;

import biz.source_code.base64Coder.Base64Coder;
import com.develcom.mantenimiento.Configuracion;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Textbox;
import ve.com.develcom.mantenimiento.MantenimientoVarios;

/**
 *
 * @author develcom
 */
public class Mantenimiento extends SelectorComposer<Component> {

    @Wire
    private Textbox txtIP;

    @Wire
    private Textbox txtNombre;

    @Wire
    private Textbox txtPuerto;

    @Wire
    private Textbox txtUsuario;

    @Wire
    private Textbox txtContrasenia;

    @Wire
    private Checkbox chkCalidad;

    @Wire
    private Checkbox chkFoliatura;

    @Wire
    private Checkbox chkFicha;

    @Wire
    private Checkbox chkCola;

    @Wire
    private Checkbox chkEliminar;

    private static final long serialVersionUID = -7145766348429567234L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(Mantenimiento.class);
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        try {

            Configuracion configuracion = new MantenimientoVarios().buscandoMantenimiento();

            txtIP.setValue(configuracion.getServerName());
            txtNombre.setValue(configuracion.getDatabaseName());
            txtPuerto.setValue(String.valueOf(configuracion.getPort()));
            txtUsuario.setValue(Base64Coder.decodeString(configuracion.getUser()));
            txtContrasenia.setValue(Base64Coder.decodeString(configuracion.getPassword()));

            chkCalidad.setChecked(configuracion.isCalidadActivo());
            chkFoliatura.setChecked(configuracion.isFoliatura());
            chkFicha.setChecked(configuracion.isFicha());
            chkCola.setChecked(configuracion.isFabrica());
            chkEliminar.setChecked(configuracion.isElimina());

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al buscar la actual configuracion del dw4j", Level.ERROR, ex);
            herramientas.error("Problemas buscando la actual configuración del DW4J", ex);
        }
    }

    @Listen("onClick = #btnGuardar")
    public void aceptar() {

        Configuracion config = new Configuracion();
        String servidor;
        String baseDatos;
        String usuario;
        String contrasenia;
        int puerto;
        char[] pass;
        boolean resp, calidad, foliatura, ficha, fabrica, eliminar;

        try {

            servidor = txtIP.getValue();
            baseDatos = txtNombre.getValue();
            usuario = txtUsuario.getValue();
            contrasenia = txtContrasenia.getValue();
            puerto = Integer.parseInt(txtPuerto.getValue());

            calidad = chkCalidad.isChecked();
            foliatura = chkFoliatura.isChecked();
            ficha = chkFicha.isChecked();
            fabrica = chkCola.isChecked();
            eliminar = chkEliminar.isChecked();

            if (!servidor.equalsIgnoreCase("")) {
                if (!baseDatos.equalsIgnoreCase("")) {
                    if (!usuario.equalsIgnoreCase("")) {
                        if (!contrasenia.equalsIgnoreCase("")) {
                            if (puerto != 0) {

                                config.setServerName(servidor);
                                config.setDatabaseName(baseDatos);
                                config.setPort(puerto);
                                config.setUser(usuario);
                                config.setPassword(contrasenia);
                                config.setCalidadActivo(calidad);
                                config.setFoliatura(foliatura);
                                config.setFicha(ficha);
                                config.setFabrica(fabrica);
                                config.setElimina(eliminar);

                                resp = new MantenimientoVarios().mantenimientoBaseDatos(config);
                                
                                if (resp) {
                                    herramientas.info("Exito en la configuracion");
                                } else {
                                    herramientas.warn("Problemas en la configuracion");
                                }

                                traza.trace("respuesta al realizar el mantenimiento " + resp, Level.INFO);
                            } else {
                                herramientas.warn("Debe colocar un numero de puerto");
                            }
                        } else {
                            herramientas.warn("Debe colocar una contraseña");
                        }
                    } else {
                        herramientas.warn("Debe colocar un usuario");
                    }
                } else {
                    herramientas.warn("Debe colocar un nombre de base de datos");
                }
            } else {
                herramientas.warn("Debe colocar un nombre de servidor o direccion ip");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al guardar la configuracion del dw4j", Level.ERROR, ex);
            herramientas.error("Problemas guardando la configuración del DW4J", ex);
        }
    }

}
