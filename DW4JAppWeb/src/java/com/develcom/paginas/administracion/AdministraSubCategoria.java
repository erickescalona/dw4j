/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.Libreria;
import com.develcom.administracion.Perfil;
import com.develcom.administracion.SubCategoria;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.trazas.Traza;
import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;

/**
 *
 * @author develcom
 */
public class AdministraSubCategoria extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibrerias;

    @Wire
    private Combobox cmbCategorias;

    @Wire
    private Listbox lstSubCategoriaExistentes;

    @Wire
    private Textbox txtSubCategoria;

    @Wire
    private Radio rdActivo;

    @Wire
    private Radio rdInactivo;

    @Wire
    private Listbox lstSubCategoriaNuevas;

    private static final long serialVersionUID = -2910377268067745947L;

    private List<SubCategoria> listaSubCategorias;
    private List<Categoria> listaCategorias;
    private List<Libreria> listaLibrerias;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraSubCategoria.class);
    private Session session;
    private ToolsFiles toolsFile = new ToolsFiles();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }

    }

    private void llenarLibreria() {

        Comboitem item;

        try {

            cmbLibrerias.getItems().clear();

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {
                    if (lib.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(lib.getIdLibreria());
                        item.setLabel(lib.getDescripcion());
                        item.setParent(cmbLibrerias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Librerias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onSelect = #cmbLibrerias")
    public void llenarCategoria() {

        Comboitem item;
        int idLibreria;

        try {

            cmbCategorias.getItems().clear();

            idLibreria = cmbLibrerias.getSelectedItem().getValue();
            listaCategorias = new AdministracionBusqueda().buscarCategoria("", idLibreria, 0);

            if (!listaCategorias.isEmpty()) {
                for (Categoria cat : listaCategorias) {
                    if (cat.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(cat.getIdCategoria());
                        item.setLabel(cat.getCategoria());
                        item.setParent(cmbCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Categorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onSelect = #cmbCategorias")
    public void llenarTablaSubCategoriaExistentes() {

        int idcategoria;
        Listitem item;
        Listcell cell;

        try {

            lstSubCategoriaExistentes.getItems().clear();

            idcategoria = cmbCategorias.getSelectedItem().getValue();
            listaSubCategorias = new AdministracionBusqueda().buscarSubCategoria("", idcategoria, 0);

            if (!listaSubCategorias.isEmpty()) {
                for (SubCategoria subCate : listaSubCategorias) {

                    item = new Listitem();

                    cell = new Listcell(subCate.getSubCategoria());
                    item.appendChild(cell);

                    cell = new Listcell(subCate.getEstatus());
                    item.appendChild(cell);

                    item.setParent(lstSubCategoriaExistentes);
                }
            } else {
                herramientas.warn("La Categoria no posee SubCategorias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onClick = #btnAgregar")
    public void agregarSubCategoria() {

        String libreria, categoria, estatus = "", subCategoria;
        boolean banLista = true, banTabla = true;
        List<Listitem> subCategoriaNuevas = new ArrayList<>();
        Listitem item;
        Listcell cell;

        try {

            libreria = cmbLibrerias.getSelectedItem().getLabel();
            categoria = cmbCategorias.getSelectedItem().getLabel();

            if (rdActivo.isSelected()) {
                estatus = rdActivo.getLabel();
            } else if (rdInactivo.isSelected()) {
                estatus = rdInactivo.getLabel();
            }

            if (!libreria.equalsIgnoreCase("")) {

                if (!categoria.equalsIgnoreCase("")) {

                    subCategoria = txtSubCategoria.getText();

                    if (!subCategoria.equalsIgnoreCase("")) {

                        for (SubCategoria cat : listaSubCategorias) {
                            String ca = cat.getSubCategoria();
                            if (ca.equalsIgnoreCase(subCategoria)) {
                                banLista = false;
                                break;
                            }
                        }

                        subCategoriaNuevas = lstSubCategoriaNuevas.getItems();

                        for (Listitem libex : subCategoriaNuevas) {
                            List<Listcell> cells = libex.getChildren();
                            String tmp = cells.get(0).getLabel();
                            if (tmp.equalsIgnoreCase(subCategoria)) {
                                banTabla = false;
                                break;
                            }
                        }

                        if (banTabla) {
                            if (banLista) {

                                item = new Listitem();

                                cell = new Listcell(libreria);
                                item.appendChild(cell);

                                cell = new Listcell(categoria);
                                item.appendChild(cell);

                                cell = new Listcell(subCategoria);
                                item.appendChild(cell);

                                cell = new Listcell(estatus);
                                item.appendChild(cell);

                                item.setParent(lstSubCategoriaNuevas);

                                txtSubCategoria.setValue("");
                                txtSubCategoria.setDisabled(false);

                            } else {

                                int n = Messagebox.show("Ya la categoria se encuentra en la lista de existentes \n ¿desea modificarlo?",
                                        "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                                        new org.zkoss.zk.ui.event.EventListener() {

                                            @Override
                                            public void onEvent(Event evt) throws Exception {
                                            }

                                        });

                                if (n == Messagebox.YES) {

                                    item = new Listitem();

                                    cell = new Listcell(libreria);
                                    item.appendChild(cell);

                                    cell = new Listcell(categoria);
                                    item.appendChild(cell);

                                    cell = new Listcell(subCategoria);
                                    item.appendChild(cell);

                                    cell = new Listcell(estatus);
                                    item.appendChild(cell);

                                    item.setParent(lstSubCategoriaNuevas);

                                    txtSubCategoria.setValue("");
                                    txtSubCategoria.setDisabled(false);

                                } else {
                                    txtSubCategoria.setValue("");
                                    txtSubCategoria.setDisabled(false);
                                }
                            }
                        } else {
                            herramientas.warn("Ya la Categoria se encuentra en la lista");
                        }

                    } else {
                        herramientas.warn("Debe colocar un nombre de la SubCategoria");
                    }
                } else {
                    herramientas.warn("Debe seleccionar una Categoria");
                }
            } else {
                herramientas.warn("Debe seleccionar una libreria");
            }

        } catch (Exception e) {
            traza.trace("problemas al agregar la libreria", Level.ERROR, e);
            herramientas.error("Problemas al agregar la Libreria", e);
        }
    }

    @Listen("onClick = #btnGuardar")
    public void guardarSubCategorias() {

        List<Perfil> SubCategorias = new ArrayList<>();
        Perfil subCategoria;
        Categoria cat;
        Libreria lib;
        SubCategoria subCat;
        List<Listitem> items;
        List<Listcell> cells;
        boolean resp;

        try {

            int n = Messagebox.show("Seguro que desea guardar las SubCategorias",
                    "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event evt) throws Exception {
                        }

                    });

            if (n == Messagebox.YES) {

                items = lstSubCategoriaNuevas.getItems();

                if (!items.isEmpty()) {
                    for (Listitem item : items) {
                        cells = item.getChildren();

                        subCategoria = new Perfil();
                        cat = new Categoria();
                        lib = new Libreria();
                        subCat = new SubCategoria();

                        lib.setDescripcion(cells.get(0).getLabel());

                        for (Libreria li : listaLibrerias) {
                            if (li.getDescripcion().equalsIgnoreCase(lib.getDescripcion())) {
                                lib.setIdLibreria(li.getIdLibreria());
                                if (li.getEstatus().equalsIgnoreCase("activo")) {
                                    lib.setEstatus("1");
                                } else if (li.getEstatus().equalsIgnoreCase("inactivo")) {
                                    lib.setEstatus("2");
                                }
                                break;
                            }
                        }

                        cat.setCategoria(cells.get(1).getLabel());

                        for (Categoria ca : listaCategorias) {
                            if (ca.getCategoria().equalsIgnoreCase(cat.getCategoria())) {
                                cat.setIdCategoria(ca.getIdCategoria());

                                if (ca.getEstatus().equalsIgnoreCase("activo")) {
                                    cat.setEstatus("1");
                                } else if (ca.getEstatus().equalsIgnoreCase("inactivo")) {
                                    cat.setEstatus("2");
                                }

                                break;
                            }
                        }

                        subCat.setSubCategoria(cells.get(2).getLabel());

                        if (cells.get(3).getLabel().equalsIgnoreCase(rdActivo.getLabel())) {
                            subCat.setEstatus("1");
                        } else if (cells.get(3).getLabel().equalsIgnoreCase(rdInactivo.getLabel())) {
                            subCat.setEstatus("2");
                        }

                        for (SubCategoria sbc : listaSubCategorias) {
                            if (subCat.getSubCategoria().equalsIgnoreCase(sbc.getSubCategoria())) {
                                subCat.setIdSubCategoria(sbc.getIdSubCategoria());
                            }
                        }

                        subCategoria.setLibreria(lib);
                        subCategoria.setCategoria(cat);
                        subCategoria.setSubCategoria(subCat);

                        SubCategorias.add(subCategoria);
                    }

                    resp = new AdministracionAgregar().insertarSubCategoria(SubCategorias);

                    if (resp) {
                        herramientas.info("SubCategorias guardadas con exito");
                    } else {
                        herramientas.warn("Problemas al guardar las SubCategorias");
                    }
                    limpiarComponentes();
                } else {
                    herramientas.info("La tabla de SubCategorias esta vacia");
                }

            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al guardar las subcategorias", Level.ERROR, ex);
            herramientas.error("Problemas al guardar las SubCategorias", ex);
        }

    }

    @Listen("onClick = #btnCambiarEstatus")
    public void cambiarEstatus() {

        List<Listcell> cells = new ArrayList<>();
        Listitem item;

        item = lstSubCategoriaExistentes.getSelectedItem();
        cells = item.getChildren();

        txtSubCategoria.setValue(cells.get(0).getLabel());
        txtSubCategoria.setDisabled(true);
    }

    private void limpiarComponentes() {
        txtSubCategoria.setText("");
        rdActivo.setSelected(false);
        rdInactivo.setSelected(false);
        lstSubCategoriaExistentes.getItems().clear();
        lstSubCategoriaNuevas.getItems().clear();
        cmbCategorias.getItems().clear();
        cmbLibrerias.setText("");
        cmbCategorias.setText("");
        llenarLibreria();
    }

}
