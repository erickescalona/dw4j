/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.tools;

import com.develcom.log.Traza;
import com.develcom.tools.cryto.CodDecodArchivos;
import com.sun.media.imageioimpl.plugins.tiff.TIFFImageReader;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import org.apache.log4j.Level;



/**
 * Clase de utilidad
 * para documentos
 * digitalizados
 * @author develcom
 */
public class ToolsFile {
    /**
     * Lista de bufer de imagenes
     */
    private ArrayList<BufferedImage> imagenesTiff;
    /**
     * Archivo tif para
     * guardar
     */
    private File fileImageTif;
    /**
     * Ruta completa del
     * archivo tiff
     */
    private static String archivoTif;
    /**
     * Ruta completa del archivo
     */
    private String tempPath;
    /**
     * Ruta completa del
     * archivo
     */
    private String archivo;
    /**
     * Ruta completa del
     * archivo codificado
     */
    private static String archivoCodificado;
    /**
     * Directorio temporal
     */
    private File dirTemporal;
    /**
     * Escribe trazas en el log
     */
    private Traza traza = new Traza(ToolsFile.class);

    /**
     * Constructor, crea el directorio
     * temporarl, arma las rutas del los
     * archivo tif y codificado e inicializa
     * las propiedades
     */
    public ToolsFile() {

        String rutaTemp = "temp";// ManejoSesion.getPropedades().getProperty("pathTmp");
        String userdir=System.getProperty("user.dir");
        String so = System.getProperty("os.name");
        String home = System.getProperty("user.home");
        
        String win = so.substring(0, 6);
        int index = so.indexOf("windows");
        boolean ok = so.startsWith("Windows");
        
        
        //if(win.equalsIgnoreCase("windows")){
        if(ok){
            
            dirTemporal = new File("c:\\"+Traza.RUTALOCAL+"\\temp");
            tempPath = "c:\\"+Traza.RUTALOCAL+"\\temp\\";
            archivo = "c:\\"+Traza.RUTALOCAL+"\\temp\\";
            archivoTif = "c:\\"+Traza.RUTALOCAL+"\\temp\\documento.tiff";
            archivoCodificado = "c:\\"+Traza.RUTALOCAL+"\\temp\\codificado.cod";
            
        }else if(so.equalsIgnoreCase("linux")){
            
            dirTemporal = new File(home+"/"+Traza.RUTALOCAL+"/temp");
            tempPath = userdir + "/" + rutaTemp + "/";
            archivo = userdir+"/"+rutaTemp+"/";
            archivoTif = userdir+"/"+rutaTemp+"/documento.tiff";
            archivoCodificado = userdir+"/"+rutaTemp+"/codificado.cod";
            
        }
        
        
        if(!dirTemporal.exists()){
            dirTemporal.mkdir();
        }

//        tempPath = userdir + "/" + rutaTemp + "/";
//        archivo = userdir+"/"+rutaTemp+"/";
//        archivoTif = userdir+"/"+rutaTemp+"/documento.tiff";//+ManejoSesion.getPropedades().getProperty("fileTif");
//        archivoCodificado = userdir+"/"+rutaTemp+"/codificado.cod";//+ManejoSesion.getPropedades().getProperty("fileCode");
        fileImageTif = new File(archivoTif);
        imagenesTiff = new ArrayList<BufferedImage>();
    }


//    public ArrayList<BufferedImage> abrir(String filename){
//
//        ImageReader reader;
//        FileSeekableStream is;
//        RenderedOp renderedOp;
//        Raster r;
//        PlanarImage planarImage;
//
//        RenderedImage renderedImage;
//
//        try{
//
//
//            File file = new File("/home/develcom/certificado/prueba.tiff");
//            SeekableStream s = new FileSeekableStream(file);
//
//            TIFFDecodeParam param = null;
//
//            ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, param);
//
//            System.out.println("Number of images in this TIFF: " +
//                               dec.getNumPages());
//
//
//
////            is = new FileSeekableStream (file);
////            image = JAI.create("stream", s);
////
////
////            for(int i=0;i<dec.getNumPages();i++){
////                imagenesTiff.add(image.getAsBufferedImage());
////            }
//
//
//
//
//
//            // Which of the multiple images in the TIFF file do we want to load
//            // 0 refers to the first, 1 to the second and so on.
//            int imageToLoad = 0;
//            for(int i = 0; i<dec.getNumPages();i++){
//
//
//                renderedImage = new NullOpImage(dec.decodeAsRenderedImage(i),
//                                    null,
//                                    OpImage.OP_IO_BOUND,
//                                    null);
//                planarImage = (PlanarImage) renderedImage;
//
//                //renderedOp = (RenderedOp) planarImage;
//
//                imagenesTiff.add(planarImage.getAsBufferedImage());
//
//            }
//        }catch(IOException e){
//
//            traza.trace("error prueba ", Level.INFO, e);
//
//        }
//
//        return imagenesTiff;
//
//    }

    /**
     * Abre un archivo tiff
     * @param filename
     * Ruta del archivo
     * @return
     * Una lista con bufer
     * de imagenes (BufferedImage)
     * @throws IOException
     * Lanza un IOException si no
     * consigue el archivo
     */
    public List<BufferedImage> open(String filename) throws IOException {


        long t1, t2, dif;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        Calendar tiempoInicio = Calendar.getInstance();
        //Calendar tiempoDiferencia = Calendar.getInstance();
        Calendar tiempoFinal;
        GregorianCalendar tiempo = new GregorianCalendar();

        int numPag;
        t1 = tiempoInicio.getTimeInMillis();
        //long startTime = (new java.util.Date()).getTime();
        //filename = "/home/develcom/imagene/imagen-en-formato-gif.gif";

        traza.trace("abriendo la imagen "+filename, Level.INFO);

        String ext = filename.substring(filename.lastIndexOf('.') + 1);
        Iterator readers = ImageIO.getImageReadersByFormatName(ext);
        if (!readers.hasNext()) {
            throw new IOException(getClass().getName() + ".open:\n\tNo reader for format '" + ext + "' available.");
        }


        //TIFFImageReader imageReader = (TIFFImageReader) readers.next();
        ImageReader imageReader = (ImageReader) readers.next();
        //ImageReadParam param = imageReader.getDefaultReadParam();
        //param.setSourceRegion (new Rectangle (0, 0, 50, 50));
        traza.trace("nombre imagen "+imageReader.getClass().getName(), Level.INFO);

//        while (!imageReader.getClass().getName().startsWith("uk.co.mmscomputing") && readers.hasNext()) {// prefer our own reader
//            //imageReader = (ImageReader) readers.next();
//            imageReader = (TIFFImageReader) readers.next();
//        }

        File fichero = new File(filename);
        ImageInputStream iis = ImageIO.createImageInputStream(fichero);
        try {
            iis.mark();
            imageReader.setInput(iis);
            numPag = imageReader.getNumImages(true);
            traza.trace("numeros de paginas "+numPag, Level.INFO);
            //System.out.println("Numero de paginas: "+reader.getNumImages(true));
            try {
                for (int i = 0; i< numPag ; i++) {
                    //IIOMetadata md =imageReader.getImageMetadata(i);
                    //if(md!=null){new MetadataReader().read(md);}
                    imagenesTiff.add(imageReader.read(i));
                }
            } catch (IndexOutOfBoundsException ioobe) {
                traza.trace("fuera del indice, abriendo imagen ", Level.ERROR, ioobe);
            }
        } catch (Error e) {
            //System.out.println("9\b" + getClass().getName() + ".open:\n\t" + e);
            traza.trace("error al abrir la imagen", Level.ERROR, e);
            traza.trace("9\b" + getClass().getName() + ".open:\n\t", Level.ERROR, e);
            throw e;
        } finally {
            iis.close();
        }
        
        tiempoFinal = Calendar.getInstance();
        t2 = tiempoFinal.getTimeInMillis();
        dif = t2 - t1;
        tiempo.setTimeInMillis(dif);
        traza.trace("Tiempo en abrir la imagen " + sdf.format(tiempo.getTime()), Level.INFO);
        
//        long endTime = (new java.util.Date()).getTime();
//        double diffTime2 = (endTime - startTime) * 0.001;
//        if(diffTime2 > 60000){
//            diffTime2 = diffTime2 / 60;
//            traza.trace("tiempo en abrir el archivo "+diffTime2+" min", Level.INFO);
//        }
//        traza.trace("tiempo en abrir el archivo " + (diffTime2 * 0.001) / 60 + " min", Level.INFO);
        return imagenesTiff;
    }

//    private IIOImage getIIOImage(ImageWriter writer,ImageWriteParam iwp,BufferedImage image){
//        ImageTypeSpecifier   it  = ImageTypeSpecifier.createFromRenderedImage(image);
//
//        /*
//        try{
//        uk.co.mmscomputing.imageio.bmp.BMPMetadata md=(uk.co.mmscomputing.imageio.bmp.BMPMetadata)image.getProperty("iiometadata");
//        if(md!=null){
//        md.setXPixelsPerMeter(11812);    // force 300 dpi for bmp images
//        md.setYPixelsPerMeter(11812);    // works only with mmsc.bmp package
//        }
//        }catch(Exception e){}
//        */
//
//        IIOMetadata md;
//        Object      obj=image.getProperty("iiometadata");               // if image is a TwainBufferedImage get metadata
//        if((obj!=null) && (obj instanceof IIOMetadata)){
//            md = (IIOMetadata)obj;
//        }else{
//            md = writer.getDefaultImageMetadata(it,iwp);
//        }
//        return new IIOImage(image,null,md);
//    }




    /** 
     * Guarda un archivo escaneado
     * @param imageBuffer
     * Lista de bufer de imagen
     * (BufferedImage) del archivo
     * tif
     * @return
     * Verdadero si lo guardo, falso
     * en caso contrario
     */
//    public boolean save(List<BufferedImage> imageBuffer){
//        traza.trace("salvando la imagen", Level.INFO);
//        TIFFImageWriteParam tiffiwp;
//
//        boolean scaneado=false;
//        ImageOutputStream imgOupStr;
//        ImageWriteParam    iwp;
//        IIOParamController controller;
//        int size=0;
//
//
//        if(!imageBuffer.isEmpty()){
//            try{
//                Iterator writers=ImageIO.getImageWritersByFormatName("tiff");
//                ImageWriter writer=(ImageWriter)writers.next();
//
//                tiffiwp = (TIFFImageWriteParam) writer.getDefaultWriteParam();
//                //tiffiwp.setPhotometricInterpretation("YCbCr");
//                tiffiwp.setCompressionType("jpeg");
//                tiffiwp.setCompressionQuality(0.5f);
//
//                controller = tiffiwp.getController();
//    //            if(controller!=null){
//    //                controller.activate(tiffiwp);
//    //            }
//
//    //            iwp = writer.getDefaultWriteParam();
//    //            controller = iwp.getController();
//    //            if(controller!=null){
//    //                controller.activate(iwp);
//    //            }
//
//                long time=System.currentTimeMillis();
//                traza.trace("tiempo antes de salvar la imagen "+time, Level.INFO);
//
//                imgOupStr=ImageIO.createImageOutputStream(fileImageTif);
//                writer.setOutput(imgOupStr);
//
//                size=imageBuffer.size();
//                traza.trace("se escanearon "+size+" documentos", Level.INFO);
//
//                if(writer.canWriteSequence()){                               //i.e tiff,sff(fax)
//                    writer.prepareWriteSequence(null);
//
//                    for(int i=0;i<size;i++){
//                        //writer.writeToSequence(getIIOImage(writer,iwp,imageBuffer.get(i)),iwp);
//                        writer.writeToSequence(getIIOImage(writer,tiffiwp,imageBuffer.get(i)),tiffiwp);
//                    }
//
//                    writer.endWriteSequence();
//                }else{
//
//                    for(int i=1; i<size; i++){
//
//                        if(writer.canInsertImage(i)){
//                            //writer.write(null,getIIOImage(writer,iwp,imageBuffer.get(i)),iwp);
//                            writer.write(null,getIIOImage(writer,tiffiwp,imageBuffer.get(i)),tiffiwp);
//                        }else{
//                            throw new IOException("Image Writer cannot append image ["+i+"] ("+fileImageTif.getName()+")");
//                        }
//                    }
//                }
//                long tim=System.currentTimeMillis()-time;
//                traza.trace("tiempo despues de salvar la imagen "+tim, Level.INFO);
//                time=time-tim;
//
//                scaneado = true;
//                imgOupStr.close();
//                traza.trace("tiempo usado para salvar la imagen es de: "+time+" milisegundos", Level.INFO);
//
//            }catch(Exception e){
//                traza.trace("Error al salvar el archivo escaneado: "+fileImageTif.getName(), Level.ERROR, e);
//
//            }
//        }
//        return scaneado;
//    }

    /**
     * Codifica el archivo
     */
    public void codificar(){
        new CodDecodArchivos().codificar(archivoTif, archivoCodificado);
    }

    /**
     * Codifica el archivo
     * @param file
     * El archivo a
     * codificar
     */
    public void codificar(File file){
        new CodDecodArchivos().codificar(file.getAbsolutePath(), archivoCodificado);
    }

    /**
     * Decodifica el archivo
     */
    public void decodificar(){
        new CodDecodArchivos().decodificar(archivoCodificado, archivoTif);
    }    
    

    public void decodificar(String nombre) {
        new CodDecodArchivos().decodificar(archivoCodificado, tempPath + nombre);
    }

//    public void decodificar(String nombre){
//        new CodDecodArchivos().decodificar(archivoCodificado, archivo+nombre);
//    }
    /**
     * Se obtiene el archivo
     * codificado
     * @return
     * El archivo codificado
     */
    public String getArchivoCodificado() {
        return archivoCodificado;
    }

    /**
     * Se obtiene el archivo tiff
     * @return
     * El archivo tif
     */
    public String getArchivoTif() {
        return archivoTif;
    }

    public String getTempPath() {
        return tempPath;
    }


    public String getArchivo() {
        return archivo;
    }
    /**
     * Se obtiene el
     * directorio temporarl
     * @return
     * El directorio temporal
     */
    public File getDirTemporal() {
        return dirTemporal;
    }
    
}
