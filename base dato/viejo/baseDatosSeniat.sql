-----------------------------------------------------------
-- Export file for user ORADW4J                          --
-- Created by Administrador on 17/09/2014, 02:47:49 p.m. --
-----------------------------------------------------------

set define off
spool baseDatosSeniat.log

prompt
prompt Creating table CATEGORIA
prompt ========================
prompt
create table ORADW4J.CATEGORIA
(
  id_categoria INTEGER not null,
  id_libreria  INTEGER not null,
  categoria    VARCHAR2(200) not null,
  id_estatus   INTEGER not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.CATEGORIA
  add constraint CATEGORIA_PK primary key (ID_CATEGORIA)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table CAUSA
prompt ====================
prompt
create table ORADW4J.CAUSA
(
  id_causa INTEGER not null,
  causa    VARCHAR2(150) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.CAUSA
  add constraint CAUSA_PK primary key (ID_CAUSA)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table CONFIGURACION
prompt ============================
prompt
create table ORADW4J.CONFIGURACION
(
  id_configuracion INTEGER not null,
  calidad          CHAR(1),
  ruta_temporal    VARCHAR2(50),
  archivo_tif      VARCHAR2(50),
  archivo_cod      VARCHAR2(50),
  log              VARCHAR2(50),
  foliatura        CHAR(1),
  server_name      VARCHAR2(50),
  database_name    VARCHAR2(50),
  port             INTEGER,
  userbd           VARCHAR2(50),
  password         VARCHAR2(50),
  ficha            CHAR(1),
  fabrica          CHAR(1),
  elimina          CHAR(1)
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.CONFIGURACION
  add constraint PK_CONFIGURACION primary key (ID_CONFIGURACION)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table SUBCATEGORIA
prompt ===========================
prompt
create table ORADW4J.SUBCATEGORIA
(
  id_subcategoria INTEGER not null,
  id_categoria    INTEGER not null,
  subcategoria    VARCHAR2(200) not null,
  id_estatus      VARCHAR2(20) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.SUBCATEGORIA
  add constraint SUBCATEGORIA_PK primary key (ID_SUBCATEGORIA)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table TIPODOCUMENTO
prompt ============================
prompt
create table ORADW4J.TIPODOCUMENTO
(
  id_documento    INTEGER not null,
  id_categoria    INTEGER not null,
  id_subcategoria INTEGER not null,
  tipo_documento  VARCHAR2(200) not null,
  id_estatus      INTEGER not null,
  vencimiento     CHAR(1),
  dato_adicional  CHAR(1),
  ficha           CHAR(1)
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.TIPODOCUMENTO
  add constraint TIPODOCUMENTO_PK primary key (ID_DOCUMENTO)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.TIPODOCUMENTO
  add constraint FK_TIPODOC_CATEGORIA foreign key (ID_CATEGORIA)
  references ORADW4J.CATEGORIA (ID_CATEGORIA);
alter table ORADW4J.TIPODOCUMENTO
  add constraint FK_TIPODOC_SUBCATEGORIA foreign key (ID_SUBCATEGORIA)
  references ORADW4J.SUBCATEGORIA (ID_SUBCATEGORIA);

prompt
prompt Creating table DATO_ADICIONAL
prompt =============================
prompt
create table ORADW4J.DATO_ADICIONAL
(
  id_dato_adicional INTEGER not null,
  indice_adicional  VARCHAR2(250) not null,
  tipo              VARCHAR2(50) not null,
  id_documento      INTEGER not null,
  codigo            INTEGER
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.DATO_ADICIONAL
  add constraint PK_DATO_ADICIONAL primary key (ID_DATO_ADICIONAL)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.DATO_ADICIONAL
  add constraint FK_DATO_ADICIONAL_TIPODOC foreign key (ID_DOCUMENTO)
  references ORADW4J.TIPODOCUMENTO (ID_DOCUMENTO);

prompt
prompt Creating table DOCUMENTO_ELIMINADO
prompt ==================================
prompt
create table ORADW4J.DOCUMENTO_ELIMINADO
(
  id_doc_eliminado  INTEGER not null,
  id_expediente     VARCHAR2(50) not null,
  id_libreria       INTEGER not null,
  id_categoria      INTEGER not null,
  id_subcategoria   INTEGER not null,
  id_documento      INTEGER not null,
  numero_documento  INTEGER not null,
  version           INTEGER not null,
  paginas           INTEGER not null,
  fecha_vencimiento DATE,
  fecha_eliminado   DATE not null,
  usuario_elimino   VARCHAR2(30) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.DOCUMENTO_ELIMINADO
  add constraint DOCUMENTO_ELIMINADO_PK primary key (ID_DOC_ELIMINADO)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table ESTATUS_DOCUMENTO
prompt ================================
prompt
create table ORADW4J.ESTATUS_DOCUMENTO
(
  id_estatus_documento INTEGER not null,
  estatus_documento    VARCHAR2(20) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.ESTATUS_DOCUMENTO
  add constraint ESTATUS_DOCUMENTO_PK primary key (ID_ESTATUS_DOCUMENTO)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table INFODOCUMENTO
prompt ============================
prompt
create table ORADW4J.INFODOCUMENTO
(
  id_infodocumento  INTEGER not null,
  id_documento      INTEGER not null,
  id_expediente     VARCHAR2(50),
  nombre_archivo    VARCHAR2(1000),
  ruta_archivo      VARCHAR2(1000),
  formato           VARCHAR2(4),
  numero_documento  INTEGER not null,
  version           INTEGER not null,
  paginas           INTEGER not null,
  fecha_vencimiento DATE,
  estatus_documento INTEGER not null,
  re_digitalizado   CHAR(1) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.INFODOCUMENTO
  add constraint PK_INFORDOCUMENTO primary key (ID_INFODOCUMENTO)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.INFODOCUMENTO
  add constraint FK_INFODOC_STATUSDOC foreign key (ESTATUS_DOCUMENTO)
  references ORADW4J.ESTATUS_DOCUMENTO (ID_ESTATUS_DOCUMENTO);
alter table ORADW4J.INFODOCUMENTO
  add constraint FK_INFODOC_TIPODOC foreign key (ID_DOCUMENTO)
  references ORADW4J.TIPODOCUMENTO (ID_DOCUMENTO);

prompt
prompt Creating table DATOS_INFODOCUMENTO
prompt ==================================
prompt
create table ORADW4J.DATOS_INFODOCUMENTO
(
  id_datos             INTEGER not null,
  id_infodocumento     INTEGER,
  id_doc_eliminado     INTEGER,
  fecha_digitalizacion DATE,
  usuario_digitalizo   VARCHAR2(30),
  fecha_aprobacion     DATE,
  usuario_aprobacion   VARCHAR2(30),
  fecha_rechazo        DATE,
  usuario_rechazo      VARCHAR2(30),
  motivo_rechazo       VARCHAR2(300),
  causa_rechazo        VARCHAR2(300),
  fecha_eliminado      DATE,
  usuario_elimino      VARCHAR2(30),
  motivo_elimino       VARCHAR2(300),
  causa_elimino        VARCHAR2(300),
  dato_adicional       VARCHAR2(30)
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.DATOS_INFODOCUMENTO
  add constraint PK_DATOS_INFODOCUMENTO primary key (ID_DATOS)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.DATOS_INFODOCUMENTO
  add constraint FK_DATOC_INFODOC foreign key (ID_INFODOCUMENTO)
  references ORADW4J.INFODOCUMENTO (ID_INFODOCUMENTO);
alter table ORADW4J.DATOS_INFODOCUMENTO
  add constraint FK_DATOS_DOC_ELIMINADO foreign key (ID_DOC_ELIMINADO)
  references ORADW4J.DOCUMENTO_ELIMINADO (ID_DOC_ELIMINADO);

prompt
prompt Creating table ESTATUS
prompt ======================
prompt
create table ORADW4J.ESTATUS
(
  id_estatus INTEGER not null,
  estatus    VARCHAR2(100) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.ESTATUS
  add constraint ESTATUS_PK primary key (ID_ESTATUS)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table LIBRERIA
prompt =======================
prompt
create table ORADW4J.LIBRERIA
(
  id_libreria INTEGER not null,
  libreria    VARCHAR2(200) not null,
  id_estatus  INTEGER not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.LIBRERIA
  add constraint LIBRERIA_PK primary key (ID_LIBRERIA)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table EXPEDIENTES
prompt ==========================
prompt
create table ORADW4J.EXPEDIENTES
(
  id_expedientes INTEGER not null,
  expediente     VARCHAR2(250) not null,
  id_indice      INTEGER not null,
  valor          VARCHAR2(250),
  id_libreria    INTEGER not null,
  id_categoria   INTEGER not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.EXPEDIENTES
  add constraint PK_EXPEDIENTES primary key (ID_EXPEDIENTES)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.EXPEDIENTES
  add constraint FK_EXPEDIENTES_CATEGORIA foreign key (ID_CATEGORIA)
  references ORADW4J.CATEGORIA (ID_CATEGORIA);
alter table ORADW4J.EXPEDIENTES
  add constraint FK_EXPEDIENTES_LIBRERIA foreign key (ID_LIBRERIA)
  references ORADW4J.LIBRERIA (ID_LIBRERIA);

prompt
prompt Creating table USUARIO
prompt ======================
prompt
create table ORADW4J.USUARIO
(
  id_usuario VARCHAR2(50) not null,
  nombre     VARCHAR2(100) not null,
  apellido   VARCHAR2(100) not null,
  cedula     VARCHAR2(50),
  sexo       CHAR(1),
  id_estatus INTEGER not null,
  password   VARCHAR2(16)
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.USUARIO
  add constraint USUARIO_PK primary key (ID_USUARIO)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.USUARIO
  add constraint FK_USUARIO_ESTATUS foreign key (ID_ESTATUS)
  references ORADW4J.ESTATUS (ID_ESTATUS);

prompt
prompt Creating table FABRICA
prompt ======================
prompt
create table ORADW4J.FABRICA
(
  usuario VARCHAR2(50) not null,
  fabrica CHAR(1) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.FABRICA
  add constraint PK_FABRICA primary key (USUARIO)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.FABRICA
  add constraint FK_FABRICA_USUARIO foreign key (USUARIO)
  references ORADW4J.USUARIO (ID_USUARIO);

prompt
prompt Creating table FOLIATURA
prompt ========================
prompt
create table ORADW4J.FOLIATURA
(
  id_foliatura     INTEGER not null,
  id_infodocumento INTEGER not null,
  id_documento     INTEGER not null,
  id_expediente    VARCHAR2(50) not null,
  pagina           INTEGER not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.FOLIATURA
  add constraint FOLIATURA_PK primary key (ID_FOLIATURA)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table INDICES
prompt ======================
prompt
create table ORADW4J.INDICES
(
  id_indice    INTEGER not null,
  id_categoria INTEGER not null,
  indice       VARCHAR2(250) not null,
  tipo         VARCHAR2(50) not null,
  codigo       INTEGER not null,
  clave        CHAR(1)
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.INDICES
  add constraint PK_INDICE primary key (ID_INDICE)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.INDICES
  add constraint FK_INDICE_CATEGORIA foreign key (ID_CATEGORIA)
  references ORADW4J.CATEGORIA (ID_CATEGORIA);

prompt
prompt Creating table LISTA_DESPLEGABLES
prompt =================================
prompt
create table ORADW4J.LISTA_DESPLEGABLES
(
  id_lista      INTEGER not null,
  codigo_indice INTEGER not null,
  descripcion   VARCHAR2(200)
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.LISTA_DESPLEGABLES
  add constraint PK_COMBO primary key (ID_LISTA)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table ROL
prompt ==================
prompt
create table ORADW4J.ROL
(
  id_rol INTEGER not null,
  rol    VARCHAR2(50) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.ROL
  add constraint ROL_PK primary key (ID_ROL)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );

prompt
prompt Creating table PERFIL
prompt =====================
prompt
create table ORADW4J.PERFIL
(
  id_perfil    INTEGER not null,
  id_libreria  INTEGER,
  id_categoria INTEGER,
  id_usuario   VARCHAR2(50) not null,
  id_rol       INTEGER not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.PERFIL
  add constraint PERFIL_PK primary key (ID_PERFIL)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.PERFIL
  add constraint FK_PERFIL_CATEGORIA foreign key (ID_CATEGORIA)
  references ORADW4J.CATEGORIA (ID_CATEGORIA);
alter table ORADW4J.PERFIL
  add constraint FK_PERFIL_LIBRERIA foreign key (ID_LIBRERIA)
  references ORADW4J.LIBRERIA (ID_LIBRERIA);
alter table ORADW4J.PERFIL
  add constraint FK_PERFIL_ROL foreign key (ID_ROL)
  references ORADW4J.ROL (ID_ROL);
alter table ORADW4J.PERFIL
  add constraint FK_PERFIL_USUARIO foreign key (ID_USUARIO)
  references ORADW4J.USUARIO (ID_USUARIO);

prompt
prompt Creating table VALOR_DATO_ADICIONAL
prompt ===================================
prompt
create table ORADW4J.VALOR_DATO_ADICIONAL
(
  id_valor          INTEGER not null,
  id_dato_adicional INTEGER not null,
  valor             VARCHAR2(250) not null,
  numero            INTEGER not null,
  version           INTEGER not null,
  expediente        VARCHAR2(250) not null
)
tablespace GESTOR
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.VALOR_DATO_ADICIONAL
  add constraint PK_VALOR_DATO_ADICIONAL primary key (ID_VALOR)
  using index 
  tablespace GESTOR
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
alter table ORADW4J.VALOR_DATO_ADICIONAL
  add constraint KF_VALOR_INDICE_DA foreign key (ID_DATO_ADICIONAL)
  references ORADW4J.DATO_ADICIONAL (ID_DATO_ADICIONAL);

prompt
prompt Creating sequence SQ_CATEGRORIA
prompt ===============================
prompt
create sequence ORADW4J.SQ_CATEGRORIA
minvalue 1
maxvalue 9999999999999999999999999999
start with 41
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_COMBO
prompt ==========================
prompt
create sequence ORADW4J.SQ_COMBO
minvalue 1
maxvalue 9999999999999999999999999999
start with 61
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_DATO_ADICIONAL
prompt ===================================
prompt
create sequence ORADW4J.SQ_DATO_ADICIONAL
minvalue 1
maxvalue 9999999999999999999999999999
start with 81
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_DATOS_INFODOCUMENTO
prompt ========================================
prompt
create sequence ORADW4J.SQ_DATOS_INFODOCUMENTO
minvalue 1
maxvalue 9999999999999999999999999999
start with 101
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_DOCUMENTO_ELIMINADO
prompt ========================================
prompt
create sequence ORADW4J.SQ_DOCUMENTO_ELIMINADO
minvalue 1
maxvalue 9999999999999999999999999999
start with 21
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_EXPEDIENTE
prompt ===============================
prompt
create sequence ORADW4J.SQ_EXPEDIENTE
minvalue 1
maxvalue 9999999999999999999999999999
start with 101
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_FOLIATURA
prompt ==============================
prompt
create sequence ORADW4J.SQ_FOLIATURA
minvalue 1
maxvalue 9999999999999999999999999999
start with 361
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_INCREMENTO
prompt ===============================
prompt
create sequence ORADW4J.SQ_INCREMENTO
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_INDICES
prompt ============================
prompt
create sequence ORADW4J.SQ_INDICES
minvalue 1
maxvalue 9999999999999999999999999999
start with 41
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_INFODOCUMENTO
prompt ==================================
prompt
create sequence ORADW4J.SQ_INFODOCUMENTO
minvalue 1
maxvalue 9999999999999999999999999999
start with 101
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_LIBRERIA
prompt =============================
prompt
create sequence ORADW4J.SQ_LIBRERIA
minvalue 1
maxvalue 9999999999999999999999999999
start with 41
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_PERFIL
prompt ===========================
prompt
create sequence ORADW4J.SQ_PERFIL
minvalue 1
maxvalue 9999999999999999999999999999
start with 83
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_SUBCATEGRORIA
prompt ==================================
prompt
create sequence ORADW4J.SQ_SUBCATEGRORIA
minvalue 1
maxvalue 9999999999999999999999999999
start with 41
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_TIPO_DOCUMENTO
prompt ===================================
prompt
create sequence ORADW4J.SQ_TIPO_DOCUMENTO
minvalue 1
maxvalue 9999999999999999999999999999
start with 81
increment by 1
cache 20;

prompt
prompt Creating sequence SQ_VALOR_DATO_ADICIONAL
prompt =========================================
prompt
create sequence ORADW4J.SQ_VALOR_DATO_ADICIONAL
minvalue 1
maxvalue 9999999999999999999999999999
start with 121
increment by 1
cache 20;

prompt
prompt Creating package PKG_ADMINISTRACION_BUSQUEDA
prompt ============================================
prompt
create or replace package oradw4j.pkg_administracion_busqueda is

  -- Author  : DEVELCOM
  -- Created : 26/02/2014 02:16:26 p.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
    funcion que busca la categorias segun el parametro dado
  **/
  function f_buscar_categoria(p_idcategoria in integer, p_idlibreria in integer, p_categoria in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
    funcion que busca las subCategoria segun el parametro dado
  **/
  function f_buscar_subcategoria(p_idcategoria in integer, p_idsubcategoria in integer, p_subcategoria in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
    funcion que busca los tipos de documentos segun el parametro dado
  **/
  function f_buscar_tipo_documento(p_idcategoria in integer, p_idsubcategoria in integer, p_tipodocumemto in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
    funcion que busca todos los argumetos del id de la
    categoria dada
  **/
  function f_buscar_indices(p_idcategoria in integer, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca todas las librerias con su categoria
     para la configuracion del usuarios
  **/
  function f_buscar_libreriascategorias(p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca el actual perfil del usuario
     para su reconfiguracion
  **/
  function f_buscar_perfil(p_idusuario in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca solo los estatus
     disponibles
  **/
  function f_buscar_estatus(p_idestatus in integer, p_estatus in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca solo librerias
  **/
  function f_buscar_libreria(p_idlibreria in integer, p_libreria in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca los datos de un
     combo en especifico
  **/
  function f_buscar_datos_combo(p_idcodigoindice in integer, p_mensaje out varchar2) return ref_cursor;

  /**
     permite comprobar si el tipo de documento
     es para la foto del expediente
  **/
  function f_comprobar_foto_ficha(p_idtipodocumento in integer, p_mensaje out varchar2) return ref_cursor;

  /**
     busca los indices de los datos adicionales
     del tipo de documento
  **/
  function f_buscar_indice_datosadicional(p_idtipodocumento in integer,
                                          p_mensaje out varchar2) return ref_cursor;

  /**
     busca los valores de los datos adicionales
     segun el codigo del indice
  **/
  function f_buscar_valor_datoadicional(p_idindicedato in integer, p_mensaje out varchar2) return ref_cursor;
  
  
  /**
     busca el usuario en la tabla de fabrica
  **/
  function f_buscar_usuario_fabrica(p_usuario in varchar2, p_mensaje out varchar2) return ref_cursor;
end pkg_administracion_busqueda;
/

prompt
prompt Creating package PKG_ADMINISTRACION_MODIFICA
prompt ============================================
prompt
create or replace package oradw4j.pkg_administracion_modifica is

  -- Author  : DEVELCOM
  -- Created : 10/03/2014 11:50:47 a.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
     procedimiento para modificar el
     estatus del usuario
  **/
  procedure p_modificar_usuario(p_idusuario in varchar2, p_idestatus in integer, p_mensaje out varchar2);

  /**
     procedimiento que elimina el actual
     perfil del usuario
  **/
  procedure p_eliminar_perfil(p_idusuario in varchar2, p_mensaje out varchar2);

  /**
     procediemiento que modifica el
     estatus de la libreria
  **/
  procedure p_actualizar_librerias(p_idlibreria in integer, p_idestatus in integer, p_mensaje out varchar2);

  /**
     procedimiento que modifica el
     estatus de la categoria
  **/
  procedure p_actualizar_categorias(p_idcategoria in integer, p_idestatus in integer, p_mensaje out varchar2);

  /**
     procedimiento que modifica el
     estatus de la subcategoria
  **/
  procedure p_actualizar_subcategorias(p_idsubcategoria in integer, p_idestatus in integer, p_mensaje out varchar2);

  /**
     procedimiento que modifica el estatus,
     si necesita o no la fecha de vencimiento
     y si lleva dato adicional el tipo de
     documento
  **/
  procedure p_actualizar_tipodocumento(p_idtipodocumento in integer, p_idestatus in integer, p_vencimiento in char, p_datoadicional in char, p_mensaje out varchar2);

  /**
     procedimiento que modifica los datos
     de un combo
  **/
  procedure p_actualizar_datos_combo(p_idcombo in integer, p_dato in varchar2, p_mensaje out varchar2);

  /**
     procedimiento para indocar el tipo de
     documento es para la foto del expediente
  **/
  procedure p_actualiza_td_foto(p_idtipodocumento in integer, p_mensaje out varchar2);

  /**
     procedimiento que actualiza los indices
     de los expedientes
  **/
  procedure p_actualizar_indices(p_idindices in integer, p_idcategoria in integer,
                                 p_indice in varchar2, p_tipodato in varchar2,
                                 p_codigo in integer, p_clave in char,
                                 p_mensaje out varchar2);

end pkg_administracion_modifica;
/

prompt
prompt Creating package PKG_ADMISTRACION_AGREGA
prompt ========================================
prompt
create or replace package oradw4j.pkg_admistracion_agrega is

  -- Author  : DEVELCOM
  -- Created : 10/03/2014 11:07:08 a.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
     procedimiento para agregar un nuevo
     usuario al gestor documental
  **/
  procedure p_agrega_usuario(p_idusuario in varchar2, p_nombre in varchar2,
                             p_apellido in varchar2, p_cedula in varchar2,
                             p_sexo in char, p_mensaje out varchar2);


  /**
     procedimiento que agrega perfil del nuevo
     del usuario
  **/
  procedure p_agregar_perfil(p_idlibreria in integer, p_idcategoria in integer, p_idusuario in varchar2, p_idrol in integer, p_mensaje out varchar2);


  /**
     procediemiento que agrega un usuario para
     identificarlo si pertenece a la fabrica de
     digitalizacion o no
  **/
  procedure p_agregar_fabrica(p_idusuario in varchar2, p_pertenece in char, p_mensaje out varchar2);

  /**
     procedimiento que agrega nuevas librerias
  **/
  procedure p_agregar_libreria(p_libreria in varchar2, p_idestatus in integer, p_mensaje out varchar2);

  /**
     procedimiento que agrega nuevas categorias
     asociado a el id de la libreria
  **/
  procedure p_agregar_categoria(p_idlibreria in integer, p_categoria in varchar2, p_idestatus in integer, p_mensaje out varchar2);

  /**
     procedimiento que agrega nuevas subcategorias
     asociado a el id de la categoria
  **/
  procedure p_agregar_subcategoria(p_idcategoria in integer, p_subcategoria in varchar2, p_idestatus in integer, p_mensaje out varchar2);

  /**
     procedimiento que agregar nuevos tipos de documentos
  **/
  procedure p_agregar_tipodocumento(p_idcategoria in integer, p_idsubcategoria in integer, p_tipodocumento in varchar2, p_idestatus in integer, p_vencimiento char, p_datoadicional in char, p_mensaje out varchar2);

  /**
     procedimiento que agrega los argumento
     para el expediente
  **/
  procedure p_agregar_indices(p_idcategoria in integer, p_indice in varchar2,
                              p_tipodato in varchar2, p_codigo in integer,
                              p_clave in char, p_mensaje out varchar2);


  /**
     procedimiento que agrega los datos
     de un combo en especifico
  **/
  procedure p_agregar_datos_combo(p_codigoindice in integer, p_dato in varchar2, p_mensaje out varchar2);


  /**
     procedimiento que agrega valores a los combos
     de los datos adicionales
  **/
  procedure p_agregar_datos_combo_da(p_iddatoadiciona in integer, p_datocombo in varchar2, p_mensaje out varchar2);

  /**
     procedimiento para agregar los indices
     de datos adicionales de un tipo de
     documento
  **/
  procedure p_guardar_indice_datoadicional(p_idtipodocumento in integer,
                                           p_datoadicional in varchar2,
                                           p_tipo in varchar2,
                                           p_mensaje out varchar2);
  /**
     guarda los valor de los datos adicionales
  **/
  procedure p_guarda_valor_dato_adicional(p_iddatoadicional in integer, p_idvalor in integer,
                                          p_valor in varchar2, p_numerodocumento in integer,
                                          p_version in integer, p_expediente in varchar2,
                                          p_flag in char, p_mensaje out varchar2);
  /**
     guarda los valor de los datos adicionales
  **/
  procedure p_actualiza_codigo_combo(p_codigocombo in integer, p_flag in char,
                                     p_mensaje out varchar2);

end pkg_admistracion_agrega;
/

prompt
prompt Creating package PKG_AUTENTICA
prompt ==============================
prompt
create or replace package oradw4j.pkg_autentica is

  -- Author  : DEVELCOM
  -- Created : 21/02/2014 10:05:37 a.m.
  -- Purpose : inicio de sesion

  type ref_cursor is ref cursor;

  /** funcion que comprueba el usuario en base de dato
      si la variable usuario es null devuelve todos los
      usuario en base de dato
  **/
  function f_verificar_usuario(p_usuario in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
    funcion que busca todos los usuarios para
    el autocompletar en el inicio de sesion
  **/
  function f_usuarios(p_mensaje out varchar2) return ref_cursor;

  /**
    funcion para crear la sesion del usuario segun su
    rol y perfil en el sistema
  **/
  function f_crear_sesion(p_usuario in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
    funcion que busca las librerias y categorias
    segun el perfil del usuario
  **/
  function f_buscar_lib_cat_perfil(p_usuario in varchar2, p_perfil in varchar2, p_mensaje out varchar2) return ref_cursor;

end pkg_autentica;
/

prompt
prompt Creating package PKG_CALIDAD_DOCUMENTO
prompt ======================================
prompt
create or replace package oradw4j.pkg_calidad_documento is

  -- Author  : DEVELCOM
  -- Created : 06/03/2014 10:43:47 a.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
     funcion que busca los usuarios que
     pertencen a la fabrica y los que no
     pertenecen a la misma
  **/
  function f_buscar_no_fabrica(p_fechadesde in date, p_fechahasta in date,
                            p_estatusdocumento integer, p_idcategoria in integer,
                            p_expedeinte in varchar2, p_mensaje out varchar2)
                            return ref_cursor;

  /**
     funcion que busca los usuarios que
     pertencen a la fabrica y los que no
     pertenecen a la misma
  **/
  function f_buscar_fabrica(p_usuario in varchar2, p_fechadesde in date,
                            p_fechahasta in date, p_estatusdocumento integer,
                            p_idcategoria in integer, p_expediente in varchar2,
                            p_mensaje out varchar2) return ref_cursor;

  /**
     procedimento que actualiza el estado
     de un tipo de documento de pendiente
     a aprobado
  **/
  procedure p_aprobar_documento(p_idinfodocumento in integer, p_usuario in varchar2, p_mensaje out varchar2);

  /**
     funcion que busca las causas de rechazos
  **/
  function f_buscar_causas_rechazo(p_mensaje out varchar2) return ref_cursor;

  /**
     procedimento que actualiza el estado
     de un tipo de documento de pendiente
     a rechazado
  **/
  procedure p_rechazar_documento(p_idinfodocumento in integer, p_usuario in varchar2, p_causa in varchar2, p_motivo in varchar2, p_mensaje out varchar2);



end pkg_calidad_documento;
/

prompt
prompt Creating package PKG_DOCUMENTO
prompt ==============================
prompt
create or replace package oradw4j.pkg_documento is

  -- Author  : DEVELCOM
  -- Created : 05/03/2014 07:54:35 a.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
     funcion que busca todos los datos de in tipo de
     documento
  **/
  function f_buscar_infodocumento(p_idexpediente in varchar2, p_ids_documento in integer,
                                  p_estatusdoc in integer, p_redigitalizo in char,
                                  p_estatusaprobado in integer,
                                  p_mensaje out varchar2) return ref_cursor;

  /**
     procedimiento que busca el puntero del
     archivo fisico para ser eliminado
  **/
  function f_eliminar_archivo(p_idinfodocumento in integer, p_mensaje out varchar2) return ref_cursor;

  /**
     procedimiento que gestiona el agregar los datos
     de un tipo de documento, que puede ser insert o
     update
  **/
  procedure p_agregar_registro_archivo(p_accion in varchar2, p_nombrearchivo in varchar2,
                                       p_iddocumento in integer, p_rutaarchivo in varchar2,
                                       p_cantpaginas in integer, p_version in integer,
                                       p_idexpediente in varchar2, p_numerodoc in integer,
                                       p_fechavencimiento in date, p_datoadicional in varchar2,
                                       p_usuario in varchar2, p_idinfodocumento in integer,
                                       p_estatus in integer, p_redigitalizo in char,
                                       p_formato in varchar2, p_idinfodoc out integer,
                                       p_mensaje out varchar2);

  /**
     procedimiento para actulizar el nombre del
     archivo despues de codificarlo
  **/
  procedure p_actualiza_nombre_archivo(p_nombrearchivo in varchar2, p_idinfodocumento in integer, p_mensaje out varchar2);

  /**
     funcion que busca el ultimo numero del tipo
     de documento
  **/
  function f_buscar_ultimo_numero(p_iddocumento in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return integer;

  /**
     funcion que verifica si el consecutivo del
     numero del documento ya esta registrado
  **/
  function f_comprobar_numero_documento(p_iddocumento in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que comprueba si el nombre del archiv
     esta registrado en la base de datos
  **/
  function f_comprobar_nombre_archivo(p_idinfodocumento in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return varchar2;

  /**
     funcion que busca los datos necesario para luego
     buscar el fisico del tipo de documento
  **/
  function f_buscar_fisico_documento(p_iddocumento in integer, p_numerodoc in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca la ultima version
     del documento
  **/
  function f_buscar_ultima_version(p_iddocumento in integer, p_idexpediente in varchar2, p_numerodocumento in integer, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca la informacion de
     cada dato adicional segun el tipo
     y numero de documento
  **/
  function f_buscar_valor_dato_adicional(p_idocumento in integer, p_idexpediente in varchar2, p_numerodoc in integer, p_version in integer, p_mensaje out varchar2) return ref_cursor;


  /**
     procedimiento para elimiar un registro
     de la tabla infodocumento al resturar
     un error en el guardado de un documento
  **/
  procedure p_eliminar_registro_archivo(p_idinfodocumento in integer, p_mensaje out varchar2);
end pkg_documento;
/

prompt
prompt Creating package PKG_ELIMINA
prompt ============================
prompt
create or replace package oradw4j.pkg_elimina is

  -- Author  : DEVELCOM
  -- Created : 07/05/2014 12:59:40 p.m.
  -- Purpose :

  -- Public type declarations
  type ref_cursor is ref cursor;

  /**
     funcion que busca la informacion necesaria
     del documento a eliminar
  **/
  function f_buscar_infodocumento(p_idinfodocumento in integer, p_iddocumento in integer,
                                  p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor;
  /**
     procedimineto para eliminar el registro
     de la informacion del tipo de documento
  **/
  procedure p_eliminar_infodocumento(p_idinfodocumento in integer, p_mensaje out varchar2);

  /**
     procedimiento que actualiza la numeracion
     del tipo de documento
  **/
  procedure p_actualizar_infodocumento(p_numerodoc in integer, p_idinfodocumento in integer,
                                       p_idexpediente in varchar2, p_mensaje out varchar2);

  /**
     procedimiento que registra la traza del
     documento que se elimino
  **/
  procedure p_traza_elimina_documento(p_idexpedeinte in varchar2, p_idlibreria in integer,
                                      p_idcategoria in integer, p_idsubcategoria in integer,
                                      p_iddocumento in integer, p_numerodoc in integer,
                                      p_fechavencimiento in date, p_cantpaginas in integer,
                                      p_versiondoc in integer, p_datoadicional in varchar2,
                                      p_usuarioelimino in varchar2, p_usuariodigitalizo in varchar2,
                                      p_usuariorechazo in varchar2, p_causaelimino in varchar2,
                                      p_motivoelimino in varchar2, p_causarechazo in varchar2,
                                      p_motivorechazo in varchar2, p_mensaje out varchar2);


  /**
     funcion que busca la informacion necesaria
     para eliminar tipos de documentos
  **/
  function f_buscar_informaciondoc(p_idsdocumento in integer, p_idexpediente in varchar2,
                                   p_mensaje out varchar2) return ref_cursor;


  /**
     funcion que busca todos los datos
     de un tipo de documento a eliminar
  **/
  function f_buscar_datosdoc(p_idinfodocumento in integer, p_versiondoc in integer,
                             p_idexpediente in varchar2, p_numerodoc in integer,
                             p_mensaje out varchar2) return ref_cursor;

end pkg_elimina;
/

prompt
prompt Creating package PKG_EXPEDIENTE
prompt ===============================
prompt
create or replace package oradw4j.pkg_expediente is

  -- Author  : DEVELCOM
  -- Created : 26/02/2014 11:39:01 a.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
    funcion para buscar y comprobar que exite el expediente
    en base de dato
  **/
  function f_buscar_expediente(p_expediente in varchar2, p_idlibreria in integer, p_idcategoria in integer, p_flag in char, p_mensaje out varchar2) return ref_cursor;

  /**
    funcion que busca los dato de una tabla, o sea,
    nombre de columna y su tipo de dato
  **/
  function f_informacion_tabla(p_tabla in varchar2, p_mensaje out varchar2) return ref_cursor;

  /**
    procedimiento que guarda un nuevo expediente
  **/
  procedure p_guardar_expediente(p_idexpediente in varchar2, p_idindice in integer,
                                 p_valor in varchar2, p_idlibreria in integer,
                                 p_idcategoria in integer, p_mensaje out varchar2);

  /**
    procedimiento para eliminar el expediente
  **/
  procedure p_eliminar_expediente(p_idexpediente in varchar2, p_mensaje out varchar2);

  /**
     procedimeinto que actualiza los
     indices de un expediente
  **/
  procedure p_actualizar_indices(p_idexpedientenuevo in varchar2,
                                 p_idexpedienteviejo in varchar2, p_idindice in integer,
                                 p_valor in varchar2, p_idlibreria in integer,
                                 p_idcategoria in integer, p_mensaje out varchar2);

  /**
     procedimeinto que actualiza los
     indices de un expediente
  **/
  function f_modificar_indices(p_idexpedientenuevo in varchar2,
                                 p_idexpedienteviejo in varchar2, 
                                 p_flag in varchar2,  p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca los tipos de documentos
     segun los identificadores de las subcategorias
  **/
  function f_buscar_tipo_documento(p_idssucategorias in integer, p_mensaje out varchar2) return ref_cursor;

  /**
     funcion que busca la informacion
     para la ficha del expediente
  **/
  function f_buscar_foto_ficha(p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor;

end pkg_expediente;
/

prompt
prompt Creating package PKG_FOLIATURA
prompt ==============================
prompt
create or replace package oradw4j.pkg_foliatura is

  -- Author  : DEVELCOM
  -- Created : 08/04/2014 01:41:37 p.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
     funcion que busca los tipos de documento de un
     expediente especifico
  **/
  function f_foliatura_buscar_expediente(p_idexpediente in varchar2, p_idlibreria in integer, p_idcategoria in integer, p_mensaje out varchar2) return ref_cursor;

  /**
    procedimiento para agregar el orden de la
    foliatura del expediente
  **/
  procedure p_agregar_foliaturas(p_idinfodocumento in integer, p_iddocumento in integer, p_idexpediente in varchar2, p_pagina in integer, p_mensaje out varchar2);

end pkg_foliatura;
/

prompt
prompt Creating package PKG_MANTENIMIENTO
prompt ==================================
prompt
create or replace package oradw4j.pkg_mantenimiento is

  -- Author  : DEVELCOM
  -- Created : 07/03/2014 03:20:03 p.m.
  -- Purpose :

  type ref_cursor is ref cursor;

  /**
     funcion que busca la configuracion
     del gestor documental
  **/
  function f_buscar_configuracion(p_mensaje out varchar2) return ref_cursor;

  /**
     procedimiento que actualiza la
     nueva configuracion
  **/
  procedure p_agregar_configuracion(p_nombreservidor in varchar2, p_nombrebasedato in varchar,
                                    p_puertobasedato in integer, p_usuariobasedato in varchar2,
                                    p_passbasedato in varchar2, p_calidad in char,
                                    p_foliatura in char, p_ficha in char,
                                    p_fabrica in char, p_elimina in char,
                                    p_mensaje out varchar2);

end pkg_mantenimiento;
/

prompt
prompt Creating package body PKG_ADMINISTRACION_BUSQUEDA
prompt =================================================
prompt
create or replace package body oradw4j.pkg_administracion_busqueda is

  /**
    funcion que busca la categorias segun el parametro dado
  **/
  function f_buscar_categoria(p_idcategoria in integer, p_idlibreria in integer, p_categoria in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_cat ref_cursor;
  begin

    p_mensaje := 'EGD00';

    if p_idCategoria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_categoria=p_idcategoria
          order by c.categoria;

    elsif p_idLibreria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_libreria=p_idlibreria
          order by c.categoria;

    elsif p_categoria is not null then
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.categoria=p_categoria
          order by c.categoria;
    else
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
           order by c.categoria;
    end if;
    return cursor_cat;
    close cursor_cat;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_cat;
  end f_buscar_categoria;


  /**
    funcion que busca las subCategoria segun el parametro dado
  **/
  function f_buscar_subcategoria(p_idcategoria in integer, p_idsubcategoria in integer, p_subcategoria in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_subcat ref_cursor;
  begin

    p_mensaje := 'EGD00';

    if p_idCategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_categoria=p_idcategoria
          order by s.subcategoria;

    elsif p_idSubCategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_subcategoria=p_idsubcategoria
          order by s.subcategoria;

    elsif p_subCategoria is not null then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.subcategoria=p_subcategoria
          order by s.subcategoria;

    else

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
             order by s.subcategoria;
    end if;
    return cursor_subcat;
    close cursor_subcat;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_subcat;
  end f_buscar_subcategoria;


  /**
    funcion que busca los tipos de documentos segun el parametro dado
  **/
  function f_buscar_tipo_documento(p_idcategoria in integer, p_idsubcategoria in integer, p_tipodocumemto in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_tipodoc ref_cursor;
  begin

    p_mensaje := 'EGD00';

    if (p_idCategoria > 0) and (p_idSubCategoria > 0) then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria and t.id_subcategoria=p_idsubcategoria
         order by t.tipo_documento;

    elsif p_idCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria order by t.tipo_documento;

    elsif p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_subcategoria=p_idsubcategoria order by t.tipo_documento;

    elsif p_tipodocumemto is not null then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.tipo_documento=p_tipodocumemto order by t.tipo_documento;

    else

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
            order by t.tipo_documento;

    end if;
    return cursor_tipodoc;
    close cursor_tipodoc;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_tipodoc;

  end f_buscar_tipo_documento;

  /**
    funcion que busca todos los argumetos del id de la
    categoria dada
  **/
  function f_buscar_indices(p_idcategoria in integer, p_mensaje out varchar2) return ref_cursor is
    cursor_arg ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_arg for
       select * from indices i where id_categoria=p_idcategoria order by i.id_indice;
    return cursor_arg;
    close cursor_arg;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_arg;
  end f_buscar_indices;

  /**
     funcion que busca todas las librerias con su categoria
     para la configuracion del usuarios
  **/
  function f_buscar_libreriascategorias(p_mensaje out varchar2) return ref_cursor is
    cursor_libcat ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_libcat for
       select l.id_libreria, l.libreria descLibreria, c.id_categoria, c.categoria descCategoria,
              r.id_rol, r.rol descRol, e.id_estatus, e.estatus desEstatus
          from libreria l inner join categoria c on l.id_libreria=c.id_libreria
                          inner join estatus e
                                 on (l.id_estatus=e.id_estatus and c.id_estatus=e.id_estatus),
                          rol r
          where e.id_estatus=1
          order by l.libreria, c.categoria;

    return cursor_libcat;
    close cursor_libcat;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_libcat;
  end f_buscar_libreriascategorias;

  /**
     funcion que busca el actual perfil del usuario
     para su reconfiguracion
  **/
  function f_buscar_perfil(p_idusuario in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_perfil ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_perfil for
        select distinct u.nombre, u.apellido, u.id_usuario, e.estatus estatus_usuario,
               f.fabrica pertenece, r.rol,
               l.id_libreria, l.libreria,
               c.id_categoria, c.categoria
            from usuario u full join perfil p on u.id_usuario=p.id_usuario
                           full join estatus e on u.id_estatus=e.id_estatus
                           full join fabrica f on u.id_usuario=f.usuario
                           full join rol r on p.id_rol=r.id_rol
                           full join libreria l on p.id_libreria=l.id_libreria
                           full join categoria c on p.id_categoria=c.id_categoria
             where u.id_usuario=p_idusuario
             order by l.libreria, c.categoria;


    return cursor_perfil;
    close cursor_perfil;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_perfil;
  end f_buscar_perfil;

  /**
     funcion que busca solo los estatus
     disponibles
  **/
  function f_buscar_estatus(p_idestatus in integer, p_estatus in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_est ref_cursor;
  begin

    p_mensaje := 'EGD00';

    if p_idestatus > 0 then
      open cursor_est for
         select id_estatus, estatus from estatus where id_estatus=p_idestatus;
    elsif p_estatus is not null then
      open cursor_est for
         select id_estatus, estatus from estatus where estatus=p_estatus;
    end if;

    return cursor_est;
    close cursor_est;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_est;
  end f_buscar_estatus;

  /**
     funcion que busca solo librerias
  **/
  function f_buscar_libreria(p_idlibreria in integer, p_libreria in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_lib ref_cursor;
  begin

    p_mensaje := 'EGD00';

    if p_idlibreria > 0 then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.id_libreria=p_idlibreria
             order by l.libreria asc;

    elsif p_libreria is not null then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.libreria=p_libreria
             order by l.libreria asc;

    else

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             order by l.libreria asc;

    end if;
    return cursor_lib;
    close cursor_lib;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_lib;
  end f_buscar_libreria;

  /**
     funcion que busca los datos de un
     combo en especifico
  **/
  function f_buscar_datos_combo(p_idcodigoindice in integer, p_mensaje out varchar2) return ref_cursor is
    cursor_cbo ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice
             from lista_desplegables l inner join indices a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idcodigoindice
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_cbo;
  end f_buscar_datos_combo;

  /**
     permite comprobar si el tipo de documento
     es para la foto del expediente
  **/
  function f_comprobar_foto_ficha(p_idtipodocumento in integer, p_mensaje out varchar2) return ref_cursor is
    cursor_foto ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_foto for
       select ficha from tipodocumento where id_documento=p_idtipodocumento;

    return cursor_foto;
    close cursor_foto;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_foto;
  end f_comprobar_foto_ficha;


  /**
     busca los indices de los datos adicionales
     del tipo de documento
  **/
  function f_buscar_indice_datosadicional(p_idtipodocumento in integer,
                                          p_mensaje out varchar2) return ref_cursor is
    cursor_dato ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_dato for
       select *
         from dato_adicional t
         where t.id_documento = p_idtipodocumento
         order by t.id_dato_adicional;

    return cursor_dato;
    close cursor_dato;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_dato;

  end f_buscar_indice_datosadicional;

  /**
     busca los valores de los datos adicionales
     segun el codigo del indice
  **/
  function f_buscar_valor_datoadicional(p_idindicedato in integer, p_mensaje out varchar2) return ref_cursor is
    cursor_cbo ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice_adicional
             from lista_desplegables l inner join dato_adicional a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idindicedato
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_cbo;
  end f_buscar_valor_datoadicional;
  
  /**
     busca el usuario en la tabla de fabrica
  **/
  function f_buscar_usuario_fabrica(p_usuario in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_fab ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_fab for
       select usuario, fabrica from fabrica f where f.usuario=p_usuario;


    return cursor_fab;
    close cursor_fab;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_fab;
    
  end f_buscar_usuario_fabrica;

end pkg_administracion_busqueda;
/

prompt
prompt Creating package body PKG_ADMINISTRACION_MODIFICA
prompt =================================================
prompt
create or replace package body oradw4j.pkg_administracion_modifica is

  /**
     procedimiento para modificar el
     estatus del usuario
  **/
  procedure p_modificar_usuario(p_idusuario in varchar2, p_idestatus in integer, p_mensaje out varchar2)is

  begin

   p_mensaje := 'EGD00';

   update usuario set id_estatus=p_idestatus where id_usuario=p_idusuario;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_modificar_usuario;

  /**
     procedimiento que elimina el actual
     perfil del usuario
  **/
  procedure p_eliminar_perfil(p_idusuario in varchar2, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   delete from perfil where id_usuario=p_idusuario;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_eliminar_perfil;

  /**
     procediemiento que modifica el
     estatus de la libreria
  **/
  procedure p_actualizar_librerias(p_idlibreria in integer, p_idestatus in integer, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   update libreria set id_estatus=p_idestatus where id_libreria=p_idlibreria;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_librerias;


  /**
     procedimiento que modifica el
     estatus de la categoria
  **/
  procedure p_actualizar_categorias(p_idcategoria in integer, p_idestatus in integer, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   update categoria set id_estatus=p_idestatus where id_categoria=p_idcategoria;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_categorias;

  /**
     procedimiento que modifica el
     estatus de la subcategoria
  **/
  procedure p_actualizar_subcategorias(p_idsubcategoria in integer, p_idestatus in integer, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   update subcategoria set id_estatus=p_idestatus where id_subcategoria=p_idsubcategoria;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_subcategorias;


  /**
     procedimiento que modifica el estatus,
     si necesita o no la fecha de vencimiento
     y si lleva dato adicional el tipo de
     documento
  **/
  procedure p_actualizar_tipodocumento(p_idtipodocumento in integer, p_idestatus in integer, p_vencimiento in char, p_datoadicional in char, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   update tipodocumento set id_estatus=p_idestatus, vencimiento=p_vencimiento,
                        dato_adicional=p_datoadicional
   where id_documento=p_idtipodocumento;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_tipodocumento;

  /**
     procedimiento que modifica los datos
     de un combo
  **/
  procedure p_actualizar_datos_combo(p_idcombo in integer, p_dato in varchar2, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   update lista_desplegables
      set descripcion = p_dato
    where id_lista = p_idcombo;


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_datos_combo;

  /**
     procedimiento para indocar el tipo de
     documento es para la foto del expediente
  **/
  procedure p_actualiza_td_foto(p_idtipodocumento in integer, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   update tipodocumento set ficha='1' where id_documento=p_idtipodocumento;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualiza_td_foto;

  /**
     procedimiento que actualiza los indices
     de los expedientes
  **/
  procedure p_actualizar_indices(p_idindices in integer, p_idcategoria in integer,
                                 p_indice in varchar2, p_tipodato in varchar2,
                                 p_codigo in integer, p_clave in char,
                                 p_mensaje out varchar2) is

  begin
    update indices
       set id_categoria = p_idcategoria,
           indice = p_indice,
           tipo = p_tipodato,
           codigo = p_codigo,
           clave = p_clave
     where id_indice = p_idindices;


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_indices;

end pkg_administracion_modifica;
/

prompt
prompt Creating package body PKG_ADMISTRACION_AGREGA
prompt =============================================
prompt
create or replace package body oradw4j.pkg_admistracion_agrega is

  /**
     procedimiento para agregar un nuevo
     usuario al gestor documental
  **/
  procedure p_agrega_usuario(p_idusuario in varchar2, p_nombre in varchar2,
                             p_apellido in varchar2, p_cedula in varchar2,
                             p_sexo in char, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into usuario
      (id_usuario, nombre, apellido, cedula, sexo, id_estatus)
    values
      (p_idusuario, p_nombre, p_apellido, p_cedula, p_sexo, 1);

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agrega_usuario;

  /**
     procedimiento que agrega perfil del nuevo
     del usuario
  **/
  procedure p_agregar_perfil(p_idlibreria in integer, p_idcategoria in integer, p_idusuario in varchar2, p_idrol in integer, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    if p_idcategoria = 0 and p_idlibreria = 0 then

      insert into perfil(id_perfil, id_usuario, id_rol)
                  values(sq_perfil.nextval, p_idusuario, p_idrol);

    elsif p_idcategoria = 0 then

      insert into perfil(id_perfil, id_libreria, id_usuario, id_rol)
                  values(sq_perfil.nextval, p_idlibreria, p_idusuario, p_idrol);
    else

      insert into perfil(id_perfil, id_libreria, id_categoria, id_usuario, id_rol)
                  values(sq_perfil.nextval, p_idlibreria, p_idcategoria, p_idusuario, p_idrol);
    end if;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_perfil;

  /**
     procediemiento que agrega un usuario para
     identificarlo si pertenece a la fabrica de
     digitalizacion o no
  **/
  procedure p_agregar_fabrica(p_idusuario in varchar2, p_pertenece in char, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into fabrica(usuario, fabrica)
           values(p_idusuario, p_pertenece);


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_fabrica;

  /**
     procedimiento que agrega nuevas librerias
  **/
  procedure p_agregar_libreria(p_libreria in varchar2, p_idestatus in integer, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into libreria(id_libreria, libreria, id_estatus)
                values(sq_libreria.nextval, p_libreria, p_idestatus);

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_libreria;

  /**
     procedimiento que agrega nuevas categorias
     asociado a el id de la libreria
  **/
  procedure p_agregar_categoria(p_idlibreria in integer, p_categoria in varchar2, p_idestatus in integer, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into categoria
      (id_categoria, id_libreria, categoria, id_estatus)
    values
      (sq_categroria.nextval, p_idlibreria, p_categoria, p_idestatus);


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_categoria;


  /**
     procedimiento que agrega nuevas subcategorias
     asociado a el id de la categoria
  **/
  procedure p_agregar_subcategoria(p_idcategoria in integer, p_subcategoria in varchar2, p_idestatus in integer, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into subcategoria
      (id_subcategoria, id_categoria, subcategoria, id_estatus)
    values
      (sq_subcategroria.nextval, p_idcategoria, p_subcategoria, p_idestatus);


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_subcategoria;


  /**
     procedimiento que agregar nuevos tipos de documentos
     asociado a el id de la categoria y subcategoria
  **/
  procedure p_agregar_tipodocumento(p_idcategoria in integer, p_idsubcategoria in integer, p_tipodocumento in varchar2, p_idestatus in integer, p_vencimiento char, p_datoadicional in char, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into tipodocumento
      (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional)
    values
      (sq_tipo_documento.nextval, p_idcategoria, p_idsubcategoria, p_tipodocumento, p_idestatus, p_vencimiento, p_datoadicional);


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_tipodocumento;

  /**
     procedimiento que agrega los argumento
     para el expediente
  **/
  procedure p_agregar_indices(p_idcategoria in integer, p_indice in varchar2,
                              p_tipodato in varchar2, p_codigo in integer,
                              p_clave in char, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into indices
      (id_indice, id_categoria, indice, tipo, codigo, clave)
    values
      (sq_indices.nextval, p_idcategoria, p_indice, p_tipodato, p_codigo, p_clave);


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_indices;


  /**
     procedimiento que agrega los datos
     de un combo en especifico
  **/
  procedure p_agregar_datos_combo(p_codigoindice in integer, p_dato in varchar2, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into lista_desplegables
      (id_lista, codigo_indice, descripcion)
    values
      (sq_combo.nextval, p_codigoindice, p_dato);


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_datos_combo;

    /**
     procedimiento que agrega valores a los combos
     de los datos adicionales
  **/
  procedure p_agregar_datos_combo_da(p_iddatoadiciona in integer, p_datocombo in varchar2, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';


    insert into lista_desplegables
      (id_lista, codigo_indice, descripcion)
    values
      (sq_combo.nextval, p_iddatoadiciona, p_datocombo);

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_datos_combo_da;


    /**
     procedimiento para agregar los indices
     de datos adicionales de un tipo de
     documento
  **/
  procedure p_guardar_indice_datoadicional(p_idtipodocumento in integer,
                                           p_datoadicional in varchar2,
                                           p_tipo in varchar2,
                                           p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into dato_adicional
      (id_dato_adicional, indice_adicional, tipo, id_documento)
    values
      (sq_dato_adicional.nextval, p_datoadicional, p_tipo, p_idtipodocumento);

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_guardar_indice_datoadicional;

  /**
     guarda los valor de los datos adicionales
  **/
  procedure p_guarda_valor_dato_adicional(p_iddatoadicional in integer, p_idvalor in integer,
                                          p_valor in varchar2, p_numerodocumento in integer,
                                          p_version in integer, p_expediente in varchar2,
                                          p_flag in char, p_mensaje out varchar2) is
  begin

    p_mensaje := 'EGD00';


    if p_flag = 0 then

      update valor_dato_adicional
         set valor = p_valor
       where id_valor = p_idvalor
         and numero = p_numerodocumento
         and version = p_version
         and expediente = p_expediente;

    elsif p_flag = 1 then

     insert into valor_dato_adicional
        (id_valor, id_dato_adicional, valor, numero, version, expediente)
      values
        (sq_valor_dato_adicional.nextval, p_iddatoadicional, p_valor, p_numerodocumento,
        p_version, p_expediente);

    end if;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_guarda_valor_dato_adicional;


  /**
     guarda los valor de los datos adicionales
  **/
  procedure p_actualiza_codigo_combo(p_codigocombo in integer, p_flag in char,
                                     p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    if p_flag = '0' then

      update indices set codigo = p_codigocombo where id_indice = p_codigocombo;

    elsif p_flag = '1' then

      update dato_adicional  set codigo = p_codigocombo where id_dato_adicional = p_codigocombo;

    end if;

  end p_actualiza_codigo_combo;

end pkg_admistracion_agrega;
/

prompt
prompt Creating package body PKG_AUTENTICA
prompt ===================================
prompt
create or replace package body oradw4j.pkg_autentica is

  -- Author  : DEVELCOM
  -- Created : 21/02/2014 10:05:37 a.m.
  -- Purpose : inicio de sesion

  /** funcion que comprueba el usuario en base de dato
      si la variable usuario es null devuelve todos los
      usuario en base de dato
  **/
  function f_verificar_usuario(p_usuario in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_user ref_cursor;
  begin

   p_mensaje := 'EGD00';

   --dbms_output.put_line(p_usuario);

   if p_usuario is not null then

    open cursor_user for
      select u.id_usuario, u.nombre, u.apellido, u.password passUser, e.id_estatus, e.estatus, c.*
      from usuario u inner join estatus e on u.id_estatus=e.id_estatus,
      configuracion c
      where u.id_usuario = p_usuario;

   else

     open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

   end if;
    return cursor_user;
    close cursor_user;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_user;
  end f_verificar_usuario;


  /**
    funcion que busca todos los usuarios para
    el autocompletar en el inicio de sesion
  **/
  function f_usuarios(p_mensaje out varchar2) return ref_cursor is
    cursor_user ref_cursor;
  begin

   p_mensaje := 'EGD00';

    open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.sexo,
              e.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

       return cursor_user;
       close cursor_user;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_user;
  end f_usuarios;

  /**
    funcion para crear la sesion del usuario segun su
    rol y perfil en el sistema
  **/
  function f_crear_sesion(p_usuario in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_user ref_cursor;
  begin
   p_mensaje := 'EGD00';
    open cursor_user for
       select distinct r.rol desc_rol, e.estatus estatus_usuario, u.id_usuario id_user,
              u.nombre, u.apellido, u.cedula, u.id_estatus
           from usuario u inner join estatus e on u.id_estatus=e.id_estatus
                inner join perfil p on p.id_usuario=u.id_usuario
                inner join rol r on r.id_rol=p.id_rol
           where u.id_usuario=p_usuario;
    return cursor_user;
    close cursor_user;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_user;
  end f_crear_sesion;

  /**
    funcion que busca las librerias y categorias
    segun el perfil del usuario
  **/
  function f_buscar_lib_cat_perfil(p_usuario in varchar2, p_perfil in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_user ref_cursor;
  begin
   p_mensaje := 'EGD00';
    open cursor_user for
        select p.id_usuario usuario, r.rol,
               p.id_libreria, l.libreria, el.estatus status_lib,
               p.id_categoria, c.categoria, ec.estatus status_cat
           from perfil p
                inner join rol r on p.id_rol=r.id_rol
                inner join libreria l on p.id_libreria=l.id_libreria
                inner join estatus el on l.id_estatus=el.id_estatus
                inner join categoria c on p.id_categoria=c.id_categoria
                inner join estatus ec on c.id_estatus=ec.id_estatus
           where p.id_usuario=p_usuario and r.rol=p_perfil
           order by l.libreria;
    return cursor_user;
    close cursor_user;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_user;
  end f_buscar_lib_cat_perfil;

end pkg_autentica;
/

prompt
prompt Creating package body PKG_CALIDAD_DOCUMENTO
prompt ===========================================
prompt
create or replace package body oradw4j.pkg_calidad_documento is



  /**
     funcion que busca los usuarios que
     pertencen a la fabrica y los que no
     pertenecen a la misma
  **/
  function f_buscar_no_fabrica(p_fechadesde in date, p_fechahasta in date,
                               p_estatusdocumento integer, p_idcategoria in integer,
                               p_expedeinte in varchar2, p_mensaje out varchar2)
                               return ref_cursor is
    cursor_user ref_cursor;

  begin

    p_mensaje := 'EGD00';

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion between to_date(p_fechadesde, 'dd/MM/yyyy')
           and to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion >= to_date(p_fechadesde, 'dd/MM/yyyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion <= to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_expedeinte is not null then

         open cursor_user for
            select i.*, e.valor, e.expediente
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
             where d.id_expediente = p_expedeinte
               and d.estatus_documento=p_estatusdocumento
               and i.id_categoria=p_idcategoria
               order by i.id_indice;
    else

      open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_user;
  end f_buscar_no_fabrica;

  /**
     funcion que busca los usuarios que
     pertencen a la fabrica y los que no
     pertenecen a la misma
  **/
  function f_buscar_fabrica(p_usuario in varchar2, p_fechadesde in date,
                            p_fechahasta in date, p_estatusdocumento integer,
                            p_idcategoria in integer, p_expediente in varchar2,
                            p_mensaje out varchar2) return ref_cursor is
    cursor_user ref_cursor;
    sfabrica char(1);
  begin

    p_mensaje := 'EGD00';

    select t.fabrica into sfabrica from fabrica t where t.usuario=p_usuario;

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion between to_date(p_fechadesde, 'dd/MM/yyyy')
           and to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion >= to_date(p_fechadesde, 'dd/MM/yyyy')
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion <= to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_expediente is not null then

         open cursor_user for
            select distinct e.expediente, e.valor, i.*, f.fabrica
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
               inner join fabrica f on f.usuario=di.usuario_digitalizo
               inner join usuario u on u.id_usuario=f.usuario
             where d.id_expediente = p_expediente
               and d.estatus_documento=p_estatusdocumento
               and f.fabrica=sfabrica
               and i.id_categoria=p_idcategoria
               order by e.expediente, i.id_indice;
    else

      open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_user;
  end f_buscar_fabrica;

  /**
     procedimento que actualiza el estado
     de un tipo de documento
  **/
  procedure p_aprobar_documento(p_idinfodocumento in integer, p_usuario in varchar2, p_mensaje out varchar2) is
    fecha_actual date;
    estatu integer;
    idInfoDoc integer;
  begin

    p_mensaje := 'EGD00';

    select sysdate into fecha_actual from dual;

    update infodocumento set estatus_documento=1 where id_infodocumento= p_idinfodocumento;
    commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;

    if estatu > 0 then

      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;

      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_aprobacion = fecha_actual,
               usuario_aprobacion = p_usuario
         where id_infodocumento = p_idinfodocumento;
      else
        insert into datos_infodocumento(id_datos, id_infodocumento, fecha_aprobacion, usuario_aprobacion)
                  values(sq_datos_infodocumento.nextval, p_idinfodocumento, fecha_actual, p_usuario);
      end if;
    else
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      commit;
      p_mensaje :='problema al cambiar el estatus del documento';
    end if;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_aprobar_documento;

  /**
     funcion que busca las causas de rechazos
  **/
  function f_buscar_causas_rechazo(p_mensaje out varchar2) return ref_cursor is
    cursor_causa ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_causa for
      select id_causa, causa from causa;

    return cursor_causa;
    close cursor_causa;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_causa;
  end f_buscar_causas_rechazo;


  /**
     procedimento que actualiza el estado
     de un tipo de documento de pendiente
     a rechazado
  **/
  procedure p_rechazar_documento(p_idinfodocumento in integer, p_usuario in varchar2, p_causa in varchar2, p_motivo in varchar2, p_mensaje out varchar2) is
    fecha_actual date;
    estatu integer;
    idInfoDoc integer;
  begin

    p_mensaje := 'EGD00';

    select sysdate into fecha_actual from dual;

    update infodocumento set estatus_documento=2 where id_infodocumento= p_idinfodocumento;
    commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;


    if estatu > 0 then

      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;

      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_rechazo = fecha_actual,
               usuario_rechazo = p_usuario,
               motivo_rechazo = p_motivo,
               causa_rechazo = p_causa
         where id_infodocumento = p_idinfodocumento;
      else

        insert into datos_infodocumento(id_datos, id_infodocumento, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo)
                  values(sq_datos_infodocumento.nextval, p_idinfodocumento, fecha_actual, p_usuario, p_motivo, p_causa);
      end if;
    else
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      commit;
      p_mensaje :='problema al cambiar el estatus del documento';

    end if;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_rechazar_documento;


end pkg_calidad_documento;
/

prompt
prompt Creating package body PKG_DOCUMENTO
prompt ===================================
prompt
create or replace package body oradw4j.pkg_documento is


  /**
     funcion que busca todos los datos de in tipo de
     documento
  **/
  function f_buscar_infodocumento(p_idexpediente in varchar2, p_ids_documento in integer,
                                  p_estatusdoc in integer, p_redigitalizo in char,
                                  p_estatusaprobado in integer,
                                  p_mensaje out varchar2) return ref_cursor is
    cursor_infodoc ref_cursor;
  begin

   p_mensaje := 'EGD00';

   if p_estatusaprobado = 1 then
      dbms_output.put_line(p_ids_documento);
      open cursor_infodoc for
        select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where i.id_expediente=p_idexpediente
                 and i.id_documento=p_ids_documento
                 --and i.id_documento in (p_ids_documento)
                 and (i.estatus_documento=p_estatusdoc or i.estatus_documento=p_estatusaprobado)
                 and i.re_digitalizado=p_redigitalizo
           order by t.tipo_documento, i.numero_documento, i.version desc;
                 --dbms_output.put_line('Rows: '||cursor_infodoc%ROWCOUNT);
   else

     open cursor_infodoc for
        select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where e.expediente=p_idexpediente
                 and i.id_documento=p_ids_documento
                 --and i.id_documento in (p_ids_documento)
                 and i.estatus_documento=p_estatusdoc and i.re_digitalizado=p_redigitalizo
           order by t.tipo_documento, i.numero_documento, i.version desc;
           --dbms_output.put_line('paso por el else');

   end if;
    return cursor_infodoc;
    close cursor_infodoc;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_infodoc;
  end f_buscar_infodocumento;

  /**
     procedimiento que busca el puntero del
     archivo fisico para ser eliminado
  **/
  function f_eliminar_archivo(p_idinfodocumento in integer, p_mensaje out varchar2) return ref_cursor is

    cursor_eliminar ref_cursor;
  begin

   p_mensaje := 'EGD00';

   /*delete from datos_infodocumento where id_infodocumento=p_idinfodocumento;
   delete from infodocumento where id_infodocumento=p_idinfodocumento;*/

   open cursor_eliminar for
        select t.ruta_archivo, t.nombre_archivo from infodocumento t where t.id_infodocumento=p_idinfodocumento;

    return cursor_eliminar;
    close cursor_eliminar;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_eliminar;
  end f_eliminar_archivo;


  /**
     procedimiento que gestiona el agregar los datos
     de un tipo de documento, que puede ser insert o
     update
  **/
  procedure p_agregar_registro_archivo(p_accion in varchar2, p_nombrearchivo in varchar2,
                                       p_iddocumento in integer, p_rutaarchivo in varchar2,
                                       p_cantpaginas in integer, p_version in integer,
                                       p_idexpediente in varchar2, p_numerodoc in integer,
                                       p_fechavencimiento in date, p_datoadicional in varchar2,
                                       p_usuario in varchar2, p_idinfodocumento in integer,
                                       p_estatus in integer, p_redigitalizo in char,
                                       p_formato in varchar2, p_idinfodoc out integer,
                                       p_mensaje out varchar2) is


   fecha_actual date;
  begin

   p_mensaje := 'EGD00';
   p_idinfodoc := 0;

   select sysdate into fecha_actual from dual;

   if p_accion = 'versionar' then

     insert into infodocumento(id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo,
                               formato, numero_documento, version, paginas, fecha_vencimiento,
                               estatus_documento, re_digitalizado)
                 values(sq_infodocumento.nextval, p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo,
                        p_formato, p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus,
                        p_redigitalizo);

     select sq_infodocumento.currval into p_idinfodoc from dual;

     if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_datos, id_infodocumento, fecha_digitalizacion,
                                        usuario_digitalizo, dato_adicional)
                    values(sq_datos_infodocumento.nextval, p_idinfodoc, fecha_actual,
                           p_usuario, p_datoadicional);
      else
        rollback;
        p_mensaje := 'Falla al obtener al id del infoDocumento en versionar';
      end if;

    elsif p_accion = 'reemplazar' then

      p_idinfodoc := -1;
      update infodocumento set nombre_archivo=p_nombrearchivo, ruta_archivo=p_rutaarchivo,
                               paginas=p_cantpaginas, version=p_version, id_expediente=p_idexpediente,
                               numero_documento=p_numerodoc, fecha_vencimiento=p_fechavencimiento,
                               formato=p_formato, estatus_documento=p_estatus
      where id_infodocumento=p_idinfodocumento;

      update datos_infodocumento set fecha_digitalizacion = fecha_actual,
                                     usuario_digitalizo = p_usuario,
                                     dato_adicional = p_datoadicional
       where id_infodocumento=p_idinfodocumento;

    elsif p_accion = 'Guardar' then
       insert into infodocumento(id_infodocumento, id_documento, id_expediente, nombre_archivo,
                                ruta_archivo, formato, numero_documento, version, paginas,
                                fecha_vencimiento, estatus_documento, re_digitalizado)
                  values(sq_infodocumento.nextval, p_iddocumento, p_idexpediente, p_nombrearchivo,
                         p_rutaarchivo, p_formato, p_numerodoc, p_version, p_cantpaginas,
                         p_fechavencimiento, p_estatus, 0);

      select sq_infodocumento.currval into p_idinfodoc from dual;

      if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_datos, id_infodocumento, fecha_digitalizacion,
                                        usuario_digitalizo, dato_adicional)
                    values(sq_datos_infodocumento.nextval, p_idinfodoc, fecha_actual,
                           p_usuario, p_datoadicional);

         if p_redigitalizo = 1 then
           update infodocumento t set t.re_digitalizado=p_redigitalizo
              where t.id_infodocumento=p_idinfodocumento;
         end if;
      else
        rollback;
        p_mensaje := 'Falla al obtener al id del infoDocumento en guardar';
      end if;

    end if;

   exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
   end p_agregar_registro_archivo;


   /**
     procedimiento para actulizar el nombre del
     archivo despues de codificarlo
  **/
  procedure p_actualiza_nombre_archivo(p_nombrearchivo in varchar2, p_idinfodocumento in integer, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   update infodocumento set nombre_archivo=p_nombrearchivo
        where id_infodocumento=p_idinfodocumento;


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualiza_nombre_archivo;


  /**
     funcion que busca el ultimo numero del tipo
     de documento
  **/
  function f_buscar_ultimo_numero(p_iddocumento in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return integer is
    numero_doc integer;
  begin

   numero_doc := 0;
   p_mensaje := 'EGD00';

   select max(numero_documento) as numeroDocumento into numero_doc from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente;

   return numero_doc;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return numero_doc;
  end f_buscar_ultimo_numero;


  /**
     funcion que verifica si el consecutivo del
     numero del documento ya esta registrado
  **/
  function f_comprobar_numero_documento(p_iddocumento in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_numdoc ref_cursor;
  begin

   p_mensaje := 'EGD00';

   open cursor_numdoc for
   select numero_documento  from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente
   order by numero_documento;

   return cursor_numdoc;
   close cursor_numdoc;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_numdoc;
  end f_comprobar_numero_documento;


  /**
     funcion que comprueba si el nombre del archiv
     esta registrado en la base de datos
  **/
  function f_comprobar_nombre_archivo(p_idinfodocumento in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return varchar2 is
    nombre varchar2(255);
  begin

   nombre := null;
   p_mensaje := 'EGD00';

   select nombre_archivo into nombre from infodocumento
   where id_infodocumento= p_idinfodocumento and id_expediente=p_idexpediente;

   return nombre;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return nombre;
  end f_comprobar_nombre_archivo;


  /**
     funcio que busca los datos necesario para luego
     buscar el fisico del tipo de documento
  **/
  function f_buscar_fisico_documento(p_iddocumento in integer, p_numerodoc in integer, p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_doc ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_doc for
        select i.*, c.id_categoria, s.id_subcategoria, t.tipo_documento tipoDoc,
               e.estatus_documento estatusArchivo
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                                inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
                                inner join tipodocumento t on i.id_documento=t.id_documento
                                inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
                                inner join categoria c on s.id_categoria=c.id_categoria
           where i.id_documento=p_iddocumento and i.id_expediente=p_idexpediente
                 and i.numero_documento=p_numerodoc and i.estatus_documento<>2
           order by i.version desc;

  return cursor_doc;
  close cursor_doc;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_doc;
  end f_buscar_fisico_documento;

  /**
     funcion que busca la ultima version
     del documento
  **/
  function f_buscar_ultima_version(p_iddocumento in integer, p_idexpediente in varchar2,
                                   p_numerodocumento in integer, p_mensaje out varchar2)
                                   return ref_cursor is
    cursor_ver ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_ver for
      select max(i.version) as version
        from infodocumento i
        where i.id_documento=p_iddocumento
              and i.id_expediente=p_idexpediente
              and i.numero_documento=p_numerodocumento
              and i.estatus_documento<>2;

  return cursor_ver;
  close cursor_ver;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_ver;
  end f_buscar_ultima_version;

  /**
     funcion que busca la informacion de
     cada dato adicional segun el tipo
     y numero de documento
  **/
  function f_buscar_valor_dato_adicional(p_idocumento in integer, p_idexpediente in varchar2,
                                         p_numerodoc in integer, p_version in integer,
                                         p_mensaje out varchar2)
                                         return ref_cursor is
    cursor_dato ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_dato for
       select *
           from dato_adicional d
             inner join valor_dato_adicional v on d.id_dato_adicional=v.id_dato_adicional
           where d.id_documento=p_idocumento
                and v.expediente=p_idexpediente
                and v.numero=p_numerodoc
                and v.version=p_version;

  return cursor_dato;
  close cursor_dato;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_dato;

  end f_buscar_valor_dato_adicional;

  /**
     procedimiento para elimiar un registro
     de la tabla infodocumento al resturar
     un error en el guardado de un documento
  **/
  procedure p_eliminar_registro_archivo(p_idinfodocumento in integer, p_mensaje out varchar2) is

  begin

   p_mensaje := 'EGD00';

   delete from datos_infodocumento where id_infodocumento=p_idinfodocumento;
   delete from infodocumento where id_infodocumento=p_idinfodocumento;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_eliminar_registro_archivo;

end pkg_documento;
/

prompt
prompt Creating package body PKG_ELIMINA
prompt =================================
prompt
create or replace package body oradw4j.pkg_elimina is

  /**
     funcion que busca la informacion necesaria
     del documento a eliminar
  **/
  function f_buscar_infodocumento(p_idinfodocumento in integer, p_iddocumento in integer,
                                  p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor is
     cursor_info ref_cursor;
  begin
    p_mensaje := 'EGD00';

  if p_idinfodocumento > 0 then

    open cursor_info for
    select * from infodocumento where id_infodocumento=p_idinfodocumento;

  elsif p_iddocumento > 0 then

    open cursor_info for
    select * from infodocumento
    where id_documento=p_iddocumento and id_expediente=p_idexpediente
    order by numero_documento, version;

  end if;
  return cursor_info;
  close cursor_info;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_info;
  end f_buscar_infodocumento;


  /**
     procedimineto para eliminar el registro
     de la informacion del tipo de documento
  **/
  procedure p_eliminar_infodocumento(p_idinfodocumento in integer, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';
    delete datos_infodocumento where id_infodocumento = p_idinfodocumento;
    delete infodocumento where id_infodocumento = p_idinfodocumento;


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_eliminar_infodocumento;

  /**
     procedimiento que actualiza la numeracion
     del tipo de documento
  **/
  procedure p_actualizar_infodocumento(p_numerodoc in integer, p_idinfodocumento in integer,
                                       p_idexpediente in varchar2, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    update infodocumento set numero_documento=p_numerodoc
    where id_infodocumento=p_idinfodocumento and id_expediente=p_idexpediente;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_infodocumento;


  /**
     procedimiento que registra la traza del
     documento que se elimino
  **/
  procedure p_traza_elimina_documento(p_idexpedeinte in varchar2, p_idlibreria in integer,
                                      p_idcategoria in integer, p_idsubcategoria in integer,
                                      p_iddocumento in integer, p_numerodoc in integer,
                                      p_fechavencimiento in date, p_cantpaginas in integer,
                                      p_versiondoc in integer, p_datoadicional in varchar2,
                                      p_usuarioelimino in varchar2, p_usuariodigitalizo in varchar2,
                                      p_usuariorechazo in varchar2, p_causaelimino in varchar2,
                                      p_motivoelimino in varchar2, p_causarechazo in varchar2,
                                      p_motivorechazo in varchar2, p_mensaje out varchar2) is

    fecha_actual date;
    iddoceliminado integer;

  begin

    p_mensaje := 'EGD00';
    select sysdate into fecha_actual from dual;

    insert into documento_eliminado
      (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino)
    values
      (sq_documento_eliminado.nextval, p_idexpedeinte, p_idlibreria, p_idcategoria, p_idsubcategoria, p_iddocumento, p_numerodoc, p_versiondoc, p_cantpaginas, p_fechavencimiento, fecha_actual, p_usuarioelimino);

    select sq_documento_eliminado.currval into iddoceliminado from dual;

    if iddoceliminado > 0 then

      insert into datos_infodocumento
        (id_datos, id_doc_eliminado, usuario_digitalizo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional)
      values
        (sq_datos_infodocumento.nextval, iddoceliminado, p_usuariodigitalizo, p_usuariorechazo, p_motivorechazo, p_causarechazo, fecha_actual, p_usuarioelimino, p_motivoelimino, p_causaelimino, p_datoadicional);
    else
      rollback;
      p_mensaje := 'problemas al agregar en documento eliminado';
    end if;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_traza_elimina_documento;


  /**
     funcion que busca la informacion necesaria
     para eliminar tipos de documentos
  **/
  function f_buscar_informaciondoc(p_idsdocumento in integer, p_idexpediente in varchar2,
                                   p_mensaje out varchar2) return ref_cursor is

  cursor_infdoc ref_cursor;

  begin

    p_mensaje := 'EGD00';

    open cursor_infdoc for
         select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento partition(part_infodoc) i
            inner join datos_infodocumento partition(part_datos) d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expedientes e on i.id_expediente=e.expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;
            /*select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expediente e on i.id_expediente=e.id_expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;*/

       return cursor_infdoc;
       close cursor_infdoc;
  exception
    /*when NO_DATA_FOUND then
      p_mensaje := 'sin datos';*/
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_infdoc;
  end f_buscar_informaciondoc;


  /**
     funcion que busca todos los datos
     de un tipo de documento a eliminar
  **/
  function f_buscar_datosdoc(p_idinfodocumento in integer, p_versiondoc in integer,
                             p_idexpediente in varchar2, p_numerodoc in integer,
                             p_mensaje out varchar2) return ref_cursor is

    cursor_doc ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_doc for
      select i.*, d.*, c.id_categoria, s.id_subcategoria,
             t.tipo_documento tipoDoc, e.estatus_documento estatusArchivo
          from infodocumento i
          inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
          inner join tipodocumento t on i.id_documento=t.id_documento
          inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
          inner join categoria c on s.id_categoria=c.id_categoria
          inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
      where i.id_infodocumento=p_idinfodocumento and i.id_expediente=p_idexpediente
            and i.version=p_versiondoc and i.numero_documento=p_numerodoc
            and i.estatus_documento=2;

     return cursor_doc;
     close cursor_doc;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_doc;
  end f_buscar_datosdoc;

end pkg_elimina;
/

prompt
prompt Creating package body PKG_EXPEDIENTE
prompt ====================================
prompt
create or replace package body oradw4j.pkg_expediente is



  /**
    funcion para buscar y comprobar que exite el expediente
    en base de dato
  **/
  function f_buscar_expediente(p_expediente in varchar2, p_idlibreria in integer, p_idcategoria in integer, p_flag in char, p_mensaje out varchar2) return ref_cursor is
    cursor_expe ref_cursor;
  begin

   p_mensaje := 'EGD00';

   if (p_flag = 1) then

      open cursor_expe for
         select e.expediente, e.id_libreria, e.id_categoria, e.valor, a.id_indice,
                c.categoria, l.libreria, a.indice, a.tipo, a.codigo, a.clave
             from expedientes e inner join indices a
                     on (e.id_categoria=a.id_categoria and e.id_indice=a.id_indice)
                  inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.id_libreria
              where e.expediente=p_expediente and a.id_categoria=p_idcategoria
                and e.id_libreria=p_idlibreria
              order by a.id_indice;
    else
      open cursor_expe for
         select e.*, c.categoria, l.libreria
             from expedientes e inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.libreria
             where e.expediente=p_expediente;
    end if;
    return cursor_expe;
    close cursor_expe;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_expe;
  end f_buscar_expediente;

  /**
    funcion que busca los dato de una tabla, o sea,
    nombre de columna y su tipo de dato
  **/
  function f_informacion_tabla(p_tabla in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_tabla ref_cursor;
  begin
    p_mensaje := 'EGD00';

    open cursor_tabla for
      SELECT s.OWNER, s.TABLE_NAME, s.COLUMN_NAME, s.DATA_TYPE
          FROM all_tab_columns s
      WHERE
      --owner = 'GESTORDOCUMENTAL' and
      table_name=p_tabla;
      return cursor_tabla;
      close cursor_tabla;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_tabla;
  end f_informacion_tabla;


   /**
    procedimiento que guarda un nuevo expediente
  **/
  procedure p_guardar_expediente(p_idexpediente in varchar2, p_idindice in integer,
                                 p_valor in varchar2, p_idlibreria in integer,
                                 p_idcategoria in integer, p_mensaje out varchar2) is

  begin
    p_mensaje := 'EGD00';

    insert into expedientes
      (id_expedientes, expediente, id_indice, valor, id_libreria, id_categoria)
    values
      (sq_expediente.nextval, p_idexpediente, p_idindice, p_valor, p_idlibreria, p_idcategoria);


  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_guardar_expediente;

  /**
    procedimiento para eliminar el expediente
  **/
  procedure p_eliminar_expediente(p_idexpediente in varchar2, p_mensaje out varchar2) is

  begin
    p_mensaje := 'EGD00';

    delete from expedientes where expediente=p_idexpediente;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_eliminar_expediente;

  /**
     procedimeinto que actualiza los
     indices de un expediente
  **/
  procedure p_actualizar_indices(p_idexpedientenuevo in varchar2,
                                 p_idexpedienteviejo in varchar2, p_idindice in integer,
                                 p_valor in varchar2, p_idlibreria in integer,
                                 p_idcategoria in integer, p_mensaje out varchar2) is
    
  begin

    p_mensaje := 'EGD00';


    if (p_idexpedienteviejo = p_idexpedientenuevo) then

      update expedientes
         set valor = p_valor
       where id_indice = p_idindice
         and id_libreria = p_idlibreria
         and id_categoria = p_idcategoria;

         dbms_output.put_line('expedientes iguales');

    else
         dbms_output.put_line('expedientes diferentes');
        insert into expedientes
          (id_expedientes, expediente, id_indice, valor, id_libreria, id_categoria)
        values
          (sq_expediente.nextval, p_idexpedientenuevo, p_idindice, p_valor, p_idlibreria, p_idcategoria);

        
    end if;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_actualizar_indices;

  /**
     procedimeinto que actualiza los
     indices de un expediente
  **/
  function f_modificar_indices(p_idexpedientenuevo in varchar2,
                                 p_idexpedienteviejo in varchar2, 
                                 p_flag in varchar2,  p_mensaje out varchar2) return ref_cursor is
    curor_indice ref_cursor;
  begin

    p_mensaje := 'EGD00';
    
    if p_flag = '0' then
    
      open curor_indice for
         select expediente from expedientes where expediente=p_idexpedientenuevo;
      return curor_indice;
      close curor_indice;
      
    elsif p_flag = '1' then
      
      delete from expedientes where expediente=p_idexpedienteviejo;

      update infodocumento set id_expediente=p_idexpedientenuevo where id_expediente=p_idexpedienteviejo;
      return curor_indice;
    end if;
    
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
    return curor_indice;
  end f_modificar_indices;                              

  /**
     funcion que busca los tipos de documentos
     segun los identificadores de las subcategorias
  **/
  function f_buscar_tipo_documento(p_idssucategorias in integer, p_mensaje out varchar2) return ref_cursor is
    curor_doc ref_cursor;
  begin
    p_mensaje := 'EGD00';

    open curor_doc for
     select t.*, e.*
       from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
       --where t.id_subcategoria in (p_idssucategorias) and e.id_estatus=1;
       where t.id_subcategoria = p_idssucategorias and e.id_estatus=1;

    return curor_doc;
    close curor_doc;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return curor_doc;
  end f_buscar_tipo_documento;

  /**
     funcion que busca la informacion
     para la ficha del expediente
  **/
  function f_buscar_foto_ficha(p_idexpediente in varchar2, p_mensaje out varchar2) return ref_cursor is
    cursor_ficha ref_cursor;
  begin
    p_mensaje := 'EGD00';

    open cursor_ficha for
        select t.id_documento, i.id_infodocumento, i.nombre_archivo as nombreArchivo,
               t.tipo_documento as tipoDocumento, i.estatus_documento, i.ruta_archivo
          from tipodocumento t inner join infodocumento i on t.id_documento=i.id_documento
          where i.id_expediente=p_idexpediente and t.ficha='1';

    return cursor_ficha;
    close cursor_ficha;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_ficha;
  end f_buscar_foto_ficha;

end pkg_expediente;
/

prompt
prompt Creating package body PKG_FOLIATURA
prompt ===================================
prompt
create or replace package body oradw4j.pkg_foliatura is

  /**
     funcion que busca la informacion necesaria
     para eliminar tipos de documentos
  **/
  function f_foliatura_buscar_expediente(p_idexpediente in varchar2, p_idlibreria in integer, p_idcategoria in integer, p_mensaje out varchar2) return ref_cursor is
    cursor_folio ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_folio for
        select i.id_expediente, l.id_libreria, c.id_categoria, s.id_subcategoria,
               t.id_documento, i.id_infodocumento, t.tipo_documento as documento,
               i.paginas as cantidadPAginas 
     from infodocumento i 
         inner join tipodocumento t on i.id_documento=t.id_documento
         inner join subcategoria s on s.id_subcategoria=t.id_subcategoria
         inner join categoria c on c.id_categoria=t.id_categoria
         inner join libreria l on l.id_libreria=c.id_libreria
        where i.id_expediente=p_idexpediente and i.estatus_documento=1 and
              l.id_libreria=p_idlibreria and c.id_categoria=p_idcategoria
        order by t.id_documento, i.numero_documento desc, i.version desc;

    return cursor_folio;
    close cursor_folio;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_folio;
  end f_foliatura_buscar_expediente;

  /**
    procedimiento para agregar el orden de la
    foliatura del expediente
  **/
  procedure p_agregar_foliaturas(p_idinfodocumento in integer, p_iddocumento in integer, p_idexpediente in varchar2, p_pagina in integer, p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    insert into foliatura
      (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina)
    values
      (sq_foliatura.nextval, p_idinfodocumento, p_iddocumento, p_idexpediente, p_pagina);

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_foliaturas;

end pkg_foliatura;
/

prompt
prompt Creating package body PKG_MANTENIMIENTO
prompt =======================================
prompt
create or replace package body oradw4j.pkg_mantenimiento is


  /**
     funcion que busca la configuracion
     del gestor documental
  **/
  function f_buscar_configuracion(p_mensaje out varchar2) return ref_cursor is
    cursor_conf ref_cursor;
  begin

    p_mensaje := 'EGD00';

    open cursor_conf for
      select * from configuracion;

    return cursor_conf;
    close cursor_conf;
  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
      return cursor_conf;
  end f_buscar_configuracion;

  /**
     procedimiento que actualiza la
     nueva configuracion
  **/
  procedure p_agregar_configuracion(p_nombreservidor in varchar2, p_nombrebasedato in varchar,
                                    p_puertobasedato in integer, p_usuariobasedato in varchar2,
                                    p_passbasedato in varchar2, p_calidad in char,
                                    p_foliatura in char, p_ficha in char,
                                    p_fabrica in char, p_elimina in char,
                                    p_mensaje out varchar2) is

  begin

    p_mensaje := 'EGD00';

    update configuracion
       set calidad = p_calidad,
           foliatura = p_foliatura,
           server_name = p_nombreservidor,
           database_name = p_nombrebasedato,
           port = p_puertobasedato,
           userbd = p_usuariobasedato,
           password = p_passbasedato,
           ficha = p_ficha,
           fabrica = p_fabrica,
           elimina = p_elimina
     where id_configuracion=1;

  exception
    when OTHERS then
      p_mensaje := to_char(sqlcode) ||' '||sqlerrm;
  end p_agregar_configuracion;

end pkg_mantenimiento;
/


spool off
