/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.Libreria;
import com.develcom.administracion.Perfil;
import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.trazas.Traza;
import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;

/**
 *
 * @author develcom
 */
public class AdministraTipoDocumento extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibrerias;

    @Wire
    private Combobox cmbCategorias;

    @Wire
    private Combobox cmbSubCategorias;

    @Wire
    private Listbox lstTiposDocumentosExistentes;

    @Wire
    private Textbox txtTipoDocumento;

    @Wire
    private Radio rdActivo;

    @Wire
    private Radio rdInactivo;

    @Wire
    private Checkbox chkDatosAdicionales;

    @Wire
    private Checkbox chkVencimiento;

    @Wire
    private Listbox lstTiposDocumentosNuevos;

    private static final long serialVersionUID = -5483085536148153046L;

    private List<TipoDocumento> listaTipoDocumentos;
    private List<SubCategoria> listaSubCategorias;
    private List<Categoria> listaCategorias;
    private List<Libreria> listaLibrerias;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraTipoDocumento.class);
    private Session session;
    private ToolsFiles toolsFile = new ToolsFiles();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }

    }

    private void llenarLibreria() {

        Comboitem item;

        try {

            cmbLibrerias.getItems().clear();

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {
                    if (lib.getEstatus().equalsIgnoreCase("Activo")) {
                    item = new Comboitem();
                    item.setValue(lib.getIdLibreria());
                    item.setLabel(lib.getDescripcion());
                    item.setParent(cmbLibrerias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Librerias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onSelect = #cmbLibrerias")
    public void llenarCategoria() {

        Comboitem item;
        int idLibreria;

        try {

            cmbCategorias.getItems().clear();

            idLibreria = cmbLibrerias.getSelectedItem().getValue();
            listaCategorias = new AdministracionBusqueda().buscarCategoria("", idLibreria, 0);

            item = new Comboitem();
            item.setValue("");
            item.setLabel("");
            item.setParent(cmbCategorias);

            if (!listaCategorias.isEmpty()) {
                for (Categoria cat : listaCategorias) {
                    if (cat.getEstatus().equalsIgnoreCase("Activo")) {
                    item = new Comboitem();
                    item.setValue(cat.getIdCategoria());
                    item.setLabel(cat.getCategoria());
                    item.setParent(cmbCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Categorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onSelect = #cmbCategorias")
    public void llenarSubCategoria() {

        Comboitem item;
        int idCategoria;

        try {

            cmbSubCategorias.getItems().clear();

            idCategoria = cmbCategorias.getSelectedItem().getValue();
            listaSubCategorias = new AdministracionBusqueda().buscarSubCategoria("", idCategoria, 0);

            if (!listaSubCategorias.isEmpty()) {
                for (SubCategoria subCat : listaSubCategorias) {
                    if (subCat.getEstatus().equalsIgnoreCase("Activo")) {
                    item = new Comboitem();
                    item.setValue(subCat.getIdSubCategoria());
                    item.setLabel(subCat.getSubCategoria());
                    item.setParent(cmbSubCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Categorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las subcategorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las subcategorias", ex);
        }
    }

    @Listen("onSelect = #cmbSubCategorias")
    public void llenarTipoDocumentosExistentes() {

        int idcategoria, idsubcategoria;
        Listitem item;
        Listcell cell;

        try {

            lstTiposDocumentosExistentes.getItems().clear();

            idcategoria = cmbCategorias.getSelectedItem().getValue();
            idsubcategoria = cmbSubCategorias.getSelectedItem().getValue();
            listaTipoDocumentos = new AdministracionBusqueda().buscarTipoDocumento("", idcategoria, idsubcategoria);

            if (!listaTipoDocumentos.isEmpty()) {
                for (TipoDocumento td : listaTipoDocumentos) {

                    item = new Listitem();

                    cell = new Listcell(td.getTipoDocumento());
                    item.appendChild(cell);

                    cell = new Listcell(td.getEstatus());
                    item.appendChild(cell);

                    try {
                        if (td.getVencimiento().equalsIgnoreCase("1")) {
                            cell = new Listcell("Si");
                            item.appendChild(cell);
                        } else {
                            cell = new Listcell("No");
                            item.appendChild(cell);
                        }
                    } catch (NullPointerException e) {
                        cell = new Listcell("No");
                        item.appendChild(cell);
                    }

                    try {
                        if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                            cell = new Listcell("Si");
                            item.appendChild(cell);
                        } else {
                            cell = new Listcell("No");
                            item.appendChild(cell);
                        }
                    } catch (NullPointerException e) {
                        cell = new Listcell("No");
                        item.appendChild(cell);
                    }

                    item.setParent(lstTiposDocumentosExistentes);
                }
            } else {
                herramientas.warn("La Categoria no posee SubCategorias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }

    }

    @Listen("onClick = #btnAgregar")
    public void agregarTipoDocumento() {
        String tipoDocumento, subCategoria, categoria, libreria, estatus = "";
        boolean banLista = true, banTabla = true;
        TipoDocumento documentoSel = new TipoDocumento();
        List<Listitem> tipoDocumentosNuevos = new ArrayList<>();
        Listitem item;
        Listcell cell;

        try {

            libreria = cmbLibrerias.getSelectedItem().getLabel();
            categoria = cmbCategorias.getSelectedItem().getLabel();
            subCategoria = cmbSubCategorias.getSelectedItem().getLabel();
            tipoDocumento = txtTipoDocumento.getValue();

            if (rdActivo.isSelected()) {
                estatus = rdActivo.getLabel();
            } else if (rdInactivo.isSelected()) {
                estatus = rdInactivo.getLabel();
            }

            if (!libreria.equalsIgnoreCase("")) {

                if (!categoria.equalsIgnoreCase("")) {

                    if (!subCategoria.equalsIgnoreCase("")) {

                        if (!tipoDocumento.equalsIgnoreCase("")) {

                            for (TipoDocumento tipoDoc : listaTipoDocumentos) {
                                String td = tipoDoc.getTipoDocumento().trim();
                                if (td.equalsIgnoreCase(tipoDocumento)) {
                                    banLista = false;
                                    documentoSel = tipoDoc;
                                    break;
                                } else {
                                    banLista = true;
                                }
                            }

                            tipoDocumentosNuevos = lstTiposDocumentosNuevos.getItems();

                            for (Listitem libex : tipoDocumentosNuevos) {
                                List<Listcell> cells = libex.getChildren();
                                String lib = cells.get(0).getLabel();
                                if (lib.equalsIgnoreCase(tipoDocumento)) {
                                    banTabla = false;
                                    break;
                                }
                            }

                            if (banTabla) {
                                if (banLista) {

                                    item = new Listitem();

                                    cell = new Listcell(libreria);
                                    item.appendChild(cell);

                                    cell = new Listcell(categoria);
                                    item.appendChild(cell);

                                    cell = new Listcell(subCategoria);
                                    item.appendChild(cell);

                                    cell = new Listcell(tipoDocumento);
                                    item.appendChild(cell);

                                    if (chkVencimiento.isChecked()) {
                                        cell = new Listcell("Si");
                                        item.appendChild(cell);
                                    } else {
                                        cell = new Listcell("No");
                                        item.appendChild(cell);
                                    }

                                    if (chkDatosAdicionales.isChecked()) {
                                        cell = new Listcell("Si");
                                        item.appendChild(cell);
                                    } else {
                                        cell = new Listcell("No");
                                        item.appendChild(cell);
                                    }

                                    cell = new Listcell(estatus);
                                    item.appendChild(cell);

                                    item.setParent(lstTiposDocumentosNuevos);

                                    txtTipoDocumento.setValue("");
                                    txtTipoDocumento.setDisabled(false);

                                } else {

                                    int n = Messagebox.show("Ya la categoria se encuentra en la lista de existentes \n ¿desea modificarlo?",
                                            "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                                            new org.zkoss.zk.ui.event.EventListener() {

                                                @Override
                                                public void onEvent(Event evt) throws Exception {
                                                }

                                            });

                                    if (n == Messagebox.YES) {

                                        item = new Listitem();

                                        cell = new Listcell(libreria);
                                        item.appendChild(cell);

                                        cell = new Listcell(categoria);
                                        item.appendChild(cell);

                                        cell = new Listcell(subCategoria);
                                        item.appendChild(cell);

                                        cell = new Listcell(tipoDocumento);
                                        item.appendChild(cell);

                                        if (documentoSel.getVencimiento().equalsIgnoreCase("1")) {
                                            cell = new Listcell("Si");
                                            item.appendChild(cell);
                                        } else {
                                            cell = new Listcell("No");
                                            item.appendChild(cell);
                                        }

                                        if (documentoSel.getDatoAdicional().equalsIgnoreCase("1")) {
                                            cell = new Listcell("Si");
                                            item.appendChild(cell);
                                        } else {
                                            cell = new Listcell("No");
                                            item.appendChild(cell);
                                        }

                                        cell = new Listcell(estatus);
                                        item.appendChild(cell);

                                        item.setParent(lstTiposDocumentosNuevos);

                                        txtTipoDocumento.setValue("");
                                        txtTipoDocumento.setDisabled(false);

                                    } else {
                                        txtTipoDocumento.setValue("");
                                        txtTipoDocumento.setDisabled(false);
                                    }
                                }
                            } else {
                                herramientas.warn("Ya el Tipo de Documento se encuentra en la lista");
                            }

                        } else {
                            herramientas.warn("Debe colocar un Tipo de Documento");
                        }
                    } else {
                        herramientas.warn("Debe seleccionar una SubCategoria");
                    }
                } else {
                    herramientas.warn("Debe seleccionar una Categoria");
                }
            } else {
                herramientas.warn("Debe seleccionar una libreria");
            }
        } catch (Exception e) {
            traza.trace("problemas al agregar la libreria", Level.ERROR, e);
            herramientas.error("Problemas al agregar la Libreria", e);
        }
    }

    @Listen("onClick = #btnGuardar")
    public void guardarTipoDocumentos() {

        List<Perfil> tiposDocumentos = new ArrayList<>();
        Perfil tipoDocumento;
        Libreria lib;
        Categoria cat;
        SubCategoria subCat;
        TipoDocumento tipoDoc;
        List<Listitem> items;
        List<Listcell> cells;
        boolean resp;

        try {

            int n = Messagebox.show("Seguro que desea guardar los Tipos de Documentos",
                    "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event evt) throws Exception {
                        }

                    });

            if (n == Messagebox.YES) {

                items = lstTiposDocumentosNuevos.getItems();

                if (!items.isEmpty()) {
                    for (Listitem item : items) {
                        cells = item.getChildren();

                        tipoDocumento = new Perfil();
                        cat = new Categoria();
                        lib = new Libreria();
                        subCat = new SubCategoria();
                        tipoDoc = new TipoDocumento();

                        lib.setDescripcion(cells.get(0).getLabel());

                        for (Libreria li : listaLibrerias) {
                            if (li.getDescripcion().equalsIgnoreCase(lib.getDescripcion())) {
                                lib.setIdLibreria(li.getIdLibreria());
                                if (li.getEstatus().equalsIgnoreCase("activo")) {
                                    lib.setEstatus("1");
                                } else if (li.getEstatus().equalsIgnoreCase("inactivo")) {
                                    lib.setEstatus("2");
                                }
                                break;
                            }
                        }

                        cat.setCategoria(cells.get(1).getLabel());

                        for (Categoria ca : listaCategorias) {
                            if (ca.getCategoria().equalsIgnoreCase(cat.getCategoria())) {
                                cat.setIdCategoria(ca.getIdCategoria());

                                if (ca.getEstatus().equalsIgnoreCase("activo")) {
                                    cat.setEstatus("1");
                                } else if (ca.getEstatus().equalsIgnoreCase("inactivo")) {
                                    cat.setEstatus("2");
                                }

                                break;
                            }
                        }

                        subCat.setSubCategoria(cells.get(2).getLabel());

                        for (SubCategoria sc : listaSubCategorias) {
                            if (sc.getSubCategoria().equalsIgnoreCase(subCat.getSubCategoria())) {
                                subCat.setIdSubCategoria(sc.getIdSubCategoria());
                                if (sc.getEstatus().equalsIgnoreCase("activo")) {
                                    subCat.setEstatus("1");
                                } else if (sc.getEstatus().equalsIgnoreCase("inactivo")) {
                                    subCat.setEstatus("2");
                                }
                                break;
                            }
                        }

                        tipoDoc.setTipoDocumento(cells.get(3).getLabel());

                        if (cells.get(4).getLabel().equalsIgnoreCase("si")) {
                            tipoDoc.setVencimiento("1");
                        } else {
                            tipoDoc.setVencimiento("0");
                        }

                        if (cells.get(5).getLabel().equalsIgnoreCase("si")) {
                            tipoDoc.setDatoAdicional("1");
                        } else {
                            tipoDoc.setDatoAdicional("0");
                        }

                        if (cells.get(6).getLabel().equalsIgnoreCase(rdActivo.getLabel())) {
                            tipoDoc.setEstatus("1");
                        } else if (cells.get(6).getLabel().equalsIgnoreCase(rdInactivo.getLabel())) {
                            tipoDoc.setEstatus("2");
                        }
                        tipoDoc.setIdCategoria(cat.getIdCategoria());
                        tipoDoc.setIdSubCategoria(subCat.getIdSubCategoria());

                        for (TipoDocumento td : listaTipoDocumentos) {
                            if (tipoDoc.getTipoDocumento().equalsIgnoreCase(td.getTipoDocumento())) {
                                tipoDoc.setIdTipoDocumento(td.getIdTipoDocumento());
                            }
                        }

                        tipoDocumento.setLibreria(lib);
                        tipoDocumento.setCategoria(cat);
                        tipoDocumento.setSubCategoria(subCat);
                        tipoDocumento.setTipoDocumento(tipoDoc);

                        tiposDocumentos.add(tipoDocumento);
                    }

                    resp = new AdministracionAgregar().insertaTipoDocumento(tiposDocumentos);

                    if (resp) {
                        herramientas.info("Tipos de Documentos guardados con exito");
                    } else {
                        herramientas.warn("Problemas al guardar los Tipos de Documentos");
                    }
                    limpiarComponentes();
                } else {
                    herramientas.info("La tabla de SubCategorias esta vacia");
                }

            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al guardar las subcategorias", Level.ERROR, ex);
            herramientas.error("Problemas al guardar las SubCategorias", ex);
        }

    }

    @Listen("onClick = #btnCambiarEstatus")
    public void cambiarEstatus() {

        List<Listcell> cells;
        Listitem item;

        item = lstTiposDocumentosExistentes.getSelectedItem();
        cells = item.getChildren();

        txtTipoDocumento.setValue(cells.get(0).getLabel());
        txtTipoDocumento.setDisabled(true);

        if (cells.get(2).getLabel().equalsIgnoreCase("si")) {
            chkVencimiento.setChecked(true);
        } else {
            chkVencimiento.setChecked(false);
        }
        chkVencimiento.setDisabled(true);

        if (cells.get(2).getLabel().equalsIgnoreCase("si")) {
            chkDatosAdicionales.setChecked(true);
        } else {
            chkDatosAdicionales.setChecked(false);
        }
        chkDatosAdicionales.setDisabled(true);
    }

    private void limpiarComponentes() {
        txtTipoDocumento.setText("");
        rdActivo.setSelected(false);
        rdInactivo.setSelected(false);
        lstTiposDocumentosExistentes.getItems().clear();
        lstTiposDocumentosNuevos.getItems().clear();
        cmbLibrerias.setText("");
        cmbCategorias.setText("");
        cmbSubCategorias.setText("");
        cmbLibrerias.getItems().clear();
        cmbCategorias.getItems().clear();
        cmbSubCategorias.getItems().clear();
        llenarLibreria();
    }

}
