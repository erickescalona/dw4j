/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.reportes.tools;

import ar.com.fdvs.dj.core.registration.EntitiesRegistrationException;
import com.develcom.dao.Campos;
import com.develcom.dao.Mensajes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.log4j.Level;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Session;
import org.zkoss.zul.Window;

/**
 *
 * @author develcom
 */
public class ProcesaReporte {

    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(ProcesaReporte.class);
    private Session session;

    public void crearReporte(String tituloReporte, String query, String lib, String cat, List<Campos> listaCampos, String fechas) {

        String raiz, nombrePDF;
        File rutaReportes, reporte;

        try {

            nombrePDF = tituloReporte.replace(" ", "_");

            session = herramientas.crearSesion();

            ReporteDinamico rd = new ReporteDinamico();

            JasperPrint jasperPrint = rd.crearReporteDinamico(tituloReporte, query, listaCampos, lib, cat, fechas);

            raiz = herramientas.getRutaAplicacion();

            rutaReportes = new File(raiz + "/reportes/");

            if (!rutaReportes.exists()) {
                rutaReportes.mkdir();
            }

            reporte = new File(raiz + "/reportes/" + nombrePDF + ".pdf");

            if (!reporte.exists()) {
                reporte.delete();
            }

            JasperExportManager.exportReportToPdfFile(jasperPrint, raiz + "/reportes/" + nombrePDF + ".pdf");

            session.setAttribute("reporte", reporte);
            Window foliarura = herramientas.crearVentanaModal("/reporte.zul");
            foliarura.doModal();

//            JasperViewer frame = new JasperViewer(jasperPrint, false);
//            frame.setTitle(tituloReporte + " - " + Mensajes.getMensaje());
//            frame.setAlwaysOnTop(true);
//            frame.pack();
//            frame.setVisible(true);
//            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//            frame.toFront();
        } catch (EntitiesRegistrationException e) {
            traza.trace("error general al construir el reporte dinamico", Level.ERROR, e);
        } catch (JRException ex) {
            traza.trace("error al construir el reporte", Level.ERROR, ex);
        }
        //return jasperPrint;
    }

    public void crearReporte(String plantilla, Map params, String tituloReporte) {

        JasperReport jasperReport;
        JasperPrint jasperPrint;
        String raiz, nombrePDF;
        int index;
        File rutaReportes, reporte;

        try {

            session = herramientas.crearSesion();

            traza.trace("plantilla " + plantilla, Level.INFO);

            index = plantilla.indexOf(".");
            nombrePDF = plantilla.substring(0, index);

            Collection valores = params.values();
            Iterator it = valores.iterator();

            while (it.hasNext()) {
                traza.trace("valor del parametro " + it.next().toString(), Level.INFO);
            }

            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            InputStream fileReport = cl.getResourceAsStream("com/develcom/paginas/reportes/plantillas/" + plantilla);

            jasperReport = JasperCompileManager.compileReport(fileReport);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, new BaseDato().conectar());

            raiz = herramientas.getRutaAplicacion();

            rutaReportes = new File(raiz + "/reportes/");

            if (!rutaReportes.exists()) {
                rutaReportes.mkdir();
            }

            reporte = new File(raiz + "/reportes/" + nombrePDF + ".pdf");

            if (!reporte.exists()) {
                reporte.delete();
            }

            JasperExportManager.exportReportToPdfFile(jasperPrint, raiz + "/reportes/" + nombrePDF + ".pdf");

            session.setAttribute("reporte", reporte);

            if (nombrePDF.equalsIgnoreCase("foliatura")) {
                Window foliarura = herramientas.crearVentanaModal("/foliatura.zul");
                foliarura.doModal();
            } else {
                Window foliarura = herramientas.crearVentanaModal("/reporte.zul");
                foliarura.doModal();
            }

//            JasperViewer frame = new JasperViewer(jasperPrint, false);
//            frame.setTitle(tituloReporte + " - " + Mensajes.getMensaje());
            //frame.setAlwaysOnTop(true);
//            frame.pack();
//            frame.setVisible(true);
//            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//            frame.toFront();
        } catch (JRException e) {
            traza.trace("error al construir el reporte", Level.ERROR, e);
        } catch (SQLException ex) {
            traza.trace("error de coneccion con la base de datos", Level.ERROR, ex);
        } catch (ClassNotFoundException ex) {
            traza.trace("error el en driver oracle", Level.ERROR, ex);
        }
    }

}
