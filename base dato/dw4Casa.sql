--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.2

-- Started on 2017-04-30 20:35:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "dw4jPrueba";
--
-- TOC entry 2437 (class 1262 OID 20936)
-- Name: dw4jPrueba; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "dw4jPrueba" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_VE.UTF-8' LC_CTYPE = 'es_VE.UTF-8';


ALTER DATABASE "dw4jPrueba" OWNER TO postgres;

\connect "dw4jPrueba"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2439 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 222 (class 1255 OID 20937)
-- Name: f_buscar_categoria(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_categoria(p_idcategoria integer, p_idlibreria integer, p_categoria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cat refcursor;
  begin


    if p_idCategoria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_categoria=p_idcategoria
          order by c.categoria;

    elsif p_idLibreria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_libreria=p_idlibreria
          order by c.categoria;

    elsif p_categoria is not null then
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.categoria=p_categoria
          order by c.categoria;
    else
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
           order by c.categoria;
    end if;
    return cursor_cat;
    close cursor_cat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_categoria(p_idcategoria integer, p_idlibreria integer, p_categoria character varying) OWNER TO postgres;

--
-- TOC entry 223 (class 1255 OID 20938)
-- Name: f_buscar_causas_rechazo(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_causas_rechazo() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_causa refcursor;
  begin

    open cursor_causa for
      select id_causa, causa from causa;

    return cursor_causa;
    close cursor_causa;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_causas_rechazo() OWNER TO postgres;

--
-- TOC entry 224 (class 1255 OID 20939)
-- Name: f_buscar_configuracion(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_configuracion() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_conf refcursor;
  begin

    open cursor_conf for
      select * from configuracion;

    return cursor_conf;
    close cursor_conf;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_configuracion() OWNER TO postgres;

--
-- TOC entry 239 (class 1255 OID 20940)
-- Name: f_buscar_datos_combo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_datos_combo(p_idcodigoindice integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice
             from lista_desplegables l inner join indices a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idcodigoindice
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_datos_combo(p_idcodigoindice integer) OWNER TO postgres;

--
-- TOC entry 240 (class 1255 OID 20941)
-- Name: f_buscar_datosdoc(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_datosdoc(p_idinfodocumento integer, p_versiondoc integer, p_idexpediente character varying, p_numerodoc integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

    cursor_doc refcursor;
  begin

    open cursor_doc for
      select i.*, d.*, c.id_categoria, s.id_subcategoria,
             t.tipo_documento tipoDoc, e.estatus_documento estatusArchivo
          from infodocumento i
          inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
          inner join tipodocumento t on i.id_documento=t.id_documento
          inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
          inner join categoria c on s.id_categoria=c.id_categoria
          inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
      where i.id_infodocumento=p_idinfodocumento and i.id_expediente=p_idexpediente
            and i.version=p_versiondoc and i.numero_documento=p_numerodoc
            and i.estatus_documento=2;

     return cursor_doc;
     close cursor_doc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_datosdoc(p_idinfodocumento integer, p_versiondoc integer, p_idexpediente character varying, p_numerodoc integer) OWNER TO postgres;

--
-- TOC entry 221 (class 1255 OID 20942)
-- Name: f_buscar_estatus(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_estatus(p_idestatus integer, p_estatus character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_est refcursor;
  begin

    if p_idestatus > 0 then
      open cursor_est for
         select id_estatus, estatus from estatus where id_estatus=p_idestatus;
    elsif p_estatus is not null then
      open cursor_est for
         select id_estatus, estatus from estatus where estatus=p_estatus;
    end if;

    return cursor_est;
    close cursor_est;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_estatus(p_idestatus integer, p_estatus character varying) OWNER TO postgres;

--
-- TOC entry 242 (class 1255 OID 20943)
-- Name: f_buscar_expediente(character varying, integer, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_expediente(p_expediente character varying, p_idlibreria integer, p_idcategoria integer, p_flag character) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_expe refcursor;
  begin

   if (p_flag = '1') then

      open cursor_expe for
         select distinct e.expediente, e.id_libreria, e.id_categoria, e.valor, e.fecha_indice, a.id_indice,
                c.categoria, l.libreria, a.indice, a.tipo, a.codigo, a.clave
             from expedientes e inner join indices a
                     on (e.id_categoria=a.id_categoria and e.id_indice=a.id_indice)
                  inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.id_libreria
              where e.expediente=p_expediente and a.id_categoria=p_idcategoria
                and e.id_libreria=p_idlibreria
              order by a.id_indice;
    else
      open cursor_expe for
         select e.*, c.categoria, l.libreria, a.indice, a.tipo, a.codigo, a.clave
             from expedientes e inner join indices a
                     on (e.id_categoria=a.id_categoria and e.id_indice=a.id_indice)
		  inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.id_libreria
             where e.expediente=p_expediente;
    end if;
    return cursor_expe;
    close cursor_expe;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_expediente(p_expediente character varying, p_idlibreria integer, p_idcategoria integer, p_flag character) OWNER TO postgres;

--
-- TOC entry 243 (class 1255 OID 20944)
-- Name: f_buscar_expediente_dinamico(character varying, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_expediente_dinamico(p_filtro character varying, p_idexpediente character varying, p_flag character) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_expe refcursor;
    query character varying;
  begin

   query:=null;
   
  if p_flag = '1' then
  
	  query:='select distinct e.expediente, e.valor, e.fecha_indice, i.* 
		 from expedientes e 
		 inner join indices i on e.id_indice=i.id_indice 
		 inner join libreria l on e.id_libreria=l.id_libreria 
		 inner join categoria c on e.id_categoria=c.id_categoria 
		 inner join subcategoria s on c.id_categoria=s.id_categoria 
		 inner join tipodocumento t on s.id_subcategoria=t.id_subcategoria '
		 ||p_filtro;
		 
   elsif p_flag = '0' then
   
          query:='select distinct e.expediente, e.valor, e.fecha_indice, i.*  
		 from expedientes e 
		 inner join indices i on e.id_indice=i.id_indice 
		 inner join infodocumento d on e.expediente=d.id_expediente 
		 inner join libreria l on e.id_libreria=l.id_libreria 
		 inner join categoria c on e.id_categoria=c.id_categoria 
		 inner join subcategoria s on c.id_categoria=s.id_categoria 
		 inner join tipodocumento t on s.id_subcategoria=t.id_subcategoria '
                 ||p_filtro;
                 
   end if;

   if query is null and p_filtro is null then

         open cursor_expe for
           select distinct i.indice, e.valor, e.fecha_indice, i.id_indice, i.id_categoria, i.tipo, i.codigo, i.clave
		    from expedientes e 
		    inner join indices i on e.id_indice=i.id_indice 
		    where e.expediente=p_idexpediente
		    order by i.id_indice;
		    
        return cursor_expe;
        close cursor_expe;
   else
     OPEN cursor_expe FOR execute query;
      return cursor_expe;
      close cursor_expe;
   end if;
   	
  
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_expediente_dinamico(p_filtro character varying, p_idexpediente character varying, p_flag character) OWNER TO postgres;

--
-- TOC entry 244 (class 1255 OID 20945)
-- Name: f_buscar_expediente_generico(character varying, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_expediente_generico(p_filtro character varying, p_idexpediente character varying, p_flag character) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_expe refcursor;
    query character varying;
  begin

   query:=null;
   
  if p_flag = '1' then

	query:='select distinct e.expediente, e.valor, e.fecha_indice, i.* 
		from libreria l inner join categoria c on l.id_libreria=c.id_libreria
		inner join expedientes e on (e.id_libreria=l.id_libreria and e.id_categoria=c.id_categoria)
		inner join indices i on i.id_indice=e.id_indice '
		 ||p_filtro;
                 
   end if;

   if query is null and p_filtro is null then

         open cursor_expe for
           select distinct i.indice, e.valor, e.fecha_indice, i.id_indice, i.id_categoria, i.tipo, i.codigo, i.clave
		    from expedientes e 
		    inner join indices i on e.id_indice=i.id_indice 
		    where e.expediente=p_idexpediente
		    order by i.id_indice;
		    
        return cursor_expe;
        close cursor_expe;
   else
     OPEN cursor_expe FOR execute query;
      return cursor_expe;
      close cursor_expe;
   end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_expediente_generico(p_filtro character varying, p_idexpediente character varying, p_flag character) OWNER TO postgres;

--
-- TOC entry 245 (class 1255 OID 20946)
-- Name: f_buscar_expediente_reporte(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_expediente_reporte(p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_expe refcursor;
  begin

   open cursor_expe for
   SELECT distinct e.expediente, e.valor, e.fecha_indice,  
        i.id_indice, i.indice, i.tipo, i.codigo, i.clave, e.id_libreria, e.id_categoria
  FROM expedientes e inner join indices i on e.id_indice=i.id_indice
  where e.id_categoria=p_idcategoria
  order by e.expediente, i.id_indice;

    return cursor_expe;
    close cursor_expe;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_expediente_reporte(p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 315 (class 1255 OID 21305)
-- Name: f_buscar_fabrica(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_fabrica(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$

  DECLARE
    cursor_user refcursor;
  begin
  
  open cursor_user for
        select f.fabrica, f.usuario
           from fabrica f 
           inner join usuario u on u.id_usuario=f.usuario
         where f.usuario=p_usuario;
    
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;
$$;


ALTER FUNCTION public.f_buscar_fabrica(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 20947)
-- Name: f_buscar_fabrica(character varying, date, date, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_fabrica(p_usuario character varying, p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
    sfabrica char(1);
  begin

    select t.fabrica into sfabrica from fabrica t where t.usuario=p_usuario;

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion between CAST (p_fechadesde AS DATE)
           and CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion >= CAST (p_fechadesde AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion <= CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_expediente is not null then

         open cursor_user for
            select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
               inner join fabrica f on f.usuario=di.usuario_digitalizo
               inner join usuario u on u.id_usuario=f.usuario
             where d.id_expediente = p_expediente
               and d.estatus_documento=p_estatusdocumento
               and f.fabrica=sfabrica
               and i.id_categoria=p_idcategoria
               order by e.expediente, i.id_indice;
    else

      open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_fabrica(p_usuario character varying, p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expediente character varying) OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 20948)
-- Name: f_buscar_fisico_documento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_fisico_documento(p_iddocumento integer, p_numerodoc integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_doc refcursor;
  begin

    open cursor_doc for
        select i.*, c.id_categoria, s.id_subcategoria, t.tipo_documento tipoDoc,
               e.estatus_documento estatusArchivo
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                                inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
                                inner join tipodocumento t on i.id_documento=t.id_documento
                                inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
                                inner join categoria c on s.id_categoria=c.id_categoria
           where i.id_documento=p_iddocumento and i.id_expediente=p_idexpediente
                 and i.numero_documento=p_numerodoc and i.estatus_documento<>2
           order by i.version desc;

  return cursor_doc;
  close cursor_doc;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_fisico_documento(p_iddocumento integer, p_numerodoc integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 20949)
-- Name: f_buscar_foto_ficha(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_foto_ficha(p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    cursor_ficha refcursor;
  begin

    open cursor_ficha for
        select t.id_documento, i.id_infodocumento, i.nombre_archivo as nombreArchivo,
               t.tipo_documento as tipoDocumento, i.estatus_documento, i.ruta_archivo
          from tipodocumento t inner join infodocumento i on t.id_documento=i.id_documento
          where i.id_expediente=p_idexpediente and t.ficha='1';

    return cursor_ficha;
    close cursor_ficha;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_foto_ficha(p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 20950)
-- Name: f_buscar_indice_datosadicional(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_indice_datosadicional(p_idtipodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select *
         from dato_adicional t
         where t.id_documento = p_idtipodocumento
         order by t.id_dato_adicional;

    return cursor_dato;
    close cursor_dato;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_indice_datosadicional(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 20951)
-- Name: f_buscar_indices(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_indices(p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_arg refcursor;
  begin

    open cursor_arg for
       select * from indices i where id_categoria=p_idcategoria order by i.id_indice;
    return cursor_arg;
    close cursor_arg;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_indices(p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 20952)
-- Name: f_buscar_infodocumento(character varying, character varying, integer, character, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_infodocumento(p_idexpediente character varying, p_ids_documento character varying, p_estatusdoc integer, p_redigitalizo character, p_estatusaprobado integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_infodoc refcursor;
    query character varying;
  begin

   if p_estatusaprobado = 1 then
   
      --open cursor_infodoc for
        query:='select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where i.id_expediente='''||p_idexpediente||'''
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in ('||p_ids_documento||')
                 and (i.estatus_documento='||p_estatusdoc||' or i.estatus_documento='||p_estatusaprobado||')
                 and i.re_digitalizado='''||p_redigitalizo||'''
           order by t.tipo_documento, i.numero_documento, i.version desc';
   else

     --open cursor_infodoc for
        query:='select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where e.expediente='''||p_idexpediente||'''
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in ('||p_ids_documento||')
                 and i.estatus_documento='||p_estatusdoc||' and i.re_digitalizado='''||p_redigitalizo||'''
           order by t.tipo_documento, i.numero_documento, i.version desc';

   end if;

    OPEN cursor_infodoc FOR execute query;
   
    return cursor_infodoc;
    close cursor_infodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_infodocumento(p_idexpediente character varying, p_ids_documento character varying, p_estatusdoc integer, p_redigitalizo character, p_estatusaprobado integer) OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 20953)
-- Name: f_buscar_infodocumento_elimina(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_infodocumento_elimina(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
     cursor_info refcursor;
  begin

  if p_idinfodocumento > 0 then

    open cursor_info for
    select * from infodocumento where id_infodocumento=p_idinfodocumento;

  elsif p_iddocumento > 0 then

    open cursor_info for
    select * from infodocumento i inner join tipodocumento t on i.id_documento=t.id_documento
    where i.id_documento=p_iddocumento and i.id_expediente=p_idexpediente
    order by numero_documento, version;

  end if;
  return cursor_info;
  close cursor_info;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_infodocumento_elimina(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 20954)
-- Name: f_buscar_informaciondoc(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_informaciondoc(p_idsdocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

  cursor_infdoc refcursor;

  begin

    open cursor_infdoc for
         select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expedientes e on i.id_expediente=e.expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;
            /*select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expediente e on i.id_expediente=e.id_expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;*/

       return cursor_infdoc;
       close cursor_infdoc;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_informaciondoc(p_idsdocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 254 (class 1255 OID 20955)
-- Name: f_buscar_lib_cat_indice_perfil(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_lib_cat_indice_perfil() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
        select  p.id_usuario usuario, r.rol,
                p.id_libreria, l.libreria, el.estatus status_lib,
                p.id_categoria, c.categoria, i.id_indice, i.indice, i.clave, 
                i.tipo, i.codigo, ec.estatus status_cat
           from perfil p
                inner join rol r on p.id_rol=r.id_rol
                inner join libreria l on p.id_libreria=l.id_libreria
                inner join estatus el on l.id_estatus=el.id_estatus
                inner join categoria c on p.id_categoria=c.id_categoria
                inner join estatus ec on c.id_estatus=ec.id_estatus
                inner join indices i on i.id_categoria=c.id_categoria
           where p.id_usuario=p_usuario and r.rol=p_perfil and i.clave != ''
                 and i.clave != 'o' and i.clave != 'O'
           order by l.libreria, c.categoria, i.clave desc;
    return cursor_user;
    close cursor_user;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_lib_cat_indice_perfil() OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 20956)
-- Name: f_buscar_lib_cat_indice_perfil(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_lib_cat_indice_perfil(p_usuario character varying, p_perfil character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
        select  p.id_usuario usuario, r.rol,
                p.id_libreria, l.libreria, el.estatus status_lib,
                p.id_categoria, c.categoria, i.id_indice, i.indice, i.clave, 
                i.tipo, i.codigo, ec.estatus status_cat
           from perfil p
                inner join rol r on p.id_rol=r.id_rol
                inner join libreria l on p.id_libreria=l.id_libreria
                inner join estatus el on l.id_estatus=el.id_estatus
                inner join categoria c on p.id_categoria=c.id_categoria
                inner join estatus ec on c.id_estatus=ec.id_estatus
                inner join indices i on i.id_categoria=c.id_categoria
           where p.id_usuario=p_usuario and r.rol=p_perfil and i.clave != ''
                 and i.clave != 'o' and i.clave != 'O'
           order by l.libreria, c.categoria, i.clave desc;
    return cursor_user;
    close cursor_user;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_lib_cat_indice_perfil(p_usuario character varying, p_perfil character varying) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 20957)
-- Name: f_buscar_lib_cat_perfil(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_lib_cat_perfil(p_usuario character varying, p_perfil character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
        select p.id_usuario usuario, r.rol,
               p.id_libreria, l.libreria, el.estatus status_lib,
               p.id_categoria, c.categoria, ec.estatus status_cat
           from perfil p
                inner join rol r on p.id_rol=r.id_rol
                inner join libreria l on p.id_libreria=l.id_libreria
                inner join estatus el on l.id_estatus=el.id_estatus
                inner join categoria c on p.id_categoria=c.id_categoria
                inner join estatus ec on c.id_estatus=ec.id_estatus
           where p.id_usuario=p_usuario and r.rol=p_perfil
           order by l.libreria, c.categoria;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_lib_cat_perfil(p_usuario character varying, p_perfil character varying) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 20958)
-- Name: f_buscar_libreria(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_libreria(p_idlibreria integer, p_libreria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_lib refcursor;
  begin

    if p_idlibreria > 0 then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.id_libreria=p_idlibreria
             order by l.libreria asc;

    elsif p_libreria is not null then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.libreria=p_libreria
             order by l.libreria asc;

    else

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             order by l.libreria asc;

    end if;
    return cursor_lib;
    close cursor_lib;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_libreria(p_idlibreria integer, p_libreria character varying) OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 20959)
-- Name: f_buscar_libreriascategorias(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_libreriascategorias() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_libcat refcursor;
  begin

    open cursor_libcat for
       select l.id_libreria, l.libreria descLibreria, c.id_categoria, c.categoria descCategoria,
              r.id_rol, r.rol descRol, e.id_estatus, e.estatus desEstatus
          from libreria l inner join categoria c on l.id_libreria=c.id_libreria
                          inner join estatus e
                                 on (l.id_estatus=e.id_estatus and c.id_estatus=e.id_estatus),
                          rol r
          where e.id_estatus=1
          order by l.libreria, c.categoria;

    return cursor_libcat;
    close cursor_libcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_libreriascategorias() OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 20960)
-- Name: f_buscar_no_fabrica(date, date, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_no_fabrica(p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expedeinte character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;

  begin

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion between CAST (p_fechadesde AS DATE)
           and CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select i.*, e.valor, e.fecha_indice, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion >= CAST (p_fechadesde AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion <= CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_expedeinte is not null then

         open cursor_user for
            select distinct e.expediente, e.valor, e.fecha_indice, i.*
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
             where d.id_expediente = p_expedeinte
               and d.estatus_documento=p_estatusdocumento
               and i.id_categoria=p_idcategoria
               order by e.expediente, i.id_indice;
    else

      open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_no_fabrica(p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expedeinte character varying) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 20961)
-- Name: f_buscar_perfil(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_perfil(p_idusuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_perfil refcursor;
  begin

    open cursor_perfil for
        select distinct u.nombre, u.apellido, u.id_usuario, e.estatus estatus_usuario,
               f.fabrica pertenece, r.rol,
               l.id_libreria, l.libreria,
               c.id_categoria, c.categoria
            from usuario u full join perfil p on u.id_usuario=p.id_usuario
                           full join estatus e on u.id_estatus=e.id_estatus
                           full join fabrica f on u.id_usuario=f.usuario
                           full join rol r on p.id_rol=r.id_rol
                           full join libreria l on p.id_libreria=l.id_libreria
                           full join categoria c on p.id_categoria=c.id_categoria
             where u.id_usuario=p_idusuario
             order by l.libreria, c.categoria;


    return cursor_perfil;
    close cursor_perfil;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_perfil(p_idusuario character varying) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 20962)
-- Name: f_buscar_subcategoria(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_subcategoria(p_idcategoria integer, p_idsubcategoria integer, p_subcategoria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_subcat refcursor;
  begin

    if p_idcategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_categoria=p_idcategoria
          order by s.subcategoria;

    elsif p_idsubcategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_subcategoria=p_idsubcategoria
          order by s.subcategoria;

    elsif p_subcategoria is not null then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.subcategoria=p_subcategoria
          order by s.subcategoria;

    else

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
             order by s.subcategoria;
    end if;
    return cursor_subcat;
    close cursor_subcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_subcategoria(p_idcategoria integer, p_idsubcategoria integer, p_subcategoria character varying) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 20963)
-- Name: f_buscar_subcategorias(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_subcategorias(p_idscategorias character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    curor_doc refcursor;
    query character varying;
  begin

    query:='select s.*, e.*
       from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
       where s.id_categoria in ('||p_idscategorias||') and e.id_estatus=1
       order by s.id_subcategoria';

    OPEN curor_doc FOR execute query;
     
    return curor_doc;
    close curor_doc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_subcategorias(p_idscategorias character varying) OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 20964)
-- Name: f_buscar_tipo_documento(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_tipo_documento(p_idssucategorias character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    curor_doc refcursor;
    query character varying;
  begin

    query:='select t.*, e.*
       from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
       where t.id_subcategoria in ('||p_idssucategorias||') and e.id_estatus=1
       order by t.id_documento';

    OPEN curor_doc FOR execute query;

    /*open curor_doc for
     select t.*, e.*
       from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
       where t.id_subcategoria in (p_idssucategorias) and e.id_estatus=1;
       --where t.id_subcategoria = p_idssucategorias and e.id_estatus=1;*/
     
    return curor_doc;
    close curor_doc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_tipo_documento(p_idssucategorias character varying) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 20965)
-- Name: f_buscar_tipo_documento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_tipo_documento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumemto character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_tipodoc refcursor;
  begin

    if p_idCategoria > 0 and p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria and t.id_subcategoria=p_idsubcategoria
         order by t.tipo_documento;

    elsif p_idCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria order by t.tipo_documento;

    elsif p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_subcategoria=p_idsubcategoria order by t.tipo_documento;

    elsif p_tipodocumemto is not null then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.tipo_documento=p_tipodocumemto order by t.tipo_documento;

    else

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
            order by t.tipo_documento;

    end if;
    return cursor_tipodoc;
    close cursor_tipodoc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_tipo_documento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumemto character varying) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 20966)
-- Name: f_buscar_ultima_version(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_ultima_version(p_iddocumento integer, p_idexpediente character varying, p_numerodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_ver refcursor;
  begin

    open cursor_ver for
      select max(i.version) as version
        from infodocumento i
        where i.id_documento=p_iddocumento
              and i.id_expediente=p_idexpediente
              and i.numero_documento=p_numerodocumento
              and i.estatus_documento<>2;

  return cursor_ver;
  close cursor_ver;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_ultima_version(p_iddocumento integer, p_idexpediente character varying, p_numerodocumento integer) OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 20967)
-- Name: f_buscar_ultimo_numero(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_ultimo_numero(p_iddocumento integer, p_idexpediente character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
    numero_doc integer;
  begin

   numero_doc := -1;
   
   select max(numero_documento) as numeroDocumento into numero_doc from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente;

   return numero_doc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_ultimo_numero(p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 20968)
-- Name: f_buscar_usuario_fabrica(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_usuario_fabrica(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_fab refcursor;
  begin

    open cursor_fab for
       select usuario, fabrica from fabrica f where f.usuario=p_usuario;


    return cursor_fab;
    close cursor_fab;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_usuario_fabrica(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 20969)
-- Name: f_buscar_valor_dato_adicional(integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_valor_dato_adicional(p_idocumento integer, p_idexpediente character varying, p_numerodoc integer, p_version integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select *
           from dato_adicional d
             inner join valor_dato_adicional v on d.id_dato_adicional=v.id_dato_adicional
           where d.id_documento=p_idocumento
                and v.expediente=p_idexpediente
                and v.numero=p_numerodoc
                and v.version=p_version;

  return cursor_dato;
  close cursor_dato;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_valor_dato_adicional(p_idocumento integer, p_idexpediente character varying, p_numerodoc integer, p_version integer) OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 20970)
-- Name: f_buscar_valor_datoadicional(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_valor_datoadicional(p_idindicedato integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice_adicional
             from lista_desplegables l inner join dato_adicional a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idindicedato
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_valor_datoadicional(p_idindicedato integer) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 20971)
-- Name: f_comprobar_foto_ficha(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_foto_ficha(p_idtipodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_foto refcursor;
  begin

    open cursor_foto for
       select ficha from tipodocumento where id_documento=p_idtipodocumento;

    return cursor_foto;
    close cursor_foto;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_foto_ficha(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 20972)
-- Name: f_comprobar_nombre_archivo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_nombre_archivo(p_idinfodocumento integer, p_idexpediente character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
  DECLARE
    nombre varchar(255);
  begin

   nombre := null;
   
   select nombre_archivo into nombre from infodocumento
   where id_infodocumento= p_idinfodocumento and id_expediente=p_idexpediente;

   return nombre;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_nombre_archivo(p_idinfodocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 20973)
-- Name: f_comprobar_numero_documento(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_numero_documento(p_iddocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_numdoc refcursor;
  begin

   open cursor_numdoc for
   select numero_documento  from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente
   order by numero_documento;

   return cursor_numdoc;
   close cursor_numdoc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_numero_documento(p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 20974)
-- Name: f_crear_sesion(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_crear_sesion(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
       select distinct r.rol desc_rol, e.estatus estatus_usuario, u.id_usuario id_user,
              u.nombre, u.apellido, u.cedula, u.id_estatus
           from usuario u inner join estatus e on u.id_estatus=e.id_estatus
                inner join perfil p on p.id_usuario=u.id_usuario
                inner join rol r on r.id_rol=p.id_rol
           where u.id_usuario=p_usuario;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_crear_sesion(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 20975)
-- Name: f_eliminar_archivo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_eliminar_archivo(p_idinfodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

    cursor_eliminar refcursor;
  begin

   open cursor_eliminar for
        select t.ruta_archivo, t.nombre_archivo from infodocumento t where t.id_infodocumento=p_idinfodocumento;

    return cursor_eliminar;
    close cursor_eliminar;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_eliminar_archivo(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 20976)
-- Name: f_foliatura_buscar_expediente(character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_foliatura_buscar_expediente(p_idexpediente character varying, p_idlibreria integer, p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_folio refcursor;
  begin
  
    open cursor_folio for
        select i.id_expediente, l.id_libreria, c.id_categoria, s.id_subcategoria,
               t.id_documento, i.id_infodocumento, t.tipo_documento as documento,
               i.paginas as cantidadPAginas 
     from infodocumento i 
         inner join tipodocumento t on i.id_documento=t.id_documento
         inner join subcategoria s on s.id_subcategoria=t.id_subcategoria
         inner join categoria c on c.id_categoria=t.id_categoria
         inner join libreria l on l.id_libreria=c.id_libreria
        where i.id_expediente=p_idexpediente and i.estatus_documento=1 and
              l.id_libreria=p_idlibreria and c.id_categoria=p_idcategoria
        order by t.id_documento, i.numero_documento desc, i.version desc;

    return cursor_folio;
    close cursor_folio;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_foliatura_buscar_expediente(p_idexpediente character varying, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 20977)
-- Name: f_informacion_tabla(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_informacion_tabla(p_tabla character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_tabla refcursor;
  begin

    open cursor_tabla for
      SELECT s.OWNER, s.TABLE_NAME, s.COLUMN_NAME, s.DATA_TYPE
          FROM all_tab_columns s
      WHERE
      --owner = 'GESTORDOCUMENTAL' and
      table_name=p_tabla;
      return cursor_tabla;
      close cursor_tabla;
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_informacion_tabla(p_tabla character varying) OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 20978)
-- Name: f_modificar_indices(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_modificar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_flag character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    curor_indice refcursor;
  begin
    
    if p_flag = '0' then
    
      open curor_indice for
         select expediente from expedientes where expediente=p_idexpedientenuevo;
      return curor_indice;
      close curor_indice;
      
    elsif p_flag = '1' then
      
      delete from expedientes where expediente=p_idexpedienteviejo;

      update infodocumento set id_expediente=p_idexpedientenuevo where id_expediente=p_idexpedienteviejo;
      return curor_indice;
    end if;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_modificar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_flag character varying) OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 20979)
-- Name: f_usuarios(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_usuarios() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin

    open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.sexo,
              e.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

       return cursor_user;
       close cursor_user;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_usuarios() OWNER TO postgres;

--
-- TOC entry 314 (class 1255 OID 20980)
-- Name: f_verificar_usuario(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_verificar_usuario(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$

  DECLARE
    cursor_user refcursor;
  begin

   if p_usuario is not null then

    open cursor_user for
      select u.id_usuario, u.nombre, u.apellido, u.password passUser, e.id_estatus, e.estatus, c.*
      from usuario u inner join estatus e on u.id_estatus=e.id_estatus,
      configuracion c
      where u.id_usuario = p_usuario;

   else

     open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

   end if;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;
$$;


ALTER FUNCTION public.f_verificar_usuario(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 20981)
-- Name: p_actualiza_codigo_combo(integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_codigo_combo(p_codigocombo integer, p_flag character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    if p_flag = '0' then

      update indices set codigo = p_codigocombo where id_indice = p_codigocombo;

    elsif p_flag = '1' then

      update dato_adicional  set codigo = p_codigocombo where id_dato_adicional = p_codigocombo;

    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_codigo_combo(p_codigocombo integer, p_flag character) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 20982)
-- Name: p_actualiza_nombre_archivo(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_nombre_archivo(p_nombrearchivo character varying, p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update infodocumento set nombre_archivo=p_nombrearchivo
        where id_infodocumento=p_idinfodocumento;
		
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_nombre_archivo(p_nombrearchivo character varying, p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 20983)
-- Name: p_actualiza_numero_da(integer, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_numero_da(p_idvalor integer, p_numerodocumento integer, p_version integer, p_expediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin
  
     update valor_dato_adicional
       set numero = p_numerodocumento
       where id_valor = p_idvalor
         and expediente = p_expediente
         and version=p_version;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_numero_da(p_idvalor integer, p_numerodocumento integer, p_version integer, p_expediente character varying) OWNER TO postgres;

--
-- TOC entry 283 (class 1255 OID 20984)
-- Name: p_actualiza_td_foto(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_td_foto(p_idtipodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update tipodocumento set ficha='1' where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_td_foto(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 284 (class 1255 OID 20985)
-- Name: p_actualizar_categorias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_categorias(p_idcategoria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update categoria set id_estatus=p_idestatus where id_categoria=p_idcategoria;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_categorias(p_idcategoria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 285 (class 1255 OID 20986)
-- Name: p_actualizar_datos_combo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_datos_combo(p_idcombo integer, p_dato character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update lista_desplegables
      set descripcion = p_dato
    where id_lista = p_idcombo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_datos_combo(p_idcombo integer, p_dato character varying) OWNER TO postgres;

--
-- TOC entry 286 (class 1255 OID 20987)
-- Name: p_actualizar_indices(integer, integer, character varying, character varying, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_indices(p_idindices integer, p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin
    update indices
       set id_categoria = p_idcategoria,
           indice = p_indice,
           tipo = p_tipodato,
           codigo = p_codigo,
           clave = p_clave
     where id_indice = p_idindices;
	 
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_indices(p_idindices integer, p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) OWNER TO postgres;

--
-- TOC entry 287 (class 1255 OID 20988)
-- Name: p_actualizar_indices(character varying, character varying, integer, character varying, date, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin

    if (p_idexpedienteviejo = p_idexpedientenuevo) then

      update expedientes
         set valor = p_valor,
             fecha_indice = p_fechaindice
       where id_indice = p_idindice
         and expediente = p_idexpedienteviejo
         and id_libreria = p_idlibreria
         and id_categoria = p_idcategoria;

    else
        insert into expedientes
          (expediente, id_indice, valor, id_libreria, id_categoria)
        values
          (p_idexpedientenuevo, p_idindice, p_valor, p_idlibreria, p_idcategoria);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 288 (class 1255 OID 20989)
-- Name: p_actualizar_infodocumento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_infodocumento(p_numerodoc integer, p_idinfodocumento integer, p_idexpediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update infodocumento set numero_documento=p_numerodoc
    where id_infodocumento=p_idinfodocumento and id_expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_infodocumento(p_numerodoc integer, p_idinfodocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 20990)
-- Name: p_actualizar_librerias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_librerias(p_idlibreria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update libreria set id_estatus=p_idestatus where id_libreria=p_idlibreria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_librerias(p_idlibreria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 290 (class 1255 OID 20991)
-- Name: p_actualizar_subcategorias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_subcategorias(p_idsubcategoria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update subcategoria set id_estatus=p_idestatus where id_subcategoria=p_idsubcategoria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_subcategorias(p_idsubcategoria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 291 (class 1255 OID 20992)
-- Name: p_actualizar_tipodocumento(integer, integer, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_tipodocumento(p_idtipodocumento integer, p_idestatus integer, p_vencimiento character, p_datoadicional character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update tipodocumento set id_estatus=p_idestatus, vencimiento=p_vencimiento,
                        dato_adicional=p_datoadicional
   where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_tipodocumento(p_idtipodocumento integer, p_idestatus integer, p_vencimiento character, p_datoadicional character) OWNER TO postgres;

--
-- TOC entry 292 (class 1255 OID 20993)
-- Name: p_agrega_usuario(character varying, character varying, character varying, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agrega_usuario(p_idusuario character varying, p_nombre character varying, p_apellido character varying, p_cedula character varying, p_sexo character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into usuario
      (id_usuario, nombre, apellido, cedula, sexo, id_estatus)
    values
      (p_idusuario, p_nombre, p_apellido, p_cedula, p_sexo, 1);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agrega_usuario(p_idusuario character varying, p_nombre character varying, p_apellido character varying, p_cedula character varying, p_sexo character) OWNER TO postgres;

--
-- TOC entry 293 (class 1255 OID 20994)
-- Name: p_agregar_categoria(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_categoria(p_idlibreria integer, p_categoria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into categoria
      (id_libreria, categoria, id_estatus)
    values
      (p_idlibreria, p_categoria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;  
  end;$$;


ALTER FUNCTION public.p_agregar_categoria(p_idlibreria integer, p_categoria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 294 (class 1255 OID 20995)
-- Name: p_agregar_configuracion(character varying, character varying, integer, character varying, character varying, character, character, character, character, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_configuracion(p_nombreservidor character varying, p_nombrebasedato character varying, p_puertobasedato integer, p_usuariobasedato character varying, p_passbasedato character varying, p_calidad character, p_foliatura character, p_ficha character, p_fabrica character, p_elimina character, p_ldap character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update configuracion
       set calidad = p_calidad,
           foliatura = p_foliatura,
           server_name = p_nombreservidor,
           database_name = p_nombrebasedato,
           port = p_puertobasedato,
           userbd = p_usuariobasedato,
           password = p_passbasedato,
           ficha = p_ficha,
           fabrica = p_fabrica,
           elimina = p_elimina,
           ldap = p_ldap
     where id_configuracion=1;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;
$$;


ALTER FUNCTION public.p_agregar_configuracion(p_nombreservidor character varying, p_nombrebasedato character varying, p_puertobasedato integer, p_usuariobasedato character varying, p_passbasedato character varying, p_calidad character, p_foliatura character, p_ficha character, p_fabrica character, p_elimina character, p_ldap character) OWNER TO postgres;

--
-- TOC entry 295 (class 1255 OID 20996)
-- Name: p_agregar_datos_combo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_datos_combo(p_codigoindice integer, p_dato character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_codigoindice, p_dato);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_datos_combo(p_codigoindice integer, p_dato character varying) OWNER TO postgres;

--
-- TOC entry 296 (class 1255 OID 20997)
-- Name: p_agregar_datos_combo_da(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_datos_combo_da(p_iddatoadiciona integer, p_datocombo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin


    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_iddatoadiciona, p_datocombo);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_datos_combo_da(p_iddatoadiciona integer, p_datocombo character varying) OWNER TO postgres;

--
-- TOC entry 297 (class 1255 OID 20998)
-- Name: p_agregar_fabrica(character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_fabrica(p_idusuario character varying, p_pertenece character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into fabrica(usuario, fabrica)
           values(p_idusuario, p_pertenece);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;	   
  end;$$;


ALTER FUNCTION public.p_agregar_fabrica(p_idusuario character varying, p_pertenece character) OWNER TO postgres;

--
-- TOC entry 298 (class 1255 OID 20999)
-- Name: p_agregar_foliaturas(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_foliaturas(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying, p_pagina integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into foliatura
      (id_infodocumento, id_documento, id_expediente, pagina)
    values
      (p_idinfodocumento, p_iddocumento, p_idexpediente, p_pagina);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_foliaturas(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying, p_pagina integer) OWNER TO postgres;

--
-- TOC entry 299 (class 1255 OID 21000)
-- Name: p_agregar_indices(integer, character varying, character varying, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_indices(p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into indices
      (id_categoria, indice, tipo, codigo, clave)
    values
      (p_idcategoria, p_indice, p_tipodato, p_codigo, p_clave);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_indices(p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) OWNER TO postgres;

--
-- TOC entry 300 (class 1255 OID 21001)
-- Name: p_agregar_libreria(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_libreria(p_libreria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into libreria(libreria, id_estatus)
                values(p_libreria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;			
  end;$$;


ALTER FUNCTION public.p_agregar_libreria(p_libreria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 301 (class 1255 OID 21002)
-- Name: p_agregar_perfil(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_perfil(p_idlibreria integer, p_idcategoria integer, p_idusuario character varying, p_idrol integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    if p_idcategoria = 0 and p_idlibreria = 0 then

      insert into perfil(id_usuario, id_rol)
                  values(p_idusuario, p_idrol);

    elsif p_idcategoria = 0 then

      insert into perfil(id_libreria, id_usuario, id_rol)
                  values(p_idlibreria, p_idusuario, p_idrol);
    else

      insert into perfil(id_libreria, id_categoria, id_usuario, id_rol)
                  values(p_idlibreria, p_idcategoria, p_idusuario, p_idrol);
    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_perfil(p_idlibreria integer, p_idcategoria integer, p_idusuario character varying, p_idrol integer) OWNER TO postgres;

--
-- TOC entry 302 (class 1255 OID 21003)
-- Name: p_agregar_registro_archivo(character varying, character varying, integer, character varying, integer, integer, character varying, integer, date, character varying, character varying, integer, integer, character, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_registro_archivo(p_accion character varying, p_nombrearchivo character varying, p_iddocumento integer, p_rutaarchivo character varying, p_cantpaginas integer, p_version integer, p_idexpediente character varying, p_numerodoc integer, p_fechavencimiento date, p_datoadicional character varying, p_usuario character varying, p_idinfodocumento integer, p_estatus integer, p_redigitalizo character, p_formato character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
   p_idinfodoc integer;
  begin

   
   p_idinfodoc := 0;


   if p_accion = 'versionar' then

     insert into infodocumento(id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, 
                               version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado)
                 values(p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo,
                        p_formato, p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus,
                        p_redigitalizo);
	
	 select lastval() into p_idinfodoc;

     if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion, usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, now(), p_usuario, p_datoadicional);
      else
        rollback;
      end if;

    elsif p_accion = 'reemplazar' then

      p_idinfodoc := -1;
      update infodocumento set nombre_archivo=p_nombrearchivo, ruta_archivo=p_rutaarchivo, paginas=p_cantpaginas, 
                               version=p_version, id_expediente=p_idexpediente, numero_documento=p_numerodoc, 
                               fecha_vencimiento=p_fechavencimiento, formato=p_formato, estatus_documento=p_estatus
      where id_infodocumento=p_idinfodocumento;

      update datos_infodocumento set fecha_digitalizacion = now(),
                                     usuario_digitalizo = p_usuario,
                                     dato_adicional = p_datoadicional
       where id_infodocumento=p_idinfodocumento;

    elsif p_accion = 'Guardar' then
       insert into infodocumento(id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, 
                                version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado)
                  values(p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo, p_formato, 
			 p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus, 0);
	
	 select lastval() into p_idinfodoc;

      if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion, usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, now(), p_usuario, p_datoadicional);

         if p_redigitalizo = '1' then
           update infodocumento set re_digitalizado=p_redigitalizo
              where id_infodocumento=p_idinfodocumento;
         end if;
      else
        rollback;
      end if;

    end if;

   return p_idinfodoc;
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
   end;$$;


ALTER FUNCTION public.p_agregar_registro_archivo(p_accion character varying, p_nombrearchivo character varying, p_iddocumento integer, p_rutaarchivo character varying, p_cantpaginas integer, p_version integer, p_idexpediente character varying, p_numerodoc integer, p_fechavencimiento date, p_datoadicional character varying, p_usuario character varying, p_idinfodocumento integer, p_estatus integer, p_redigitalizo character, p_formato character varying) OWNER TO postgres;

--
-- TOC entry 303 (class 1255 OID 21004)
-- Name: p_agregar_subcategoria(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_subcategoria(p_idcategoria integer, p_subcategoria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into subcategoria
      (id_categoria, subcategoria, id_estatus)
    values
      (p_idcategoria, p_subcategoria, p_idestatus);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_subcategoria(p_idcategoria integer, p_subcategoria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 304 (class 1255 OID 21005)
-- Name: p_agregar_tipodocumento(integer, integer, character varying, integer, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_tipodocumento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumento character varying, p_idestatus integer, p_vencimiento character, p_datoadicional character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into tipodocumento
      (id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional)
    values
      (p_idcategoria, p_idsubcategoria, p_tipodocumento, p_idestatus, p_vencimiento, p_datoadicional);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_tipodocumento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumento character varying, p_idestatus integer, p_vencimiento character, p_datoadicional character) OWNER TO postgres;

--
-- TOC entry 305 (class 1255 OID 21006)
-- Name: p_aprobar_documento(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_aprobar_documento(p_idinfodocumento integer, p_usuario character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
    estatu integer;
    idInfoDoc integer;
begin

    update infodocumento set estatus_documento=1 where id_infodocumento=p_idinfodocumento;
    --commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;

    if estatu > 0 then
    
      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;
    
      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_aprobacion = now(),
               usuario_aprobacion = p_usuario
         where id_infodocumento = p_idinfodocumento;
      else
        insert into datos_infodocumento(id_infodocumento, fecha_aprobacion, usuario_aprobacion)
                  values(p_idinfodocumento, now(), p_usuario);
      end if;
      
    else
    
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      --commit;
      
    end if;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %', SQLERRM;
 end;$$;


ALTER FUNCTION public.p_aprobar_documento(p_idinfodocumento integer, p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 306 (class 1255 OID 21007)
-- Name: p_eliminar_expediente(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_expediente(p_idexpediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    delete from expedientes where expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_expediente(p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 307 (class 1255 OID 21008)
-- Name: p_eliminar_infodocumento(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_infodocumento(p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  
  begin

    
    delete from datos_infodocumento where id_infodocumento = p_idinfodocumento;
    delete from infodocumento where id_infodocumento = p_idinfodocumento;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_infodocumento(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 308 (class 1255 OID 21009)
-- Name: p_eliminar_perfil(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_perfil(p_idusuario character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   delete from perfil where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_perfil(p_idusuario character varying) OWNER TO postgres;

--
-- TOC entry 309 (class 1255 OID 21010)
-- Name: p_eliminar_registro_archivo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_registro_archivo(p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   delete from datos_infodocumento where id_infodocumento=p_idinfodocumento;
   delete from infodocumento where id_infodocumento=p_idinfodocumento;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_registro_archivo(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 310 (class 1255 OID 21011)
-- Name: p_eliminar_valordatadic(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_valordatadic(p_idvalor integer, p_versiondoc integer, p_numerodoc integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin
  
    delete from valor_dato_adicional
     where id_valor = p_idvalor and version=p_versiondoc and numero=p_numerodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_valordatadic(p_idvalor integer, p_versiondoc integer, p_numerodoc integer) OWNER TO postgres;

--
-- TOC entry 311 (class 1255 OID 21012)
-- Name: p_guarda_valor_dato_adicional(integer, integer, character varying, integer, integer, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guarda_valor_dato_adicional(p_iddatoadicional integer, p_idvalor integer, p_valor character varying, p_numerodocumento integer, p_version integer, p_expediente character varying, p_flag character) RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin


    if p_flag = '0' then

      update valor_dato_adicional
         set valor = p_valor
       where id_valor = p_idvalor
         and numero = p_numerodocumento
         and version = p_version
         and expediente = p_expediente;

    elsif p_flag = '1' then

     insert into valor_dato_adicional
        (id_dato_adicional, valor, numero, version, expediente)
      values
        (p_iddatoadicional, p_valor, p_numerodocumento, p_version, p_expediente);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guarda_valor_dato_adicional(p_iddatoadicional integer, p_idvalor integer, p_valor character varying, p_numerodocumento integer, p_version integer, p_expediente character varying, p_flag character) OWNER TO postgres;

--
-- TOC entry 225 (class 1255 OID 21013)
-- Name: p_guardar_expediente(character varying, integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into expedientes
      (expediente, id_indice, valor, id_libreria, id_categoria)
    values
      (p_idexpediente, p_idindice, p_valor, p_idlibreria, p_idcategoria);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 21014)
-- Name: p_guardar_expediente(character varying, integer, character varying, date, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into expedientes
      (expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria)
    values
      (p_idexpediente, p_idindice, p_valor, p_fechaindice, p_idlibreria, p_idcategoria);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 226 (class 1255 OID 21015)
-- Name: p_guardar_indice_datoadicional(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_indice_datoadicional(p_idtipodocumento integer, p_datoadicional character varying, p_tipo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into dato_adicional
      (indice_adicional, tipo, id_documento)
    values
      (p_datoadicional, p_tipo, p_idtipodocumento);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_indice_datoadicional(p_idtipodocumento integer, p_datoadicional character varying, p_tipo character varying) OWNER TO postgres;

--
-- TOC entry 241 (class 1255 OID 21016)
-- Name: p_modificar_usuario(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_modificar_usuario(p_idusuario character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update usuario set id_estatus=p_idestatus where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_modificar_usuario(p_idusuario character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 312 (class 1255 OID 21017)
-- Name: p_rechazar_documento(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_rechazar_documento(p_idinfodocumento integer, p_usuario character varying, p_causa character varying, p_motivo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
    estatu integer;
    idInfoDoc integer;
  begin

    update infodocumento set estatus_documento=2 where id_infodocumento= p_idinfodocumento;
    --commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;


    if estatu > 0 then

      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;

      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_rechazo = now(),
               usuario_rechazo = p_usuario,
               motivo_rechazo = p_motivo,
               causa_rechazo = p_causa
         where id_infodocumento = p_idinfodocumento;
      else

        insert into datos_infodocumento(id_infodocumento, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo)
                  values(p_idinfodocumento, now(), p_usuario, p_motivo, p_causa);
      end if;
    else
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      --commit;

    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %', SQLERRM;
  end;$$;


ALTER FUNCTION public.p_rechazar_documento(p_idinfodocumento integer, p_usuario character varying, p_causa character varying, p_motivo character varying) OWNER TO postgres;

--
-- TOC entry 313 (class 1255 OID 21018)
-- Name: p_traza_elimina_documento(character varying, integer, integer, integer, integer, integer, date, date, date, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_traza_elimina_documento(p_idexpedeinte character varying, p_idlibreria integer, p_idcategoria integer, p_idsubcategoria integer, p_iddocumento integer, p_numerodoc integer, p_fechavencimiento date, p_fechadigitalizo date, p_fecharechazo date, p_cantpaginas integer, p_versiondoc integer, p_usuarioelimino character varying, p_usuariodigitalizo character varying, p_usuariorechazo character varying, p_causaelimino character varying, p_motivoelimino character varying, p_causarechazo character varying, p_motivorechazo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
  
    iddoceliminado integer;

  begin

    insert into documento_eliminado
      (id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino)
    values
      (p_idexpedeinte, p_idlibreria, p_idcategoria, p_idsubcategoria, p_iddocumento, p_numerodoc, p_versiondoc, p_cantpaginas, p_fechavencimiento, now(), p_usuarioelimino);

    select lastval() into iddoceliminado;

    if iddoceliminado > 0 then

	 insert into datos_infodocumento
	(id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino)
      values
        (iddoceliminado, p_fechadigitalizo, p_usuariodigitalizo, p_fecharechazo, p_usuariorechazo, p_motivorechazo, p_causarechazo, now(), p_usuarioelimino, p_motivoelimino, p_causaelimino);
 
    else
      rollback;
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_traza_elimina_documento(p_idexpedeinte character varying, p_idlibreria integer, p_idcategoria integer, p_idsubcategoria integer, p_iddocumento integer, p_numerodoc integer, p_fechavencimiento date, p_fechadigitalizo date, p_fecharechazo date, p_cantpaginas integer, p_versiondoc integer, p_usuarioelimino character varying, p_usuariodigitalizo character varying, p_usuariorechazo character varying, p_causaelimino character varying, p_motivoelimino character varying, p_causarechazo character varying, p_motivorechazo character varying) OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 21019)
-- Name: sq_categroria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_categroria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_categroria OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 21021)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categoria (
    id_categoria integer DEFAULT nextval('sq_categroria'::regclass) NOT NULL,
    id_libreria integer NOT NULL,
    categoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE categoria OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 21025)
-- Name: causa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE causa (
    id_causa integer NOT NULL,
    causa character varying(150) NOT NULL
);


ALTER TABLE causa OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 21028)
-- Name: configuracion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE configuracion (
    id_configuracion integer NOT NULL,
    calidad character(1),
    ruta_temporal character varying(50),
    archivo_tif character varying(50),
    archivo_cod character varying(50),
    log character varying(50),
    foliatura character(1),
    server_name character varying(50),
    database_name character varying(50),
    port integer,
    userbd character varying(50),
    password character varying(50),
    ficha character(1),
    fabrica character(1),
    elimina character(1),
    ldap character(1)
);


ALTER TABLE configuracion OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 21031)
-- Name: sq_dato_adicional; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_dato_adicional
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_dato_adicional OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 21033)
-- Name: dato_adicional; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE dato_adicional (
    id_dato_adicional integer DEFAULT nextval('sq_dato_adicional'::regclass) NOT NULL,
    indice_adicional character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    id_documento integer NOT NULL,
    codigo integer
);


ALTER TABLE dato_adicional OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 21037)
-- Name: sq_datos_infodocumento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_datos_infodocumento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_datos_infodocumento OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 21039)
-- Name: datos_infodocumento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE datos_infodocumento (
    id_datos integer DEFAULT nextval('sq_datos_infodocumento'::regclass) NOT NULL,
    id_infodocumento integer,
    id_doc_eliminado integer,
    fecha_digitalizacion date,
    usuario_digitalizo character varying(30),
    fecha_aprobacion date,
    usuario_aprobacion character varying(30),
    fecha_rechazo date,
    usuario_rechazo character varying(30),
    motivo_rechazo character varying(300),
    causa_rechazo character varying(300),
    fecha_eliminado date,
    usuario_elimino character varying(30),
    motivo_elimino character varying(300),
    causa_elimino character varying(300),
    dato_adicional character varying(30)
);


ALTER TABLE datos_infodocumento OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 21046)
-- Name: sq_documento_eliminado; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_documento_eliminado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_documento_eliminado OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 21048)
-- Name: documento_eliminado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE documento_eliminado (
    id_doc_eliminado integer DEFAULT nextval('sq_documento_eliminado'::regclass) NOT NULL,
    id_expediente character varying(50) NOT NULL,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    id_documento integer NOT NULL,
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    fecha_eliminado date NOT NULL,
    usuario_elimino character varying(30) NOT NULL
);


ALTER TABLE documento_eliminado OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 21052)
-- Name: estatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE estatus (
    id_estatus integer NOT NULL,
    estatus character varying(100) NOT NULL
);


ALTER TABLE estatus OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 21055)
-- Name: estatus_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE estatus_documento (
    id_estatus_documento integer NOT NULL,
    estatus_documento character varying(20) NOT NULL
);


ALTER TABLE estatus_documento OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 21058)
-- Name: sq_expediente; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_expediente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_expediente OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 21060)
-- Name: expedientes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expedientes (
    id_expedientes integer DEFAULT nextval('sq_expediente'::regclass) NOT NULL,
    expediente character varying(250) NOT NULL,
    id_indice integer NOT NULL,
    valor character varying(250),
    fecha_indice date,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL
);


ALTER TABLE expedientes OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 21067)
-- Name: fabrica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE fabrica (
    usuario character varying(50) NOT NULL,
    fabrica character(1) NOT NULL
);


ALTER TABLE fabrica OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 21070)
-- Name: sq_foliatura; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_foliatura
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_foliatura OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 21072)
-- Name: foliatura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE foliatura (
    id_foliatura integer DEFAULT nextval('sq_foliatura'::regclass) NOT NULL,
    id_infodocumento integer NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50) NOT NULL,
    pagina integer NOT NULL
);


ALTER TABLE foliatura OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 21076)
-- Name: sq_indices; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_indices
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_indices OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 21078)
-- Name: indices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE indices (
    id_indice integer DEFAULT nextval('sq_indices'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    indice character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    codigo integer NOT NULL,
    clave character(1)
);


ALTER TABLE indices OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 21082)
-- Name: sq_infodocumento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_infodocumento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_infodocumento OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 21084)
-- Name: infodocumento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE infodocumento (
    id_infodocumento integer DEFAULT nextval('sq_infodocumento'::regclass) NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50),
    nombre_archivo character varying(1000),
    ruta_archivo character varying(1000),
    formato character varying(4),
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    estatus_documento integer NOT NULL,
    re_digitalizado character(1) NOT NULL
);


ALTER TABLE infodocumento OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 21091)
-- Name: sq_libreria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_libreria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_libreria OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 21093)
-- Name: libreria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE libreria (
    id_libreria integer DEFAULT nextval('sq_libreria'::regclass) NOT NULL,
    libreria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE libreria OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 21097)
-- Name: sq_combo; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_combo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_combo OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 21099)
-- Name: lista_desplegables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE lista_desplegables (
    id_lista integer DEFAULT nextval('sq_combo'::regclass) NOT NULL,
    codigo_indice integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE lista_desplegables OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 21103)
-- Name: sq_perfil; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_perfil
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_perfil OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 21105)
-- Name: perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE perfil (
    id_perfil integer DEFAULT nextval('sq_perfil'::regclass) NOT NULL,
    id_libreria integer,
    id_categoria integer,
    id_usuario character varying(50) NOT NULL,
    id_rol integer NOT NULL
);


ALTER TABLE perfil OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 21109)
-- Name: reporte; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reporte (
    id_categoria integer,
    expediente character varying(250),
    numero_de_solicitud_u_oficio character varying(250),
    cedula_de_identidad_de_empleado character varying(250),
    apellidos_y_nombres_de_empleado character varying(250),
    organismo_principal character varying(250)
);


ALTER TABLE reporte OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 21115)
-- Name: rol; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE rol (
    id_rol integer NOT NULL,
    rol character varying(50) NOT NULL
);


ALTER TABLE rol OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 21118)
-- Name: sq_subcategroria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_subcategroria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_subcategroria OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 21120)
-- Name: sq_tipo_documento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_tipo_documento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_tipo_documento OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 21122)
-- Name: sq_valor_dato_adicional; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_valor_dato_adicional
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_valor_dato_adicional OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 21124)
-- Name: subcategoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE subcategoria (
    id_subcategoria integer DEFAULT nextval('sq_subcategroria'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    subcategoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE subcategoria OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 21128)
-- Name: tipodocumento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tipodocumento (
    id_documento integer DEFAULT nextval('sq_tipo_documento'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    tipo_documento character varying(200) NOT NULL,
    id_estatus integer NOT NULL,
    vencimiento character(1),
    dato_adicional character(1),
    ficha character(1)
);


ALTER TABLE tipodocumento OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 21132)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuario (
    id_usuario character varying(50) NOT NULL,
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    cedula character varying(50),
    sexo character(1),
    id_estatus integer NOT NULL,
    password character varying(16)
);


ALTER TABLE usuario OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 21135)
-- Name: valor_dato_adicional; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE valor_dato_adicional (
    id_valor integer DEFAULT nextval('sq_valor_dato_adicional'::regclass) NOT NULL,
    id_dato_adicional integer NOT NULL,
    valor character varying(250) NOT NULL,
    numero integer NOT NULL,
    version integer NOT NULL,
    expediente character varying(250) NOT NULL
);


ALTER TABLE valor_dato_adicional OWNER TO postgres;

--
-- TOC entry 2398 (class 0 OID 21021)
-- Dependencies: 186
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categoria VALUES (21, 21, '01.-Expedientes de Resumen de Conformidad', 1);
INSERT INTO categoria VALUES (41, 41, '01.-  EXPEDIENTES  DE  DRAW  BACK', 1);
INSERT INTO categoria VALUES (61, 61, 'CATEGORÍA:01.- PRUEBA - EXPEDIENTES DE AGENTES DEADUANA  (AA)', 1);
INSERT INTO categoria VALUES (81, 61, 'CATEGORÍA:02.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS PERSONAS NATURALESBAJO RELACIÓN DE DEPENDENCIA  (AA-PN-BRD)', 1);
INSERT INTO categoria VALUES (101, 61, '03.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS CAPACITADOS ADUANEROS  (AA-CA)', 1);
INSERT INTO categoria VALUES (121, 61, 'CATEGORÍA:03.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS CAPACITADOS ADUANEROS  (AA-CA)', 1);
INSERT INTO categoria VALUES (1, 1, 'CategoriaPrueba', 1);
INSERT INTO categoria VALUES (161, 101, 'CATEGORÍA:01.-  EXPEDIENTES  DE  DRAW  BACK', 1);
INSERT INTO categoria VALUES (162, 101, 'EXPEDIENTES  DE  DRAW  BACK', 2);
INSERT INTO categoria VALUES (163, 103, '01.- 2 PRUEBA - EXPEDIENTES DE AGENTES DE ADUANA (AA)', 1);
INSERT INTO categoria VALUES (164, 104, '+CATEGORÍA:01.- PRUEBA - EXPEDIENTES DE AGENTES DEADUANA  (AA)', 1);
INSERT INTO categoria VALUES (165, 105, '01.- PRUEBA - EXPEDIENTES DE AGENTES DEADUANA  (AA)', 1);
INSERT INTO categoria VALUES (166, 105, '02.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS PERSONAS NATURALESBAJO RELACIÓN DE DEPENDENCIA  (AA-PN-BRD)', 1);
INSERT INTO categoria VALUES (141, 1, 'CategoriaPrueba2', 1);
INSERT INTO categoria VALUES (167, 21, '02.- Expediente de Draw Back', 1);
INSERT INTO categoria VALUES (168, 1, 'CategoriaPrueba3', 2);
INSERT INTO categoria VALUES (2, 2, '0003-MPPD-PRUEBA (MPPDEFENSA)', 1);
INSERT INTO categoria VALUES (3, 2, '0013-0003-MPPRIJP-SAREN-PRUEBA (SAREN)', 1);
INSERT INTO categoria VALUES (4, 2, '0000-H-FDPS (EXPEDIENTES HISTORICOS DE PAGO DEL FDPS)', 1);
INSERT INTO categoria VALUES (5, 2, '0121-GEAPU-PRUEBA (GOB APURE)', 1);


--
-- TOC entry 2399 (class 0 OID 21025)
-- Dependencies: 187
-- Data for Name: causa; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO causa VALUES (1, 'Mala Calidad en la Imagen');
INSERT INTO causa VALUES (2, 'Mala tipificación del Documento');
INSERT INTO causa VALUES (3, 'Los Índices no concuerdan con el Documento');
INSERT INTO causa VALUES (4, 'Mala Orientación del Documento');
INSERT INTO causa VALUES (5, 'Visualización Nula del Documento');
INSERT INTO causa VALUES (6, 'Falla Técnica del Sistema');
INSERT INTO causa VALUES (7, 'Eliminacion de Documento');


--
-- TOC entry 2400 (class 0 OID 21028)
-- Dependencies: 188
-- Data for Name: configuracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO configuracion VALUES (1, '1', 'temp', 'documento.tiff', 'codificado.cod', '/lib/log4j-config.properties', '0', '192.168.1.4', 'dw4j', 5432, 'cG9zdGdyZXM=', 'MTA2NjgxNTU=', '0', '0', '1', '0');


--
-- TOC entry 2402 (class 0 OID 21033)
-- Dependencies: 190
-- Data for Name: dato_adicional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO dato_adicional VALUES (1, 'Dato Texto', 'TEXTO', 1, NULL);
INSERT INTO dato_adicional VALUES (2, 'Dato Numero', 'NUMERO', 1, NULL);
INSERT INTO dato_adicional VALUES (3, 'Dato Fecha', 'FECHA', 1, NULL);
INSERT INTO dato_adicional VALUES (5, 'Dato Area', 'AREA', 1, NULL);
INSERT INTO dato_adicional VALUES (21, 'Fecha de Recepción', 'FECHA', 61, NULL);
INSERT INTO dato_adicional VALUES (41, 'Fecha de Recepción', 'FECHA', 62, NULL);
INSERT INTO dato_adicional VALUES (61, 'Fecha de Declaración', 'FECHA', 63, NULL);
INSERT INTO dato_adicional VALUES (81, 'Fecha de Emisión', 'FECHA', 64, NULL);
INSERT INTO dato_adicional VALUES (101, 'Fecha de Emisión', 'FECHA', 65, NULL);
INSERT INTO dato_adicional VALUES (121, 'Fecha de Emisión', 'FECHA', 66, NULL);
INSERT INTO dato_adicional VALUES (141, 'Fecha de Emisión', 'FECHA', 67, NULL);
INSERT INTO dato_adicional VALUES (161, 'Fecha de Notificación', 'FECHA', 68, NULL);
INSERT INTO dato_adicional VALUES (181, 'Fecha de Recepción', 'FECHA', 69, NULL);
INSERT INTO dato_adicional VALUES (201, 'Fecha de Recepción', 'FECHA', 70, NULL);
INSERT INTO dato_adicional VALUES (221, 'Fecha de Recepción', 'FECHA', 71, NULL);
INSERT INTO dato_adicional VALUES (241, 'Fecha de Recepción', 'FECHA', 72, NULL);
INSERT INTO dato_adicional VALUES (261, 'Fecha de Recepción', 'FECHA', 73, NULL);
INSERT INTO dato_adicional VALUES (281, 'Fecha de Recepción', 'FECHA', 74, NULL);
INSERT INTO dato_adicional VALUES (301, 'Fecha de Notificación', 'FECHA', 75, NULL);
INSERT INTO dato_adicional VALUES (321, 'Fecha de Recepción', 'FECHA', 76, NULL);
INSERT INTO dato_adicional VALUES (341, 'Fecha de Emisión', 'FECHA', 77, NULL);
INSERT INTO dato_adicional VALUES (361, 'Descripción', 'TEXTO', 78, NULL);
INSERT INTO dato_adicional VALUES (381, 'Número', 'NUMERO', 101, NULL);
INSERT INTO dato_adicional VALUES (401, 'Fecha', 'FECHA', 121, NULL);
INSERT INTO dato_adicional VALUES (402, 'Número', 'NUMERO', 121, NULL);
INSERT INTO dato_adicional VALUES (421, 'Fecha', 'FECHA', 141, NULL);
INSERT INTO dato_adicional VALUES (422, 'Número', 'NUMERO', 141, NULL);
INSERT INTO dato_adicional VALUES (441, 'Número', 'NUMERO', 202, NULL);
INSERT INTO dato_adicional VALUES (481, 'Número', 'FECHA', 204, NULL);
INSERT INTO dato_adicional VALUES (501, 'Observaciones', 'AREA', 205, NULL);
INSERT INTO dato_adicional VALUES (521, 'Número', 'FECHA', 221, NULL);
INSERT INTO dato_adicional VALUES (522, 'Ano', 'NUMERO', 221, NULL);
INSERT INTO dato_adicional VALUES (541, 'Número', 'NUMERO', 222, NULL);
INSERT INTO dato_adicional VALUES (561, 'Fecha', 'FECHA', 223, NULL);
INSERT INTO dato_adicional VALUES (562, 'Número', 'NUMERO', 223, NULL);
INSERT INTO dato_adicional VALUES (581, 'Fecha', 'FECHA', 224, NULL);
INSERT INTO dato_adicional VALUES (582, 'Número', 'NUMERO', 224, NULL);
INSERT INTO dato_adicional VALUES (601, 'Número', 'NUMERO', 242, NULL);
INSERT INTO dato_adicional VALUES (621, 'Número', 'NUMERO', 243, NULL);
INSERT INTO dato_adicional VALUES (641, 'Observaciones', 'AREA', 244, NULL);
INSERT INTO dato_adicional VALUES (661, 'Número', 'FECHA', 261, NULL);
INSERT INTO dato_adicional VALUES (662, 'Ano', 'NUMERO', 261, NULL);
INSERT INTO dato_adicional VALUES (681, 'Número', 'NUMERO', 262, NULL);
INSERT INTO dato_adicional VALUES (701, 'Número', 'NUMERO', 264, NULL);
INSERT INTO dato_adicional VALUES (721, 'Número', 'NUMERO', 265, NULL);
INSERT INTO dato_adicional VALUES (741, 'Observaciones', 'AREA', 266, NULL);
INSERT INTO dato_adicional VALUES (761, 'Aduana', 'FECHA', 281, NULL);
INSERT INTO dato_adicional VALUES (762, 'Número', 'NUMERO', 281, NULL);
INSERT INTO dato_adicional VALUES (781, 'Aduana', 'FECHA', 282, NULL);
INSERT INTO dato_adicional VALUES (782, 'Número', 'NUMERO', 282, NULL);
INSERT INTO dato_adicional VALUES (801, 'Aduana', 'FECHA', 283, NULL);
INSERT INTO dato_adicional VALUES (802, 'Número', 'NUMERO', 283, NULL);
INSERT INTO dato_adicional VALUES (821, 'Aduana', 'FECHA', 284, NULL);
INSERT INTO dato_adicional VALUES (822, 'Número', 'NUMERO', 284, NULL);
INSERT INTO dato_adicional VALUES (841, 'Aduana', 'FECHA', 285, NULL);
INSERT INTO dato_adicional VALUES (842, 'Número', 'NUMERO', 285, NULL);
INSERT INTO dato_adicional VALUES (861, 'Aduana', 'FECHA', 286, NULL);
INSERT INTO dato_adicional VALUES (862, 'Número', 'NUMERO', 286, NULL);
INSERT INTO dato_adicional VALUES (881, 'Observaciones', 'AREA', 287, NULL);
INSERT INTO dato_adicional VALUES (382, 'Ano', 'COMBO', 101, 382);
INSERT INTO dato_adicional VALUES (403, 'Ano', 'COMBO', 121, 403);
INSERT INTO dato_adicional VALUES (423, 'Ano', 'COMBO', 141, 423);
INSERT INTO dato_adicional VALUES (442, 'Ano', 'COMBO', 202, 442);
INSERT INTO dato_adicional VALUES (461, 'Número', 'COMBO', 203, 461);
INSERT INTO dato_adicional VALUES (462, 'Ano', 'COMBO', 203, 462);
INSERT INTO dato_adicional VALUES (482, 'Ano', 'COMBO', 204, 482);
INSERT INTO dato_adicional VALUES (483, 'Aduana', 'COMBO', 204, 483);
INSERT INTO dato_adicional VALUES (523, 'Aduana', 'COMBO', 221, 523);
INSERT INTO dato_adicional VALUES (542, 'Ano', 'COMBO', 222, 542);
INSERT INTO dato_adicional VALUES (563, 'Ano', 'COMBO', 223, 563);
INSERT INTO dato_adicional VALUES (583, 'Ano', 'COMBO', 224, 583);
INSERT INTO dato_adicional VALUES (602, 'Ano', 'COMBO', 242, 602);
INSERT INTO dato_adicional VALUES (622, 'Ano', 'COMBO', 243, 622);
INSERT INTO dato_adicional VALUES (663, 'Aduana', 'COMBO', 261, 663);
INSERT INTO dato_adicional VALUES (682, 'Ano', 'COMBO', 262, 682);
INSERT INTO dato_adicional VALUES (702, 'Ano', 'COMBO', 264, 702);
INSERT INTO dato_adicional VALUES (722, 'Ano', 'COMBO', 265, 722);
INSERT INTO dato_adicional VALUES (763, 'Ano', 'COMBO', 281, 763);
INSERT INTO dato_adicional VALUES (783, 'Ano', 'COMBO', 282, 783);
INSERT INTO dato_adicional VALUES (803, 'Ano', 'COMBO', 283, 803);
INSERT INTO dato_adicional VALUES (823, 'Ano', 'COMBO', 284, 823);
INSERT INTO dato_adicional VALUES (843, 'Ano', 'COMBO', 285, 843);
INSERT INTO dato_adicional VALUES (863, 'Ano', 'COMBO', 286, 863);
INSERT INTO dato_adicional VALUES (901, '.-Aduana', 'COMBO', 281, 901);
INSERT INTO dato_adicional VALUES (921, '.-Aduana', 'COMBO', 282, 921);
INSERT INTO dato_adicional VALUES (941, '.-Aduana', 'COMBO', 283, 941);
INSERT INTO dato_adicional VALUES (961, '.-Aduana', 'COMBO', 284, 961);
INSERT INTO dato_adicional VALUES (981, '.-Aduana', 'COMBO', 285, 981);
INSERT INTO dato_adicional VALUES (1001, '.-Aduana', 'COMBO', 286, 1001);
INSERT INTO dato_adicional VALUES (1021, 'Número', 'NUMERO', 321, NULL);
INSERT INTO dato_adicional VALUES (1041, 'Número', 'NUMERO', 325, NULL);
INSERT INTO dato_adicional VALUES (1061, 'Número', 'NUMERO', 326, NULL);
INSERT INTO dato_adicional VALUES (1081, 'Número', 'NUMERO', 327, NULL);
INSERT INTO dato_adicional VALUES (1101, 'Observaciones', 'AREA', 329, NULL);
INSERT INTO dato_adicional VALUES (1121, 'Número RIF', 'TEXTO', 341, NULL);
INSERT INTO dato_adicional VALUES (1122, 'Fecha Incorporación', 'FECHA', 341, NULL);
INSERT INTO dato_adicional VALUES (1141, 'Número RIF', 'TEXTO', 361, NULL);
INSERT INTO dato_adicional VALUES (1142, 'Fecha Incorporación', 'FECHA', 361, NULL);
INSERT INTO dato_adicional VALUES (1161, 'Número RIF', 'TEXTO', 381, NULL);
INSERT INTO dato_adicional VALUES (1162, 'Fecha Desincorporación', 'FECHA', 381, NULL);
INSERT INTO dato_adicional VALUES (1022, 'Ano', 'COMBO', 321, 1022);
INSERT INTO dato_adicional VALUES (1042, 'Ano', 'COMBO', 325, 1042);
INSERT INTO dato_adicional VALUES (1062, 'Ano', 'COMBO', 326, 1062);
INSERT INTO dato_adicional VALUES (1082, 'Ano', 'COMBO', 327, 1082);
INSERT INTO dato_adicional VALUES (1083, 'Aduana', 'COMBO', 327, 1083);
INSERT INTO dato_adicional VALUES (1123, 'Aduana', 'COMBO', 341, 1123);
INSERT INTO dato_adicional VALUES (1143, 'Aduana', 'COMBO', 361, 1143);
INSERT INTO dato_adicional VALUES (1163, 'Aduana', 'COMBO', 381, 1163);
INSERT INTO dato_adicional VALUES (1181, 'Número RIF', 'TEXTO', 421, NULL);
INSERT INTO dato_adicional VALUES (1182, 'Fecha Inclusión', 'FECHA', 421, NULL);
INSERT INTO dato_adicional VALUES (1201, 'Número RIF', 'TEXTO', 441, NULL);
INSERT INTO dato_adicional VALUES (1202, 'Fecha Exclusión', 'FECHA', 441, NULL);
INSERT INTO dato_adicional VALUES (1183, 'Aduana', 'COMBO', 421, 1183);
INSERT INTO dato_adicional VALUES (1203, 'Aduana', 'COMBO', 441, 1203);
INSERT INTO dato_adicional VALUES (1221, 'Fecha de Recepción', 'FECHA', 463, NULL);
INSERT INTO dato_adicional VALUES (1222, 'Fecha de Recepción', 'FECHA', 464, NULL);
INSERT INTO dato_adicional VALUES (1223, 'Fecha de Declaración', 'FECHA', 465, NULL);
INSERT INTO dato_adicional VALUES (1224, 'Fecha de Emisión', 'FECHA', 466, NULL);
INSERT INTO dato_adicional VALUES (1225, 'Fecha de Emisión', 'FECHA', 467, NULL);
INSERT INTO dato_adicional VALUES (1226, 'Fecha de Emisión', 'FECHA', 468, NULL);
INSERT INTO dato_adicional VALUES (1227, 'Fecha de Emisión', 'FECHA', 469, NULL);
INSERT INTO dato_adicional VALUES (1228, 'Fecha de Notificación', 'FECHA', 470, NULL);
INSERT INTO dato_adicional VALUES (1229, 'Fecha de Recepción', 'FECHA', 471, NULL);
INSERT INTO dato_adicional VALUES (1230, 'Fecha de Recepción', 'FECHA', 472, NULL);
INSERT INTO dato_adicional VALUES (1231, 'Fecha de Recepción', 'FECHA', 473, NULL);
INSERT INTO dato_adicional VALUES (1232, 'Fecha de Recepción', 'FECHA', 474, NULL);
INSERT INTO dato_adicional VALUES (1233, 'Fecha de Recepción', 'FECHA', 475, NULL);
INSERT INTO dato_adicional VALUES (1234, 'Fecha de Recepción', 'FECHA', 476, NULL);
INSERT INTO dato_adicional VALUES (1235, 'Fecha de Notificación', 'FECHA', 477, NULL);
INSERT INTO dato_adicional VALUES (1236, 'Fecha de Recepción', 'FECHA', 478, NULL);
INSERT INTO dato_adicional VALUES (1237, 'Fecha de Emisión', 'FECHA', 479, NULL);
INSERT INTO dato_adicional VALUES (1238, 'Descripción', 'TEXTO', 480, NULL);
INSERT INTO dato_adicional VALUES (1239, 'Número', 'TEXTO', 483, NULL);
INSERT INTO dato_adicional VALUES (1240, 'Año', 'TEXTO', 483, NULL);
INSERT INTO dato_adicional VALUES (1241, 'Fecha', 'FECHA', 484, NULL);
INSERT INTO dato_adicional VALUES (1242, 'Número', 'TEXTO', 484, NULL);
INSERT INTO dato_adicional VALUES (1243, 'Año', 'TEXTO', 484, NULL);
INSERT INTO dato_adicional VALUES (1244, 'Fecha', 'FECHA', 485, NULL);
INSERT INTO dato_adicional VALUES (1245, 'Número', 'TEXTO', 485, NULL);
INSERT INTO dato_adicional VALUES (1246, 'Año', 'TEXTO', 485, NULL);
INSERT INTO dato_adicional VALUES (1247, 'Número', 'TEXTO', 489, NULL);
INSERT INTO dato_adicional VALUES (1248, 'Año', 'TEXTO', 489, NULL);
INSERT INTO dato_adicional VALUES (1249, 'Número', 'TEXTO', 490, NULL);
INSERT INTO dato_adicional VALUES (1250, 'Año', 'TEXTO', 490, NULL);
INSERT INTO dato_adicional VALUES (1251, 'Número', 'TEXTO', 491, NULL);
INSERT INTO dato_adicional VALUES (1252, 'Año', 'TEXTO', 491, NULL);
INSERT INTO dato_adicional VALUES (1254, 'Descripción', 'TEXTO', 492, NULL);
INSERT INTO dato_adicional VALUES (1253, 'Aduana', 'COMBO', 491, 1253);
INSERT INTO dato_adicional VALUES (1255, 'NÚMERO', 'NUMERO', 495, NULL);
INSERT INTO dato_adicional VALUES (1256, 'ANO', 'TEXTO', 495, NULL);
INSERT INTO dato_adicional VALUES (1257, 'FECHA', 'FECHA', 496, NULL);
INSERT INTO dato_adicional VALUES (1258, 'NUMERO', 'NUMERO', 496, NULL);
INSERT INTO dato_adicional VALUES (1259, 'ANO', 'TEXTO', 496, NULL);
INSERT INTO dato_adicional VALUES (1260, 'FECHA', 'FECHA', 497, NULL);
INSERT INTO dato_adicional VALUES (1261, 'NUMERO', 'NUMERO', 497, NULL);
INSERT INTO dato_adicional VALUES (1262, 'ANO', 'TEXTO', 497, NULL);
INSERT INTO dato_adicional VALUES (1263, 'NUMERO', 'NUMERO', 501, NULL);
INSERT INTO dato_adicional VALUES (1264, 'ANO', 'TEXTO', 501, NULL);
INSERT INTO dato_adicional VALUES (1265, 'NUMERO', 'NUMERO', 502, NULL);
INSERT INTO dato_adicional VALUES (1266, 'ANO', 'TEXTO', 502, NULL);
INSERT INTO dato_adicional VALUES (1267, 'NUMERO', 'NUMERO', 503, NULL);
INSERT INTO dato_adicional VALUES (1268, 'ANO', 'TEXTO', 503, NULL);
INSERT INTO dato_adicional VALUES (1270, 'OBSERVACIONES', 'AREA', 504, NULL);
INSERT INTO dato_adicional VALUES (1271, 'NUMERO', 'NUMERO', 505, NULL);
INSERT INTO dato_adicional VALUES (1272, 'ANO', 'TEXTO', 505, NULL);
INSERT INTO dato_adicional VALUES (1274, 'NUMERO', 'NUMERO', 506, NULL);
INSERT INTO dato_adicional VALUES (1275, 'ANO', 'TEXTO', 506, NULL);
INSERT INTO dato_adicional VALUES (1276, 'FECHA', 'FECHA', 507, NULL);
INSERT INTO dato_adicional VALUES (1277, 'NUMERO', 'NUMERO', 507, NULL);
INSERT INTO dato_adicional VALUES (1278, 'ANO', 'TEXTO', 507, NULL);
INSERT INTO dato_adicional VALUES (1279, 'FECHA', 'FECHA', 508, NULL);
INSERT INTO dato_adicional VALUES (1280, 'NUMERO', 'NUMERO', 508, NULL);
INSERT INTO dato_adicional VALUES (1281, 'ANO', 'TEXTO', 508, NULL);
INSERT INTO dato_adicional VALUES (1282, 'NUMERO', 'NUMERO', 510, NULL);
INSERT INTO dato_adicional VALUES (1283, 'ANO', 'TEXTO', 510, NULL);
INSERT INTO dato_adicional VALUES (1284, 'NUMERO', 'NUMERO', 511, NULL);
INSERT INTO dato_adicional VALUES (1285, 'ANO', 'TEXTO', 511, NULL);
INSERT INTO dato_adicional VALUES (1286, 'OBSERVACIONES', 'AREA', 512, NULL);
INSERT INTO dato_adicional VALUES (1287, 'NUMERO', 'NUMERO', 513, NULL);
INSERT INTO dato_adicional VALUES (1288, 'ANO', 'TEXTO', 513, NULL);
INSERT INTO dato_adicional VALUES (1290, 'NUMERO', 'NUMERO', 514, NULL);
INSERT INTO dato_adicional VALUES (1291, 'ANO', 'TEXTO', 514, NULL);
INSERT INTO dato_adicional VALUES (1292, 'NUMERO', 'NUMERO', 516, NULL);
INSERT INTO dato_adicional VALUES (1293, 'ANO', 'TEXTO', 516, NULL);
INSERT INTO dato_adicional VALUES (1294, 'NUMERO', 'NUMERO', 517, NULL);
INSERT INTO dato_adicional VALUES (1295, 'ANO', 'TEXTO', 517, NULL);
INSERT INTO dato_adicional VALUES (1296, 'OBERVACIONES', 'AREA', 518, NULL);
INSERT INTO dato_adicional VALUES (1298, 'NUMERO', 'NUMERO', 519, NULL);
INSERT INTO dato_adicional VALUES (1299, 'ANO', 'TEXTO', 519, NULL);
INSERT INTO dato_adicional VALUES (1301, 'NUMERO', 'NUMERO', 520, NULL);
INSERT INTO dato_adicional VALUES (1302, 'ANO', 'TEXTO', 520, NULL);
INSERT INTO dato_adicional VALUES (1304, 'NUMERO', 'NUMERO', 521, NULL);
INSERT INTO dato_adicional VALUES (1305, 'ANO', 'TEXTO', 521, NULL);
INSERT INTO dato_adicional VALUES (1307, 'NUMERO', 'NUMERO', 522, NULL);
INSERT INTO dato_adicional VALUES (1308, 'ANO', 'TEXTO', 522, NULL);
INSERT INTO dato_adicional VALUES (1310, 'NUMERO', 'NUMERO', 523, NULL);
INSERT INTO dato_adicional VALUES (1311, 'ANO', 'TEXTO', 523, NULL);
INSERT INTO dato_adicional VALUES (1313, 'NUMERO', 'NUMERO', 524, NULL);
INSERT INTO dato_adicional VALUES (1314, 'ANO', 'TEXTO', 524, NULL);
INSERT INTO dato_adicional VALUES (1315, 'OBSERVACIONES', 'AREA', 525, NULL);
INSERT INTO dato_adicional VALUES (1269, 'ADUANA', 'COMBO', 503, 1269);
INSERT INTO dato_adicional VALUES (1273, 'ADUANA', 'COMBO', 505, 1273);
INSERT INTO dato_adicional VALUES (1289, 'ADUANA', 'COMBO', 513, 1289);
INSERT INTO dato_adicional VALUES (1297, 'ADUANA', 'COMBO', 519, 1297);
INSERT INTO dato_adicional VALUES (1300, 'ADUANA', 'COMBO', 520, 1300);
INSERT INTO dato_adicional VALUES (1303, 'ADUANA', 'COMBO', 521, 1303);
INSERT INTO dato_adicional VALUES (1363, 'Fecha de Recepción', 'FECHA', 581, NULL);
INSERT INTO dato_adicional VALUES (1364, 'Fecha de Recepción', 'FECHA', 583, NULL);
INSERT INTO dato_adicional VALUES (1365, 'Fecha de Recepción', 'FECHA', 584, NULL);
INSERT INTO dato_adicional VALUES (1306, 'ADUANA', 'COMBO', 522, 1306);
INSERT INTO dato_adicional VALUES (1366, 'Fecha de Recepción', 'FECHA', 585, NULL);
INSERT INTO dato_adicional VALUES (1309, 'ADUANA', 'COMBO', 523, 1309);
INSERT INTO dato_adicional VALUES (1312, 'ADUANA', 'COMBO', 524, 1312);
INSERT INTO dato_adicional VALUES (1316, 'Número', 'NUMERO', 527, NULL);
INSERT INTO dato_adicional VALUES (1317, 'Año', 'TEXTO', 527, NULL);
INSERT INTO dato_adicional VALUES (1318, 'Fecha', 'FECHA', 528, NULL);
INSERT INTO dato_adicional VALUES (1319, 'Número', 'NUMERO', 528, NULL);
INSERT INTO dato_adicional VALUES (1320, 'Año', 'TEXTO', 528, NULL);
INSERT INTO dato_adicional VALUES (1321, 'Fecha', 'FECHA', 529, NULL);
INSERT INTO dato_adicional VALUES (1322, 'Número', 'NUMERO', 529, NULL);
INSERT INTO dato_adicional VALUES (1323, 'Año', 'TEXTO', 529, NULL);
INSERT INTO dato_adicional VALUES (1324, 'Número', 'NUMERO', 533, NULL);
INSERT INTO dato_adicional VALUES (1325, 'Año', 'TEXTO', 533, NULL);
INSERT INTO dato_adicional VALUES (1326, 'Número', 'NUMERO', 534, NULL);
INSERT INTO dato_adicional VALUES (1327, 'Año', 'TEXTO', 534, NULL);
INSERT INTO dato_adicional VALUES (1328, 'Número', 'NUMERO', 535, NULL);
INSERT INTO dato_adicional VALUES (1329, 'Año', 'TEXTO', 535, NULL);
INSERT INTO dato_adicional VALUES (1331, 'Descripción', 'TEXTO', 536, NULL);
INSERT INTO dato_adicional VALUES (1335, 'Número', 'NUMERO', 537, NULL);
INSERT INTO dato_adicional VALUES (1336, 'Año', 'TEXTO', 537, NULL);
INSERT INTO dato_adicional VALUES (1338, 'Número', 'NUMERO', 538, NULL);
INSERT INTO dato_adicional VALUES (1339, 'Año', 'TEXTO', 538, NULL);
INSERT INTO dato_adicional VALUES (1340, 'Fecha', 'FECHA', 539, NULL);
INSERT INTO dato_adicional VALUES (1341, 'Número', 'NUMERO', 539, NULL);
INSERT INTO dato_adicional VALUES (1342, 'Año', 'TEXTO', 539, NULL);
INSERT INTO dato_adicional VALUES (1343, 'Fecha', 'FECHA', 540, NULL);
INSERT INTO dato_adicional VALUES (1344, 'Número', 'NUMERO', 540, NULL);
INSERT INTO dato_adicional VALUES (1345, 'Año', 'TEXTO', 540, NULL);
INSERT INTO dato_adicional VALUES (1346, 'Número', 'NUMERO', 542, NULL);
INSERT INTO dato_adicional VALUES (1347, 'Año', 'TEXTO', 542, NULL);
INSERT INTO dato_adicional VALUES (1348, 'Número', 'NUMERO', 543, NULL);
INSERT INTO dato_adicional VALUES (1349, 'Año', 'TEXTO', 543, NULL);
INSERT INTO dato_adicional VALUES (1350, 'Descripción', 'TEXTO', 544, NULL);
INSERT INTO dato_adicional VALUES (1330, 'Aduana', 'COMBO', 535, 1330);
INSERT INTO dato_adicional VALUES (1337, 'Aduana', 'COMBO', 537, 1337);
INSERT INTO dato_adicional VALUES (1351, 'Fecha de Recepción', 'FECHA', 573, NULL);
INSERT INTO dato_adicional VALUES (1352, 'Número de Solicitud', 'NUMERO', 574, NULL);
INSERT INTO dato_adicional VALUES (1353, 'Fecha de Solicitud', 'FECHA', 574, NULL);
INSERT INTO dato_adicional VALUES (1355, 'Número de Declaración', 'NUMERO', 575, NULL);
INSERT INTO dato_adicional VALUES (1356, 'Fecha de Declaración', 'FECHA', 575, NULL);
INSERT INTO dato_adicional VALUES (1358, 'Fecha de Emisión', 'FECHA', 576, NULL);
INSERT INTO dato_adicional VALUES (1359, 'Fecha de Emisión', 'FECHA', 577, NULL);
INSERT INTO dato_adicional VALUES (1360, 'Fecha de Emisión', 'FECHA', 578, NULL);
INSERT INTO dato_adicional VALUES (1361, 'Fecha de Emisión', 'FECHA', 579, NULL);
INSERT INTO dato_adicional VALUES (1362, 'Fecha de Notificación', 'FECHA', 580, NULL);
INSERT INTO dato_adicional VALUES (1368, 'Fecha de Notificación', 'FECHA', 587, NULL);
INSERT INTO dato_adicional VALUES (1369, 'Fecha de Recepción', 'COMBO', 588, NULL);
INSERT INTO dato_adicional VALUES (1370, 'Fecha de Emisión', 'FECHA', 589, NULL);
INSERT INTO dato_adicional VALUES (1371, 'Descripción', 'TEXTO', 590, NULL);
INSERT INTO dato_adicional VALUES (1354, 'Aduana', 'COMBO', 574, 1354);
INSERT INTO dato_adicional VALUES (1357, 'Aduana', 'COMBO', 575, 1357);
INSERT INTO dato_adicional VALUES (1372, 'Fecha de Recepción.', 'FECHA', 586, NULL);
INSERT INTO dato_adicional VALUES (1367, 'Fecha de Recepción', 'COMBO', 586, 1367);
INSERT INTO dato_adicional VALUES (1373, 'Dato Texto Web', 'TEXTO', 1, NULL);
INSERT INTO dato_adicional VALUES (4, 'Dato Combo', 'COMBO', 1, 4);
INSERT INTO dato_adicional VALUES (6, 'Numero de Relacion', 'TEXTO', 46, NULL);
INSERT INTO dato_adicional VALUES (8, 'Nombre de Archivo PDF', 'TEXTO', 47, NULL);
INSERT INTO dato_adicional VALUES (9, 'Numero de Certificacion', 'TEXTO', 48, NULL);
INSERT INTO dato_adicional VALUES (10, 'Fecha de Pago', 'FECHA', 48, NULL);
INSERT INTO dato_adicional VALUES (11, 'Numero de Relacion', 'TEXTO', 3, NULL);
INSERT INTO dato_adicional VALUES (13, 'Numero de Movimiento', 'TEXTO', 8, NULL);
INSERT INTO dato_adicional VALUES (14, 'Fecha de Preparacion', 'FECHA', 8, NULL);
INSERT INTO dato_adicional VALUES (16, 'Fecha de Antecedente', 'FECHA', 9, NULL);
INSERT INTO dato_adicional VALUES (17, 'Fecha de Contrato', 'FECHA', 10, NULL);
INSERT INTO dato_adicional VALUES (18, 'Numero de Oficio', 'TEXTO', 11, NULL);
INSERT INTO dato_adicional VALUES (19, 'Fecha de Oficio', 'FECHA', 11, NULL);
INSERT INTO dato_adicional VALUES (15, 'Tipo de Movimiento', 'COMBO', 8, 15);
INSERT INTO dato_adicional VALUES (7, 'Tipo de Liquidacion', 'COMBO', 46, 7);
INSERT INTO dato_adicional VALUES (20, 'Fecha de Constancia', 'FECHA', 12, NULL);
INSERT INTO dato_adicional VALUES (22, 'Numero de Punto', 'TEXTO', 13, NULL);
INSERT INTO dato_adicional VALUES (23, 'Fecha de Punto', 'FECHA', 13, NULL);
INSERT INTO dato_adicional VALUES (24, 'Fecha de Preparacion', 'FECHA', 14, NULL);
INSERT INTO dato_adicional VALUES (25, 'Fecha de Emision', 'FECHA', 17, NULL);
INSERT INTO dato_adicional VALUES (26, 'Fecha de Constancia', 'FECHA', 18, NULL);
INSERT INTO dato_adicional VALUES (27, 'Numero de Resolucion', 'TEXTO', 22, NULL);
INSERT INTO dato_adicional VALUES (28, 'Fecha de Resolucion', 'FECHA', 22, NULL);
INSERT INTO dato_adicional VALUES (29, 'Fecha de Carta', 'FECHA', 23, NULL);
INSERT INTO dato_adicional VALUES (30, 'Numero de Acta', 'TEXTO', 24, NULL);
INSERT INTO dato_adicional VALUES (31, 'Fecha de Acta', 'FECHA', 24, NULL);
INSERT INTO dato_adicional VALUES (32, 'Numero de Oficio', 'TEXTO', 25, NULL);
INSERT INTO dato_adicional VALUES (33, 'Fecha de Oficio', 'FECHA', 25, NULL);
INSERT INTO dato_adicional VALUES (34, 'Numero de Oficio', 'TEXTO', 26, NULL);
INSERT INTO dato_adicional VALUES (35, 'Fecha de Oficio', 'FECHA', 26, NULL);
INSERT INTO dato_adicional VALUES (36, 'Fecha de Certificacion', 'FECHA', 27, NULL);
INSERT INTO dato_adicional VALUES (37, 'Fecha de Acta', 'FECHA', 28, NULL);
INSERT INTO dato_adicional VALUES (38, 'Numero de Solicitud', 'TEXTO', 30, NULL);
INSERT INTO dato_adicional VALUES (39, 'Fecha de Certificacion', 'FECHA', 30, NULL);
INSERT INTO dato_adicional VALUES (40, 'Fecha de Certificacion', 'FECHA', 33, NULL);
INSERT INTO dato_adicional VALUES (42, 'Numero de Sentencia', 'TEXTO', 34, NULL);
INSERT INTO dato_adicional VALUES (43, 'Fecha de Sentencia', 'FECHA', 34, NULL);
INSERT INTO dato_adicional VALUES (44, 'Fecha de Certificacion', 'FECHA', 36, NULL);
INSERT INTO dato_adicional VALUES (45, 'Numero de Declaracion', 'TEXTO', 37, NULL);
INSERT INTO dato_adicional VALUES (46, 'Fecha de Consignacion', 'FECHA', 37, NULL);
INSERT INTO dato_adicional VALUES (47, 'Numero de Consecutivo', 'TEXTO', 42, NULL);
INSERT INTO dato_adicional VALUES (48, 'Fecha de Pago', 'FECHA', 42, NULL);
INSERT INTO dato_adicional VALUES (49, 'Numero de Cheque', 'TEXTO', 43, NULL);
INSERT INTO dato_adicional VALUES (50, 'Fecha de Entrega', 'FECHA', 43, NULL);
INSERT INTO dato_adicional VALUES (51, 'Numero de Cheque', 'TEXTO', 44, NULL);
INSERT INTO dato_adicional VALUES (52, 'Fecha de Entrega', 'FECHA', 44, NULL);
INSERT INTO dato_adicional VALUES (53, 'Numero de Finiquito', 'TEXTO', 45, NULL);
INSERT INTO dato_adicional VALUES (54, 'Fecha de Emision', 'FECHA', 45, NULL);
INSERT INTO dato_adicional VALUES (12, 'Tipo de Liquidacion', 'COMBO', 3, 12);
INSERT INTO dato_adicional VALUES (55, 'Ano de Finiquito', 'TEXTO', 49, NULL);
INSERT INTO dato_adicional VALUES (56, 'Numero de Certificacion', 'TEXTO', 50, NULL);
INSERT INTO dato_adicional VALUES (57, 'Fecha de Pago', 'FECHA', 50, NULL);
INSERT INTO dato_adicional VALUES (1375, 'Numero de Relacion', 'TEXTO', 592, NULL);
INSERT INTO dato_adicional VALUES (1380, 'Numero de Movimiento', 'TEXTO', 596, NULL);
INSERT INTO dato_adicional VALUES (1381, 'Fecha de Preparacion', 'FECHA', 596, NULL);
INSERT INTO dato_adicional VALUES (1383, 'Fecha de Antecedente', 'FECHA', 597, NULL);
INSERT INTO dato_adicional VALUES (1384, 'Fecha de Contrato', 'FECHA', 598, NULL);
INSERT INTO dato_adicional VALUES (1385, 'Numero de Oficio', 'TEXTO', 599, NULL);
INSERT INTO dato_adicional VALUES (1386, 'Fecha de Oficio', 'FECHA', 599, NULL);
INSERT INTO dato_adicional VALUES (1387, 'Fecha de Constancia', 'FECHA', 600, NULL);
INSERT INTO dato_adicional VALUES (1388, 'Numero de Punto', 'TEXTO', 601, NULL);
INSERT INTO dato_adicional VALUES (1389, 'Fecha de Punto', 'FECHA', 601, NULL);
INSERT INTO dato_adicional VALUES (1390, 'Fecha de Preparacion', 'FECHA', 602, NULL);
INSERT INTO dato_adicional VALUES (1391, 'Fecha de Emision', 'FECHA', 605, NULL);
INSERT INTO dato_adicional VALUES (1392, 'Numero de Resolucion', 'TEXTO', 606, NULL);
INSERT INTO dato_adicional VALUES (1393, 'Fecha de Resolucion', 'FECHA', 606, NULL);
INSERT INTO dato_adicional VALUES (1394, 'Fecha de Carta', 'FECHA', 607, NULL);
INSERT INTO dato_adicional VALUES (1395, 'Numero de Acta', 'TEXTO', 608, NULL);
INSERT INTO dato_adicional VALUES (1396, 'Fecha de Acta', 'FECHA', 608, NULL);
INSERT INTO dato_adicional VALUES (1397, 'Numero de Oficio', 'TEXTO', 609, NULL);
INSERT INTO dato_adicional VALUES (1398, 'Fecha de Oficio', 'FECHA', 609, NULL);
INSERT INTO dato_adicional VALUES (1399, 'Numero de Oficio', 'TEXTO', 610, NULL);
INSERT INTO dato_adicional VALUES (1400, 'Fecha de Oficio', 'FECHA', 610, NULL);
INSERT INTO dato_adicional VALUES (1401, 'Fecha de Certificacion', 'FECHA', 611, NULL);
INSERT INTO dato_adicional VALUES (1402, 'Fecha de Acta', 'FECHA', 612, NULL);
INSERT INTO dato_adicional VALUES (1403, 'Numero de Solicitud', 'TEXTO', 614, NULL);
INSERT INTO dato_adicional VALUES (1404, 'Fecha de Certificacion', 'FECHA', 614, NULL);
INSERT INTO dato_adicional VALUES (1405, 'Fecha de Certificacion', 'FECHA', 617, NULL);
INSERT INTO dato_adicional VALUES (1406, 'Numero de Sentencia', 'TEXTO', 618, NULL);
INSERT INTO dato_adicional VALUES (1407, 'Fecha de Sentencia', 'FECHA', 618, NULL);
INSERT INTO dato_adicional VALUES (1408, 'Fecha de Certificacion', 'FECHA', 620, NULL);
INSERT INTO dato_adicional VALUES (1409, 'Numero de Declaracion', 'TEXTO', 621, NULL);
INSERT INTO dato_adicional VALUES (1410, 'Fecha de Consignacion', 'FECHA', 621, NULL);
INSERT INTO dato_adicional VALUES (1411, 'Numero de Consecutivo', 'TEXTO', 622, NULL);
INSERT INTO dato_adicional VALUES (1412, 'Fecha de Pago', 'FECHA', 622, NULL);
INSERT INTO dato_adicional VALUES (1413, 'Numero de Cheque', 'TEXTO', 623, NULL);
INSERT INTO dato_adicional VALUES (1414, 'Fecha de Entrega', 'FECHA', 623, NULL);
INSERT INTO dato_adicional VALUES (1415, 'Numero de Cheque', 'TEXTO', 624, NULL);
INSERT INTO dato_adicional VALUES (1416, 'Fecha de Entrega', 'FECHA', 624, NULL);
INSERT INTO dato_adicional VALUES (1417, 'Numero de Finiquito', 'TEXTO', 625, NULL);
INSERT INTO dato_adicional VALUES (1418, 'Fecha de Emision', 'FECHA', 625, NULL);
INSERT INTO dato_adicional VALUES (1376, 'Tipo de Liquidacion', 'COMBO', 592, 1376);
INSERT INTO dato_adicional VALUES (1382, 'Tipo de Movimiento', 'COMBO', 596, 1382);


--
-- TOC entry 2404 (class 0 OID 21039)
-- Dependencies: 192
-- Data for Name: datos_infodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO datos_infodocumento VALUES (1, 1, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (21, 21, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (41, 41, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (61, 61, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (81, 81, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (101, 101, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (121, 121, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (141, 141, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (161, 161, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3921, 3281, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3861, 3221, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO INVERTIDO, DEBE SER SER REEMPLAZADO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (201, 201, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (401, 401, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO CON MALA CALIDAD', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (441, 441, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (461, 461, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (481, 481, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO VOLTEADO DEBE SER ELMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (501, 501, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (621, 621, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (641, 641, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO CON PESIMA CALIDAD. DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (521, 521, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO ERRONEO. DEBE SER ELMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (541, 541, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (581, 581, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (221, 221, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO ERRONEO. DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (241, 241, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO VENCIDO, DEBE SER VERSIONADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (421, 421, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (261, 261, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO INVERTIDO, DEBE SER REEMPLAZADO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (281, 281, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (301, 301, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (321, 321, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO CON INFORMACION ADICIONAL ERRONEA', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (341, 341, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (361, 361, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (381, 381, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (801, 801, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (861, 861, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (841, 841, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (781, 781, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'VERSIONAR DOCUMENTO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (721, 721, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (761, 761, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (881, NULL, 1, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO INVERTIDO. DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', '2014-10-17', 'tgarrido', 'Prueba de eliminacion', 'Mala Orientación del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (901, NULL, 21, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', '2014-10-17', 'tgarrido', 'Eliminacion de documento', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (821, 821, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3981, 3341, NULL, '2014-10-21', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (941, 901, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (961, 921, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4364, 3621, NULL, '2014-10-27', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1021, 981, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1041, 1001, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1901, 1861, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1121, 1081, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1141, 1101, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1161, 1121, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1701, 1661, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1301, 1261, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO MAL TIPIFICADO, DEBE SER REEMPLAZADO', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1361, 1321, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1401, 1361, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1421, 1381, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1441, 1401, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1461, 1421, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1501, 1461, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1521, 1481, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1561, 1521, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1581, 1541, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1601, 1561, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1941, 1901, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2001, 1961, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1721, 1681, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1741, 1701, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1961, 1921, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1781, 1741, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1821, 1781, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1841, 1801, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1861, 1821, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2061, 2021, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2081, 2041, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2101, 2061, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2121, 2081, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2021, 1981, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4201, NULL, 681, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'prueba', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4241, NULL, 721, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'Para eliminar', 'Los Índices no concuerdan con el Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2301, 2261, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2321, 2281, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2381, 2341, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2401, 2361, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO MAL VISUALIZADO DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2421, 2381, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DE OTRO EXPEDIENTE ', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2281, 2241, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2441, 2401, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2461, 2421, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2481, 2441, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2361, 2321, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO POR PESAR MAS DE LO ESTABLECIDO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2601, NULL, 41, '2014-10-20', 'eescalona', NULL, NULL, '2014-10-20', 'eescalona', 'eliminar ', 'Mala Orientación del Documento', '2014-10-20', 'eescalona', 'eliminado', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (981, 941, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2521, 2481, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2541, 2501, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO POR PESIMA VISUALIZACION', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1181, 1141, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1281, 1241, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1321, 1281, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'INFORMACION ADICIONAL ERRONEA. DEBE SER REEMPLAZADO', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1341, 1301, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO INVERTIDO, DEBE SER REEMPLAZADO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1381, 1341, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO Y MODIFICADO', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1541, 1501, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1621, 1581, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1661, 1621, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2801, NULL, 61, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '1.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2821, NULL, 81, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '2.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Mala Calidad en la Imagen', NULL);
INSERT INTO datos_infodocumento VALUES (2841, NULL, 101, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Visualización Nula del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2861, NULL, 121, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '3.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2881, NULL, 141, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '8.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2901, NULL, 161, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '9.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2921, NULL, 181, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Visualización Nula del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2661, 2601, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2681, 2621, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2721, 2661, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2701, 2641, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2741, 2681, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2761, 2701, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2621, 2561, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2781, 2721, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2641, 2581, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2561, 2521, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', 'MALA CALIDAD DE LA IMAGEN', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (2581, 2541, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3941, 3301, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4001, 3361, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4041, NULL, 661, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Rechazo para Eliminación', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'Eliminacion por pruebas', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4362, 3619, NULL, '2014-10-27', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4161, 3481, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4281, 3541, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4321, 3581, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4323, 3583, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4325, 3585, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4327, 3587, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4329, 3589, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4331, 3591, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3221, 3021, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4333, 3593, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4335, 3595, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3201, 3001, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4337, 3597, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4339, 3599, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4363, 3620, NULL, '2014-10-27', 'nilda_blanco', NULL, NULL, '2014-10-27', 'nilda_blanco', 'DOCUMENTO DEBE SER MEJORADO', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3241, NULL, 201, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO MUY PESADO. DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO POR PESAR MAS DE LO ESTABLECIDO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (2041, 2001, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3261, NULL, 221, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3281, NULL, 241, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3301, NULL, 261, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3321, NULL, 281, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3341, NULL, 301, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3361, NULL, 321, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3381, NULL, 341, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3401, NULL, 361, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3421, NULL, 381, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3441, NULL, 401, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3461, NULL, 421, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3481, NULL, 441, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3501, NULL, 461, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3521, NULL, 481, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3541, NULL, 501, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3581, NULL, 521, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3601, NULL, 541, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3621, NULL, 561, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3821, 3181, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3641, NULL, 581, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3661, NULL, 601, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3681, NULL, 621, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (3561, 3041, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3721, 3081, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3741, 3101, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3761, 3121, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4332, 3592, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3781, 3141, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'rechazo por prueba', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3841, 3201, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (3961, 3321, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4021, NULL, 641, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', '
Rechazo para redigitalizacion en pdf', 'Falla Técnica del Sistema', '2014-10-21', 'javier_plaza', 'Eliminado por pruebas', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4061, 3381, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4334, 3594, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4141, 3461, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4181, 3501, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4221, NULL, 701, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'prueba', 'Los Índices no concuerdan con el Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4261, 3521, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4301, 3561, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4322, 3582, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4324, 3584, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4326, 3586, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4328, 3588, NULL, '2014-10-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4336, 3596, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4338, 3598, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4330, 3590, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4340, 3600, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4348, 3608, NULL, '2014-10-24', 'javier_plaza', '2014-10-24', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4351, NULL, 743, '2014-10-24', 'javier_plaza', NULL, NULL, '2014-10-24', 'javier_plaza', 'Porque es prueba', 'Eliminacion de Documento', '2014-10-24', 'javier_plaza', 'Pare pruebas', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4344, 3604, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, '2014-10-24', 'javier_plaza', 'Pare pruebas', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4345, 3605, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, '2014-10-24', 'javier_plaza', 'Pare pruebas', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4346, 3606, NULL, '2014-10-24', 'javier_plaza', NULL, NULL, '2014-10-24', 'javier_plaza', 'Pare pruebas', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4349, NULL, 741, '2014-10-24', 'javier_plaza', NULL, NULL, '2014-10-24', 'javier_plaza', 'Holis', 'Eliminacion de Documento', '2014-10-24', 'javier_plaza', 'Pare pruebas', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4350, NULL, 742, '2014-10-24', 'javier_plaza', NULL, NULL, '2014-10-24', 'javier_plaza', 'para eliminar', 'Eliminacion de Documento', '2014-10-24', 'javier_plaza', 'Pare pruebas', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4347, 3607, NULL, '2014-10-24', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4352, 3609, NULL, '2014-10-24', 'nilda_blanco', '2014-10-24', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4354, 3611, NULL, '2014-10-24', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4357, 3614, NULL, '2014-10-24', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4358, 3615, NULL, '2014-10-24', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4359, 3616, NULL, '2014-10-24', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4360, 3617, NULL, '2014-10-24', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4361, 3618, NULL, '2014-10-27', 'nilda_blanco', NULL, NULL, '2014-10-27', 'nilda_blanco', 'IMAGEN PIXELADA, DEBE SER REEMPLAZADA', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (1001, 961, NULL, '2014-10-27', 'nilda_blanco', '2014-10-20', 'nilda_blanco', '2014-10-27', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4365, 3622, NULL, '2014-10-27', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4366, 3623, NULL, '2014-10-27', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4367, 3624, NULL, '2014-10-27', 'nilda_blanco', '2014-10-27', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4368, NULL, 744, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-27', 'nilda_blanco', 'ELIMINAR', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4400, NULL, 756, '2014-10-28', 'nilda_blanco', NULL, NULL, '2014-10-28', 'nilda_blanco', 'DOCUMENTO MAL TIPIFICADO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-28', 'nilda_blanco', '1', 'Falla Técnica del Sistema', NULL);
INSERT INTO datos_infodocumento VALUES (4356, 3613, NULL, '2014-10-24', 'javier_plaza', '2014-10-28', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4373, NULL, 745, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'Prueba', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'Prueba', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4403, NULL, 757, '2014-10-21', 'eescalona', NULL, NULL, '2014-10-28', 'eescalona', 'eliminar', 'Visualización Nula del Documento', '2014-10-28', 'eescalona', 'eliminado', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4376, NULL, 746, '2014-10-28', 'eescalona', NULL, NULL, '2014-10-28', 'eescalona', 'eliminar 1', 'Falla Técnica del Sistema', '2014-10-28', 'eescalona', 'eliminar 1', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4404, NULL, 758, '2014-10-21', 'eescalona', NULL, NULL, '2014-10-28', 'eescalona', 'eliminar', 'Los Índices no concuerdan con el Documento', '2014-10-28', 'eescalona', 'eliminado', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4405, NULL, 759, '2014-10-28', 'eescalona', NULL, NULL, '2014-10-28', 'eescalona', 'eliminar', 'Los Índices no concuerdan con el Documento', '2014-10-28', 'eescalona', 'eliminado tiff', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4388, NULL, 747, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'Prueba', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'Eliminación', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4385, 3639, NULL, '2014-10-28', 'nilda_blanco', NULL, NULL, '2014-10-28', 'nilda_blanco', 'ELIMINAR ', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4387, 3641, NULL, '2014-10-28', 'nilda_blanco', NULL, NULL, '2014-10-28', 'nilda_blanco', 'DOCUMENTO MAL VISTO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4389, NULL, 748, '2014-10-28', 'nilda_blanco', NULL, NULL, '2014-10-28', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', '2014-10-28', 'nilda_blanco', '12', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4390, NULL, 749, '2014-10-28', 'nilda_blanco', NULL, NULL, '2014-10-28', 'nilda_blanco', 'ELIMINAR DOCUMENTO LG', 'Eliminacion de Documento', '2014-10-28', 'nilda_blanco', '155', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4406, 3647, NULL, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'eliminar', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4407, 3648, NULL, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'pruweba reindexacion', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4394, NULL, 750, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'Erick Holaaa', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'EHHH se prueba.', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4395, NULL, 751, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'Prueba', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'HELLOOO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4377, 3631, NULL, '2014-10-28', 'eescalona', NULL, NULL, '2014-10-28', 'eescalona', 'eliminar', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4408, NULL, 760, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'Eliminar', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'Porqie si', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4396, NULL, 752, '2014-10-28', 'eescalona', NULL, NULL, '2014-10-28', 'eescalona', 'eliminar', 'Visualización Nula del Documento', '2014-10-28', 'eescalona', 'eliminado', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4397, NULL, 753, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'Prueba', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'Hola hola, traje pollito.', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4398, NULL, 754, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'Hola gola', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'Hellos', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4399, NULL, 755, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'HOLISSS', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'Erick. Funciono', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4409, NULL, 761, '2014-10-28', 'javier_plaza', NULL, NULL, '2014-10-28', 'javier_plaza', 'ilo', 'Eliminacion de Documento', '2014-10-28', 'javier_plaza', 'Prueba', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4417, 3656, NULL, '2014-10-30', 'javier_plaza', '2014-10-30', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4412, 3651, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-30', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4413, 3652, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4415, 3654, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4427, 3666, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4418, 3657, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4414, 3653, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4419, 3658, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4423, 3662, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4420, 3659, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4422, 3661, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4424, 3663, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4428, 3667, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4430, 3669, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4431, 3670, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4432, 3671, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4434, 3673, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4436, 3675, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4437, 3676, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4438, 3677, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4441, 3680, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4439, 3678, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4440, 3679, NULL, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', 'Holis', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4445, 3684, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4446, 3685, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4442, 3681, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'ELIMINAR', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4444, 3683, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4447, 3686, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4449, 3688, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'ELIMINAR', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4379, 3633, NULL, '2014-10-28', 'eescalona', '2015-11-28', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4381, 3635, NULL, '2014-10-28', 'eescalona', '2015-11-28', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4383, 3637, NULL, '2014-10-28', 'eescalona', '2015-11-28', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4499, NULL, 764, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', 'd', 'Eliminacion de Documento', '2014-10-31', 'javier_plaza', 'Hety hjey	 ', 'Mala Orientación del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4410, 3649, NULL, '2014-10-30', 'nilda_blanco', '2014-10-30', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4425, 3664, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4416, 3655, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4421, 3660, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'DOCUMENTO MAL VISTO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4426, 3665, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'DOCUMENTO MALO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4429, 3668, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'RE', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4433, 3672, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'DOCUMENTO VOLTEADO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4435, 3674, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'ELIMINAR', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4443, 3682, NULL, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', 'Hello deja el show', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4450, 3689, NULL, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', 'Hey Hey', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4547, 3772, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4500, NULL, 765, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', 'ddd', 'Eliminacion de Documento', '2014-10-31', 'javier_plaza', '5', 'Mala Orientación del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4448, 3687, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4501, NULL, 766, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-30', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Mala Calidad en la Imagen', '2014-10-31', 'javier_plaza', 'Prueba Eliminación', 'Mala Calidad en la Imagen', NULL);
INSERT INTO datos_infodocumento VALUES (4508, 3742, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4509, 3743, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4451, 3690, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4455, 3694, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', '12', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4458, 3697, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4460, 3699, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'NO CONCUERDAN', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4462, 3701, NULL, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'EN BLANCO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4457, 3696, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4465, 3704, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4466, 3705, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4459, 3698, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4461, 3700, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4516, NULL, 768, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'VSN', 'Visualización Nula del Documento', '2014-10-31', 'nilda_blanco', '015241', 'Mala tipificación del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4473, 3712, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4463, 3702, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4477, 3716, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4517, NULL, 769, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'DOCUMENTO', 'Falla Técnica del Sistema', '2014-10-31', 'nilda_blanco', '415456', 'Mala Orientación del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4475, 3714, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4464, 3703, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4467, 3706, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4468, 3707, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4518, NULL, 770, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'ELIMINAR', 'Eliminacion de Documento', '2014-10-31', 'nilda_blanco', '654654', 'Falla Técnica del Sistema', NULL);
INSERT INTO datos_infodocumento VALUES (4481, 3720, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4478, 3717, NULL, '2014-10-30', 'javier_plaza', '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4519, NULL, 771, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'ELIMINAR', 'Eliminacion de Documento', '2014-10-31', 'nilda_blanco', '232', 'Visualización Nula del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4469, 3708, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4470, 3709, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4471, 3710, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4506, 3740, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', '64655', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4482, 3721, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4483, 3722, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4484, 3723, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4485, 3724, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4486, 3725, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4487, 3726, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4488, 3727, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4489, 3728, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4490, 3729, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4491, 3730, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4492, 3731, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4493, 3732, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4494, 3733, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4495, 3734, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4496, 3735, NULL, '2014-10-30', 'nilda_blanco', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4497, NULL, 762, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', 'Hellooo', 'Eliminacion de Documento', '2014-10-31', 'javier_plaza', 'Suena el telefono', 'Mala Orientación del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4498, NULL, 763, '2014-10-30', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', 'Prueb	 ', 'Eliminacion de Documento', '2014-10-31', 'javier_plaza', '55', 'Mala tipificación del Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4505, 3739, NULL, '2014-10-31', 'javier_plaza', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4502, 3736, NULL, '2014-10-31', 'javier_plaza', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4503, 3737, NULL, '2014-10-31', 'javier_plaza', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4504, 3738, NULL, '2014-10-31', 'javier_plaza', '2014-10-31', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4507, 3741, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', '555', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4510, 3744, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4513, 3747, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4515, NULL, 767, '2014-10-30', 'javier_plaza', NULL, NULL, '2014-10-31', 'nilda_blanco', 'MALA TIPI', 'Los Índices no concuerdan con el Documento', '2014-10-31', 'nilda_blanco', '12', 'Mala Calidad en la Imagen', NULL);
INSERT INTO datos_infodocumento VALUES (4512, 3746, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', '00', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4514, 3748, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', '55', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4511, 3745, NULL, '2014-10-31', 'nilda_blanco', NULL, NULL, '2014-10-31', 'javier_plaza', '55', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4521, 3750, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4522, 3751, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4524, 3753, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4525, 3754, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4567, 3792, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4520, 3749, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4523, 3752, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4568, 3793, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4583, 3807, NULL, '2014-12-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4569, 3794, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4571, 3796, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4538, 3767, NULL, '2014-11-04', 'javier_plaza', NULL, NULL, '2014-11-04', 'javier_plaza', 'Para reemplazo', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4539, 3768, NULL, '2014-11-04', 'javier_plaza', NULL, NULL, '2014-11-04', 'javier_plaza', 'Para redigitalizar
', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4542, NULL, 772, '2014-11-04', 'javier_plaza', NULL, NULL, '2014-11-04', 'javier_plaza', 'Eliminacion	 ', 'Eliminacion de Documento', '2014-11-04', 'javier_plaza', 'Porque esta gordito', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4543, NULL, 773, '2014-11-04', 'javier_plaza', NULL, NULL, '2014-11-04', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', '2014-11-04', 'javier_plaza', 'Eeeeeehhh Caracas UHHHH	 ', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4544, NULL, 774, '2014-11-04', 'javier_plaza', NULL, NULL, '2014-11-04', 'javier_plaza', '22', 'Falla Técnica del Sistema', '2014-11-04', 'javier_plaza', 'Ehhhhh Caracas UHHHH	 ', 'Falla Técnica del Sistema', NULL);
INSERT INTO datos_infodocumento VALUES (4545, NULL, 775, '2014-11-04', 'javier_plaza', NULL, NULL, '2014-11-04', 'javier_plaza', 'Porque la base datos se va a morir', 'Eliminacion de Documento', '2014-11-04', 'javier_plaza', 'YUJU	', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4546, 3771, NULL, '2014-11-04', 'javier_plaza', '2014-11-04', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4570, 3795, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4572, 3797, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4549, 3774, NULL, '2014-11-05', 'javier_plaza', '2014-11-05', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4554, 3779, NULL, '2014-11-06', 'javier_plaza', '2014-11-06', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4550, 3775, NULL, '2014-11-05', 'javier_plaza', '2014-11-06', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4551, 3776, NULL, '2014-11-05', 'javier_plaza', '2014-11-06', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4552, 3777, NULL, '2014-11-05', 'javier_plaza', '2014-11-06', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4553, 3778, NULL, '2014-11-05', 'javier_plaza', '2014-11-06', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4548, 3773, NULL, '2014-11-05', 'javier_plaza', NULL, NULL, '2014-11-06', 'javier_plaza', 'Erick no hizo bien el reporte y debi rechazar.,', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4573, 3798, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4555, 3780, NULL, '2014-11-11', 'javier_plaza', NULL, NULL, '2014-11-11', 'javier_plaza', 'Prueba Reportes	 ', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4540, 3769, NULL, '2014-11-04', 'javier_plaza', NULL, NULL, '2014-11-11', 'javier_plaza', 're', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4559, 3784, NULL, '2014-11-17', 'eescalona', '2014-11-17', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4560, 3785, NULL, '2014-11-18', 'eescalona', '2014-11-18', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4558, 3783, NULL, '2014-11-11', 'javier_plaza', NULL, NULL, '2014-11-21', 'javier_plaza', 'No se ve', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4556, 3781, NULL, '2014-11-11', 'javier_plaza', NULL, NULL, '2014-11-21', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4557, 3782, NULL, '2014-11-11', 'javier_plaza', NULL, NULL, '2014-11-21', 'javier_plaza', 'ff', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4561, 3786, NULL, '2014-11-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4574, 3799, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4575, 3800, NULL, '2014-12-22', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4562, 3787, NULL, '2014-12-22', 'javier_plaza', '2014-12-22', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4563, 3788, NULL, '2014-12-22', 'javier_plaza', NULL, NULL, '2014-12-22', 'javier_plaza', 'Documento con calidad insuficiente, se rechaza para redigitalizar', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4576, 3801, NULL, '2014-12-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4577, 3802, NULL, '2014-12-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4584, 3808, NULL, '2014-12-29', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4579, 3804, NULL, '2014-12-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4580, 3805, NULL, '2014-12-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4581, NULL, 776, '2014-12-22', 'javier_plaza', NULL, NULL, '2014-12-23', 'javier_plaza', 'Se procede a Eliminar el documento por X razon.', 'Eliminacion de Documento', '2014-12-23', 'javier_plaza', 'Rechazado para eliminar por X causa.', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4582, 3806, NULL, '2014-12-23', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4565, 3790, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4566, 3791, NULL, '2014-12-22', 'javier_plaza', '2014-12-23', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4585, 3809, NULL, '2014-12-29', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4586, 3810, NULL, '2014-12-29', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4587, 3811, NULL, '2015-01-14', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4599, 3821, NULL, '2015-03-13', 'tgarrido', NULL, NULL, '2015-03-13', 'tgarrido', 'Prueba de Redigitalización', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4589, NULL, 777, '2015-01-20', 'eescalona', NULL, NULL, '2015-01-20', 'eescalona', 'elimina varios datos adicionales', 'Eliminacion de Documento', '2015-01-20', 'eescalona', 'elimina varios datos adicionales', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4600, 3822, NULL, '2015-03-13', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4591, NULL, 778, '2015-01-20', 'eescalona', NULL, NULL, '2015-01-20', 'eescalona', 'prueba eliminar con varios datos', 'Eliminacion de Documento', '2015-01-20', 'eescalona', 'prueba eliminar con varios datos adicionales', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4593, 3815, NULL, '2015-02-18', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4592, 3814, NULL, '2015-01-26', 'eescalona', '2015-02-18', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4594, 3816, NULL, '2015-03-12', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4595, 3817, NULL, '2015-03-13', 'tgarrido', '2015-03-13', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4596, 3818, NULL, '2015-03-13', 'tgarrido', NULL, NULL, '2015-03-13', 'tgarrido', 'Prueba de Eliminación', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4597, 3819, NULL, '2015-03-13', 'tgarrido', NULL, NULL, '2015-03-13', 'tgarrido', 'Prueba de eliminación', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4598, 3820, NULL, '2015-03-13', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4578, 3803, NULL, '2015-03-13', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4603, 3824, NULL, '2015-05-07', 'tgarrido', '2015-05-07', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4602, NULL, 779, '2015-03-17', 'tgarrido', NULL, NULL, '2015-03-17', 'tgarrido', 'Documento para eliminar', 'Eliminacion de Documento', '2015-03-17', 'tgarrido', 'Eliminacion de documento', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento VALUES (4606, 3827, NULL, '2015-05-07', 'tgarrido', '2015-05-07', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4605, 3826, NULL, '2015-05-07', 'tgarrido', '2015-05-07', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4607, 3828, NULL, '2015-05-07', 'tgarrido', '2015-05-07', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4604, 3825, NULL, '2015-05-07', 'tgarrido', NULL, NULL, '2015-05-07', 'tgarrido', 'Se rechaza para eliminar', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4608, 3829, NULL, '2015-05-07', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4609, 3830, NULL, '2015-05-07', 'tgarrido', '2015-05-07', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4610, 3831, NULL, '2015-05-07', 'tgarrido', NULL, NULL, '2015-05-07', 'tgarrido', 'SE RECHAZA PARA ELIMINAR', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4611, 3832, NULL, '2015-05-07', 'tgarrido', '2015-05-07', 'tgarrido', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4612, 3833, NULL, '2015-11-28', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4380, 3634, NULL, '2014-10-28', 'eescalona', '2015-11-28', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4632, 3853, NULL, '2016-10-22', 'daniel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4633, 3854, NULL, '2016-10-22', 'daniel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4636, 3857, NULL, '2016-10-22', 'david_ruiz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4637, 3858, NULL, '2016-10-22', 'david_ruiz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4640, 3861, NULL, '2016-10-22', 'fbrito', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4641, 3862, NULL, '2016-10-22', 'fbrito', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4613, 3834, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4614, 3835, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4615, 3836, NULL, '2016-10-22', 'asalmeron', NULL, NULL, '2016-10-22', 'thais', 'documento rechazado', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4618, 3839, NULL, '2016-10-22', 'asalmeron', NULL, NULL, '2016-10-22', 'thais', 'documento rechazado', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4617, 3838, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4619, 3840, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4620, 3841, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4623, 3844, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4621, 3842, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4622, 3843, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4624, 3845, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4625, 3846, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4626, 3847, NULL, '2016-10-22', 'asalmeron', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4630, 3851, NULL, '2016-10-22', 'daniel', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4631, 3852, NULL, '2016-10-22', 'daniel', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4629, 3850, NULL, '2016-10-22', 'daniel', NULL, NULL, '2016-10-22', 'thais', 'documento rechazado', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4627, 3848, NULL, '2016-10-22', 'daniel', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4628, 3849, NULL, '2016-10-22', 'daniel', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4634, 3855, NULL, '2016-10-22', 'david_ruiz', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4635, 3856, NULL, '2016-10-22', 'david_ruiz', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4638, 3859, NULL, '2016-10-22', 'fbrito', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (4639, 3860, NULL, '2016-10-22', 'fbrito', '2016-10-22', 'thais', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 2406 (class 0 OID 21048)
-- Dependencies: 194
-- Data for Name: documento_eliminado; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO documento_eliminado VALUES (1, '18110507', 41, 41, 42, 67, 1, 0, 0, NULL, '2014-10-17', 'tgarrido');
INSERT INTO documento_eliminado VALUES (21, '18331751', 41, 41, 42, 68, 1, 0, 0, NULL, '2014-10-17', 'tgarrido');
INSERT INTO documento_eliminado VALUES (41, '01', 1, 1, 1, 1, 4, 0, 0, '2014-10-31', '2014-10-20', 'eescalona');
INSERT INTO documento_eliminado VALUES (61, '17718114', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (81, '17718114', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (101, '17718114', 61, 61, 81, 205, 8, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (121, '17718114', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (141, '17718114', 61, 61, 81, 205, 5, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (161, '17718114', 61, 61, 81, 205, 5, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (181, '17718114', 61, 61, 81, 205, 5, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (201, '16415223', 61, 61, 81, 203, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (221, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (241, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (261, '16415223', 61, 61, 81, 205, 4, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (281, '16415223', 61, 61, 81, 205, 6, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (301, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (321, '16415223', 61, 61, 81, 205, 4, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (341, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (361, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (381, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (401, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (421, '16415223', 61, 61, 101, 244, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (441, '16415223', 61, 61, 141, 281, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (461, '16415223', 61, 61, 141, 281, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (481, '16415223', 61, 61, 141, 281, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (501, '16415223', 61, 61, 141, 287, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (521, '16415223', 61, 61, 141, 281, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (541, '16415223', 61, 61, 141, 287, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (561, '16415223', 61, 61, 141, 287, 4, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (581, '16415223', 61, 61, 141, 287, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (601, '16415223', 61, 61, 141, 287, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (621, '16415223', 61, 61, 141, 287, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (641, 'G-1234567-8', 61, 61, 81, 202, 1, 0, 0, NULL, '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (661, 'G-1234567-8', 61, 61, 81, 121, 1, 0, 0, '2010-12-31', '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (681, 'G-1234567-8', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (701, 'G-1234567-8', 61, 61, 81, 205, 2, 0, 0, NULL, '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (721, 'G-1234567-8', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (741, 'J-1234567-8', 103, 163, 225, 492, 1, 0, 0, NULL, '2014-10-24', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (742, 'J-1234567-8', 103, 163, 225, 492, 1, 0, 0, NULL, '2014-10-24', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (743, 'J-1234567-8', 103, 163, 225, 492, 1, 0, 0, NULL, '2014-10-24', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (744, '17718114', 61, 61, 141, 287, 1, 0, 0, NULL, '2014-10-27', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (745, 'J-1234567-8', 103, 163, 225, 492, 4, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (746, '01', 1, 1, 1, 1, 4, 0, 0, '2014-10-31', '2014-10-28', 'eescalona');
INSERT INTO documento_eliminado VALUES (747, 'J-1234567-8', 103, 163, 225, 492, 7, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (748, '17718114', 61, 61, 81, 205, 6, 0, 0, NULL, '2014-10-28', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (749, '17718114', 61, 61, 81, 205, 7, 0, 0, NULL, '2014-10-28', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (750, 'J-1234567-8', 103, 163, 225, 492, 9, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (751, 'J-1234567-8', 103, 163, 225, 492, 7, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (752, '01', 1, 1, 1, 1, 5, 0, 0, '2014-10-30', '2014-10-28', 'eescalona');
INSERT INTO documento_eliminado VALUES (753, 'J-1234567-8', 103, 163, 225, 492, 7, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (754, 'J-1234567-8', 103, 163, 225, 492, 7, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (755, 'J-1234567-8', 103, 163, 225, 492, 7, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (756, '17718114', 61, 61, 81, 205, 5, 0, 0, NULL, '2014-10-28', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (757, '01', 1, 1, 1, 2, 1, 0, 0, NULL, '2014-10-28', 'eescalona');
INSERT INTO documento_eliminado VALUES (758, '01', 1, 1, 1, 2, 1, 0, 0, NULL, '2014-10-28', 'eescalona');
INSERT INTO documento_eliminado VALUES (759, '01', 1, 1, 1, 1, 8, 0, 0, '2014-11-03', '2014-10-28', 'eescalona');
INSERT INTO documento_eliminado VALUES (760, 'J-98765432-1', 103, 163, 225, 492, 1, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (761, 'J-98765432-1', 103, 163, 225, 492, 1, 0, 0, NULL, '2014-10-28', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (762, 'J-121188-9', 104, 164, 232, 512, 2, 0, 0, NULL, '2014-10-31', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (763, 'J-121188-9', 104, 164, 232, 512, 1, 0, 0, NULL, '2014-10-31', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (764, 'J-121188-9', 104, 164, 232, 512, 1, 0, 0, NULL, '2014-10-31', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (765, 'J-121188-9', 104, 164, 232, 512, 1, 0, 0, NULL, '2014-10-31', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (766, 'J-121188-9', 104, 164, 231, 495, 1, 0, 0, NULL, '2014-10-31', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (767, 'V-18760843-7', 104, 164, 234, 519, 1, 0, 0, NULL, '2014-10-31', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (768, 'V-18760843-7', 104, 164, 234, 521, 1, 0, 0, NULL, '2014-10-31', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (769, 'V-18760843-7', 104, 164, 234, 523, 1, 0, 0, NULL, '2014-10-31', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (770, 'V-18760843-7', 104, 164, 234, 525, 1, 0, 0, NULL, '2014-10-31', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (771, 'V-18760843-7', 104, 164, 234, 525, 1, 0, 0, NULL, '2014-10-31', 'nilda_blanco');
INSERT INTO documento_eliminado VALUES (772, 'J-12345678-9', 105, 165, 237, 531, 1, 0, 0, '2012-12-31', '2014-11-04', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (773, 'J-12345678-9', 105, 165, 237, 536, 1, 0, 0, NULL, '2014-11-04', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (774, 'J-12345678-9', 105, 165, 237, 536, 1, 0, 0, NULL, '2014-11-04', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (775, 'J-12345678-9', 105, 165, 237, 536, 1, 0, 0, NULL, '2014-11-04', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (776, '137397', 21, 167, 247, 575, 1, 0, 0, NULL, '2014-12-23', 'javier_plaza');
INSERT INTO documento_eliminado VALUES (777, '01', 1, 1, 1, 1, 10, 0, 0, '2015-01-14', '2015-01-20', 'eescalona');
INSERT INTO documento_eliminado VALUES (778, '01', 1, 1, 1, 1, 10, 0, 0, '2015-01-31', '2015-01-20', 'eescalona');
INSERT INTO documento_eliminado VALUES (779, '137397', 21, 167, 247, 575, 1, 0, 0, NULL, '2015-03-17', 'tgarrido');


--
-- TOC entry 2407 (class 0 OID 21052)
-- Dependencies: 195
-- Data for Name: estatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estatus VALUES (1, 'Activo');
INSERT INTO estatus VALUES (2, 'Inactivo');


--
-- TOC entry 2408 (class 0 OID 21055)
-- Dependencies: 196
-- Data for Name: estatus_documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estatus_documento VALUES (0, 'Pendiente');
INSERT INTO estatus_documento VALUES (1, 'Aprobado');
INSERT INTO estatus_documento VALUES (2, 'Rechazado');


--
-- TOC entry 2410 (class 0 OID 21060)
-- Dependencies: 198
-- Data for Name: expedientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO expedientes VALUES (21, '18331750', 21, '18331750', NULL, 41, 41);
INSERT INTO expedientes VALUES (22, '18331750', 22, NULL, '2014-03-10', 41, 41);
INSERT INTO expedientes VALUES (23, '18331750', 23, 'V-18331750', NULL, 41, 41);
INSERT INTO expedientes VALUES (24, '18331750', 24, 'Nilda', NULL, 41, 41);
INSERT INTO expedientes VALUES (25, '18331750', 25, 'Blanco', NULL, 41, 41);
INSERT INTO expedientes VALUES (26, '18331750', 26, NULL, '2013-11-12', 41, 41);
INSERT INTO expedientes VALUES (27, '18331750', 27, 'A3-1-1', NULL, 41, 41);
INSERT INTO expedientes VALUES (28, '18331750', 28, 'Recibido', NULL, 41, 41);
INSERT INTO expedientes VALUES (41, '18110507', 21, '18110507', NULL, 41, 41);
INSERT INTO expedientes VALUES (42, '18110507', 22, NULL, '2013-11-12', 41, 41);
INSERT INTO expedientes VALUES (43, '18110507', 23, 'J-18110507', NULL, 41, 41);
INSERT INTO expedientes VALUES (44, '18110507', 24, 'Kenny Ramirez', NULL, 41, 41);
INSERT INTO expedientes VALUES (45, '18110507', 25, '18110507', NULL, 41, 41);
INSERT INTO expedientes VALUES (61, '18110507', 21, '18110507', NULL, 41, 41);
INSERT INTO expedientes VALUES (62, '18110507', 22, NULL, '2013-11-12', 41, 41);
INSERT INTO expedientes VALUES (63, '18110507', 23, 'J-18110507', NULL, 41, 41);
INSERT INTO expedientes VALUES (64, '18110507', 24, 'Kenny Ramirez', NULL, 41, 41);
INSERT INTO expedientes VALUES (65, '18110507', 25, '18110507', NULL, 41, 41);
INSERT INTO expedientes VALUES (66, '18110507', 26, NULL, '2013-06-02', 41, 41);
INSERT INTO expedientes VALUES (67, '18110507', 27, '', NULL, 41, 41);
INSERT INTO expedientes VALUES (68, '18110507', 28, '', NULL, 41, 41);
INSERT INTO expedientes VALUES (81, '18331751', 21, '18331751', NULL, 41, 41);
INSERT INTO expedientes VALUES (82, '18331751', 22, NULL, '1987-03-10', 41, 41);
INSERT INTO expedientes VALUES (83, '18331751', 23, 'J-18331751', NULL, 41, 41);
INSERT INTO expedientes VALUES (84, '18331751', 24, 'SAMIR ALVAREZ', NULL, 41, 41);
INSERT INTO expedientes VALUES (85, '18331751', 25, '121188', NULL, 41, 41);
INSERT INTO expedientes VALUES (86, '18331751', 26, NULL, '1998-02-06', 41, 41);
INSERT INTO expedientes VALUES (87, '18331751', 27, 'A5-1-1', NULL, 41, 41);
INSERT INTO expedientes VALUES (88, '18331751', 28, 'Recibido', NULL, 41, 41);
INSERT INTO expedientes VALUES (1, '01', 1, '01', NULL, 1, 1);
INSERT INTO expedientes VALUES (101, '17714118', 21, '17714118', NULL, 41, 41);
INSERT INTO expedientes VALUES (102, '17714118', 22, NULL, '2014-10-17', 41, 41);
INSERT INTO expedientes VALUES (103, '17714118', 23, 'V-17714118', NULL, 41, 41);
INSERT INTO expedientes VALUES (104, '17714118', 24, 'ALI SUAREZ', NULL, 41, 41);
INSERT INTO expedientes VALUES (105, '17714118', 25, '17714118', NULL, 41, 41);
INSERT INTO expedientes VALUES (106, '17714118', 26, NULL, '2014-09-17', 41, 41);
INSERT INTO expedientes VALUES (107, '17714118', 27, 'A1-1-1', NULL, 41, 41);
INSERT INTO expedientes VALUES (108, '17714118', 28, 'Recibido', NULL, 41, 41);
INSERT INTO expedientes VALUES (121, '17718114', 41, '17718114', NULL, 61, 61);
INSERT INTO expedientes VALUES (122, '17718114', 42, 'ALI SUAREZ', NULL, 61, 61);
INSERT INTO expedientes VALUES (123, '17718114', 43, 'Firma Personal', NULL, 61, 61);
INSERT INTO expedientes VALUES (124, '17718114', 44, '17718114', NULL, 61, 61);
INSERT INTO expedientes VALUES (125, '17718114', 45, '17718114', NULL, 61, 61);
INSERT INTO expedientes VALUES (126, '17718114', 46, NULL, '2014-10-17', 61, 61);
INSERT INTO expedientes VALUES (127, '17718114', 47, '1', NULL, 61, 61);
INSERT INTO expedientes VALUES (128, '17718114', 48, NULL, '2014-10-17', 61, 61);
INSERT INTO expedientes VALUES (129, '17718114', 49, 'BOCA DE GRITA', NULL, 61, 61);
INSERT INTO expedientes VALUES (130, '17718114', 50, 'Activo', NULL, 61, 61);
INSERT INTO expedientes VALUES (141, '16415223', 81, '16415223', NULL, 61, 121);
INSERT INTO expedientes VALUES (142, '16415223', 82, 'DAVID MEDINA', NULL, 61, 121);
INSERT INTO expedientes VALUES (143, '16415223', 83, '16415223', NULL, 61, 121);
INSERT INTO expedientes VALUES (144, '16415223', 84, NULL, '1987-05-24', 61, 121);
INSERT INTO expedientes VALUES (145, '16415223', 85, 'PTO. CABELLO', NULL, 61, 121);
INSERT INTO expedientes VALUES (146, '16415223', 86, 'Activo', NULL, 61, 121);
INSERT INTO expedientes VALUES (181, '4567890', 41, '4567890', NULL, 61, 61);
INSERT INTO expedientes VALUES (182, '4567890', 42, 'PILAR GARCIA', NULL, 61, 61);
INSERT INTO expedientes VALUES (183, '4567890', 43, 'Persona Jurídica', NULL, 61, 61);
INSERT INTO expedientes VALUES (184, '4567890', 44, '4567890', NULL, 61, 61);
INSERT INTO expedientes VALUES (185, '4567890', 45, '4567890', NULL, 61, 61);
INSERT INTO expedientes VALUES (186, '4567890', 46, NULL, '1990-11-08', 61, 61);
INSERT INTO expedientes VALUES (187, '4567890', 47, '0811', NULL, 61, 61);
INSERT INTO expedientes VALUES (188, '4567890', 48, NULL, '1991-10-12', 61, 61);
INSERT INTO expedientes VALUES (189, '4567890', 49, 'PTO. PALMARITO', NULL, 61, 61);
INSERT INTO expedientes VALUES (190, '4567890', 50, 'Activo', NULL, 61, 61);
INSERT INTO expedientes VALUES (201, 'G-1234567-8', 41, 'G-1234567-8', NULL, 61, 61);
INSERT INTO expedientes VALUES (202, 'G-1234567-8', 42, 'Corporacion Venezolana de Guayana CVG', NULL, 61, 61);
INSERT INTO expedientes VALUES (203, 'G-1234567-8', 43, 'Persona Jurídica', NULL, 61, 61);
INSERT INTO expedientes VALUES (204, 'G-1234567-8', 44, '123456', NULL, 61, 61);
INSERT INTO expedientes VALUES (205, 'G-1234567-8', 45, '2008-1452', NULL, 61, 61);
INSERT INTO expedientes VALUES (206, 'G-1234567-8', 46, NULL, '2008-12-12', 61, 61);
INSERT INTO expedientes VALUES (207, 'G-1234567-8', 47, '5268', NULL, 61, 61);
INSERT INTO expedientes VALUES (208, 'G-1234567-8', 48, NULL, '2008-10-30', 61, 61);
INSERT INTO expedientes VALUES (209, 'G-1234567-8', 49, 'LA GUAIRA', NULL, 61, 61);
INSERT INTO expedientes VALUES (210, 'G-1234567-8', 50, 'Activo', NULL, 61, 61);
INSERT INTO expedientes VALUES (221, '17110114', 101, '17110114', NULL, 101, 161);
INSERT INTO expedientes VALUES (222, '17110114', 102, NULL, '2014-10-02', 101, 161);
INSERT INTO expedientes VALUES (223, '17110114', 103, 'V-17110114', NULL, 101, 161);
INSERT INTO expedientes VALUES (224, '17110114', 104, 'RUBEN MUÑOZ', NULL, 101, 161);
INSERT INTO expedientes VALUES (225, '17110114', 105, '17110114', NULL, 101, 161);
INSERT INTO expedientes VALUES (226, '17110114', 106, NULL, '2014-10-22', 101, 161);
INSERT INTO expedientes VALUES (227, '17110114', 107, 'A1-1-1', NULL, 101, 161);
INSERT INTO expedientes VALUES (228, '17110114', 108, 'Por Análisis', NULL, 101, 161);
INSERT INTO expedientes VALUES (229, 'J-1234567-8', 109, 'J-1234567-8', NULL, 103, 163);
INSERT INTO expedientes VALUES (230, 'J-1234567-8', 110, 'El Guapetón C.A.', NULL, 103, 163);
INSERT INTO expedientes VALUES (231, 'J-1234567-8', 111, 'Persona Jurídica', NULL, 103, 163);
INSERT INTO expedientes VALUES (232, 'J-1234567-8', 112, '123456', NULL, 103, 163);
INSERT INTO expedientes VALUES (233, 'J-1234567-8', 113, 'AVG-2008-1305', NULL, 103, 163);
INSERT INTO expedientes VALUES (234, 'J-1234567-8', 114, NULL, '2008-09-09', 103, 163);
INSERT INTO expedientes VALUES (235, 'J-1234567-8', 115, '30.465', NULL, 103, 163);
INSERT INTO expedientes VALUES (236, 'J-1234567-8', 116, NULL, '2008-09-11', 103, 163);
INSERT INTO expedientes VALUES (237, 'J-1234567-8', 117, 'LA GUAIRA', NULL, 103, 163);
INSERT INTO expedientes VALUES (238, 'J-1234567-8', 118, 'Activo', NULL, 103, 163);
INSERT INTO expedientes VALUES (239, 'J-98765432-1', 109, 'J-98765432-1', NULL, 103, 163);
INSERT INTO expedientes VALUES (240, 'J-98765432-1', 110, 'Serial Killer F.P.', NULL, 103, 163);
INSERT INTO expedientes VALUES (241, 'J-98765432-1', 111, 'Firma Personal', NULL, 103, 163);
INSERT INTO expedientes VALUES (242, 'J-98765432-1', 112, '123458', NULL, 103, 163);
INSERT INTO expedientes VALUES (243, 'J-98765432-1', 113, '2008-20045', NULL, 103, 163);
INSERT INTO expedientes VALUES (244, 'J-98765432-1', 114, NULL, '2009-12-31', 103, 163);
INSERT INTO expedientes VALUES (245, 'J-98765432-1', 115, '35.546', NULL, 103, 163);
INSERT INTO expedientes VALUES (246, 'J-98765432-1', 116, NULL, '2008-12-31', 103, 163);
INSERT INTO expedientes VALUES (247, 'J-98765432-1', 117, 'PTO. CABELLO', NULL, 103, 163);
INSERT INTO expedientes VALUES (248, 'J-98765432-1', 118, 'Inactivo', NULL, 103, 163);
INSERT INTO expedientes VALUES (249, 'J-121188-9', 119, 'J-121188-9', NULL, 104, 164);
INSERT INTO expedientes VALUES (250, 'J-121188-9', 120, 'OCEANO AZUL', NULL, 104, 164);
INSERT INTO expedientes VALUES (251, 'J-121188-9', 121, 'PERSONA JURIDICA', NULL, 104, 164);
INSERT INTO expedientes VALUES (252, 'J-121188-9', 122, '121188', NULL, 104, 164);
INSERT INTO expedientes VALUES (253, 'J-121188-9', 123, NULL, '2014-10-30', 104, 164);
INSERT INTO expedientes VALUES (254, 'J-121188-9', 124, NULL, '2014-10-30', 104, 164);
INSERT INTO expedientes VALUES (255, 'J-121188-9', 125, '1988', NULL, 104, 164);
INSERT INTO expedientes VALUES (256, 'J-121188-9', 126, NULL, '2013-11-12', 104, 164);
INSERT INTO expedientes VALUES (257, 'J-121188-9', 127, '06.- PTO. AYACUCHO', NULL, 104, 164);
INSERT INTO expedientes VALUES (258, 'J-121188-9', 128, 'Activo', NULL, 104, 164);
INSERT INTO expedientes VALUES (259, 'V-18760843-7', 119, 'V-18760843-7', NULL, 104, 164);
INSERT INTO expedientes VALUES (260, 'V-18760843-7', 120, 'Hello Deja El Show', NULL, 104, 164);
INSERT INTO expedientes VALUES (261, 'V-18760843-7', 121, 'FIRMA PERSONAL', NULL, 104, 164);
INSERT INTO expedientes VALUES (262, 'V-18760843-7', 122, '12345678', NULL, 104, 164);
INSERT INTO expedientes VALUES (263, 'V-18760843-7', 123, NULL, '2008-12-31', 104, 164);
INSERT INTO expedientes VALUES (264, 'V-18760843-7', 124, NULL, '2008-12-31', 104, 164);
INSERT INTO expedientes VALUES (265, 'V-18760843-7', 125, '31.568', NULL, 104, 164);
INSERT INTO expedientes VALUES (266, 'V-18760843-7', 126, NULL, '2008-12-31', 104, 164);
INSERT INTO expedientes VALUES (267, 'V-18760843-7', 127, '01.- LA GUAIRA', NULL, 104, 164);
INSERT INTO expedientes VALUES (268, 'V-18760843-7', 128, 'Activo', NULL, 104, 164);
INSERT INTO expedientes VALUES (269, 'J-12345678-9', 129, 'J-12345678-9', NULL, 105, 165);
INSERT INTO expedientes VALUES (270, 'J-12345678-9', 130, 'Los Afrodescendientes C.A.', NULL, 105, 165);
INSERT INTO expedientes VALUES (271, 'J-12345678-9', 131, 'Persona Jurídica', NULL, 105, 165);
INSERT INTO expedientes VALUES (272, 'J-12345678-9', 132, '123456', NULL, 105, 165);
INSERT INTO expedientes VALUES (273, 'J-12345678-9', 133, 'N-2008', NULL, 105, 165);
INSERT INTO expedientes VALUES (274, 'J-12345678-9', 134, NULL, '2008-12-31', 105, 165);
INSERT INTO expedientes VALUES (275, 'J-12345678-9', 135, '30.556', NULL, 105, 165);
INSERT INTO expedientes VALUES (276, 'J-12345678-9', 136, NULL, '2009-01-01', 105, 165);
INSERT INTO expedientes VALUES (277, 'J-12345678-9', 137, '06.- Pto Ayacucho', NULL, 105, 165);
INSERT INTO expedientes VALUES (278, 'J-12345678-9', 138, 'Activo', NULL, 105, 165);
INSERT INTO expedientes VALUES (287, '144985', 101, '144985', NULL, 101, 161);
INSERT INTO expedientes VALUES (288, '144985', 102, NULL, '2005-08-18', 101, 161);
INSERT INTO expedientes VALUES (289, '144985', 103, 'J-00029652', NULL, 101, 161);
INSERT INTO expedientes VALUES (290, '144985', 104, 'PLASTIC ENVASES', NULL, 101, 161);
INSERT INTO expedientes VALUES (291, '144985', 105, '015968941', NULL, 101, 161);
INSERT INTO expedientes VALUES (292, '144985', 106, NULL, '2007-07-15', 101, 161);
INSERT INTO expedientes VALUES (293, '144985', 107, 'A1-1-1', NULL, 101, 161);
INSERT INTO expedientes VALUES (294, '144985', 108, 'Por Análisis', NULL, 101, 161);
INSERT INTO expedientes VALUES (295, '144985', 148, '144985', NULL, 21, 167);
INSERT INTO expedientes VALUES (296, '144985', 149, NULL, '2005-08-18', 21, 167);
INSERT INTO expedientes VALUES (297, '144985', 150, 'J-000296952', NULL, 21, 167);
INSERT INTO expedientes VALUES (298, '144985', 151, 'PLASTIC ENVASES C.A', NULL, 21, 167);
INSERT INTO expedientes VALUES (299, '144985', 152, '015968941', NULL, 21, 167);
INSERT INTO expedientes VALUES (300, '144985', 153, NULL, '2007-07-15', 21, 167);
INSERT INTO expedientes VALUES (301, '144985', 154, 'A1-1-1', NULL, 21, 167);
INSERT INTO expedientes VALUES (302, '144985', 155, 'Recibido', NULL, 21, 167);
INSERT INTO expedientes VALUES (279, '137397', 148, '137397', NULL, 21, 167);
INSERT INTO expedientes VALUES (280, '137397', 149, NULL, '2005-01-13', 21, 167);
INSERT INTO expedientes VALUES (281, '137397', 150, 'J-00029695-2', NULL, 21, 167);
INSERT INTO expedientes VALUES (282, '137397', 151, 'PLASTIC ENVASES C.A', NULL, 21, 167);
INSERT INTO expedientes VALUES (283, '137397', 152, '008164091', NULL, 21, 167);
INSERT INTO expedientes VALUES (284, '137397', 153, NULL, '2004-12-08', 21, 167);
INSERT INTO expedientes VALUES (285, '137397', 154, 'A3-1-1', NULL, 21, 167);
INSERT INTO expedientes VALUES (286, '137397', 155, 'Recibido', NULL, 21, 167);
INSERT INTO expedientes VALUES (5, '01', 5, 'direccion prueba nuevo', NULL, 1, 1);
INSERT INTO expedientes VALUES (308, '192785', 148, '192785', NULL, 21, 167);
INSERT INTO expedientes VALUES (309, '192785', 149, NULL, '2013-08-12', 21, 167);
INSERT INTO expedientes VALUES (310, '192785', 150, 'J-00029695-2', NULL, 21, 167);
INSERT INTO expedientes VALUES (311, '192785', 151, 'PLASTIC ENVASES C.A.', NULL, 21, 167);
INSERT INTO expedientes VALUES (312, '192785', 152, '009834562', NULL, 21, 167);
INSERT INTO expedientes VALUES (313, '192785', 153, NULL, '2013-08-28', 21, 167);
INSERT INTO expedientes VALUES (314, '192785', 154, 'A6-1-1', NULL, 21, 167);
INSERT INTO expedientes VALUES (315, '192785', 155, 'Recibido', NULL, 21, 167);
INSERT INTO expedientes VALUES (316, '245689', 148, '245689', NULL, 21, 167);
INSERT INTO expedientes VALUES (317, '245689', 149, NULL, '2014-11-18', 21, 167);
INSERT INTO expedientes VALUES (318, '245689', 150, 'J-00029695-2', NULL, 21, 167);
INSERT INTO expedientes VALUES (319, '245689', 151, 'PLASTIC ENVASES C.A.', NULL, 21, 167);
INSERT INTO expedientes VALUES (320, '245689', 152, '032746985', NULL, 21, 167);
INSERT INTO expedientes VALUES (321, '245689', 153, NULL, '2014-12-19', 21, 167);
INSERT INTO expedientes VALUES (322, '245689', 154, 'A9-1-1', NULL, 21, 167);
INSERT INTO expedientes VALUES (323, '245689', 155, 'Recibido', NULL, 21, 167);
INSERT INTO expedientes VALUES (324, '02', 1, '02', NULL, 1, 1);
INSERT INTO expedientes VALUES (325, '02', 2, '9', NULL, 1, 1);
INSERT INTO expedientes VALUES (326, '02', 3, NULL, '2015-01-01', 1, 1);
INSERT INTO expedientes VALUES (327, '02', 4, 'Dato11', NULL, 1, 1);
INSERT INTO expedientes VALUES (328, '02', 5, 'prueb', NULL, 1, 1);
INSERT INTO expedientes VALUES (329, '02', 161, NULL, '2000-11-02', 1, 1);
INSERT INTO expedientes VALUES (2, '01', 2, '1234567', NULL, 1, 1);
INSERT INTO expedientes VALUES (3, '01', 3, NULL, '2014-04-13', 1, 1);
INSERT INTO expedientes VALUES (4, '01', 4, 'Valor3', NULL, 1, 1);
INSERT INTO expedientes VALUES (331, '250001', 7, '250001', NULL, 2, 3);
INSERT INTO expedientes VALUES (332, '250001', 8, '3915779', NULL, 2, 3);
INSERT INTO expedientes VALUES (333, '250001', 9, 'GARRIDO P. NELLY M.', NULL, 2, 3);
INSERT INTO expedientes VALUES (334, '250001', 10, '0013 - 0003 - MPPRIJP-SAREN', NULL, 2, 3);
INSERT INTO expedientes VALUES (335, '250001', 11, 'A1303 - SAREN Servicio Autonomo de Registros y Notarias', NULL, 2, 3);
INSERT INTO expedientes VALUES (336, '250001', 12, NULL, '2016-09-22', 2, 3);
INSERT INTO expedientes VALUES (337, '250001', 13, '01 - Renuncia', NULL, 2, 3);
INSERT INTO expedientes VALUES (338, '250001', 14, '02 - Empleado', NULL, 2, 3);
INSERT INTO expedientes VALUES (339, '250001', 15, '# de Finiquito 98410001', NULL, 2, 3);
INSERT INTO expedientes VALUES (340, '250001', 16, '04 - Expediente Aprobado', NULL, 2, 3);
INSERT INTO expedientes VALUES (341, '250002', 7, '250002', NULL, 2, 3);
INSERT INTO expedientes VALUES (342, '250002', 8, '3915778', NULL, 2, 3);
INSERT INTO expedientes VALUES (343, '250002', 9, 'GARRIDO P. CARMEN V.', NULL, 2, 3);
INSERT INTO expedientes VALUES (344, '250002', 10, '0013 - 0003 - MPPRIJP-SAREN', NULL, 2, 3);
INSERT INTO expedientes VALUES (345, '250002', 11, 'A1303 - SAREN Servicio Autonomo de Registros y Notarias', NULL, 2, 3);
INSERT INTO expedientes VALUES (346, '250002', 12, NULL, '2010-10-15', 2, 3);
INSERT INTO expedientes VALUES (347, '250002', 13, '03 - Jubilacion', NULL, 2, 3);
INSERT INTO expedientes VALUES (348, '250002', 14, '02 - Empleado', NULL, 2, 3);
INSERT INTO expedientes VALUES (349, '250002', 15, 'Num. Cheque 17289645', NULL, 2, 3);
INSERT INTO expedientes VALUES (350, '250002', 16, '11 - Carga Datos de Cheque', NULL, 2, 3);
INSERT INTO expedientes VALUES (351, '250003', 7, '250003', NULL, 2, 3);
INSERT INTO expedientes VALUES (352, '250003', 8, '10668155', NULL, 2, 3);
INSERT INTO expedientes VALUES (353, '250003', 9, 'ESCALONA ERICK J', NULL, 2, 3);
INSERT INTO expedientes VALUES (354, '250003', 10, '0013 - 0003 - MPPRIJP-SAREN', NULL, 2, 3);
INSERT INTO expedientes VALUES (355, '250003', 11, 'A1303 - SAREN Servicio Autonomo de Registros y Notarias', NULL, 2, 3);
INSERT INTO expedientes VALUES (356, '250003', 12, NULL, '2012-08-23', 2, 3);
INSERT INTO expedientes VALUES (357, '250003', 13, '04 - Invalidez', NULL, 2, 3);
INSERT INTO expedientes VALUES (358, '250003', 14, '01 - Obrero', NULL, 2, 3);
INSERT INTO expedientes VALUES (359, '250003', 15, 'Num. Recibo 78524169', NULL, 2, 3);
INSERT INTO expedientes VALUES (360, '250003', 16, '04 - Expediente Aprobado', NULL, 2, 3);
INSERT INTO expedientes VALUES (361, '250004', 7, '250004', NULL, 2, 3);
INSERT INTO expedientes VALUES (362, '250004', 8, '12456876', NULL, 2, 3);
INSERT INTO expedientes VALUES (363, '250004', 9, 'PEREZ DANIEL', NULL, 2, 3);
INSERT INTO expedientes VALUES (364, '250004', 10, '0013 - 0003 - MPPRIJP-SAREN', NULL, 2, 3);
INSERT INTO expedientes VALUES (365, '250004', 11, 'A1303 - SAREN Servicio Autonomo de Registros y Notarias', NULL, 2, 3);
INSERT INTO expedientes VALUES (366, '250004', 12, NULL, '2013-05-10', 2, 3);
INSERT INTO expedientes VALUES (367, '250004', 13, '01 - Renuncia', NULL, 2, 3);
INSERT INTO expedientes VALUES (368, '250004', 14, '02 - Empleado', NULL, 2, 3);
INSERT INTO expedientes VALUES (369, '250004', 15, 'Num. Finiquito 23672198', NULL, 2, 3);
INSERT INTO expedientes VALUES (370, '250004', 16, '07 - Asignado', NULL, 2, 3);
INSERT INTO expedientes VALUES (371, '785001', 163, '785001', NULL, 2, 4);
INSERT INTO expedientes VALUES (372, '785001', 164, '3915779', NULL, 2, 4);
INSERT INTO expedientes VALUES (373, '785001', 165, 'GARRIDO P. NELLY M.', NULL, 2, 4);
INSERT INTO expedientes VALUES (374, '785001', 166, '0010 - MINISTERIO DEL PODER POPULAR PARA LA SALUD', NULL, 2, 4);
INSERT INTO expedientes VALUES (375, '785001', 167, '0010 - A0061 - INHRR (Instituto Nacional de Higiene "Rafael Rangel")', NULL, 2, 4);
INSERT INTO expedientes VALUES (376, '785001', 168, '2012', NULL, 2, 4);
INSERT INTO expedientes VALUES (377, '785001', 169, 'Num. Finiquito 56983421', NULL, 2, 4);
INSERT INTO expedientes VALUES (378, '785002', 163, '785002', NULL, 2, 4);
INSERT INTO expedientes VALUES (379, '785002', 164, '10668155', NULL, 2, 4);
INSERT INTO expedientes VALUES (380, '785002', 165, 'ESCALONA ERICK J.', NULL, 2, 4);
INSERT INTO expedientes VALUES (381, '785002', 166, '0003 - MINISTERIO DEL PODER POPULAR DE LA DEFENSA', NULL, 2, 4);
INSERT INTO expedientes VALUES (382, '785002', 167, '0003 - A0050 - IPSFA (Instituto de Prevision Social de las Fuerzas Armadas Nacionales)', NULL, 2, 4);
INSERT INTO expedientes VALUES (383, '785002', 168, '2011', NULL, 2, 4);
INSERT INTO expedientes VALUES (384, '785002', 169, 'NUM. CHEQUE 79425133', NULL, 2, 4);
INSERT INTO expedientes VALUES (385, '785003', 163, '785003', NULL, 2, 4);
INSERT INTO expedientes VALUES (386, '785003', 164, '12456876', NULL, 2, 4);
INSERT INTO expedientes VALUES (387, '785003', 165, 'PEREZ DANIEL', NULL, 2, 4);
INSERT INTO expedientes VALUES (388, '785003', 166, '0005 - MINISTERIO DEL PODER POPULAR PARA LA EDUCACION', NULL, 2, 4);
INSERT INTO expedientes VALUES (389, '785003', 167, '0005 - A0138 - Academia Nacional de la Historia', NULL, 2, 4);
INSERT INTO expedientes VALUES (390, '785003', 168, '2010', NULL, 2, 4);
INSERT INTO expedientes VALUES (391, '785003', 169, 'NUM. RECIBO 42237845', NULL, 2, 4);
INSERT INTO expedientes VALUES (392, '322301', 170, '322301', NULL, 2, 2);
INSERT INTO expedientes VALUES (393, '322301', 171, '3915778', NULL, 2, 2);
INSERT INTO expedientes VALUES (394, '322301', 172, 'GARRIDO P. CARMEN V.', NULL, 2, 2);
INSERT INTO expedientes VALUES (395, '322301', 173, '0003 - MINISTERIO DEL PODER POPULAR DE LA DEFENSA', NULL, 2, 2);
INSERT INTO expedientes VALUES (396, '322301', 174, '0003 - A0066 - SASFAN (Servicio Autonomo de Salud de la Fuerza Armada Nacional)', NULL, 2, 2);
INSERT INTO expedientes VALUES (397, '322301', 175, NULL, '2016-02-16', 2, 2);
INSERT INTO expedientes VALUES (398, '322301', 176, '', NULL, 2, 2);
INSERT INTO expedientes VALUES (399, '322301', 177, '', NULL, 2, 2);
INSERT INTO expedientes VALUES (400, '322301', 178, '', NULL, 2, 2);
INSERT INTO expedientes VALUES (401, '322301', 179, '06 - Recibido', NULL, 2, 2);
INSERT INTO expedientes VALUES (402, '322302', 170, '322302', NULL, 2, 2);
INSERT INTO expedientes VALUES (403, '322302', 171, '10668155', NULL, 2, 2);
INSERT INTO expedientes VALUES (404, '322302', 172, 'ESCALONA ERICK J.', NULL, 2, 2);
INSERT INTO expedientes VALUES (405, '322302', 173, '0003 - MINISTERIO DEL PODER POPULAR DE LA DEFENSA', NULL, 2, 2);
INSERT INTO expedientes VALUES (406, '322302', 174, '0003 - A0192 - OCHINA (Oficina Coordinadora de Hidrografía y Navegación)', NULL, 2, 2);
INSERT INTO expedientes VALUES (407, '322302', 175, NULL, '2008-05-25', 2, 2);
INSERT INTO expedientes VALUES (408, '322302', 176, '03 - Jubilacion', NULL, 2, 2);
INSERT INTO expedientes VALUES (409, '322302', 177, '01 - Obrero', NULL, 2, 2);
INSERT INTO expedientes VALUES (410, '322302', 178, 'NUM. FINIQUITO 23674523', NULL, 2, 2);
INSERT INTO expedientes VALUES (411, '322302', 179, '04 - Expediente Aprobado', NULL, 2, 2);
INSERT INTO expedientes VALUES (412, '67876001', 180, '67876001', NULL, 2, 5);
INSERT INTO expedientes VALUES (413, '67876001', 181, '3915779', NULL, 2, 5);
INSERT INTO expedientes VALUES (414, '67876001', 182, 'GARRIDO P. NELLY M.', NULL, 2, 5);
INSERT INTO expedientes VALUES (415, '67876001', 183, '0121 - GOBERNACION DEL ESTADO APURE', NULL, 2, 5);
INSERT INTO expedientes VALUES (416, '67876001', 184, '0121 - A0121 - GOBERNACION DEL ESTADO APURE', NULL, 2, 5);
INSERT INTO expedientes VALUES (417, '67876001', 185, NULL, '2015-08-03', 2, 5);
INSERT INTO expedientes VALUES (418, '67876001', 186, '05 - Pension', NULL, 2, 5);
INSERT INTO expedientes VALUES (419, '67876001', 187, '03 - Docente', NULL, 2, 5);
INSERT INTO expedientes VALUES (420, '67876001', 188, 'NUM. RECIBO 82078201', NULL, 2, 5);
INSERT INTO expedientes VALUES (421, '67876001', 189, '04 - Expediente Aprobado', NULL, 2, 5);
INSERT INTO expedientes VALUES (422, '67876002', 180, '67876002', NULL, 2, 5);
INSERT INTO expedientes VALUES (423, '67876002', 181, '20211355', NULL, 2, 5);
INSERT INTO expedientes VALUES (424, '67876002', 182, 'MARTINEZ G. MARIA J.', NULL, 2, 5);
INSERT INTO expedientes VALUES (425, '67876002', 183, '0121 - GOBERNACION DEL ESTADO APURE', NULL, 2, 5);
INSERT INTO expedientes VALUES (426, '67876002', 184, '0121 - A0121 - GOBERNACION DEL ESTADO APURE', NULL, 2, 5);
INSERT INTO expedientes VALUES (427, '67876002', 185, NULL, '2016-09-01', 2, 5);
INSERT INTO expedientes VALUES (428, '67876002', 186, '01 - Renuncia', NULL, 2, 5);
INSERT INTO expedientes VALUES (429, '67876002', 187, '02 - Empleado', NULL, 2, 5);
INSERT INTO expedientes VALUES (430, '67876002', 188, '', NULL, 2, 5);
INSERT INTO expedientes VALUES (431, '67876002', 189, '06 - Recibido', NULL, 2, 5);
INSERT INTO expedientes VALUES (432, '912', 163, '912', NULL, 2, 4);
INSERT INTO expedientes VALUES (433, '912', 164, '7257831', NULL, 2, 4);
INSERT INTO expedientes VALUES (434, '912', 165, 'RODRIGUEZ BRACHO NATALI TATIANA', NULL, 2, 4);
INSERT INTO expedientes VALUES (435, '912', 166, '0001 - MINISTERIO DEL PODER POPULAR DE AGRICULTURA Y TIERRAS', NULL, 2, 4);
INSERT INTO expedientes VALUES (436, '912', 167, '0001 - A0022 - INIA (Instituto Nacional de Investigaciones Agrícolas)', NULL, 2, 4);
INSERT INTO expedientes VALUES (437, '912', 168, '2014', NULL, 2, 4);
INSERT INTO expedientes VALUES (438, '912', 169, '12', NULL, 2, 4);
INSERT INTO expedientes VALUES (439, '3149936', 163, '5862', NULL, 2, 4);
INSERT INTO expedientes VALUES (440, '3149936', 164, '3149936', NULL, 2, 4);
INSERT INTO expedientes VALUES (441, '3149936', 165, 'ROLLINSON YOHN DEL C', NULL, 2, 4);
INSERT INTO expedientes VALUES (442, '3149936', 166, '0005 - MINISTERIO DEL PODER POPULAR PARA LA EDUCACION', NULL, 2, 4);
INSERT INTO expedientes VALUES (443, '3149936', 167, '0010 - A0446 - Fundación Misión Barrio Adentro', NULL, 2, 4);
INSERT INTO expedientes VALUES (444, '3149936', 168, '1982', NULL, 2, 4);
INSERT INTO expedientes VALUES (445, '3149936', 169, '123', NULL, 2, 4);


--
-- TOC entry 2411 (class 0 OID 21067)
-- Dependencies: 199
-- Data for Name: fabrica; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO fabrica VALUES ('nilda_blanco', '0');
INSERT INTO fabrica VALUES ('tgarrido', '0');
INSERT INTO fabrica VALUES ('javier_plaza', '0');
INSERT INTO fabrica VALUES ('eescalona', '1');
INSERT INTO fabrica VALUES ('thais', '0');
INSERT INTO fabrica VALUES ('asalmeron', '0');


--
-- TOC entry 2413 (class 0 OID 21072)
-- Dependencies: 201
-- Data for Name: foliatura; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO foliatura VALUES (881, 3830, 573, '245689', 1);
INSERT INTO foliatura VALUES (882, 3832, 577, '245689', 2);
INSERT INTO foliatura VALUES (883, 3832, 577, '245689', 3);
INSERT INTO foliatura VALUES (884, 3832, 577, '245689', 4);
INSERT INTO foliatura VALUES (885, 3832, 577, '245689', 5);


--
-- TOC entry 2415 (class 0 OID 21078)
-- Dependencies: 203
-- Data for Name: indices; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO indices VALUES (1, 1, 'Indice Primario Texto', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (2, 1, 'Indice Segundo Numero', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (3, 1, 'Indice Tercero Fecha', 'FECHA', 0, 'S');
INSERT INTO indices VALUES (5, 1, 'Indice Quinto Area', 'AREA', 0, ' ');
INSERT INTO indices VALUES (21, 41, 'Número de Solicitud', 'NUMERO', 0, 'Y');
INSERT INTO indices VALUES (22, 41, 'Fecha de Solicitud', 'FECHA', 0, 'S');
INSERT INTO indices VALUES (23, 41, 'Número de RIF', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (24, 41, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (25, 41, 'Número de Declaración', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (26, 41, 'Fecha de Declaración', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (27, 41, 'Ubicación Física', 'COMBO', 27, ' ');
INSERT INTO indices VALUES (28, 41, 'Estatus del Expediente', 'COMBO', 28, ' ');
INSERT INTO indices VALUES (41, 61, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (42, 61, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (44, 61, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (45, 61, 'Número Providencia Administrativa', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (46, 61, 'Fecha Providencia Administrativa', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (47, 61, 'Número Gaceta Oficial', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (48, 61, 'Fecha Gaceta Oficial', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (43, 61, 'Tipo Persona', 'COMBO', 43, 'S');
INSERT INTO indices VALUES (49, 61, 'Aduana', 'COMBO', 49, ' ');
INSERT INTO indices VALUES (50, 61, 'Estatus de Expediente', 'COMBO', 50, ' ');
INSERT INTO indices VALUES (61, 81, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (62, 81, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (63, 81, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (64, 81, 'Número Providencia Administrativa', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (65, 81, 'Fecha Providencia Administrativa', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (66, 81, 'Número Gaceta Oficial', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (67, 81, 'Fecha Gaceta Oficial', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (68, 81, 'Aduana', 'COMBO', 68, ' ');
INSERT INTO indices VALUES (69, 81, 'Estatus de Expediente', 'COMBO', 69, ' ');
INSERT INTO indices VALUES (81, 121, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (82, 121, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (83, 121, 'Número Registro', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (84, 121, 'Fecha Registro', 'FECHA', 0, 'S');
INSERT INTO indices VALUES (85, 121, 'Aduana', 'COMBO', 85, ' ');
INSERT INTO indices VALUES (86, 121, 'Estatus de Expediente', 'COMBO', 86, ' ');
INSERT INTO indices VALUES (101, 161, 'Número de Solicitud', 'NUMERO', 0, 'Y');
INSERT INTO indices VALUES (102, 161, 'Fecha de Solicitud', 'FECHA', 0, 'S');
INSERT INTO indices VALUES (103, 161, 'Número de RIF', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (104, 161, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (105, 161, 'Número de Declaración', 'NUMERO', 0, ' ');
INSERT INTO indices VALUES (106, 161, 'Fecha de Declaración', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (107, 161, 'Ubicación Física ', 'COMBO', 107, ' ');
INSERT INTO indices VALUES (108, 161, 'Estatus del Expediente', 'COMBO', 108, ' ');
INSERT INTO indices VALUES (109, 163, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (110, 163, 'Nombre o Razón Social   ', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (112, 163, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (113, 163, 'Número Providencia Administrativa', 'TEXTO', 0, 'O');
INSERT INTO indices VALUES (114, 163, 'Fecha Providencia Administrativa   ', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (115, 163, 'Número Gaceta Oficial', 'TEXTO', 0, 'O');
INSERT INTO indices VALUES (116, 163, 'Fecha Gaceta Oficial', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (111, 163, 'Tipo Persona   ', 'COMBO', 111, 'S');
INSERT INTO indices VALUES (161, 1, 'Prueba Fecha', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (117, 163, 'Aduana', 'COMBO', 117, 'O');
INSERT INTO indices VALUES (118, 163, 'Estatus de Expediente', 'COMBO', 118, 'O');
INSERT INTO indices VALUES (119, 164, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (120, 164, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (122, 164, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (123, 164, 'Número Providencia Administrativa', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (124, 164, 'Fecha Providencia Administrativa', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (125, 164, 'Número Gaceta Oficial', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (126, 164, 'Fecha Gaceta Oficial', 'FECHA', 0, ' ');
INSERT INTO indices VALUES (121, 164, 'Tipo Persona', 'COMBO', 121, 'S');
INSERT INTO indices VALUES (127, 164, 'Aduana', 'COMBO', 127, ' ');
INSERT INTO indices VALUES (128, 164, 'Estatus de Expediente', 'COMBO', 128, ' ');
INSERT INTO indices VALUES (129, 165, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (130, 165, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (132, 165, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (133, 165, 'Número Providencia Administrativa', 'TEXTO', 0, 'O');
INSERT INTO indices VALUES (134, 165, 'Fecha Providencia Administrativa', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (135, 165, 'Número Gaceta Oficial', 'TEXTO', 0, 'O');
INSERT INTO indices VALUES (136, 165, 'Fecha Gaceta Oficial', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (139, 166, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (140, 166, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (141, 166, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (142, 166, 'Número Providencia Administrativa', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (143, 166, 'Fecha Providencia Administrativa', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (144, 166, 'Número Gaceta Oficial', 'TEXTO', 0, 'O');
INSERT INTO indices VALUES (145, 166, 'Fecha Gaceta Oficial', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (131, 165, 'Tipo Persona', 'COMBO', 131, 'S');
INSERT INTO indices VALUES (137, 165, 'Aduana', 'COMBO', 137, 'O');
INSERT INTO indices VALUES (138, 165, 'Estatus de Expediente', 'COMBO', 138, 'O');
INSERT INTO indices VALUES (146, 166, 'Aduana', 'COMBO', 146, 'O');
INSERT INTO indices VALUES (147, 166, 'Estatus de Expediente', 'COMBO', 147, 'O');
INSERT INTO indices VALUES (148, 167, 'Número de Solicitud', 'NUMERO', 0, 'Y');
INSERT INTO indices VALUES (149, 167, 'Fecha de Solicitud', 'FECHA', 0, 'S');
INSERT INTO indices VALUES (150, 167, 'Número de RIF', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (151, 167, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (152, 167, 'Número de Declaración', 'TEXTO', 0, 'O');
INSERT INTO indices VALUES (153, 167, 'Fecha de Declaración', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (154, 167, 'Ubicación Física', 'COMBO', 154, 'O');
INSERT INTO indices VALUES (155, 167, 'Estatus del Expediente', 'COMBO', 155, ' ');
INSERT INTO indices VALUES (156, 141, ' Indice Primario Texto Web', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (157, 141, ' Indice Segundo Numero Web', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (158, 141, ' Indice Tercero Fecha Web', 'FECHA', 0, 'S');
INSERT INTO indices VALUES (159, 141, ' Indice Cuarto Combo Web', 'COMBO', 0, 'S');
INSERT INTO indices VALUES (160, 141, ' Indice Quinto Area Web', 'AREA', 0, 'O');
INSERT INTO indices VALUES (4, 1, 'Indice Cuarto Combo', 'COMBO', 4, 'S');
INSERT INTO indices VALUES (7, 3, 'NUMERO DE SOLICITUD U OFICIO', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (8, 3, 'CEDULA DE IDENTIDAD DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (9, 3, 'APELLIDOS Y NOMBRES DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (12, 3, 'FECHA DE SOLICITUD', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (15, 3, 'NUMERO DE CERTIFICACION DE PAGO', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (14, 3, 'TIPO DE EMPLEADO', 'COMBO', 14, ' ');
INSERT INTO indices VALUES (16, 3, 'ESTATUS DE EXPEDIENTE', 'COMBO', 16, ' ');
INSERT INTO indices VALUES (10, 3, 'ORGANISMO PRINCIPAL', 'COMBO', 10, 'S');
INSERT INTO indices VALUES (11, 3, 'ORGANO-ENTE-COMPONENTE-DEPENDENCIA', 'COMBO', 11, 'O');
INSERT INTO indices VALUES (163, 4, 'NUMERO DE SOLICITUD U OFICIO', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (164, 4, 'CEDULA DE IDENTIDAD DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (165, 4, 'APELLIDOS Y NOMBRES DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (168, 4, 'ANO DE SOLICITUD', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (169, 4, 'NUMERO DE CERTIFICACION DE PAGO', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (166, 4, 'ORGANISMO PRINCIPAL', 'COMBO', 166, 'S');
INSERT INTO indices VALUES (167, 4, 'ORGANO-ENTE-COMPONENTE-DEPENDENCIA', 'COMBO', 167, ' ');
INSERT INTO indices VALUES (13, 3, 'TIPO DE EGRESO', 'COMBO', 13, ' ');
INSERT INTO indices VALUES (170, 2, 'NUMERO DE SOLICITUD U OFICIO', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (171, 2, 'CEDULA DE IDENTIDAD DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (172, 2, 'APELLIDOS Y NOMBRES DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (175, 2, 'FECHA DE SOLICITUD', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (178, 2, 'NUMERO DE CERTIFICACION DE PAGO', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (173, 2, 'ORGANISMO PRINCIPAL', 'COMBO', 173, 'S');
INSERT INTO indices VALUES (174, 2, 'ORGANO-ENTE-COMPONENTE-DEPENDENCIA', 'COMBO', 174, ' ');
INSERT INTO indices VALUES (176, 2, 'TIPO DE EGRESO', 'COMBO', 176, ' ');
INSERT INTO indices VALUES (177, 2, 'TIPO DE EMPLEADO', 'COMBO', 177, ' ');
INSERT INTO indices VALUES (179, 2, 'ESTATUS DE EXPEDIENTE', 'COMBO', 179, ' ');
INSERT INTO indices VALUES (180, 5, 'NUMERO DE SOLICITUD U OFICIO', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (181, 5, 'CEDULA DE IDENTIDAD DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (182, 5, 'APELLIDOS Y NOMBRES DE EMPLEADO', 'TEXTO', 0, 'S');
INSERT INTO indices VALUES (185, 5, 'FECHA DE SOLICITUD', 'FECHA', 0, 'O');
INSERT INTO indices VALUES (188, 5, 'NUMERO DE CERTIFICACION DE PAGO', 'TEXTO', 0, ' ');
INSERT INTO indices VALUES (183, 5, 'ORGANISMO PRINCIPAL', 'COMBO', 183, 'S');
INSERT INTO indices VALUES (184, 5, 'ORGANO-ENTE-COMPONENTE-DEPENDENCIA', 'COMBO', 184, 'O');
INSERT INTO indices VALUES (186, 5, 'TIPO DE EGRESO', 'COMBO', 186, ' ');
INSERT INTO indices VALUES (187, 5, 'TIPO DE EMPLEADO', 'COMBO', 187, ' ');
INSERT INTO indices VALUES (189, 5, 'ESTATUS DE EXPEDIENTE', 'COMBO', 189, ' ');


--
-- TOC entry 2417 (class 0 OID 21084)
-- Dependencies: 205
-- Data for Name: infodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO infodocumento VALUES (21, 1, '01', 'a739cf3f0c217c270b0b7298068ba2913c59dc048e8850243be8079a5c74d079-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 2, 0, 1, '2014-11-30', 1, '0');
INSERT INTO infodocumento VALUES (281, 68, '18331750', '923e87e5be075189f373b21e0f12cb2fe3796ae838835da0b6f6ea37bcf8bcb7-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (41, 1, '01', 'a739cf3f0c217c270b0b7298068ba2913416a75f4cea9109507cacd8e2f2aefc-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'jpg', 3, 0, 1, '2014-12-31', 1, '0');
INSERT INTO infodocumento VALUES (81, 1, '01', '025cac2b0de928a6c51cdbdba840d26843ec517d68b6edd3015b3edc9a11367b-1', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'jpg', 2, 1, 1, '2014-11-30', 1, '0');
INSERT INTO infodocumento VALUES (301, 72, '18331750', '1275797809c15f8cd3aaed9564c9ddc334ed066df378efacc9b924ec161e7639-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (321, 75, '18331750', '8d72d315a88575960facaf51d43e644acaf1a3dfb505ffed0d024130f58c5cfa-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (101, 1, '01', 'ad783cd17bf6a8bf3a8f84b9ed92affb38b3eff8baf56627478ec76a704e9b52-1', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 3, 1, 1, '2014-12-31', 1, '0');
INSERT INTO infodocumento VALUES (61, 1, '01', '2cba044b496b2d7d0c77a42d1d7b7c6f7f39f8317fbdb1988ef4c628eba02591-1', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 1, 1, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento VALUES (141, 1, '01', 'ec919513da4e72e0bf9b497dc2be347b0f28b5d49b3020afeecd95b4009adf4c-2', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 2, 2, 1, '2014-11-30', 1, '0');
INSERT INTO infodocumento VALUES (341, 75, '18331750', '8d72d315a88575960facaf51d43e644a3dd48ab31d016ffcbf3314df2b3cb9ce-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (161, 1, '01', '4042a708b14bbcc656ba9539e175573cbd4c9ab730f5513206b999ec0d90d1fb-2', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 3, 2, 1, '2014-12-31', 1, '0');
INSERT INTO infodocumento VALUES (621, 63, '18110507', 'ea3d85914a75a95b5b0f8358eab9665385fc37b18c57097425b52fc7afbb6969-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (401, 62, '18331750', '2d6c9e9c8a10cd696d6fb223cd60ef79816b112c6105b3ebd537828a39af4818-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (241, 64, '18331750', 'e8e536b925e0a050beb32316faba4945f340f1b1f65b6df5b5e3f94d95b11daf-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (421, 65, '18331750', '01c2e6e78cdd8d1e88fbec4a604418b0e0c641195b27425bb056ac56f8953d24-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (361, 76, '18331750', 'f20f8281ae998114905a8616249ed22452720e003547c70561bf5e03b95aa99f-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (381, 78, '18331750', '752fe60b9e0543e7b8d25a6f6bd2952700ec53c4682d36f5c4359f4ae7bd7ba1-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (481, 61, '18110507', '35f16ae87de3d7a1ecd64d826ee4cb789461cce28ebe3e76fb4b931c35a169b0-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (441, 62, '18331750', '2d6c9e9c8a10cd696d6fb223cd60ef7915d4e891d784977cacbfcbb00c48f133-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (261, 66, '18331750', '1c91f57ac50c00b2c34506f86f5d7a1db1a59b315fc9a3002ce38bbe070ec3f5-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 6, NULL, 2, '0');
INSERT INTO infodocumento VALUES (461, 62, '18331750', '2d6c9e9c8a10cd696d6fb223cd60ef790353ab4cbed5beae847a7ff6e220b5cf-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3647, 492, 'J-98765432-1', '752fe60b9e0543e7b8d25a6f6bd295270d8080853a54f8985276b0130266a657-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 1, 0, 3, NULL, 2, '0');
INSERT INTO infodocumento VALUES (501, 62, '18110507', '2d6c9e9c8a10cd696d6fb223cd60ef795b69b9cb83065d403869739ae7f0995e-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (541, 66, '18110507', '1c91f57ac50c00b2c34506f86f5d7a1d16c222aa19898e5058938167c8ab6c57-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (121, 1, '01', '0f3f8b833be1a515dc723845994419414c56ff4ce4aaf9573aa5dff913df997a-2', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'jpg', 1, 2, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento VALUES (581, 68, '18110507', '923e87e5be075189f373b21e0f12cb2fc6e19e830859f2cb9f7c8f8cacb8d2a6-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (221, 63, '18331750', 'ea3d85914a75a95b5b0f8358eab96653060ad92489947d410d897474079c1477-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'pdf', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3361, 203, 'G-1234567-8', '3e85182069d5d9dd7b0b81f8fc708b9a8d9766a69b764fefc12f56739424d136-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (521, 64, '18110507', 'e8e536b925e0a050beb32316faba494507563a3fe3bbe7e3ba84431ad9d055af-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3461, 205, 'G-1234567-8', '752fe60b9e0543e7b8d25a6f6bd295277e8d7e5ccbddfd9576be61e3ab86aa73-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (201, 61, '18331750', '35f16ae87de3d7a1ecd64d826ee4cb78757b505cfd34c64c85ca5b5690ee5293-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3181, 242, '4567890', '9a327d1072fabff1a9057bca9db13bd7f4aa0dd960521e045ae2f20621fb4ee9-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3481, 205, 'G-1234567-8', '752fe60b9e0543e7b8d25a6f6bd295273fb04953d95a94367bb133f862402bce-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 2, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (821, 61, '18110507', '6a1be40c9d6854b9e84a50f724cb11594558dbb6f6f8bb2e16d03b85bde76e2c-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3281, 161, 'G-1234567-8', 'b072911f7517dd93353ab854a3177e7360a70bb05b08d6cd95deb3bdb750dce8-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 1, 10, '2015-12-31', 1, '0');
INSERT INTO infodocumento VALUES (801, 62, '18110507', '5a1434c2341b284becace05845972f321905aedab9bf2477edc068a355bba31a-1', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (861, 63, '18110507', '31f44729b4943d0f6e088cb6d23fcf8bf9a40a4780f5e1306c46f1c8daecee3b-1', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 2, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (841, 66, '18110507', 'ad2f01434ba5e1cc6884a69b05c850ba02a32ad2669e6fe298e607fe7cc0e1a0-1', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (641, 63, '18110507', 'ea3d85914a75a95b5b0f8358eab9665367e103b0761e60683e83c559be18d40c-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 3, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (781, 65, '18331751', '01c2e6e78cdd8d1e88fbec4a604418b07143d7fbadfa4693b9eec507d9d37443-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3381, 201, 'G-1234567-8', '268bc283be64fb07fa6b9c605f4f7df76e16656a6ee1de7232164767ccfa7920-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, '2010-12-31', 1, '0');
INSERT INTO infodocumento VALUES (721, 77, '18331751', 'a7b10532accb7d13ff37f70cf4484bedaba3b6fd5d186d28e06ff97135cade7f-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (761, 78, '18331751', '752fe60b9e0543e7b8d25a6f6bd2952788ae6372cfdc5df69a976e893f4d554b-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (901, 62, '17714118', '2d6c9e9c8a10cd696d6fb223cd60ef79892c91e0a653ba19df81a90f89d99bcd-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (1081, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd2952736a16a2505369e0c922b6ea7a23a56d2-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (921, 64, '17714118', 'e8e536b925e0a050beb32316faba4945430c3626b879b4005d41b8a46172e0c0-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (1101, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd29527c6bff625bdb0393992c9d4db0c6bbe45-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3541, 205, 'G-1234567-8', '89e7f1f4ce4043ba1ff352de83e0b07cc5f5c23be1b71adb51ea9dc8e9d444a8-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1121, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd295273a15c7d0bbe60300a39f76f8a5ba6896-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (981, 181, '17718114', 'f6b121340c85b55db6c672e33c283aae287e03db1d99e0ec2edb90d079e142f3-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2015-06-01', 1, '0');
INSERT INTO infodocumento VALUES (3582, 465, '17110114', 'ea3d85914a75a95b5b0f8358eab966539d684c589d67031a627ad33d59db65e5-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3586, 473, '17110114', 'fbb5ff5d01ac2f11b1ec4e1d06ace628224e5e49814ca908e58c02e28a0462c1-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3589, 479, '17110114', 'a7b10532accb7d13ff37f70cf4484beda385d7d1e52d89d1a445faa37f5b5307-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'pdf', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1001, 202, '17718114', '2604c1897bbc3f7e6c7be05be59db36ab8c37e33defde51cf91e1e03e51657da-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1141, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd29527f7f580e11d00a75814d2ded41fe8e8fe-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 4, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1261, 224, '17718114', '450bed356087d626e24a049b3f35defd17326d10d511828f6b34fa6d751739e2-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '2016-06-01', 2, '1');
INSERT INTO infodocumento VALUES (1281, 241, '17718114', '4c6c2287ced2e3a3243029b0fcc925e3d94e18a8adb4cc0f623f7a83b1ac75b4-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '2014-11-11', 2, '1');
INSERT INTO infodocumento VALUES (1301, 242, '17718114', '9a327d1072fabff1a9057bca9db13bd72df45244f09369e16ea3f9117ca45157-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (1321, 242, '17718114', '9a327d1072fabff1a9057bca9db13bd7f9be311e65d81a9ad8150a60844bb94c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 2, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (1341, 244, '17718114', 'c964af849361c451de597f91ff444a6a33ebd5b07dc7e407752fe773eed20635-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (1361, 262, '17718114', '659747fd917f32c719b276ecea951508cf9a242b70f45317ffd281241fa66502-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1241, 221, '17718114', 'e95efe46ecfc71e51d415f00d36a21381c65cef3dfd1e00c0b03923a1c591db4-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (1421, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b9aa42b31882ec039965f3c4923ce901b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1461, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b95151403b0db4f75bfd8da0b393af853-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3201, 244, '4567890', 'c964af849361c451de597f91ff444a6a24ec8468b67314c2013d215b77034476-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1481, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b4e8412ad48562e3c9934f45c3e144d48-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1501, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b5cbdfd0dfa22a3fca7266376887f549b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 4, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1521, 281, '17718114', '0b6093e91d6c565882bd1af19c7b388f253f7b5d921338af34da817c00f42753-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2001, 265, '16415223', '4776d05c920ee1c2eedd53625de72b86d0fb963ff976f9c37fc81fe03c21ea7b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '2003-02-15', 1, '0');
INSERT INTO infodocumento VALUES (1541, 284, '17718114', '6a16ad0007dca405d7cce200f23f237d1373b284bc381890049e92d324f56de0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1561, 285, '17718114', '922f79842924cbd161fa93420b178793b132ecc1609bfcf302615847c1caa69a-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1861, 101, '16415223', 'e42de1fa95baac471eecbf8f1f312138f9d1152547c0bde01830b7e8bd60024c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1621, 287, '17718114', 'fbc4b2a492ed2e54929eb69312301c1e4462bf0ddbe0d0da40e1e828ebebeb11-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (1581, 286, '17718114', 'f315c06bf0c4da671672bd0b0651ee4e88a199611ac2b85bd3f76e8ee7e55650-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, '2020-08-15', 2, '1');
INSERT INTO infodocumento VALUES (1901, 141, '16415223', '5742286f3c6907872e67f5c203d4e5b1d54e99a6c03704e95e6965532dec148b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2003-03-11', 1, '0');
INSERT INTO infodocumento VALUES (1781, 221, '16415223', 'e95efe46ecfc71e51d415f00d36a21388b6a80c3cf2cbd5f967063618dc54f39-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1701, 181, '16415223', 'f6b121340c85b55db6c672e33c283aae15231a7ce4ba789d13b722cc5c955834-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '1988-11-11', 1, '0');
INSERT INTO infodocumento VALUES (1921, 202, '16415223', '2604c1897bbc3f7e6c7be05be59db36a9f6992966d4c363ea0162a056cb45fe5-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3301, 181, 'G-1234567-8', 'c02e7d715d0b7654044a8aabce2df4ff8217bb4e7fa0541e0f5e04fea764ab91-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 1, 1, '2016-12-31', 1, '0');
INSERT INTO infodocumento VALUES (1801, 223, '16415223', '9ac235aa477ee62c435f98721264a307cd14821dab219ea06e2fd1a2df2e3582-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '1998-11-11', 1, '0');
INSERT INTO infodocumento VALUES (1821, 242, '16415223', '9a327d1072fabff1a9057bca9db13bd7596dedf4498e258e4bdc9fd70df9a859-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2081, 287, '16415223', 'c32c51a2aeff86ad96dc15998e3fbaeb0070d23b06b1486a538c0eaa45dd167a-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1981, 263, '16415223', '96aebd65e843ad4144b098af44775a3fb3b4d2dbedc99fe843fd3dedb02f086f-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2021, 281, '16415223', '809c822955c062ce078df7b3f1393f7d05a5cf06982ba7892ed2a6d38fe832d6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1961, 261, '16415223', 'e8be9944201791ffd0a128644a2c61a5f106b7f99d2cb30c3db1c3cc0fde9ccb-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1681, 161, '16415223', 'ea26359f89d921345bc68fec7606720af50a6c02a3fc5a3a5d4d9391f05f3efc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 5, '2012-12-21', 1, '0');
INSERT INTO infodocumento VALUES (1741, 204, '16415223', 'c66dc80afdea51b798b3913974cc38dfb3b43aeeacb258365cc69cdaf42a68af-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1661, 121, '16415223', 'ef857acb97c2e362b608f99c0255bb0c7d12b66d3df6af8d429c1a357d8b9e1a-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2006-06-05', 1, '0');
INSERT INTO infodocumento VALUES (3501, 205, 'G-1234567-8', '752fe60b9e0543e7b8d25a6f6bd29527d494020ff8ec181ef98ed97ac3f25453-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 3, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3583, 467, '17110114', '01c2e6e78cdd8d1e88fbec4a604418b069eba34671b3ef1ef38ee85caae6b2a1-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2041, 283, '16415223', 'b9c894170eb9f7aaa6b77733b05c6e4f89885ff2c83a10305ee08bd507c1049c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2061, 285, '16415223', '18eb0a7b230aa5d9ab5b5fb13905f9ac52dbb0686f8bd0c0c757acf716e28ec0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1401, 265, '17718114', '465717bdf0c25516f05ce1c5516ff00b9701a1c165dd9420816bfec5edd6c2b1-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '2018-06-01', 1, '0');
INSERT INTO infodocumento VALUES (2321, 242, '4567890', '9a327d1072fabff1a9057bca9db13bd7761c7920f470038d4c8a619c79eddd62-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 8, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3617, 266, '4567890', 'fe4ef4e66671f49a7813973fb7881c1adb68512896941514a089c37392f0683b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 4, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3221, 285, '4567890', '922f79842924cbd161fa93420b178793f5c150afbfbcef941def203e85cf40bc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (2241, 81, '4567890', '8e7b805b563975a276e133302eca5205b6e32320fa6bc5a588b90183b95dc028-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/f605f837d7dac58aad33dc3af98f5a35', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2361, 262, '4567890', '2a79282c361091cea02cd7338c6eab48794288f252f45d35735a13853e605939-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (2501, 266, '4567890', 'd51487b435be314159e60e23ace11eb73f998e713a6e02287c374fd26835d87e-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 5, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (2581, 287, '4567890', '1833a2b1b440cf5ce04d7ee615c46959309a8e73b2cdb95fc1affa8845504e87-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (2521, 282, '4567890', '927a4fe639f66b1988f8f5112ce380274a1590df1d5968d41b855005bb8b67bf-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3321, 161, 'G-1234567-8', 'b072911f7517dd93353ab854a3177e733cfbdf468f0a03187f6cee51a25e5e9a-2', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 1, 2, 1, '2015-12-31', 1, '0');
INSERT INTO infodocumento VALUES (2561, 286, '4567890', 'e0022deb6b3d9c2d53345d51361162d159eb5dd36914c29b299c84b7ddaf08ec-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, '2012-02-15', 1, '0');
INSERT INTO infodocumento VALUES (2261, 222, '4567890', '51dca8005775aa6d89befed929c203e5bbaa9d6a1445eac881750bea6053f564-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (1381, 264, '17718114', 'b9f7659765fe892bb14f3024ebddcca5d82118376df344b0010f53909b961db3-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '1998-11-07', 1, '0');
INSERT INTO infodocumento VALUES (2281, 224, '4567890', 'cf1c8bf91a6ad9dd39478ddfcd3721b60f46c64b74a6c964c674853a89796c8e-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '2014-10-20', 1, '0');
INSERT INTO infodocumento VALUES (2341, 243, '4567890', '478ad03cf0312a4d3369d8021de357f4c8dfece5cc68249206e4690fc4737a8d-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3616, 266, '4567890', '853dc323d2e55b16c95825f5f284365878d69f40906679a976dc4d45cebffbe6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2541, 284, '4567890', '717bae51b1fd658caf09c8cc66eb4000e92d74ccacdc984afa0c517ad0d557a6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3584, 469, '17110114', '91a49c34c2647654c7be5fc5bbe0729d565767eb96d87d0d3af8dfb332c2003f-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3561, 205, 'G-1234567-8', '878d781b05dff873e5d23694c564e5a2414a7497190eaef6b5d75d5a6a11afcf-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 3, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3587, 475, '17110114', 'ebf0c71a99a556f29588365b03b352bfaba53da2f6340a8b89dc96d09d0d0430-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3592, 485, 'J-1234567-8', '5742286f3c6907872e67f5c203d4e5b169f357fcc8e6d119f3d95f33cedb5915-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 1, 0, 4, '2009-12-31', 1, '0');
INSERT INTO infodocumento VALUES (2401, 266, '4567890', 'd78ab2eb6d6a440131b2b4f50fa0645f959ef477884b6ac2241b19ee4fb776ae-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3590, 483, 'J-1234567-8', '1d124ee21bb29912e414ddd29af9b435da6ea77475918a3d83c7e49223d453cc-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (941, 101, '17718114', 'e42de1fa95baac471eecbf8f1f31213892262bf907af914b95a0fc33c3f33bf6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2621, 224, '17718114', '81c816e0d7a4211b9ac37a6fbccc08f4cc70903297fe1e25537ae50aea186306-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'pdf', 1, 0, 5, '2016-06-01', 1, '0');
INSERT INTO infodocumento VALUES (2661, 242, '17718114', 'f0c1b362c6f7d85a80980dde61a83a4f2417dc8af8570f274e6775d4d60496da-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2681, 242, '17718114', '9b8984eefdbb4804bc0336262ff4e06edcda54e29207294d8e7e1b537338b1c0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'jpg', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2641, 241, '17718114', '39bcf6e4c11c908fd882a4053944d583f21e255f89e0f258accbe4e984eef486-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'jpg', 1, 0, 1, '2014-11-11', 1, '0');
INSERT INTO infodocumento VALUES (2701, 244, '17718114', '4919a4f143e7f22fc5fe2ab433020a0bdf0e09d6f25a15a815563df9827f48fa-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2441, 266, '4567890', 'a2299896b628c1798e85d68a8bde231b7a68443f5c80d181c42967cd71612af1-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 3, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (2721, 286, '17718114', 'ab042d83580b0e3e42a8756ad535d9a3362387494f6be6613daea643a7706a42-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'pdf', 1, 0, 5, '2022-08-14', 1, '0');
INSERT INTO infodocumento VALUES (2601, 221, '17718114', '5d2b7dc2dc014d82f67171986970465ee02e27e04fdff967ba7d76fb24b8069d-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3041, 101, 'G-1234567-8', '09ecb0e7a4c9b3db9a6bb80a425ee1669922f5774d88b203c4ec0fdd26616899-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3081, 141, 'G-1234567-8', '5742286f3c6907872e67f5c203d4e5b1ce60ff163cab97029cc727e20e0fc3a7-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 5, '2013-05-12', 1, '0');
INSERT INTO infodocumento VALUES (3101, 161, 'G-1234567-8', 'ea26359f89d921345bc68fec7606720a62f91ce9b820a491ee78c108636db089-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, '2015-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3121, 181, 'G-1234567-8', 'f6b121340c85b55db6c672e33c283aae097e26b2ffb0339458b55da17425a71f-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 1, 0, 1, '2016-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3021, 222, '16415223', '3e26498adf21c45b43d3ae923a5fda639cb9ed4f35cf7c2f295cc2bc6f732a84-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3591, 484, 'J-1234567-8', 'ef857acb97c2e362b608f99c0255bb0c9001ca429212011f4a4fda6c778cc318-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'pdf', 1, 0, 1, '2010-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3648, 492, 'J-98765432-1', '752fe60b9e0543e7b8d25a6f6bd29527cff02a74da64d145a4aed3a577a106ab-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 2, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3141, 201, 'G-1234567-8', 'f6baea765e649a9b9a4fa0166f02a5ea92a08bf918f44ccd961477be30023da1-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 3, '2010-12-31', 2, '1');
INSERT INTO infodocumento VALUES (3593, 486, 'J-1234567-8', 'ea26359f89d921345bc68fec7606720acdd0500dc0ef6682fa6ec6d2e6b577c4-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'jpg', 1, 0, 1, '2015-10-31', 1, '0');
INSERT INTO infodocumento VALUES (3594, 487, 'J-1234567-8', 'f6b121340c85b55db6c672e33c283aaedce8af15f064d1accb98887a21029b08-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 1, 0, 1, '2014-10-01', 1, '0');
INSERT INTO infodocumento VALUES (3001, 281, '16415223', '749db0019bb38996c1e4c16f01139927908c9a564a86426585b29f5335b619bc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3595, 488, 'J-1234567-8', 'f6baea765e649a9b9a4fa0166f02a5eac0e8517b1fe0b5270f3f41d4b56d6118-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tiff', 1, 0, 1, '2018-10-19', 1, '0');
INSERT INTO infodocumento VALUES (3596, 489, 'J-1234567-8', '2604c1897bbc3f7e6c7be05be59db36a52c409f1571f500e28f490a302a12540-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3597, 490, 'J-1234567-8', '3e85182069d5d9dd7b0b81f8fc708b9ab89c30965ebc74912de879f22da62dbf-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3521, 205, 'G-1234567-8', '009692bbb41115390632242c3db6b9af98afdcc1ebd85daa0f1749c5e56b9d8c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3598, 491, 'J-1234567-8', 'c66dc80afdea51b798b3913974cc38df6a450490f238b4ddff085d66a916a206-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'pdf', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3581, 463, '17110114', '35f16ae87de3d7a1ecd64d826ee4cb7867606d48e361ce176ca71fd54fcf4286-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3599, 483, 'J-1234567-8', '1d124ee21bb29912e414ddd29af9b435e7e69cdf28f8ce6b69b4e1853ee21bab-1', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 1, 1, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3600, 483, 'J-1234567-8', '90948f3083c74e585cf95c74d2b4b36e8d7628dd7a710c8638dbd22d4421ee46-2', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'pdf', 1, 2, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3585, 471, '17110114', '4097de69b33a3ad14ff50612b78b23272668a7105966cae6e23901495176b8f9-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3588, 477, '17110114', '8d72d315a88575960facaf51d43e644ac2964caac096f26db222cb325aa267cb-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3660, 498, 'V-18760843-7', 'ea26359f89d921345bc68fec7606720a9627c45df543c816a3ddf2d8ea686a99-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 2, '2008-12-31', 2, '1');
INSERT INTO infodocumento VALUES (2381, 264, '4567890', '03ec2b6213936f48f9f7372b95811fa57b66b4fd401a271a1c7224027ce111bc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '2008-08-11', 2, '1');
INSERT INTO infodocumento VALUES (3608, 483, 'J-1234567-8', 'e42de1fa95baac471eecbf8f1f31213860a0575ee6ce460e1d86c0e9d281c4f1-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3605, 492, 'J-1234567-8', '752fe60b9e0543e7b8d25a6f6bd29527a05d886123a54de3ca4b0985b718fb9b-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'pdf', 4, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3611, 262, '4567890', '7ca545b24c057346d84fe5675676f7854cf33e18ede11b79827bc78b7f2075ae-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3606, 492, 'J-1234567-8', '752fe60b9e0543e7b8d25a6f6bd29527774b0e07753b0b94d1a1c5b0543b5fe1-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'jpg', 5, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3609, 262, '4567890', '0698a0bae696647d5c032ed83cb45e88649a066d415bdda4ce2a7088292645e0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3604, 492, 'J-1234567-8', '752fe60b9e0543e7b8d25a6f6bd29527c254e7753095807e1cca159e48eceb21-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'tif', 1, 0, 3, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3341, 284, '4567890', '5304fd501ca14fb9d180333c34fe0072acf06cdd9c744f969958e1f085554c8b-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3607, 264, '4567890', 'e38eb3406617faa2b6550ae4c9991db1967990de5b3eac7b87d49a13c6834978-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'jpg', 1, 0, 1, '2008-08-11', 1, '0');
INSERT INTO infodocumento VALUES (3679, 509, 'J-121188-9', '4c6c2287ced2e3a3243029b0fcc925e374791edf1f8e8b8289a5067737630874-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'jpg', 2, 0, 1, '2014-11-10', 2, '1');
INSERT INTO infodocumento VALUES (3661, 502, 'J-121188-9', '3e85182069d5d9dd7b0b81f8fc708b9ae761813f83dfc86fa1c6e0da5510c3b8-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3667, 504, 'J-121188-9', '752fe60b9e0543e7b8d25a6f6bd29527f095cedd23b99f1696fc8caecbcf257e-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3613, 492, 'J-1234567-8', '9a162eb8ec65bf52fb15e2365a1537e4c4fa7aecedac73641320d24d5bf3bf38-0', 'a491fbeb0191f9f06224a01850100b9d/1846d4fbf25d60cfa2c910d5c6e35bf9/d2e7e49ef6e0fcc241ba9192593a7885', 'jpg', 6, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3657, 499, 'J-121188-9', 'f6b121340c85b55db6c672e33c283aaea22d33b4a00c165507a61f3bed4b5149-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 1, '1960-09-27', 1, '0');
INSERT INTO infodocumento VALUES (3670, 502, 'V-18760843-7', '3e85182069d5d9dd7b0b81f8fc708b9a77ec6f21c85b637cc42bb997841e11a6-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 8, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3673, 506, 'J-121188-9', '3e26498adf21c45b43d3ae923a5fda63740a02d0786a4239a62076f650cd26da-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3676, 507, 'J-121188-9', '9ac235aa477ee62c435f98721264a30711338326597d14a1f7c745853f4d50a8-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 2, '0204-10-15', 1, '0');
INSERT INTO infodocumento VALUES (3682, 510, 'J-121188-9', '9a327d1072fabff1a9057bca9db13bd7898aef0932f6aaecda27aba8e9903991-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 4, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3694, 513, 'V-18760843-7', 'e8be9944201791ffd0a128644a2c61a54b26dc4663ccf960c8538d595d0a1d3a-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 5, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3685, 511, 'J-121188-9', '5a4d1129f7c7576fe00153e97a9498be55a0df4b5a1786cd13a7a8de759859d4-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3664, 495, 'J-121188-9', 'e42de1fa95baac471eecbf8f1f312138b67fb3360ae5597d85a005153451dd4e-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3688, 511, 'V-18760843-7', '5a4d1129f7c7576fe00153e97a9498bed880067f879409df09ac50ba315707aa-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 2, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3699, 515, 'V-18760843-7', '3043e5164e76c4e02101cdd2581d3ca3a18630ab1c3b9f14454cf70dc7114834-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3697, 514, 'V-18760843-7', '659747fd917f32c719b276ecea9515080b105cf1504c4e241fcc6d519ea962fb-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3701, 516, 'V-18760843-7', 'b9f7659765fe892bb14f3024ebddcca5b181eaa49f5924e16c772dcb718fcd0f-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 2, '2014-12-31', 2, '1');
INSERT INTO infodocumento VALUES (3705, 518, 'V-18760843-7', 'a2299896b628c1798e85d68a8bde231b2cfa3753d6a524711acb5fce38eeca1a-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3703, 515, 'J-121188-9', '3043e5164e76c4e02101cdd2581d3ca37d2a383e54274888b4b73b97e1aaa491-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3707, 517, 'J-121188-9', '465717bdf0c25516f05ce1c5516ff00b2ad9e5e943e43cad612a7996c12a8796-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 2, '2010-10-25', 1, '0');
INSERT INTO infodocumento VALUES (3717, 524, 'V-18760843-7', 'f315c06bf0c4da671672bd0b0651ee4e240c945bb72980130446fc2b40fbb8e0-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 1, 0, 1, '2008-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3709, 518, 'J-121188-9', 'a2299896b628c1798e85d68a8bde231b56db57b4db0a6fcb7f9e0c0b504f6472-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 2, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3721, 519, 'J-121188-9', '0b6093e91d6c565882bd1af19c7b388f9e406957d45fcb6c6f38c2ada7bace91-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3723, 519, 'J-121188-9', '0b6093e91d6c565882bd1af19c7b388fafa299a4d1d8c52e75dd8a24c3ce534f-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'pdf', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3725, 520, 'J-121188-9', '6791589ec6f4de6c8055aa5654bae2c994ef7214c4a90790186e255304f8fd1f-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3727, 522, 'J-121188-9', '6a16ad0007dca405d7cce200f23f237dd3802b1dc0d80d8a3c8ccc6ccc068e7c-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3728, 523, 'J-121188-9', '922f79842924cbd161fa93420b178793460b491b917d4185ed1f5be97229721a-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 1, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2421, 266, '4567890', '433f9dfc3feead72372eba69f6a3954ee139c454239bfde741e893edb46a06cc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 2, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3615, 266, '4567890', '3db92bcc9cd4bf413657883d636efb0bd8c9d05ec6e86d5bbad7a2f88a1701d0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'pdf', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3658, 500, 'J-121188-9', 'f6baea765e649a9b9a4fa0166f02a5ea30a237d18c50f563cba4531f1db44acf-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, '1999-12-25', 1, '0');
INSERT INTO infodocumento VALUES (3662, 499, 'V-18760843-7', 'f6b121340c85b55db6c672e33c283aae2b346a0aa375a07f5a90a344a61416c4-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 1, '2008-01-01', 1, '0');
INSERT INTO infodocumento VALUES (3668, 501, 'V-18760843-7', '2604c1897bbc3f7e6c7be05be59db36a1d2a48c55f6f10010887cc7d849469a1-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 8, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3674, 504, 'V-18760843-7', '752fe60b9e0543e7b8d25a6f6bd29527f35fd567065af297ae65b621e0a21ae9-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 2, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3671, 505, 'J-121188-9', 'e95efe46ecfc71e51d415f00d36a213854ebdfbbfe6c31c39aaba9a1ee83860a-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3677, 508, 'J-121188-9', '450bed356087d626e24a049b3f35defd71d7232b9fed020ca23729017873089e-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'pdf', 1, 0, 1, '2014-12-24', 1, '0');
INSERT INTO infodocumento VALUES (3680, 505, 'V-18760843-7', 'e95efe46ecfc71e51d415f00d36a21382122c699d5e3d2fa6690771845bd7904-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3665, 500, 'V-18760843-7', 'f6baea765e649a9b9a4fa0166f02a5ea6915849303a3fe93657587cb9c469f00-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 1, '2014-12-31', 2, '1');
INSERT INTO infodocumento VALUES (3686, 508, 'V-18760843-7', '450bed356087d626e24a049b3f35defddc727151e5d55dde1e950767cf861ca5-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 1, '2015-01-01', 1, '0');
INSERT INTO infodocumento VALUES (3689, 511, 'J-121188-9', '5a4d1129f7c7576fe00153e97a9498be39ea40e164f970c54b0530436d5a9f7a-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'pdf', 2, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3683, 507, 'V-18760843-7', '9ac235aa477ee62c435f98721264a307acb5d1120b8a0b8d3d97905ba9a72dc4-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 1, '2008-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3704, 517, 'V-18760843-7', '465717bdf0c25516f05ce1c5516ff00b9308b0d6e5898366a4a986bc33f3d3e7-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 3, '0012-12-14', 1, '0');
INSERT INTO infodocumento VALUES (3700, 513, 'J-121188-9', 'e8be9944201791ffd0a128644a2c61a5f92586a25bb3145facd64ab20fd554ff-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 3, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3712, 520, 'V-18760843-7', '6791589ec6f4de6c8055aa5654bae2c987ae6fb631f7c8a627e8e28785d9992d-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3716, 514, 'J-121188-9', '659747fd917f32c719b276ecea9515081b32a022c52c0c6255c2a32e580be34f-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'jpg', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3698, 513, 'J-121188-9', 'e8be9944201791ffd0a128644a2c61a5943aa0fcda4ee2901a7de9321663b114-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'pdf', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3720, 517, 'J-121188-9', '465717bdf0c25516f05ce1c5516ff00b532b81fa223a1b1ec74139a5b8151d12-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 2, 0, 9, '2012-12-12', 1, '0');
INSERT INTO infodocumento VALUES (3710, 518, 'J-121188-9', 'f974271bff094dfb078d7474631f6eea5505712229fb1eb500efadddc0353264-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 3, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3702, 514, 'J-121188-9', '659747fd917f32c719b276ecea951508a928731e103dfc64c0027fa84709689e-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3706, 516, 'J-121188-9', 'b9f7659765fe892bb14f3024ebddcca54764f37856fc727f70b666b8d0c4ab7a-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 4, '2010-11-12', 1, '0');
INSERT INTO infodocumento VALUES (3722, 519, 'J-121188-9', '0b6093e91d6c565882bd1af19c7b388f56e6a93212e4482d99c84a639d254b67-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 2, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3714, 522, 'V-18760843-7', '6a16ad0007dca405d7cce200f23f237de36286b94d3c219f414e0427e5f73aa5-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3708, 518, 'J-121188-9', 'a2299896b628c1798e85d68a8bde231b34ffeb359a192eb8174b6854643cc046-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'pdf', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3724, 519, 'J-121188-9', '0b6093e91d6c565882bd1af19c7b388f4172f3101212a2009c74b547b6ddf935-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 4, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3726, 521, 'J-121188-9', '0b6f1631be58085481ba4af66a2455529fe77ac7060e716f2d42631d156825c0-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (2481, 266, '4567890', '611058b8ca5c96bf67b019961346ef196b5754d737784b51ec5075c0dc437bf0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 4, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3614, 266, '4567890', '35ba546a1e759b0d787e5a38236bf2066add07cf50424b14fdf649da87843d01-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3634, 1, '01', 'a739cf3f0c217c270b0b7298068ba291e2eabaf96372e20a9e3d4b5f83723a61-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 6, 0, 2, '2014-11-01', 1, '0');
INSERT INTO infodocumento VALUES (3635, 1, '01', 'a739cf3f0c217c270b0b7298068ba291a098b2eb3138551138d127925d092d67-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 7, 0, 1, '2014-11-02', 1, '0');
INSERT INTO infodocumento VALUES (3637, 1, '01', 'a739cf3f0c217c270b0b7298068ba291cae82d4350cc23aca7fc9ae38dab38ab-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 8, 0, 1, '2014-11-04', 1, '0');
INSERT INTO infodocumento VALUES (3619, 181, '17718114', 'c02e7d715d0b7654044a8aabce2df4ff820e694038fadbf9b60b834215b46fdb-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 1, 1, 1, '2015-06-01', 1, '0');
INSERT INTO infodocumento VALUES (3633, 1, '01', 'a739cf3f0c217c270b0b7298068ba291acf922154627f6788918f03c42b123cd-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 5, 0, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento VALUES (961, 121, '17718114', '50ef4bbd4e96f8b0f5d8dfbd34fe7564d707329bece455a462b58ce00d1194c9-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2013-11-11', 2, '1');
INSERT INTO infodocumento VALUES (3618, 101, '17718114', '698088b8b14259335d27d21f274a26ec4da9d7b6d119db4d2d564a2197798380-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 1, 1, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3624, 205, '17718114', 'd4673fe680003eb8dc833c855a86474d5ee0070c40a7c781507b38c59c3eb8d4-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 2, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3620, 202, '17718114', '393f8e732b7de8915548848bb41c04284baf54f36935058bcc696fcef3f4689b-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 1, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3639, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd29527ba304f3809ed31d0ad97b5a2b5df2a39-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 5, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (1, 1, '01', 'a739cf3f0c217c270b0b7298068ba291c4ca4238a0b923820dcc509a6f75849b-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 1, 0, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento VALUES (3666, 497, 'J-121188-9', '5742286f3c6907872e67f5c203d4e5b11ea97de85eb634d580161c603422437f-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 2, 0, 4, '2014-11-14', 1, '0');
INSERT INTO infodocumento VALUES (3622, 101, '17718114', '83cf6fa4e9105b562802851cb157c17b856b503e276cc491e7e6e0ac1b9f4b17-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3621, 121, '17718114', '3228776faa96535be20953fd9e7b656ec5b270a763686e776039618cc709f3a6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, '2013-11-11', 1, '0');
INSERT INTO infodocumento VALUES (3623, 202, '17718114', '6fca27aa24f930e739c66dd70b8e43acecdcd675b3a4cbb5578baf72f255ec21-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3663, 503, 'J-121188-9', 'c66dc80afdea51b798b3913974cc38dfddf9029977a61241841edeae15e9b53f-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'pdf', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3641, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd2952722c5a901070d1c2ad33e821d071ae97e-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 6, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3631, 1, '01', 'aa9c3d1e6d5aa59b02e38843dbe92c78fd4c2dc64ccb8496e6f1f94c85f30d06-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 4, 0, 1, '2014-10-29', 2, '0');
INSERT INTO infodocumento VALUES (3669, 504, 'J-121188-9', '752fe60b9e0543e7b8d25a6f6bd29527e1228be46de6a0234ac22ded31417bc7-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 2, 0, 5, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3659, 501, 'J-121188-9', '2604c1897bbc3f7e6c7be05be59db36a07845cd9aefa6cde3f8926d25138a3a2-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3675, 506, 'J-121188-9', '3e26498adf21c45b43d3ae923a5fda63845f3cb43a07259b2e4724dfa5c5c0d1-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'pdf', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3678, 509, 'J-121188-9', '4c6c2287ced2e3a3243029b0fcc925e32151b4c76b4dcb048d06a5c32942b6f6-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 4, '2010-10-10', 1, '0');
INSERT INTO infodocumento VALUES (3684, 510, 'J-121188-9', '9a327d1072fabff1a9057bca9db13bd7c90e274d55309db944076afb3ff9c391-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'jpg', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3687, 510, 'V-18760843-7', '9a327d1072fabff1a9057bca9db13bd76754e06e46dfa419d5afe3c9781cecad-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 10, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3681, 506, 'V-18760843-7', '3e26498adf21c45b43d3ae923a5fda639426c311e76888b3b2368150cd05f362-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3672, 503, 'V-18760843-7', 'c66dc80afdea51b798b3913974cc38dff3c89b7be367aa4246f90aa007efe525-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 2, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3696, 513, 'J-121188-9', 'e8be9944201791ffd0a128644a2c61a595c9d994f8d75d4d60f8bb8f25902339-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3690, 512, 'V-18760843-7', 'c964af849361c451de597f91ff444a6a781397bc0630d47ab531ea850bddcf63-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3649, 493, 'J-121188-9', '8e7b805b563975a276e133302eca520549c0fa7f96aa0a5fb95c62909d5190a6-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/f3e369b0bd9c6a23cf86b8a62c9eaca3', 'pdf', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3656, 497, 'V-18760843-7', '5742286f3c6907872e67f5c203d4e5b114678db82874f1456031fcc05a3afaf6-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 1, '2008-11-30', 1, '0');
INSERT INTO infodocumento VALUES (3652, 496, 'J-121188-9', 'ef857acb97c2e362b608f99c0255bb0ce1021d43911ca2c1845910d84f40aeae-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 2, '2014-11-12', 1, '0');
INSERT INTO infodocumento VALUES (3654, 497, 'J-121188-9', '5742286f3c6907872e67f5c203d4e5b1cf88118aa2ba88de549d08038ae76606-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, '2010-10-10', 1, '0');
INSERT INTO infodocumento VALUES (3655, 498, 'J-121188-9', 'ea26359f89d921345bc68fec7606720a310cc7ca5a76a446f85c1a0d641ba96d-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 3, '2008-09-14', 1, '0');
INSERT INTO infodocumento VALUES (3653, 496, 'V-18760843-7', 'ef857acb97c2e362b608f99c0255bb0c2da6cc4a5d3a7ee43c1b3af99267ed17-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 1, '2008-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3729, 524, 'J-121188-9', 'f315c06bf0c4da671672bd0b0651ee4ecb16b8498f74ba6b6a6873518624168c-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'pdf', 1, 0, 1, '2008-08-15', 1, '0');
INSERT INTO infodocumento VALUES (3730, 524, 'J-121188-9', 'f315c06bf0c4da671672bd0b0651ee4ed8c24ca8f23c562a5600876ca2a550ce-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'jpg', 2, 0, 1, '2006-06-06', 1, '0');
INSERT INTO infodocumento VALUES (3731, 524, 'J-121188-9', 'f315c06bf0c4da671672bd0b0651ee4e7ec3b3cf674f4f1d23e9d30c89426cce-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 3, 0, 3, '2015-10-15', 1, '0');
INSERT INTO infodocumento VALUES (3732, 525, 'J-121188-9', 'fbc4b2a492ed2e54929eb69312301c1eee23e7ad9b473ad072d57aaa9b2a5222-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3733, 525, 'J-121188-9', 'fbc4b2a492ed2e54929eb69312301c1e64d52e08cc03e6090bc1ef30b73ccb85-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 2, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3734, 525, 'J-121188-9', 'fbc4b2a492ed2e54929eb69312301c1e9d752cb08ef466fc480fba981cfa44a1-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'pdf', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3735, 525, 'J-121188-9', 'fbc4b2a492ed2e54929eb69312301c1edc0c398086fee58f9d64e1e47aa4e586-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/099c0795e3716324aec82b439a8c502d', 'tif', 4, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3651, 495, 'V-18760843-7', 'e42de1fa95baac471eecbf8f1f312138c203e4a1bdef9372cb9864bfc9b511cc-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 3, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3748, 504, 'V-18760843-7', 'd189873643060ba28a0f6f61f8097e8daaaccd2766ec67aecbe26459bb828d81-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3745, 500, 'V-18760843-7', '268bc283be64fb07fa6b9c605f4f7df76d3a2d24eb109dddf78374fe5d0ee067-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, '2014-11-30', 2, '0');
INSERT INTO infodocumento VALUES (3739, 505, 'J-121188-9', 'a468fdffd7811ba8dc55d852991ff0b220885c72ca35d75619d6a378edea9f76-1', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'pdf', 1, 1, 5, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3736, 509, 'J-121188-9', '2bbdad9beddd8ad159e38cfdb6b2d54c3e195b0793297114c668f772c6e2d9ba-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'pdf', 2, 0, 1, '2014-11-10', 1, '0');
INSERT INTO infodocumento VALUES (3737, 510, 'J-121188-9', '513425f8a44886209d5a2958674baffb3db11d259a9db7fb8965bdf25ec850b9-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3738, 511, 'J-121188-9', 'bf9ca340b6a15b3bb468e92183c95e4f16738419b15b05e74e1ecb164430bfa8-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/a00448e77d1d88ac80b181e804fab3d0', 'jpg', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3752, 530, 'J-12345678-9', 'ea26359f89d921345bc68fec7606720a48df7b8e8d586a55cf3e7054a4c85b30-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tif', 1, 0, 1, '2008-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3742, 513, 'V-18760843-7', '96b83fd9a862648752a55caa06da7e9c1f72e258ff730035f2a1fb6637f562c2-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'jpg', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3743, 515, 'V-18760843-7', '8bdbe42c37b9c7c6664ce361ab2be5d90937fb5864ed06ffb59ae5f9b5ed67a9-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'tif', 1, 0, 2, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3744, 516, 'V-18760843-7', 'c951025f677c6e3fabd27d61180d42fc56517f19aa289885c43e8db9137fb1b0-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/6de4a97495bc4e84fbbd589674f26802', 'pdf', 1, 0, 1, '2014-12-31', 0, '0');
INSERT INTO infodocumento VALUES (3747, 503, 'V-18760843-7', '6948cfd7d2033253dcd5d8c28370fdc1d8847be3f7cc1b14e9173908bebb2106-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3740, 495, 'V-18760843-7', '50c16aed69a325a480c4540e319d129633ef701c8059391708f1c3ddbe9f1f81-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3741, 498, 'V-18760843-7', '4c08de9bbff6d21ec6f940a7acfebd4663ce12dcf1ede17589befd56bb5281a5-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, '2014-12-12', 2, '0');
INSERT INTO infodocumento VALUES (3746, 501, 'V-18760843-7', 'f3c4d803a29f297b49fca3c2510cf43ce8542a04d734d0cae36d648b3f519e5c-0', '95baf23963160f8d78f3dd069fb5b0bd/b90fcd3ae764daea9c8a78ee93ef2d75/7916024c8c98e2de97dbbcba2b2c5480', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3753, 533, 'J-12345678-9', '2604c1897bbc3f7e6c7be05be59db36a258e130476290221f597c56d351224b6-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3771, 527, 'J-12345678-9', '32c2517cc3189fe7bb8f036c2be09ef7201e5bacd665709851b77148e225b332-1', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'pdf', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3754, 535, 'J-12345678-9', 'c66dc80afdea51b798b3913974cc38df2e3d2c4f33a7a1f58bc6c81cacd21e9c-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3767, 536, 'J-12345678-9', '752fe60b9e0543e7b8d25a6f6bd29527d74cb35426f3d808325876f45b69dbf1-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tiff', 1, 0, 4, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3772, 527, 'J-12345678-9', '88f6a66d5b64c78f2e51b9de235d481483ddfbd1c4f871159c148d7a010e69be-2', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'jpg', 1, 2, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3749, 527, 'J-12345678-9', 'e15cb92c1d02408c846b4242062f33a0f9beb1e831faf6aaec2a5cecaf1af293-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3750, 528, 'J-12345678-9', 'ef857acb97c2e362b608f99c0255bb0c685ac8cadc1be5ac98da9556bc1c8d9e-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tif', 1, 0, 1, '2010-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3751, 529, 'J-12345678-9', '5742286f3c6907872e67f5c203d4e5b121ce689121e39821d07d04faab328370-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tif', 1, 0, 1, '2009-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3768, 536, 'J-12345678-9', '752fe60b9e0543e7b8d25a6f6bd2952728d437661d95291767e7402dfe969962-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'jpg', 2, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3777, 539, 'J-12345678-9', '9ac235aa477ee62c435f98721264a307add7a048049671970976f3e18f21ade3-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/88d6a5eb1f6f896882e690b184f1735e', 'tif', 1, 0, 1, '2010-12-31', 1, '0');
INSERT INTO infodocumento VALUES (3776, 538, 'J-12345678-9', '3e26498adf21c45b43d3ae923a5fda632119b8d43eafcf353e07d7cb5554170b-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/88d6a5eb1f6f896882e690b184f1735e', 'jpg', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3778, 540, 'J-12345678-9', '450bed356087d626e24a049b3f35defdcab070d53bd0d200746fb852a922064a-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/88d6a5eb1f6f896882e690b184f1735e', 'tif', 1, 0, 1, '2014-11-03', 1, '0');
INSERT INTO infodocumento VALUES (3773, 536, 'J-12345678-9', 'a3c4e397b0e7cd2ef5741772cb4ae1c75c151c2a9b76f9ef26d7e0f0d00c9a89-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tiff', 1, 0, 4, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3774, 536, 'J-12345678-9', 'ecbb7f9571f26e883f70d84b45a6be8a73f104c9fba50050eea11d9d075247cc-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'jpg', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3775, 537, 'J-12345678-9', '1afb512f6eb8420a610708efbc0f1ae6f2b93ce08763fddf54bcb7beb62a2c74-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/88d6a5eb1f6f896882e690b184f1735e', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3779, 530, 'J-12345678-9', 'ea26359f89d921345bc68fec7606720a8ce87bdda85cd44f14de9afb86491884-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tiff', 2, 0, 4, '2014-11-30', 1, '0');
INSERT INTO infodocumento VALUES (3769, 536, 'J-12345678-9', '6f2e345b1c96431251ff3fe413b65a5e4625d8e31dad7d1c4c83399a6eb62f0c-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'jpg', 3, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3807, 581, '144985', '4097de69b33a3ad14ff50612b78b232743a115cbd6f4788924537365be3d6012-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3794, 582, '137397', 'f3646bfe5f59d4b0cd4ff3dfed67ec73916d3891a243c10fede49f9c276f1a20-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3795, 583, '137397', 'fbb5ff5d01ac2f11b1ec4e1d06ace62842547f5a44d87da3bc40ee5d09624606-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3784, 2, '01', 'ebded8a31c5c3b36bad010a310bd57b7e16e74a63567ecb44ade5c87002bb1d9-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 1, 0, 24, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3798, 587, '137397', '8d72d315a88575960facaf51d43e644ac82836ed448c41094025b4a872c5341e-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3785, 2, '01', 'ebded8a31c5c3b36bad010a310bd57b744feb0096faa8326192570788b38c1d1-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tiff', 2, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3783, 533, 'J-12345678-9', '2604c1897bbc3f7e6c7be05be59db36a1aa3d9c6ce672447e1e5d0f1b5207e85-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tiff', 3, 0, 4, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3781, 534, 'J-12345678-9', '3e85182069d5d9dd7b0b81f8fc708b9a9fc664916bce863561527f06a96f5ff3-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'PDF', 1, 0, 2, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3782, 535, 'J-12345678-9', 'c66dc80afdea51b798b3913974cc38dff87e955fd6b89f8963b6934beb077d6e-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tiff', 2, 0, 4, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3780, 533, 'J-12345678-9', '7ee947c20a43ccf0b5faa1a992368a873569df159ec477451530c4455b2a9e86-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tiff', 2, 0, 4, NULL, 2, '1');
INSERT INTO infodocumento VALUES (3786, 533, 'J-12345678-9', 'f500f03d04212b634f8ceb8c7319e11c34306d99c63613fad5b2a140398c0420-0', '923ae9bfecfe4786b2430a051b939b6b/7f9cd1b02dc5c3e8a38ff49792ad8bf8/67fbe850421a9187916a39b77dc30af7', 'tiff', 2, 0, 4, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3801, 573, '144985', '35f16ae87de3d7a1ecd64d826ee4cb7895323660ed2124450caaac2c46b5ed90-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, '2007-08-06', 0, '0');
INSERT INTO infodocumento VALUES (3799, 590, '137397', '752fe60b9e0543e7b8d25a6f6bd2952758521e4e2bd3d4b988cbd17d7365df3c-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 9, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3805, 577, '144985', '01c2e6e78cdd8d1e88fbec4a604418b098cac9d33aad44bb31800130e8c50b5f-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3792, 580, '137397', '923e87e5be075189f373b21e0f12cb2f4ecb679fd35dcfd0f0894c399590be1a-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3796, 584, '137397', '1275797809c15f8cd3aaed9564c9ddc341c542dfe6e4fc3deb251d64cf6ed2e4-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 6, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3808, 582, '144985', 'f3646bfe5f59d4b0cd4ff3dfed67ec73cd8d5260c8131ca7aeea5d41796d1a0a-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3800, 463, '144985', '35f16ae87de3d7a1ecd64d826ee4cb7863c6598e9ddd2961e7dfa4d4eb8144a1-0', 'a911933bc1345b3a6f5e5a3be99d1e89/1f0e809096a7c7f949ff3fe19f2e3e00/eff82a8057fe7592e506697f08c5f1e4', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3787, 573, '137397', '35f16ae87de3d7a1ecd64d826ee4cb78f3175210f90bfc7ea82901db0ef7452f-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, '2005-06-06', 1, '0');
INSERT INTO infodocumento VALUES (3788, 574, '137397', '325fd8163b8b0ef8d5048c5aef1b9f5bdeb74a85a4a68465b75c721d45db5d3b-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 2, '2004-12-08', 2, '0');
INSERT INTO infodocumento VALUES (3802, 574, '144985', '2d6c9e9c8a10cd696d6fb223cd60ef79d3614f463b2d42aba2700556d42740d0-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, '2005-07-15', 0, '0');
INSERT INTO infodocumento VALUES (3793, 581, '137397', '4097de69b33a3ad14ff50612b78b2327c157297d1a1ff043255bfb18530caaa2-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3803, 575, '144985', 'f8e4976aa5766488de2b2bb458553edde97399278d24e6bbf3a2d5e9c8d34262-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 7, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3804, 576, '144985', 'e8e536b925e0a050beb32316faba494537968ad196a5085f5ed91e58df8c2cef-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3806, 580, '144985', '923e87e5be075189f373b21e0f12cb2f5e7264477cf9b6b237a0d254cf0324e2-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3790, 576, '137397', 'e8e536b925e0a050beb32316faba4945e73fecc08ee9b0e1a876614ec3178bac-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3791, 577, '137397', '01c2e6e78cdd8d1e88fbec4a604418b0690bb330e5e7e3e07867fafc4d32ec82-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3797, 585, '137397', 'ebf0c71a99a556f29588365b03b352bf36072923bfc3cf47745d704feb489480-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3809, 583, '144985', 'fbb5ff5d01ac2f11b1ec4e1d06ace62838ef4b66cb25e92abe4d594acb841471-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 2, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3810, 587, '144985', '8d72d315a88575960facaf51d43e644a02ae6a786bbf135d3d223cbc0e770b6e-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3811, 1, '01', 'a739cf3f0c217c270b0b7298068ba2918068fee5f49946b3a8f85b1007cd40bb-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 9, 0, 3, '2015-01-29', 1, '0');
INSERT INTO infodocumento VALUES (3832, 577, '245689', '01c2e6e78cdd8d1e88fbec4a604418b0ea9268cb43f55d1d12380fb6ea5bf572-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tiff', 1, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3814, 1, '01', 'a739cf3f0c217c270b0b7298068ba291c563c2c394023a07d56ad6b3eb09537a-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 10, 0, 6, '2015-01-29', 1, '0');
INSERT INTO infodocumento VALUES (3816, 573, '144985', '35f16ae87de3d7a1ecd64d826ee4cb78c911241d00294e8bb714eee2e83fa475-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tiff', 2, 0, 1, '2015-05-31', 0, '0');
INSERT INTO infodocumento VALUES (3817, 574, '144985', '2d6c9e9c8a10cd696d6fb223cd60ef79fa6c94460e902005a0b660266190c8ba-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tiff', 2, 0, 1, '2014-03-03', 1, '0');
INSERT INTO infodocumento VALUES (3833, 1, '02', 'a739cf3f0c217c270b0b7298068ba29165b1e92c585fd4c2159d5f33b5030ff2-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 1, 0, 1, '2015-12-24', 0, '0');
INSERT INTO infodocumento VALUES (3818, 590, '144985', '752fe60b9e0543e7b8d25a6f6bd29527b994697479c5716eda77e8e9713e5f0f-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tiff', 1, 0, 4, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3819, 590, '144985', '752fe60b9e0543e7b8d25a6f6bd29527eb1848290d5a7de9c9ccabc67fefa211-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tiff', 2, 0, 4, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3820, 574, '144985', '334514aca5d8fdcf172ddbe117f6617891e50fe1e39af2869d3336eaaeebdb43-1', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 2, 1, 3, '2015-05-29', 0, '0');
INSERT INTO infodocumento VALUES (3827, 589, '192785', 'a7b10532accb7d13ff37f70cf4484bedb5b8c484824d8a06f4f3d570bc420313-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3826, 585, '192785', 'ebf0c71a99a556f29588365b03b352bfa292f1c5874b2be8395ffd75f313937f-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3821, 574, '144985', '2d6c9e9c8a10cd696d6fb223cd60ef79c04c19c2c2474dbf5f7ac4372c5b9af1-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tiff', 3, 0, 1, '2014-03-03', 2, '1');
INSERT INTO infodocumento VALUES (3822, 574, '144985', 'fccc4466d45f4c62d903fd99116203e20e57098d0318a954d1443e2974a38fac-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 3, 0, 7, '2015-03-31', 0, '0');
INSERT INTO infodocumento VALUES (3828, 577, '192785', '01c2e6e78cdd8d1e88fbec4a604418b046d3f6029f6170ebccb28945964d09bf-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tiff', 1, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3825, 575, '192785', 'ea3d85914a75a95b5b0f8358eab96653c24fe9f765a44048868b5a620f05678e-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 2, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3815, 1, '01', 'a739cf3f0c217c270b0b7298068ba291143758ee65fb29d30caa170c0db0ed36-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 11, 0, 1, '2015-02-26', 1, '0');
INSERT INTO infodocumento VALUES (3824, 574, '192785', '2d6c9e9c8a10cd696d6fb223cd60ef7967974233917cea0e42a49a2fb7eb4cf4-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, '2013-12-16', 1, '0');
INSERT INTO infodocumento VALUES (3829, 575, '192785', 'ea3d85914a75a95b5b0f8358eab96653b7d0858d41a6c29b873e4aba411e6d04-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 2, 0, 2, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3830, 573, '245689', '35f16ae87de3d7a1ecd64d826ee4cb78fcde14913c766cf307c75059e0e89af5-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 1, '2015-05-29', 1, '0');
INSERT INTO infodocumento VALUES (3831, 574, '245689', '2d6c9e9c8a10cd696d6fb223cd60ef79cac8e13055d2e4f62b6322254203b293-0', '53658a7292aafcd73f19ee645450724b/8f1800bf6b141ee58208b0beb62cad84/86fee854b569eb55241c85aff80e5125', 'tif', 1, 0, 5, '2015-09-30', 2, '0');
INSERT INTO infodocumento VALUES (3835, 47, '250001', '48b355f3ee56b38530b0961a7564203f1c66f4627f1b9679f8db47d1287d7e98-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3836, 48, '250001', '8e23812a502d05e21a292b885ad1a09a6e2eec9ca19c076736d19ac5426473af-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 3, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3839, 47, '250002', '48b355f3ee56b38530b0961a7564203f9b2f00f37307f2c2f372acafe55843f3-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 2, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3851, 50, '785002', '0983f65f10dabe052eda4876d622a719dc0439caeb74ffc2795571af07a7eab1-0', '0c7fe18f65d75112a5682b1852d61dbf/e5a0fbac84f76fd5f2afd46478ab0330/a469a1ec5034e4d269312af394429aa2', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3840, 48, '250002', '8e23812a502d05e21a292b885ad1a09adfa037a53e121ecc9e0926800c3e814e-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3838, 46, '250002', 'a13a340b02ff4959e375e66434d7a5d6f26df67e8110ee2b44923db775e3e47f-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3841, 46, '250003', 'a13a340b02ff4959e375e66434d7a5d67da18d0326a9f46a4817e19c805819ae-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3844, 46, '250003', 'a13a340b02ff4959e375e66434d7a5d6494c08f7a144d3cc4cfa661ed1244039-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 2, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3843, 48, '250003', '8e23812a502d05e21a292b885ad1a09a9eac167ec1efbe078138397fabba902e-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3845, 46, '250004', 'a13a340b02ff4959e375e66434d7a5d6c6d6445d97e06d08b60853156601cf58-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3842, 47, '250003', '48b355f3ee56b38530b0961a7564203f7fa215c9efebb3811a7ef58409907899-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3846, 47, '250004', '48b355f3ee56b38530b0961a7564203f6236c78e73f52110ae39e588ba88de0b-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3847, 48, '250004', '8e23812a502d05e21a292b885ad1a09aa3048e47310d6efaa4b1eaf55227bc92-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3849, 50, '785001', '0983f65f10dabe052eda4876d622a7194f5a97cf06cf69028997db51d8726d28-0', '0c7fe18f65d75112a5682b1852d61dbf/e5a0fbac84f76fd5f2afd46478ab0330/a469a1ec5034e4d269312af394429aa2', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3855, 3, '322301', '618fde607bb69b1f02d7bfdfc2f62f9fd58f855fdcc76daf232aee454c4e59f7-0', '0c7fe18f65d75112a5682b1852d61dbf/4caf5c8cff584301ccec637cb77c422e/be26010778cef1c7888af1978c5b79d8', 'tif', 1, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3848, 49, '785001', '5d97b8786e633bbf8da100fb9465b6ed42c8938e4cf5777700700e642dc2a8cd-0', '0c7fe18f65d75112a5682b1852d61dbf/e5a0fbac84f76fd5f2afd46478ab0330/a469a1ec5034e4d269312af394429aa2', 'tif', 1, 0, 8, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3852, 50, '785002', '0983f65f10dabe052eda4876d622a719582967e09f1b30ca2539968da0a174fa-0', '0c7fe18f65d75112a5682b1852d61dbf/e5a0fbac84f76fd5f2afd46478ab0330/a469a1ec5034e4d269312af394429aa2', 'tif', 2, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3850, 49, '785002', '5d97b8786e633bbf8da100fb9465b6edf550e0ba9e1c4e8bb4a5ed0ac23a952d-0', '0c7fe18f65d75112a5682b1852d61dbf/e5a0fbac84f76fd5f2afd46478ab0330/a469a1ec5034e4d269312af394429aa2', 'tif', 1, 0, 10, NULL, 2, '0');
INSERT INTO infodocumento VALUES (3853, 49, '785003', '5d97b8786e633bbf8da100fb9465b6edb9f35816f460ab999cbc168c4da26ff3-0', '0c7fe18f65d75112a5682b1852d61dbf/e5a0fbac84f76fd5f2afd46478ab0330/a469a1ec5034e4d269312af394429aa2', 'tif', 1, 0, 13, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3854, 50, '785003', '0983f65f10dabe052eda4876d622a71931c0b36aef265d9221af80872ceb62f9-0', '0c7fe18f65d75112a5682b1852d61dbf/e5a0fbac84f76fd5f2afd46478ab0330/a469a1ec5034e4d269312af394429aa2', 'tif', 1, 0, 4, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3856, 8, '322301', '4f4e9e1bbac6e85d7019be69d6c769644c2e5eaae9152079b9e95845750bb9ab-0', '0c7fe18f65d75112a5682b1852d61dbf/4caf5c8cff584301ccec637cb77c422e/08b135689b0e63cef79e2db908aed727', 'tif', 1, 0, 4, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3859, 592, '67876001', '618fde607bb69b1f02d7bfdfc2f62f9f7171e95248ff768e1ebee3edde01ea7a-0', '0c7fe18f65d75112a5682b1852d61dbf/fd6b255fc003658b6ee1b72448e61b45/be26010778cef1c7888af1978c5b79d8', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3857, 3, '322302', '618fde607bb69b1f02d7bfdfc2f62f9ff269cb7796c3319c9aa4d146b52139e6-0', '0c7fe18f65d75112a5682b1852d61dbf/4caf5c8cff584301ccec637cb77c422e/be26010778cef1c7888af1978c5b79d8', 'tif', 1, 0, 7, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3858, 8, '322302', '4f4e9e1bbac6e85d7019be69d6c7696450dd7100bcbd98c41b1179143a2325a4-0', '0c7fe18f65d75112a5682b1852d61dbf/4caf5c8cff584301ccec637cb77c422e/08b135689b0e63cef79e2db908aed727', 'tif', 1, 0, 3, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3834, 46, '250001', 'a13a340b02ff4959e375e66434d7a5d6243be2818a23c980ad664f30f48e5d19-0', '0c7fe18f65d75112a5682b1852d61dbf/217041e454b3f05b9c2a0be8e2e89807/f708352367c4c18cfdf00a03fdae3636', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento VALUES (3861, 592, '67876002', '618fde607bb69b1f02d7bfdfc2f62f9fa6197a578fe7778e8d49a95ac425bcfc-0', '0c7fe18f65d75112a5682b1852d61dbf/fd6b255fc003658b6ee1b72448e61b45/be26010778cef1c7888af1978c5b79d8', 'tif', 1, 0, 3, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3862, 602, '67876002', '9888e8630ca8251ba28c56711b95fdef533fa796b43291fc61a9e812a50c3fb6-0', '0c7fe18f65d75112a5682b1852d61dbf/fd6b255fc003658b6ee1b72448e61b45/0d02574cacd311540fa4500336d36e05', 'tif', 1, 0, 10, NULL, 0, '0');
INSERT INTO infodocumento VALUES (3860, 596, '67876001', '8fe00197f847cffdf84593a7ff4522275a5eab21ca2a8fef4af5e35709ecca15-0', '0c7fe18f65d75112a5682b1852d61dbf/fd6b255fc003658b6ee1b72448e61b45/08b135689b0e63cef79e2db908aed727', 'tif', 1, 0, 8, NULL, 1, '0');


--
-- TOC entry 2419 (class 0 OID 21093)
-- Dependencies: 207
-- Data for Name: libreria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO libreria VALUES (21, '01.-Gerencia de Regímenes Aduaneros', 1);
INSERT INTO libreria VALUES (41, '02.-  REGÍMENES ADUANEROS – DEPARTAMENTO DE DRAW BACK', 1);
INSERT INTO libreria VALUES (61, 'LIBRERÍA:03.- PRUEBA - REGÍMENES ADUANEROS – DIVISIÓN AUXILIARES ADUANEROS', 1);
INSERT INTO libreria VALUES (1, 'LibreriaPrueba', 1);
INSERT INTO libreria VALUES (101, '-LIBRERÍA:02.-  REGÍMENES ADUANEROS – DEPARTAMENTO DEDRAW BACK', 1);
INSERT INTO libreria VALUES (103, '03.- 2 PRUEBA - REGÍMENES ADUANEROS - DIVISIÓN AUXILIARES ADUANEROS', 1);
INSERT INTO libreria VALUES (104, '+LIBRERÍA:03.-  PRUEBA - REGÍMENES ADUANEROS – DIVISIÓN AUXILIARES ADUANEROS', 1);
INSERT INTO libreria VALUES (105, '04.-  PRUEBA - REGÍMENES ADUANEROS – DIVISIÓN AUXILIARES ADUANEROS', 1);
INSERT INTO libreria VALUES (81, '03.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS CAPACITADOS ADUANEROS  (AA-CA)', 1);
INSERT INTO libreria VALUES (2, '01 - FDPS - LIBRERIA PRUEBAS EXPEDIENTES', 1);


--
-- TOC entry 2421 (class 0 OID 21099)
-- Dependencies: 209
-- Data for Name: lista_desplegables; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO lista_desplegables VALUES (2, 4, 'Valor2');
INSERT INTO lista_desplegables VALUES (3, 4, 'Valor3');
INSERT INTO lista_desplegables VALUES (4, 4, 'Valor4');
INSERT INTO lista_desplegables VALUES (5, 4, 'Valor5');
INSERT INTO lista_desplegables VALUES (22, 4, 'Dato2');
INSERT INTO lista_desplegables VALUES (23, 4, 'Dato3');
INSERT INTO lista_desplegables VALUES (24, 4, 'Dato4');
INSERT INTO lista_desplegables VALUES (41, 27, 'A1-1-1');
INSERT INTO lista_desplegables VALUES (42, 27, 'A2-1-1');
INSERT INTO lista_desplegables VALUES (43, 27, 'A3-1-1');
INSERT INTO lista_desplegables VALUES (44, 27, 'A4-1-1');
INSERT INTO lista_desplegables VALUES (45, 27, 'A5-1-1');
INSERT INTO lista_desplegables VALUES (46, 27, 'A6-1-1');
INSERT INTO lista_desplegables VALUES (47, 27, 'A7-1-1');
INSERT INTO lista_desplegables VALUES (48, 27, 'A8-1-1');
INSERT INTO lista_desplegables VALUES (49, 27, 'A9-1-1');
INSERT INTO lista_desplegables VALUES (61, 28, 'Recibido');
INSERT INTO lista_desplegables VALUES (62, 28, 'Extemporáneo');
INSERT INTO lista_desplegables VALUES (63, 28, 'Por Análisis');
INSERT INTO lista_desplegables VALUES (64, 28, 'Por Recaudos');
INSERT INTO lista_desplegables VALUES (65, 28, 'Perención');
INSERT INTO lista_desplegables VALUES (66, 28, 'Improcendente');
INSERT INTO lista_desplegables VALUES (67, 28, 'Liquidado');
INSERT INTO lista_desplegables VALUES (81, 43, 'Firma Personal');
INSERT INTO lista_desplegables VALUES (82, 43, 'Persona Jurídica');
INSERT INTO lista_desplegables VALUES (101, 49, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (102, 49, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (103, 49, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (104, 49, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (105, 49, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (106, 49, 'GUANTA');
INSERT INTO lista_desplegables VALUES (107, 49, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (108, 49, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (121, 50, 'Activo');
INSERT INTO lista_desplegables VALUES (122, 50, 'Inactivo');
INSERT INTO lista_desplegables VALUES (123, 50, 'Decaído');
INSERT INTO lista_desplegables VALUES (124, 50, 'Revocado');
INSERT INTO lista_desplegables VALUES (125, 50, 'Suspendido');
INSERT INTO lista_desplegables VALUES (126, 50, 'Cesado');
INSERT INTO lista_desplegables VALUES (141, 382, '2015');
INSERT INTO lista_desplegables VALUES (142, 382, '2014');
INSERT INTO lista_desplegables VALUES (143, 382, '2013');
INSERT INTO lista_desplegables VALUES (144, 382, '2012');
INSERT INTO lista_desplegables VALUES (145, 382, '2011');
INSERT INTO lista_desplegables VALUES (146, 382, '2010');
INSERT INTO lista_desplegables VALUES (161, 403, '2015');
INSERT INTO lista_desplegables VALUES (162, 403, '2014');
INSERT INTO lista_desplegables VALUES (163, 403, '2013');
INSERT INTO lista_desplegables VALUES (164, 403, '2012');
INSERT INTO lista_desplegables VALUES (165, 403, '2011');
INSERT INTO lista_desplegables VALUES (166, 403, '2010');
INSERT INTO lista_desplegables VALUES (181, 423, '2015');
INSERT INTO lista_desplegables VALUES (182, 423, '2014');
INSERT INTO lista_desplegables VALUES (183, 423, '2013');
INSERT INTO lista_desplegables VALUES (184, 423, '2012');
INSERT INTO lista_desplegables VALUES (185, 423, '2011');
INSERT INTO lista_desplegables VALUES (186, 423, '2010');
INSERT INTO lista_desplegables VALUES (201, 442, '2015');
INSERT INTO lista_desplegables VALUES (202, 442, '2014');
INSERT INTO lista_desplegables VALUES (203, 442, '2013');
INSERT INTO lista_desplegables VALUES (204, 442, '2012');
INSERT INTO lista_desplegables VALUES (205, 442, '2011');
INSERT INTO lista_desplegables VALUES (206, 442, '2010');
INSERT INTO lista_desplegables VALUES (221, 461, '1');
INSERT INTO lista_desplegables VALUES (222, 461, '2');
INSERT INTO lista_desplegables VALUES (223, 461, '3');
INSERT INTO lista_desplegables VALUES (224, 461, '4');
INSERT INTO lista_desplegables VALUES (225, 461, '5');
INSERT INTO lista_desplegables VALUES (241, 462, '2015');
INSERT INTO lista_desplegables VALUES (242, 462, '2014');
INSERT INTO lista_desplegables VALUES (243, 462, '2013');
INSERT INTO lista_desplegables VALUES (244, 462, '2012');
INSERT INTO lista_desplegables VALUES (245, 462, '2011');
INSERT INTO lista_desplegables VALUES (246, 462, '2010');
INSERT INTO lista_desplegables VALUES (261, 482, '2015');
INSERT INTO lista_desplegables VALUES (262, 482, '2014');
INSERT INTO lista_desplegables VALUES (263, 482, '2013');
INSERT INTO lista_desplegables VALUES (264, 482, '2012');
INSERT INTO lista_desplegables VALUES (265, 482, '2011');
INSERT INTO lista_desplegables VALUES (266, 482, '2010');
INSERT INTO lista_desplegables VALUES (281, 483, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (282, 483, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (283, 483, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (284, 483, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (285, 483, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (286, 483, 'GUANTA');
INSERT INTO lista_desplegables VALUES (287, 483, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (288, 483, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (301, 523, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (302, 523, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (303, 523, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (304, 523, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (305, 523, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (306, 523, 'GUANTA');
INSERT INTO lista_desplegables VALUES (307, 523, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (308, 523, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (321, 542, '2015');
INSERT INTO lista_desplegables VALUES (322, 542, '2014');
INSERT INTO lista_desplegables VALUES (323, 542, '2013');
INSERT INTO lista_desplegables VALUES (324, 542, '2012');
INSERT INTO lista_desplegables VALUES (325, 542, '2011');
INSERT INTO lista_desplegables VALUES (326, 542, '2010');
INSERT INTO lista_desplegables VALUES (341, 563, '2015');
INSERT INTO lista_desplegables VALUES (342, 563, '2014');
INSERT INTO lista_desplegables VALUES (343, 563, '2013');
INSERT INTO lista_desplegables VALUES (344, 563, '2012');
INSERT INTO lista_desplegables VALUES (345, 563, '2011');
INSERT INTO lista_desplegables VALUES (346, 563, '2010');
INSERT INTO lista_desplegables VALUES (361, 583, '2015');
INSERT INTO lista_desplegables VALUES (362, 583, '2014');
INSERT INTO lista_desplegables VALUES (363, 583, '2013');
INSERT INTO lista_desplegables VALUES (364, 583, '2012');
INSERT INTO lista_desplegables VALUES (365, 583, '2011');
INSERT INTO lista_desplegables VALUES (366, 583, '2010');
INSERT INTO lista_desplegables VALUES (381, 602, '2015');
INSERT INTO lista_desplegables VALUES (382, 602, '2014');
INSERT INTO lista_desplegables VALUES (383, 602, '2013');
INSERT INTO lista_desplegables VALUES (384, 602, '2012');
INSERT INTO lista_desplegables VALUES (385, 602, '2011');
INSERT INTO lista_desplegables VALUES (386, 602, '2010');
INSERT INTO lista_desplegables VALUES (401, 622, '2015');
INSERT INTO lista_desplegables VALUES (402, 622, '2014');
INSERT INTO lista_desplegables VALUES (403, 622, '2013');
INSERT INTO lista_desplegables VALUES (404, 622, '2012');
INSERT INTO lista_desplegables VALUES (405, 622, '2011');
INSERT INTO lista_desplegables VALUES (406, 622, '2010');
INSERT INTO lista_desplegables VALUES (421, 663, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (422, 663, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (423, 663, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (424, 663, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (425, 663, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (426, 663, 'GUANTA');
INSERT INTO lista_desplegables VALUES (427, 663, 'BOCA DE GRITA ');
INSERT INTO lista_desplegables VALUES (428, 663, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (441, 682, '2015');
INSERT INTO lista_desplegables VALUES (442, 682, '2014');
INSERT INTO lista_desplegables VALUES (443, 682, '2013');
INSERT INTO lista_desplegables VALUES (444, 682, '2012');
INSERT INTO lista_desplegables VALUES (445, 682, '2011');
INSERT INTO lista_desplegables VALUES (446, 682, '2010');
INSERT INTO lista_desplegables VALUES (461, 702, '2015');
INSERT INTO lista_desplegables VALUES (462, 702, '2014');
INSERT INTO lista_desplegables VALUES (463, 702, '2013');
INSERT INTO lista_desplegables VALUES (464, 702, '2012');
INSERT INTO lista_desplegables VALUES (465, 702, '2011');
INSERT INTO lista_desplegables VALUES (466, 702, '2010');
INSERT INTO lista_desplegables VALUES (481, 722, '2015');
INSERT INTO lista_desplegables VALUES (482, 722, '2014');
INSERT INTO lista_desplegables VALUES (483, 722, '2013');
INSERT INTO lista_desplegables VALUES (484, 722, '2012');
INSERT INTO lista_desplegables VALUES (485, 722, '2011');
INSERT INTO lista_desplegables VALUES (486, 722, '2010');
INSERT INTO lista_desplegables VALUES (501, 763, '2015');
INSERT INTO lista_desplegables VALUES (502, 763, '2014');
INSERT INTO lista_desplegables VALUES (503, 763, '2013');
INSERT INTO lista_desplegables VALUES (504, 763, '2012');
INSERT INTO lista_desplegables VALUES (505, 763, '2011');
INSERT INTO lista_desplegables VALUES (506, 763, '2010');
INSERT INTO lista_desplegables VALUES (521, 783, '2015');
INSERT INTO lista_desplegables VALUES (522, 783, '2014');
INSERT INTO lista_desplegables VALUES (523, 783, '2013');
INSERT INTO lista_desplegables VALUES (524, 783, '2012');
INSERT INTO lista_desplegables VALUES (525, 783, '2011');
INSERT INTO lista_desplegables VALUES (526, 783, '2010');
INSERT INTO lista_desplegables VALUES (541, 803, '2015');
INSERT INTO lista_desplegables VALUES (542, 803, '2014');
INSERT INTO lista_desplegables VALUES (543, 803, '2013');
INSERT INTO lista_desplegables VALUES (544, 803, '2012');
INSERT INTO lista_desplegables VALUES (545, 803, '2011');
INSERT INTO lista_desplegables VALUES (546, 803, '2010');
INSERT INTO lista_desplegables VALUES (561, 823, '2015');
INSERT INTO lista_desplegables VALUES (562, 823, '2014');
INSERT INTO lista_desplegables VALUES (563, 823, '2013');
INSERT INTO lista_desplegables VALUES (564, 823, '2012');
INSERT INTO lista_desplegables VALUES (565, 823, '2011');
INSERT INTO lista_desplegables VALUES (25, 4, 'Dato55');
INSERT INTO lista_desplegables VALUES (566, 823, '2010');
INSERT INTO lista_desplegables VALUES (581, 843, '2015');
INSERT INTO lista_desplegables VALUES (582, 843, '2014');
INSERT INTO lista_desplegables VALUES (583, 843, '2013');
INSERT INTO lista_desplegables VALUES (584, 843, '2012');
INSERT INTO lista_desplegables VALUES (585, 843, '2011');
INSERT INTO lista_desplegables VALUES (586, 843, '2010');
INSERT INTO lista_desplegables VALUES (601, 863, '2015');
INSERT INTO lista_desplegables VALUES (602, 863, '2014');
INSERT INTO lista_desplegables VALUES (603, 863, '2013');
INSERT INTO lista_desplegables VALUES (604, 863, '2012');
INSERT INTO lista_desplegables VALUES (605, 863, '2011');
INSERT INTO lista_desplegables VALUES (606, 863, '2010');
INSERT INTO lista_desplegables VALUES (621, 901, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (622, 901, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (623, 901, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (624, 901, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (625, 901, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (626, 901, 'GUANTA');
INSERT INTO lista_desplegables VALUES (627, 901, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (628, 901, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (641, 921, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (642, 921, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (643, 921, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (644, 921, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (645, 921, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (646, 921, 'GUANTA');
INSERT INTO lista_desplegables VALUES (647, 921, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (648, 921, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (661, 941, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (662, 941, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (663, 941, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (664, 941, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (665, 941, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (666, 941, 'GUANTA');
INSERT INTO lista_desplegables VALUES (667, 941, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (668, 941, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (681, 961, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (682, 961, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (683, 961, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (684, 961, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (685, 961, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (686, 961, 'GUANTA');
INSERT INTO lista_desplegables VALUES (687, 961, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (688, 961, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (701, 981, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (702, 981, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (703, 981, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (704, 981, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (705, 981, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (706, 981, 'GUANTA');
INSERT INTO lista_desplegables VALUES (707, 981, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (708, 981, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (721, 1001, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (722, 1001, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (723, 1001, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (724, 1001, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (725, 1001, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (726, 1001, 'GUANTA');
INSERT INTO lista_desplegables VALUES (727, 1001, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (728, 1001, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (741, 68, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (742, 68, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (743, 68, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (744, 68, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (745, 68, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (746, 68, 'GUANTA');
INSERT INTO lista_desplegables VALUES (747, 68, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (748, 68, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (761, 69, 'Activo');
INSERT INTO lista_desplegables VALUES (762, 69, 'Inactivo');
INSERT INTO lista_desplegables VALUES (763, 69, 'Decaído');
INSERT INTO lista_desplegables VALUES (764, 69, 'Revocado');
INSERT INTO lista_desplegables VALUES (765, 69, 'Suspendido');
INSERT INTO lista_desplegables VALUES (766, 69, 'Cesado');
INSERT INTO lista_desplegables VALUES (781, 1022, '2015');
INSERT INTO lista_desplegables VALUES (782, 1022, '2014');
INSERT INTO lista_desplegables VALUES (783, 1022, '2013');
INSERT INTO lista_desplegables VALUES (784, 1022, '2012');
INSERT INTO lista_desplegables VALUES (785, 1022, '2011');
INSERT INTO lista_desplegables VALUES (786, 1022, '2010');
INSERT INTO lista_desplegables VALUES (801, 1042, '2015');
INSERT INTO lista_desplegables VALUES (802, 1042, '2014');
INSERT INTO lista_desplegables VALUES (803, 1042, '2013');
INSERT INTO lista_desplegables VALUES (804, 1042, '2012');
INSERT INTO lista_desplegables VALUES (805, 1042, '2011');
INSERT INTO lista_desplegables VALUES (806, 1042, '2010');
INSERT INTO lista_desplegables VALUES (821, 1062, '2015');
INSERT INTO lista_desplegables VALUES (822, 1062, '2014');
INSERT INTO lista_desplegables VALUES (823, 1062, '2013');
INSERT INTO lista_desplegables VALUES (824, 1062, '2012');
INSERT INTO lista_desplegables VALUES (825, 1062, '2011');
INSERT INTO lista_desplegables VALUES (826, 1062, '2010');
INSERT INTO lista_desplegables VALUES (841, 1082, '2015');
INSERT INTO lista_desplegables VALUES (842, 1082, '2014');
INSERT INTO lista_desplegables VALUES (843, 1082, '2013');
INSERT INTO lista_desplegables VALUES (844, 1082, '2012');
INSERT INTO lista_desplegables VALUES (845, 1082, '2011');
INSERT INTO lista_desplegables VALUES (846, 1082, '2010');
INSERT INTO lista_desplegables VALUES (861, 1083, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (862, 1083, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (863, 1083, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (864, 1083, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (865, 1083, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (866, 1083, 'GUANTA');
INSERT INTO lista_desplegables VALUES (867, 1083, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (868, 1083, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (881, 1123, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (882, 1123, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (883, 1123, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (884, 1123, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (885, 1123, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (886, 1123, 'GUANTA');
INSERT INTO lista_desplegables VALUES (887, 1123, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (888, 1123, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (901, 1143, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (902, 1143, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (903, 1143, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (904, 1143, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (905, 1143, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (906, 1143, 'GUANTA');
INSERT INTO lista_desplegables VALUES (907, 1143, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (908, 1143, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (921, 1163, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (922, 1163, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (923, 1163, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (924, 1163, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (925, 1163, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (926, 1163, 'GUANTA');
INSERT INTO lista_desplegables VALUES (927, 1163, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (928, 1163, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (941, 1183, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (942, 1183, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (943, 1183, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (944, 1183, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (945, 1183, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (946, 1183, 'GUANTA');
INSERT INTO lista_desplegables VALUES (947, 1183, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (948, 1183, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (961, 1203, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (962, 1203, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (963, 1203, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (964, 1203, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (965, 1203, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (966, 1203, 'GUANTA');
INSERT INTO lista_desplegables VALUES (967, 1203, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (968, 1203, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (981, 85, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (982, 85, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (983, 85, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (984, 85, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (985, 85, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (986, 85, 'GUANTA');
INSERT INTO lista_desplegables VALUES (987, 85, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (988, 85, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1001, 86, 'Activo');
INSERT INTO lista_desplegables VALUES (1002, 86, 'Inactivo');
INSERT INTO lista_desplegables VALUES (1003, 86, 'Decaído');
INSERT INTO lista_desplegables VALUES (1004, 86, 'Revocado');
INSERT INTO lista_desplegables VALUES (1005, 86, 'Suspendido');
INSERT INTO lista_desplegables VALUES (1006, 86, 'Cesado');
INSERT INTO lista_desplegables VALUES (1021, 107, 'A1-1-1');
INSERT INTO lista_desplegables VALUES (1022, 107, 'A2-1-1');
INSERT INTO lista_desplegables VALUES (1023, 107, 'A3-1-1');
INSERT INTO lista_desplegables VALUES (1024, 107, 'A4-1-1');
INSERT INTO lista_desplegables VALUES (1025, 107, 'A5-1-1');
INSERT INTO lista_desplegables VALUES (1026, 107, 'A6-1-1');
INSERT INTO lista_desplegables VALUES (1027, 107, 'A7-1-1');
INSERT INTO lista_desplegables VALUES (1028, 107, 'A8-1-1');
INSERT INTO lista_desplegables VALUES (1029, 107, 'A9-1-1');
INSERT INTO lista_desplegables VALUES (1030, 108, 'Recibido');
INSERT INTO lista_desplegables VALUES (1031, 108, 'Extemporáneo');
INSERT INTO lista_desplegables VALUES (1032, 108, 'Por Análisis');
INSERT INTO lista_desplegables VALUES (1033, 108, 'Por Recaudos');
INSERT INTO lista_desplegables VALUES (1034, 108, 'Perención');
INSERT INTO lista_desplegables VALUES (1035, 108, 'Improcedente');
INSERT INTO lista_desplegables VALUES (1036, 108, 'Liquidado');
INSERT INTO lista_desplegables VALUES (21, 4, 'Dato11');
INSERT INTO lista_desplegables VALUES (1, 4, 'Valor11');
INSERT INTO lista_desplegables VALUES (1037, 111, 'Firma Personal');
INSERT INTO lista_desplegables VALUES (1039, 4, 'Dato6');
INSERT INTO lista_desplegables VALUES (1040, 117, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1041, 117, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1042, 117, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1043, 117, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1044, 117, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1045, 117, 'GUANTA');
INSERT INTO lista_desplegables VALUES (1046, 117, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1047, 117, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1048, 118, 'Activo');
INSERT INTO lista_desplegables VALUES (1049, 118, 'Inactivo');
INSERT INTO lista_desplegables VALUES (1050, 118, 'Revocado');
INSERT INTO lista_desplegables VALUES (1051, 118, 'Suspendido');
INSERT INTO lista_desplegables VALUES (1052, 118, 'Cesado');
INSERT INTO lista_desplegables VALUES (1053, 1253, 'LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1054, 1253, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1055, 1253, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1056, 1253, 'PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1057, 1253, 'EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1058, 1253, 'GUANTA');
INSERT INTO lista_desplegables VALUES (1059, 1253, 'BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1060, 1253, 'PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1126, 1306, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1038, 111, 'Persona Jurídica');
INSERT INTO lista_desplegables VALUES (1061, 121, 'FIRMA PERSONAL');
INSERT INTO lista_desplegables VALUES (1062, 121, 'PERSONA JURIDICA');
INSERT INTO lista_desplegables VALUES (1063, 127, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1064, 127, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1065, 127, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1066, 127, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1067, 127, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1068, 127, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1069, 127, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1070, 127, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1071, 128, 'Activo');
INSERT INTO lista_desplegables VALUES (1072, 128, 'Inactivo');
INSERT INTO lista_desplegables VALUES (1073, 128, 'Decaído');
INSERT INTO lista_desplegables VALUES (1074, 128, 'Revocado');
INSERT INTO lista_desplegables VALUES (1075, 128, 'Suspendido');
INSERT INTO lista_desplegables VALUES (1076, 128, 'Cesado');
INSERT INTO lista_desplegables VALUES (1077, 1269, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1078, 1269, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1079, 1269, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1080, 1269, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1081, 1269, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1082, 1269, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1083, 1269, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1084, 1269, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1085, 1273, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1086, 1273, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1087, 1273, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1088, 1273, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1089, 1273, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1090, 1273, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1091, 1273, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1092, 1273, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1093, 1289, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1094, 1289, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1095, 1289, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1096, 1289, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1097, 1289, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1098, 1289, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1099, 1289, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1100, 1289, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1101, 1297, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1102, 1297, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1103, 1297, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1104, 1297, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1106, 1297, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1107, 1297, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1108, 1297, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1105, 1297, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1109, 1300, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1110, 1300, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1111, 1300, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1112, 1300, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1114, 1300, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1115, 1300, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1116, 1300, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1113, 1300, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1117, 1303, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1118, 1303, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1119, 1303, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1120, 1303, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1121, 1303, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1122, 1303, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1123, 1303, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1143, 4, 'Dato7');
INSERT INTO lista_desplegables VALUES (1124, 1306, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1125, 1306, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1127, 1306, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1128, 1306, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1129, 1306, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1130, 1306, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1144, 1306, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1145, 1309, '02.- PTO. CABELLO');
INSERT INTO lista_desplegables VALUES (1146, 1309, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1147, 1312, '01.- LA GUAIRA');
INSERT INTO lista_desplegables VALUES (1148, 1312, '02.- PTO CABELLO');
INSERT INTO lista_desplegables VALUES (1149, 131, 'Firma Personal');
INSERT INTO lista_desplegables VALUES (1131, 1309, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1132, 1309, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1133, 1309, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1134, 1309, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1135, 1309, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1136, 1309, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1150, 131, 'Persona Jurídica');
INSERT INTO lista_desplegables VALUES (1151, 137, '01.- La Guaira');
INSERT INTO lista_desplegables VALUES (1152, 137, '02.- Pto Cabello');
INSERT INTO lista_desplegables VALUES (1153, 137, '03.- El Guamache');
INSERT INTO lista_desplegables VALUES (1137, 1312, '05.- SAN CRISTOBAL');
INSERT INTO lista_desplegables VALUES (1138, 1312, '06.- PTO. AYACUCHO');
INSERT INTO lista_desplegables VALUES (1139, 1312, '03.- EL GUAMACHE');
INSERT INTO lista_desplegables VALUES (1140, 1312, '04.- GUANTA');
INSERT INTO lista_desplegables VALUES (1141, 1312, '07.-  BOCA DE GRITA');
INSERT INTO lista_desplegables VALUES (1142, 1312, '08.- PTO. PALMARITO');
INSERT INTO lista_desplegables VALUES (1154, 137, '04.- Guanta');
INSERT INTO lista_desplegables VALUES (1155, 137, '05.- San Cristobal');
INSERT INTO lista_desplegables VALUES (1156, 137, '06.- Pto Ayacucho');
INSERT INTO lista_desplegables VALUES (1157, 137, '07.- Boca de Grita');
INSERT INTO lista_desplegables VALUES (1158, 137, '08.- Pto Palmarito');
INSERT INTO lista_desplegables VALUES (1159, 138, 'Activo');
INSERT INTO lista_desplegables VALUES (1160, 138, 'Inactivo');
INSERT INTO lista_desplegables VALUES (1161, 138, 'Decaido');
INSERT INTO lista_desplegables VALUES (1162, 138, 'Revocado');
INSERT INTO lista_desplegables VALUES (1163, 138, 'Suspendido');
INSERT INTO lista_desplegables VALUES (1164, 138, 'Cesado');
INSERT INTO lista_desplegables VALUES (1165, 146, '01.- La Guaira');
INSERT INTO lista_desplegables VALUES (1166, 146, '02.- Pto Cabello');
INSERT INTO lista_desplegables VALUES (1167, 146, '03.- El Guamache');
INSERT INTO lista_desplegables VALUES (1168, 146, '04.- Guanta');
INSERT INTO lista_desplegables VALUES (1169, 146, '05.- San Cristobal');
INSERT INTO lista_desplegables VALUES (1170, 146, '06.- Pto. Ayacucho');
INSERT INTO lista_desplegables VALUES (1171, 146, '07.- Boca de Grita');
INSERT INTO lista_desplegables VALUES (1172, 146, '08.- Pto. Palmarito');
INSERT INTO lista_desplegables VALUES (1173, 147, 'Activo');
INSERT INTO lista_desplegables VALUES (1174, 147, 'Inactivo');
INSERT INTO lista_desplegables VALUES (1175, 147, 'Decaído');
INSERT INTO lista_desplegables VALUES (1176, 147, 'Revocado');
INSERT INTO lista_desplegables VALUES (1177, 147, 'Suspendido');
INSERT INTO lista_desplegables VALUES (1178, 147, 'Cesado');
INSERT INTO lista_desplegables VALUES (1179, 1330, '01.- La Guaira');
INSERT INTO lista_desplegables VALUES (1180, 1330, '02.- Pto Cabello');
INSERT INTO lista_desplegables VALUES (1181, 1330, '03.- El Guamache');
INSERT INTO lista_desplegables VALUES (1182, 1330, '04.- Guanta');
INSERT INTO lista_desplegables VALUES (1183, 1330, '05.- San Cristobal');
INSERT INTO lista_desplegables VALUES (1184, 1330, '06.- Pto Cabello');
INSERT INTO lista_desplegables VALUES (1185, 1330, '07.- Boca de Grita');
INSERT INTO lista_desplegables VALUES (1186, 1330, '08.- Pto Palmarito');
INSERT INTO lista_desplegables VALUES (1187, 1337, '01.- La Guaira');
INSERT INTO lista_desplegables VALUES (1188, 1337, '02.- Pto Cabello');
INSERT INTO lista_desplegables VALUES (1189, 1337, '03.- El Guamache');
INSERT INTO lista_desplegables VALUES (1190, 1337, '04.- Guanta');
INSERT INTO lista_desplegables VALUES (1191, 1337, '05.- San Cristobal');
INSERT INTO lista_desplegables VALUES (1192, 1337, '06.- Pto Ayacucho');
INSERT INTO lista_desplegables VALUES (1193, 1337, '07.- Boca de Grita');
INSERT INTO lista_desplegables VALUES (1194, 1337, '08.- Pto Palmarito');
INSERT INTO lista_desplegables VALUES (1195, 154, 'A1-1-1');
INSERT INTO lista_desplegables VALUES (1196, 154, 'A2-1-1');
INSERT INTO lista_desplegables VALUES (1197, 154, 'A3-1-1');
INSERT INTO lista_desplegables VALUES (1198, 154, 'A4-1-1');
INSERT INTO lista_desplegables VALUES (1199, 154, 'A5-1-1');
INSERT INTO lista_desplegables VALUES (1200, 154, 'A6-1-1');
INSERT INTO lista_desplegables VALUES (1201, 154, 'A7-1-1');
INSERT INTO lista_desplegables VALUES (1202, 154, 'A8-1-1');
INSERT INTO lista_desplegables VALUES (1203, 154, 'A9-1-1');
INSERT INTO lista_desplegables VALUES (1204, 155, 'Recibido');
INSERT INTO lista_desplegables VALUES (1205, 155, 'Extemporáneo');
INSERT INTO lista_desplegables VALUES (1206, 155, 'Por Análisis');
INSERT INTO lista_desplegables VALUES (1207, 155, 'Por Recaudos');
INSERT INTO lista_desplegables VALUES (1208, 155, 'Perención');
INSERT INTO lista_desplegables VALUES (1209, 155, 'Improcedente');
INSERT INTO lista_desplegables VALUES (1210, 155, 'Liquidado');
INSERT INTO lista_desplegables VALUES (1211, 1354, 'La Guaira');
INSERT INTO lista_desplegables VALUES (1212, 1354, 'Higuerote');
INSERT INTO lista_desplegables VALUES (1213, 1354, 'Aérea de Maiquetía');
INSERT INTO lista_desplegables VALUES (1214, 1354, 'Aérea de La Carlota');
INSERT INTO lista_desplegables VALUES (1215, 1354, 'Aérea Metropolitana de Caracas');
INSERT INTO lista_desplegables VALUES (1216, 1354, 'Charallave');
INSERT INTO lista_desplegables VALUES (1217, 1354, 'Aeropuerto Internacional Simón Bolívar');
INSERT INTO lista_desplegables VALUES (1218, 1354, 'Aérea de Valencia');
INSERT INTO lista_desplegables VALUES (1219, 1354, 'Puerto Cabello');
INSERT INTO lista_desplegables VALUES (1220, 1357, 'La Guaira');
INSERT INTO lista_desplegables VALUES (1221, 1357, 'Higuerote');
INSERT INTO lista_desplegables VALUES (1222, 1357, 'Aérea de Maiquetía');
INSERT INTO lista_desplegables VALUES (1223, 1357, 'Aérea de La Carlota');
INSERT INTO lista_desplegables VALUES (1224, 1357, 'Aérea Metropolitana de Caracas');
INSERT INTO lista_desplegables VALUES (1225, 1357, 'Charallave');
INSERT INTO lista_desplegables VALUES (1226, 1357, 'Aeropuerto Internacional Simón Bolívar');
INSERT INTO lista_desplegables VALUES (1227, 1357, 'Aérea de Valencia');
INSERT INTO lista_desplegables VALUES (1228, 1357, 'Puerto Cabello');
INSERT INTO lista_desplegables VALUES (1229, 1367, 'Dato mal seleccionado.');
INSERT INTO lista_desplegables VALUES (1230, 4, 'Dato8');
INSERT INTO lista_desplegables VALUES (1231, 4, 'Valor6');
INSERT INTO lista_desplegables VALUES (6, 12, '01 - Sencilla');
INSERT INTO lista_desplegables VALUES (7, 12, '02 - Doble');
INSERT INTO lista_desplegables VALUES (8, 12, '03 - Anticipo');
INSERT INTO lista_desplegables VALUES (9, 12, '04 - Parcial Doble');
INSERT INTO lista_desplegables VALUES (10, 12, '05 - Otros');
INSERT INTO lista_desplegables VALUES (11, 12, '06 - Fideicomiso');
INSERT INTO lista_desplegables VALUES (12, 15, '01 - Ingreso');
INSERT INTO lista_desplegables VALUES (13, 15, '02 - Jubilacion');
INSERT INTO lista_desplegables VALUES (14, 15, '03 - Egreso');
INSERT INTO lista_desplegables VALUES (15, 15, '04 - Reingreso');
INSERT INTO lista_desplegables VALUES (16, 15, '05 - Ascenso');
INSERT INTO lista_desplegables VALUES (17, 15, '06 - Clasificacion');
INSERT INTO lista_desplegables VALUES (18, 15, '07 - Traslado');
INSERT INTO lista_desplegables VALUES (19, 15, '08 - Reincorporacion');
INSERT INTO lista_desplegables VALUES (20, 15, '09 - Reubicacion');
INSERT INTO lista_desplegables VALUES (26, 7, '01 - Sencilla');
INSERT INTO lista_desplegables VALUES (27, 7, '02 - Doble');
INSERT INTO lista_desplegables VALUES (28, 7, '03 - Anticipo');
INSERT INTO lista_desplegables VALUES (29, 7, '04 - Parcial Doble');
INSERT INTO lista_desplegables VALUES (30, 7, '05 - Otros');
INSERT INTO lista_desplegables VALUES (31, 7, '06 - Fideicomiso');
INSERT INTO lista_desplegables VALUES (32, 16, '04 - Expediente Aprobado');
INSERT INTO lista_desplegables VALUES (33, 14, '01 - Obrero');
INSERT INTO lista_desplegables VALUES (34, 14, '02 - Empleado');
INSERT INTO lista_desplegables VALUES (35, 14, '03 - Docente');
INSERT INTO lista_desplegables VALUES (36, 14, '07 - Otros');
INSERT INTO lista_desplegables VALUES (50, 16, '06 - Recibido');
INSERT INTO lista_desplegables VALUES (51, 16, '07 - Asignado');
INSERT INTO lista_desplegables VALUES (52, 16, '08 - Pendiente');
INSERT INTO lista_desplegables VALUES (53, 16, '09 - Analizado');
INSERT INTO lista_desplegables VALUES (54, 16, '10 - Objetado');
INSERT INTO lista_desplegables VALUES (55, 16, '11 - Carga Datos de Cheque');
INSERT INTO lista_desplegables VALUES (56, 10, '0013 - 0003 - MPPRIJP-SAREN');
INSERT INTO lista_desplegables VALUES (57, 11, 'A1303 - SAREN Servicio Autonomo de Registros y Notarias');
INSERT INTO lista_desplegables VALUES (58, 13, '01 - Renuncia');
INSERT INTO lista_desplegables VALUES (1233, 166, '0001 - MINISTERIO DEL PODER POPULAR DE AGRICULTURA Y TIERRAS');
INSERT INTO lista_desplegables VALUES (1234, 166, '0003 - MINISTERIO DEL PODER POPULAR DE LA DEFENSA');
INSERT INTO lista_desplegables VALUES (1235, 166, '0005 - MINISTERIO DEL PODER POPULAR PARA LA EDUCACION');
INSERT INTO lista_desplegables VALUES (1236, 166, '0010 - MINISTERIO DEL PODER POPULAR PARA LA SALUD');
INSERT INTO lista_desplegables VALUES (1237, 166, '0013 - MINISTERIO DEL PODER POPULAR PARA RELACIONES INTERIORES Y JUSTICIA PAZ');
INSERT INTO lista_desplegables VALUES (1238, 166, '0054 - CUERPO DE INVESTIGACIONES CIENTIFICAS PENALES Y CRIMINALISTICAS');
INSERT INTO lista_desplegables VALUES (1239, 166, '0113 - MINISTERIO DEL PODER POPULAR PARA LA EDUCACION UNIVERSITARIA');
INSERT INTO lista_desplegables VALUES (1240, 166, '0120 - GOBERNACION DEL ESTADO ARAGUA');
INSERT INTO lista_desplegables VALUES (1241, 166, '0121 - GOBERNACION DEL ESTADO APURE');
INSERT INTO lista_desplegables VALUES (1242, 166, '0123 - GOBERNACION DEL ESTADO SUCRE');
INSERT INTO lista_desplegables VALUES (1243, 166, '0124 - GOBERNACION DE GUARICO');
INSERT INTO lista_desplegables VALUES (1244, 166, '0999 - OTROS ORGANISMOS');
INSERT INTO lista_desplegables VALUES (1245, 166, '1303 - SERVICIO AUTONOMO DE REGISTROS Y NOTARIAS');
INSERT INTO lista_desplegables VALUES (1246, 167, '0001 - A0022 - INIA (Instituto Nacional de Investigaciones Agrícolas)');
INSERT INTO lista_desplegables VALUES (1247, 167, '0001 - A0933 - INDER (Instituto Nacional de Desarrollo Rural)');
INSERT INTO lista_desplegables VALUES (1248, 167, '0001 - A0935 - INTI (Instituto Nacional de Tierras)');
INSERT INTO lista_desplegables VALUES (1249, 167, '0003 - A0005 -	IACFA (Instituto Autonomo Circulo de las Fuerzas Armadas)');
INSERT INTO lista_desplegables VALUES (1250, 167, '0003 - A0050 - IPSFA (Instituto de Prevision Social de las Fuerzas Armadas Nacionales)');
INSERT INTO lista_desplegables VALUES (1251, 167, '0003 - A0066 - SASFAN (Servicio Autonomo de Salud de la Fuerza Armada Nacional)');
INSERT INTO lista_desplegables VALUES (1252, 167, '0003 - A0192 - OCHINA (Oficina Coordinadora de Hidrografía y Navegación)');
INSERT INTO lista_desplegables VALUES (1253, 167, '0005 - A0138 - Academia Nacional de la Historia');
INSERT INTO lista_desplegables VALUES (1254, 167, '0005 - A0323 - FEDE (Fundación de Edificaciones y Dotaciones Educativas)');
INSERT INTO lista_desplegables VALUES (1255, 167, '0005 - A0410 - FUNDABIT (Fundación Bolivariana de Informática y Telemática)');
INSERT INTO lista_desplegables VALUES (1256, 167, '0010 - A0061 - INHRR (Instituto Nacional de Higiene "Rafael Rangel")');
INSERT INTO lista_desplegables VALUES (1257, 167, '0010 - A0446 - Fundación Misión Barrio Adentro');
INSERT INTO lista_desplegables VALUES (1258, 167, '0010 - A0731 - Universidad de las Ciencias de la Salud');
INSERT INTO lista_desplegables VALUES (1259, 167, '0013 - A0538 - Instituto Nacional contra la Discriminación Racial');
INSERT INTO lista_desplegables VALUES (1260, 167, '0013 - A0940 - INTT (Instituto Nacional de Transporte Terrestre)');
INSERT INTO lista_desplegables VALUES (1261, 167, '0054 - A0054 - CICPC (Instituto Autónomo de Previsión Social del Cuerpo de Investigaciones Científicas, Penales y  Criminalísticas)');
INSERT INTO lista_desplegables VALUES (1262, 167, '0113 - A0083 - ULA (Universidad de Los Andes)');
INSERT INTO lista_desplegables VALUES (1263, 167, '0113 - A0087 - UNA (Universidad Nacional Abierta)');
INSERT INTO lista_desplegables VALUES (1264, 167, '0113 - A0093 - UCV (Universidad Central de Venezuela)');
INSERT INTO lista_desplegables VALUES (1265, 167, '0120 - A0120 - GOBERNACION DEL ESTADO ARAGUA');
INSERT INTO lista_desplegables VALUES (1266, 167, '0121 - A0121 - GOBERNACION DEL ESTADO APURE');
INSERT INTO lista_desplegables VALUES (1267, 167, '0123 - A0123 - GOBERNACION DEL ESTADO SUCRE');
INSERT INTO lista_desplegables VALUES (1268, 167, '0124 - A0124 - GOBERNACION DE GUARICO');
INSERT INTO lista_desplegables VALUES (1269, 167, '0999 - A0999 - OTROS ORGANISMOS');
INSERT INTO lista_desplegables VALUES (1270, 167, '1303 - A1303 - SAREN (Servicio Autónomo de Registros y Notarías)');
INSERT INTO lista_desplegables VALUES (1271, 13, '02 - Remocion o Retiro');
INSERT INTO lista_desplegables VALUES (1272, 13, '03 - Jubilacion');
INSERT INTO lista_desplegables VALUES (1273, 13, '04 - Invalidez');
INSERT INTO lista_desplegables VALUES (1274, 13, '05 - Pension');
INSERT INTO lista_desplegables VALUES (1275, 13, '06 - Reduccion');
INSERT INTO lista_desplegables VALUES (1276, 13, '07 - Destitucion');
INSERT INTO lista_desplegables VALUES (1277, 13, '08 - Fallecimiento');
INSERT INTO lista_desplegables VALUES (1278, 13, '09 - Injustificado');
INSERT INTO lista_desplegables VALUES (1279, 13, '10 - Complemento u Otros');
INSERT INTO lista_desplegables VALUES (1280, 13, '22 - Otros');
INSERT INTO lista_desplegables VALUES (1281, 173, '0003 - MINISTERIO DEL PODER POPULAR DE LA DEFENSA');
INSERT INTO lista_desplegables VALUES (1282, 174, '0003 - A0005 - IACFA (Instituto Autonomo Circulo de las Fuerzas Armadas)');
INSERT INTO lista_desplegables VALUES (1283, 174, '0003 - A0050 - IPSFA (Instituto de Prevision Social de las Fuerzas Armadas Nacionales)');
INSERT INTO lista_desplegables VALUES (1284, 174, '0003 - A0066 - SASFAN (Servicio Autonomo de Salud de la Fuerza Armada Nacional)');
INSERT INTO lista_desplegables VALUES (1285, 174, '0003 - A0192 - OCHINA (Oficina Coordinadora de Hidrografía y Navegación)');
INSERT INTO lista_desplegables VALUES (1286, 176, '01 - Renuncia');
INSERT INTO lista_desplegables VALUES (1287, 176, '02 - Remocion o Retiro');
INSERT INTO lista_desplegables VALUES (1288, 176, '03 - Jubilacion');
INSERT INTO lista_desplegables VALUES (1289, 176, '04 - Invalidez');
INSERT INTO lista_desplegables VALUES (1290, 176, '05 - Pension');
INSERT INTO lista_desplegables VALUES (1291, 176, '06 - Reduccion');
INSERT INTO lista_desplegables VALUES (1292, 176, '07 - Destitucion');
INSERT INTO lista_desplegables VALUES (1293, 176, '08 - Fallecimiento');
INSERT INTO lista_desplegables VALUES (1294, 176, '09 - Injustificado');
INSERT INTO lista_desplegables VALUES (1295, 176, '10 - Complemento u Otros');
INSERT INTO lista_desplegables VALUES (1296, 176, '22 - Otros');
INSERT INTO lista_desplegables VALUES (1297, 177, '01 - Obrero');
INSERT INTO lista_desplegables VALUES (1298, 177, '02 - Empleado');
INSERT INTO lista_desplegables VALUES (1299, 177, '03 - Docente');
INSERT INTO lista_desplegables VALUES (1300, 177, '07 - Otros');
INSERT INTO lista_desplegables VALUES (1301, 179, '04 - Expediente Aprobado');
INSERT INTO lista_desplegables VALUES (1302, 179, '06 - Recibido');
INSERT INTO lista_desplegables VALUES (1303, 179, '07 - Asignado');
INSERT INTO lista_desplegables VALUES (1304, 179, '08 - Pendiente');
INSERT INTO lista_desplegables VALUES (1305, 183, '0121 - GOBERNACION DEL ESTADO APURE');
INSERT INTO lista_desplegables VALUES (1306, 184, '0121 - A0121 - GOBERNACION DEL ESTADO APURE');
INSERT INTO lista_desplegables VALUES (1307, 186, '01 - Renuncia');
INSERT INTO lista_desplegables VALUES (1308, 186, '02 - Remocion o Retiro');
INSERT INTO lista_desplegables VALUES (1309, 186, '03 - Jubilacion');
INSERT INTO lista_desplegables VALUES (1310, 186, '04 - Invalidez');
INSERT INTO lista_desplegables VALUES (1311, 186, '05 - Pension');
INSERT INTO lista_desplegables VALUES (1312, 186, '06 - Reduccion');
INSERT INTO lista_desplegables VALUES (1313, 186, '07 - Destitucion');
INSERT INTO lista_desplegables VALUES (1314, 186, '08 - Fallecimiento');
INSERT INTO lista_desplegables VALUES (1315, 186, '09 - Injustificado');
INSERT INTO lista_desplegables VALUES (1316, 186, '10 - Complemento u Otros');
INSERT INTO lista_desplegables VALUES (1317, 186, '22 - Otros');
INSERT INTO lista_desplegables VALUES (1318, 187, '01 - Obrero');
INSERT INTO lista_desplegables VALUES (1319, 187, '02 - Empleado');
INSERT INTO lista_desplegables VALUES (1320, 187, '03 - Docente');
INSERT INTO lista_desplegables VALUES (1321, 187, '07 - Otros');
INSERT INTO lista_desplegables VALUES (1322, 189, '04 - Expediente Aprobado');
INSERT INTO lista_desplegables VALUES (1323, 189, '06 - Recibido');
INSERT INTO lista_desplegables VALUES (1324, 189, '07 - Asignado');
INSERT INTO lista_desplegables VALUES (1325, 189, '08 - Pendiente');
INSERT INTO lista_desplegables VALUES (1326, 189, '09 - Analizado');
INSERT INTO lista_desplegables VALUES (1327, 1376, '01 - Sencilla');
INSERT INTO lista_desplegables VALUES (1328, 1376, '02 - Doble');
INSERT INTO lista_desplegables VALUES (1329, 1376, '03 - Anticipo');
INSERT INTO lista_desplegables VALUES (1330, 1376, '04 - Parcial Doble');
INSERT INTO lista_desplegables VALUES (1331, 1376, '05 - Otros');
INSERT INTO lista_desplegables VALUES (1332, 1376, '06 - Fideicomiso');
INSERT INTO lista_desplegables VALUES (1333, 1382, '01 - Ingreso');
INSERT INTO lista_desplegables VALUES (1334, 1382, '02 - Jubilacion');
INSERT INTO lista_desplegables VALUES (1335, 1382, '03 - Egreso');
INSERT INTO lista_desplegables VALUES (1336, 1382, '04 - Reingreso');
INSERT INTO lista_desplegables VALUES (1337, 1382, '05 - Ascenso');
INSERT INTO lista_desplegables VALUES (1338, 1382, '06 - Clasificacion');
INSERT INTO lista_desplegables VALUES (1339, 1382, '07 - Traslado');
INSERT INTO lista_desplegables VALUES (1340, 1382, '08 - Reincorporacion');
INSERT INTO lista_desplegables VALUES (1341, 1382, '09 - Reubicacion');


--
-- TOC entry 2423 (class 0 OID 21105)
-- Dependencies: 211
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil VALUES (1, NULL, NULL, 'dw4jconf', 7);
INSERT INTO perfil VALUES (2, NULL, NULL, 'dw4jconf', 8);
INSERT INTO perfil VALUES (512, 21, 21, 'javier_plaza', 2);
INSERT INTO perfil VALUES (513, 21, 21, 'javier_plaza', 4);
INSERT INTO perfil VALUES (514, 21, 21, 'javier_plaza', 3);
INSERT INTO perfil VALUES (515, 21, 21, 'javier_plaza', 9);
INSERT INTO perfil VALUES (516, 21, 21, 'javier_plaza', 5);
INSERT INTO perfil VALUES (517, 21, 21, 'javier_plaza', 6);
INSERT INTO perfil VALUES (518, 103, 163, 'javier_plaza', 2);
INSERT INTO perfil VALUES (519, 103, 163, 'javier_plaza', 4);
INSERT INTO perfil VALUES (520, 103, 163, 'javier_plaza', 3);
INSERT INTO perfil VALUES (521, 103, 163, 'javier_plaza', 9);
INSERT INTO perfil VALUES (522, 103, 163, 'javier_plaza', 5);
INSERT INTO perfil VALUES (523, 103, 163, 'javier_plaza', 6);
INSERT INTO perfil VALUES (524, 105, 165, 'javier_plaza', 2);
INSERT INTO perfil VALUES (525, 105, 165, 'javier_plaza', 4);
INSERT INTO perfil VALUES (526, 105, 165, 'javier_plaza', 3);
INSERT INTO perfil VALUES (527, 105, 165, 'javier_plaza', 9);
INSERT INTO perfil VALUES (528, 105, 165, 'javier_plaza', 5);
INSERT INTO perfil VALUES (529, 105, 165, 'javier_plaza', 6);
INSERT INTO perfil VALUES (530, 104, 164, 'javier_plaza', 2);
INSERT INTO perfil VALUES (531, 104, 164, 'javier_plaza', 4);
INSERT INTO perfil VALUES (532, 104, 164, 'javier_plaza', 3);
INSERT INTO perfil VALUES (533, 104, 164, 'javier_plaza', 9);
INSERT INTO perfil VALUES (534, 104, 164, 'javier_plaza', 5);
INSERT INTO perfil VALUES (535, 104, 164, 'javier_plaza', 6);
INSERT INTO perfil VALUES (536, 61, 61, 'javier_plaza', 2);
INSERT INTO perfil VALUES (537, 61, 61, 'javier_plaza', 4);
INSERT INTO perfil VALUES (538, 61, 61, 'javier_plaza', 3);
INSERT INTO perfil VALUES (539, 61, 61, 'javier_plaza', 9);
INSERT INTO perfil VALUES (540, 61, 61, 'javier_plaza', 5);
INSERT INTO perfil VALUES (541, 61, 61, 'javier_plaza', 6);
INSERT INTO perfil VALUES (542, 61, 81, 'javier_plaza', 2);
INSERT INTO perfil VALUES (543, 61, 81, 'javier_plaza', 4);
INSERT INTO perfil VALUES (544, 61, 81, 'javier_plaza', 3);
INSERT INTO perfil VALUES (545, 61, 81, 'javier_plaza', 9);
INSERT INTO perfil VALUES (546, 61, 81, 'javier_plaza', 5);
INSERT INTO perfil VALUES (547, 61, 81, 'javier_plaza', 6);
INSERT INTO perfil VALUES (548, 61, 121, 'javier_plaza', 2);
INSERT INTO perfil VALUES (549, 61, 121, 'javier_plaza', 4);
INSERT INTO perfil VALUES (550, 61, 121, 'javier_plaza', 3);
INSERT INTO perfil VALUES (551, 61, 121, 'javier_plaza', 9);
INSERT INTO perfil VALUES (552, 61, 121, 'javier_plaza', 5);
INSERT INTO perfil VALUES (553, 61, 121, 'javier_plaza', 6);
INSERT INTO perfil VALUES (554, 1, 1, 'javier_plaza', 2);
INSERT INTO perfil VALUES (463, 41, 41, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (464, 41, 41, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (465, 41, 41, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (466, 41, 41, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (467, 41, 41, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (468, 41, 41, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (469, 101, 161, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (470, 101, 161, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (471, 101, 161, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (472, 101, 161, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (473, 101, 161, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (474, 101, 161, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (475, 104, 164, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (476, 104, 164, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (477, 104, 164, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (478, 104, 164, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (479, 104, 164, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (480, 104, 164, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (481, 61, 61, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (482, 61, 61, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (483, 61, 61, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (484, 61, 61, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (485, 61, 61, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (486, 61, 61, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (487, 61, 81, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (488, 61, 81, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (489, 61, 81, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (490, 61, 81, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (491, 61, 81, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (492, 61, 81, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (493, 61, 121, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (494, 61, 121, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (495, 61, 121, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (496, 61, 121, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (497, 61, 121, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (498, 61, 121, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (499, NULL, NULL, 'nilda_blanco', 1);
INSERT INTO perfil VALUES (500, 21, 21, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (501, 21, 167, 'nilda_blanco', 4);
INSERT INTO perfil VALUES (502, 21, 21, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (503, 21, 167, 'nilda_blanco', 5);
INSERT INTO perfil VALUES (504, 21, 21, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (505, 21, 167, 'nilda_blanco', 3);
INSERT INTO perfil VALUES (506, 21, 21, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (507, 21, 167, 'nilda_blanco', 2);
INSERT INTO perfil VALUES (508, 21, 21, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (509, 21, 167, 'nilda_blanco', 6);
INSERT INTO perfil VALUES (510, 21, 21, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (511, 21, 167, 'nilda_blanco', 9);
INSERT INTO perfil VALUES (555, 1, 1, 'javier_plaza', 4);
INSERT INTO perfil VALUES (556, 1, 1, 'javier_plaza', 3);
INSERT INTO perfil VALUES (557, 1, 1, 'javier_plaza', 9);
INSERT INTO perfil VALUES (558, 1, 1, 'javier_plaza', 5);
INSERT INTO perfil VALUES (559, 1, 1, 'javier_plaza', 6);
INSERT INTO perfil VALUES (560, NULL, NULL, 'javier_plaza', 1);
INSERT INTO perfil VALUES (561, 21, 167, 'javier_plaza', 4);
INSERT INTO perfil VALUES (562, 21, 167, 'javier_plaza', 5);
INSERT INTO perfil VALUES (563, 21, 167, 'javier_plaza', 3);
INSERT INTO perfil VALUES (564, 21, 167, 'javier_plaza', 2);
INSERT INTO perfil VALUES (565, 21, 167, 'javier_plaza', 6);
INSERT INTO perfil VALUES (566, 21, 167, 'javier_plaza', 9);
INSERT INTO perfil VALUES (580, 21, 167, 'tgarrido', 2);
INSERT INTO perfil VALUES (581, 21, 167, 'tgarrido', 4);
INSERT INTO perfil VALUES (582, 21, 167, 'tgarrido', 3);
INSERT INTO perfil VALUES (583, 21, 167, 'tgarrido', 9);
INSERT INTO perfil VALUES (584, 21, 167, 'tgarrido', 5);
INSERT INTO perfil VALUES (585, 21, 167, 'tgarrido', 6);
INSERT INTO perfil VALUES (586, 41, 41, 'tgarrido', 2);
INSERT INTO perfil VALUES (587, 41, 41, 'tgarrido', 4);
INSERT INTO perfil VALUES (588, 41, 41, 'tgarrido', 3);
INSERT INTO perfil VALUES (589, 41, 41, 'tgarrido', 9);
INSERT INTO perfil VALUES (590, 41, 41, 'tgarrido', 5);
INSERT INTO perfil VALUES (591, 41, 41, 'tgarrido', 6);
INSERT INTO perfil VALUES (592, NULL, NULL, 'tgarrido', 1);
INSERT INTO perfil VALUES (593, 21, 21, 'tgarrido', 4);
INSERT INTO perfil VALUES (594, 21, 21, 'tgarrido', 5);
INSERT INTO perfil VALUES (595, 21, 21, 'tgarrido', 3);
INSERT INTO perfil VALUES (596, 21, 21, 'tgarrido', 2);
INSERT INTO perfil VALUES (597, 21, 21, 'tgarrido', 6);
INSERT INTO perfil VALUES (598, 21, 21, 'tgarrido', 9);
INSERT INTO perfil VALUES (599, 41, 41, 'eescalona', 2);
INSERT INTO perfil VALUES (600, 41, 41, 'eescalona', 4);
INSERT INTO perfil VALUES (601, 41, 41, 'eescalona', 3);
INSERT INTO perfil VALUES (602, 41, 41, 'eescalona', 9);
INSERT INTO perfil VALUES (603, 41, 41, 'eescalona', 5);
INSERT INTO perfil VALUES (604, 41, 41, 'eescalona', 6);
INSERT INTO perfil VALUES (605, 1, 1, 'eescalona', 2);
INSERT INTO perfil VALUES (606, 1, 1, 'eescalona', 4);
INSERT INTO perfil VALUES (607, 1, 1, 'eescalona', 3);
INSERT INTO perfil VALUES (608, 1, 1, 'eescalona', 9);
INSERT INTO perfil VALUES (609, 1, 1, 'eescalona', 5);
INSERT INTO perfil VALUES (610, 1, 1, 'eescalona', 6);
INSERT INTO perfil VALUES (611, NULL, NULL, 'eescalona', 1);
INSERT INTO perfil VALUES (4, 2, 3, 'asalmeron', 4);
INSERT INTO perfil VALUES (5, 2, 3, 'asalmeron', 5);
INSERT INTO perfil VALUES (6, 2, 3, 'asalmeron', 3);
INSERT INTO perfil VALUES (7, 2, 3, 'asalmeron', 2);
INSERT INTO perfil VALUES (8, 2, 3, 'asalmeron', 6);
INSERT INTO perfil VALUES (9, 2, 3, 'asalmeron', 9);
INSERT INTO perfil VALUES (630, 2, 4, 'daniel', 4);
INSERT INTO perfil VALUES (631, 2, 4, 'daniel', 5);
INSERT INTO perfil VALUES (632, 2, 4, 'daniel', 3);
INSERT INTO perfil VALUES (633, 2, 4, 'daniel', 6);
INSERT INTO perfil VALUES (634, 2, 2, 'david_ruiz', 4);
INSERT INTO perfil VALUES (635, 2, 2, 'david_ruiz', 5);
INSERT INTO perfil VALUES (636, 2, 2, 'david_ruiz', 3);
INSERT INTO perfil VALUES (637, 2, 2, 'david_ruiz', 6);
INSERT INTO perfil VALUES (638, 2, 5, 'fbrito', 4);
INSERT INTO perfil VALUES (639, 2, 5, 'fbrito', 5);
INSERT INTO perfil VALUES (640, 2, 5, 'fbrito', 3);
INSERT INTO perfil VALUES (641, 2, 5, 'fbrito', 6);
INSERT INTO perfil VALUES (642, 2, 4, 'thais', 2);
INSERT INTO perfil VALUES (643, 2, 4, 'thais', 4);
INSERT INTO perfil VALUES (644, 2, 4, 'thais', 9);
INSERT INTO perfil VALUES (645, 2, 4, 'thais', 5);
INSERT INTO perfil VALUES (646, 2, 2, 'thais', 2);
INSERT INTO perfil VALUES (647, 2, 2, 'thais', 4);
INSERT INTO perfil VALUES (648, 2, 2, 'thais', 9);
INSERT INTO perfil VALUES (649, 2, 2, 'thais', 5);
INSERT INTO perfil VALUES (650, 2, 3, 'thais', 2);
INSERT INTO perfil VALUES (651, 2, 3, 'thais', 4);
INSERT INTO perfil VALUES (652, 2, 3, 'thais', 9);
INSERT INTO perfil VALUES (653, 2, 3, 'thais', 5);
INSERT INTO perfil VALUES (654, 2, 5, 'thais', 2);
INSERT INTO perfil VALUES (655, 2, 5, 'thais', 4);
INSERT INTO perfil VALUES (656, 2, 5, 'thais', 9);
INSERT INTO perfil VALUES (657, 2, 5, 'thais', 5);
INSERT INTO perfil VALUES (658, NULL, NULL, 'thais', 1);
INSERT INTO perfil VALUES (659, 2, 4, 'thais', 3);
INSERT INTO perfil VALUES (660, 2, 2, 'thais', 10);
INSERT INTO perfil VALUES (661, 2, 4, 'thais', 10);
INSERT INTO perfil VALUES (662, 2, 3, 'thais', 10);


--
-- TOC entry 2424 (class 0 OID 21109)
-- Dependencies: 212
-- Data for Name: reporte; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO reporte VALUES (3, '250001', '250001', '3915779', 'GARRIDO P. NELLY M.', '0013 - 0003 - MPPRIJP-SAREN');


--
-- TOC entry 2425 (class 0 OID 21115)
-- Dependencies: 213
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rol VALUES (1, 'ADMINISTRADOR');
INSERT INTO rol VALUES (2, 'APROBADOR');
INSERT INTO rol VALUES (3, 'DIGITALIZADOR');
INSERT INTO rol VALUES (5, 'IMPRIMIR');
INSERT INTO rol VALUES (6, 'REPORTES');
INSERT INTO rol VALUES (7, 'CONFIGURADOR');
INSERT INTO rol VALUES (8, 'MANTENIMIENTO');
INSERT INTO rol VALUES (9, 'ELIMINAR');
INSERT INTO rol VALUES (10, 'CONSULTAR POR EXPEDIENTE');
INSERT INTO rol VALUES (4, 'CONSULTAR POR CATEGORIA');


--
-- TOC entry 2440 (class 0 OID 0)
-- Dependencies: 185
-- Name: sq_categroria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_categroria', 169, true);


--
-- TOC entry 2441 (class 0 OID 0)
-- Dependencies: 208
-- Name: sq_combo; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_combo', 1341, true);


--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 189
-- Name: sq_dato_adicional; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_dato_adicional', 1418, true);


--
-- TOC entry 2443 (class 0 OID 0)
-- Dependencies: 191
-- Name: sq_datos_infodocumento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_datos_infodocumento', 4641, true);


--
-- TOC entry 2444 (class 0 OID 0)
-- Dependencies: 193
-- Name: sq_documento_eliminado; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_documento_eliminado', 780, true);


--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 197
-- Name: sq_expediente; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_expediente', 445, true);


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 200
-- Name: sq_foliatura; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_foliatura', 1, false);


--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 202
-- Name: sq_indices; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_indices', 189, true);


--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 204
-- Name: sq_infodocumento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_infodocumento', 3862, true);


--
-- TOC entry 2449 (class 0 OID 0)
-- Dependencies: 206
-- Name: sq_libreria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_libreria', 106, true);


--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 210
-- Name: sq_perfil; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_perfil', 659, true);


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 214
-- Name: sq_subcategroria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_subcategroria', 258, true);


--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 215
-- Name: sq_tipo_documento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_tipo_documento', 625, true);


--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 216
-- Name: sq_valor_dato_adicional; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_valor_dato_adicional', 3497, true);


--
-- TOC entry 2429 (class 0 OID 21124)
-- Dependencies: 217
-- Data for Name: subcategoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO subcategoria VALUES (21, 21, '01-Resumen de Conformidad', 1);
INSERT INTO subcategoria VALUES (41, 41, '00.- RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (42, 41, '01.- DOCUMENTOS  DE DRAW  BACK', 1);
INSERT INTO subcategoria VALUES (61, 61, 'SUB CATEGORÍA:00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (81, 61, 'SUB CATEGORÍA:01.-  PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria VALUES (101, 61, 'SUB CATEGORÍA:02.-  PRUEBA - ACTUALIZACIÓN', 1);
INSERT INTO subcategoria VALUES (121, 61, 'SUB CATEGORÍA:03.-  PRUEBA - NOTIFICACIÓN CESE DEFINITIVO', 1);
INSERT INTO subcategoria VALUES (141, 61, 'SUB CATEGORÍA:10.-  PRUEBA - CLAVES SISTEMAS AUTOMATIZADOS', 1);
INSERT INTO subcategoria VALUES (161, 81, 'SUB CATEGORÍA:00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (162, 81, 'SUB CATEGORÍA:01.-  PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria VALUES (163, 81, 'SUB CATEGORÍA:02.-  PRUEBA - INCORPORACIÓN', 1);
INSERT INTO subcategoria VALUES (164, 81, 'SUB CATEGORÍA:03.-  PRUEBA - TRANSFERENCIA', 1);
INSERT INTO subcategoria VALUES (165, 81, 'SUB CATEGORÍA:04.-  PRUEBA - DESINCORPORACIÓN', 1);
INSERT INTO subcategoria VALUES (181, 121, '00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (182, 121, '01.-  PRUEBA - INCLUSIÓN', 1);
INSERT INTO subcategoria VALUES (183, 121, '02.-  PRUEBA - EXCLUSIÓN', 1);
INSERT INTO subcategoria VALUES (230, 164, '+SUB CATEGORÍA:00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (231, 164, '+SUB CATEGORÍA:01.-  PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria VALUES (232, 164, '+SUB CATEGORÍA:02.-  PRUEBA - ACTUALIZACIÓN', 1);
INSERT INTO subcategoria VALUES (1, 1, 'SubCategoriaPrueba', 1);
INSERT INTO subcategoria VALUES (221, 161, 'SUB CATEGORÍA:00.- RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (222, 161, 'SUB CATEGORÍA:01.- DOCUMENTOS  DE DRAW  BACK', 1);
INSERT INTO subcategoria VALUES (223, 161, 'DOCUMENTOS  DE DRAW  BACK', 2);
INSERT INTO subcategoria VALUES (224, 163, '00.- 2 PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (225, 163, '01.- 2 PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria VALUES (226, 163, '02.- 2 PRUEBA - ACTUALIZACIÓN', 1);
INSERT INTO subcategoria VALUES (227, 163, '03.- 2 PRUEBA - NOTIFICACIÓN CESE DEFINITIVO', 1);
INSERT INTO subcategoria VALUES (228, 163, '10.-  PRUEBA - CLAVES SISTEMAS AUTOMATIZADOS', 2);
INSERT INTO subcategoria VALUES (229, 163, '10.- 2 PRUEBA - CLAVES SISTEMAS AUTOMATIZADOS', 1);
INSERT INTO subcategoria VALUES (233, 164, '+SUB CATEGORÍA:03.-  PRUEBA - NOTIFICACIÓN CESE DEFINITIVO', 1);
INSERT INTO subcategoria VALUES (234, 164, '+SUB CATEGORÍA:10.-  PRUEBA - CLAVES SISTEMAS AUTOMATIZADOS', 1);
INSERT INTO subcategoria VALUES (235, 164, 'PRUEBA', 2);
INSERT INTO subcategoria VALUES (236, 165, '00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (237, 165, '01.-  PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria VALUES (238, 165, '02.-  PRUEBA - ACTUALIZACIÓN', 1);
INSERT INTO subcategoria VALUES (239, 165, '03.-  PRUEBA - NOTIFICACIÓN CESE DEFINITIVO', 1);
INSERT INTO subcategoria VALUES (240, 165, '10.-  PRUEBA - CLAVES SISTEMAS AUTOMATIZADOS', 1);
INSERT INTO subcategoria VALUES (241, 166, '00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria VALUES (242, 166, '01.-  PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria VALUES (243, 166, '02.-  PRUEBA - INCORPORACIÓN', 1);
INSERT INTO subcategoria VALUES (244, 166, '03.-  PRUEBA - TRANSFERENCIA', 1);
INSERT INTO subcategoria VALUES (245, 166, '04.-  PRUEBA - DESINCORPORACIÓN', 1);
INSERT INTO subcategoria VALUES (246, 167, '00.- Resumen del Expediente', 1);
INSERT INTO subcategoria VALUES (247, 167, '01.- Documentos de Draw Back', 1);
INSERT INTO subcategoria VALUES (248, 1, 'SubCategoriaPrueba3', 2);
INSERT INTO subcategoria VALUES (201, 1, 'SubCategoriaPrueba2', 1);
INSERT INTO subcategoria VALUES (2, 2, '01.- RELACION SUMARIA', 1);
INSERT INTO subcategoria VALUES (3, 2, '02.- VERIFICACION DE DATOS', 1);
INSERT INTO subcategoria VALUES (4, 2, '03.- CERTIFICACION DE INGRESO', 1);
INSERT INTO subcategoria VALUES (5, 2, '04.- EXTRACTO DE CUENTA', 1);
INSERT INTO subcategoria VALUES (6, 2, '05.- CERTIFICACION DE EGRESO', 1);
INSERT INTO subcategoria VALUES (7, 2, '06.- CERTIFICACION BANCARIA', 1);
INSERT INTO subcategoria VALUES (8, 2, '07.- DOCUMENTOS POR FALLECIMIENTO Y/O MANUTENCION', 1);
INSERT INTO subcategoria VALUES (9, 2, '08.- DECLARACION JURADA DE PATRIMONIO', 1);
INSERT INTO subcategoria VALUES (10, 2, '09.- CERTIFICACION DE PAGO', 1);
INSERT INTO subcategoria VALUES (11, 3, '01.- EXPEDIENTE DE SAREN', 1);
INSERT INTO subcategoria VALUES (12, 4, '01.- EXPEDIENTE HISTORICO', 1);
INSERT INTO subcategoria VALUES (250, 5, '01.- RELACION SUMARIA', 1);
INSERT INTO subcategoria VALUES (251, 5, '02.- VERIFICACION DE DATOS', 1);
INSERT INTO subcategoria VALUES (252, 5, '03.- CERTIFICACION DE INGRESO', 1);
INSERT INTO subcategoria VALUES (253, 5, '04.- EXTRACTO DE CUENTA', 1);
INSERT INTO subcategoria VALUES (254, 5, '05.- CERTIFICACION DE EGRESO', 1);
INSERT INTO subcategoria VALUES (255, 5, '06.- CERTIFICACION BANCARIA', 1);
INSERT INTO subcategoria VALUES (256, 5, '07.- DOCUMENTOS POR FALLECIMIENTO Y/O MANUTENCION', 1);
INSERT INTO subcategoria VALUES (257, 5, '08.- DECLARACION JURADA DE PATRIMONIO', 1);
INSERT INTO subcategoria VALUES (258, 5, '09.- CERTIFICACION DE PAGO', 1);


--
-- TOC entry 2430 (class 0 OID 21128)
-- Dependencies: 218
-- Data for Name: tipodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipodocumento VALUES (526, 165, 236, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (537, 165, 238, '2.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (21, 21, 21, '1.01.-Resumen de Conformidad', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (41, 41, 41, '0.01.- Relación de Documentos', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (61, 41, 42, '1.01.- Constancia de Recepción de Documentos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (62, 41, 42, '1.02.- Solicitud de Draw Back', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (63, 41, 42, '1.03.- Declaración de Exportación  (DUA – Forma D)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (64, 41, 42, '1.04.- Factura Comercial', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (65, 41, 42, '1.05.- Documento de Transporte  BL', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (66, 41, 42, '1.06.- Documento de Transporte  CPIC/MCI', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (67, 41, 42, '1.07.- Documento de Transporte  GA', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (68, 41, 42, '1.08.- Solicitud de Recaudos Ingreso', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (69, 41, 42, '1.09.- Certificación de Venta de Divisas', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (70, 41, 42, '1.10.- Desglose de Venta de Divisas (Valor FOB)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (71, 41, 42, '1.11.- Carta Bajo Fe de Juramento Modalidad', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (72, 41, 42, '1.12.- Relación Insumo Producto (RIP)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (73, 41, 42, '1.13.- Descripción Literal y Grafica del Proceso Productivo (DLG)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (74, 41, 42, '1.14.- Relación de Insumos Importados', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (75, 41, 42, '1.15.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (76, 41, 42, '1.16.- Corrección de Datos Exportación', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (77, 41, 42, '1.17.- Formulario “Forma 16”', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (78, 41, 42, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (81, 61, 61, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (101, 61, 81, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (121, 61, 81, '1.02.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (141, 61, 81, '1.03.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (161, 61, 81, '1.04.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (181, 61, 81, '1.16.- Registro de Información Fiscal (RIF) - Socios', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (201, 61, 81, '1.17.- Registro de Información Fiscal (RIF) - Administradores', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (202, 61, 81, '1.21.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (203, 61, 81, '1.22.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (204, 61, 81, '1.23.- Providencia Administrativa', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (205, 61, 81, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (221, 61, 101, '2.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (222, 61, 101, '2.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (223, 61, 101, '2.03.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (224, 61, 101, '2.04.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (241, 61, 101, '2.05.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (242, 61, 101, '2.13.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (243, 61, 101, '2.14.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (244, 61, 101, '2.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (261, 61, 121, '3.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (262, 61, 121, '3.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (263, 61, 121, '3.03.- Modificaciones del Documento Constitutivo', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (264, 61, 121, '3.04.- Solicitud de Recaudos', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (265, 61, 121, '3.05.- Alcances', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (266, 61, 121, '3.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (281, 61, 141, '10.01.- Acceso', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (282, 61, 141, '10.02.- Reseteo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (283, 61, 141, '10.03.- Usuarios Adicionales', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (284, 61, 141, '10.04.- Bloqueo Temporal', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (285, 61, 141, '10.05.- Bloqueo Definitivo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (286, 61, 141, '10.06.- I-Aduana ó I-Seniat', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (287, 61, 141, '10.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (301, 81, 161, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (321, 81, 162, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (322, 81, 162, '1.02.- Cédula de Identidad', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (323, 81, 162, '1.07.- Impuesto Sobre La Renta (ISLR)', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (324, 81, 162, '1.08.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (325, 81, 162, '1.09.- Solicitud de Recaudos', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (326, 81, 162, '1.10.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (327, 81, 162, '1.11.- Providencia Administrativa', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (328, 81, 162, '1.12.- Timbres Fiscales', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (329, 81, 162, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (341, 81, 163, '2.01.- Incorporación', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (361, 81, 164, '3.01.- Transferencia', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (381, 81, 165, '4.01.- Desincorporación', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (401, 121, 181, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (421, 121, 182, '1.01.- Inclusión', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (441, 121, 183, '2.01.- Exclusión', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (482, 163, 224, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (481, 161, 222, '14', 2, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (483, 163, 225, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (484, 163, 225, '1.02.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (461, 161, 221, '0.01.- Relación de Documentos', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (462, 161, 221, 'Relación de Documentos', 2, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (463, 161, 222, '1.01.- Constancia de Recepción de Documentos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (464, 161, 222, '1.02.- Solicitud de Draw Back', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (465, 161, 222, '1.03.- Declaración de Exportación  (DUA – Forma D)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (466, 161, 222, '1.04.- Factura Comercial', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (467, 161, 222, '1.05.- Documento de Transporte  BL', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (468, 161, 222, '1.06.- Documento de Transporte  CPIC/MCI', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (469, 161, 222, '1.07.- Documento de Transporte  GA', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (470, 161, 222, '1.08.- Solicitud de Recaudos Ingreso', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (471, 161, 222, '1.09.- Certificación de Venta de Divisas', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (472, 161, 222, '1.10.- Desglose de Venta de Divisas (Valor FOB)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (473, 161, 222, '1.11.- Carta Bajo Fe de Juramento Modalidad', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (474, 161, 222, '1.12.- Relación Insumo Producto (RIP)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (475, 161, 222, '1.13.- Descripción Literal y Grafica del Proceso Productivo (DLG)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (476, 161, 222, '1.14.- Relación de Insumos Importados', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (477, 161, 222, '1.15.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (478, 161, 222, '1.16.- Corrección de Datos Exportación', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (479, 161, 222, '1.17.- Formulario “Forma 16”', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (480, 161, 222, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (485, 163, 225, '1.03.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (486, 163, 225, '1.04.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (487, 163, 225, '1.16.- Registro de Información Fiscal (RIF) - Socios', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (488, 163, 225, '1.17.- Registro de Información Fiscal (RIF) - Administradores', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (489, 163, 225, '1.21.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (490, 163, 225, '1.22.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (491, 163, 225, '1.23.- Providencia Administrativa', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (492, 163, 225, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (1, 1, 1, 'TipoDocumentoDatosCompleta', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (493, 164, 230, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (494, 164, 230, 'PRUEBA', 2, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (495, 164, 231, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (496, 164, 231, '1.02.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (497, 164, 231, '1.03.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (498, 164, 231, '1.04.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (499, 164, 231, '1.16.- Registro de Información Fiscal (RIF) - Socios', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (500, 164, 231, '1.17.- Registro de Información Fiscal (RIF) - Administradores', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (501, 164, 231, '1.21.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (502, 164, 231, '1.22.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (503, 164, 231, '1.23.- Providencia Administrativa', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (504, 164, 231, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (505, 164, 232, '2.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (506, 164, 232, '2.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (507, 164, 232, '2.03.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (508, 164, 232, '2.04.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (509, 164, 232, '2.05.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (510, 164, 232, '2.13.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (511, 164, 232, '2.14.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (512, 164, 232, '2.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (513, 164, 233, '3.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (514, 164, 233, '3.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (515, 164, 233, '3.03.- Modificaciones del Documento Constitutivo', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (516, 164, 233, '3.04.- Solicitud de Recaudos', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (517, 164, 233, '3.05.- Alcances', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (518, 164, 233, '3.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (519, 164, 234, '10.01.- Acceso', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (520, 164, 234, '10.02.- Reseteo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (521, 164, 234, '10.03.- Usuarios Adicionales', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (522, 164, 234, '10.04.- Bloqueo Temporal', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (523, 164, 234, '10.05.- Bloqueo Definitivo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (524, 164, 234, '10.06.- I-Aduana ó I-Seniat', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (525, 164, 234, '10.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (527, 165, 237, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (528, 165, 237, '1.02.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (529, 165, 237, '1.03.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (530, 165, 237, '1.04.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (531, 165, 237, '1.16.- Registro de Información Fiscal (RIF) - Socios', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (532, 165, 237, '1.17.- Registro de Información Fiscal (RIF) - Administradores', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (533, 165, 237, '1.21.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (534, 165, 237, '1.22.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (535, 165, 237, '1.23.- Providencia Administrativa', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (538, 165, 238, '2.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (539, 165, 238, '2.03.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (540, 165, 238, '2.04.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (541, 165, 238, '2.05.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (542, 165, 238, '2.13.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (543, 165, 238, '2.14.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (544, 165, 238, '2.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (545, 165, 239, '3.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (546, 165, 239, '3.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (547, 165, 239, '3.03.- Modificaciones del Documento Constitutivo', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (548, 165, 239, '3.04.- Solicitud de Recaudos', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (549, 165, 239, '3.05.- Alcances', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (550, 165, 239, '3.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (551, 165, 240, '10.01.- Acceso', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (552, 165, 240, '10.02.- Reseteo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (553, 165, 240, '10.03.- Usuarios Adicionales', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (554, 165, 240, '10.04.- Bloqueo Temporal', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (555, 165, 240, '10.05.- Bloqueo Definitivo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (556, 165, 240, '10.06.- I-Aduana ó I-Seniat', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (557, 165, 240, '10.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (558, 166, 241, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (559, 166, 242, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (560, 166, 242, '1.02.- Cédula de Identidad', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (561, 166, 242, '1.07.- Impuesto Sobre La Renta (ISLR)', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (562, 166, 242, '1.08.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (563, 166, 242, '1.09.- Solicitud de Recaudos', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (564, 166, 242, '1.10.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (565, 166, 242, '1.11.- Providencia Administrativa', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (566, 166, 242, '1.12.- Timbres Fiscales', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (567, 166, 242, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (568, 166, 243, '2.01.- Incorporación', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (569, 166, 244, '3.01.- Transferencia', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (570, 166, 245, '4.01.- Desincorporación', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (536, 165, 237, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (571, 167, 246, '0.01.- Relación de Documentos', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (572, 167, 246, '0.02.- Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (573, 167, 247, '1.01.- Constancia de Recepción de Documentos', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (574, 167, 247, '1.02.- Solicitud de Draw Back', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (575, 167, 247, '1.03.- Declaración de Exportación  (DUA – Forma D)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (576, 167, 247, '1.04.- Factura Comercial', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (577, 167, 247, '1.05.- Documento de Transporte  BL', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (578, 167, 247, '1.06.- Documento de Transporte  CPIC/MCI', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (579, 167, 247, '1.07.- Documento de Transporte  GA', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (580, 167, 247, '1.08.- Solicitud de Recaudos Ingreso', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (581, 167, 247, '1.09.- Certificación de Venta de Divisas', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (582, 167, 247, '1.10.- Desglose de Venta de Divisas (Valor FOB)', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (583, 167, 247, '1.11.- Carta Bajo Fe de Juramento Modalidad', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (584, 167, 247, '1.12.- Relación Insumo Producto (RIP)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (585, 167, 247, '1.13.- Descripción Literal y Grafica del Proceso Productivo (DLG)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (2, 1, 1, 'TipodocumentoSimple', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (586, 167, 247, '1.14.- Relación de Insumos Importados', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (587, 167, 247, '1.15.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (588, 167, 247, '1.16.- Corrección de Datos Exportación', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (589, 167, 247, '1.17.- Formulario “Forma 16”', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (590, 167, 247, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (591, 167, 247, '1.00.- Acto de Apertura', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (3, 2, 2, '1.01.- Relacion de Prestaciones Sociales (Relacion Sumaria)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (4, 2, 3, '2.01.- Ficha de Identificacion de Acreedor', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (5, 2, 3, '2.02.- Cedula de Identidad', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (6, 2, 3, '2.03.- Pasaporte', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (7, 2, 3, '2.04.- Datos Filiatorios', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (8, 2, 4, '3.01.- Movimiento de Personal', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (9, 2, 4, '3.02.- Antecedentes de Servicios', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (10, 2, 4, '3.03.- Contrato de Servicio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (11, 2, 4, '3.04.- Oficio de Nombramiento', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (12, 2, 4, '3.05.- Constancia de Trabajo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (13, 2, 4, '3.06.- Punto de Cuenta', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (14, 2, 5, '4.01.- Calculo de Prestaciones Sociales', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (15, 2, 5, '4.02.- Calculo de Intereses', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (16, 2, 5, '4.03.- Anticipos y Finiquitos', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (17, 2, 5, '4.04.- Certificaciones de Sueldos y Salarios', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (18, 2, 5, '4.05.- Constancia de Bonificacion por Transferencia', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (22, 2, 6, '5.01.- Resuelto de Jubilacion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (23, 2, 6, '5.02.- Carta de Renuncia', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (24, 2, 6, '5.03.- Acta de Defuncion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (25, 2, 6, '5.04.- Oficio de Destitucion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (26, 2, 6, '5.05.- Oficio de Remocion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (27, 2, 7, '6.01.- Certificacion Bancaria', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (28, 2, 7, '6.02.- Acta de Certificacion (Cuenta Bancaria)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (29, 2, 7, '6.03.- Recibos de Pago', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (30, 2, 8, '7.01.- Declaracion de Unicos y Universales Herederos de Beneficiario', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (31, 2, 8, '7.02.- Cedula de Beneficiario', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (32, 2, 8, '7.03.- Partida de Nacimiento de Beneficiario', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (33, 2, 8, '7.04.- Certificacion Bancaria de Beneficiario', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (34, 2, 8, '7.05.- Sentencia Judicial', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (35, 2, 8, '7.06.- Cedula de Identidad de Tutor', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (36, 2, 8, '7.07.- Certificacion Bancaria de Tutor', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (37, 2, 9, '8.01.- Declaracion Jurada de Patrimonio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (42, 2, 10, '9.01.- Recibo de Pago', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (43, 2, 10, '9.02.- Talonario de Cheque de Gerencia BDV', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (44, 2, 10, '9.03.- Boucher de Pago BCV', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (45, 2, 10, '9.04.- Finiquito de Pago', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (46, 3, 11, '1.01.- Relacion de Prestaciones Sociales o Relacion Sumaria', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (47, 3, 11, '1.02.- Documentos para el Calculo de Prestaciones Sociales', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (48, 3, 11, '1.03.- Certificacion de Pago', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (49, 4, 12, '1.01.- Finiquito', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (50, 4, 12, '1.02.- Certificacion de Pago', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (592, 5, 250, '1.01.- Relacion de Prestaciones Sociales  (Relacion Sumaria)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (593, 5, 251, '2.01.- Cedula de Identidad', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (594, 5, 251, '2.02.- Pasaporte', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (595, 5, 251, '2.03.- Datos Filiatorios', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (596, 5, 252, '3.01.- Proposicion de Nombramiento o Movimiento de Personal', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (597, 5, 252, '3.02.- Antecedentes de Servicios', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (598, 5, 252, '3.03.- Contrato de Servicio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (599, 5, 252, '3.04.- Oficio de Nombramiento', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (600, 5, 252, '3.05.- Constancia de Trabajo', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (601, 5, 252, '3.06.- Punto de Cuenta', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (602, 5, 253, '4.01.- Calculo de Prestaciones Sociales', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (603, 5, 253, '4.02.- Calculo de Intereses', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (604, 5, 253, '4.03.- Anticipos y Finiquitos', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (605, 5, 253, '4.04.- Certificaciones de Sueldos y Salarios', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (606, 5, 254, '5.01.- Resuelto de Jubilacion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (607, 5, 254, '5.02.- Carta de Renuncia', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (608, 5, 254, '5.03.- Acta de Defuncion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (609, 5, 254, '5.04.- Oficio de Destitucion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (610, 5, 254, '5.05.- Oficio de Remocion', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (611, 5, 255, '6.01.- Certificacion Bancaria', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (612, 5, 255, '6.02.- Acta de Certificacion (Cuenta Bancaria)', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (613, 5, 255, '6.03.- Recibos de Pago', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (614, 5, 256, '7.01.- Declaracion de Unicos y Universales Herederos de Beneficiario', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (615, 5, 256, '7.02.- Cedula de Beneficiario', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (616, 5, 256, '7.03.- Partida de Nacimiento de Beneficiario', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (617, 5, 256, '7.04.- Certificacion Bancaria de Beneficiario', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (618, 5, 256, '7.05.- Sentencia Judicial', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (619, 5, 256, '7.06.- Cedula de Identidad de Tutor', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (620, 5, 256, '7.07.- Certificacion Bancaria de Tutor', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (621, 5, 257, '8.01.- Declaracion Jurada de Patrimonio', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (622, 5, 258, '9.01.- Recibo de Pago', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (623, 5, 258, '9.02.- Talonario de Cheque de Gerencia BDV', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (624, 5, 258, '9.03.- Boucher de Pago BCV', 1, '0', '1', NULL);
INSERT INTO tipodocumento VALUES (625, 5, 258, '9.04.- Finiquito de Pago', 1, '0', '1', NULL);


--
-- TOC entry 2431 (class 0 OID 21132)
-- Dependencies: 219
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario VALUES ('dw4jconf', 'Usuario', 'Configurador', NULL, NULL, 1, 'RGV2ZWxjb21HRA==');
INSERT INTO usuario VALUES ('eescalona', 'Escalona', 'Erick', '1040', NULL, 1, NULL);
INSERT INTO usuario VALUES ('nilda_blanco', 'Blanco', 'Nilda', NULL, NULL, 1, NULL);
INSERT INTO usuario VALUES ('tgarrido', 'garrido', 'thais', NULL, NULL, 1, NULL);
INSERT INTO usuario VALUES ('javier_plaza', 'Plaza', 'Javier', NULL, NULL, 1, NULL);
INSERT INTO usuario VALUES ('thais', 'thais', 'thais', NULL, NULL, 1, NULL);
INSERT INTO usuario VALUES ('asalmeron', 'salmeron', 'antonio', NULL, NULL, 1, NULL);
INSERT INTO usuario VALUES ('daniel', 'Guasamucaro', 'Daniel', NULL, NULL, 1, NULL);
INSERT INTO usuario VALUES ('david_ruiz', 'Ruiz', 'David', NULL, NULL, 1, NULL);
INSERT INTO usuario VALUES ('fbrito', 'Brito', 'Freddy', NULL, NULL, 1, NULL);


--
-- TOC entry 2432 (class 0 OID 21135)
-- Dependencies: 220
-- Data for Name: valor_dato_adicional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO valor_dato_adicional VALUES (1201, 581, '02/06/2010', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1202, 582, '10', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1203, 583, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1181, 521, '15/10/2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1182, 522, '2000', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1241, 601, '1919', 2, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1242, 602, '2013', 2, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (121, 1, 'documento jpg version 2', 1, 2, '01');
INSERT INTO valor_dato_adicional VALUES (122, 2, '1234132', 1, 2, '01');
INSERT INTO valor_dato_adicional VALUES (123, 3, '10/10/2014', 1, 2, '01');
INSERT INTO valor_dato_adicional VALUES (124, 4, 'Valor1', 1, 2, '01');
INSERT INTO valor_dato_adicional VALUES (125, 5, 'asdfgh', 1, 2, '01');
INSERT INTO valor_dato_adicional VALUES (141, 1, 'documento tif version 2', 2, 2, '01');
INSERT INTO valor_dato_adicional VALUES (142, 2, '332452', 2, 2, '01');
INSERT INTO valor_dato_adicional VALUES (143, 3, '17/10/2014', 2, 2, '01');
INSERT INTO valor_dato_adicional VALUES (144, 4, 'Dato4', 2, 2, '01');
INSERT INTO valor_dato_adicional VALUES (145, 5, 'asdfg', 2, 2, '01');
INSERT INTO valor_dato_adicional VALUES (161, 1, 'documento tif version 2', 3, 2, '01');
INSERT INTO valor_dato_adicional VALUES (162, 2, '131234', 3, 2, '01');
INSERT INTO valor_dato_adicional VALUES (163, 3, '10/10/2014', 3, 2, '01');
INSERT INTO valor_dato_adicional VALUES (164, 4, 'Dato2', 3, 2, '01');
INSERT INTO valor_dato_adicional VALUES (165, 5, 'asdfg', 3, 2, '01');
INSERT INTO valor_dato_adicional VALUES (181, 21, '10/01/2013', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (201, 61, '12/11/1999', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (221, 81, '02/06/2010', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (241, 121, '22/03/2013', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (261, 161, '21/10/2013', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (281, 241, '12/11/1988', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (301, 301, '12/06/2011', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (321, 301, '10/03/2012', 2, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (341, 321, '02/06/2010', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (361, 361, 'DOC NO CLASIFICADOS', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (381, 41, '12/08/1998', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (401, 101, '08/08/1998', 1, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (421, 41, '02/07/1999', 2, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (441, 41, '13/05/2013', 3, 0, '18331750');
INSERT INTO valor_dato_adicional VALUES (481, 41, '12/09/2009', 1, 0, '18110507');
INSERT INTO valor_dato_adicional VALUES (501, 81, '15/08/2008', 1, 0, '18110507');
INSERT INTO valor_dato_adicional VALUES (521, 121, '22/02/2012', 1, 0, '18110507');
INSERT INTO valor_dato_adicional VALUES (1221, 601, '12', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (561, 161, '22/02/2002', 1, 0, '18110507');
INSERT INTO valor_dato_adicional VALUES (1222, 602, '2013', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (601, 61, '01/01/2001', 2, 0, '18110507');
INSERT INTO valor_dato_adicional VALUES (621, 61, '11/10/2009', 3, 0, '18110507');
INSERT INTO valor_dato_adicional VALUES (2861, 841, '14/10/2014', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (1261, 641, 'REPRUEBA ', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1081, 501, 'PRUEBA 7', 4, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (701, 341, '13/12/2011', 1, 0, '18331751');
INSERT INTO valor_dato_adicional VALUES (41, 1, 'documento jpg', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (741, 361, 'DOC NO CLASIFICADOS', 1, 0, '18331751');
INSERT INTO valor_dato_adicional VALUES (761, 101, '21/12/2012', 1, 0, '18331751');
INSERT INTO valor_dato_adicional VALUES (781, 41, '10/02/2002', 1, 1, '18110507');
INSERT INTO valor_dato_adicional VALUES (801, 121, '12/11/2011', 1, 1, '18110507');
INSERT INTO valor_dato_adicional VALUES (821, 61, '12/11/1995', 2, 1, '18110507');
INSERT INTO valor_dato_adicional VALUES (461, 21, '11/02/2003', 1, 0, '18110507');
INSERT INTO valor_dato_adicional VALUES (1041, 501, 'PRUEBA 5', 2, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (2862, 842, '14', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2863, 843, '2014', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2864, 981, 'GUANTA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (861, 41, '17/08/2014', 1, 0, '17714118');
INSERT INTO valor_dato_adicional VALUES (881, 81, '15/07/2007', 1, 0, '17714118');
INSERT INTO valor_dato_adicional VALUES (901, 381, '17', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (902, 382, '2010', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (941, 441, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (942, 442, '2013', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (3041, 1221, '04/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (2981, 501, 'PDF para redigitalizar', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3044, 1227, '22/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (3047, 1233, '05/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (1281, 681, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1282, 682, '2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1301, 701, '22', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1302, 702, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1321, 721, '18', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1322, 722, '2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1341, 741, 'PRUEBA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (3058, 1247, '1345', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3059, 1248, '2007', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2, 2, '123', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3050, 1239, '1354', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (21, 1, 'documento pdf', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (1381, 741, 'PRUEBA 1', 2, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1401, 741, 'PRUEBA 2', 3, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1421, 741, 'PRUEBA 3', 4, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1441, 761, '15/10/2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1442, 762, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1443, 763, '2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1444, 901, 'SAN CRISTOBAL', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1461, 821, '12/11/1998', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1462, 822, '12', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1463, 823, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1464, 961, 'LA GUAIRA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1481, 841, '04/10/2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1482, 842, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1021, 501, 'PRUEBA 4', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (42, 2, '4535432', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (1061, 501, 'PRUEBA 6', 3, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (61, 1, 'documento pdf version 1', 1, 1, '01');
INSERT INTO valor_dato_adicional VALUES (62, 2, '2324214', 1, 1, '01');
INSERT INTO valor_dato_adicional VALUES (63, 3, '25/10/2014', 1, 1, '01');
INSERT INTO valor_dato_adicional VALUES (43, 3, '24/12/2014', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (81, 1, 'documento jpg version 1', 2, 1, '01');
INSERT INTO valor_dato_adicional VALUES (22, 2, '2142', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (23, 3, '25/10/2014', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (25, 5, 'area', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (82, 2, '1232143', 2, 1, '01');
INSERT INTO valor_dato_adicional VALUES (83, 3, '18/10/2014', 2, 1, '01');
INSERT INTO valor_dato_adicional VALUES (84, 4, 'Dato4', 2, 1, '01');
INSERT INTO valor_dato_adicional VALUES (101, 1, 'documento pdf version 1', 3, 1, '01');
INSERT INTO valor_dato_adicional VALUES (102, 2, '4242', 3, 1, '01');
INSERT INTO valor_dato_adicional VALUES (921, 401, '11/11/2011', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (922, 402, '2011', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (45, 5, 'area', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (103, 3, '25/10/2014', 3, 1, '01');
INSERT INTO valor_dato_adicional VALUES (104, 4, 'Dato3', 3, 1, '01');
INSERT INTO valor_dato_adicional VALUES (44, 4, 'Dato5', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (1483, 843, '2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1484, 981, 'PTO. PALMARITO', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1183, 523, 'GUANTA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1501, 861, '03/10/2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1502, 862, '1122', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1503, 863, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1504, 1001, 'BOCA DE GRITA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1581, 401, '15/11/2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1582, 402, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1583, 403, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (3081, 401, '15/10/2013', 1, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (1621, 481, '04/10/2014', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1622, 482, '2014', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1623, 483, 'PTO. CABELLO', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2281, 741, 'PRUEBA', 3, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (3082, 402, '1', 1, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (3083, 403, '2011', 1, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (2341, 741, 'PRUEBA 4', 5, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2361, 781, '12/12/2012', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (1661, 521, '15/10/2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1662, 522, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1663, 523, 'EL GUAMACHE', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1681, 561, '15/11/2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1682, 562, '15', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1683, 563, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1701, 601, '225', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1702, 602, '2013', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2362, 782, '12121', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (1741, 381, '1919', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1742, 382, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2363, 783, '2012', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2364, 921, 'SAN CRISTOBAL', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2381, 821, '12/10/2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2382, 822, '21010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2383, 823, '2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (1781, 421, '12/10/2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1782, 422, '1213', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1783, 423, '2013', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1801, 441, '4518', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1802, 442, '2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2384, 961, 'GUANTA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (1841, 661, '15/10/2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1842, 662, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1843, 663, 'PTO. AYACUCHO', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2401, 861, '15/10/2008', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2402, 862, '1510', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2403, 863, '2011', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2404, 1001, 'PTO. PALMARITO', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2421, 881, 'PRUEBA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2881, 821, '02/10/2014', 1, 1, '4567890');
INSERT INTO valor_dato_adicional VALUES (2882, 822, '1545', 1, 1, '4567890');
INSERT INTO valor_dato_adicional VALUES (2883, 823, '2014', 1, 1, '4567890');
INSERT INTO valor_dato_adicional VALUES (2884, 961, 'BOCA DE GRITA', 1, 1, '4567890');
INSERT INTO valor_dato_adicional VALUES (2221, 701, '1212', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2222, 702, '2013', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (3001, 501, 'JPG para redigitalizar', 2, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3042, 1223, '11/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (3045, 1229, '09/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (3048, 1235, '05/10/2013', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (2161, 601, '1821', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2162, 602, '2011', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (3052, 1241, '15/10/2009', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3053, 1242, '13234', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3054, 1243, '2008', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3060, 1249, '1567', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3061, 1250, '2008', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (65, 5, 'qwerty', 1, 1, '01');
INSERT INTO valor_dato_adicional VALUES (64, 4, 'Valor1', 1, 1, '01');
INSERT INTO valor_dato_adicional VALUES (85, 5, 'qwerty', 2, 1, '01');
INSERT INTO valor_dato_adicional VALUES (105, 5, 'qwerty', 3, 1, '01');
INSERT INTO valor_dato_adicional VALUES (3051, 1240, '2008', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3067, 1239, '03', 1, 2, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3068, 1240, '2008', 1, 2, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2201, 681, '0424', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (1881, 761, '12/12/2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1882, 762, '201212', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2202, 682, '2014', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (3074, 1254, 'Para redigitalizar JPG', 6, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (1941, 881, 'PRUEBA 1', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2701, 761, '21/10/2010', 2, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2101, 541, '1312', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2102, 542, '2011', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2241, 741, 'PRUEBA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2121, 581, '21/10/2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2321, 741, 'PRUEBA 3', 4, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (3077, 681, '1515', 1, 1, '4567890');
INSERT INTO valor_dato_adicional VALUES (2122, 582, '2121', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (1901, 801, '15/09/2009', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1902, 802, '2009', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1903, 803, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1904, 941, 'GUANTA', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1921, 841, '12/11/2007', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1922, 842, '121188', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1923, 843, '2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1924, 981, 'PTO. AYACUCHO', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (3078, 682, '2015', 1, 1, '4567890');
INSERT INTO valor_dato_adicional VALUES (2123, 583, '2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2181, 621, '121188', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2182, 622, '2015', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2261, 741, 'PRUEBA 1', 2, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (3084, 441, '1988', 1, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (3085, 442, '2015', 1, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (1541, 881, 'PRUEBA 1', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (1, 1, 'documento tiff', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3107, 1, 'eliminar 3', 5, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3108, 2, '43745', 5, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3109, 3, '31/10/2014', 5, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3111, 5, 'eliminar 3', 5, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3, 3, '13/10/2014', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (4, 4, 'Dato1', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (5, 5, 'area', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3110, 4, 'Dato3', 5, 0, '01');
INSERT INTO valor_dato_adicional VALUES (2721, 541, '12', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2722, 542, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1861, 721, '0216', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1862, 722, '2014', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2901, 461, '3', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2902, 462, '2013', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (24, 4, 'Dato5', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3097, 1, 'eliminar 1', 4, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3098, 2, '24214', 4, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3021, 501, 'TIFF para redigitalizar', 3, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3043, 1225, '10/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (3046, 1231, '04/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (3049, 1237, '08/10/2014', 1, 0, '17110114');
INSERT INTO valor_dato_adicional VALUES (3055, 1244, '08/10/2011', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3056, 1245, '1245', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3057, 1246, '2009', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3062, 1251, '1511', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3063, 1252, '2009', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3064, 1253, 'LA GUAIRA', 1, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3065, 1239, '02', 1, 1, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3066, 1240, '2009', 1, 1, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (1883, 763, '2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (1884, 901, 'EL GUAMACHE', 1, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2702, 762, '5', 2, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2703, 763, '2015', 2, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2704, 901, 'PTO. PALMARITO', 2, 0, '16415223');
INSERT INTO valor_dato_adicional VALUES (2741, 381, '123456', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2742, 382, '2011', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2781, 421, '05/05/2011', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2782, 422, '156348', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2783, 423, '2011', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional VALUES (2821, 601, '1235', 2, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2822, 602, '2014', 2, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (2841, 641, 'PRUEBA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional VALUES (3075, 1239, '01', 2, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3076, 1240, '2005', 2, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3099, 3, '29/10/2014', 4, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3112, 1, 'eliminar 4', 6, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3175, 1265, '1985', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3176, 1266, '1985', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (923, 403, '2011', 1, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (3079, 381, '12121', 1, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (3080, 382, '2012', 1, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (3177, 1271, '12', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3178, 1272, '2012', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3179, 1273, '04.- GUANTA', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3141, 1254, 'TIFF Eliminar', 1, 0, 'J-98765432-1');
INSERT INTO valor_dato_adicional VALUES (3142, 1254, 'TIFF Pruebas', 2, 0, 'J-98765432-1');
INSERT INTO valor_dato_adicional VALUES (3073, 1254, 'Para redigitalizar PDF', 4, 0, 'J-1234567-8');
INSERT INTO valor_dato_adicional VALUES (3086, 501, 'PRUEBAS', 2, 1, '17718114');
INSERT INTO valor_dato_adicional VALUES (3147, 1257, '01/10/2014', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3148, 1258, '12', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3149, 1259, '2014', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3150, 1257, '31/01/2001', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3151, 1258, '555', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3152, 1259, '2008', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3153, 1260, '12/10/2014', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3154, 1261, '1210', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3155, 1262, '2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3156, 1260, '06/05/2008', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3157, 1261, '12345', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3158, 1262, '2008', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3159, 1263, '15', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3160, 1264, '2007', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3161, 1265, '15', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3162, 1266, '2011', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3163, 1267, '1', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3133, 501, 'REPRUEBAS n', 5, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (3135, 501, 'REPRUEBAS 3n', 6, 0, '17718114');
INSERT INTO valor_dato_adicional VALUES (3113, 2, '2424', 6, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3120, 4, 'Dato55', 7, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3121, 5, 'eliminar 5', 7, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3117, 1, 'eliminar 5', 7, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3118, 2, '43646', 7, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3119, 3, '02/11/2014', 7, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3127, 1, 'eliminar 7', 8, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3128, 2, '437657', 8, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3129, 3, '04/11/2014', 8, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3130, 4, 'Valor11', 8, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3131, 5, 'eliminar 7', 8, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3164, 1268, '2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3165, 1269, '03.- EL GUAMACHE', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3168, 1260, '02/10/2014', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3169, 1261, '2', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3170, 1262, '2010', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3171, 1270, 'PRUEBA', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3174, 1270, 'PRUEBA 1', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3183, 1274, '15', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3184, 1275, '2000', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3186, 1274, '45', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3187, 1275, '2004', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3188, 1276, '12/11/2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3189, 1277, '1211', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3190, 1278, '2011', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3191, 1279, '12/10/2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3192, 1280, '2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3193, 1281, '20', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3194, 1271, '265', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3195, 1272, '1999', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3196, 1273, '02.- PTO. CABELLO', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3197, 1274, '0125', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3198, 1275, '1992', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3166, 1255, '14', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3172, 1263, '666', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3173, 1264, '2014', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3145, 1255, '100', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3146, 1256, '2000', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3180, 1267, '5555', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3181, 1268, '2010', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3185, 1270, 'jajaja', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3114, 3, '01/11/2014', 6, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3116, 5, 'eliminar 4', 6, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3115, 4, 'Dato4', 6, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3201, 1276, '01/01/2014', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3202, 1277, '0055', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3203, 1278, '2015', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3204, 1282, '34', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3205, 1283, '2014', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3206, 1284, '122', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3207, 1285, '2012', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3208, 1279, '01/01/2015', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3209, 1280, '10089', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3210, 1281, '1998', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3211, 1282, '3500', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3212, 1283, '1985', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3213, 1284, '1', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3214, 1285, '1969', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3217, 1286, 'El Guapeton', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3225, 1287, '12', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3226, 1288, '2012', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3227, 1289, '06.- PTO. AYACUCHO', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3228, 1290, '1985', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3229, 1291, '2015', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3230, 1287, '45', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3231, 1288, '1998', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3232, 1289, '06.- PTO. AYACUCHO', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3233, 1287, '4541', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3234, 1288, '1999', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3235, 1289, '07.-  BOCA DE GRITA', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3238, 1290, '12', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3239, 1291, '2012', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3240, 1294, '12', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3241, 1295, '2012', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3242, 1296, 'Subete la minifalda hasta la espalda.', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3243, 1292, '1222', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3244, 1293, '2012', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3245, 1294, '10210', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3246, 1295, '2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3247, 1296, 'PRUEBA ', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3248, 1296, 'PRUEBAS 3', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3253, 1300, '05.- SAN CRISTOBAL', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3254, 1301, '8596848', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3255, 1302, '1996', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3249, 1296, 'PRUEBAS 4', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3259, 1306, '02.- PTO. CABELLO', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3260, 1307, '64654', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3261, 1308, '1999', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3265, 1290, '156496', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3266, 1291, '2010', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3267, 1312, '06.- PTO. AYACUCHO', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3268, 1313, '1999', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3269, 1314, '1999', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3272, 1294, '12', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3273, 1295, '2012', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3274, 1297, '05.- SAN CRISTOBAL', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3275, 1298, '1', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3276, 1299, '2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3277, 1297, '07.-  BOCA DE GRITA', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3278, 1298, '2', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3279, 1299, '2012', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3280, 1297, '01.- LA GUAIRA', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3281, 1298, '3', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3282, 1299, '2013', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3283, 1297, '08.- PTO. PALMARITO', 4, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3284, 1298, '4', 4, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3285, 1299, '2014', 4, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3286, 1300, '02.- PTO. CABELLO', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3287, 1301, '2', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3288, 1302, '2002', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3289, 1303, '03.- EL GUAMACHE', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3290, 1304, '9898', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3291, 1305, '1987', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3292, 1306, '04.- GUANTA', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3293, 1307, '4', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3294, 1308, '2004', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3295, 1309, '08.- PTO. PALMARITO', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3296, 1310, '4545', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3297, 1311, '2005', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3298, 1312, '08.- PTO. PALMARITO', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3299, 1313, '15', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3300, 1314, '2011', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3301, 1312, '05.- SAN CRISTOBAL', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3302, 1313, '66', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3303, 1314, '2009', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3304, 1312, '02.- PTO CABELLO', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3305, 1313, '222', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3306, 1314, '2002', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3307, 1315, 'PRUEBAS ', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3308, 1315, 'PRUEBAS 1', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3309, 1315, 'PRUEBAS 2', 3, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3310, 1315, 'PRUEBAS 3', 4, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3167, 1256, '2014', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3199, 1282, '21', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3200, 1283, '2010', 1, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3215, 1284, '215', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3216, 1285, '2010', 2, 0, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3311, 1271, '01', 1, 1, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3312, 1272, '2001', 1, 1, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3313, 1273, '04.- GUANTA', 1, 1, 'J-121188-9');
INSERT INTO valor_dato_adicional VALUES (3221, 1287, '99', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3222, 1288, '1999', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3223, 1289, '02.- PTO. CABELLO', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3236, 1292, '54', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3237, 1293, '210', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3182, 1269, '04.- GUANTA', 1, 0, 'V-18760843-7');
INSERT INTO valor_dato_adicional VALUES (3316, 1318, '31/12/2008', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3317, 1319, '165', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3318, 1320, '2008', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3319, 1321, '01/01/2009', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3320, 1322, '554649891', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3321, 1323, '2008', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3322, 1324, '6546498', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3323, 1325, '2010', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3324, 1328, '21313', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3325, 1329, '02005', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3326, 1330, '03.- El Guamache', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3314, 1316, '125', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3315, 1317, '2008', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3101, 5, 'eliminar 1', 4, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3100, 4, 'Dato11', 4, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3387, 1, 'pruebaweb', 9, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3388, 2, '123456', 9, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3389, 3, '01/01/2015', 9, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3390, 4, 'Dato2', 9, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3391, 5, 'pruebaweb123654', 9, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3402, 1, 'wdsfsf', 10, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3403, 2, '53532', 10, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3404, 3, '07/01/2015', 10, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3405, 4, 'Dato11', 10, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3406, 5, 'dfsfbdsbg', 10, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3407, 1, 'texto', 11, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3333, 1316, '65465', 1, 1, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3334, 1317, 'Version PDF', 1, 1, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3335, 1316, '65416', 1, 2, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3336, 1317, 'JPG Version', 1, 2, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3332, 1331, 'JPG', 3, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3330, 1331, 'TIFF', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3331, 1331, 'JPG', 2, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3337, 1335, '5555', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3338, 1336, '2008', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3339, 1337, '03.- El Guamache', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3340, 1338, '5552', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3341, 1339, '2008 JPG', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3342, 1340, '31/12/2010', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3343, 1341, '6566', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3344, 1342, '2005', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3345, 1343, '05/11/2014', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3346, 1344, '52555', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3347, 1345, '0001', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3350, 1326, '5628', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3351, 1327, 'Pruebas Reportes', 1, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3408, 2, '12345', 11, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3409, 3, '26/02/2015', 11, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3352, 1328, '2500', 2, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3353, 1329, '1620', 2, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3354, 1330, '02.- Pto Cabello', 2, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3355, 1324, '0158', 3, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3356, 1325, 'Por Rechazar.', 3, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3348, 1324, '123456', 2, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3349, 1325, 'Prueba Reporte 1', 2, 0, 'J-12345678-9');
INSERT INTO valor_dato_adicional VALUES (3357, 1351, '06/06/2005', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3358, 1352, '137397', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3359, 1353, '13/01/2005', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3360, 1354, 'La Guaira', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3364, 1358, '03/12/2004', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3365, 1359, '07/12/2004', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3366, 1362, '13/01/2005', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3367, 1363, '31/05/2005', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3368, 1364, '12/01/2005', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3369, 1365, '18/04/2007', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3370, 1366, '12/01/2005', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3371, 1368, '29/09/2006', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3372, 1371, 'PRORROGA', 1, 0, '137397');
INSERT INTO valor_dato_adicional VALUES (3373, 1221, '06/08/2007', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3374, 1351, '06/08/2007', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3375, 1352, '144985', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3376, 1353, '15/07/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3377, 1354, 'Aérea Metropolitana de Caracas', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3381, 1358, '14/07/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3382, 1359, '14/07/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3383, 1362, '18/08/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3384, 1363, '01/11/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3385, 1364, '10/08/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3386, 1368, '10/08/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3431, 1355, '009834562', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3432, 1356, '28/08/2013', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3433, 1357, 'Aérea Metropolitana de Caracas', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3434, 1366, '12/08/2013', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3435, 1370, '12/08/2013', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3410, 4, 'Valor5', 11, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3411, 5, 'area', 11, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3412, 1373, 'webtexto', 11, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3413, 1351, '09/03/2015', 2, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3414, 1352, '144985', 2, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3415, 1353, '03/02/2014', 2, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3416, 1354, 'Aeropuerto Internacional Simón Bolívar', 2, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3417, 1371, 'Documento de Prueba', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3418, 1371, 'Documento de Prueba 2', 2, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3419, 1352, '144985', 2, 1, '144985');
INSERT INTO valor_dato_adicional VALUES (3420, 1353, '03/02/2014', 2, 1, '144985');
INSERT INTO valor_dato_adicional VALUES (3421, 1354, 'Higuerote', 2, 1, '144985');
INSERT INTO valor_dato_adicional VALUES (3378, 1355, '144985', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3379, 1356, '18/08/2005', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3380, 1357, 'Puerto Cabello', 1, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3422, 1352, '144985', 3, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3423, 1353, '01/03/2013', 3, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3424, 1354, 'Charallave', 3, 0, '144985');
INSERT INTO valor_dato_adicional VALUES (3428, 1352, '192785', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3429, 1353, '12/08/2013', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3430, 1354, 'Aérea Metropolitana de Caracas', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3436, 1359, '12/08/2013', 1, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3437, 1355, '009834562', 2, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3438, 1356, '28/08/2013', 2, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3439, 1357, 'Aérea Metropolitana de Caracas', 2, 0, '192785');
INSERT INTO valor_dato_adicional VALUES (3440, 1351, '04/05/2015', 1, 0, '245689');
INSERT INTO valor_dato_adicional VALUES (3441, 1352, '245689', 1, 0, '245689');
INSERT INTO valor_dato_adicional VALUES (3442, 1353, '18/11/2014', 1, 0, '245689');
INSERT INTO valor_dato_adicional VALUES (3443, 1354, 'Aérea de La Carlota', 1, 0, '245689');
INSERT INTO valor_dato_adicional VALUES (3444, 1359, '14/05/2015', 1, 0, '245689');
INSERT INTO valor_dato_adicional VALUES (3445, 1, 'er', 1, 0, '02');
INSERT INTO valor_dato_adicional VALUES (3446, 2, '32', 1, 0, '02');
INSERT INTO valor_dato_adicional VALUES (3447, 3, '01/01/2014', 1, 0, '02');
INSERT INTO valor_dato_adicional VALUES (3448, 4, 'Dato2', 1, 0, '02');
INSERT INTO valor_dato_adicional VALUES (3449, 5, 'er', 1, 0, '02');
INSERT INTO valor_dato_adicional VALUES (3450, 1373, 'we', 1, 0, '02');
INSERT INTO valor_dato_adicional VALUES (1375, 6, '250001', 1, 0, '250001');
INSERT INTO valor_dato_adicional VALUES (1376, 7, '01 - Sencilla', 1, 0, '250001');
INSERT INTO valor_dato_adicional VALUES (1377, 8, 'archivo1.pdf', 1, 0, '250001');
INSERT INTO valor_dato_adicional VALUES (1378, 9, '7896541', 1, 0, '250001');
INSERT INTO valor_dato_adicional VALUES (1379, 10, '22/09/2016', 1, 0, '250001');
INSERT INTO valor_dato_adicional VALUES (3452, 6, '250002', 1, 0, '250002');
INSERT INTO valor_dato_adicional VALUES (3453, 7, '02 - Doble', 1, 0, '250002');
INSERT INTO valor_dato_adicional VALUES (3454, 8, 'archivo2.pdf', 1, 0, '250002');
INSERT INTO valor_dato_adicional VALUES (3455, 9, 'Num Cheque 17289645', 1, 0, '250002');
INSERT INTO valor_dato_adicional VALUES (3456, 10, '15/11/2010', 1, 0, '250002');
INSERT INTO valor_dato_adicional VALUES (3457, 6, '250003', 1, 0, '250003');
INSERT INTO valor_dato_adicional VALUES (3458, 7, '04 - Parcial Doble', 1, 0, '250003');
INSERT INTO valor_dato_adicional VALUES (3459, 8, 'archivo4.pdf', 1, 0, '250003');
INSERT INTO valor_dato_adicional VALUES (3460, 9, 'Num. Recibo 78524169', 1, 0, '250003');
INSERT INTO valor_dato_adicional VALUES (3461, 10, '15/12/2012', 1, 0, '250003');
INSERT INTO valor_dato_adicional VALUES (3462, 6, '250003-1', 2, 0, '250003');
INSERT INTO valor_dato_adicional VALUES (3463, 7, '02 - Doble', 2, 0, '250003');
INSERT INTO valor_dato_adicional VALUES (3464, 6, '250004', 1, 0, '250004');
INSERT INTO valor_dato_adicional VALUES (3465, 7, '05 - Otros', 1, 0, '250004');
INSERT INTO valor_dato_adicional VALUES (3466, 8, 'archivo5.pdf', 1, 0, '250004');
INSERT INTO valor_dato_adicional VALUES (3467, 9, 'Num. Finiquito 23672198', 1, 0, '250004');
INSERT INTO valor_dato_adicional VALUES (3468, 10, '20/08/2013', 1, 0, '250004');
INSERT INTO valor_dato_adicional VALUES (3469, 55, '2012', 1, 0, '785001');
INSERT INTO valor_dato_adicional VALUES (3470, 56, 'Num. Finiquito 56983421', 1, 0, '785001');
INSERT INTO valor_dato_adicional VALUES (3471, 57, '25/11/2012', 1, 0, '785001');
INSERT INTO valor_dato_adicional VALUES (3472, 55, '2011', 1, 0, '785002');
INSERT INTO valor_dato_adicional VALUES (3473, 56, 'NUM. CHEQUE 79425133', 1, 0, '785002');
INSERT INTO valor_dato_adicional VALUES (3474, 57, '23/06/2011', 1, 0, '785002');
INSERT INTO valor_dato_adicional VALUES (3475, 56, 'NUM. FINIQUITO456325874', 2, 0, '785002');
INSERT INTO valor_dato_adicional VALUES (3476, 57, '15/08/2011', 2, 0, '785002');
INSERT INTO valor_dato_adicional VALUES (3477, 55, '2010', 1, 0, '785003');
INSERT INTO valor_dato_adicional VALUES (3478, 56, 'NUM. RECIBO 42237845', 1, 0, '785003');
INSERT INTO valor_dato_adicional VALUES (3479, 57, '18/07/2010', 1, 0, '785003');
INSERT INTO valor_dato_adicional VALUES (3480, 11, '322301', 1, 0, '322301');
INSERT INTO valor_dato_adicional VALUES (3481, 12, '01 - Sencilla', 1, 0, '322301');
INSERT INTO valor_dato_adicional VALUES (3482, 13, 'm-3543', 1, 0, '322301');
INSERT INTO valor_dato_adicional VALUES (3483, 14, '09/08/2016', 1, 0, '322301');
INSERT INTO valor_dato_adicional VALUES (3484, 15, '03 - Egreso', 1, 0, '322301');
INSERT INTO valor_dato_adicional VALUES (3485, 11, '322302', 1, 0, '322302');
INSERT INTO valor_dato_adicional VALUES (3486, 12, '02 - Doble', 1, 0, '322302');
INSERT INTO valor_dato_adicional VALUES (3487, 13, 'M-5498845', 1, 0, '322302');
INSERT INTO valor_dato_adicional VALUES (3488, 14, '31/10/2008', 1, 0, '322302');
INSERT INTO valor_dato_adicional VALUES (3489, 15, '02 - Jubilacion', 1, 0, '322302');
INSERT INTO valor_dato_adicional VALUES (3490, 1375, '67876001', 1, 0, '67876001');
INSERT INTO valor_dato_adicional VALUES (3491, 1376, '02 - Doble', 1, 0, '67876001');
INSERT INTO valor_dato_adicional VALUES (3492, 1380, 'M-53226445', 1, 0, '67876001');
INSERT INTO valor_dato_adicional VALUES (3493, 1381, '02/06/2015', 1, 0, '67876001');
INSERT INTO valor_dato_adicional VALUES (3494, 1382, '02 - Jubilacion', 1, 0, '67876001');
INSERT INTO valor_dato_adicional VALUES (3495, 1375, '67876002', 1, 0, '67876002');
INSERT INTO valor_dato_adicional VALUES (3496, 1376, '01 - Sencilla', 1, 0, '67876002');
INSERT INTO valor_dato_adicional VALUES (3497, 1390, '07/09/2016', 1, 0, '67876002');


--
-- TOC entry 2218 (class 2606 OID 21143)
-- Name: categoria categoria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pk PRIMARY KEY (id_categoria);


--
-- TOC entry 2220 (class 2606 OID 21145)
-- Name: causa causa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY causa
    ADD CONSTRAINT causa_pk PRIMARY KEY (id_causa);


--
-- TOC entry 2228 (class 2606 OID 21147)
-- Name: documento_eliminado documento_eliminado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documento_eliminado
    ADD CONSTRAINT documento_eliminado_pk PRIMARY KEY (id_doc_eliminado);


--
-- TOC entry 2232 (class 2606 OID 21149)
-- Name: estatus_documento estatus_documento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estatus_documento
    ADD CONSTRAINT estatus_documento_pk PRIMARY KEY (id_estatus_documento);


--
-- TOC entry 2230 (class 2606 OID 21151)
-- Name: estatus estatus_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estatus
    ADD CONSTRAINT estatus_pk PRIMARY KEY (id_estatus);


--
-- TOC entry 2238 (class 2606 OID 21153)
-- Name: foliatura foliatura_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY foliatura
    ADD CONSTRAINT foliatura_pk PRIMARY KEY (id_foliatura);


--
-- TOC entry 2244 (class 2606 OID 21155)
-- Name: libreria libreria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY libreria
    ADD CONSTRAINT libreria_pk PRIMARY KEY (id_libreria);


--
-- TOC entry 2248 (class 2606 OID 21157)
-- Name: perfil perfil_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pk PRIMARY KEY (id_perfil);


--
-- TOC entry 2246 (class 2606 OID 21159)
-- Name: lista_desplegables pk_combo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lista_desplegables
    ADD CONSTRAINT pk_combo PRIMARY KEY (id_lista);


--
-- TOC entry 2222 (class 2606 OID 21161)
-- Name: configuracion pk_configuracion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY configuracion
    ADD CONSTRAINT pk_configuracion PRIMARY KEY (id_configuracion);


--
-- TOC entry 2224 (class 2606 OID 21163)
-- Name: dato_adicional pk_dato_adicional; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT pk_dato_adicional PRIMARY KEY (id_dato_adicional);


--
-- TOC entry 2226 (class 2606 OID 21165)
-- Name: datos_infodocumento pk_datos_infodocumento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT pk_datos_infodocumento PRIMARY KEY (id_datos);


--
-- TOC entry 2234 (class 2606 OID 21167)
-- Name: expedientes pk_expedientes; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT pk_expedientes PRIMARY KEY (id_expedientes);


--
-- TOC entry 2236 (class 2606 OID 21169)
-- Name: fabrica pk_fabrica; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fabrica
    ADD CONSTRAINT pk_fabrica PRIMARY KEY (usuario);


--
-- TOC entry 2240 (class 2606 OID 21171)
-- Name: indices pk_indice; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT pk_indice PRIMARY KEY (id_indice);


--
-- TOC entry 2242 (class 2606 OID 21173)
-- Name: infodocumento pk_infordocumento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT pk_infordocumento PRIMARY KEY (id_infodocumento);


--
-- TOC entry 2258 (class 2606 OID 21175)
-- Name: valor_dato_adicional pk_valor_dato_adicional; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT pk_valor_dato_adicional PRIMARY KEY (id_valor);


--
-- TOC entry 2250 (class 2606 OID 21177)
-- Name: rol rol_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pk PRIMARY KEY (id_rol);


--
-- TOC entry 2252 (class 2606 OID 21179)
-- Name: subcategoria subcategoria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subcategoria
    ADD CONSTRAINT subcategoria_pk PRIMARY KEY (id_subcategoria);


--
-- TOC entry 2254 (class 2606 OID 21181)
-- Name: tipodocumento tipodocumento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT tipodocumento_pk PRIMARY KEY (id_documento);


--
-- TOC entry 2256 (class 2606 OID 21183)
-- Name: usuario usuario_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id_usuario);


--
-- TOC entry 2259 (class 2606 OID 21184)
-- Name: categoria fk_categoria_estatus; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT fk_categoria_estatus FOREIGN KEY (id_estatus) REFERENCES estatus(id_estatus);


--
-- TOC entry 2260 (class 2606 OID 21189)
-- Name: categoria fk_categoria_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT fk_categoria_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


--
-- TOC entry 2261 (class 2606 OID 21194)
-- Name: dato_adicional fk_dato_adicional_tipodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT fk_dato_adicional_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


--
-- TOC entry 2262 (class 2606 OID 21199)
-- Name: datos_infodocumento fk_datoc_infodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datoc_infodoc FOREIGN KEY (id_infodocumento) REFERENCES infodocumento(id_infodocumento);


--
-- TOC entry 2263 (class 2606 OID 21204)
-- Name: datos_infodocumento fk_datos_doc_eliminado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datos_doc_eliminado FOREIGN KEY (id_doc_eliminado) REFERENCES documento_eliminado(id_doc_eliminado);


--
-- TOC entry 2264 (class 2606 OID 21209)
-- Name: expedientes fk_expediente_indices; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expediente_indices FOREIGN KEY (id_indice) REFERENCES indices(id_indice);


--
-- TOC entry 2265 (class 2606 OID 21214)
-- Name: expedientes fk_expedientes_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2266 (class 2606 OID 21219)
-- Name: expedientes fk_expedientes_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


--
-- TOC entry 2267 (class 2606 OID 21224)
-- Name: fabrica fk_fabrica_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fabrica
    ADD CONSTRAINT fk_fabrica_usuario FOREIGN KEY (usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2268 (class 2606 OID 21229)
-- Name: indices fk_indice_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT fk_indice_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2269 (class 2606 OID 21234)
-- Name: infodocumento fk_infodoc_statusdoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_statusdoc FOREIGN KEY (estatus_documento) REFERENCES estatus_documento(id_estatus_documento);


--
-- TOC entry 2270 (class 2606 OID 21239)
-- Name: infodocumento fk_infodoc_tipodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


--
-- TOC entry 2271 (class 2606 OID 21244)
-- Name: perfil fk_perfil_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2272 (class 2606 OID 21249)
-- Name: perfil fk_perfil_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


--
-- TOC entry 2273 (class 2606 OID 21254)
-- Name: perfil fk_perfil_rol; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_rol FOREIGN KEY (id_rol) REFERENCES rol(id_rol);


--
-- TOC entry 2274 (class 2606 OID 21259)
-- Name: perfil fk_perfil_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_usuario FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2275 (class 2606 OID 21264)
-- Name: subcategoria fk_subcategoria_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subcategoria
    ADD CONSTRAINT fk_subcategoria_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2276 (class 2606 OID 21269)
-- Name: tipodocumento fk_tipodoc_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2277 (class 2606 OID 21274)
-- Name: tipodocumento fk_tipodoc_subcategoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_subcategoria FOREIGN KEY (id_subcategoria) REFERENCES subcategoria(id_subcategoria);


--
-- TOC entry 2278 (class 2606 OID 21279)
-- Name: usuario fk_usuario_estatus; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fk_usuario_estatus FOREIGN KEY (id_estatus) REFERENCES estatus(id_estatus);


--
-- TOC entry 2279 (class 2606 OID 21284)
-- Name: valor_dato_adicional kf_valor_indice_da; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT kf_valor_indice_da FOREIGN KEY (id_dato_adicional) REFERENCES dato_adicional(id_dato_adicional);


-- Completed on 2017-04-30 20:35:47

--
-- PostgreSQL database dump complete
--

