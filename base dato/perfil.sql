--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.1
-- Started on 2015-02-04 10:25:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2974 (class 0 OID 35082)
-- Dependencies: 199
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (1, NULL, NULL, 'dw4jconf', 7);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (2, NULL, NULL, 'dw4jconf', 8);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (43, 1, 1, 'eescalona', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (44, 1, 1, 'eescalona', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (45, 1, 1, 'eescalona', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (46, 1, 1, 'eescalona', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (47, 1, 1, 'eescalona', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (48, 1, 1, 'eescalona', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (49, NULL, NULL, 'eescalona', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (50, 41, 41, 'eescalona', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (51, 41, 41, 'eescalona', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (52, 41, 41, 'eescalona', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (53, 41, 41, 'eescalona', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (54, 41, 41, 'eescalona', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (55, 41, 41, 'eescalona', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (63, NULL, NULL, 'tgarrido', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (64, 41, 41, 'tgarrido', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (65, 41, 41, 'tgarrido', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (66, 41, 41, 'tgarrido', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (67, 41, 41, 'tgarrido', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (68, 41, 41, 'tgarrido', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (69, 41, 41, 'tgarrido', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (512, 21, 21, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (513, 21, 21, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (514, 21, 21, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (515, 21, 21, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (516, 21, 21, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (517, 21, 21, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (518, 103, 163, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (519, 103, 163, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (520, 103, 163, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (521, 103, 163, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (522, 103, 163, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (523, 103, 163, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (524, 105, 165, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (525, 105, 165, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (526, 105, 165, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (527, 105, 165, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (528, 105, 165, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (529, 105, 165, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (530, 104, 164, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (531, 104, 164, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (532, 104, 164, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (533, 104, 164, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (534, 104, 164, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (535, 104, 164, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (536, 61, 61, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (537, 61, 61, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (538, 61, 61, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (539, 61, 61, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (540, 61, 61, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (541, 61, 61, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (542, 61, 81, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (543, 61, 81, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (544, 61, 81, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (545, 61, 81, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (546, 61, 81, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (547, 61, 81, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (548, 61, 121, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (549, 61, 121, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (550, 61, 121, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (551, 61, 121, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (552, 61, 121, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (553, 61, 121, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (554, 1, 1, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (463, 41, 41, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (464, 41, 41, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (465, 41, 41, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (466, 41, 41, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (467, 41, 41, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (468, 41, 41, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (469, 101, 161, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (470, 101, 161, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (471, 101, 161, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (472, 101, 161, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (473, 101, 161, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (474, 101, 161, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (475, 104, 164, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (476, 104, 164, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (477, 104, 164, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (478, 104, 164, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (479, 104, 164, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (480, 104, 164, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (481, 61, 61, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (482, 61, 61, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (483, 61, 61, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (484, 61, 61, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (485, 61, 61, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (486, 61, 61, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (487, 61, 81, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (488, 61, 81, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (489, 61, 81, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (490, 61, 81, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (491, 61, 81, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (492, 61, 81, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (493, 61, 121, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (494, 61, 121, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (495, 61, 121, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (496, 61, 121, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (497, 61, 121, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (498, 61, 121, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (499, NULL, NULL, 'nilda_blanco', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (500, 21, 21, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (501, 21, 167, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (502, 21, 21, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (503, 21, 167, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (504, 21, 21, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (505, 21, 167, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (506, 21, 21, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (507, 21, 167, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (508, 21, 21, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (509, 21, 167, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (510, 21, 21, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (511, 21, 167, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (555, 1, 1, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (556, 1, 1, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (557, 1, 1, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (558, 1, 1, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (559, 1, 1, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (560, NULL, NULL, 'javier_plaza', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (561, 21, 167, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (562, 21, 167, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (563, 21, 167, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (564, 21, 167, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (565, 21, 167, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (566, 21, 167, 'javier_plaza', 9);


-- Completed on 2015-02-04 10:25:11

--
-- PostgreSQL database dump complete
--

