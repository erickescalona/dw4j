/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.reportes;

import com.develcom.autentica.Perfil;
import com.develcom.autentica.Usuario;
import com.develcom.dao.Campos;
import com.develcom.dao.Expediente;
import com.develcom.dao.Mensajes;
import com.develcom.excepcion.DW4JDesktopExcepcion;
import com.develcom.paginas.reportes.tools.ConstruyeConsultas;
import com.develcom.paginas.reportes.tools.ProcesaReporte;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;
import ve.com.develcom.sesion.IniciaSesion;

/**
 *
 * @author develcom
 */
public class Reportes extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibreria;

    @Wire
    private Combobox cmbCategoria;

    @Wire
    private Groupbox grpPanel;

    @Wire
    private Caption gbTiptulo;

    @Wire
    private Groupbox grpLibreriaCategoria;

    @Wire
    private Window winReportes;

    @Wire
    private Datebox dbFechaDesde;

    @Wire
    private Datebox dbFechaHasta;

    @Wire
    private Listbox lstUsuarios;

    private Radio rd30 = new Radio("30 días");
    private Radio rd60 = new Radio("60 días");
    private Radio rd90 = new Radio("90 días");
    private Datebox dbDesde = new Datebox(new Date());
    private Datebox dbHasta = new Datebox(new Date());
    private Combobox comboMes = new Combobox();
    private Combobox comboAnio = new Combobox();
    private Combobox comboMesDesde = new Combobox();
    private Combobox comboAnioDesde = new Combobox();
    private Combobox comboMesHasta = new Combobox();
    private Combobox comboAnioHasta = new Combobox();
    private Combobox comboUsuario = new Combobox();

    private static final long serialVersionUID = -8796084149278088791L;
    private Traza traza = new Traza(Reportes.class);
    private Herramientas herramientas = new Herramientas();
    private Session session;
    private Expediente expediente;
    private String login;
    private List<Perfil> perfiles;
    private int reporte;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        try {

            if (session != null) {

                expediente = (Expediente) session.getAttribute("expediente");

                login = (String) session.getAttribute("login");
                reporte = expediente.getReporte();
                traza.trace("reporte seleccionado " + reporte, Level.INFO);

                if (reporte != ConstruyeConsultas.DETALLE_DOCUMENTO_INDEXADOS_POR_INDEXADOR) {
                    grpPanel.setVisible(false);
                    winReportes.setTitle(expediente.getNombreReporte());

                    llenarComboLibrerias();

                    armarPanel();
                    desactivarComponentes();
                }else{
                    winReportes.setTitle(expediente.getNombreReporte());
                    llenarComboListaUsuarios();
                }
            }

        } catch (Exception e) {
            traza.trace("problema general en los reportes", Level.ERROR, e);
            herramientas.error("Problemas general en los reportes", e);
        }

    }

    private void llenarComboLibrerias() {

        Comboitem item;

        try {
            cmbCategoria.setDisabled(true);

            if (session != null) {

                traza.trace("buscando las libreria del usuario " + login + " con el rol " + Constantes.ROL, Level.INFO);
                perfiles = new IniciaSesion().buscarLibCatPerfil(login, Constantes.ROL);

                traza.trace("llenando lista de librerias", Level.INFO);
                String lib = "";

                for (Perfil perfil : perfiles) {
                    String des = perfil.getLibreria().getDescripcion();

                    if (!lib.equalsIgnoreCase(des)) {
                        lib = perfil.getLibreria().getDescripcion();

                        if (perfil.getLibreria().getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {
                            traza.trace("libreria agregada " + lib, Level.INFO);
                            item = new Comboitem();
                            item.setValue(perfil.getLibreria().getIdLibreria());
                            item.setLabel(lib);
                            item.setParent(cmbLibreria);
                        }
                    }
                }

            } else {
                herramientas.warn("Problemas con el objecto Sesion");
                herramientas.navegarPagina("index.zul");
            }
        } catch (SOAPException | SOAPFaultException e) {
            herramientas.error("Error al llenar la lista de librerias ", e);
            traza.trace("error llenando el combo libreria", Level.ERROR, e);
        }
    }

    @Listen("onSelect = #cmbLibreria")
    public void llenarComboCategoria() {
        int lib;
        String cat = "";
        Comboitem item;

        traza.trace("libreria selecionada para llenar combo categoria " + cmbLibreria.getValue(), Level.INFO);

        try {

            try {

                cmbCategoria.getItems().clear();
            } catch (NullPointerException e) {
            }

            if (session != null) {

                lib = cmbLibreria.getSelectedItem().getValue();

                traza.trace("llenando lista de categorias", Level.INFO);
                traza.trace("id libreria seleccionada " + lib, Level.INFO);

                for (Perfil cate : perfiles) {
                    String categ = cate.getCategoria().getCategoria();

                    if (cate.getLibreria().getIdLibreria() == lib) {
                        if (!cat.equalsIgnoreCase(categ)) {
                            cat = cate.getCategoria().getCategoria();

                            if (cate.getCategoria().getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {
                                traza.trace("categoria agregada " + cat, Level.INFO);
                                item = new Comboitem();
                                item.setValue(cate.getCategoria().getIdCategoria());
                                item.setLabel(cat);
                                item.setParent(cmbCategoria);
                            }
                        }
                    }
                }
                cmbCategoria.setDisabled(false);
            } else {
                herramientas.warn("Problemas con el objecto Sesion");
                herramientas.navegarPagina("index.zul");
            }

        } catch (Exception e) {
            herramientas.error("Problemas al llenar la lista de categorias", e);
            traza.trace("error al llenar lista de categoria", Level.ERROR, e);
        }
    }

    private void armarPanel() {

        Grid grid = new Grid();
        Columns columns = new Columns();
        Rows rows = new Rows();
        Row row;
        Label label;

        if (reporte == ConstruyeConsultas.DOCUMENTO_A_VENCERSE) {

            Radiogroup rdgDocVencerse = new Radiogroup();
            Vbox vbox = new Vbox();
            gbTiptulo.setLabel("Seleccione el periodo");
            gbTiptulo.setParent(grpPanel);

            rd30.setRadiogroup(rdgDocVencerse);
            vbox.appendChild(rd30);

            rd60.setRadiogroup(rdgDocVencerse);
            vbox.appendChild(rd60);

            rd90.setRadiogroup(rdgDocVencerse);
            vbox.appendChild(rd90);

            rdgDocVencerse.appendChild(vbox);

            rdgDocVencerse.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_VENCIDOS) {

            gbTiptulo.setLabel("Seleccione un rango de fechas");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Fecha Desde: ");
            row.appendChild(label);

            dbDesde.setFormat("MM/dd/yyyy");
            row.appendChild(dbDesde);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Fecha Hasta: ");
            row.appendChild(label);

            dbHasta.setFormat("MM/dd/yyyy");
            row.appendChild(dbHasta);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTOS_RECHAZADOS) {

            gbTiptulo.setLabel("Seleccione un rango de fechas");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Fecha Desde: ");
            row.appendChild(label);

            dbDesde.setFormat("MM/dd/yyyy");
            row.appendChild(dbDesde);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Fecha Hasta: ");
            row.appendChild(label);

            dbHasta.setFormat("MM/dd/yyyy");
            row.appendChild(dbHasta);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_PENDIENTE_POR_APROBAR) {

        } else if (reporte == ConstruyeConsultas.CAUSA_DE_RECHAZO) {

            gbTiptulo.setLabel("Seleccione un rango de fechas");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Fecha Desde: ");
            row.appendChild(label);

            dbDesde.setFormat("MM/dd/yyyy");
            row.appendChild(dbDesde);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Fecha Hasta: ");
            row.appendChild(label);

            dbHasta.setFormat("MM/dd/yyyy");
            row.appendChild(dbHasta);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_INDEXADOS_RECHAZADO_APROBADO_PENDIENTE) {

            gbTiptulo.setLabel("Seleccione Mes y Año");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Mes: ");
            row.appendChild(label);
            comboMes = llenarComboMes();
            row.appendChild(comboMes);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Año: ");
            row.appendChild(label);
            comboAnio = llenarComboAnio();
            row.appendChild(comboAnio);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.CRECIMIENTO_INTERMENSUAL_DE_DOCUMENTOS) {

            grpLibreriaCategoria.setVisible(false);

            gbTiptulo.setLabel("Seleccione Mes y Año");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Mes desde: ");
            row.appendChild(label);
            comboMesDesde = llenarComboMes();
            row.appendChild(comboMesDesde);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Mes hasta: ");
            row.appendChild(label);
            comboMesHasta = llenarComboMes();
            row.appendChild(comboMesHasta);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Año desde: ");
            row.appendChild(label);
            comboAnioDesde = llenarComboAnio();
            row.appendChild(comboAnioDesde);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Año hasta: ");
            row.appendChild(label);
            comboAnioHasta = llenarComboAnio();
            row.appendChild(comboAnioHasta);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.CANTIDAD_DOCUMENTO_INDEXADOS_POR_INDEXADOR) {

            gbTiptulo.setLabel("Seleccione Mes y Año");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Mes: ");
            row.appendChild(label);
            comboMes = llenarComboMes();
            row.appendChild(comboMes);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Año: ");
            row.appendChild(label);
            comboAnio = llenarComboAnio();
            row.appendChild(comboAnio);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.EXPEDIENTE_ESTATUS) {

            grpLibreriaCategoria.setVisible(false);

            gbTiptulo.setLabel("Seleccione un rango de fechas");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Fecha Desde: ");
            row.appendChild(label);

            dbDesde.setFormat("MM/dd/yyyy");
            row.appendChild(dbDesde);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Fecha Hasta: ");
            row.appendChild(label);

            dbHasta.setFormat("MM/dd/yyyy");
            row.appendChild(dbHasta);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_ELIMINADO) {

            gbTiptulo.setLabel("Seleccione un rango de fechas");
            gbTiptulo.setParent(grpPanel);

            columns.appendChild(new Column());
            columns.appendChild(new Column());
            columns.setParent(grid);

            row = new Row();
            label = new Label("Fecha Desde: ");
            row.appendChild(label);

            dbDesde.setFormat("MM/dd/yyyy");
            row.appendChild(dbDesde);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Fecha Hasta: ");
            row.appendChild(label);

            dbHasta.setFormat("MM/dd/yyyy");
            row.appendChild(dbHasta);

            rows.appendChild(row);

            row = new Row();
            label = new Label("Usuario: ");
            row.appendChild(label);
            comboUsuario = llenarComboListaUsuarios();
            row.appendChild(comboUsuario);

            rows.appendChild(row);

            rows.setParent(grid);
            grid.setParent(grpPanel);
            grpPanel.setVisible(true);

        }
    }

    private Combobox llenarComboMes() {

        String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        Combobox combo = new Combobox();
        Comboitem item;

        for (String mes : meses) {
            item = new Comboitem();
            item.setValue(mes);
            item.setLabel(mes);
            item.setParent(combo);
        }

        return combo;
    }

    private Combobox llenarComboAnio() {

        Calendar calendar;// = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        Combobox combo = new Combobox();
        Comboitem item;

        for (int i = 0; i < 4; i++) {
            calendar = new GregorianCalendar();
            calendar.add(Calendar.YEAR, -i);

            item = new Comboitem();
            item.setValue(sdf.format(calendar.getTime()));
            item.setLabel(sdf.format(calendar.getTime()));
            item.setParent(combo);

            traza.trace("año " + calendar.get(Calendar.YEAR), Level.INFO);
        }

        return combo;
    }

    private Combobox llenarComboListaUsuarios() {

        Combobox combo = new Combobox();
        Comboitem cbmItem;
        Listitem lstItem;
        Listcell cell;
        List<Usuario> lstUser;

        try {

            lstUser = new IniciaSesion().autocomplete();

            if (reporte == ConstruyeConsultas.DETALLE_DOCUMENTO_INDEXADOS_POR_INDEXADOR) {

                for (Usuario user : lstUser) {
                    if (user.getIdEstatus() == 1) {
                        lstItem = new Listitem(user.getIdUsuario());
                        
                        lstItem.setParent(lstUsuarios);
                    }
                }

            } else if (reporte == ConstruyeConsultas.DOCUMENTO_ELIMINADO) {

                for (Usuario user : lstUser) {
                    if (user.getIdEstatus() == 1) {

                        cbmItem = new Comboitem();
                        cbmItem.setValue(user.getIdUsuario());
                        cbmItem.setLabel(user.getIdUsuario());
                        cbmItem.setParent(combo);
                    }
                }
            }
        } catch (SOAPException | SOAPFaultException | ConnectException ex) {
            traza.trace("problemas al buscar los usuarios", Level.ERROR, ex);
            herramientas.error("Problemas al llenar la lista desplegable de los usuarios", ex);
        }

        return combo;
    }

    @Listen("onSelect = #cmbCategoria")
    public void activarComponentes() {

        if (reporte == ConstruyeConsultas.DOCUMENTO_A_VENCERSE) {

            rd30.setDisabled(false);
            rd60.setDisabled(false);
            rd90.setDisabled(false);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_VENCIDOS
                || reporte == ConstruyeConsultas.DOCUMENTOS_RECHAZADOS
                || reporte == ConstruyeConsultas.CAUSA_DE_RECHAZO
                || reporte == ConstruyeConsultas.EXPEDIENTE_ESTATUS) {

            dbDesde.setDisabled(false);
            dbHasta.setDisabled(false);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_PENDIENTE_POR_APROBAR) {

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_INDEXADOS_RECHAZADO_APROBADO_PENDIENTE
                || reporte == ConstruyeConsultas.CANTIDAD_DOCUMENTO_INDEXADOS_POR_INDEXADOR) {

            comboMes.setDisabled(false);
            comboAnio.setDisabled(false);

        } else if (reporte == ConstruyeConsultas.CRECIMIENTO_INTERMENSUAL_DE_DOCUMENTOS) {

            comboMesDesde.setDisabled(false);
            comboMesHasta.setDisabled(false);
            comboAnioDesde.setDisabled(false);
            comboAnioHasta.setDisabled(false);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_ELIMINADO) {

            dbDesde.setDisabled(false);
            dbHasta.setDisabled(false);
            comboUsuario.setDisabled(false);
        }
    }

    private void desactivarComponentes() {

        if (reporte == ConstruyeConsultas.DOCUMENTO_A_VENCERSE) {

            rd30.setDisabled(true);
            rd60.setDisabled(true);
            rd90.setDisabled(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_VENCIDOS
                || reporte == ConstruyeConsultas.DOCUMENTOS_RECHAZADOS
                || reporte == ConstruyeConsultas.CAUSA_DE_RECHAZO) {

            dbDesde.setDisabled(true);
            dbHasta.setDisabled(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_PENDIENTE_POR_APROBAR) {

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_INDEXADOS_RECHAZADO_APROBADO_PENDIENTE
                || reporte == ConstruyeConsultas.CANTIDAD_DOCUMENTO_INDEXADOS_POR_INDEXADOR) {

            comboMes.setDisabled(true);
            comboAnio.setDisabled(true);

        } else if (reporte == ConstruyeConsultas.DOCUMENTO_ELIMINADO) {

            dbDesde.setDisabled(true);
            dbHasta.setDisabled(true);
            comboUsuario.setDisabled(true);
        }
    }

    @Listen("onClick = #btnAceptar")
    public void aceptar() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Expediente expedient = new Expediente();
        //ProcesaReporte pr = new ProcesaReporte();
        ConstruyeConsultas consultas = new ConstruyeConsultas();
        String lib, cat, query = null, fechas, strFechaHasta;
        int idLib, idCat;
        Set<Listitem> itemSel;
        List<Listitem> listitems = new ArrayList<>();
        List<String> usuariosSelecionados = new ArrayList<>();

        try {

            try {
                idLib = cmbLibreria.getSelectedItem().getValue();
                lib = cmbLibreria.getSelectedItem().getLabel();
                idCat = cmbCategoria.getSelectedItem().getValue();
                cat = cmbCategoria.getSelectedItem().getLabel();
            } catch (NullPointerException e) {
                idLib = 0;
                idCat = 0;
                lib = "";
                cat = "";
            }

            if (reporte == ConstruyeConsultas.CRECIMIENTO_INTERMENSUAL_DE_DOCUMENTOS) {

                String mesDesde, mesHasta, anioDesde, anioHasta;
                int mDesde, mHasta, aDesde, aHasta;
                HashMap mesAnio = new HashMap();

                mesDesde = comboMesDesde.getSelectedItem().getValue();
                mesHasta = comboMesHasta.getSelectedItem().getValue();
                anioDesde = comboAnioDesde.getSelectedItem().getValue();
                anioHasta = comboAnioHasta.getSelectedItem().getValue();

                if ((mesDesde != null) && (mesHasta != null) && ((!mesDesde.equalsIgnoreCase("")) && (!mesHasta.equalsIgnoreCase("")))
                        && (anioDesde != null) && (anioHasta != null) && (!anioDesde.equalsIgnoreCase("") && !anioHasta.equalsIgnoreCase(""))) {

                    mDesde = Integer.parseInt(consultas.getMapMeses().get(mesDesde));
                    mHasta = Integer.parseInt(consultas.getMapMeses().get(mesHasta));
                    aDesde = Integer.parseInt(comboAnioDesde.getSelectedItem().getValue().toString());
                    aHasta = Integer.parseInt(comboAnioHasta.getSelectedItem().getValue().toString());

                    if (mDesde == 12 && mHasta >= 1) {
                        if (aHasta == (aDesde + 1)) {

                            traza.trace("mes desde " + mesDesde, Level.INFO);
                            traza.trace("mes hasta " + mesHasta, Level.INFO);
                            traza.trace("año desde " + anioDesde, Level.INFO);
                            traza.trace("año hasta " + anioHasta, Level.INFO);

                            mesAnio.put("mesAnioDesde", anioDesde + consultas.getMapMeses().get(mesDesde));
                            mesAnio.put("mesAnioHasta", anioHasta + consultas.getMapMeses().get(mesHasta));
                            mesAnio.put("mesAnioDesdeTitulo", consultas.getMapMeses().get(mesDesde) + " / " + anioDesde);
                            mesAnio.put("mesAnioHastaTitulo", consultas.getMapMeses().get(mesHasta) + " / " + anioHasta);

                            reportesPlantilla("crecimientoIntermensual.jrxml", mesAnio, expediente.getNombreReporte());

                        }
                    }

                    if (mDesde <= mHasta) {
                        if (aDesde <= aHasta) {

                            traza.trace("mes desde " + mesDesde, Level.INFO);
                            traza.trace("mes hasta " + mesHasta, Level.INFO);
                            traza.trace("año desde " + anioDesde, Level.INFO);
                            traza.trace("año hasta " + anioHasta, Level.INFO);

                            mesAnio.put("mesAnioDesde", anioDesde + consultas.getMapMeses().get(mesDesde));
                            mesAnio.put("mesAnioHasta", anioHasta + consultas.getMapMeses().get(mesHasta));
                            mesAnio.put("mesAnioDesdeTitulo", consultas.getMapMeses().get(mesDesde) + " / " + anioDesde);
                            mesAnio.put("mesAnioHastaTitulo", consultas.getMapMeses().get(mesHasta) + " / " + anioHasta);

                            reportesPlantilla("crecimientoIntermensual.jrxml", mesAnio, expediente.getNombreReporte());

                        } else {
                            throw new DW4JDesktopExcepcion("El año desde debe ser menor que el año hasta");
                        }
                    } else {
                        throw new DW4JDesktopExcepcion("El mes desde debe ser menor que el mes hasta");
                    }
                } else {
                    throw new DW4JDesktopExcepcion("Debe selecionar el Mes y el Año");
                }

            } else if (reporte == ConstruyeConsultas.EXPEDIENTE_ESTATUS) {

                HashMap map = new HashMap();
                Date desde = dbDesde.getValue();
                Date hasta = dbHasta.getValue();

                if ((desde != null) && (hasta != null)) {

                    Calendar fechaDesde = new GregorianCalendar();
                    fechaDesde.setTime(desde);

                    Calendar fechaHasta = new GregorianCalendar();
                    fechaHasta.setTime(hasta);
                    fechas = "Fecha desde: " + sdf.format(fechaDesde.getTime()) + " - Fecha hasta: " + sdf.format(fechaHasta.getTime());

                    if (fechaHasta.after(fechaDesde)) {

                        map.put("fechaDesde", fechaDesde.getTime());
                        map.put("fechaHasta", fechaHasta.getTime());

                        traza.trace("rango de fechas " + fechas, Level.INFO);
                        reportesPlantilla("reporteEstatusExpediente.jrxml", map, expediente.getNombreReporte());

                    } else {
                        throw new DW4JDesktopExcepcion("La fecha hasta debe ser menor que la fehca desde");
                    }
                } else {
                    throw new DW4JDesktopExcepcion("Debe selecionar una fecha desde y una fecha hasta");
                }

            } else if (reporte == ConstruyeConsultas.DETALLE_DOCUMENTO_INDEXADOS_POR_INDEXADOR) {

                itemSel = lstUsuarios.getSelectedItems();
                listitems.addAll(itemSel);
                Date desde = dbFechaDesde.getValue();
                Date hasta = dbFechaHasta.getValue();

                if (!listitems.isEmpty()) {
                    if ((desde != null) && (hasta != null)) {

                        Calendar fechaDesde = new GregorianCalendar();
                        fechaDesde.setTime(desde);

                        Calendar fechaHasta = new GregorianCalendar();
                        fechaHasta.setTime(hasta);
                        fechas = "Fecha desde: " + sdf.format(fechaDesde.getTime()) + " - Fecha hasta: " + sdf.format(fechaHasta.getTime());
                        traza.trace("rango de fechas " + fechas, Level.INFO);

                        if (fechaHasta.after(fechaDesde)) {

                            for (Listitem item : listitems) {
                                usuariosSelecionados.add(item.getLabel());
                            }

                            reporteDetalleIndexador(usuariosSelecionados, fechaDesde, fechaHasta);

                        } else {
                            throw new DW4JDesktopExcepcion("La fecha hasta debe ser menor que la fehca desde");
                        }

                    } else {
                        throw new DW4JDesktopExcepcion("Debe Selecionar un rango de fechas");
                    }
                } else {
                    throw new DW4JDesktopExcepcion("Debe Selecionar uno o varios usuarios");
                }

            } else if ((lib != null) && (!lib.equalsIgnoreCase(""))) {
                if ((cat != null) && (!cat.equalsIgnoreCase(""))) {

                    expedient.setIdLibreria(idLib);
                    expedient.setLibreria(lib);
                    expedient.setIdCategoria(idCat);
                    expedient.setCategoria(cat);

                    if (reporte == ConstruyeConsultas.DOCUMENTO_A_VENCERSE) {

                        String dias = null;

                        if (rd30.isSelected()) {

                            traza.trace("seleciono 30 dias", Level.INFO);
                            dias = "30";
                            query = consultas.crearQuery(ConstruyeConsultas.DOCUMENTO_A_VENCERSE, dias, null, null, expedient);

                        } else if (rd60.isSelected()) {

                            traza.trace("seleciono 60 dias", Level.INFO);
                            dias = "60";
                            query = consultas.crearQuery(ConstruyeConsultas.DOCUMENTO_A_VENCERSE, dias, null, null, expedient);

                        } else if (rd90.isSelected()) {

                            traza.trace("seleciono 90 dias", Level.INFO);
                            dias = "90";
                            query = consultas.crearQuery(ConstruyeConsultas.DOCUMENTO_A_VENCERSE, dias, null, null, expedient);

                        } else {
                            herramientas.warn("Debe seleccionar un periodo");
                        }

                        if (query != null && dias != null) {
                            reportesDinamico("Documentos a Vencerse", query, lib, cat, consultas.getListaCampos(), "Período de " + dias + " días");
                        }

                    } else if (reporte == ConstruyeConsultas.DOCUMENTO_VENCIDOS) {

                        Date desde = dbDesde.getValue();
                        Date hasta = dbHasta.getValue();

                        if ((desde != null) && (hasta != null)) {

                            Calendar fechaDesde = new GregorianCalendar();
                            fechaDesde.setTime(desde);

                            Calendar fechaHasta = new GregorianCalendar();
                            fechaHasta.setTime(hasta);
                            fechas = "Fecha desde: " + sdf.format(fechaDesde.getTime()) + " - Fecha hasta: " + sdf.format(fechaHasta.getTime());

                            if (fechaHasta.after(fechaDesde)) {

                                traza.trace("rango de fechas " + fechas, Level.INFO);

                                query = consultas.crearQuery(ConstruyeConsultas.DOCUMENTO_VENCIDOS, null, fechaDesde, fechaHasta, expedient);

                                reportesDinamico("Documentos Vencidos", query, lib, cat, consultas.getListaCampos(), fechas);

                            } else {
                                throw new DW4JDesktopExcepcion("La fecha hasta debe ser menor que la fehca desde");
                            }
                        } else {
                            throw new DW4JDesktopExcepcion("Debe selecionar una fecha desde y una fecha hasta");
                        }

                    } else if (reporte == ConstruyeConsultas.DOCUMENTOS_RECHAZADOS) {

                        Date desde = dbDesde.getValue();
                        Date hasta = dbHasta.getValue();

                        if ((desde != null) && (hasta != null)) {

                            Calendar fechaDesde = new GregorianCalendar();
                            fechaDesde.setTime(desde);

                            Calendar fechaHasta = new GregorianCalendar();
                            fechaHasta.setTime(hasta);
                            fechas = "Fecha desde: " + sdf.format(fechaDesde.getTime()) + " - Fecha hasta: " + sdf.format(fechaHasta.getTime());

                            if (fechaHasta.after(fechaDesde)) {

                                traza.trace("rango de fechas " + fechas, Level.INFO);

                                query = consultas.crearQuery(ConstruyeConsultas.DOCUMENTOS_RECHAZADOS, null, fechaDesde, fechaHasta, expedient);

                                reportesDinamico("Documentos Vencidos", query, lib, cat, consultas.getListaCampos(), fechas);

                            } else {
                                throw new DW4JDesktopExcepcion("La fecha hasta debe ser menor que la fehca desde");
                            }
                        } else {
                            throw new DW4JDesktopExcepcion("Debe selecionar una fecha desde y una fecha hasta");
                        }

                    } else if (reporte == ConstruyeConsultas.DOCUMENTO_PENDIENTE_POR_APROBAR) {

                        traza.trace("generando el reporte " + expediente.getNombreReporte(), Level.INFO);

                        query = consultas.crearQuery(ConstruyeConsultas.DOCUMENTO_PENDIENTE_POR_APROBAR, null, null, null, expedient);

                        reportesDinamico("Documentos Pendientes por Aprobar", query, lib, cat, consultas.getListaCampos(), "");

                    } else if (reporte == ConstruyeConsultas.CAUSA_DE_RECHAZO) {

                        HashMap param = new HashMap();
                        Date desde = dbDesde.getValue();
                        Date hasta = dbHasta.getValue();

                        if ((desde != null) && (hasta != null)) {

                            Calendar fechaDesde = new GregorianCalendar();
                            fechaDesde.setTime(desde);

                            Calendar fechaHasta = new GregorianCalendar();
                            fechaHasta.setTime(hasta);
                            fechas = "Fecha desde: " + sdf.format(fechaDesde.getTime()) + " - Fecha hasta: " + sdf.format(fechaHasta.getTime());

                            if (fechaHasta.after(fechaDesde)) {

                                traza.trace("rango de fechas " + fechas, Level.INFO);

                                param.put("idLib", expedient.getIdLibreria());
                                param.put("idCat", expedient.getIdCategoria());
                                param.put("fechaDesde", fechaDesde.getTime());
                                param.put("fechaHasta", fechaHasta.getTime());

                                reportesPlantilla("causaRechazo.jrxml", param, expediente.getNombreReporte());

                            } else {
                                throw new DW4JDesktopExcepcion("La fecha hasta debe ser menor que la fehca desde");
                            }
                        } else {
                            throw new DW4JDesktopExcepcion("Debe selecionar una fecha desde y una fecha hasta");
                        }

                    } else if (reporte == ConstruyeConsultas.DOCUMENTO_INDEXADOS_RECHAZADO_APROBADO_PENDIENTE) {

                        String mes, anio;
                        HashMap mesAnio = new HashMap();

                        mes = comboMes.getSelectedItem().getValue();
                        anio = comboAnio.getSelectedItem().getValue();

                        if ((mes != null) && (anio != null) && ((!mes.equalsIgnoreCase("")) && (!anio.equalsIgnoreCase("")))) {

                            mesAnio.put("idLib", expedient.getIdLibreria());
                            mesAnio.put("idCat", expedient.getIdCategoria());
                            mesAnio.put("mesAnio", anio + consultas.getMapMeses().get(mes));
                            mesAnio.put("mesAnioTitulo", consultas.getMapMeses().get(mes) + " / " + anio);

                            traza.trace("mes " + mes, Level.INFO);
                            traza.trace("año " + anio, Level.INFO);

                            reportesPlantilla("docIndexRechAprobPend.jrxml", mesAnio, expediente.getNombreReporte());

                        } else {
                            throw new DW4JDesktopExcepcion("Debe selecionar el Mes y el Año");
                        }

                    } else if (reporte == ConstruyeConsultas.CANTIDAD_DOCUMENTO_INDEXADOS_POR_INDEXADOR) {

                        String mes, anio;
                        HashMap mesAnio = new HashMap();

                        mes = comboMes.getSelectedItem().getValue();
                        anio = comboAnio.getSelectedItem().getValue();

                        if ((mes != null) && (anio != null) && ((!mes.equalsIgnoreCase("")) && (!anio.equalsIgnoreCase("")))) {

                            mesAnio.put("idLib", expedient.getIdLibreria());
                            mesAnio.put("idCat", expedient.getIdCategoria());
                            mesAnio.put("mesAnio", anio + consultas.getMapMeses().get(mes));
                            mesAnio.put("mesAnioTitulo", consultas.getMapMeses().get(mes) + " / " + anio);

                            traza.trace("mes " + mes, Level.INFO);
                            traza.trace("año " + anio, Level.INFO);

                            reportesPlantilla("CantDocIndexados.jrxml", mesAnio, expediente.getNombreReporte());

                        } else {
                            throw new DW4JDesktopExcepcion("Debe selecionar el Mes y el Año");
                        }

                    } else if (reporte == ConstruyeConsultas.DOCUMENTO_ELIMINADO) {

                        HashMap map = new HashMap();
                        Date desde = dbDesde.getValue();
                        Date hasta = dbHasta.getValue();
                        String usuario = comboUsuario.getSelectedItem().getValue();

                        if ((desde != null) && (hasta != null)) {

                            Calendar fechaDesde = new GregorianCalendar();
                            fechaDesde.setTime(desde);

                            Calendar fechaHasta = new GregorianCalendar();
                            fechaHasta.setTime(hasta);
                            fechas = "Fecha desde: " + sdf.format(fechaDesde.getTime()) + " - Fecha hasta: " + sdf.format(fechaHasta.getTime());

                            if (fechaHasta.after(fechaDesde)) {
                                if (!usuario.equalsIgnoreCase("")) {

                                    map.put("idLib", expedient.getIdLibreria());
                                    map.put("idCat", expedient.getIdCategoria());
                                    map.put("usuario", usuario);
                                    map.put("fechaDesde", fechaDesde.getTime());
                                    map.put("fechaHasta", fechaHasta.getTime());

                                    traza.trace("rango de fechas " + fechas, Level.INFO);
                                    reportesPlantilla("documentosEliminados.jrxml", map, expediente.getNombreReporte());

                                } else {
                                    throw new DW4JDesktopExcepcion("Debe selecionar el tipo de personal");
                                }
                            } else {
                                throw new DW4JDesktopExcepcion("La fecha hasta debe ser menor que la fehca desde");
                            }
                        } else {
                            throw new DW4JDesktopExcepcion("Debe selecionar una fecha desde y una fecha hasta");
                        }

                    }

                } else {
                    throw new DW4JDesktopExcepcion("Debe seleccionar una categoria");
                }
            } else {
                throw new DW4JDesktopExcepcion("Debe seleccionar una libreria");
            }

        } catch (DW4JDesktopExcepcion e) {
            traza.trace("", Level.WARN, e);
            herramientas.warn(e.getMessage());
        } catch (Exception e) {
            traza.trace("error general al procesar el reporte", Level.ERROR, e);
            herramientas.error("Error general al procesar el reporter", e);
        }

    }

    private void reportesDinamico(String titulo, String query, String lib, String cat, List<Campos> campos, String fechas) {

        ProcesaReporte pr = new ProcesaReporte();

        pr.crearReporte(titulo, query, lib, cat, campos, fechas);
    }

    private void reportesPlantilla(final String plantilla, final HashMap map, final String titulo) {

        ProcesaReporte pr = new ProcesaReporte();

        pr.crearReporte(plantilla, map, titulo);
    }

    private void reporteDetalleIndexador(List<String> usuariosSelecionados, Calendar fechaDesd, Calendar fechaHast) {

        ProcesaReporte pr = new ProcesaReporte();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        HashMap param = new HashMap();
        String users = "";
        int sel, cont = 0;

        try {

            Mensajes.setMensaje("");

            sel = usuariosSelecionados.size();
            traza.trace("tamaño de la lista de usuarios seleccionado " + sel, Level.INFO);

            for (String obj : usuariosSelecionados) {

                if (cont == (sel - 1)) {
                    users = users + "'" + obj + "'";
                } else {
                    users = users + "'" + obj + "',";
                }
                traza.trace("contador " + cont, Level.INFO);
                cont++;
            }

            param.put("usuarios", users);
            param.put("fechaDesde", fechaDesd.getTime());
            param.put("fechaHasta", fechaHast.getTime());

            traza.trace("usuarios seleccionados " + users, Level.INFO);
            traza.trace("fecha desde " + sdf.format(fechaDesd.getTime()), Level.INFO);
            traza.trace("fecha hasta " + sdf.format(fechaHast.getTime()), Level.INFO);

            pr.crearReporte("detalleIndexador.jrxml", param, expediente.getNombreReporte());

        } catch (Exception e) {
            traza.trace("error ene proceso del reporte", Level.ERROR, e);
        }

    }
}
