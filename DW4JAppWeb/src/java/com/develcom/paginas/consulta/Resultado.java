/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.consulta;

import com.develcom.administracion.Combo;
import com.develcom.expediente.ConsultaDinamica;
import com.develcom.expediente.Indice;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import com.develcom.dao.DatosTabla;
import com.develcom.dao.Expediente;
import com.develcom.expediente.Expedientes;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.excepcion.DW4JException;
import com.develcom.tools.trazas.Traza;
import java.text.ParseException;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import ve.com.develcom.administracion.ActualizaIndice;
import ve.com.develcom.expediente.BuscaIndice;
import ve.com.develcom.expediente.LLenarListaDesplegable;

/**
 *
 * @author develcom
 */
public class Resultado extends SelectorComposer<Component> {

    @Wire
    private Listbox lstResultado;

    @Wire
    private Groupbox grpIndices;

    @Wire
    private Grid panelIndice;

    @Wire
    private Button btnGuarda;

    @Wire
    private Window winResultado;

    private Object indicePrimarioActual;
    private List<Indice> lstIndices = new ArrayList<>();
    private List<ConsultaDinamica> consultaDinamicas;
    private String idExpediente;
    private List<DatosTabla> datosTablas = new ArrayList<>();
    private static final long serialVersionUID = 4679389834585865080L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(Resultado.class);
    private Session session;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    public void iniciar() {

        try {

            session = herramientas.crearSesion();

            if (session != null) {

                grpIndices.setVisible(false);
                btnGuarda.setVisible(false);

                expediente = (Expediente) session.getAttribute("expediente");

                if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {
                    List<com.develcom.calidad.ConsultaDinamica> dinamicas = (List<com.develcom.calidad.ConsultaDinamica>) session.getAttribute("consultaDinamicas");
                    convertir(dinamicas);
                } else {
                    consultaDinamicas = (List<ConsultaDinamica>) session.getAttribute("consultaDinamicas");
                }

                if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {
                    winResultado.setTitle("Calidad");
                } else {
                    winResultado.setTitle("Indices");
                }

                llenarDatosTabla();
                construirTabla();

            }

        } catch (Exception e) {
            traza.trace("error al iniciar el objeto consulta", Level.INFO, e);
            herramientas.error("Problemas en el resultado de la consulta", e);
        }

    }

    private void convertir(List<com.develcom.calidad.ConsultaDinamica> dinamicas) {

        ConsultaDinamica conDinamica;
        Indice indice;
        List<com.develcom.calidad.Indice> indices;
        boolean finder = false;

        if (dinamicas != null) {

            if (idExpediente != null) {
                for (com.develcom.calidad.ConsultaDinamica cd : dinamicas) {

                    indices = cd.getIndices();
                    for (int i = 0; i < indices.size(); i++) {
                        com.develcom.calidad.Indice ind = indices.get(i);
                        try {
                            if (idExpediente.equalsIgnoreCase(ind.getValor().toString())) {
                                traza.trace("removiendo el expediente " + ind.getValor().toString() + " del listado", Level.INFO);

                                indices.remove(i);
                                indices.remove(i);
                                indices.remove(i);
                                indices.remove(i);

                                dinamicas.remove(cd);
                                com.develcom.calidad.ConsultaDinamica dinamica = new com.develcom.calidad.ConsultaDinamica();
                                dinamica.setExiste(true);

                                for (com.develcom.calidad.Indice in : indices) {
                                    dinamica.getIndices().add(in);
                                }
                                dinamicas.add(dinamica);
                                finder = true;
                                break;
                            }
                        } catch (Exception e) {
                        }
                    }
                    if (finder) {
                        break;
                    }
                }
            }

            if (consultaDinamicas != null) {
                if (!consultaDinamicas.isEmpty()) {
                    consultaDinamicas.clear();
                }
            } else {
                consultaDinamicas = new ArrayList<>();
            }

            for (com.develcom.calidad.ConsultaDinamica cd : dinamicas) {

                conDinamica = new ConsultaDinamica();
                indices = cd.getIndices();

                for (com.develcom.calidad.Indice ind : indices) {
                    indice = new Indice();

                    indice.setIndice(ind.getIndice());
                    indice.setClave(ind.getClave());
                    indice.setTipo(ind.getTipo());
                    indice.setValor(ind.getValor());
                    conDinamica.getIndices().add(indice);
                }
                consultaDinamicas.add(conDinamica);
            }
        }

    }

    private void llenarDatosTabla() {

        traza.trace("armando el data table", Level.INFO);
        DatosTabla dt = null;
        boolean sec1 = true, sec2 = true, sec3 = true, ban = true;
        int cont = 0;
        List<Indice> listaIndice;

        try {

            if (session != null) {

                for (ConsultaDinamica cd : consultaDinamicas) {
                    listaIndice = cd.getIndices();

                    for (Indice ind : listaIndice) {

                        try {

                            if (ban) {
                                dt = new DatosTabla();
                                sec1 = true;
                                sec2 = true;
                                sec3 = true;
                            }

                            if (ind.getClave().equalsIgnoreCase("y")) {

                                if (ind.getTipo().equalsIgnoreCase("FECHA")) {

//                                    XMLGregorianCalendar fechaIngreso = (XMLGregorianCalendar) ind.getValor();
//                                    Calendar c = fechaIngreso.toGregorianCalendar();
                                    dt.setClavePrimaria(ind.getIndice());
//                                    dt.setDatoPrimario(sdf.format(c.getTime()));
                                    dt.setDatoPrimario(ind.getValor().toString());

                                } else {

                                    dt.setClavePrimaria(ind.getIndice());
                                    dt.setDatoPrimario(ind.getValor().toString());
                                }

                                ban = false;
                                cont++;
                                traza.trace(dt.getClavePrimaria() + " - " + dt.getDatoPrimario() + " - " + ind.getClave(), Level.INFO);

                            } else if (ind.getClave().equalsIgnoreCase("s")) {

                                if (sec1) {

                                    if (ind.getTipo().equalsIgnoreCase("FECHA")) {

//                                        XMLGregorianCalendar fechaIngreso = (XMLGregorianCalendar) ind.getValor();
//                                        Calendar c = fechaIngreso.toGregorianCalendar();
                                        dt.setClaveSec1(ind.getIndice());
//                                    dt.setDatoPrimario(sdf.format(c.getTime()));
                                        dt.setDatoSec1(ind.getValor().toString());

                                    } else {

                                        dt.setClaveSec1(ind.getIndice());
                                        dt.setDatoSec1(ind.getValor().toString());
                                    }

                                    sec1 = false;
                                    cont++;
                                    traza.trace(dt.getClaveSec1() + " - " + dt.getDatoSec1() + " - " + ind.getClave(), Level.INFO);

                                } else if (sec2) {

                                    if (ind.getTipo().equalsIgnoreCase("FECHA")) {

//                                        XMLGregorianCalendar fechaIngreso = (XMLGregorianCalendar) ind.getValor();
//                                        Calendar c = fechaIngreso.toGregorianCalendar();
                                        dt.setClaveSec2(ind.getIndice());
//                                    dt.setDatoPrimario(sdf.format(c.getTime()));
                                        dt.setDatoSec2(ind.getValor().toString());

                                    } else {

                                        dt.setClaveSec2(ind.getIndice());
                                        dt.setDatoSec2(ind.getValor().toString());
                                    }

                                    sec2 = false;
                                    cont++;
                                    traza.trace(dt.getClaveSec2() + " - " + dt.getDatoSec2() + " - " + ind.getClave(), Level.INFO);

                                } else if (sec3) {

                                    if (ind.getTipo().equalsIgnoreCase("FECHA")) {

//                                        XMLGregorianCalendar fechaIngreso = (XMLGregorianCalendar) ind.getValor();
//                                        Calendar c = fechaIngreso.toGregorianCalendar();
                                        dt.setClaveSec3(ind.getIndice());
//                                    dt.setDatoPrimario(sdf.format(c.getTime()));
                                        dt.setDatoSec3(ind.getValor().toString());

                                    } else {

                                        dt.setClaveSec3(ind.getIndice());
                                        dt.setDatoSec3(ind.getValor().toString());
                                    }

                                    sec3 = false;
                                    cont++;
                                    traza.trace(dt.getClaveSec3() + " - " + dt.getDatoSec3() + " - " + ind.getClave(), Level.INFO);
                                }
                            }

                            if (cont == 4) {
                                datosTablas.add(dt);
                                ban = true;
                                cont = 0;
                            }
                        } catch (NullPointerException e) {
                        }
                    }
                }
            } else {
                herramientas.navegarPagina("index.zul");
            }
        } catch (Exception e) {
            traza.trace("error generando el resultado de la consulta", Level.ERROR, e);
            herramientas.error("Error generando el resultado de la consulta", e);
        }
    }

    private void construirTabla() {

        Listhead cabezera;
        Listitem datos;
        Listheader titulo;
        boolean flagCabezera = false;

        try {

            for (DatosTabla data : datosTablas) {

                if (!flagCabezera) {

                    cabezera = new Listhead();
                    cabezera.setValue(data.getClavePrimaria());
                    titulo = new Listheader(data.getClavePrimaria());
                    titulo.setAlign("center");
                    cabezera.appendChild(titulo);

                    titulo = new Listheader(data.getClaveSec1());
                    titulo.setAlign("center");
                    cabezera.appendChild(titulo);

                    titulo = new Listheader(data.getClaveSec2());
                    titulo.setAlign("center");
                    cabezera.appendChild(titulo);

                    titulo = new Listheader(data.getClaveSec3());
                    titulo.setAlign("center");
                    cabezera.appendChild(titulo);

                    cabezera.setParent(lstResultado);

                    flagCabezera = true;

                }

                datos = new Listitem();
                datos.setValue(data.getDatoPrimario());
                datos.appendChild(new Listcell(data.getDatoPrimario()));
                datos.appendChild(new Listcell(data.getDatoSec1()));
                datos.appendChild(new Listcell(data.getDatoSec2()));
                datos.appendChild(new Listcell(data.getDatoSec3()));
                datos.setParent(lstResultado);

            }
        } catch (Exception e) {
            traza.trace("Problemas generando la tabla de resultados", Level.ERROR, e);
            herramientas.error("Problemas generando la tabla de resultados", e);
        }
    }

    @Listen("onSelect = #lstResultado")
    public void seleccionar() {

        Listitem seleccion;

        seleccion = lstResultado.getSelectedItem();
        idExpediente = seleccion.getValue();

        traza.trace("selecciono el expediente " + idExpediente, Level.INFO);

        expediente.setIdExpediente(idExpediente);
        session.setAttribute("expediente", expediente);

        if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {
            herramientas.navegarPagina("calidadDocumento.zul");
        } else if (Constantes.ACCION.equalsIgnoreCase("CONSULTAR")) {
            herramientas.navegarPagina("arbol.zul");
        } else if (Constantes.ACCION.equalsIgnoreCase("ACTUALIZAR_INDICES")) {
            grpIndices.setVisible(true);
            btnGuarda.setVisible(true);
            mostrarIndces();
        }
    }

    private void mostrarIndces() {

        Rows rows = null;
        List<Row> childrenRow;
        Row row = null;
        Label label;
        Textbox textbox;
        Combobox combobox;
        int cont = 2;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        try {

            if (!lstIndices.isEmpty()) {
                lstIndices.clear();
            }

            lstIndices = new BuscaIndice().buscaDatosIndice(expediente.getIdExpediente(), expediente.getIdLibreria(), expediente.getIdCategoria());

            Rows rowsUsed = panelIndice.getRows();
            rows = panelIndice.getRows();

            if (!rows.getChildren().isEmpty()) {
                rows.getChildren().clear();
            }

            for (Indice ind : lstIndices) {

                if (cont == 2) {
                    cont = 0;
                    row = new Row();
                }

                if (ind.getTipo().equalsIgnoreCase("NUMERO")) {

                    label = new Label(ind.getIndice());

                    textbox = new Textbox();
                    textbox.setId(ind.getIndice().replace(" ", "_"));
                    textbox.setValue(ind.getValor().toString());

                    row.appendChild(label);
                    row.appendChild(textbox);

                    rows.appendChild(row);

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("TEXTO")) {

                    label = new Label(ind.getIndice());

                    textbox = new Textbox();
                    textbox.setId(ind.getIndice().replace(" ", "_"));
                    textbox.setValue(ind.getValor().toString());

                    row.appendChild(label);
                    row.appendChild(textbox);

                    rows.appendChild(row);

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("AREA")) {

                    label = new Label(ind.getIndice());

                    textbox = new Textbox();
                    textbox.setId(ind.getIndice().replace(" ", "_"));
                    textbox.setRows(6);
                    textbox.setValue(ind.getValor().toString());

                    row.appendChild(label);
                    row.appendChild(textbox);

                    rows.appendChild(row);

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("COMBO")) {
                    Comboitem item;

                    label = new Label(ind.getIndice());

                    combobox = new Combobox();
                    combobox.setId(ind.getIndice().replace(" ", "_"));

                    List<Combo> datosCombo = new LLenarListaDesplegable().buscarData(ind.getCodigo(), false);

                    for (Combo combo : datosCombo) {
                        item = new Comboitem();
                        item.setValue(combo.getIdCombo());
                        item.setLabel(combo.getDatoCombo());
                        item.setParent(combobox);
                    }

                    row.appendChild(label);
                    row.appendChild(combobox);

                    rows.appendChild(row);
                    combobox.setValue(ind.getValor().toString());

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("FECHA")) {

                    Label labelDesde = new Label(ind.getIndice().replace(" ", "_"));
                    Datebox datebox = new Datebox();
                    datebox.setId(ind.getIndice().replace(" ", "_"));
                    datebox.setFormat("dd/MM/yyyy");
                    datebox.setValue(sdf.parse(ind.getValor().toString()));

                    row.appendChild(labelDesde);
                    row.appendChild(datebox);

                    rows.appendChild(row);

                    cont++;
                }

            }

            buscarValorIndicePrimario();

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al buscar los indices", Level.ERROR, ex);
        } catch (ParseException ex) {
            traza.trace("problemas convirtiendo la fecha", Level.ERROR, ex);
        }

    }

    private void buscarValorIndicePrimario() {

        for (Indice indice : lstIndices) {
            if (indice.getClave().equalsIgnoreCase("y")) {

                indicePrimarioActual = indice.getValor();
            }
        }
    }

    @Listen("onClick = #btnGuarda")
    public void guardarIndices() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Expedientes expeWS = new Expedientes();
        int idLibreria, idCategoria, resp;
        boolean exito = false;
        Object indicePrimarioNuevo = null;

        try {

            if (session != null) {

                idCategoria = expediente.getIdCategoria();
                idLibreria = expediente.getIdLibreria();

                for (int i = 0; i < lstIndices.size(); i++) {

                    Indice indice = lstIndices.get(i);

                    if (panelIndice.getFellowIfAny(indice.getIndice().replace(" ", "_")) instanceof Textbox) {

                        Textbox textbox = (Textbox) panelIndice.getFellowIfAny(indice.getIndice().replace(" ", "_"));

                        if ((indice.getClave().equalsIgnoreCase("y")) || (indice.getClave().equalsIgnoreCase("s")) || (indice.getClave().equalsIgnoreCase("o"))) {

                            if (textbox.getValue() != null || !textbox.getValue().equalsIgnoreCase("")) {

                                if (indice.getClave().equalsIgnoreCase("y")) {
                                    indicePrimarioNuevo = textbox.getValue();
                                }

                                indice.setValor(textbox.getValue());

                            } else {
                                String arg = indice.getIndice().replace("_", " ");
                                arg = arg.toLowerCase();
                                char[] cs = arg.toCharArray();
                                char ch = cs[0];
                                cs[0] = Character.toUpperCase(ch);
                                arg = String.valueOf(cs);

                                throw new DW4JException("El " + arg + " no debe estar vacio");
                            }
                        } else {
                            indice.setValor(textbox.getValue());
                        }

                        traza.trace("Indice" + indice.getIndice() + " valor " + textbox.getValue(), Level.INFO);

                    } else if (panelIndice.getFellowIfAny(indice.getIndice().replace(" ", "_")) instanceof Combobox) {

                        Combobox combobox = (Combobox) panelIndice.getFellowIfAny(indice.getIndice().replace(" ", "_"));

                        if ((indice.getClave().equalsIgnoreCase("y")) || (indice.getClave().equalsIgnoreCase("s")) || (indice.getClave().equalsIgnoreCase("o"))) {

                            if (combobox.getValue() != null || !combobox.getValue().equalsIgnoreCase("")) {

                                if (indice.getClave().equalsIgnoreCase("y")) {
                                    indicePrimarioNuevo = combobox.getSelectedItem().getValue();
                                }

                                indice.setValor(combobox.getSelectedItem().getValue());

                            } else {
                                String arg = indice.getIndice().replace("_", " ");
                                arg = arg.toLowerCase();
                                char[] cs = arg.toCharArray();
                                char ch = cs[0];
                                cs[0] = Character.toUpperCase(ch);
                                arg = String.valueOf(cs);

                                throw new DW4JException("El " + arg + " no debe estar vacio");
                            }
                        } else {
                            indice.setValor(combobox.getSelectedItem().getValue());
                        }

                        traza.trace("Indice " + indice.getIndice() + " valor " + combobox.getValue(), Level.INFO);

                    } else if (panelIndice.getFellowIfAny(indice.getIndice().replace(" ", "_")) instanceof Datebox) {

                        Datebox datebox = (Datebox) panelIndice.getFellowIfAny(indice.getIndice().replace(" ", "_"));

                        if ((indice.getClave().equalsIgnoreCase("y")) || (indice.getClave().equalsIgnoreCase("s")) || (indice.getClave().equalsIgnoreCase("o"))) {

                            if (datebox.getValue() != null) {

                                if (indice.getClave().equalsIgnoreCase("y")) {
                                    indicePrimarioNuevo = datebox.getValue();
                                }

                                indice.setValor(sdf.format(datebox.getValue()));

                            } else {
                                String arg = indice.getIndice().replace("_", " ");
                                arg = arg.toLowerCase();
                                char[] cs = arg.toCharArray();
                                char ch = cs[0];
                                cs[0] = Character.toUpperCase(ch);
                                arg = String.valueOf(cs);

                                throw new DW4JException("El " + arg + " no debe estar vacio");
                            }
                        } else {
                            indice.setValor(sdf.format(datebox.getValue()));
                        }

                        traza.trace("Indice " + indice.getIndice() + " valor " + datebox.getValue(), Level.INFO);

                    }
                    expeWS.getIndices().add(indice);

                }

                expeWS.setExpediente(expediente.getIdExpediente());
                expeWS.setIdLibreria(idLibreria);
                expeWS.setIdCategoria(idCategoria);

                if (indicePrimarioActual.equals(indicePrimarioNuevo)) {
                    resp = Messagebox.show("Se procedera a cambiar el dato del indice principal\n"
                            + "¿Esta seguro de hacerlo?",
                            "Advertencia", Messagebox.YES | Messagebox.NO, Messagebox.EXCLAMATION,
                            new org.zkoss.zk.ui.event.EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                }

                            });
                    if (resp == Messagebox.YES) {

                        exito = new ActualizaIndice().updateIndices(expeWS);

                    } else {

                        resp = Messagebox.show("Seguro desea actualizar los Indices",
                                "Advertencia", Messagebox.YES | Messagebox.NO, Messagebox.EXCLAMATION,
                                new org.zkoss.zk.ui.event.EventListener() {

                                    @Override
                                    public void onEvent(Event evt) throws Exception {
                                    }

                                });
                        if (resp == Messagebox.YES) {

                            exito = new ActualizaIndice().updateIndices(expeWS);

                        }
                    }
                }

                if (exito) {
                    expediente.setResultado(true);
                    session.setAttribute("expediente", expediente);
                    herramientas.info("Indices Actializados");
                    mostrarIndces();
                } else {
                    expediente.setResultado(false);
                    session.setAttribute("expediente", expediente);
                    herramientas.warn("Problema al Actualizar los Indices");
                }

            } else {
                herramientas.info("Sesión finalizada");
                herramientas.navegarPagina("index.zul");
            }

        } catch (DW4JException e) {
            herramientas.warn(e.getMessage());
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al actualizar los indices", Level.ERROR, ex);
            herramientas.error("Problemas al Actualizar los Indices", ex);
        }

    }

    @Listen("onClick = #exit")
    public void salir() {

        try {
            System.runFinalization();
            System.gc();
            if (session != null) {
                herramientas.cerrarSesion();
                herramientas.navegarPagina("index.zul");
            } else {
                herramientas.navegarPagina("index.zul");
            }
        } catch (Exception ex) {
            traza.trace("error al salir", Level.ERROR, ex);
        }
    }

    @Listen("onClick = #regresa")
    public void regresar() {

        System.runFinalization();
        System.gc();
        try {
            if (session != null) {
                if (Constantes.ACCION.equalsIgnoreCase("CONSULTAR")) {
                    herramientas.navegarPagina("consulta.zul");
                } else if (Constantes.ACCION.equalsIgnoreCase("ACTUALIZAR_INDICES")) {
                    herramientas.navegarPagina("consultarIndices.zul");
                } else if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {
                    herramientas.navegarPagina("buscaExpediente.zul");
                }
            } else {
                herramientas.navegarPagina("index.zul");
            }
        } catch (Exception ex) {
            traza.trace("error al regresar", Level.ERROR, ex);
        }
    }
}
