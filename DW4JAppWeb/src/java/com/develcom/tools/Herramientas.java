/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.tools;

import com.develcom.tools.barra.IBarraEstado;
import com.develcom.tools.trazas.Traza;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author develcom
 */
public class Herramientas {

    private Traza traza = new Traza(Herramientas.class);
    private Session session;

    public Session crearSesion() {

        if (session == null) {
            session = Sessions.getCurrent();
        }

        return session;
    }

    public void cerrarSesion() {
        session.invalidate();
    }

    public String getNavegador() {

        String userAgent = Executions.getCurrent().getUserAgent();
        return userAgent;
    }

    public String getRutaAplicacion() {

        String realpath = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");
        return realpath;
    }

    public void error(String error, Throwable exception) {
        Messagebox.show(error + ":\n" + exception, "Error", Messagebox.OK, Messagebox.ERROR);

    }

    public void info(String message) {
        Messagebox.show(message, "Informacion", Messagebox.OK, Messagebox.INFORMATION);
    }

    public void warn(String message) {
        Messagebox.show(message, "Informacion", Messagebox.OK, Messagebox.EXCLAMATION);
    }

    public void navegarPagina(String url) {
        traza.trace("redirigiendo a la pagina "+url, Level.INFO);
        Executions.sendRedirect(url);
    }
    
    public Window crearVentanaModal(String archivo){
        Window window;
        traza.trace("creadon ventana modal con el archivo "+archivo, Level.INFO);
        window = (Window) Executions.createComponents(archivo, null, null);
        return window;
    }
    
    public IBarraEstado getBarraEstado(){        
        IBarraEstado barraEstado = null;
        
        if(barraEstado == null){
            barraEstado = (IBarraEstado) session.getAttribute("status");
        }
        
        traza.trace("barra de estado " + barraEstado, Level.INFO);
        
        return barraEstado;
    }

    public String getIP() {
        String ip;
        ip = Executions.getCurrent().getRemoteAddr();
        return ip;
    }

    public String getHost() {
        String host;
        host = Executions.getCurrent().getRemoteHost();
        return host;
    }

    public String getUsuario() {
        String usuario;
        usuario = Executions.getCurrent().getRemoteUser();        
        return usuario;
    }

    public String getID() {
        String id;
        id = Executions.getCurrent().getDesktop().getId();        
        return id;
    }
}
