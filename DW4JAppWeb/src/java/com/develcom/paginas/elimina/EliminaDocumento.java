/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.elimina;

import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.autentica.Configuracion;
import com.develcom.dao.Expediente;
import com.develcom.documento.Bufer;
import com.develcom.elimina.DatoAdicional;
import com.develcom.elimina.InfoDocumento;
import com.develcom.excepcion.DW4JDesktopExcepcion;
import com.develcom.paginas.herramientaArbol.CeldaArbol;
import com.develcom.paginas.herramientaArbol.InterpretaArbol;
import com.develcom.paginas.herramientaArbol.DatoNodoArbol;
import com.develcom.paginas.herramientaArbol.ModeloArbol;
import com.develcom.paginas.herramientaArbol.NodoArbol;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.trazas.Traza;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Window;
import ve.com.develcom.administracion.AdministracionBusqueda;
import ve.com.develcom.archivo.GestionArchivos;
import ve.com.develcom.elimina.QuitaDocumento;
import ve.com.develcom.expediente.GestionDocumentos;

/**
 *
 * @author develcom
 */
public class EliminaDocumento extends SelectorComposer<Component> {

    private static final long serialVersionUID = -8516820387373970052L;

    @Wire
    private Combobox cmbSubCategorias;

    @Wire
    private Tree arbolDocumentos;

    @Wire
    private Textbox txtExpediente;

    @Wire
    private Iframe documento;

    @Wire
    private Label lblFechaVencimiento;

    private Configuracion configuracion;
    private List<InfoDocumento> documentosEliminar = new ArrayList<>();
    private List<SubCategoria> subCategorias = null;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(EliminaDocumento.class);
    private Session session;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

//            lblFechaVencimiento.setVisible(true);
            expediente = (Expediente) session.getAttribute("expediente");
            configuracion = (Configuracion) session.getAttribute("configuracion");

            llenarSubCategorias();
        }

    }

    private boolean llenarSubCategorias() {
        boolean resp = false;
        Comboitem item;
        try {
            traza.trace("llenado listado de subCategorias", Level.INFO);

            subCategorias = new AdministracionBusqueda().buscarSubCategoria(null, expediente.getIdCategoria(), 0);

            if (subCategorias != null && !subCategorias.isEmpty()) {

                for (SubCategoria sc : subCategorias) {
                    if (sc.getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {

                        item = new Comboitem();
                        item.setValue(sc.getIdSubCategoria());
                        item.setLabel(sc.getSubCategoria().trim());
                        item.setParent(cmbSubCategorias);
                    }
                }
                resp = true;
            } else {
                herramientas.warn("La Categoria " + expediente.getCategoria() + "\nno tiene subCategorias");
            }
        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("error al llenar el listado de subCategorias", Level.ERROR, e);
            herramientas.error("Error al llenar el listado de subCategorias\n", e);
        }
        return resp;
    }

    @Listen("onSelect = #cmbSubCategorias")
    public void buscarDocumentos() {

        int idSubCategoria;
        String idExpediente, subCategoria;

        if (session != null) {

            idSubCategoria = Integer.parseInt(cmbSubCategorias.getSelectedItem().getValue().toString());
            subCategoria = cmbSubCategorias.getSelectedItem().getLabel();
            idExpediente = txtExpediente.getValue();
            construirArbol(idExpediente, idSubCategoria, subCategoria);
        }

    }

    private void construirArbol(String idExpediente, int idSubCategoria, String subCategoria) {

        List<Integer> idDocumento = new ArrayList<>();
        List<TipoDocumento> tipoDocumentos = new ArrayList<>();
        int idCat, totalDocDig, cont = 0;
        String nombreTD, tmp[], fullName;

        Treecols treecols = new Treecols();
        Treecol categoria;
        Treecol datoAdicional = new Treecol("Datos Adicionales");

        NodoArbol raiz;
        LinkedList<NodoArbol> nodoSubCate = new LinkedList();
        LinkedList<NodoArbol> nodoTipoDoc = new LinkedList();
        DatoNodoArbol dna;

        try {

            if (session != null) {

                if (!documentosEliminar.isEmpty()) {
                    documentosEliminar.clear();
                }
                if (!nodoTipoDoc.isEmpty()) {
                    nodoTipoDoc.clear();
                }

                idCat = expediente.getIdCategoria();

                categoria = new Treecol(expediente.getCategoria() + " - " + txtExpediente.getValue());
                categoria.setWidth("250px");
                categoria.setParent(treecols);

                datoAdicional.setWidth("300px");
                datoAdicional.setParent(treecols);

                treecols.setParent(arbolDocumentos);

                traza.trace("subCategoria " + subCategoria, Level.INFO);
                tipoDocumentos = new AdministracionBusqueda().buscarTipoDocumento(null, idCat, idSubCategoria);

                for (TipoDocumento td : tipoDocumentos) {
                    if (td.getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {
                        idDocumento.add(td.getIdTipoDocumento());
                    }
                }

                if (!idDocumento.isEmpty()) {
                    traza.trace("busca informacion de los documentos de la SubCategoria " + subCategoria + " su id " + idSubCategoria, Level.INFO);
                    //infoDoc = ed.buscarListadoInfoDocumentos(idDocumento, idExpediente);
//                infoDocumentosArbol = new QuitaDocumento().buscarListadoInfoDocumentos(idDocumento, idExpediente);
                    List<com.develcom.documento.InfoDocumento> infoDocumentosArbolDoc = new GestionDocumentos().encontrarInformacionDoc(idDocumento, idExpediente, 0, 0, 2, 2, false);
                    convertir(infoDocumentosArbolDoc);
                    traza.trace("tamaño lista de InfoDocumento " + infoDocumentosArbolDoc.size(), Level.INFO);

                    if (!documentosEliminar.isEmpty()) {

                        totalDocDig = documentosEliminar.size();

                        tmp = new String[totalDocDig];

                        for (int j = 0; j < totalDocDig; j++) {

                            InfoDocumento infoDoc = documentosEliminar.get(j);

                            nombreTD = infoDoc.getTipoDocumento() + " - " + infoDoc.getNumeroDocumento();
                            infoDoc.setTipoDocumento(nombreTD);
                            tmp[cont] = nombreTD;
                            cont++;
                            int h = 0;
                            for (String s : tmp) {
                                try {
                                    if (s.equalsIgnoreCase(nombreTD)) {
                                        h++;
                                    }
                                } catch (NullPointerException e) {
                                }

                            }
                            if (h == 1) {
                                String da = "";
                                if (infoDoc.isTipoDocDatoAdicional()) {
                                    List<DatoAdicional> datosAdicionales = infoDoc.getLsDatosAdicionales();
                                    int size = datosAdicionales.size(), i = 1;
                                    for (DatoAdicional datAd : datosAdicionales) {

                                        if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                            if (i == size) {
                                                da = da + " " + datAd.getValor().toString();
                                            } else {
                                                da = da + " " + datAd.getValor().toString() + ",";
                                            }
                                        } else {
                                            if (i == size) {
                                                da = da + " " + datAd.getValor();
                                            } else {
                                                da = da + " " + datAd.getValor() + ",";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                da = da.trim();

                                if (configuracion.isCalidadActivo()) {
                                    if (infoDoc.isTipoDocDatoAdicional()) {

                                        fullName = nombreTD + " " + infoDoc.getEstatusDocumento() + " (" + da + ")";

                                        dna = new DatoNodoArbol(infoDoc.getTipoDocumento(), da);
                                        dna.setIdInfoDocumento(infoDoc.getIdInfoDocumento());
                                        dna.setIdDocumento(infoDoc.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosEliminar.get(j).setTipoDocumento(fullName);

                                    } else {

                                        fullName = nombreTD + " " + infoDoc.getEstatusDocumento();

                                        dna = new DatoNodoArbol(infoDoc.getTipoDocumento(), "");
                                        dna.setIdInfoDocumento(infoDoc.getIdInfoDocumento());
                                        dna.setIdDocumento(infoDoc.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosEliminar.get(j).setTipoDocumento(fullName);

                                    }
                                } else {
                                    if (infoDoc.isTipoDocDatoAdicional()) {

                                        fullName = nombreTD + " (" + da + ")";

                                        dna = new DatoNodoArbol(infoDoc.getTipoDocumento(), da);
                                        dna.setIdInfoDocumento(infoDoc.getIdInfoDocumento());
                                        dna.setIdDocumento(infoDoc.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosEliminar.get(j).setTipoDocumento(fullName);

                                    } else {

                                        fullName = nombreTD;

                                        dna = new DatoNodoArbol(infoDoc.getTipoDocumento(), "");
                                        dna.setIdInfoDocumento(infoDoc.getIdInfoDocumento());
                                        dna.setIdDocumento(infoDoc.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosEliminar.get(j).setTipoDocumento(fullName);

                                    }
                                }

                                traza.trace("tipo de documento digitalizado " + fullName + " id InfoDocumento " + infoDoc.getIdInfoDocumento(), Level.INFO);
                            }
                        }
                        nodoSubCate.add(new NodoArbol(new DatoNodoArbol(subCategoria), nodoTipoDoc));
                    } else {
                        herramientas.info("No hay documentos para eliminar");
                    }
                }

                raiz = new NodoArbol(null, nodoSubCate);
                arbolDocumentos.setItemRenderer(new InterpretaArbol());
                arbolDocumentos.setModel(new ModeloArbol(raiz));

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("Error al crear el arbol de documentos digitalizados", Level.ERROR, e);
            herramientas.error("Error al crear el arbol de documentos", e);
        }
    }

    private void convertir(List<com.develcom.documento.InfoDocumento> documentos) {

        InfoDocumento infoDocumento;
        for (com.develcom.documento.InfoDocumento infoDoc : documentos) {
            infoDocumento = new InfoDocumento();

            infoDocumento.setCantPaginas(infoDoc.getCantPaginas());
            infoDocumento.setCausaRechazo(infoDoc.getCausaRechazo());
            infoDocumento.setDatoAdicional(infoDoc.getDatoAdicional());
            infoDocumento.setEstatus(infoDoc.getEstatus());
            infoDocumento.setEstatusDocumento(infoDoc.getEstatusDocumento());
            infoDocumento.setFechaActual(infoDoc.getFechaActual());
            infoDocumento.setFechaAprobacion(infoDoc.getFechaAprobacion());
            infoDocumento.setFechaDigitalizacion(infoDoc.getFechaDigitalizacion());
            infoDocumento.setFechaRechazo(infoDoc.getFechaRechazo());
            infoDocumento.setFechaVencimiento(infoDoc.getFechaVencimiento());
            infoDocumento.setFolio(infoDoc.getFolio());
            infoDocumento.setFormato(infoDoc.getFormato());
            infoDocumento.setIdDocumento(infoDoc.getIdDocumento());
            infoDocumento.setIdExpediente(infoDoc.getIdExpediente());
            infoDocumento.setIdInfoDocumento(infoDoc.getIdInfoDocumento());
            infoDocumento.setMotivoRechazo(infoDoc.getMotivoRechazo());
            infoDocumento.setNombreArchivo(infoDoc.getNombreArchivo());
            infoDocumento.setNuevo(infoDoc.isNuevo());
            infoDocumento.setNumeroDocumento(infoDoc.getNumeroDocumento());
            infoDocumento.setReDigitalizo(infoDoc.isReDigitalizo());
            infoDocumento.setRutaArchivo(infoDoc.getRutaArchivo());
            infoDocumento.setTipoDocumento(infoDoc.getTipoDocumento());
            infoDocumento.setUsuarioAprobacion(infoDoc.getUsuarioAprobacion());
            infoDocumento.setUsuarioDigitalizo(infoDoc.getUsuarioDigitalizo());
            infoDocumento.setUsuarioRechazo(infoDoc.getUsuarioRechazo());
            infoDocumento.setVersion(infoDoc.getVersion());
            infoDocumento.setTipoDocDatoAdicional(infoDoc.isTipoDocDatoAdicional());
            if (!infoDoc.getLsDatosAdicionales().isEmpty()) {
                for (com.develcom.documento.DatoAdicional da : infoDoc.getLsDatosAdicionales()) {
                    com.develcom.elimina.DatoAdicional da1 = new com.develcom.elimina.DatoAdicional();
                    da1.setCodigo(da.getCodigo());
                    da1.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
                    da1.setIdDatoAdicional(da.getIdDatoAdicional());
                    da1.setIdTipoDocumento(da.getIdTipoDocumento());
                    da1.setIdValor(da.getIdValor());
                    da1.setNumeroDocumento(da.getNumeroDocumento());
                    da1.setTipo(da.getTipo());
                    da1.setValor(da.getValor());
                    infoDocumento.getLsDatosAdicionales().add(da1);
                }
            }

            documentosEliminar.add(infoDocumento);
        }
    }

    @Listen("onSelect = #arbolDocumentos")
    public void verDocumento() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Bufer buffer;
        FileOutputStream escribiendo;
        ToolsFiles toolsFile = new ToolsFiles();
        InfoDocumento infoDocBuscado;
        int idInfoDocumento, idDoc, idCat, version, numDoc, idSubCat;
        String idExpediente, tipoDocumento, ruta, archivo;
        Treeitem treeitem;
        Treerow treerow;
        CeldaArbol tipoDoc;
        NodoArbol<DatoNodoArbol> nodoArbol;
        DatoNodoArbol dna;
        File fileCod, doc;

        try {
            if (session != null) {
                
                documento.setContent(null);

                nodoArbol = arbolDocumentos.getSelectedItem().getValue();
                dna = nodoArbol.getData();
                traza.trace("id infodocumento seleccionado " + dna.getIdInfoDocumento(), Level.INFO);
                idInfoDocumento = dna.getIdInfoDocumento();

                for (InfoDocumento id : documentosEliminar) {
                    if (id.getIdInfoDocumento() == idInfoDocumento) {

                        idDoc = dna.getIdDocumento();
                        version = id.getVersion();
                        numDoc = id.getNumeroDocumento();
                        idCat = expediente.getIdCategoria();
                        idSubCat = dna.getIdSubCategoria();
                        idExpediente = txtExpediente.getValue();
                        traza.trace("buscando el documento idDocumento " + idDoc + " idCategoria " + idCat + " idSubCategoria " + idSubCat + " idExpediente " + idExpediente, Level.INFO);

                        System.gc();
                        infoDocBuscado = new QuitaDocumento().buscarDatosDoc(idInfoDocumento, idExpediente, version, numDoc);

                        if (infoDocBuscado != null) {

                            fileCod = new File(toolsFile.getArchivoCodificado());
                            if (toolsFile.getDirTemporal().exists()) {
                                File[] files = toolsFile.getDirTemporal().listFiles();
                                for (File f : files) {
                                    if (f.delete()) {
                                        traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                                    } else {
                                        traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                                    }
                                }
                            }

                            try {
                                XMLGregorianCalendar xmlCalendar = infoDocBuscado.getFechaVencimiento();
                                GregorianCalendar fechaVencimiento = xmlCalendar.toGregorianCalendar();

                                lblFechaVencimiento.setValue("Fecha de Vencimiento: " + sdf.format(fechaVencimiento.getTime()));
                                lblFechaVencimiento.setVisible(true);
                            } catch (NullPointerException e) {
                            }

                            ruta = infoDocBuscado.getRutaArchivo();
                            archivo = infoDocBuscado.getNombreArchivo();
                            version = infoDocBuscado.getVersion();

                            traza.trace("ruta a buscar " + ruta, Level.INFO);
                            traza.trace("archivo a buscar " + archivo, Level.INFO);
                            traza.trace("version del documento a mostrar " + version, Level.INFO);

                            if (archivo == null) {
                                throw new DW4JDesktopExcepcion("Falta información del documento");
                            }

                            //busca el archivo fisico del tipo de documento
                            buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);

                            if (!buffer.isExiste()) {
                                throw new Exception("Problemas al buscar el fisico del documento");
                            }
                            fileCod = new File(toolsFile.getArchivoCodificado());
                            escribiendo = new FileOutputStream(fileCod);
                            escribiendo.write(buffer.getBufer());
                            escribiendo.flush();
                            escribiendo.close();

                            doc = new File("documento" + Constantes.CONTADOR++ + "." + id.getFormato());

                            toolsFile.decodificar(doc.getName());

                            if (fileCod.exists()) {
                                mostarDocumento(new File(toolsFile.getTempPath(), doc.getName()));
                            }

                        }

                        break;
                    }
                }

            }
        } catch (Exception e) {
            herramientas.error("Problemas al generar la información para mostra el Documento", e);
            traza.trace("error al ver el documento", Level.ERROR, e);
        }
    }

    @Listen("onClick = #btnRechazar")
    public void rechazar() {

        Treeitem item;
        NodoArbol<DatoNodoArbol> nodoArbol;
        DatoNodoArbol dna;

        if (session != null) {

            nodoArbol = arbolDocumentos.getSelectedItem().getValue();
            dna = nodoArbol.getData();

            item = arbolDocumentos.getSelectedItem();

            if (nodoArbol.isLeaf()) {

                Window window = herramientas.crearVentanaModal("/rechazaDocumento.zul");
                window.doModal();

                session = herramientas.crearSesion();
                expediente = (Expediente) session.getAttribute("expediente");

                if (expediente.isResultado()) {

                    nodoArbol.removeFromParent();
                    documento.setContent(null);

                }
            }
        }
    }

    private void mostarDocumento(File file) {

        ToolsFiles toolsFile = new ToolsFiles();
        AMedia media;
        ByteArrayInputStream is;
        FileInputStream fs;
        byte[] buffer;
        String formato;

        try {

            if (session != null) {

                formato = file.getName().substring(file.getName().indexOf(".") + 1);

                if (formato.equalsIgnoreCase("pdf")) {

                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else if ((formato.equalsIgnoreCase("jpg")) || (formato.equalsIgnoreCase("jpge"))) {

                    file = toolsFile.guardarArchivoJPGtoPDF(file);
                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else if ((formato.equalsIgnoreCase("tif")) || (formato.equalsIgnoreCase("tiff"))) {

                    file = toolsFile.guardarArchivoTIFFtoPDF(file);
                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else {
                    herramientas.warn("Hubo un problema en la conversión del documento, se regresará");
                }

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (Exception e) {
            traza.trace("problemas al mostrar el documento", Level.ERROR, e);
        }
    }

}
