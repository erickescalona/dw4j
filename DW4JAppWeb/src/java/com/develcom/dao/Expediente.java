/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.dao;

//import com.develcom.expediente.SubCategoria;
import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.documento.DatoAdicional;
import com.develcom.tools.excepcion.DW4JException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author develcom
 */
public class Expediente implements Serializable {

    private static final long serialVersionUID = -1119294399934574731L;

    private String idExpediente;

    private int idLibreria;
    private String libreria;
    private int idCategoria;
    private String categoria;
    private int idSubCategoria;
    private String subCategoria;
    private int idTipoDocumento;
    private String tipoDocumento;
    private int idInfoDocumento;
    private String nombreReporte;
    private int reporte;
    private List<String> rolUsuario = new ArrayList<>();

    private List<SubCategoria> subCategorias;
    private List<TipoDocumento> tipoDocumentos;

    private String accion;
    private int numeroDocumento;
    private int versionDocumento;
    
    private boolean resultado;

    public List<SubCategoria> getSubCategorias() {
        return subCategorias;
    }

    public void setSubCategorias(List<SubCategoria> subCategorias) {
        this.subCategorias = subCategorias;
    }

    public List<TipoDocumento> getTipoDocumentos() {
        return tipoDocumentos;
    }

    public void setTipoDocumentos(List<TipoDocumento> tipoDocumentos) {
        this.tipoDocumentos = tipoDocumentos;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(String idExpediente) {
        this.idExpediente = idExpediente;
    }

    public int getIdInfoDocumento() {
        return idInfoDocumento;
    }

    public void setIdInfoDocumento(int idInfoDocumento) {
        this.idInfoDocumento = idInfoDocumento;
    }

    public String getNombreReporte() {
        return nombreReporte;
    }

    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

    public int getReporte() {
        return reporte;
    }

    public void setReporte(int reporte) {
        this.reporte = reporte;
    }

    public int getIdLibreria() {
        return idLibreria;
    }

    public void setIdLibreria(int idLibreria) {
        this.idLibreria = idLibreria;
    }

    public int getIdSubCategoria() {
        return idSubCategoria;
    }

    public void setIdSubCategoria(int idSubCategoria) {
        this.idSubCategoria = idSubCategoria;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getLibreria() {
        return libreria;
    }

    public void setLibreria(String libreria) {
        this.libreria = libreria;
    }

    public String getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(String subCategoria) {
        this.subCategoria = subCategoria;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public List<String> getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(List<String> rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public void agregaRolUsuario(String rol) throws DW4JException {
        if (rol != null) {
            if (!rol.equalsIgnoreCase("")) {
                rolUsuario.add(rol);
            } else {
                throw new DW4JException("Falta Rol");
            }
        } else {
            throw new DW4JException("Rol nulo");
        }
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public int getVersionDocumento() {
        return versionDocumento;
    }

    public void setVersionDocumento(int versionDocumento) {
        this.versionDocumento = versionDocumento;
    }

    public boolean isResultado() {
        return resultado;
    }

    public void setResultado(boolean resultado) {
        this.resultado = resultado;
    }
}
