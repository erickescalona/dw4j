/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom;

import com.develcom.dto.Expediente;
import com.develcom.dto.ManejoSesion;
import com.develcom.log.Traza;
import com.develcom.verdocumento.VerDocumento;
import com.develcom.documento.InfoDocumento;
import com.develcom.verdocumento.VerDocumentoPDF;
import com.develcom.verdocumento.VerImagenes;
import java.io.File;
import java.security.AccessController;
import java.security.PrivilegedAction;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
//import net.datenwerke.sandbox.SandboxContext;
//import net.datenwerke.sandbox.SandboxService;
//import net.datenwerke.sandbox.SandboxServiceImpl;
import org.apache.log4j.Level;
import ve.com.develcom.tools.Configuracion;

/**
 *
 * @author develcom
 */
public class Main {

    public static void main(String[] args) {

        Traza traza = new Traza(Main.class);
        InfoDocumento infoDocumento = new InfoDocumento();
        Expediente expediente = new Expediente();
        String home = System.getProperty("user.home");
        String so = System.getProperty("os.name");
        //String separador = System.getProperty("line.separator");
        File dir = null;

        //String win = so.substring(0, 6);
        //int index = so.indexOf("windows");
        boolean ok = so.startsWith("Windows"), creo;

        //if(win.equalsIgnoreCase("windows")){
        if (ok) {
            dir = new File("c:\\" + Traza.RUTALOCAL);
        } else if (so.equalsIgnoreCase("linux")) {
            dir = new File(home + "/" + Traza.RUTALOCAL);
        }

        try {
            creo = dir.exists();
            if (creo) {
                traza.trace("exito al crea el directorio en: " + so, Level.INFO);
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "Error al crear la carpeta principal\n(" + so + ")", "Error...!!!", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            }

            //            if(!dir.exists()){
            //                dir.mkdir();
            //            }else{
            //                JOptionPane.showMessageDialog(new JFrame(), "Error al crear la carpeta principal", "Error...!!!", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            //            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(new JFrame(), "Error al crear la carpeta principal\n" + e.getMessage(), "Error...!!!", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
        }

        //System.out.println("ruta donde se ejecuta el jnlp "+appPath);
        //        traza.trace("ruta donde se ejecuta el jnlp "+appPath, Level.INFO);
        int idInfoDoc;
        int idDoc;
        int version;
        int numDoc;
        int idCat;
        int idSubCat;
        String idExpediente;
        String libreria;
        String categoria;
        String subCategoria;
        String tipoDocumento;
        String login;
        String formatoArchivo;
        String srvWS;
        String puertoWS;

        idInfoDoc = Integer.valueOf(args[0].replace("\"", ""));
        idDoc = Integer.valueOf(args[1].replace("\"", ""));
        version = Integer.valueOf(args[2].replace("\"", ""));
        numDoc = Integer.valueOf(args[3].replace("\"", ""));
        idCat = Integer.valueOf(args[4].replace("\"", ""));
        idSubCat = Integer.valueOf(args[5].replace("\"", ""));
        idExpediente = args[6].replace("\"", "");
        libreria = args[7].replace("\"", "");
        categoria = args[8].replace("\"", "");
        subCategoria = args[9].replace("\"", "");
        tipoDocumento = args[10].replace("\"", "");
        login = args[11].replace("\"", "");
        formatoArchivo = args[12];
        srvWS = args[13];
        puertoWS = args[14];

        infoDocumento.setIdInfoDocumento(idInfoDoc);
        infoDocumento.setIdDocumento(idDoc);
        infoDocumento.setVersion(version);
        infoDocumento.setNumeroDocumento(numDoc);
        infoDocumento.setFormato(formatoArchivo);
        expediente.setIdCategoria(idCat);
        expediente.setIdSubCategoria(idSubCat);
        expediente.setIdExpediente(idExpediente);
        expediente.setLibreria(libreria);
        expediente.setCategoria(categoria);
        expediente.setSubCategoria(subCategoria);
        expediente.setTipoDocumento(tipoDocumento);
        infoDocumento.setTipoDocumento(tipoDocumento);

        traza.trace("bienvenido", Level.INFO);
        traza.trace("idInfoDocumento " + idInfoDoc, Level.INFO);
        traza.trace("idDocumento " + idDoc, Level.INFO);
        traza.trace("version " + version, Level.INFO);
        traza.trace("numero documento " + numDoc, Level.INFO);
        traza.trace("idCategoria " + idCat, Level.INFO);
        traza.trace("idSubCategoria " + idSubCat, Level.INFO);
        traza.trace("idExpediente " + idExpediente, Level.INFO);
        traza.trace("libreria " + libreria, Level.INFO);
        traza.trace("categoria " + categoria, Level.INFO);
        traza.trace("subCategoria " + subCategoria, Level.INFO);
        traza.trace("tipoDocumento " + tipoDocumento, Level.INFO);
        traza.trace("usuario " + login, Level.INFO);
        traza.trace("extencion del archivo " + formatoArchivo, Level.INFO);
        traza.trace("servidor WS " + srvWS, Level.INFO);
        traza.trace("puerto WS " + puertoWS, Level.INFO);

        ManejoSesion.setExpediente(expediente);
        ManejoSesion.setLogin(login);

        Configuracion.setDocView(true);
        Configuracion.setSrvWS(srvWS);
        Configuracion.setPuertoWS(puertoWS);

        if (formatoArchivo.equalsIgnoreCase("tif") || formatoArchivo.equalsIgnoreCase("tiff")) {
            VerDocumento documento = new VerDocumento(infoDocumento);
        } else if (formatoArchivo.equalsIgnoreCase("pdf")) {
            VerDocumentoPDF vdpdf = new VerDocumentoPDF(infoDocumento);
        } else {
            VerImagenes verImagenes = new VerImagenes(infoDocumento);
        }

//        AccessController.doPrivileged(new PrivilegedAction() {
//
//            @Override
//            public Object run() {
//
//                try {
//                    if (formatoArchivo.equalsIgnoreCase("tif") || formatoArchivo.equalsIgnoreCase("tiff")) {
//                        VerDocumento documento = new VerDocumento(infoDocumento);
//                    } else if (formatoArchivo.equalsIgnoreCase("pdf")) {
//                        VerDocumentoPDF vdpdf = new VerDocumentoPDF(infoDocumento);
//                    } else {
//                        VerImagenes verImagenes = new VerImagenes(infoDocumento);
//                    }
//                } catch (Exception e) {
//
//                }
//                return null;
//            }
//        });
    }
}
