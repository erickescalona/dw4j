/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.calidad;

import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.dao.Expediente;
import com.develcom.documento.Bufer;
import com.develcom.documento.InfoDocumento;
import com.develcom.excepcion.DW4JDesktopExcepcion;
import com.develcom.expediente.Indice;
import com.develcom.paginas.herramientaArbol.InterpretaArbol;
import com.develcom.paginas.consulta.Resultado;
import com.develcom.paginas.herramientaArbol.DatoNodoArbol;
import com.develcom.paginas.herramientaArbol.ModeloArbol;
import com.develcom.paginas.herramientaArbol.NodoArbol;
import com.develcom.paginas.reportes.tools.ProcesaReporte;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.trazas.Traza;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Window;
import ve.com.develcom.administracion.AdministracionBusqueda;
import ve.com.develcom.archivo.GestionArchivos;
import ve.com.develcom.expediente.BuscaIndice;
import ve.com.develcom.expediente.GestionDocumentos;

/**
 *
 * @author develcom
 */
public class CalidadDocumento extends SelectorComposer<Component> {

    @Wire
    private Tree arbolDoc;

    @Wire
    private Iframe documento;

    @Wire
    private Label lblFechaVencimiento;

    @Wire
    private Combobox cmbVersion;

    @Wire
    private Button btnAbrir;

    @Wire
    private Button btnAprobar;

    @Wire
    private Button btnRechazar;

    @Wire
    private Groupbox grpTitulo;

    private List<SubCategoria> listaSubCategorias;
    private List<InfoDocumento> infoDocumentos = new ArrayList<>();
    private static final long serialVersionUID = -6839694495364740826L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(Resultado.class);
    private Session session;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    public void iniciar() {

        try {

            session = herramientas.crearSesion();

            if (session != null) {

                documento.setScrolling("yes");
                btnAbrir.setVisible(false);
                lblFechaVencimiento.setVisible(false);
                cmbVersion.setVisible(false);

                btnRechazar.setDisabled(true);
                btnAprobar.setDisabled(true);

                expediente = (Expediente) session.getAttribute("expediente");

                grpTitulo.setTitle(crearTituloExpediente());

                mostrarArbol();
            }

        } catch (Exception e) {
            traza.trace("error al iniciar el objeto consulta", Level.INFO, e);
            herramientas.error("Problemas en el resultado de la consulta", e);
        }
    }

    private void mostrarArbol() {

        List<InfoDocumento> infoDocs = new ArrayList<>();
        List<TipoDocumento> tipoDocumentos;
        int idCat, idSubCat, totalDocDig, cont = 0;
        String tmp[], nombreTD;

        Treecols treecols = new Treecols();
        Treecol categoria;
        Treecol datoAdicional = new Treecol("Datos Adicionales");

        NodoArbol raiz;
        LinkedList<NodoArbol> nodoSubCate = new LinkedList();
        LinkedList<NodoArbol> nodoTipoDoc = new LinkedList();
        DatoNodoArbol dna;
        List<Integer> idDocumento = new ArrayList<>();

        try {

            if (session != null) {
                idCat = expediente.getIdCategoria();
                idSubCat = expediente.getIdSubCategoria();

                listaSubCategorias = new AdministracionBusqueda().buscarSubCategoria("", idCat, 0);
                traza.trace("tamaño lista de SubCategoria " + listaSubCategorias.size(), Level.INFO);

                categoria = new Treecol(expediente.getCategoria());
                categoria.setWidth("250px");
                categoria.setParent(treecols);

                datoAdicional.setWidth("300px");
                datoAdicional.setParent(treecols);

                treecols.setParent(arbolDoc);

                for (SubCategoria sc : listaSubCategorias) {

                    traza.trace("subCategoria " + sc.getSubCategoria(), Level.INFO);
                    tipoDocumentos = new AdministracionBusqueda().buscarTipoDocumento(null, idCat, sc.getIdSubCategoria());

                    for (TipoDocumento td : tipoDocumentos) {
                        if (td.getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {
                            idDocumento.add(td.getIdTipoDocumento());
                        }
                    }

                    if (!idDocumento.isEmpty()) {
                        traza.trace("busca informacion de los documentos de la SubCategoria " + sc.getSubCategoria() + " su id " + sc.getIdSubCategoria(), Level.INFO);
                        infoDocs = new GestionDocumentos().encontrarInformacionDoc(idDocumento, expediente.getIdExpediente(), idCat, sc.getIdSubCategoria(), 0, 0, false);

                        traza.trace("tamaño lista de InfoDocumento " + infoDocs.size(), Level.INFO);

                        if (!infoDocs.isEmpty()) {

                            totalDocDig = infoDocs.size();
                            traza.trace("total documentos digitalizados de la subCategoria " + sc.getSubCategoria(), Level.INFO);

                            tmp = new String[totalDocDig];

                            for (InfoDocumento id : infoDocs) {
                                infoDocumentos.add(id);

                                nombreTD = id.getTipoDocumento() + " - " + id.getNumeroDocumento();
                                id.setTipoDocumento(nombreTD);
                                tmp[cont] = nombreTD;
                                cont++;
                                int h = 0;
                                for (String s : tmp) {
                                    try {
                                        if (s.equalsIgnoreCase(nombreTD)) {
                                            h++;
                                        }
                                    } catch (NullPointerException e) {
                                    }

                                }
                                if (h == 1) {
                                    String da = "";
                                    if (id.isTipoDocDatoAdicional()) {
                                        List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                                        int size = datosAdicionales.size(), i = 1;
                                        for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                            if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                                if (i == size) {
                                                    da = da + " " + datAd.getValor().toString();
                                                } else {
                                                    da = da + " " + datAd.getValor().toString() + ",";
                                                }
                                            } else {
                                                if (i == size) {
                                                    da = da + " " + datAd.getValor();
                                                } else {
                                                    da = da + " " + datAd.getValor() + ",";
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                    da = da.trim();
                                    if (id.isTipoDocDatoAdicional()) {

                                        dna = new DatoNodoArbol(id.getTipoDocumento(), da);
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(sc.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                    } else {

                                        dna = new DatoNodoArbol(id.getTipoDocumento(), "");
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(sc.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                    }
                                    traza.trace("tipo de documento digitalizado " + id.getTipoDocumento() + " id InfoDocumento " + id.getIdInfoDocumento(), Level.INFO);
                                }
                            }
                            nodoSubCate.add(new NodoArbol(new DatoNodoArbol(sc.getSubCategoria()), nodoTipoDoc));

                        }

                    }
                    cont = 0;

                    if (!idDocumento.isEmpty()) {
                        idDocumento.clear();
                    }
                    if (!tipoDocumentos.isEmpty()) {
                        tipoDocumentos.clear();
                    }
                    if (!infoDocs.isEmpty()) {
                        infoDocs.clear();
                    }

                }
                raiz = new NodoArbol(null, nodoSubCate, true);
                arbolDoc.setItemRenderer(new InterpretaArbol());
                arbolDoc.setModel(new ModeloArbol(raiz));
            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al generar el arbol de documentos", Level.ERROR, ex);
            herramientas.error("Problemas creando el arbol de documentos", ex);
        }
    }

    @Listen("onSelect = #arbolDoc")
    public void verDocumento() {

        InfoDocumento infoDocumento = null;
        NodoArbol<DatoNodoArbol> nodoArbol;
        DatoNodoArbol dna;
        int idInfoDocumento, idDocumento, idSubCategoria;
        String subCategoria, tipoDocumento, formato;

        try {
            if (session != null) {

                nodoArbol = arbolDoc.getSelectedItem().getValue();
                dna = nodoArbol.getData();

                traza.trace("id infodocumento seleccionado " + dna.getIdInfoDocumento(), Level.INFO);

                if (nodoArbol.isLeaf()) {
                    idSubCategoria = dna.getIdSubCategoria();
                    tipoDocumento = dna.getTipoDocumento();
                    idDocumento = dna.getIdDocumento();
                    idInfoDocumento = dna.getIdInfoDocumento();

                    for (InfoDocumento infDoc : infoDocumentos) {
                        if (infDoc.getIdInfoDocumento() == idInfoDocumento) {

                            formato = infDoc.getFormato();
                            expediente.setTipoDocumento(tipoDocumento);
                            expediente.setIdSubCategoria(idSubCategoria);
                            expediente.setIdTipoDocumento(idDocumento);
                            expediente.setAccion("vizualizar");

                            traza.trace("tipo de documento a mostrar " + tipoDocumento, Level.INFO);
                            traza.trace("nombre del archivo " + formato, Level.INFO);
                            traza.trace("formato del archivo " + infDoc.getFormato(), Level.INFO);
                            traza.trace("ruta del archivo " + infDoc.getRutaArchivo(), Level.INFO);
                            traza.trace("version del archivo " + infDoc.getVersion(), Level.INFO);
                            traza.trace("id del archivo " + infDoc.getIdInfoDocumento(), Level.INFO);
                            traza.trace("numero del documento (archivo) " + infDoc.getNumeroDocumento(), Level.INFO);
                            traza.trace("id del documento " + infDoc.getIdDocumento(), Level.INFO);
                            traza.trace("fecha vencimiento del archivo " + infDoc.getFechaVencimiento(), Level.INFO);

                            infoDocumento = infDoc;
                            infoDocumento.setIdInfoDocumento(idInfoDocumento);

                            break;

                        }

                    }
                    buscarDocumento(infoDocumento, idSubCategoria, idDocumento, idInfoDocumento);
                }

            }

        } catch (Exception e) {
            herramientas.error("Problemas al generar la información para mostra el Documento", e);
            traza.trace("error al ver el documento", Level.ERROR, e);
        }

    }

    private void buscarDocumento(InfoDocumento infoDocumento, int idSubCategoria, int idDocumento, int idInfoDocumento) {

        List<InfoDocumento> infoDocs;// = new ArrayList<>();
        ToolsFiles toolsFile = new ToolsFiles();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String ruta = "", archivo = "", idExpediente;
        Bufer buffer;
        FileOutputStream escribiendo;
        File doc, fileCod = null;
        int idCat, numDoc, cont = 0, version;
        Comboitem item;

        try {

            if (documento.getContent() != null) {
                documento.setContent(null);
            }

            fileCod = new File(toolsFile.getArchivoCodificado());

            numDoc = infoDocumento.getNumeroDocumento();
            idCat = expediente.getIdCategoria();
            idExpediente = expediente.getIdExpediente();
            version = infoDocumento.getVersion();

            infoDocs = new GestionArchivos().buscarImagenDocumentos(idInfoDocumento, idDocumento, idCat, idSubCategoria, version, numDoc, idExpediente);

            if (infoDocs.size() > 0) {

                if (toolsFile.getDirTemporal().exists()) {
                    File[] files = toolsFile.getDirTemporal().listFiles();
                    for (File f : files) {
                        if (f.delete()) {
                            traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                        } else {
                            traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                        }
                    }
                }

                for (InfoDocumento id : infoDocs) {

                    if (id.getEstatusDocumento().equalsIgnoreCase("pendiente")) {
                        item = new Comboitem();
                        item.setLabel(String.valueOf(id.getVersion()));
                        item.setValue(id.getVersion());
                        item.setParent(cmbVersion);
                        cont++;
                    }

                    if (id.getFechaVencimiento() != null) {

                        XMLGregorianCalendar xmlCalendar = id.getFechaVencimiento();
                        GregorianCalendar fechaVencimiento = xmlCalendar.toGregorianCalendar();

                        lblFechaVencimiento.setValue("Fecha de Vencimiento: " + sdf.format(fechaVencimiento.getTime()));
                        lblFechaVencimiento.setVisible(true);

                        ruta = id.getRutaArchivo();
                        archivo = id.getNombreArchivo();

                    } else {
                        ruta = id.getRutaArchivo();
                        archivo = id.getNombreArchivo();
                    }
                }

                if (infoDocs.size() == 1 || cont == 1) {
                    cmbVersion.setVisible(false);
                    btnAbrir.setVisible(false);
                } else {
                    cmbVersion.setVisible(true);
                    btnAbrir.setVisible(true);
                }

                traza.trace("ruta a buscar " + ruta, Level.INFO);
                traza.trace("archivo a buscar " + archivo, Level.INFO);

                if (archivo == null) {
                    throw new DW4JDesktopExcepcion("Falta información del documento");
                }
                //busca el archivo fisico del tipo de documento
                buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);
                if (!buffer.isExiste()) {
                    throw new DW4JDesktopExcepcion("Problemas al buscar el fisico del documento\nComuniquese con el administrador del sistema");
                }
                escribiendo = new FileOutputStream(fileCod);
                //escribiendo.write(buffer);
                escribiendo.write(buffer.getBufer());
                escribiendo.flush();
                escribiendo.close();

                if (fileCod.exists()) {
                    doc = new File("documento" + Constantes.CONTADOR++ + "." + infoDocumento.getFormato());

                    toolsFile.decodificar(doc.getName());

                    mostarDocumento(new File(toolsFile.getTempPath(), doc.getName()));

                }

            }

        } catch (DW4JDesktopExcepcion e) {
            traza.trace("problemas al buscar el documento", Level.ERROR, e);
            herramientas.error("Problemas buscando el documento", e);
        } catch (SOAPFaultException | SOAPException ex) {
            traza.trace("problemas buscando informacion del archivo", Level.ERROR, ex);
            herramientas.error("Problemas buscando el fisico del documento", ex);
        } catch (FileNotFoundException ex) {
            traza.trace("archivo no encontrado", Level.ERROR, ex);
            herramientas.error("Archivo no encontrado " + fileCod.toString(), ex);
        } catch (IOException ex) {
            traza.trace("problemas escribienfo el archivo", Level.ERROR, ex);
            herramientas.error("Problemas al escribier el archivo", ex);
        }

    }

    @Listen("onClick = #btnAbrir")
    public void getVersion() {

        ToolsFiles toolsFile = new ToolsFiles();
        int version;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String ruta, archivo;
        OutputStream outputStream;
        Bufer buffer;
        File doc, fileCod;

        try {

            if (documento.getContent() != null) {
                documento.setContent(null);
            }

            version = cmbVersion.getSelectedItem().getValue();

            for (InfoDocumento infoDoc : infoDocumentos) {

                if (infoDoc.getVersion() == version) {

                    expediente.setVersionDocumento(infoDoc.getVersion());
                    expediente.setNumeroDocumento(infoDoc.getNumeroDocumento());

                    if (toolsFile.getDirTemporal().exists()) {
                        File[] files = toolsFile.getDirTemporal().listFiles();
                        for (File f : files) {
                            try {
                                boolean del = f.delete();
                                if (del) {
                                    traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                                } else {
                                    traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                                }
                            } catch (Exception e) {
                                traza.trace("problemas al eliminar el archivo " + f.getName(), Level.ERROR, e);
                            }
                        }
                    }

                    if (infoDoc.getFechaVencimiento() != null) {

                        XMLGregorianCalendar xmlCalendar = infoDoc.getFechaVencimiento();
                        GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                        lblFechaVencimiento.setValue("Fecha de Vencimiento: " + sdf.format(rechaVencimiento.getTime()));
                        lblFechaVencimiento.setVisible(true);

                        ruta = infoDoc.getRutaArchivo();
                        archivo = infoDoc.getNombreArchivo();
                        version = infoDoc.getVersion();

                    } else {
                        ruta = infoDoc.getRutaArchivo();
                        archivo = infoDoc.getNombreArchivo();
                        version = infoDoc.getVersion();
                    }

                    traza.trace("ruta a buscar " + ruta, Level.INFO);
                    traza.trace("archivo a buscar " + archivo, Level.INFO);
                    traza.trace("version del documento a mostrar " + version, Level.INFO);

                    //jlMensaje4.setText("Tipo de Documento: "+infoDocumento.getTipoDocumento()+" \"ULTIMA VERSION: "+version+"\"");
                    if (archivo == null) {
                        throw new Exception("Falta información del documento");
                    }
                    //busca el archivo fisico del tipo de documento
                    buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);

                    if (!buffer.isExiste()) {
                        throw new Exception("Problemas al buscar el fisico del documento");
                    }
                    fileCod = new File(toolsFile.getArchivoCodificado());
                    outputStream = new FileOutputStream(fileCod);
                    outputStream.write(buffer.getBufer());
                    outputStream.flush();
                    outputStream.close();

                    doc = new File("documento" + Constantes.CONTADOR++ + "." + infoDoc.getFormato());

                    toolsFile.decodificar(doc.getName());

                    mostarDocumento(new File(toolsFile.getTempPath(), doc.getName()));
                }
            }
        } catch (SOAPException | SOAPFaultException ex) {
            herramientas.error("Problemas con los Servicios Web", ex);
            traza.trace("problemas webswervice", Level.ERROR, ex);
        } catch (FileNotFoundException ex) {
            herramientas.error("Archivo no encontrado", ex);
            traza.trace("problemas archivo no encontrado", Level.ERROR, ex);
        } catch (IOException ex) {
            herramientas.error("Problemas al crear el documento", ex);
            traza.trace("problemas i/o", Level.ERROR, ex);
        } catch (Exception ex) {
            herramientas.error("Problema general al visualizar la version del documento", ex);
            traza.trace("problemas general", Level.ERROR, ex);
        }
    }

    private void mostarDocumento(File file) {

        ToolsFiles toolsFile = new ToolsFiles();
        AMedia media;
        ByteArrayInputStream is;
        FileInputStream fs;
        byte[] buffer;
        String formato;

        try {

            if (session != null) {

                formato = file.getName().substring(file.getName().indexOf(".") + 1);

                if (formato.equalsIgnoreCase("pdf")) {

                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else if ((formato.equalsIgnoreCase("jpg")) || (formato.equalsIgnoreCase("jpge"))) {

                    file = toolsFile.guardarArchivoJPGtoPDF(file);
                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else if ((formato.equalsIgnoreCase("tif")) || (formato.equalsIgnoreCase("tiff"))) {

                    file = toolsFile.guardarArchivoTIFFtoPDF(file);
                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else {
                    herramientas.warn("Hubo un problema en la conversión del documento");
                }

                btnRechazar.setDisabled(false);
                btnAprobar.setDisabled(false);

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (Exception e) {
            traza.trace("problemas al mostrar el documento", Level.ERROR, e);
        }
    }

    @Listen("onClick = #btnAprobar")
    public void aprobarDocumento() {

        NodoArbol<DatoNodoArbol> nodoArbol;
        DatoNodoArbol dna;
        boolean aprobado;
        String user;

        try {
            if (session != null) {

                user = session.getAttribute("login").toString();

                nodoArbol = arbolDoc.getSelectedItem().getValue();
                dna = nodoArbol.getData();

                if (nodoArbol.isLeaf()) {

                    int n = Messagebox.show("Seguro que desea Aprobar el documento " + dna.getTipoDocumento(),
                            "¿?", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                }
                            });

                    if (n == Messagebox.YES) {
                        aprobado = new ve.com.develcom.aprueba.CalidadDocumento().aprobarDoc(dna.getIdInfoDocumento(), user);

                        traza.trace("exito aprobado " + aprobado, Level.INFO);

                        if (aprobado) {
                            nodoArbol.removeFromParent();
                            documento.setContent(null);
                        }
                    }
                }
            }
        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("error al apobrar el documento", Level.ERROR, e);
            herramientas.error("Problemas al aprobar el documento", e);
        }
    }

    @Listen("onClick = #btnRechazar")
    public void rechazar() {

        Treeitem item;
        NodoArbol<DatoNodoArbol> nodoArbol;
        DatoNodoArbol dna;

        if (session != null) {

            nodoArbol = arbolDoc.getSelectedItem().getValue();
            dna = nodoArbol.getData();

            item = arbolDoc.getSelectedItem();

            if (nodoArbol.isLeaf()) {

                expediente.setIdInfoDocumento(dna.getIdInfoDocumento());
                session.setAttribute("expediente", expediente);

                Window window = herramientas.crearVentanaModal("/rechazaDocumento.zul");
                window.doModal();

                session = herramientas.crearSesion();
                expediente = (Expediente) session.getAttribute("expediente");

                if (expediente.isResultado()) {

                    nodoArbol.removeFromParent();
                    documento.setContent(null);

                }
            }
        }
    }

    /**
     * Genera la foliatura del Expediente
     */
    @Listen("onClick = #btnFoliatura")
    public void crearFoliatura() {

        boolean imprimir;
        HashMap folio = new HashMap();
        ProcesaReporte pr = new ProcesaReporte();

        try {

            int idLibreria = expediente.getIdLibreria();
            int idCategoria = expediente.getIdCategoria();
            String idExpediente = expediente.getIdExpediente();

            traza.trace("imprimiendo la foliatura", Level.INFO);
            traza.trace("idLibreria: " + idLibreria, Level.INFO);
            traza.trace("idCategoria: " + idCategoria, Level.INFO);
            traza.trace("idExpediente: " + idExpediente, Level.INFO);

            imprimir = new ve.com.develcom.foliatura.Foliatura().armarFoliatura(idExpediente, idLibreria, idCategoria);

            traza.trace("respuesta al armar la foliatura " + imprimir, Level.INFO);

            if (imprimir) {
                folio.put("idLib", idLibreria);
                folio.put("idCat", idCategoria);
                folio.put("idExpediente", idExpediente);
                pr.crearReporte("foliatura.jrxml", folio, "Indice de Foliatura");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas creando la foliatura", Level.ERROR, ex);
            herramientas.error("Problemas creando la Foliatura", ex);
        }
    }

    private String crearTituloExpediente() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String titulo = "", tmp = "";
        int cont = 0;
        List<Indice> indicess;

        try {

            indicess = new BuscaIndice().buscaDatosIndice(expediente.getIdExpediente(), expediente.getIdLibreria(), expediente.getIdCategoria());

            for (Indice ind : indicess) {

                traza.trace("indice " + ind.getIndice() + " valor " + ind.getValor(), Level.INFO);

                if (ind.getClave().equalsIgnoreCase("y")) {

                    if (ind.getTipo().equalsIgnoreCase("FECHA")) {

                        tmp = "Expediente: " + ind.getValor().toString() + " -";

                    } else {
                        tmp = "Expediente: " + ind.getValor().toString() + " -";
                    }

                } else if (ind.getClave().equalsIgnoreCase("s")) {

                    if (ind.getTipo().equalsIgnoreCase("FECHA")) {

                        tmp = tmp + " " + ind.getValor().toString();

                    } else {
                        tmp = tmp + " " + ind.getValor().toString();
                    }

                    cont++;
                }
                if (cont == 2) {
                    titulo = tmp;
                    break;
                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al crear el titulo", Level.ERROR, ex);
            herramientas.error("Problemas para crear el titulo del Expediente", ex);
        }

        traza.trace("titulo " + titulo, Level.INFO);
        return titulo;
    }

}
