/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.Indices;
import com.develcom.administracion.Libreria;
import com.develcom.excepcion.DW4JDesktopExcepcion;
import com.develcom.paginas.menu.MenuPrincipal;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;

/**
 *
 * @author develcom
 */
public class AdministraIndice extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibrerias;

    @Wire
    private Combobox cmbCategorias;

    @Wire
    private Listbox lstIndicesExistentes;

    @Wire
    private Caption capTituloIndicesexistentes;

    @Wire
    private Textbox txtNombreIndice;

    @Wire
    private Checkbox chkObligatorio;

    @Wire
    private Combobox cmbTipoDato;

    @Wire
    private Radio rdPrincipal;

    @Wire
    private Radio rd2do;

    @Wire
    private Radio rd3ro;

    @Wire
    private Radio rd4to;

    @Wire
    private Listbox lstIndicesNuevos;

    @Wire
    private Button btnGuardar;

    private static final long serialVersionUID = 2945096328114459936L;
    private static int libreria, categoria;
    private boolean existe = false;
    private List<Indices> indicesExistentes;
    private List<Categoria> listaCategorias;
    private List<Libreria> listaLibrerias;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraIndice.class);
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }

    }

    private void llenarLibreria() {

        Comboitem item;

        try {

            cmbLibrerias.getItems().clear();

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            item = new Comboitem();
            item.setValue("");
            item.setLabel("");
            item.setParent(cmbLibrerias);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {

                    item = new Comboitem();
                    item.setValue(lib.getIdLibreria());
                    item.setLabel(lib.getDescripcion());
                    item.setParent(cmbLibrerias);

                }
            } else {
                herramientas.warn("Problemas al buscar las Librerias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onSelect = #cmbLibrerias")
    public void llenarCategoria() {

        Comboitem item;
        int idLibreria;

        try {

            cmbCategorias.getItems().clear();

            idLibreria = cmbLibrerias.getSelectedItem().getValue();
            listaCategorias = new AdministracionBusqueda().buscarCategoria("", idLibreria, 0);

            item = new Comboitem();
            item.setValue("");
            item.setLabel("");
            item.setParent(cmbCategorias);

            if (!listaCategorias.isEmpty()) {
                for (Categoria cat : listaCategorias) {
                    if (cat.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(cat.getIdCategoria());
                        item.setLabel(cat.getCategoria());
                        item.setParent(cmbCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Categorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onSelect = #cmbCategorias")
    public void llenarTablaIndicesExistentes() {

        int idCategoria;
        String categoriaSel = "";
        int cont = 1;
        Listitem item;
        Listcell cell;

        try {

            lstIndicesExistentes.getItems().clear();

            idCategoria = cmbCategorias.getSelectedItem().getValue();
            categoriaSel = cmbCategorias.getSelectedItem().getLabel();

            capTituloIndicesexistentes.setLabel("Indices existentes " + categoriaSel);

            indicesExistentes = new AdministracionBusqueda().buscandoIndices(idCategoria);

            if (!indicesExistentes.isEmpty()) {

                btnGuardar.setLabel("Modificar");
                existe = true;
                rdPrincipal.setDisabled(true);
                rd2do.setDisabled(true);
                rd3ro.setDisabled(true);
                rd4to.setDisabled(true);
                chkObligatorio.setDisabled(true);

                for (Indices ind : indicesExistentes) {

                    item = new Listitem();

                    cell = new Listcell(ind.getIndice());
                    item.appendChild(cell);

                    try {
                        if (ind.getClave().equalsIgnoreCase("o")) {

                            cell = new Listcell("");
                            item.appendChild(cell);

                            cell = new Listcell("Si");
                            item.appendChild(cell);

                            cell = new Listcell(ind.getTipo());
                            item.appendChild(cell);

                        } else if (ind.getClave().equalsIgnoreCase("s")) {

                            cell = new Listcell("clave " + ++cont);
                            item.appendChild(cell);

                            cell = new Listcell("Si");
                            item.appendChild(cell);

                            cell = new Listcell(ind.getTipo());
                            item.appendChild(cell);

                        } else if (ind.getClave().equalsIgnoreCase("y")) {

                            cell = new Listcell("Principal");
                            item.appendChild(cell);

                            cell = new Listcell("Si");
                            item.appendChild(cell);

                            cell = new Listcell(ind.getTipo());
                            item.appendChild(cell);

                        } else {

                            cell = new Listcell("");
                            item.appendChild(cell);

                            cell = new Listcell("No");
                            item.appendChild(cell);

                            cell = new Listcell(ind.getTipo());
                            item.appendChild(cell);
                        }

                    } catch (NullPointerException e) {
                    }

                    item.setParent(lstIndicesExistentes);
                }

            } else {
                existe = false;
                rdPrincipal.setDisabled(false);
                rd2do.setDisabled(false);
                rd3ro.setDisabled(false);
                rd4to.setDisabled(false);
                chkObligatorio.setDisabled(false);
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problema al buscar los indices de la categoria " + categoriaSel, Level.ERROR, ex);
            herramientas.error("Problema al buscar los indices de la categoria " + categoriaSel, ex);
        }
    }

    @Listen("onClick = #btnAgregar")
    public void agregarIndicesNuevos() {

        String indiceNuevo, tipo, lib, cat;
        boolean banLista = true, banTabla = true;
        List<Listitem> indicesNuevos = new ArrayList<>();
        List<Listcell> celdasNuevos = new ArrayList<>();
        Listitem item;
        Listcell cell;

        try {

            libreria = cmbLibrerias.getSelectedItem().getValue();
            categoria = cmbCategorias.getSelectedItem().getValue();
            lib = cmbLibrerias.getSelectedItem().getLabel();
            cat = cmbCategorias.getSelectedItem().getLabel();
            tipo = cmbTipoDato.getSelectedItem().getLabel();
            indiceNuevo = txtNombreIndice.getValue();

            if (!lib.equalsIgnoreCase("")) {
                if (!cat.equalsIgnoreCase("")) {
                    if (!tipo.equalsIgnoreCase("")) {
                        if (!indiceNuevo.equalsIgnoreCase("")) {

                            item = new Listitem();

                            cell = new Listcell(indiceNuevo);
                            item.appendChild(cell);

                            if (!existe) {
                                indicesNuevos = lstIndicesNuevos.getItems();

                                if (!indicesNuevos.isEmpty()) {
                                    if (indicesNuevos.size() < 4) {
                                        for (int i = 0; i < indicesNuevos.size(); i++) {
//                                        for (Listitem litem : indicesNuevos) {
                                            Listitem fila = indicesNuevos.get(i);
                                            Listcell idIndices = (Listcell) fila.getChildren().get(1);

                                            if (!idIndices.getLabel().equalsIgnoreCase(rdPrincipal.getLabel())) {
                                                if (!rdPrincipal.isSelected()) {
                                                    throw new DW4JDesktopExcepcion("Debe seleccionar el id principal primero");
                                                } else {
                                                    cell = new Listcell(rdPrincipal.getLabel());
                                                    item.appendChild(cell);
                                                    rdPrincipal.setDisabled(true);
                                                    chkObligatorio.setChecked(true);
                                                }
                                            } else if (idIndices.getLabel().equalsIgnoreCase(rdPrincipal.getLabel())) {

                                                if (indicesNuevos.size() != 1) {
                                                    fila = indicesNuevos.get(++i);
                                                    idIndices = (Listcell) fila.getChildren().get(1);
                                                }

                                                if (!idIndices.getLabel().equalsIgnoreCase(rd2do.getLabel())) {
                                                    if (!rd2do.isSelected()) {
                                                        throw new DW4JDesktopExcepcion("Debe seleccionar el segundo id despues del principal");
                                                    } else {
                                                        cell = new Listcell(rd2do.getLabel());
                                                        item.appendChild(cell);
                                                        rd2do.setDisabled(true);
                                                        chkObligatorio.setChecked(true);
                                                    }
                                                } else if (idIndices.getLabel().equalsIgnoreCase(rd2do.getLabel())) {

                                                    if (indicesNuevos.size() != 2) {
                                                        fila = indicesNuevos.get(++i);
                                                        idIndices = (Listcell) fila.getChildren().get(1);
                                                    }

                                                    if (!idIndices.getLabel().equalsIgnoreCase(rd3ro.getLabel())) {
                                                        if (!rd3ro.isSelected()) {
                                                            throw new DW4JDesktopExcepcion("Debe seleccionar el tercero id despues del segundo");
                                                        } else {
                                                            cell = new Listcell(rd3ro.getLabel());
                                                            item.appendChild(cell);
                                                            rd3ro.setDisabled(true);
                                                            chkObligatorio.setChecked(true);
                                                        }
                                                    } else if (idIndices.getLabel().equalsIgnoreCase(rd3ro.getLabel())) {
                                                        if (indicesNuevos.size() != 3) {
                                                            fila = indicesNuevos.get(++i);
                                                            idIndices = (Listcell) fila.getChildren().get(1);
                                                        }

                                                        if (!idIndices.getLabel().equalsIgnoreCase(rd4to.getLabel())) {
                                                            if (!rd4to.isSelected()) {
                                                                throw new DW4JDesktopExcepcion("Debe seleccionar el cuarto id despues del tercero");
                                                            } else {
                                                                cell = new Listcell(rd4to.getLabel());
                                                                item.appendChild(cell);
                                                                rd4to.setDisabled(true);
                                                                chkObligatorio.setChecked(true);
                                                            }
                                                        } else if (idIndices.getLabel().equalsIgnoreCase(rd4to.getLabel())) {
                                                            break;
                                                        }
                                                    }
                                                }

                                            } else {
                                                break;
                                            }
                                        }
                                    } else {
                                        cell = new Listcell("");
                                        item.appendChild(cell);
                                    }
                                } else {
                                    if (!rdPrincipal.isSelected()) {
                                        throw new DW4JDesktopExcepcion("Debe seleccionar el id principal primero");
                                    } else {
                                        cell = new Listcell(rdPrincipal.getLabel());
                                        item.appendChild(cell);
                                        rdPrincipal.setDisabled(true);
                                        chkObligatorio.setChecked(true);
                                    }
                                }
                            } else {
                                cell = new Listcell("adicional");
                                item.appendChild(cell);
                            }

                            if (chkObligatorio.isChecked()) {
                                cell = new Listcell("Si");
                                item.appendChild(cell);
                                chkObligatorio.setChecked(false);
                            } else {
                                cell = new Listcell("No");
                                item.appendChild(cell);
                                chkObligatorio.setChecked(false);
                            }

                            cell = new Listcell(tipo);
                            item.appendChild(cell);

                            for (Indices arg : indicesExistentes) {
                                String argu = arg.getIndice();
                                if (argu.equalsIgnoreCase(indiceNuevo)) {
                                    banLista = false;
                                    break;
                                } else {
                                    banLista = true;
                                }
                            }

                            indicesNuevos = lstIndicesNuevos.getItems();
                            for (Listitem libex : indicesNuevos) {
                                List<Listcell> cells = libex.getChildren();
                                String tmp = cells.get(0).getLabel();
                                if (tmp.equalsIgnoreCase(indiceNuevo)) {
                                    banTabla = false;
                                    break;
                                }
                            }

                            if (banTabla) {
                                if (banLista) {
                                    item.setParent(lstIndicesNuevos);
                                    txtNombreIndice.setText("");
                                    cmbTipoDato.setText("");
                                }
                            } else {
                                herramientas.warn("Ya el indice se encuentra en la tabla");
                            }

                        } else {
                            herramientas.warn("Debe colocar un nombre de Indice");
                        }
                    } else {
                        herramientas.warn("Debe seleccionar un tipo de dato");
                    }
                } else {
                    herramientas.warn("Debe seleccionar una Categoria");
                }
            } else {
                herramientas.warn("Debe selecionar una Libreria");
            }
        } catch (DW4JDesktopExcepcion e) {
            traza.trace("", Level.ERROR, e);
            herramientas.warn(e.getMessage());
        } catch (Exception e) {
            traza.trace("problemas general al agregar el indice", Level.ERROR, e);
            herramientas.error("Problemas general al agregar el Indice", e);
        }

    }

    @Listen("onClick = #btnGuardar")
    public void guardarIndices() {

        int idCategoria = 0, idLibreria = 0;
        List<Indices> listaIndices = new ArrayList<Indices>();
        Indices argumento;
        boolean resp = false;
        String principal = "";
        List<Listitem> items;
        List<Listcell> cells;

        try {

            items = lstIndicesExistentes.getItems();
            for (Listitem item : items) {
                cells = item.getChildren();
                if (cells.get(1).getLabel().equalsIgnoreCase("principal")) {
                    principal = rdPrincipal.getLabel();
                    break;
                }
            }

            int n = Messagebox.show("Seguro que desea guardar los Indices",
                    "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event evt) throws Exception {
                        }

                    });

            if (n == Messagebox.YES) {

                items.clear();

                items = lstIndicesNuevos.getItems();

                if (!principal.equalsIgnoreCase(rdPrincipal.getLabel())) {
                    Listitem item = items.get(0);
                    Listcell cell = (Listcell) item.getChildren().get(1);
                    principal = cell.getLabel();
                }

                if (!items.isEmpty()) {
                    if (principal.equalsIgnoreCase(rdPrincipal.getLabel())) {

                        idLibreria = libreria;
                        idCategoria = categoria;
                        for (Listitem item : items) {
                            cells = item.getChildren();

                            argumento = new Indices();

                            argumento.setIndice(cells.get(0).getLabel());
                            argumento.setIdCategoria(idCategoria);

                            if (cells.get(1).getLabel().equalsIgnoreCase(rdPrincipal.getLabel())) {
                                argumento.setClave("Y");
                            } else if (cells.get(1).getLabel().equalsIgnoreCase(rd2do.getLabel())) {
                                argumento.setClave("S");
                            } else if (cells.get(1).getLabel().equalsIgnoreCase(rd3ro.getLabel())) {
                                argumento.setClave("S");
                            } else if (cells.get(1).getLabel().equalsIgnoreCase(rd4to.getLabel())) {
                                argumento.setClave("S");
                            } else if (cells.get(2).getLabel().equalsIgnoreCase("si")) {
                                argumento.setClave("O");
                            } else {
                                argumento.setClave("");
                            }

                            argumento.setTipo(cells.get(3).getLabel());

                            listaIndices.add(argumento);
                        }

                        resp = new AdministracionAgregar().guardarIndices(listaIndices);

                        if (resp) {
                            herramientas.info("Indices agregados con exito");
                        } else {
                            herramientas.warn("Problemas al agregar los Indices");
                        }
                        limpiarComponentes();

                    } else {
                        herramientas.warn("La tabla debe tener el indice principal");
                    }
                } else {
                    herramientas.warn("La tabla de Indices esta vacia \n no se pueder guardar");
                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al agregar los indices", Level.ERROR, ex);
            herramientas.error("Problemas al agregar los Indices", ex);
        }

    }

    private void limpiarComponentes() {
        txtNombreIndice.setText("");
        cmbCategorias.setText("");
        cmbLibrerias.setText("");
        chkObligatorio.setChecked(false);
        rdPrincipal.setSelected(false);
        rd2do.setSelected(false);
        rd3ro.setSelected(false);
        rd4to.setSelected(false);
        lstIndicesExistentes.getItems().clear();
        lstIndicesNuevos.getItems().clear();
    }

}
