/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.reportes;

import com.develcom.dao.Expediente;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import org.apache.log4j.Level;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

/**
 *
 * @author develcom
 */
public class Foliatura extends SelectorComposer<Component> {

    @Wire
    private Iframe foliatura;
    
    @Wire
    private Window winFoliatura;

    private static final long serialVersionUID = 238187118789516672L;
    private Traza traza = new Traza(Foliatura.class);
    private Herramientas herramientas = new Herramientas();
    private Session session;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {
        AMedia media;
        ByteArrayInputStream is;
        FileInputStream fs;
        File archivo;
        byte[] buffer;
        String nombrePDF;
        int index;

        try {

            session = herramientas.crearSesion();

            if (session != null) {
                
                expediente = (Expediente) session.getAttribute("expediente");
                
                winFoliatura.setTitle("Foliatura del Expediente: "+expediente.getIdExpediente());
                
                archivo = (File) session.getAttribute("reporte");
                
                index = archivo.getName().indexOf(".");
                nombrePDF = archivo.getName().substring(0, index-1);
                
                buffer = new byte[(int) archivo.length()];
                fs = new FileInputStream(archivo);
                fs.read(buffer);
                fs.close();
                is = new ByteArrayInputStream(buffer);

                media = new AMedia(nombrePDF, "pdf", "application/pdf", is);
                foliatura.setContent(media);
            }
            
        } catch (Exception e) {
            traza.trace("problemas al mostrar la foliatura", Level.ERROR, e);
            herramientas.error("Problemas al mostrar la Foliatura", e);
        }
    }

    @Listen("onClick = #btnCerrar")
    public void cerrar(){
        winFoliatura.setVisible(false);
        winFoliatura.detach();
    }
}
