/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.indice;

import com.develcom.administracion.Combo;
import com.develcom.administracion.Indices;
import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.dao.Expediente;
import com.develcom.expediente.ConsultaDinamica;
import com.develcom.expediente.Indice;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import ve.com.develcom.busquedadinamica.BuscaExpedienteDinamico;
import ve.com.develcom.expediente.BuscaIndice;
import ve.com.develcom.expediente.LLenarListaDesplegable;

/**
 *
 * @author develcom
 */
public class ConsultaIndice extends SelectorComposer<Component> {

    @Wire
    private Grid panelIndice;
    
    @Wire
    private Label lblLibreria;
    
    @Wire
    private Label lblCategoria;

    private List<Indice> lstIndices = new ArrayList<>();
    private static final long serialVersionUID = 8994410596621742476L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(ConsultaIndice.class);
    private Session session;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    public void iniciar() {
        
        session = herramientas.crearSesion();
        
        if(session != null){
            
            expediente = (Expediente) session.getAttribute("expediente");
            
            lblLibreria.setValue("Libreria: "+expediente.getLibreria());
            lblCategoria.setValue("Categoria: "+expediente.getCategoria());
            
            buscarIndces();
            
        }

    }

    private void buscarIndces() {

        Rows rows;
        Row row = null;
        List<Indices> listaIndices;
        Label label;
        Textbox textbox;
        Combobox combobox;
        int cont = 2;

        try {

            listaIndices = new BuscaIndice().buscarIndice(expediente.getIdCategoria());
            convertirIndices(listaIndices);

            rows = panelIndice.getRows();

            for (Indice ind : lstIndices) {

                if (cont == 2) {
                    cont = 0;
                    row = new Row();
                }

                if (ind.getTipo().equalsIgnoreCase("NUMERO")) {

                    label = new Label(ind.getIndice());

                    textbox = new Textbox();
                    textbox.setId(ind.getIndice().replace(" ", "_"));

                    row.appendChild(label);
                    row.appendChild(textbox);

                    rows.appendChild(row);

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("TEXTO")) {

                    label = new Label(ind.getIndice());

                    textbox = new Textbox();
                    textbox.setId(ind.getIndice().replace(" ", "_"));

                    row.appendChild(label);
                    row.appendChild(textbox);

                    rows.appendChild(row);

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("AREA")) {

                    label = new Label(ind.getIndice());

                    textbox = new Textbox();
                    textbox.setId(ind.getIndice().replace(" ", "_"));
                    textbox.setRows(6);

                    row.appendChild(label);
                    row.appendChild(textbox);

                    rows.appendChild(row);

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("COMBO")) {
                    Comboitem item;

                    label = new Label(ind.getIndice());

                    combobox = new Combobox();
                    combobox.setId(ind.getIndice().replace(" ", "_"));

                    List<Combo> datosCombo = new LLenarListaDesplegable().buscarData(ind.getCodigo(), false);

                    for (Combo combo : datosCombo) {
                        item = new Comboitem();
                        item.setValue(combo.getIdCombo());
                        item.setLabel(combo.getDatoCombo());
                        item.setParent(combobox);
                    }

                    row.appendChild(label);
                    row.appendChild(combobox);

                    rows.appendChild(row);

                    cont++;

                } else if (ind.getTipo().equalsIgnoreCase("FECHA")) {

                    Label labelDesde = new Label(ind.getIndice().replace(" ", "_") + "_desde");
                    Datebox dateboxDesde = new Datebox();
                    dateboxDesde.setId(ind.getIndice().replace(" ", "_") + "_desde");
                    dateboxDesde.setFormat("dd/MM/yyyy");

                    row.appendChild(labelDesde);
                    row.appendChild(dateboxDesde);

                    rows.appendChild(row);

                    cont++;

                    if (cont == 2) {
                        cont = 0;
                        row = new Row();
                    }

                    Label labelHasta = new Label(ind.getIndice().replace(" ", "_") + "_hasta");
                    Datebox dateboxHasta = new Datebox();
                    dateboxHasta.setId(ind.getIndice().replace(" ", "_") + "_hasta");
                    dateboxHasta.setFormat("dd/MM/yyyy");

                    row.appendChild(labelHasta);
                    row.appendChild(dateboxHasta);

                    rows.appendChild(row);

                    cont++;
                }

            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al buscar los indices", Level.ERROR, ex);
        }
    }

    private void convertirIndices(List<Indices> listaIndices) {

        Indice indice;

        for (Indices indices : listaIndices) {
            indice = new Indice();

            indice.setClave(indices.getClave());
            indice.setCodigo(indices.getCodigo());
            indice.setIdCategoria(indices.getIdCategoria());
            indice.setIdIndice(indices.getIdIndice());
            indice.setIndice(indices.getIndice());
            indice.setTipo(indices.getTipo());
            lstIndices.add(indice);
        }

    }

    @Listen("onClick = #consulta")
    public void consultar() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<ConsultaDinamica> consultaDinamicas;
        List<TipoDocumento> listaTipoDoc = new ArrayList<>();
        List<SubCategoria> listaSubCat = new ArrayList<>();
        Indice indice;
        List<Indice> listaIndices = new ArrayList<>();

        try {
            
            
            if (session != null) {

                for (Indice ind : lstIndices) {

                    if (panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_")) instanceof Textbox) {

                        Textbox textbox = (Textbox) panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_"));

                        traza.trace("argumento " + ind.getIndice() + " valor " + textbox.getValue(), Level.INFO);

                        if (textbox.getValue() != null || !textbox.getValue().equalsIgnoreCase("")) {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            indice.setValor(textbox.getValue());
                            listaIndices.add(indice);

                        } else {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            listaIndices.add(indice);

                        }

                    } else if (panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_")) instanceof Combobox) {

                        Combobox combobox = (Combobox) panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_"));

                        traza.trace("argumento " + ind.getIndice() + " valor " + combobox.getValue(), Level.INFO);

                        if (combobox.getValue() != null || !combobox.getValue().equalsIgnoreCase("")) {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            indice.setValor(combobox.getValue());
                            listaIndices.add(indice);

                        } else {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            listaIndices.add(indice);

                        }

                    } else if (panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_desde") instanceof Datebox) {

                        Datebox datebox = (Datebox) panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_desde");

                        traza.trace("argumento " + ind.getIndice() + " valor " + datebox.getValue(), Level.INFO);

                        if (datebox.getValue() != null) {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            indice.setValor(sdf.format(datebox.getValue()));
                            listaIndices.add(indice);

                        } else {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            listaIndices.add(indice);
                        }

                    } else if (panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_hasta") instanceof Datebox) {

                        Datebox input = (Datebox) panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_hasta");

                        traza.trace("argumento " + ind.getIndice() + " valor " + input.getValue(), Level.INFO);

                        if (input.getValue() != null) {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            indice.setValor(sdf.format(input.getValue()));
                            listaIndices.add(indice);

                        } else {

                            indice = new Indice();
                            indice.setIdCategoria(ind.getIdCategoria());
                            indice.setIdIndice(ind.getIdIndice());
                            indice.setIndice(ind.getIndice());
                            indice.setClave(ind.getClave());
                            indice.setTipo(ind.getTipo());
                            listaIndices.add(indice);
                        }
                    }
                }

                if (comprobarFechas()) {
                    
                    consultaDinamicas = new BuscaExpedienteDinamico().consultarExpedienteDinamico(listaIndices, convertirSubCategorias(listaSubCat), convertirTipoDocumento(listaTipoDoc), 1, expediente.getIdLibreria());

                    if (!consultaDinamicas.isEmpty()) {
                        if (consultaDinamicas.get(0).isExiste()) {

                            expediente.setTipoDocumentos(listaTipoDoc);
                            expediente.setSubCategorias(listaSubCat);

                            session.setAttribute("consultaDinamicas", consultaDinamicas);
                            session.setAttribute("expediente", expediente);
                            session.setAttribute("tamañoConsulta", consultaDinamicas.size());

                            traza.trace("tamaño de la consulta " + consultaDinamicas.size(), Level.INFO);
                            traza.trace("exito al en la consulta dinamica ", Level.INFO);

                            herramientas.navegarPagina("resultado.zul");
                        } else {
                            herramientas.warn("Debe indicar algún Índice de Búsqueda");
                        }
                    } else {
                        herramientas.warn("No se encontraron expedientes asociados a la búsqueda ingresada");
                    }
                } else {
                    herramientas.warn("Verifique los rangos de las fechas entrantes y salientes");
                }
            } else {
                herramientas.navegarPagina("index.zul");
            }
        } catch (WrongValueException | NumberFormatException | SOAPException | SOAPFaultException e) {
            herramientas.error("", e);
            traza.trace("error al realizar la consulta", Level.ERROR, e);
        }

        //return null;
    }

    private List<com.develcom.expediente.SubCategoria> convertirSubCategorias(List<SubCategoria> listSubCat) {
        List<com.develcom.expediente.SubCategoria> scs = new ArrayList<>();
        com.develcom.expediente.SubCategoria sc;
        for (SubCategoria subCa : listSubCat) {
            sc = new com.develcom.expediente.SubCategoria();
            sc.setEstatus(subCa.getEstatus());
            sc.setIdCategoria(subCa.getIdCategoria());
            sc.setIdSubCategoria(subCa.getIdSubCategoria());
            sc.setSubCategoria(subCa.getSubCategoria());
            scs.add(sc);
        }
        return scs;
    }

    private List<com.develcom.expediente.TipoDocumento> convertirTipoDocumento(List<TipoDocumento> documentos) {
        List<com.develcom.expediente.TipoDocumento> tds = new ArrayList<>();
        com.develcom.expediente.TipoDocumento td;

        for (TipoDocumento tp : documentos) {
            td = new com.develcom.expediente.TipoDocumento();
            td.setDatoAdicional(tp.getDatoAdicional());
            td.setEstatus(tp.getEstatus());
            td.setFicha(tp.getFicha());
            td.setIdCategoria(tp.getIdCategoria());
            td.setIdSubCategoria(tp.getIdSubCategoria());
            td.setIdTipoDocumento(tp.getIdTipoDocumento());
            td.setTipoDocumento(tp.getTipoDocumento());
            td.setVencimiento(tp.getVencimiento());
            tds.add(td);
        }

        return tds;
    }

    private boolean comprobarFechas() {
        boolean resp = true;
        Date fechDesde = null, fechHasta = null;
        String etiDesde = null, etiHasta = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        for (Indice ind : lstIndices) {

            if (panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_desde") instanceof Datebox) {

                Datebox input = (Datebox) panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_desde");
                etiDesde = input.getId().replace("_", " ");
                fechDesde = input.getValue();

            } else if (panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_hasta") instanceof Datebox) {

                Datebox input = (Datebox) panelIndice.getFellowIfAny(ind.getIndice().replace(" ", "_") + "_hasta");
                etiHasta = input.getId().replace("_", " ");
                fechHasta = input.getValue();

            }

        }

        if (fechHasta != null) {
            if (fechDesde != null) {
                if (fechDesde.after(fechHasta)) {
                    resp = false;
                    herramientas.warn("problema de fechas");
                    herramientas.warn(etiDesde + " - " + sdf.format(fechDesde));
                    herramientas.warn(etiHasta + " - " + sdf.format(fechHasta));
                    traza.trace("problema de fechas", Level.INFO);
                    traza.trace(etiDesde + " - " + sdf.format(fechDesde.getTime()), Level.INFO);
                    traza.trace(etiHasta + " - " + sdf.format(fechHasta.getTime()), Level.INFO);
                }
            }
        }

        return resp;
    }

}
