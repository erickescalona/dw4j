/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * VerImagenes.java
 *
 * Created on 10-sep-2012, 13:03:52
 */

package com.develcom.verdocumento;

import com.develcom.documento.Bufer;
import com.develcom.documento.InfoDocumento;
import com.develcom.dto.Expediente;
import com.develcom.dto.ManejoSesion;
import com.develcom.expediente.Indice;
import com.develcom.log.Traza;
import com.develcom.tools.Constantes;
import com.develcom.tools.ToolsFile;
import com.develcom.tools.VerificaPerfil;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import ve.com.develcom.archivo.GestionArchivos;

/**
 *
 * @author develcom
 */
public class VerImagenes extends javax.swing.JFrame {


    /**
     * Total de paginas digitalizadas
     */
    private int cantidadPaginas=0;
    /**
     * lista de indices dinamicos
     */
    private List<Indice> indices = new ArrayList<Indice>();
    /**
     * accion a tomar (versionar o reemplazar)
     */
//    private String accion;
    /**
     * objecto con la informacion del
     * documento
     */
    private InfoDocumento infoDocumento;
    private List<InfoDocumento> infoDocumentos;
    private Expediente expediente;
    private Traza traza = new Traza(VerImagenes.class);

    private String nombreDocumento;

    private int versionSeleccionada;

    private Image imagen;
    private ToolsFile toolsTiff = new ToolsFile();


    /** Creates new form VerImagenes */
    public VerImagenes(InfoDocumento infoDocumento) {
        this.expediente=ManejoSesion.getExpediente();
        this.infoDocumento=infoDocumento;
//        this.accion=accion;

        iniciar();
    }



    private void iniciar(){

        initComponents();

        jlTipoDocumento.setText(expediente.getTipoDocumento());
        jlLibreria.setText(expediente.getLibreria());
        jlCategoria.setText(expediente.getCategoria());
        jlSubCategoria.setText(expediente.getSubCategoria());
        //traza.trace("accion "+accion, Level.INFO);

        jlFechaVencimiento.setVisible(false);

        zommOut.setVisible(false);
        zommIn.setVisible(false);


        setLocationRelativeTo(null);

        new VerificaPerfil(this).verificarPerfil();
        visualizar();



        nombreDocumento = expediente.getTipoDocumento();
        setTitle(nombreDocumento);
        traza.trace("nombre del documento "+nombreDocumento, Level.INFO);


        

    }

    private void visualizar(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        String ruta="", archivo="";
        int contDocAprob = 0, contDocPend = 0;
        EjecutaEspera hilo = new EjecutaEspera();
        OutputStream escribiendo;
        Bufer buffer;

        int idInfoDocumento = infoDocumento.getIdInfoDocumento();
        int idDoc = infoDocumento.getIdDocumento();
        int version = infoDocumento.getVersion();
        int numDoc = infoDocumento.getNumeroDocumento();
        int idCat = ManejoSesion.getExpediente().getIdCategoria();
        int idSubCat = ManejoSesion.getExpediente().getIdSubCategoria();
        String idExpediente = ManejoSesion.getExpediente().getIdExpediente();
        traza.trace("buscando el documento idDocumento "+idDoc+" idCategoria "+idCat+" idSubCategoria "+idSubCat+" idExpediente "+idExpediente, Level.INFO);
        
        try {
            
            infoDocumentos = new GestionArchivos().buscarImagenDocumentos(idInfoDocumento, idDoc, idCat, idSubCat, version, numDoc, idExpediente);

            hilo.start();
            if(infoDocumentos.size()>0){

                File fileCod = new File(toolsTiff.getArchivoCodificado());
                if(toolsTiff.getDirTemporal().exists()){
                    File[] files = toolsTiff.getDirTemporal().listFiles();
                    for(File f : files){
                        if(f.delete()){
                            traza.trace("eliminado archivo "+f.getName(), Level.INFO);
                        }else{
                            traza.trace("problemas al eliminar el archivo "+f.getName(), Level.WARN);
                        }
                    }
                }

                for(InfoDocumento id : infoDocumentos){

                    if(id.getFechaVencimiento()!=null){

                        XMLGregorianCalendar xmlCalendar = id.getFechaVencimiento();
                        GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                        jlFechaVencimiento.setText("Fecha de vencimiento: "+sdf.format(rechaVencimiento.getTime()));
                        jlFechaVencimiento.setVisible(true);

                        ruta = id.getRutaArchivo();
                        archivo = id.getNombreArchivo();
                        version = id.getVersion();
                        break;

                    }else{
                        ruta = id.getRutaArchivo();
                        archivo = id.getNombreArchivo();
                        version = id.getVersion();
                        break;
                    }
                }
                traza.trace("ruta a buscar "+ruta, Level.INFO);
                traza.trace("archivo a buscar "+archivo, Level.INFO);
                traza.trace("version del documento a mostrar "+version, Level.INFO);

                //busca el archivo fisico del tipo de documento
                buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);
                
                escribiendo = new FileOutputStream(fileCod);
                escribiendo.write(buffer.getBufer());
                escribiendo.flush();
                escribiendo.close();

                new ToolsFile().decodificar("imagen."+infoDocumento.getFormato());

                imagen = new ImageIcon(toolsTiff.getArchivo()+"imagen."+infoDocumento.getFormato()).getImage();

                panelImagen.setImagen(imagen);

                for(InfoDocumento infoDoc : infoDocumentos){

                    if(Constantes.ACCION.equalsIgnoreCase("CONSULTAR")){

                        if(infoDoc.getEstatusDocumento().equalsIgnoreCase("aprobado")){
                            cboVersion.addItem(infoDoc.getVersion());
                            contDocAprob++;
                        }

                    }else if(Constantes.ACCION.equalsIgnoreCase("APROBAR")){

                        if(infoDoc.getEstatusDocumento().equalsIgnoreCase("pendiente")){
                            cboVersion.addItem(infoDoc.getVersion());
                            contDocPend++;
                        }

                    }else if(Constantes.ACCION.equalsIgnoreCase("CAPTURAR")){
                        cboVersion.addItem(infoDoc.getVersion());
                    }
                }

                if(infoDocumentos.size()==1 || contDocAprob==1 || contDocPend==1){
                    cboVersion.setEnabled(false);
                    btnAbrir.setEnabled(false);
                }
                setVisible(true);

            }

            hilo.interrupt();
            hilo.terminar();
        } catch (SOAPException ex) {
            traza.trace("error al buscar el archivo "+ex.getMessage(), Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();
            hilo.interrupt();
            hilo.terminar();
        } catch (FileNotFoundException ex) {

            traza.trace("Error al buscar el archivo", Level.ERROR, ex);
            this.dispose();
            JOptionPane.showMessageDialog(this, "Error al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            hilo.interrupt();
            hilo.terminar();

        } catch(SOAPFaultException e){
            traza.trace("error al buscar el archivo "+e.getMessage(), Level.ERROR, e);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();
            hilo.interrupt();
            hilo.terminar();
        }catch (Exception ex) {
            traza.trace("Error general al buscar el archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();
            hilo.interrupt();
            hilo.terminar();
        }

    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barra = new javax.swing.JToolBar();
        jPanel3 = new javax.swing.JPanel();
        cancelar = new javax.swing.JButton();
        zommIn = new javax.swing.JButton();
        zommOut = new javax.swing.JButton();
        jbImprimir = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        cboVersion = new javax.swing.JComboBox();
        jlFechaVencimiento = new javax.swing.JLabel();
        jlTipoDocumento = new javax.swing.JLabel();
        jlLibreria = new javax.swing.JLabel();
        jlCategoria = new javax.swing.JLabel();
        jlSubCategoria = new javax.swing.JLabel();
        panelImagen = new com.develcom.tools.PanelImagen();

        setBackground(new java.awt.Color(224, 239, 255));

        barra.setFloatable(false);

        cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/Knob Cancel1.png"))); // NOI18N
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });

        zommIn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/ZoomIn24.gif"))); // NOI18N
        zommIn.setMnemonic('+');
        zommIn.setToolTipText("Aumentar Imagen");
        zommIn.setPreferredSize(new java.awt.Dimension(50, 40));

        zommOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/ZoomOut24.gif"))); // NOI18N
        zommOut.setMnemonic('-');
        zommOut.setToolTipText("Disminuir Imagen");
        zommOut.setPreferredSize(new java.awt.Dimension(50, 40));

        jbImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/printer.png"))); // NOI18N
        jbImprimir.setToolTipText("Imprimir");
        jbImprimir.setFocusable(false);
        jbImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbImprimir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbImprimirActionPerformed(evt);
            }
        });

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/Open24.gif"))); // NOI18N
        btnAbrir.setToolTipText("Abrir");
        btnAbrir.setFocusable(false);
        btnAbrir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAbrir.setPreferredSize(new java.awt.Dimension(59, 25));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });

        cboVersion.setFont(new java.awt.Font("DejaVu Sans", 0, 12)); // NOI18N
        cboVersion.setToolTipText("Seleccione la versión del documento que desea ver");
        cboVersion.setDoubleBuffered(true);
        cboVersion.setName(""); // NOI18N
        cboVersion.setPreferredSize(new java.awt.Dimension(122, 25));
        cboVersion.setVerifyInputWhenFocusTarget(false);

        jlFechaVencimiento.setText("fecha Vencimiento");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jbImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(cboVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAbrir, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlFechaVencimiento, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zommOut, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zommIn, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(103, 103, 103)
                .addComponent(cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(153, 153, 153))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(zommIn, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                        .addComponent(zommOut, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                        .addComponent(jbImprimir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cancelar, 0, 0, Short.MAX_VALUE))
                    .addComponent(btnAbrir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        barra.add(jPanel3);

        jlTipoDocumento.setText("jLabel1");
        jlTipoDocumento.setAutoscrolls(true);

        jlLibreria.setText("jLabel2");
        jlLibreria.setAutoscrolls(true);

        jlCategoria.setText("jLabel2");
        jlCategoria.setAutoscrolls(true);

        jlSubCategoria.setText("jLabel2");
        jlSubCategoria.setAutoscrolls(true);

        javax.swing.GroupLayout panelImagenLayout = new javax.swing.GroupLayout(panelImagen);
        panelImagen.setLayout(panelImagenLayout);
        panelImagenLayout.setHorizontalGroup(
            panelImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelImagenLayout.setVerticalGroup(
            panelImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 584, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(barra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlSubCategoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlCategoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlLibreria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlTipoDocumento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelImagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(barra, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelImagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlTipoDocumento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlLibreria, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlSubCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        
        File fileCod = new File(toolsTiff.getArchivoCodificado());
        if(toolsTiff.getDirTemporal().exists()){
            File[] files = toolsTiff.getDirTemporal().listFiles();
            for(File f : files){
                if(f.delete()){
                    traza.trace("eliminado archivo "+f.getName(), Level.INFO);
                }else{
                    traza.trace("problemas al eliminar el archivo "+f.getName(), Level.WARN);
                }
            }
        }
        this.dispose();
        Runtime.getRuntime().gc();
        Runtime.getRuntime().exit(0);
        
}//GEN-LAST:event_cancelarActionPerformed

    private void jbImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbImprimirActionPerformed
        //acciones.doPrint();
}//GEN-LAST:event_jbImprimirActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed

        versionSeleccionada = Integer.parseInt(cboVersion.getSelectedItem().toString());

        getVersion(versionSeleccionada);
        //version(versionSeleccionada);
}//GEN-LAST:event_btnAbrirActionPerformed


    /**
     * Busca la version seleccionada
     * del documento
     * @param version
     * La version seleccionada
     */
    private void getVersion(int version) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String ruta, archivo;
        //String buffer;
        BufferedReader leer;
        PrintWriter escribir;
        //ToolsFile toolsTiff = new ToolsFile();
        OutputStream escribiendo;
        Bufer buffer;
        //byte[] buffer;

        traza.trace("version a buscar "+version, Level.INFO);

        try{
            for(InfoDocumento infoDoc : infoDocumentos){
                if(infoDoc.getVersion()==version){

                    File fileCod = new File(toolsTiff.getArchivoCodificado());
                    if(toolsTiff.getDirTemporal().exists()){
                        File[] files = toolsTiff.getDirTemporal().listFiles();
                        for(File f : files){
                            if(f.delete()){
                                traza.trace("eliminado archivo "+f.getName(), Level.INFO);
                            }else{
                                traza.trace("problemas al eliminar el archivo "+f.getName(), Level.WARN);
                            }
                        }
                    }


                    if(infoDoc.getFechaVencimiento()!=null){

                        XMLGregorianCalendar xmlCalendar = infoDoc.getFechaVencimiento();
                        GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                        jlFechaVencimiento.setText("Fecha de vencimiento: "+sdf.format(rechaVencimiento.getTime()));
                        jlFechaVencimiento.setVisible(true);

                        ruta = infoDoc.getRutaArchivo();
                        archivo = infoDoc.getNombreArchivo();
                        version = infoDoc.getVersion();

                    }else{
                        ruta = infoDoc.getRutaArchivo();
                        archivo = infoDoc.getNombreArchivo();
                        version = infoDoc.getVersion();
                    }

                    buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);

                    escribiendo = new FileOutputStream(fileCod);
                    escribiendo.write(buffer.getBufer());
                    escribiendo.flush();
                    escribiendo.close();

                    new ToolsFile().decodificar("imagen."+infoDocumento.getFormato());

                    imagen = new ImageIcon(toolsTiff.getArchivo()+"imagen."+infoDocumento.getFormato()).getImage();

                    panelImagen.setImagen(imagen);
                }
            }
        } catch (SOAPException ex) {
            traza.trace("error al buscar el archivo "+ex.getMessage(), Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();
        } catch (IOException ex) {
            traza.trace("Error al buscar la version del archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error al buscar la version del archivo\n" + ex.getMessage(), "Error...", JOptionPane.ERROR_MESSAGE);
        } catch(SOAPFaultException e){
            traza.trace("error al buscar el archivo "+e.getMessage(), Level.ERROR, e);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToolBar barra;
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton cancelar;
    private javax.swing.JComboBox cboVersion;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton jbImprimir;
    private javax.swing.JLabel jlCategoria;
    private javax.swing.JLabel jlFechaVencimiento;
    private javax.swing.JLabel jlLibreria;
    private javax.swing.JLabel jlSubCategoria;
    private javax.swing.JLabel jlTipoDocumento;
    private com.develcom.tools.PanelImagen panelImagen;
    private javax.swing.JButton zommIn;
    private javax.swing.JButton zommOut;
    // End of variables declaration//GEN-END:variables

    public void setIndices(List<Indice> indices) {
        this.indices = indices;
    }

    public JButton getJbImprimir() {
        return jbImprimir;
    }

    
}
