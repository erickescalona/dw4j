create table CATEGORIA
(
  id_categoria INTEGER not null DEFAULT nextval('SQ_CATEGRORIA'::regclass),
  id_libreria  INTEGER not null,
  categoria    VARCHAR(200) not null,
  id_estatus   INTEGER not null
);
alter table CATEGORIA
  add constraint CATEGORIA_PK primary key (ID_CATEGORIA);


create table CAUSA
(
  id_causa INTEGER not null,
  causa    VARCHAR(150) not null
);
alter table CAUSA
  add constraint CAUSA_PK primary key (ID_CAUSA);


create table CONFIGURACION
(
  id_configuracion INTEGER not null,
  calidad          CHAR(1),
  ruta_temporal    VARCHAR(50),
  archivo_tif      VARCHAR(50),
  archivo_cod      VARCHAR(50),
  log              VARCHAR(50),
  foliatura        CHAR(1),
  server_name      VARCHAR(50),
  database_name    VARCHAR(50),
  port             INTEGER,
  userbd           VARCHAR(50),
  password         VARCHAR(50),
  ficha            CHAR(1),
  fabrica          CHAR(1),
  elimina          CHAR(1)
);
alter table CONFIGURACION
  add constraint PK_CONFIGURACION primary key (ID_CONFIGURACION);


create table SUBCATEGORIA
(
  id_subcategoria INTEGER not null DEFAULT nextval('SQ_SUBCATEGRORIA'::regclass),
  id_categoria    INTEGER not null,
  subcategoria    VARCHAR(200) not null,
  id_estatus      VARCHAR(20) not null
);
alter table SUBCATEGORIA
  add constraint SUBCATEGORIA_PK primary key (ID_SUBCATEGORIA);

create table TIPODOCUMENTO
(
  id_documento    INTEGER not null DEFAULT nextval('SQ_TIPO_DOCUMENTO'::regclass),
  id_categoria    INTEGER not null,
  id_subcategoria INTEGER not null,
  tipo_documento  VARCHAR(200) not null,
  id_estatus      INTEGER not null,
  vencimiento     CHAR(1),
  dato_adicional  CHAR(1),
  ficha           CHAR(1)
);
alter table TIPODOCUMENTO
  add constraint TIPODOCUMENTO_PK primary key (ID_DOCUMENTO);
alter table TIPODOCUMENTO
  add constraint FK_TIPODOC_CATEGORIA foreign key (ID_CATEGORIA)
  references CATEGORIA (ID_CATEGORIA);
alter table TIPODOCUMENTO
  add constraint FK_TIPODOC_SUBCATEGORIA foreign key (ID_SUBCATEGORIA)
  references SUBCATEGORIA (ID_SUBCATEGORIA);


create table DATO_ADICIONAL
(
  id_dato_adicional INTEGER not null DEFAULT nextval('SQ_DATO_ADICIONAL'::regclass),
  indice_adicional  VARCHAR(250) not null,
  tipo              VARCHAR(50) not null,
  id_documento      INTEGER not null,
  codigo            INTEGER
);
alter table DATO_ADICIONAL
  add constraint PK_DATO_ADICIONAL primary key (ID_DATO_ADICIONAL);
alter table DATO_ADICIONAL
  add constraint FK_DATO_ADICIONAL_TIPODOC foreign key (ID_DOCUMENTO)
  references TIPODOCUMENTO (ID_DOCUMENTO);


create table DOCUMENTO_ELIMINADO
(
  id_doc_eliminado  INTEGER not null DEFAULT nextval('SQ_DOCUMENTO_ELIMINADO'::regclass),
  id_expediente     VARCHAR(50) not null,
  id_libreria       INTEGER not null,
  id_categoria      INTEGER not null,
  id_subcategoria   INTEGER not null,
  id_documento      INTEGER not null,
  numero_documento  INTEGER not null,
  version           INTEGER not null,
  paginas           INTEGER not null,
  fecha_vencimiento DATE,
  fecha_eliminado   DATE not null,
  usuario_elimino   VARCHAR(30) not null
);
alter table DOCUMENTO_ELIMINADO
  add constraint DOCUMENTO_ELIMINADO_PK primary key (ID_DOC_ELIMINADO);


create table ESTATUS_DOCUMENTO
(
  id_estatus_documento INTEGER not null,
  estatus_documento    VARCHAR(20) not null
);
alter table ESTATUS_DOCUMENTO
  add constraint ESTATUS_DOCUMENTO_PK primary key (ID_ESTATUS_DOCUMENTO);


create table INFODOCUMENTO
(
  id_infodocumento  INTEGER not null DEFAULT nextval('SQ_INFODOCUMENTO'::regclass),
  id_documento      INTEGER not null,
  id_expediente     VARCHAR(50),
  nombre_archivo    VARCHAR(1000),
  ruta_archivo      VARCHAR(1000),
  formato           VARCHAR(4),
  numero_documento  INTEGER not null,
  version           INTEGER not null,
  paginas           INTEGER not null,
  fecha_vencimiento DATE,
  estatus_documento INTEGER not null,
  re_digitalizado   CHAR(1) not null
);
alter table INFODOCUMENTO
  add constraint PK_INFORDOCUMENTO primary key (ID_INFODOCUMENTO);
alter table INFODOCUMENTO
  add constraint FK_INFODOC_STATUSDOC foreign key (ESTATUS_DOCUMENTO)
  references ESTATUS_DOCUMENTO (ID_ESTATUS_DOCUMENTO);
alter table INFODOCUMENTO
  add constraint FK_INFODOC_TIPODOC foreign key (ID_DOCUMENTO)
  references TIPODOCUMENTO (ID_DOCUMENTO);


create table DATOS_INFODOCUMENTO
(
  id_datos             INTEGER not null DEFAULT nextval('SQ_DATOS_INFODOCUMENTO'::regclass),
  id_infodocumento     INTEGER,
  id_doc_eliminado     INTEGER,
  fecha_digitalizacion DATE,
  usuario_digitalizo   VARCHAR(30),
  fecha_aprobacion     DATE,
  usuario_aprobacion   VARCHAR(30),
  fecha_rechazo        DATE,
  usuario_rechazo      VARCHAR(30),
  motivo_rechazo       VARCHAR(300),
  causa_rechazo        VARCHAR(300),
  fecha_eliminado      DATE,
  usuario_elimino      VARCHAR(30),
  motivo_elimino       VARCHAR(300),
  causa_elimino        VARCHAR(300),
  dato_adicional       VARCHAR(30)
);
alter table DATOS_INFODOCUMENTO
  add constraint PK_DATOS_INFODOCUMENTO primary key (ID_DATOS);
alter table DATOS_INFODOCUMENTO
  add constraint FK_DATOC_INFODOC foreign key (ID_INFODOCUMENTO)
  references INFODOCUMENTO (ID_INFODOCUMENTO);
alter table DATOS_INFODOCUMENTO
  add constraint FK_DATOS_DOC_ELIMINADO foreign key (ID_DOC_ELIMINADO)
  references DOCUMENTO_ELIMINADO (ID_DOC_ELIMINADO);


create table ESTATUS
(
  id_estatus INTEGER not null,
  estatus    VARCHAR(100) not null
);
alter table ESTATUS
  add constraint ESTATUS_PK primary key (ID_ESTATUS);


create table LIBRERIA
(
  id_libreria INTEGER not null DEFAULT nextval('SQ_LIBRERIA'::regclass),
  libreria    VARCHAR(200) not null,
  id_estatus  INTEGER not null
);
alter table LIBRERIA
  add constraint LIBRERIA_PK primary key (ID_LIBRERIA);


create table EXPEDIENTES
(
  id_expedientes INTEGER not null DEFAULT nextval('SQ_EXPEDIENTE'::regclass),
  expediente     VARCHAR(250) not null,
  id_indice      INTEGER not null,
  valor          VARCHAR(250),
  id_libreria    INTEGER not null,
  id_categoria   INTEGER not null
);
alter table EXPEDIENTES
  add constraint PK_EXPEDIENTES primary key (ID_EXPEDIENTES);
alter table EXPEDIENTES
  add constraint FK_EXPEDIENTES_CATEGORIA foreign key (ID_CATEGORIA)
  references CATEGORIA (ID_CATEGORIA);
alter table EXPEDIENTES
  add constraint FK_EXPEDIENTES_LIBRERIA foreign key (ID_LIBRERIA)
  references LIBRERIA (ID_LIBRERIA);


create table USUARIO
(
  id_usuario VARCHAR(50) not null,
  nombre     VARCHAR(100) not null,
  apellido   VARCHAR(100) not null,
  cedula     VARCHAR(50),
  sexo       CHAR(1),
  id_estatus INTEGER not null,
  password   VARCHAR(16)
);
alter table USUARIO
  add constraint USUARIO_PK primary key (ID_USUARIO);
alter table USUARIO
  add constraint FK_USUARIO_ESTATUS foreign key (ID_ESTATUS)
  references ESTATUS (ID_ESTATUS);


create table FABRICA
(
  usuario VARCHAR(50) not null,
  fabrica CHAR(1) not null
);
alter table FABRICA
  add constraint PK_FABRICA primary key (USUARIO);
alter table FABRICA
  add constraint FK_FABRICA_USUARIO foreign key (USUARIO)
  references USUARIO (ID_USUARIO);


create table FOLIATURA
(
  id_foliatura     INTEGER not null DEFAULT nextval('SQ_FOLIATURA'::regclass),
  id_infodocumento INTEGER not null,
  id_documento     INTEGER not null,
  id_expediente    VARCHAR(50) not null,
  pagina           INTEGER not null
);
alter table FOLIATURA
  add constraint FOLIATURA_PK primary key (ID_FOLIATURA);


create table INDICES
(
  id_indice    INTEGER not null DEFAULT nextval('SQ_INDICES'::regclass),
  id_categoria INTEGER not null,
  indice       VARCHAR(250) not null,
  tipo         VARCHAR(50) not null,
  codigo       INTEGER not null,
  clave        CHAR(1)
);
alter table INDICES
  add constraint PK_INDICE primary key (ID_INDICE);
alter table INDICES
  add constraint FK_INDICE_CATEGORIA foreign key (ID_CATEGORIA)
  references CATEGORIA (ID_CATEGORIA);


create table LISTA_DESPLEGABLES
(
  id_lista      INTEGER not null DEFAULT nextval('SQ_COMBO'::regclass),
  codigo_indice INTEGER not null,
  descripcion   VARCHAR(200)
);
alter table LISTA_DESPLEGABLES
  add constraint PK_COMBO primary key (ID_LISTA);


create table ROL
(
  id_rol INTEGER not null,
  rol    VARCHAR(50) not null
);
alter table ROL
  add constraint ROL_PK primary key (ID_ROL);


create table PERFIL
(
  id_perfil    INTEGER not null DEFAULT nextval('SQ_PERFIL'::regclass),
  id_libreria  INTEGER,
  id_categoria INTEGER,
  id_usuario   VARCHAR(50) not null,
  id_rol       INTEGER not null
);
alter table PERFIL
  add constraint PERFIL_PK primary key (ID_PERFIL);
alter table PERFIL
  add constraint FK_PERFIL_CATEGORIA foreign key (ID_CATEGORIA)
  references CATEGORIA (ID_CATEGORIA);
alter table PERFIL
  add constraint FK_PERFIL_LIBRERIA foreign key (ID_LIBRERIA)
  references LIBRERIA (ID_LIBRERIA);
alter table PERFIL
  add constraint FK_PERFIL_ROL foreign key (ID_ROL)
  references ROL (ID_ROL);
alter table PERFIL
  add constraint FK_PERFIL_USUARIO foreign key (ID_USUARIO)
  references USUARIO (ID_USUARIO);


create table VALOR_DATO_ADICIONAL
(
  id_valor          INTEGER not null DEFAULT nextval('SQ_VALOR_DATO_ADICIONAL'::regclass),
  id_dato_adicional INTEGER not null,
  valor             VARCHAR(250) not null,
  numero            INTEGER not null,
  version           INTEGER not null,
  expediente        VARCHAR(250) not null
);
alter table VALOR_DATO_ADICIONAL
  add constraint PK_VALOR_DATO_ADICIONAL primary key (ID_VALOR);
alter table VALOR_DATO_ADICIONAL
  add constraint KF_VALOR_INDICE_DA foreign key (ID_DATO_ADICIONAL)
  references DATO_ADICIONAL (ID_DATO_ADICIONAL);