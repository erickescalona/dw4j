/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.captura;

import com.develcom.paginas.herramientaArbol.InterpretaArbol;
import com.develcom.paginas.herramientaArbol.ModeloArbol;
import com.develcom.paginas.herramientaArbol.NodoArbol;
import com.develcom.paginas.herramientaArbol.DatoNodoArbol;
import com.develcom.administracion.DatoAdicional;
import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.autentica.Configuracion;
import com.develcom.dao.Expediente;
import com.develcom.documento.InfoDocumento;
import com.develcom.expediente.Indice;
import com.develcom.paginas.captura.applet.Principal;
import com.develcom.paginas.reportes.tools.ProcesaReporte;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.excepcion.DW4JException;
import com.develcom.tools.scan.EscaneaDocumento;
import com.develcom.tools.trazas.Traza;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Applet;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Window;
import ve.com.develcom.administracion.AdministracionBusqueda;
import ve.com.develcom.expediente.BuscaIndice;
import ve.com.develcom.expediente.GestionDocumentos;
import ve.com.develcom.expediente.GestionExpediente;

/**
 *
 * @author develcom
 */
public class Digitaliza extends SelectorComposer<Component> {

    private static final long serialVersionUID = 5629474072508651512L;

    @Wire
    private Tree arbolDocDisp;

    @Wire
    private Tree arbolDocDigit;

    @Wire
    private Tree arbolDocRech;

    @Wire
    private Textbox txtExpediente;

    @Wire
    private Combobox cmbSubCategoria;

    @Wire
    private Listbox indices;

    @Wire
    private Window winDigitaliza;

    @Wire
    private Datebox fechaVencimiento;

    @Wire
    private Datebox fechaRechazado;

    @Wire
    private Groupbox grupoRechazado;

    @Wire
    private Caption capTitulo;

    private List<InfoDocumento> documentosDigitalizado = new ArrayList<>();
    private List<InfoDocumento> docDigitalizadoRecha = new ArrayList<>();
    private List<TipoDocumento> tipoDocumentosDisponibles = new ArrayList<>();
    private List<InfoDocumento> infoDocumentos = new ArrayList<>();
    private List<SubCategoria> subCategorias = null;

    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(Digitaliza.class);
    private Session session;
    private Expediente expediente;
//    private IBarraEstado barraEstado;
    private Configuracion configuracion;
    private List<Integer> idDocumento = new ArrayList<>();
    private boolean respMensaje;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

//            barraEstado = (IBarraEstado) session.getAttribute("status");
            expediente = (Expediente) session.getAttribute("expediente");
            configuracion = (Configuracion) session.getAttribute("configuracion");

            capTitulo.setLabel(expediente.getLibreria() + " - " + expediente.getCategoria());
//            grupoRechazado.setVisible(false);

            llenarSubCategorias();

        } else {

            herramientas.warn("Problemas con la Sesión, posiblemente se venció");
            herramientas.navegarPagina("index.zul");
        }

    }

    /**
     * Llena el combobox de las SubCategorias
     *
     * @return
     */
    private boolean llenarSubCategorias() {
        boolean resp = false;
        Comboitem item;
        try {
            traza.trace("llenado listado de subCategorias", Level.INFO);

            subCategorias = new AdministracionBusqueda().buscarSubCategoria(null, expediente.getIdCategoria(), 0);

            if (subCategorias != null && !subCategorias.isEmpty()) {

                for (SubCategoria sc : subCategorias) {
                    if (sc.getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {

                        item = new Comboitem();
                        item.setValue(sc.getSubCategoria().trim());
                        item.setLabel(sc.getSubCategoria().trim());
                        item.setParent(cmbSubCategoria);
                    }
                }
                resp = true;
            } else {
                herramientas.warn("La Categoria " + expediente.getCategoria() + "\nno tiene subCategorias");
            }
        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("error al llenar el listado de subCategorias", Level.ERROR, e);
            herramientas.error("Error al llenar el listado de subCategorias\n", e);
        }
        return resp;
    }

    private void verProgreso(final Integer valor, final String mensaje) {

//        Clients.showBusy(winDigitaliza, mensaje);
        Integer porcentaje;

        porcentaje = (valor * 100) / 5;
//        barraProgreso.setValue(porcentaje);
//        lblProgreso.setValue(mensaje);

//        barraEstado.setStatus(mensaje, porcentaje);
    }

    /**
     * Busca el expediente y los documentos disponibles, digitalizados y
     * rechazados
     */
    @Listen("onClick = #btnBuscar")
    public void buscarExpediente() {
        String expe, subcat;

        try {

            Treecols treecolsDigi = arbolDocDigit.getTreecols();
            if (treecolsDigi != null) {
                arbolDocDigit.removeChild(treecolsDigi);
            }
            Treecols treecolsDisp = arbolDocDisp.getTreecols();
            if (treecolsDisp != null) {
                arbolDocDisp.removeChild(treecolsDisp);
            }
            Treecols treecolsRech = arbolDocRech.getTreecols();
            if (treecolsRech != null) {
                arbolDocRech.removeChild(treecolsRech);
            }

            indices.getItems().clear();

            expe = txtExpediente.getValue();

            if (!expe.equalsIgnoreCase("")) {
                
                try {
                    subcat = cmbSubCategoria.getSelectedItem().getValue();
                } catch (NullPointerException e) {
                    subcat = "";
                }
                
                if (!subcat.equalsIgnoreCase("")) {
                    
                    verProgreso(1, "Comprobando el expedente " + txtExpediente.getValue());
                    if (comprobarExpediente(expe, subcat)) {

                        verProgreso(2, "Buscando los Indices del Expediente");
                        construirIndices();

                        verProgreso(3, "Buscando los Tipos de Documentos Disponibles");
                        buscarDocumentosDisponibles();

                        verProgreso(4, "Buscando los Tipos de Documentos Digitalizado");
                        buscarDocumentosDigitalizado();

                        verProgreso(5, "Buscando los Tipos de Documentos Rechazados");
                        buscarDocumentosRechazados();

                    } else {
                        if (!respMensaje) {
                            respMensaje = false;
                            herramientas.warn("No se creo el Expediente");
                        } else {
                            herramientas.warn("Problemas con la busqueda del expediente");
                        }
                    }
                } else {
                    herramientas.warn("Debe seleccionar una subCategoria");
                }
            } else {
                herramientas.warn("Debe colocar un identificador de expediente");
            }

        } catch (Exception e) {
            traza.trace("problemas al comprobar el expedeinte ", Level.ERROR, e);
            herramientas.warn("Problemas al comprobar el expediente");
        }

    }

    /**
     * Comprueba si el expediente existe, sino se creara en el DW4J
     *
     * @param exped El identificador del expdiente
     * @param subCategoria La subCategoria seleccionada
     */
    private boolean comprobarExpediente(String exped, String subCategoria) {

        com.develcom.expediente.Expedientes expedienteBuscado;
        int idCat, idLib, idSubCat = 0;
        boolean resp = false;

        try {
            traza.trace("expediente a digitalizar " + txtExpediente.getValue(), Level.INFO);

            idCat = expediente.getIdCategoria();
            idLib = expediente.getIdLibreria();

            traza.trace("verificando que el expediente " + exped + " exista", Level.INFO);
            expedienteBuscado = new GestionExpediente().encuentraExpediente(exped, idCat, idLib);

            if (expedienteBuscado.isExiste()) {

                traza.trace("seleccion la subCategoria " + subCategoria, Level.INFO);

                for (SubCategoria sc : subCategorias) {
                    if (subCategoria.equalsIgnoreCase(sc.getSubCategoria())) {
                        idSubCat = sc.getIdSubCategoria();
                        expediente.setIdSubCategoria(idSubCat);
                    }
                }

                traza.trace("seleccion la categoria " + expediente.getCategoria(), Level.INFO);

                expediente.setIdExpediente(exped);
                expediente.setSubCategoria(subCategoria);

                traza.trace("libreria " + idLib + " - " + expediente.getLibreria(), Level.INFO);
                traza.trace("categoria " + idCat + " - " + expediente.getCategoria(), Level.INFO);
                traza.trace("subCategoria " + idSubCat + " - " + subCategoria, Level.INFO);
                traza.trace("expediente " + exped, Level.INFO);
                session.setAttribute("expediente", expediente);
                resp = true;

            } else {

                int n = Messagebox.show("El expediente \"" + exped + "\" no existe, \n¿desea crearlo?\n" + expedienteBuscado.getMensaje(),
                        "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {

                            @Override
                            public void onEvent(Event evt) throws Exception {
                            }

                        });

                if (n == Messagebox.YES) {

                    traza.trace("el expediente " + exped + " no existe se creara", Level.INFO);

                    expediente.setIdExpediente(exped);
                    expediente.setSubCategoria(subCategoria);

                    traza.trace("libreria " + idLib + " - " + expediente.getLibreria(), Level.INFO);
                    traza.trace("categoria " + idCat + " - " + expediente.getCategoria(), Level.INFO);
                    traza.trace("subCategoria " + idSubCat + " - " + subCategoria, Level.INFO);

                    session.setAttribute("expediente", expediente);

                    Window window = herramientas.crearVentanaModal("creaExpediente.zul");
                    window.doModal();

                    session = herramientas.crearSesion();
                    expediente = (Expediente) session.getAttribute("expediente");
                    respMensaje = expediente.isResultado();
                    traza.trace("se creo el expediente? " + respMensaje, Level.INFO);
                    if (respMensaje) {
                        resp = true;
                    }

                } else {
                    resp = false;
                    txtExpediente.setValue("");
                }
            }
        } catch (SOAPException e) {
            herramientas.error("Problemas buscando el expediente " + txtExpediente.getValue(), e);
            traza.trace("problemas buscando el expediente " + txtExpediente.getValue(), Level.ERROR, e);
            resp = false;
        }

        return resp;
    }

    /**
     * Busca los documentos disponibles de la subCategoria seleccionada
     */
    private void buscarDocumentosDisponibles() {

        int idCat, idSubCat;

        Treecols treecols = new Treecols();
        Treecol categoria;

        NodoArbol raiz;
        LinkedList<NodoArbol> nodoSubCate = new LinkedList();
        LinkedList<NodoArbol> nodoTipoDoc = new LinkedList();
        DatoNodoArbol dna;

        try {

            if (session != null) {

                if (!idDocumento.isEmpty()) {
                    idDocumento.clear();
                }
                if (!nodoTipoDoc.isEmpty()) {
                    nodoTipoDoc.clear();
                }

                idCat = expediente.getIdCategoria();
                idSubCat = expediente.getIdSubCategoria();

                //busca los tipos de documentos disponibles
                tipoDocumentosDisponibles.clear();
                tipoDocumentosDisponibles = new AdministracionBusqueda().buscarTipoDocumento(null, idCat, idSubCat);

                categoria = new Treecol(expediente.getCategoria());
                categoria.setWidth("370px");
                categoria.setParent(treecols);
                treecols.setParent(arbolDocDisp);

                for (TipoDocumento td : tipoDocumentosDisponibles) {
                    if (td.getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {

                        traza.trace("agregando la hoja " + td.getTipoDocumento(), Level.INFO);
                        dna = new DatoNodoArbol(td.getTipoDocumento().trim());
                        dna.setIdDocumento(td.getIdTipoDocumento());
                        dna.setTipoDocumento(td.getTipoDocumento());
                        nodoTipoDoc.add(new NodoArbol(dna));
                        idDocumento.add(td.getIdTipoDocumento());
                    }
                }
                nodoSubCate.add(new NodoArbol(new DatoNodoArbol(expediente.getSubCategoria()), nodoTipoDoc));

                raiz = new NodoArbol(null, nodoSubCate, true);
                arbolDocDisp.setItemRenderer(new InterpretaArbol());
                arbolDocDisp.setModel(new ModeloArbol(raiz));

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("Error al crear el arbol de documentos disponibles", Level.ERROR, e);
            herramientas.error("Error al crear el arbol de documentos", e);
        }
    }

    /**
     * Busca los documentos ya digitalizados del expediente colocado y
     * subCategoria seleccionada
     */
    private void buscarDocumentosDigitalizado() {

        int idCat, idSubCat, totalDocDig, cont = 0;
        String nombreTD, tmp[], fullName;

        Treecols treecols = new Treecols();
        Treecol categoria;
        Treecol datoAdicional = new Treecol("Datos Adicionales");

        NodoArbol raiz;
        LinkedList<NodoArbol> nodoSubCate = new LinkedList();
        LinkedList<NodoArbol> nodoTipoDoc = new LinkedList();
        DatoNodoArbol dna;

        try {

            if (session != null) {

                Treecols treecolsDigi = arbolDocDigit.getTreecols();
                if (treecolsDigi != null) {
                    arbolDocDigit.removeChild(treecolsDigi);
                }

                if (!documentosDigitalizado.isEmpty()) {
                    documentosDigitalizado.clear();
                }
                if (!nodoTipoDoc.isEmpty()) {
                    nodoTipoDoc.clear();
                }

                idCat = expediente.getIdCategoria();
                idSubCat = expediente.getIdSubCategoria();

                categoria = new Treecol(expediente.getCategoria());
                categoria.setWidth("400px");
                categoria.setParent(treecols);
                
                datoAdicional.setWidth("400px");
                datoAdicional.setParent(treecols);
                
                treecols.setParent(arbolDocDigit);

                if (!idDocumento.isEmpty()) {

                    //busca los tipos de documentos digitalizados
                    documentosDigitalizado.clear();
                    if (configuracion.isCalidadActivo()) {
                        documentosDigitalizado = new GestionDocumentos().encontrarInformacionDoc(idDocumento, expediente.getIdExpediente(), idCat, idSubCat, 0, 1, false);
                    } else {
                        documentosDigitalizado = new GestionDocumentos().encontrarInformacionDoc(idDocumento, expediente.getIdExpediente(), idCat, idSubCat, 1, 1, false);
                    }

                    if (!documentosDigitalizado.isEmpty()) {

                        totalDocDig = documentosDigitalizado.size();

                        tmp = new String[totalDocDig];

                        for (int j = 0; j < totalDocDig; j++) {

                            InfoDocumento id = documentosDigitalizado.get(j);
//                            barraEstado.setStatus("Generando el Expediente: Agregando el Tipo de Documento " + id.getTipoDocumento() + " tiene dato adicioanl " + id.isTipoDocDatoAdicional());
                            infoDocumentos.add(id);

                            nombreTD = id.getTipoDocumento().trim() + " - " + id.getNumeroDocumento();
                            id.setTipoDocumento(nombreTD);
                            tmp[cont] = nombreTD;
                            cont++;
                            int h = 0;
                            for (String s : tmp) {
                                try {
                                    if (s.equalsIgnoreCase(nombreTD)) {
                                        h++;
                                    }
                                } catch (NullPointerException e) {
                                }

                            }
                            if (h == 1) {
                                String da = "";
                                if (id.isTipoDocDatoAdicional()) {
                                    List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                                    int size = datosAdicionales.size(), i = 1;
                                    for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                        if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                            if (i == size) {
                                                da = da + " " + datAd.getValor().toString().trim();
                                            } else {
                                                da = da + " " + datAd.getValor().toString().trim() + ",";
                                            }
                                        } else {
                                            if (i == size) {
                                                da = da + " " + datAd.getValor().toString().trim();
                                            } else {
                                                da = da + " " + datAd.getValor().toString().trim() + ",";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                da = da.trim();

                                if (configuracion.isCalidadActivo()) {
                                    if (id.isTipoDocDatoAdicional()) {

                                        fullName = nombreTD.trim() + " " + id.getEstatusDocumento().trim() + " (" + da + ")";

                                        dna = new DatoNodoArbol(id.getTipoDocumento().trim() + " " + id.getEstatusDocumento().trim(), da);
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosDigitalizado.get(j).setTipoDocumento(fullName.trim());

                                    } else {

                                        fullName = nombreTD.trim() + " " + id.getEstatusDocumento().trim();

                                        dna = new DatoNodoArbol(id.getTipoDocumento().trim() + " " + id.getEstatusDocumento().trim(), "");
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosDigitalizado.get(j).setTipoDocumento(fullName.trim());

                                    }
                                } else {
                                    if (id.isTipoDocDatoAdicional()) {

                                        fullName = nombreTD.trim() + " (" + da + ")";

                                        dna = new DatoNodoArbol(id.getTipoDocumento().trim(), da);
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosDigitalizado.get(j).setTipoDocumento(fullName.trim());

                                    } else {

                                        fullName = nombreTD;

                                        dna = new DatoNodoArbol(id.getTipoDocumento(), "");
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        documentosDigitalizado.get(j).setTipoDocumento(fullName);

                                    }
                                }

                                traza.trace("tipo de documento digitalizado " + fullName + " id InfoDocumento " + id.getIdInfoDocumento(), Level.INFO);
                            }
                        }
                        nodoSubCate.add(new NodoArbol(new DatoNodoArbol(expediente.getSubCategoria()), nodoTipoDoc));
                    }
                }

                raiz = new NodoArbol(null, nodoSubCate);
                arbolDocDigit.setItemRenderer(new InterpretaArbol());
                arbolDocDigit.setModel(new ModeloArbol(raiz));

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("Error al crear el arbol de documentos digitalizados", Level.ERROR, e);
            herramientas.error("Error al crear el arbol de documentos", e);
        }
    }

    /**
     * Busca los documentos rechazado del expediente colocado y la subCategoria
     * seleccionada
     */
    private void buscarDocumentosRechazados() {

        int idCat, idSubCat, totalDocDig, cont = 0;
        String nombreTD, tmp[], fullName;

        Treecols treecols = new Treecols();
        Treecol categoria;
        Treecol datoAdicional = new Treecol("Datos Adicionales");

        NodoArbol raiz;
        LinkedList<NodoArbol> nodoSubCate = new LinkedList();
        LinkedList<NodoArbol> nodoTipoDoc = new LinkedList();
        DatoNodoArbol dna;

        try {

            if (session != null) {

                if (!docDigitalizadoRecha.isEmpty()) {
                    docDigitalizadoRecha.clear();
                }
                if (!nodoTipoDoc.isEmpty()) {
                    nodoTipoDoc.clear();
                }

                idCat = expediente.getIdCategoria();
                idSubCat = expediente.getIdSubCategoria();

                categoria = new Treecol(expediente.getCategoria());
                categoria.setWidth("400px");
                categoria.setParent(treecols);
                
                datoAdicional.setWidth("400px");
                datoAdicional.setParent(treecols);
                
                treecols.setParent(arbolDocRech);

                if (!idDocumento.isEmpty()) {

                    //busca los tipos de documentos rechazados
                    docDigitalizadoRecha.clear();
                    docDigitalizadoRecha = new GestionDocumentos().encontrarInformacionDoc(idDocumento, expediente.getIdExpediente(), idCat, idSubCat, 2, 0, false);

                    if (!docDigitalizadoRecha.isEmpty()) {

                        grupoRechazado.setVisible(true);

                        totalDocDig = docDigitalizadoRecha.size();

                        tmp = new String[totalDocDig];

                        for (int j = 0; j < totalDocDig; j++) {

                            InfoDocumento id = docDigitalizadoRecha.get(j);
//                            barraEstado.setStatus("Generando el Expediente: Agregando el Tipo de Documento " + id.getTipoDocumento() + " tiene dato adicioanl " + id.isTipoDocDatoAdicional());
                            infoDocumentos.add(id);

                            nombreTD = id.getTipoDocumento() + " - " + id.getNumeroDocumento();
                            id.setTipoDocumento(nombreTD);
                            tmp[cont] = nombreTD;
                            cont++;
                            int h = 0;
                            for (String s : tmp) {
                                try {
                                    if (s.equalsIgnoreCase(nombreTD)) {
                                        h++;
                                    }
                                } catch (NullPointerException e) {
                                }

                            }
                            if (h == 1) {
                                String da = "";
                                if (id.isTipoDocDatoAdicional()) {
                                    List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                                    int size = datosAdicionales.size(), i = 1;
                                    for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                        if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                            if (i == size) {
                                                da = da + " " + datAd.getValor().toString();
                                            } else {
                                                da = da + " " + datAd.getValor().toString() + ",";
                                            }
                                        } else {
                                            if (i == size) {
                                                da = da + " " + datAd.getValor();
                                            } else {
                                                da = da + " " + datAd.getValor() + ",";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                da = da.trim();

                                if (configuracion.isCalidadActivo()) {
                                    if (id.isTipoDocDatoAdicional()) {

                                        fullName = nombreTD + " " + id.getEstatusDocumento() + " (" + da + ")";

                                        dna = new DatoNodoArbol(id.getTipoDocumento(), da);
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        docDigitalizadoRecha.get(j).setTipoDocumento(fullName);

                                    } else {

                                        fullName = nombreTD + " " + id.getEstatusDocumento();

                                        dna = new DatoNodoArbol(id.getTipoDocumento(), "");
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        docDigitalizadoRecha.get(j).setTipoDocumento(fullName);

                                    }
                                } else {
                                    if (id.isTipoDocDatoAdicional()) {

                                        fullName = nombreTD + " (" + da + ")";

                                        dna = new DatoNodoArbol(id.getTipoDocumento(), da);
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        docDigitalizadoRecha.get(j).setTipoDocumento(fullName);

                                    } else {

                                        fullName = nombreTD;

                                        dna = new DatoNodoArbol(id.getTipoDocumento(), "");
                                        dna.setIdInfoDocumento(id.getIdInfoDocumento());
                                        dna.setIdDocumento(id.getIdDocumento());
                                        dna.setIdSubCategoria(expediente.getIdSubCategoria());
                                        nodoTipoDoc.add(new NodoArbol(dna));

                                        docDigitalizadoRecha.get(j).setTipoDocumento(fullName);

                                    }
                                }

                                traza.trace("tipo de documento digitalizado " + fullName + " id InfoDocumento " + id.getIdInfoDocumento(), Level.INFO);
                            }
                        }
                        nodoSubCate.add(new NodoArbol(new DatoNodoArbol(expediente.getSubCategoria()), nodoTipoDoc));
                    }
                }

                raiz = new NodoArbol(null, nodoSubCate);
                arbolDocRech.setItemRenderer(new InterpretaArbol());
                arbolDocRech.setModel(new ModeloArbol(raiz));

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("Error al crear el arbol de documentos digitalizados", Level.ERROR, e);
            herramientas.error("Error al crear el arbol de documentos", e);
        }
    }

    /**
     * Busca los indices del expediente
     */
    private void construirIndices() {

        List<Indice> listaIndic;
        Listitem datos;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        try {

            if (session != null) {

                listaIndic = new BuscaIndice().buscaDatosIndice(expediente.getIdExpediente(), expediente.getIdLibreria(), expediente.getIdCategoria());

//                session.setAttribute("listaIndices", listaIndic);
                for (Indice ind : listaIndic) {
                    traza.trace("agregando el indice a la lista " + ind.getIndice() + ": " + ind.getValor(), Level.INFO);
                    datos = new Listitem();
                    datos.appendChild(new Listcell(ind.getIndice() + ": " + ind.getValor()));
                    datos.setParent(indices);
                }

            }

        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("Problemas con los indices del expediente " + expediente.getIdExpediente(), Level.ERROR, e);
            herramientas.error("Problemas con los indices del expediente " + expediente.getIdExpediente(), e);
        }
    }

    /**
     * Digitaliza un documento
     */
    @Listen("onClick = #btnScanDisponible")
    public void digitalizarDisponible() {

        List<DatoAdicional> lstDatosAdicionales;;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;
        String tipoDocumentoDigitalizar;
        int dig = 0, idDoc = 0;
        InfoDocumento infoDocumento;
        XMLGregorianCalendar xmlFechaVencimiento;
        GregorianCalendar calendar = new GregorianCalendar();
        Date fecha;
        DatatypeFactory dtf;
        boolean flag = false, tiene = true;

        try {

            if (session != null) {

                value = arbolDocDisp.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumentoDigitalizar = dna.getTipoDocumento();
                    traza.trace("tipo de documento rechazado seleccionado " + tipoDocumentoDigitalizar, Level.INFO);

                    int n = Messagebox.show("Desea  crear un nuevo documento \n(" + tipoDocumentoDigitalizar + ") del expediente \n" + txtExpediente.getValue(),
                            "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                    respMensaje = evt.getName().equalsIgnoreCase(Messagebox.ON_YES);
                                }
                            });

//                    if (respMensaje) {
                    if (n == Messagebox.YES) {

                        infoDocumento = new InfoDocumento();

                        try {
                            for (TipoDocumento td : tipoDocumentosDisponibles) {
                                if (tipoDocumentoDigitalizar.contains(td.getTipoDocumento().trim())) {

                                    idDoc = td.getIdTipoDocumento();

                                    if (td.getVencimiento().equalsIgnoreCase("1")) {
                                        if (fechaVencimiento.getValue() != null) {

                                            fecha = fechaVencimiento.getValue();
                                            calendar.setTime(fecha);
                                            dtf = DatatypeFactory.newInstance();
                                            xmlFechaVencimiento = dtf.newXMLGregorianCalendar(calendar);
                                            infoDocumento.setFechaVencimiento(xmlFechaVencimiento);

                                            traza.trace("fecha de vencimiento " + sdf.format(fecha.getTime()), Level.INFO);
                                            traza.trace("fecha de vencimiento " + xmlFechaVencimiento, Level.INFO);
                                            break;
                                        } else {
                                            throw new DW4JException("Debe agregar una fecha de vencimiento");
                                        }
                                    }
                                }
                            }
                        } catch (NullPointerException e) {
                        }

                        try {
                            for (TipoDocumento td : tipoDocumentosDisponibles) {
                                if (tipoDocumentoDigitalizar.contains(td.getTipoDocumento().trim())) {
                                    if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                                        infoDocumento.setTipoDocDatoAdicional(true);
                                        tiene = true;
                                        expediente.setTipoDocumento(tipoDocumentoDigitalizar);
                                        expediente.setIdTipoDocumento(td.getIdTipoDocumento());
                                        expediente.setAccion("guardar");
                                        expediente.setNumeroDocumento(0);
                                        expediente.setVersionDocumento(0);

                                        session.setAttribute("expediente", expediente);

                                        Window window = herramientas.crearVentanaModal("/datosAdicionales.zul");// Executions.createComponents("/datosAdicionales.zul", null, null);
                                        window.doModal();

                                        session = herramientas.crearSesion();

                                        lstDatosAdicionales = (List<DatoAdicional>) session.getAttribute("lstDatosAdicionales");

//                                        DatoAdicional dag = new DatoAdicional(expediente, false, this, 0, 0);
                                        for (DatoAdicional da : lstDatosAdicionales) {
                                            com.develcom.documento.DatoAdicional da1 = new com.develcom.documento.DatoAdicional();

                                            da1.setCodigo(da.getCodigo());
                                            da1.setIdDatoAdicional(da.getIdDatoAdicional());
                                            da1.setIdTipoDocumento(da.getIdTipoDocumento());
                                            da1.setIdValor(da.getIdValor());
                                            da1.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
                                            da1.setNumeroDocumento(da.getNumeroDocumento());
                                            da1.setTipo(da.getTipo());
                                            da1.setValor(da.getValor());
                                            da1.setVersion(da.getVersion());

                                            infoDocumento.getLsDatosAdicionales().add(da1);
                                            flag = true;
                                        }
                                        break;
                                    } else {
                                        tiene = false;
                                    }
                                }
                            }
                            if (!tiene) {
                                flag = true;
                            }
                        } catch (NullPointerException e) {
                        }

                        expediente.setTipoDocumento(tipoDocumentoDigitalizar);
                        expediente.setIdTipoDocumento(idDoc);

                        session.setAttribute("expediente", expediente);

                        infoDocumento.setTipoDocumento(tipoDocumentoDigitalizar);
                        infoDocumento.setIdDocumento(idDoc);
                        infoDocumento.setIdExpediente(txtExpediente.getValue());
                        infoDocumento.setReDigitalizo(false);

                        if (configuracion.isCalidadActivo()) {
                            infoDocumento.setEstatus(0);
                        } else {
                            infoDocumento.setEstatus(1);
                        }

                        traza.trace("digitalizando el documento " + tipoDocumentoDigitalizar, Level.INFO);
                        traza.trace("version del documento " + infoDocumento.getVersion(), Level.INFO);
                        traza.trace("numero del documento " + infoDocumento.getNumeroDocumento(), Level.INFO);
                        if (flag) {
                            new EscaneaDocumento("guardar", infoDocumento).escanearDocumento();
                            
//                            Principal principal = new Principal();

                            session = herramientas.crearSesion();
                            expediente = (Expediente) session.getAttribute("expediente");
                            if (expediente.isResultado()) {
                                this.buscarDocumentosDigitalizado();
                            }
                        }

                    } else {
                        herramientas.info("Por favor seleccione el documento del arbol digitalizados,\n y luego use los botones segun su preferencia");
                    }

                } else {
                    herramientas.warn("Debe elegir un tipo de documento para ser digitalizado");
                }

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (NullPointerException e) {
            traza.trace("no selecciono documento", Level.ERROR, e);
            herramientas.error("Debe seleccionar un Tipo de Documento", e);
        } catch (DatatypeConfigurationException ex) {
            traza.trace("error al digitalizar el documento", Level.ERROR, ex);
            herramientas.error("Problemas al subir el documento", ex);
        } catch (DW4JException e) {
            traza.trace("", Level.ERROR, e);
            herramientas.error("", e);
        }
    }

    /**
     * Redigitaliza un documento rechazado
     */
    @Listen("onClick = #btnScanRechazado")
    public void digitalizarRechazado() {

        List<DatoAdicional> lstDatosAdicionales;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;
        String tipoDocumento;
        int dig = 0, idDoc = 0;
        InfoDocumento infoDocumento;
        XMLGregorianCalendar xmlFechaVencimiento;
        GregorianCalendar calendar = new GregorianCalendar();
        Date fecha;
        DatatypeFactory dtf;
        boolean flag = false, tiene = true;

        try {

            if (session != null) {

                value = arbolDocDisp.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumento = dna.getTipoDocumento();
                    traza.trace("tipo de documento rechazado seleccionado " + tipoDocumento, Level.INFO);

                    int n = Messagebox.show("Desea rechazar el documento \n(" + tipoDocumento + ") del expediente \n" + txtExpediente.getValue(),
                            "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                    respMensaje = evt.getName().equalsIgnoreCase(Messagebox.ON_YES);
                                }
                            });

//                    if (respMensaje) {
                    if (n == Messagebox.YES) {

                        infoDocumento = new InfoDocumento();

                        try {

                            for (InfoDocumento id : docDigitalizadoRecha) {

                                String nombre = id.getTipoDocumento().trim();
                                nombre = nombre.trim();
                                traza.trace("buscando tipo de documento rechazado para la fecha de vencimiento " + nombre, Level.INFO);

                                if (tipoDocumento.equalsIgnoreCase(nombre)) {

                                    for (TipoDocumento td : tipoDocumentosDisponibles) {
                                        if (tipoDocumento.contains(td.getTipoDocumento().trim())) {

                                            idDoc = td.getIdTipoDocumento();

                                            if (td.getVencimiento().equalsIgnoreCase("1")) {
                                                if (fechaVencimiento.getValue() != null) {

                                                    fecha = fechaVencimiento.getValue();
                                                    calendar.setTime(fecha);
                                                    dtf = DatatypeFactory.newInstance();
                                                    xmlFechaVencimiento = dtf.newXMLGregorianCalendar(calendar);
                                                    infoDocumento.setFechaVencimiento(xmlFechaVencimiento);

                                                    traza.trace("fecha de vencimiento " + sdf.format(fecha.getTime()), Level.INFO);
                                                    traza.trace("fecha de vencimiento " + xmlFechaVencimiento, Level.INFO);
                                                    break;
                                                } else {
                                                    throw new DW4JException("Debe agregar una fecha de vencimiento");
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        } catch (NullPointerException e) {
                        }

                        try {

                            for (InfoDocumento id : docDigitalizadoRecha) {

                                String nombre = id.getTipoDocumento().trim();
                                nombre = nombre.trim();
                                traza.trace("buscando tipo de documento rechazado para la fecha de vencimiento " + nombre, Level.INFO);

                                if (tipoDocumento.equalsIgnoreCase(nombre)) {
                                    for (TipoDocumento td : tipoDocumentosDisponibles) {
                                        if (tipoDocumento.contains(td.getTipoDocumento().trim())) {
                                            if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                                                infoDocumento.setTipoDocDatoAdicional(true);
                                                tiene = true;
                                                expediente.setTipoDocumento(tipoDocumento);
                                                expediente.setIdTipoDocumento(td.getIdTipoDocumento());
                                                expediente.setAccion("guardar");
                                                expediente.setNumeroDocumento(0);
                                                expediente.setVersionDocumento(0);

                                                session.setAttribute("expediente", expediente);

                                                Window window = herramientas.crearVentanaModal("/datosAdicionales.zul");// Executions.createComponents("/datosAdicionales.zul", null, null);
                                                window.doModal();

                                                session = herramientas.crearSesion();

                                                lstDatosAdicionales = (List<DatoAdicional>) session.getAttribute("lstDatosAdicionales");

                                                for (DatoAdicional da : lstDatosAdicionales) {
                                                    com.develcom.documento.DatoAdicional da1 = new com.develcom.documento.DatoAdicional();

                                                    da1.setCodigo(da.getCodigo());
                                                    da1.setIdDatoAdicional(da.getIdDatoAdicional());
                                                    da1.setIdTipoDocumento(da.getIdTipoDocumento());
                                                    da1.setIdValor(da.getIdValor());
                                                    da1.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
                                                    da1.setNumeroDocumento(da.getNumeroDocumento());
                                                    da1.setTipo(da.getTipo());
                                                    da1.setValor(da.getValor());
                                                    da1.setVersion(da.getVersion());

                                                    infoDocumento.getLsDatosAdicionales().add(da1);
                                                    flag = true;
                                                }
                                                break;
                                            } else {
                                                tiene = false;
                                            }
                                        }
                                    }
                                    if (!tiene) {
                                        flag = true;
                                    }
                                }
                            }

                        } catch (NullPointerException e) {
                        }

                        expediente.setTipoDocumento(tipoDocumento);
                        expediente.setIdTipoDocumento(idDoc);

                        session.setAttribute("expediente", expediente);

                        infoDocumento.setTipoDocumento(tipoDocumento);
                        infoDocumento.setIdDocumento(idDoc);
                        infoDocumento.setIdExpediente(txtExpediente.getValue());
                        infoDocumento.setReDigitalizo(true);

                        if (configuracion.isCalidadActivo()) {
                            infoDocumento.setEstatus(0);
                        } else {
                            infoDocumento.setEstatus(1);
                        }

                        traza.trace("digitalizando el documento " + tipoDocumento, Level.INFO);
                        traza.trace("version del documento " + infoDocumento.getVersion(), Level.INFO);
                        traza.trace("numero del documento " + infoDocumento.getNumeroDocumento(), Level.INFO);
                        if (flag) {
                            new EscaneaDocumento("guardar", infoDocumento).escanearDocumento();

                            session = herramientas.crearSesion();
                            expediente = (Expediente) session.getAttribute("expediente");
                            if (expediente.isResultado()) {
                                this.buscarDocumentosDigitalizado();
                                this.buscarDocumentosRechazados();
                            }
                        }

                    } else {
                        herramientas.info("Por favor seleccione el documento del arbol digitalizados,\n y luego use los botones segun su preferencia");
                    }

                } else {
                    herramientas.warn("Debe elegir un tipo de documento para ser digitalizado");
                }

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (NullPointerException e) {
            traza.trace("no selecciono documento", Level.ERROR, e);
            herramientas.error("Debe seleccionar un Tipo de Documento", e);
        } catch (DatatypeConfigurationException ex) {
            traza.trace("error al digitalizar el documento", Level.ERROR, ex);
            herramientas.error("Problemas al subir el documento", ex);
        } catch (DW4JException e) {
            traza.trace("", Level.ERROR, e);
            herramientas.error("", e);
        }
    }

    /**
     * Digitaliza un documento desde un archivo
     *
     * @param event
     */
    @Listen("onUpload = #btnExplDisponible")
    public void buscarArchivoNuevo(UploadEvent event) {

        Media media;
        File archivoDescargado;
        List<DatoAdicional> lstDatosAdicionales;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;
        String tipoDocumentoDigitalizar, fileName, filePath, ext, rutaTemp, realPath;
        int dig = 0, idDoc = 0;
        InfoDocumento infoDocumento;
        XMLGregorianCalendar xmlFechaVencimiento;
        GregorianCalendar calendar = new GregorianCalendar();
        Date fecha;
        DatatypeFactory dtf;
        boolean flag = false, tiene = true;

        try {

            if (session != null) {

                value = arbolDocDisp.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumentoDigitalizar = dna.getTipoDocumento();
                    traza.trace("tipo de documento rechazado seleccionado " + tipoDocumentoDigitalizar, Level.INFO);

                    Messagebox.show("Desea  crear un nuevo documento \n(" + tipoDocumentoDigitalizar + ") del expediente \n" + txtExpediente.getValue(),
                            "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                    respMensaje = evt.getName().equalsIgnoreCase(Messagebox.ON_YES);
                                }
                            });

                    if (respMensaje) {

                        infoDocumento = new InfoDocumento();

                        try {
                            for (TipoDocumento td : tipoDocumentosDisponibles) {
                                if (tipoDocumentoDigitalizar.contains(td.getTipoDocumento().trim())) {

                                    idDoc = td.getIdTipoDocumento();

                                    if (td.getVencimiento().equalsIgnoreCase("1")) {
                                        if (fechaVencimiento.getValue() != null) {

                                            fecha = fechaVencimiento.getValue();
                                            calendar.setTime(fecha);
                                            dtf = DatatypeFactory.newInstance();
                                            xmlFechaVencimiento = dtf.newXMLGregorianCalendar(calendar);
                                            infoDocumento.setFechaVencimiento(xmlFechaVencimiento);

                                            traza.trace("fecha de vencimiento " + sdf.format(fecha.getTime()), Level.INFO);
                                            traza.trace("fecha de vencimiento " + xmlFechaVencimiento, Level.INFO);
                                            break;
                                        } else {
                                            throw new DW4JException("Debe agregar una fecha de vencimiento");
                                        }
                                    }
                                }
                            }
                        } catch (NullPointerException e) {
                        }

                        try {
                            for (TipoDocumento td : tipoDocumentosDisponibles) {
                                if (tipoDocumentoDigitalizar.contains(td.getTipoDocumento().trim())) {
                                    if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                                        infoDocumento.setTipoDocDatoAdicional(true);
                                        tiene = true;
                                        expediente.setTipoDocumento(tipoDocumentoDigitalizar);
                                        expediente.setIdTipoDocumento(td.getIdTipoDocumento());
                                        expediente.setAccion("Guardar");
                                        expediente.setNumeroDocumento(0);
                                        expediente.setVersionDocumento(0);

                                        session.setAttribute("expediente", expediente);

                                        Window window = herramientas.crearVentanaModal("/datosAdicionales.zul");
                                        window.doModal();

                                        session = herramientas.crearSesion();

                                        lstDatosAdicionales = (List<DatoAdicional>) session.getAttribute("lstDatosAdicionales");

                                        for (DatoAdicional da : lstDatosAdicionales) {
                                            com.develcom.documento.DatoAdicional da1 = new com.develcom.documento.DatoAdicional();

                                            da1.setCodigo(da.getCodigo());
                                            da1.setIdDatoAdicional(da.getIdDatoAdicional());
                                            da1.setIdTipoDocumento(da.getIdTipoDocumento());
                                            da1.setIdValor(da.getIdValor());
                                            da1.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
                                            da1.setNumeroDocumento(da.getNumeroDocumento());
                                            da1.setTipo(da.getTipo());
                                            da1.setValor(da.getValor());
                                            da1.setVersion(da.getVersion());

                                            infoDocumento.getLsDatosAdicionales().add(da1);
                                            flag = true;
                                        }
                                        break;
                                    } else {
                                        tiene = false;
                                    }
                                }
                            }
                            if (!tiene) {
                                flag = true;
                            }
                        } catch (NullPointerException e) {
                        }

                        expediente.setTipoDocumento(tipoDocumentoDigitalizar);
                        expediente.setIdTipoDocumento(idDoc);

                        session.setAttribute("expediente", expediente);

                        infoDocumento.setTipoDocumento(tipoDocumentoDigitalizar);
                        infoDocumento.setIdDocumento(idDoc);
                        infoDocumento.setIdExpediente(txtExpediente.getValue());
                        infoDocumento.setReDigitalizo(false);

                        if (configuracion.isCalidadActivo()) {
                            infoDocumento.setEstatus(0);
                        } else {
                            infoDocumento.setEstatus(1);
                        }

                        traza.trace("digitalizando el documento " + tipoDocumentoDigitalizar, Level.INFO);
                        traza.trace("version del documento " + infoDocumento.getVersion(), Level.INFO);
                        traza.trace("numero del documento " + infoDocumento.getNumeroDocumento(), Level.INFO);

                        if (flag) {

                            media = event.getMedia();

                            realPath = herramientas.getRutaAplicacion();
                            rutaTemp = configuracion.getPathTmp();
                            traza.trace("ruta aplicacion " + realPath, Level.INFO);
                            traza.trace("ruta temporal " + rutaTemp, Level.INFO);
                            filePath = realPath + "/" + rutaTemp + "/";
                            traza.trace("ruta completa " + filePath, Level.INFO);

                            fileName = media.getName();
                            traza.trace("nombre archivo " + fileName, Level.INFO);
                            ext = media.getFormat();
                            traza.trace("ext archivo " + ext, Level.INFO);

                            if (ext.equalsIgnoreCase("pdf")) {

                                archivoDescargado = new File(filePath + fileName);
                                traza.trace("archivo descargado " + archivoDescargado, Level.INFO);
                                traza.trace("media " + media, Level.INFO);
                                Files.copy(archivoDescargado, media.getStreamData());
                                new EscaneaDocumento("Guardar", infoDocumento).codificar(archivoDescargado, false);

                            } else if ((ext.equalsIgnoreCase("jpg")) || (ext.equalsIgnoreCase("jpge"))) {

                                archivoDescargado = new File(filePath + fileName);
                                traza.trace("archivo descargado " + archivoDescargado, Level.INFO);
                                traza.trace("media " + media, Level.INFO);
                                Files.copy(archivoDescargado, media.getStreamData());
                                new EscaneaDocumento("Guardar", infoDocumento).codificar(archivoDescargado, false);

                            } else if ((ext.equalsIgnoreCase("tif") || (ext.equalsIgnoreCase("tiff")))) {

                                archivoDescargado = new File(filePath + fileName);
                                traza.trace("archivo descargado " + archivoDescargado, Level.INFO);
                                traza.trace("media " + media, Level.INFO);
                                Files.copy(archivoDescargado, media.getStreamData());
                                new EscaneaDocumento("Guardar", infoDocumento).codificar(archivoDescargado, false);

                            } else {
                                herramientas.warn("El documento debe ser un pdf, jpg o tif");
                            }

                            session = herramientas.crearSesion();
                            expediente = (Expediente) session.getAttribute("expediente");
                            if (expediente.isResultado()) {
                                this.buscarDocumentosDigitalizado();
                            }
                        }

                    } else {
                        herramientas.info("Por favor seleccione el documento del arbol digitalizados,\n y luego use los botones segun su preferencia");
                    }

                } else {
                    herramientas.warn("Debe elegir un tipo de documento para ser digitalizado");
                }

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (NullPointerException e) {
            traza.trace("no selecciono documento", Level.ERROR, e);
            herramientas.error("Debe seleccionar un Tipo de Documento", e);
        } catch (DatatypeConfigurationException | IOException ex) {
            traza.trace("error al digitalizar el documento", Level.ERROR, ex);
            herramientas.error("Problemas al subir el documento", ex);
        } catch (DW4JException e) {
            traza.trace("", Level.ERROR, e);
            herramientas.error("", e);
        }
    }

    /**
     * ReDigitaliza un documento rechazado desde un archivo
     *
     * @param event
     */
    @Listen("onUpload = #btnExplRechazado")
    public void buscarArchivoRechazado(UploadEvent event) {

        Media media;
        File archivoDescargado;
        List<DatoAdicional> lstDatosAdicionales;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;
        String tipoDocumentoDigitalizar, fileName, filePath, ext, rutaTemp, realPath;
        int dig = 0, idDoc = 0;
        InfoDocumento infoDocumento;
        XMLGregorianCalendar xmlFechaVencimiento;
        GregorianCalendar calendar = new GregorianCalendar();
        Date fecha;
        DatatypeFactory dtf;
        boolean flag = false, tiene = true;

        try {

            if (session != null) {

                value = arbolDocDisp.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumentoDigitalizar = dna.getTipoDocumento();
                    traza.trace("tipo de documento rechazado seleccionado " + tipoDocumentoDigitalizar, Level.INFO);

                    Messagebox.show("Desea rechazar un nuevo documento \n(" + tipoDocumentoDigitalizar + ") del expediente \n" + txtExpediente.getValue(),
                            "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                    respMensaje = evt.getName().equalsIgnoreCase(Messagebox.ON_YES);
                                }
                            });

                    if (respMensaje) {

                        infoDocumento = new InfoDocumento();

                        try {

                            for (InfoDocumento id : docDigitalizadoRecha) {
                                String nombre = id.getTipoDocumento().trim();
                                nombre = nombre.trim();
                                traza.trace("buscando tipo de documento rechazado para la fecha de vencimiento " + nombre, Level.INFO);

                                for (TipoDocumento td : tipoDocumentosDisponibles) {
                                    if (tipoDocumentoDigitalizar.contains(td.getTipoDocumento().trim())) {

                                        idDoc = td.getIdTipoDocumento();

                                        if (td.getVencimiento().equalsIgnoreCase("1")) {
                                            if (fechaRechazado.getValue() != null) {

                                                fecha = fechaRechazado.getValue();
                                                calendar.setTime(fecha);
                                                dtf = DatatypeFactory.newInstance();
                                                xmlFechaVencimiento = dtf.newXMLGregorianCalendar(calendar);
                                                infoDocumento.setFechaVencimiento(xmlFechaVencimiento);

                                                traza.trace("fecha de vencimiento " + sdf.format(fecha.getTime()), Level.INFO);
                                                traza.trace("fecha de vencimiento " + xmlFechaVencimiento, Level.INFO);
                                                break;
                                            } else {
                                                throw new DW4JException("Debe agregar una fecha de vencimiento");
                                            }
                                        }
                                    }
                                }
                            }

                        } catch (NullPointerException e) {
                        }

                        try {

                            for (InfoDocumento id : docDigitalizadoRecha) {
                                String nombre = id.getTipoDocumento().trim();
                                nombre = nombre.trim();
                                traza.trace("buscando tipo de documento rechazado para la fecha de vencimiento " + nombre, Level.INFO);

                                for (TipoDocumento td : tipoDocumentosDisponibles) {
                                    if (tipoDocumentoDigitalizar.contains(td.getTipoDocumento().trim())) {
                                        if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                                            infoDocumento.setTipoDocDatoAdicional(true);
                                            tiene = true;
                                            expediente.setTipoDocumento(tipoDocumentoDigitalizar);
                                            expediente.setIdTipoDocumento(td.getIdTipoDocumento());
                                            expediente.setAccion("Guardar");
                                            expediente.setNumeroDocumento(0);
                                            expediente.setVersionDocumento(0);

                                            session.setAttribute("expediente", expediente);

                                            Window window = herramientas.crearVentanaModal("/datosAdicionales.zul");
                                            window.doModal();

                                            session = herramientas.crearSesion();

                                            lstDatosAdicionales = (List<DatoAdicional>) session.getAttribute("lstDatosAdicionales");

                                            for (DatoAdicional da : lstDatosAdicionales) {
                                                com.develcom.documento.DatoAdicional da1 = new com.develcom.documento.DatoAdicional();

                                                da1.setCodigo(da.getCodigo());
                                                da1.setIdDatoAdicional(da.getIdDatoAdicional());
                                                da1.setIdTipoDocumento(da.getIdTipoDocumento());
                                                da1.setIdValor(da.getIdValor());
                                                da1.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
                                                da1.setNumeroDocumento(da.getNumeroDocumento());
                                                da1.setTipo(da.getTipo());
                                                da1.setValor(da.getValor());
                                                da1.setVersion(da.getVersion());

                                                infoDocumento.getLsDatosAdicionales().add(da1);
                                                flag = true;
                                            }
                                            break;
                                        } else {
                                            tiene = false;
                                        }
                                    }
                                }
                                if (!tiene) {
                                    flag = true;
                                }
                            }

                        } catch (NullPointerException e) {
                        }

                        expediente.setTipoDocumento(tipoDocumentoDigitalizar);
                        expediente.setIdTipoDocumento(idDoc);

                        session.setAttribute("expediente", expediente);

                        infoDocumento.setTipoDocumento(tipoDocumentoDigitalizar);
                        infoDocumento.setIdDocumento(idDoc);
                        infoDocumento.setIdExpediente(txtExpediente.getValue());
                        infoDocumento.setReDigitalizo(true);

                        if (configuracion.isCalidadActivo()) {
                            infoDocumento.setEstatus(0);
                        } else {
                            infoDocumento.setEstatus(1);
                        }

                        traza.trace("digitalizando el documento " + tipoDocumentoDigitalizar, Level.INFO);
                        traza.trace("version del documento " + infoDocumento.getVersion(), Level.INFO);
                        traza.trace("numero del documento " + infoDocumento.getNumeroDocumento(), Level.INFO);

                        if (flag) {

                            media = event.getMedia();

                            realPath = herramientas.getRutaAplicacion();
                            rutaTemp = configuracion.getPathTmp();
                            filePath = realPath + "/" + rutaTemp + "/";

                            fileName = media.getName();
                            ext = media.getFormat();

                            if (ext.equalsIgnoreCase("pdf")) {

                                archivoDescargado = new File(filePath + fileName);
                                Files.copy(archivoDescargado, media.getStreamData());
                                new EscaneaDocumento("Guardar", infoDocumento).codificar(archivoDescargado, false);

                            } else if ((ext.equalsIgnoreCase("jpg")) || (ext.equalsIgnoreCase("jpge"))) {

                                archivoDescargado = new File(filePath + fileName);
                                Files.copy(archivoDescargado, media.getStreamData());
                                new EscaneaDocumento("Guardar", infoDocumento).codificar(archivoDescargado, false);

                            } else if ((ext.equalsIgnoreCase("tif") || (ext.equalsIgnoreCase("tiff")))) {

                                archivoDescargado = new File(filePath + fileName);
                                Files.copy(archivoDescargado, media.getStreamData());
                                new EscaneaDocumento("Guardar", infoDocumento).codificar(archivoDescargado, false);

                            } else {
                                herramientas.warn("El documento debe ser un pdf, jpg o tif");
                            }

                            session = herramientas.crearSesion();
                            expediente = (Expediente) session.getAttribute("expediente");
                            if (expediente.isResultado()) {
                                this.buscarDocumentosDigitalizado();
                                this.buscarDocumentosRechazados();
                            }

                        }

                    } else {
                        herramientas.info("Por favor seleccione el documento del arbol digitalizados,\n y luego use los botones segun su preferencia");
                    }

                } else {
                    herramientas.warn("Debe elegir un tipo de documento para ser digitalizado");
                }

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (NullPointerException e) {
            traza.trace("no selecciono documento", Level.ERROR, e);
            herramientas.error("Debe seleccionar un Tipo de Documento", e);
        } catch (DatatypeConfigurationException | IOException ex) {
            traza.trace("error al digitalizar el documento", Level.ERROR, ex);
            herramientas.error("Problemas al subir el documento", ex);
        } catch (DW4JException e) {
            traza.trace("", Level.ERROR, e);
            herramientas.error("", e);
        }
    }

    /**
     * Muestra un documento digitalizado
     */
    @Listen("onClick =#btnVisualizar")
    public void vizualizarDocumento() {

        InfoDocumento infoDocumento;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;
        String tipoDocumento;

        try {

            if (session != null) {

                value = arbolDocDigit.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumento = dna.getTipoDocumento();
                    traza.trace("tipo de documento rechazado seleccionado " + tipoDocumento, Level.INFO);

                    for (InfoDocumento infDoc : documentosDigitalizado) {
                        traza.trace("tipo de documento digitalizado " + infDoc.getTipoDocumento().trim(), Level.INFO);

                        if (infDoc.getTipoDocumento().trim().contains(tipoDocumento)) {

                            traza.trace("tipo de documento a mostrar " + tipoDocumento, Level.INFO);
                            traza.trace("nombre del archivo " + infDoc.getNombreArchivo(), Level.INFO);
                            traza.trace("ruta del archivo " + infDoc.getRutaArchivo(), Level.INFO);
                            traza.trace("version del archivo " + infDoc.getVersion(), Level.INFO);
                            traza.trace("id del archivo " + infDoc.getIdInfoDocumento(), Level.INFO);
                            traza.trace("numero del documento (archivo) " + infDoc.getNumeroDocumento(), Level.INFO);
                            traza.trace("id del documento " + infDoc.getIdDocumento(), Level.INFO);
                            traza.trace("fecha vencimiento del archivo " + infDoc.getFechaVencimiento(), Level.INFO);
                            traza.trace("estatus documento " + infDoc.getEstatusDocumento(), Level.INFO);
                            traza.trace("formato del documento " + infDoc.getFormato(), Level.INFO);

                            infoDocumento = infDoc;

                            expediente.setAccion("vizualizar");
                            expediente.setTipoDocumento(tipoDocumento);
                            expediente.setIdTipoDocumento(dna.getIdDocumento());

                            session.setAttribute("infoDocumento", infoDocumento);

                            Window window = herramientas.crearVentanaModal("/documento.zul");
                            window.doModal();

                            break;
                        }
                    }
                }
            }

        } catch (NullPointerException e) {
            traza.trace("no selecciono documento", Level.ERROR, e);
            herramientas.error("Debe seleccionar un Tipo de Documento", e);
        }
    }

    /**
     * Versiona un documento desde un archivo
     *
     * @param event
     */
    @Listen("onUpload = #btnVersionar")
    public void versionarDocumento(UploadEvent event) {

        Media media;
        File archivoDescargado;
        List<DatoAdicional> lstDatosAdicionales;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;
        String tipoDocumento, fileName, filePath, ext, rutaTemp, realPath, tiDo;
        int numDoc, idDoc, version;
        InfoDocumento infoDocumento;
        XMLGregorianCalendar xmlFechaVencimiento = null;
        GregorianCalendar calendar = new GregorianCalendar();
        Date fecha;
        DatatypeFactory dtf;
        boolean flag = false, tiene = true;

        try {

            if (session != null) {

                value = arbolDocDigit.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumento = dna.getTipoDocumento();
                    traza.trace("tipo de documento versionado seleccionado " + tipoDocumento, Level.INFO);

                    if (tipoDocumento.contains("Pendiente")) {
                        throw new DW4JException("Documento con estatus pendiente no se puede versionar");
                    }

                    Messagebox.show("Desea versionar documento \n(" + tipoDocumento + ") del expediente \n" + txtExpediente.getValue(),
                            "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                    respMensaje = evt.getName().equalsIgnoreCase(Messagebox.ON_YES);
                                }
                            });

                    if (respMensaje) {

                        infoDocumento = new InfoDocumento();

                        try {

                            for (InfoDocumento id : documentosDigitalizado) {
                                traza.trace("buscando tipo de documento para la fecha de vencimiento " + id.getTipoDocumento().trim(), Level.INFO);

                                String da = "";
                                if (id.isTipoDocDatoAdicional()) {
                                    List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                                    int size = datosAdicionales.size(), i = 1;
                                    for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                        if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                            if (i == size) {
                                                da = da + " " + datAd.getValor().toString();
                                            } else {
                                                da = da + " " + datAd.getValor().toString() + ",";
                                            }
                                        } else {
                                            if (i == size) {
                                                da = da + " " + datAd.getValor();
                                            } else {
                                                da = da + " " + datAd.getValor() + ",";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                da = "(" + da.trim() + ")";

                                tiDo = tipoDocumento + " " + da;

                                if (tiDo.contains(id.getTipoDocumento().trim())) {

                                    for (TipoDocumento td : tipoDocumentosDisponibles) {

                                        if (configuracion.isCalidadActivo()) {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento() + " " + id.getEstatusDocumento();
                                        } else {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento();
                                        }
                                        tiDo = tiDo.trim();

                                        if (tipoDocumento.contains(tiDo)) {

                                            if (td.getVencimiento().equalsIgnoreCase("1")) {
                                                if (fechaVencimiento.getValue() != null) {

                                                    fecha = fechaVencimiento.getValue();
                                                    calendar.setTime(fecha);
                                                    dtf = DatatypeFactory.newInstance();
                                                    xmlFechaVencimiento = dtf.newXMLGregorianCalendar(calendar);
                                                    infoDocumento.setFechaVencimiento(xmlFechaVencimiento);

                                                    traza.trace("fecha de vencimiento " + sdf.format(fecha.getTime()), Level.INFO);
                                                    traza.trace("fecha de vencimiento " + xmlFechaVencimiento, Level.INFO);
                                                    break;
                                                } else {
                                                    throw new DW4JException("Debe agregar una fecha de vencimiento");
                                                }
                                            }
                                        }
                                    }

                                }

                            }

                        } catch (NullPointerException e) {
                        }

                        try {

                            for (InfoDocumento id : documentosDigitalizado) {
                                traza.trace("buscando tipo de documento para los datos adicionales " + id.getTipoDocumento().trim(), Level.INFO);

                                String da2 = "";
                                if (id.isTipoDocDatoAdicional()) {
                                    List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                                    int size = datosAdicionales.size(), i = 1;
                                    for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                        if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                            if (i == size) {
                                                da2 = da2 + " " + datAd.getValor().toString();
                                            } else {
                                                da2 = da2 + " " + datAd.getValor().toString() + ",";
                                            }
                                        } else {
                                            if (i == size) {
                                                da2 = da2 + " " + datAd.getValor();
                                            } else {
                                                da2 = da2 + " " + datAd.getValor() + ",";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                da2 = "(" + da2.trim() + ")";

                                tiDo = tipoDocumento + " " + da2;

                                if (tiDo.contains(id.getTipoDocumento().trim())) {
                                    for (TipoDocumento td : tipoDocumentosDisponibles) {

                                        if (configuracion.isCalidadActivo()) {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento() + " " + id.getEstatusDocumento();
                                        } else {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento();
                                        }
                                        tiDo = tiDo.trim();
                                        if (tipoDocumento.contains(tiDo)) {
                                            if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                                                infoDocumento.setTipoDocDatoAdicional(true);
                                                tiene = true;
                                                expediente.setTipoDocumento(tipoDocumento);
                                                expediente.setIdTipoDocumento(td.getIdTipoDocumento());
                                                expediente.setAccion("actualizar");
                                                expediente.setNumeroDocumento(id.getNumeroDocumento());
                                                expediente.setVersionDocumento(id.getVersion());

                                                session.setAttribute("expediente", expediente);

                                                Window window = herramientas.crearVentanaModal("/datosAdicionales.zul");
                                                window.doModal();

                                                session = herramientas.crearSesion();

                                                lstDatosAdicionales = (List<DatoAdicional>) session.getAttribute("lstDatosAdicionales");

                                                for (DatoAdicional da : lstDatosAdicionales) {
                                                    com.develcom.documento.DatoAdicional da1 = new com.develcom.documento.DatoAdicional();

                                                    da1.setCodigo(da.getCodigo());
                                                    da1.setIdDatoAdicional(da.getIdDatoAdicional());
                                                    da1.setIdTipoDocumento(da.getIdTipoDocumento());
                                                    da1.setIdValor(da.getIdValor());
                                                    da1.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
                                                    da1.setNumeroDocumento(da.getNumeroDocumento());
                                                    da1.setTipo(da.getTipo());
                                                    da1.setValor(da.getValor());
                                                    da1.setVersion(da.getVersion());

                                                    infoDocumento.getLsDatosAdicionales().add(da1);
                                                    flag = true;
                                                }
                                                break;
                                            } else {
                                                tiene = false;
                                            }
                                        }
                                    }
                                    if (!tiene) {
                                        flag = true;
                                    }
                                }
                            }
                        } catch (NullPointerException e) {
                        }

                        if (!documentosDigitalizado.isEmpty()) {
                            for (InfoDocumento infoDoc : documentosDigitalizado) {

                                if (tipoDocumento.contains(infoDoc.getTipoDocumento().trim())) {

                                    idDoc = infoDoc.getIdDocumento();
                                    numDoc = infoDoc.getNumeroDocumento();

                                    infoDocumento = infoDoc;
                                    if (xmlFechaVencimiento != null) {
                                        infoDocumento.setFechaVencimiento(xmlFechaVencimiento);
                                    }

                                    expediente.setTipoDocumento(infoDoc.getTipoDocumento().trim());
                                    expediente.setIdTipoDocumento(idDoc);
                                    expediente.setIdInfoDocumento(infoDoc.getIdInfoDocumento());

                                    infoDocumento.setIdExpediente(expediente.getIdExpediente());

                                    traza.trace("actual version del documento " + infoDoc.getVersion(), Level.INFO);
                                    version = new GestionDocumentos().buscarVersionUltima(idDoc, expediente.getIdExpediente(), numDoc) + 1;
                                    traza.trace("version nueva del documento " + version, Level.INFO);
                                    infoDocumento.setVersion(version);

                                    if (configuracion.isCalidadActivo()) {
                                        infoDocumento.setEstatus(0);
                                    } else {
                                        infoDocumento.setEstatus(1);
                                    }

                                    traza.trace("version del documento " + infoDocumento.getVersion(), Level.INFO);
                                    traza.trace("numero del documento " + infoDocumento.getNumeroDocumento(), Level.INFO);

                                    session.setAttribute("expediente", expediente);

                                    if (flag) {

                                        media = event.getMedia();

                                        realPath = herramientas.getRutaAplicacion();
                                        rutaTemp = configuracion.getPathTmp();
                                        filePath = realPath + "/" + rutaTemp + "/";

                                        fileName = media.getName();
                                        ext = media.getFormat();

                                        if (ext.equalsIgnoreCase("pdf")) {

                                            archivoDescargado = new File(filePath + fileName);
                                            Files.copy(archivoDescargado, media.getStreamData());
                                            new EscaneaDocumento("versionar", infoDocumento).codificar(archivoDescargado, false);

                                        } else if ((ext.equalsIgnoreCase("jpg")) || (ext.equalsIgnoreCase("jpge"))) {

                                            archivoDescargado = new File(filePath + fileName);
                                            Files.copy(archivoDescargado, media.getStreamData());
                                            new EscaneaDocumento("versionar", infoDocumento).codificar(archivoDescargado, false);

                                        } else if ((ext.equalsIgnoreCase("tif") || (ext.equalsIgnoreCase("tiff")))) {

                                            archivoDescargado = new File(filePath + fileName);
                                            Files.copy(archivoDescargado, media.getStreamData());
                                            new EscaneaDocumento("versionar", infoDocumento).codificar(archivoDescargado, false);

                                        } else {
                                            herramientas.warn("El documento debe ser un pdf, jpg o tif");
                                        }
                                    }

                                    session = herramientas.crearSesion();
                                    expediente = (Expediente) session.getAttribute("expediente");
                                    if (expediente.isResultado()) {
                                        this.buscarDocumentosDigitalizado();
                                    }
                                    break;
                                }
                            }
                        }

                    } else {
                        herramientas.info("Por favor seleccione el documento del arbol digitalizados,\n y luego use los botones segun su preferencia");
                    }

                } else {
                    herramientas.warn("Debe elegir un tipo de documento para ser digitalizado");
                }

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (NullPointerException e) {
            traza.trace("no selecciono documento", Level.ERROR, e);
            herramientas.error("Debe seleccionar un Tipo de Documento", e);
        } catch (DatatypeConfigurationException | IOException ex) {
            traza.trace("error al digitalizar el documento", Level.ERROR, ex);
            herramientas.error("Problemas al subir el documento", ex);
        } catch (DW4JException e) {
            traza.trace("", Level.ERROR, e);
            herramientas.error("", e);
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al buscar la ultima version del documento", Level.ERROR, ex);
            herramientas.error("Problemas al buscar la ultima version del documento", ex);
        }
    }

    /**
     * Reemplaza un documento desde un archivo
     *
     * @param event
     */
    @Listen("onUpload = #btnReemplazar")
    public void reemplazarDocumento(UploadEvent event) {

        Media media;
        File archivoDescargado;
        List<DatoAdicional> lstDatosAdicionales;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;
        String tipoDocumento, fileName, filePath, ext, rutaTemp, realPath, tiDo;
        int idDoc;
        InfoDocumento infoDocumento;
        XMLGregorianCalendar xmlFechaVencimiento = null;
        GregorianCalendar calendar = new GregorianCalendar();
        Date fecha;
        DatatypeFactory dtf;
        boolean flag = false, tiene = true;

        try {

            if (session != null) {

                value = arbolDocDigit.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumento = dna.getTipoDocumento();
                    traza.trace("tipo de documento reemplazado seleccionado " + tipoDocumento, Level.INFO);

                    Messagebox.show("Desea reemplazar el documento \n(" + tipoDocumento + ") del expediente \n" + txtExpediente.getValue(),
                            "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {
                                    respMensaje = evt.getName().equalsIgnoreCase(Messagebox.ON_YES);
                                }
                            });

                    if (respMensaje) {

                        infoDocumento = new InfoDocumento();

                        try {

                            for (InfoDocumento id : documentosDigitalizado) {
                                traza.trace("buscando tipo de documento para la fecha de vencimiento " + id.getTipoDocumento().trim(), Level.INFO);

                                String da = "";
                                if (id.isTipoDocDatoAdicional()) {
                                    List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                                    int size = datosAdicionales.size(), i = 1;
                                    for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                        if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                            if (i == size) {
                                                da = da + " " + datAd.getValor().toString();
                                            } else {
                                                da = da + " " + datAd.getValor().toString() + ",";
                                            }
                                        } else {
                                            if (i == size) {
                                                da = da + " " + datAd.getValor();
                                            } else {
                                                da = da + " " + datAd.getValor() + ",";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                da = "(" + da.trim() + ")";

                                tiDo = tipoDocumento + " " + da;

                                if (tiDo.contains(id.getTipoDocumento().trim())) {

                                    for (TipoDocumento td : tipoDocumentosDisponibles) {

                                        if (configuracion.isCalidadActivo()) {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento() + " " + id.getEstatusDocumento();
                                        } else {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento();
                                        }
                                        tiDo = tiDo.trim();

                                        if (tipoDocumento.contains(tiDo)) {

                                            if (td.getVencimiento().equalsIgnoreCase("1")) {
                                                if (fechaVencimiento.getValue() != null) {

                                                    fecha = fechaVencimiento.getValue();
                                                    calendar.setTime(fecha);
                                                    dtf = DatatypeFactory.newInstance();
                                                    xmlFechaVencimiento = dtf.newXMLGregorianCalendar(calendar);
                                                    infoDocumento.setFechaVencimiento(xmlFechaVencimiento);

                                                    traza.trace("fecha de vencimiento " + sdf.format(fecha.getTime()), Level.INFO);
                                                    traza.trace("fecha de vencimiento " + xmlFechaVencimiento, Level.INFO);
                                                    break;
                                                } else {
                                                    throw new DW4JException("Debe agregar una fecha de vencimiento");
                                                }
                                            }
                                        }
                                    }

                                }

                            }

                        } catch (NullPointerException e) {
                        }

                        try {

                            for (InfoDocumento id : documentosDigitalizado) {
                                traza.trace("buscando tipo de documento para los datos adicionales " + id.getTipoDocumento().trim(), Level.INFO);

                                String da2 = "";
                                if (id.isTipoDocDatoAdicional()) {
                                    List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                                    int size = datosAdicionales.size(), i = 1;
                                    for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                        if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                            if (i == size) {
                                                da2 = da2 + " " + datAd.getValor().toString();
                                            } else {
                                                da2 = da2 + " " + datAd.getValor().toString() + ",";
                                            }
                                        } else {
                                            if (i == size) {
                                                da2 = da2 + " " + datAd.getValor();
                                            } else {
                                                da2 = da2 + " " + datAd.getValor() + ",";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                da2 = "(" + da2.trim() + ")";

                                tiDo = tipoDocumento + " " + da2;

                                if (tiDo.contains(id.getTipoDocumento().trim())) {
                                    for (TipoDocumento td : tipoDocumentosDisponibles) {

                                        if (configuracion.isCalidadActivo()) {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento() + " " + id.getEstatusDocumento();
                                        } else {
                                            tiDo = td.getTipoDocumento() + " - " + id.getNumeroDocumento();
                                        }
                                        tiDo = tiDo.trim();

                                        if (tipoDocumento.contains(tiDo)) {
                                            if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                                                infoDocumento.setTipoDocDatoAdicional(true);
                                                tiene = true;
                                                expediente.setTipoDocumento(tipoDocumento);
                                                expediente.setIdTipoDocumento(td.getIdTipoDocumento());
                                                expediente.setAccion("actualizar");
                                                expediente.setNumeroDocumento(id.getNumeroDocumento());
                                                expediente.setVersionDocumento(id.getVersion());

                                                session.setAttribute("expediente", expediente);

                                                Window window = herramientas.crearVentanaModal("/datosAdicionales.zul");
                                                window.doModal();

                                                session = herramientas.crearSesion();

                                                lstDatosAdicionales = (List<DatoAdicional>) session.getAttribute("lstDatosAdicionales");

                                                for (DatoAdicional da : lstDatosAdicionales) {
                                                    com.develcom.documento.DatoAdicional da1 = new com.develcom.documento.DatoAdicional();

                                                    da1.setCodigo(da.getCodigo());
                                                    da1.setIdDatoAdicional(da.getIdDatoAdicional());
                                                    da1.setIdTipoDocumento(da.getIdTipoDocumento());
                                                    da1.setIdValor(da.getIdValor());
                                                    da1.setIndiceDatoAdicional(da.getIndiceDatoAdicional());
                                                    da1.setNumeroDocumento(da.getNumeroDocumento());
                                                    da1.setTipo(da.getTipo());
                                                    da1.setValor(da.getValor());
                                                    da1.setVersion(da.getVersion());

                                                    infoDocumento.getLsDatosAdicionales().add(da1);
                                                    flag = true;
                                                }
                                                break;
                                            } else {
                                                tiene = false;
                                            }
                                        }
                                    }
                                    if (!tiene) {
                                        flag = true;
                                    }

                                }

                            }
                        } catch (NullPointerException e) {
                        }

                        if (!documentosDigitalizado.isEmpty()) {
                            for (InfoDocumento infoDoc : documentosDigitalizado) {

                                if (tipoDocumento.contains(infoDoc.getTipoDocumento().trim())) {

                                    idDoc = infoDoc.getIdDocumento();

                                    infoDocumento = infoDoc;
                                    if (xmlFechaVencimiento != null) {
                                        infoDocumento.setFechaVencimiento(xmlFechaVencimiento);
                                    }

                                    expediente.setTipoDocumento(infoDoc.getTipoDocumento().trim());
                                    expediente.setIdTipoDocumento(idDoc);
                                    expediente.setIdInfoDocumento(infoDoc.getIdInfoDocumento());

                                    infoDocumento.setIdExpediente(expediente.getIdExpediente());

                                    if (configuracion.isCalidadActivo()) {
                                        infoDocumento.setEstatus(0);
                                    } else {
                                        infoDocumento.setEstatus(1);
                                    }

                                    traza.trace("version del documento " + infoDocumento.getVersion(), Level.INFO);
                                    traza.trace("numero del documento " + infoDocumento.getNumeroDocumento(), Level.INFO);

                                    session.setAttribute("expediente", expediente);

                                    if (flag) {

                                        media = event.getMedia();

                                        realPath = herramientas.getRutaAplicacion();
                                        rutaTemp = configuracion.getPathTmp();
                                        filePath = realPath + "/" + rutaTemp + "/";

                                        fileName = media.getName();
                                        ext = media.getFormat();

                                        if (ext.equalsIgnoreCase("pdf")) {

                                            archivoDescargado = new File(filePath + fileName);
                                            Files.copy(archivoDescargado, media.getStreamData());
                                            new EscaneaDocumento("reemplazar", infoDocumento).codificar(archivoDescargado, false);

                                        } else if ((ext.equalsIgnoreCase("jpg")) || (ext.equalsIgnoreCase("jpge"))) {

                                            archivoDescargado = new File(filePath + fileName);
                                            Files.copy(archivoDescargado, media.getStreamData());
                                            new EscaneaDocumento("reemplazar", infoDocumento).codificar(archivoDescargado, false);

                                        } else if ((ext.equalsIgnoreCase("tif") || (ext.equalsIgnoreCase("tiff")))) {

                                            archivoDescargado = new File(filePath + fileName);
                                            Files.copy(archivoDescargado, media.getStreamData());
                                            new EscaneaDocumento("reemplazar", infoDocumento).codificar(archivoDescargado, false);

                                        } else {
                                            herramientas.warn("El documento debe ser un pdf, jpg o tif");
                                        }
                                    }

                                    session = herramientas.crearSesion();
                                    expediente = (Expediente) session.getAttribute("expediente");
                                    if (expediente.isResultado()) {
                                        this.buscarDocumentosDigitalizado();
                                    }
                                    break;
                                }
                            }
                        }

                    } else {
                        herramientas.info("Por favor seleccione el documento del arbol digitalizados,\n y luego use los botones segun su preferencia");
                    }

                } else {
                    herramientas.warn("Debe elegir un tipo de documento para ser digitalizado");
                }

            } else {
                herramientas.navegarPagina("index.zul");
            }

        } catch (NullPointerException e) {
            traza.trace("no selecciono documento", Level.ERROR, e);
            herramientas.error("Debe seleccionar un Tipo de Documento", e);
        } catch (DatatypeConfigurationException | IOException ex) {
            traza.trace("error al digitalizar el documento", Level.ERROR, ex);
            herramientas.error("Problemas al subir el documento", ex);
        } catch (DW4JException e) {
            traza.trace("", Level.ERROR, e);
            herramientas.error("", e);
        }
    }

    /**
     * Genera la foliatura del Expediente
     */
    @Listen("onClick = #btnFoliatura")
    public void crearFoliatura() {

        boolean imprimir = false;
        HashMap folio = new HashMap();
        ProcesaReporte pr = new ProcesaReporte();

        try {
            if (!txtExpediente.getValue().equals("")) {

                int idLibreria = expediente.getIdLibreria();
                int idCategoria = expediente.getIdCategoria();
                String idExpediente = txtExpediente.getText();

                traza.trace("imprimiendo la foliatura", Level.INFO);
                traza.trace("idLibreria: " + idLibreria, Level.INFO);
                traza.trace("idCategoria: " + idCategoria, Level.INFO);
                traza.trace("idExpediente: " + idExpediente, Level.INFO);

                imprimir = new ve.com.develcom.foliatura.Foliatura().armarFoliatura(idExpediente, idLibreria, idCategoria);

                traza.trace("respuesta al armar la foliatura " + imprimir, Level.INFO);

                if (imprimir) {
                    expediente.setIdExpediente(idExpediente);
                    session.setAttribute("expediente", expediente);
                    folio.put("idLib", idLibreria);
                    folio.put("idCat", idCategoria);
                    folio.put("idExpediente", idExpediente);
                    pr.crearReporte("foliatura.jrxml", folio, "Indice de Foliatura");
                }

            } else {
                herramientas.warn("Debe introducir un identificador de expediente");
            }
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas creando la foliatura", Level.ERROR, ex);
            herramientas.error("Problemas creando la Foliatura", ex);
        }
    }

    @Listen("onSelect = #arbolDocDigit")
    public void mostraInfoDocVencido() {

        GregorianCalendar fechaActual;
        String tipoDocumento, fileName, filePath, ext, rutaTemp, realPath, tiDo;
        NodoArbol<DatoNodoArbol> value;
        DatoNodoArbol dna;

        try {
            if (session != null) {

                value = arbolDocDigit.getSelectedItem().getValue();

                if (value.isLeaf()) {

                    dna = value.getData();
                    tipoDocumento = dna.getTipoDocumento();

                    for (InfoDocumento id : documentosDigitalizado) {
                        traza.trace("buscando tipo de documento para la fecha de vencimiento " + id.getTipoDocumento().trim(), Level.INFO);

                        String da = "";
                        if (id.isTipoDocDatoAdicional()) {
                            List<com.develcom.documento.DatoAdicional> datosAdicionales = id.getLsDatosAdicionales();
                            int size = datosAdicionales.size(), i = 1;
                            for (com.develcom.documento.DatoAdicional datAd : datosAdicionales) {

                                if (datAd.getTipo().equalsIgnoreCase("fecha")) {

                                    if (i == size) {
                                        da = da + " " + datAd.getValor().toString();
                                    } else {
                                        da = da + " " + datAd.getValor().toString() + ",";
                                    }
                                } else {
                                    if (i == size) {
                                        da = da + " " + datAd.getValor();
                                    } else {
                                        da = da + " " + datAd.getValor() + ",";
                                    }
                                }
                                i++;
                            }
                        }
                        da = "(" + da.trim() + ")";

                        tiDo = tipoDocumento + " " + da;

                        if (tiDo.contains(id.getTipoDocumento().trim())) {
                            XMLGregorianCalendar xmlCalendarActual = id.getFechaActual();
                            XMLGregorianCalendar xmlCalendarVencimiento = id.getFechaVencimiento();
                            GregorianCalendar fechVencimiento = xmlCalendarVencimiento.toGregorianCalendar();

                            fechaActual = xmlCalendarActual.toGregorianCalendar();

                            traza.trace("fecha de vencimiento del documento " + tipoDocumento + " " + new SimpleDateFormat("dd/MM/yyyy").format(fechVencimiento.getTime()), Level.INFO);
                            traza.trace("fecha del sistema " + new SimpleDateFormat("dd/MM/yyyy").format(fechaActual.getTime()), Level.INFO);

                            if (fechaActual.after(fechVencimiento)) {
                                herramientas.warn("Documento Vencido");
//                                arbolDocDigit.clearSelection();
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            traza.trace("problemas comprobando la fecha de vencimiento", Level.ERROR, e);
            herramientas.error("problemas comprobando la fecha de vencimiento", e);
        }

    }
}
