/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.calidad;

import com.develcom.administracion.SubCategoria;
import com.develcom.calidad.ConsultaDinamica;
import com.develcom.dao.Expediente;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import ve.com.develcom.administracion.AdministracionBusqueda;
import ve.com.develcom.aprueba.CalidadDocumento;
import ve.com.develcom.tools.Traza;

/**
 *
 * @author develcom
 */
public class BuscaExpediente extends SelectorComposer<Component> {

    @Wire
    private Window winBuscaExpediente;

    @Wire
    private Radio rdBuscarSimple;

    @Wire
    private Radio rdRangoFecha;

    @Wire
    private Radio rdListadoPendiente;

    @Wire
    private Textbox txtExpediente;

    @Wire
    private Datebox dbFechaDesde;

    @Wire
    private Datebox dbFechaHasta;

    private static final long serialVersionUID = -5542573325659019118L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(BuscaExpediente.class);
    private Session session;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            expediente = (Expediente) session.getAttribute("expediente");

            if (Constantes.ACCION.equalsIgnoreCase("ELIMINAR")) {
                winBuscaExpediente.setTitle("Eliminar");
            } else {
                winBuscaExpediente.setTitle("Calidad");
            }

        }

    }

    @Listen("onClick = #btnAceptar")
    public void aceptar() {

        String usuario;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        GregorianCalendar fechaDesde = new GregorianCalendar();
        GregorianCalendar fechaHasta = new GregorianCalendar();
        XMLGregorianCalendar fechaXMLDesde = null;
        XMLGregorianCalendar fechaXMLHasta = null;
        String idExpediente;
        List<ConsultaDinamica> consultaDinamicas = new ArrayList<>();
//        List<TipoDocumento> listaTipoDoc = new ArrayList<>();
        boolean flag = true;
        Date desde, hasta;

        try {

            consultaDinamicas.clear();
            usuario = (String) session.getAttribute("login");

            if (rdBuscarSimple.isSelected()) {

                idExpediente = txtExpediente.getValue();
                if (!idExpediente.equalsIgnoreCase("")) {
                    consultaDinamicas = new CalidadDocumento().buscarDocumentosPendientes(usuario, null, null, 0, expediente.getIdCategoria(), idExpediente);
                } else {
                    flag = false;
                    herramientas.warn("Debe colocar el id del Expediente");
                }
            } else if (rdRangoFecha.isSelected()) {

                desde = dbFechaDesde.getValue();
                hasta = dbFechaHasta.getValue();

                fechaDesde.setGregorianChange(desde);
                fechaHasta.setGregorianChange(hasta);

                if ((fechaDesde != null) && (fechaHasta != null)) {
                    fechaXMLDesde = DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaDesde);
                    fechaXMLHasta = DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaHasta);

                    consultaDinamicas = new CalidadDocumento().buscarDocumentosPendientes(usuario, fechaXMLDesde, fechaXMLHasta, 0, expediente.getIdCategoria(), null);

                } else if (fechaDesde != null) {

                    fechaXMLDesde = DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaDesde);

                    consultaDinamicas = new CalidadDocumento().buscarDocumentosPendientes(usuario, fechaXMLDesde, fechaXMLHasta, 0, expediente.getIdCategoria(), null);

                } else if (fechaHasta != null) {

                    fechaXMLHasta = DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaHasta);

                    consultaDinamicas = new CalidadDocumento().buscarDocumentosPendientes(usuario, fechaXMLDesde, fechaXMLHasta, 0, expediente.getIdCategoria(), null);
                } else {
                    flag = false;
                    herramientas.warn("Debe seleccionar alguna fecha");
                }

            } else if (rdListadoPendiente.isSelected()) {
                consultaDinamicas = new CalidadDocumento().buscarDocumentosPendientes(usuario, null, null, 0, expediente.getIdCategoria(), null);
            } else {
                herramientas.warn("Debe seleccionar alguna fecha");
            }

            if (flag) {
                if (!consultaDinamicas.isEmpty()) {
                    if (consultaDinamicas.get(0).isExiste()) {

                        List<SubCategoria> listaSubCategorias = new AdministracionBusqueda().buscarSubCategoria(null, expediente.getIdCategoria(), 0);
                        expediente.setSubCategorias(listaSubCategorias);
                        session.setAttribute("consultaDinamicas", consultaDinamicas);
                        session.setAttribute("expediente", expediente);

                        herramientas.navegarPagina("resultado.zul");

                    } else {
                        herramientas.warn("No existen Expedientes asociados a la busqueda");
                    }
                } else {
                    herramientas.warn("No existen expedientes de acuerdo \ncon los parámetros indicados");
                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas con la busqueda de expedientes", Level.ERROR, ex);
            herramientas.error("Problemas con la busqueda de expedientes", ex);
        } catch (DatatypeConfigurationException ex) {
            traza.trace("problemas al convertir fechas", Level.ERROR, ex);
            herramientas.error("Problemas al convertir fechas", ex);
        }

    }

    @Listen("onFocus= #txtExpediente")
    public void focusBuscarSimple() {
        rdBuscarSimple.setSelected(true);
    }

    @Listen("onFocus= #dbFechaDesde")
    public void focusBuscarFechaDesde() {
        rdBuscarSimple.setSelected(true);
    }

    @Listen("onFocus= #dbFechaHasta")
    public void focusBuscarFechaHasta() {
        rdBuscarSimple.setSelected(true);
    }
}
