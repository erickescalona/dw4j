--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-10-02 14:53:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 191 (class 1259 OID 33343)
-- Name: expedientes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE expedientes (
    id_expedientes integer DEFAULT nextval('sq_expediente'::regclass) NOT NULL,
    expediente character varying(250) NOT NULL,
    id_indice integer NOT NULL,
    valor character varying(250),
    fecha_indice date,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL
);


ALTER TABLE public.expedientes OWNER TO postgres;

--
-- TOC entry 2965 (class 0 OID 33343)
-- Dependencies: 191
-- Data for Name: expedientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, id_libreria, id_categoria) VALUES (1, '01', 21, '01', 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, id_libreria, id_categoria) VALUES (2, '01', 22, '123456', 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, id_libreria, id_categoria) VALUES (4, '01', 24, 'Valor1', 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, fecha_indice, id_libreria, id_categoria) VALUES (3, '01', 23, '02/10/2014', 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, id_libreria, id_categoria) VALUES (5, '01', 25, 'direccion casa', 1, 1);


--
-- TOC entry 2855 (class 2606 OID 33351)
-- Name: pk_expedientes; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT pk_expedientes PRIMARY KEY (id_expedientes);


--
-- TOC entry 2856 (class 2606 OID 33352)
-- Name: fk_expedientes_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2857 (class 2606 OID 33357)
-- Name: fk_expedientes_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


-- Completed on 2014-10-02 14:53:20

--
-- PostgreSQL database dump complete
--

