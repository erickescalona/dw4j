INSERT INTO causa (id_causa, causa) VALUES (1, 'Mala Calidad en la Imagen');
INSERT INTO causa (id_causa, causa) VALUES (2, 'Mala tipificación del Documento');
INSERT INTO causa (id_causa, causa) VALUES (3, 'Los Índices no concuerdan con el Documento');
INSERT INTO causa (id_causa, causa) VALUES (4, 'Mala Orientación del Documento');
INSERT INTO causa (id_causa, causa) VALUES (5, 'Visualización Nula del Documento');
INSERT INTO causa (id_causa, causa) VALUES (6, 'Falla Técnica del Sistema');
INSERT INTO causa (id_causa, causa) VALUES (7, 'Eliminacion de Documento');

INSERT INTO configuracion (id_configuracion, calidad, ruta_temporal, archivo_tif, archivo_cod, log, foliatura, server_name, database_name, port, userbd, password, ficha, fabrica, elimina) VALUES (1, '1', 'temp', 'documento.tiff', 'codificado.cod', '/lib/log4j-config.properties', '1', '192.168.0.142', 'dw4j', 5432, 'cG9zdGdyZXM=', 'ZGV2ZWxjb20=', '1', '1', '1');

INSERT INTO estatus (id_estatus, estatus) VALUES (1, 'Activo');
INSERT INTO estatus (id_estatus, estatus) VALUES (2, 'Inactivo');;

INSERT INTO estatus_documento (id_estatus_documento, estatus_documento) VALUES (0, 'Pendiente');
INSERT INTO estatus_documento (id_estatus_documento, estatus_documento) VALUES (1, 'Aprobado');
INSERT INTO estatus_documento (id_estatus_documento, estatus_documento) VALUES (2, 'Rechazado');

INSERT INTO rol (id_rol, rol) VALUES (1, 'ADMINISTRADOR');
INSERT INTO rol (id_rol, rol) VALUES (2, 'APROBADOR');
INSERT INTO rol (id_rol, rol) VALUES (3, 'DIGITALIZADOR');
INSERT INTO rol (id_rol, rol) VALUES (4, 'CONSULTAR');
INSERT INTO rol (id_rol, rol) VALUES (5, 'IMPRIMIR');
INSERT INTO rol (id_rol, rol) VALUES (6, 'REPORTES');
INSERT INTO rol (id_rol, rol) VALUES (7, 'CONFIGURADOR');
INSERT INTO rol (id_rol, rol) VALUES (8, 'MANTENIMIENTO');
INSERT INTO rol (id_rol, rol) VALUES (9, 'ELIMINAR');

INSERT INTO usuario (id_usuario, nombre, apellido, cedula, sexo, id_estatus, password) VALUES ('dw4jconf', 'Usuario', 'Configurador', NULL, NULL, 1, 'RGV2ZWxjb21HRA==');

INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (1, NULL, NULL, 'dw4jconf', 7);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (2, NULL, NULL, 'dw4jconf', 8);
