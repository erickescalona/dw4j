--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-10-22 15:21:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE dw4j;
--
-- TOC entry 3101 (class 1262 OID 34909)
-- Name: dw4j; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE dw4j WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 've_ZA.utf8' LC_CTYPE = 've_ZA.utf8';


ALTER DATABASE dw4j OWNER TO postgres;

\connect dw4j

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3102 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 206 (class 3079 OID 12617)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3104 (class 0 OID 0)
-- Dependencies: 206
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 220 (class 1255 OID 34910)
-- Name: f_buscar_categoria(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_categoria(p_idcategoria integer, p_idlibreria integer, p_categoria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cat refcursor;
  begin


    if p_idCategoria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_categoria=p_idcategoria
          order by c.categoria;

    elsif p_idLibreria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_libreria=p_idlibreria
          order by c.categoria;

    elsif p_categoria is not null then
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.categoria=p_categoria
          order by c.categoria;
    else
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
           order by c.categoria;
    end if;
    return cursor_cat;
    close cursor_cat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_categoria(p_idcategoria integer, p_idlibreria integer, p_categoria character varying) OWNER TO postgres;

--
-- TOC entry 221 (class 1255 OID 34911)
-- Name: f_buscar_causas_rechazo(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_causas_rechazo() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_causa refcursor;
  begin

    open cursor_causa for
      select id_causa, causa from causa;

    return cursor_causa;
    close cursor_causa;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_causas_rechazo() OWNER TO postgres;

--
-- TOC entry 222 (class 1255 OID 34912)
-- Name: f_buscar_configuracion(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_configuracion() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_conf refcursor;
  begin

    open cursor_conf for
      select * from configuracion;

    return cursor_conf;
    close cursor_conf;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_configuracion() OWNER TO postgres;

--
-- TOC entry 223 (class 1255 OID 34913)
-- Name: f_buscar_datos_combo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_datos_combo(p_idcodigoindice integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice
             from lista_desplegables l inner join indices a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idcodigoindice
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_datos_combo(p_idcodigoindice integer) OWNER TO postgres;

--
-- TOC entry 224 (class 1255 OID 34914)
-- Name: f_buscar_datosdoc(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_datosdoc(p_idinfodocumento integer, p_versiondoc integer, p_idexpediente character varying, p_numerodoc integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

    cursor_doc refcursor;
  begin

    open cursor_doc for
      select i.*, d.*, c.id_categoria, s.id_subcategoria,
             t.tipo_documento tipoDoc, e.estatus_documento estatusArchivo
          from infodocumento i
          inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
          inner join tipodocumento t on i.id_documento=t.id_documento
          inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
          inner join categoria c on s.id_categoria=c.id_categoria
          inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
      where i.id_infodocumento=p_idinfodocumento and i.id_expediente=p_idexpediente
            and i.version=p_versiondoc and i.numero_documento=p_numerodoc
            and i.estatus_documento=2;

     return cursor_doc;
     close cursor_doc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_datosdoc(p_idinfodocumento integer, p_versiondoc integer, p_idexpediente character varying, p_numerodoc integer) OWNER TO postgres;

--
-- TOC entry 225 (class 1255 OID 34915)
-- Name: f_buscar_estatus(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_estatus(p_idestatus integer, p_estatus character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_est refcursor;
  begin

    if p_idestatus > 0 then
      open cursor_est for
         select id_estatus, estatus from estatus where id_estatus=p_idestatus;
    elsif p_estatus is not null then
      open cursor_est for
         select id_estatus, estatus from estatus where estatus=p_estatus;
    end if;

    return cursor_est;
    close cursor_est;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_estatus(p_idestatus integer, p_estatus character varying) OWNER TO postgres;

--
-- TOC entry 295 (class 1255 OID 34916)
-- Name: f_buscar_expediente(character varying, integer, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_expediente(p_expediente character varying, p_idlibreria integer, p_idcategoria integer, p_flag character) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_expe refcursor;
  begin

   if (p_flag = '1') then

      open cursor_expe for
         select distinct e.expediente, e.id_libreria, e.id_categoria, e.valor, e.fecha_indice, a.id_indice,
                c.categoria, l.libreria, a.indice, a.tipo, a.codigo, a.clave
             from expedientes e inner join indices a
                     on (e.id_categoria=a.id_categoria and e.id_indice=a.id_indice)
                  inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.id_libreria
              where e.expediente=p_expediente and a.id_categoria=p_idcategoria
                and e.id_libreria=p_idlibreria
              order by a.id_indice;
    else
      open cursor_expe for
         select e.*, c.categoria, l.libreria
             from expedientes e inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.id_libreria
             where e.expediente=p_expediente;
    end if;
    return cursor_expe;
    close cursor_expe;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_expediente(p_expediente character varying, p_idlibreria integer, p_idcategoria integer, p_flag character) OWNER TO postgres;

--
-- TOC entry 294 (class 1255 OID 34917)
-- Name: f_buscar_expediente_reporte(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_expediente_reporte(p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_expe refcursor;
  begin

   open cursor_expe for
   SELECT distinct e.expediente, e.valor, e.fecha_indice,  
        i.id_indice, i.indice, i.tipo, i.codigo, i.clave, e.id_libreria, e.id_categoria
  FROM expedientes e inner join indices i on e.id_indice=i.id_indice
  where e.id_categoria=p_idcategoria
  order by e.expediente, i.id_indice;

    return cursor_expe;
    close cursor_expe;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_expediente_reporte(p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 231 (class 1255 OID 34918)
-- Name: f_buscar_fabrica(character varying, date, date, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_fabrica(p_usuario character varying, p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
    sfabrica char(1);
  begin

    select t.fabrica into sfabrica from fabrica t where t.usuario=p_usuario;

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion between CAST (p_fechadesde AS DATE)
           and CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion >= CAST (p_fechadesde AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion <= CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_expediente is not null then

         open cursor_user for
            select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
               inner join fabrica f on f.usuario=di.usuario_digitalizo
               inner join usuario u on u.id_usuario=f.usuario
             where d.id_expediente = p_expediente
               and d.estatus_documento=p_estatusdocumento
               and f.fabrica=sfabrica
               and i.id_categoria=p_idcategoria
               order by e.expediente, i.id_indice;
    else

      open cursor_user for
        select distinct e.expediente, e.valor, e.fecha_indice, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_fabrica(p_usuario character varying, p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expediente character varying) OWNER TO postgres;

--
-- TOC entry 226 (class 1255 OID 34919)
-- Name: f_buscar_fisico_documento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_fisico_documento(p_iddocumento integer, p_numerodoc integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_doc refcursor;
  begin

    open cursor_doc for
        select i.*, c.id_categoria, s.id_subcategoria, t.tipo_documento tipoDoc,
               e.estatus_documento estatusArchivo
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                                inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
                                inner join tipodocumento t on i.id_documento=t.id_documento
                                inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
                                inner join categoria c on s.id_categoria=c.id_categoria
           where i.id_documento=p_iddocumento and i.id_expediente=p_idexpediente
                 and i.numero_documento=p_numerodoc and i.estatus_documento<>2
           order by i.version desc;

  return cursor_doc;
  close cursor_doc;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_fisico_documento(p_iddocumento integer, p_numerodoc integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 227 (class 1255 OID 34920)
-- Name: f_buscar_foto_ficha(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_foto_ficha(p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    cursor_ficha refcursor;
  begin

    open cursor_ficha for
        select t.id_documento, i.id_infodocumento, i.nombre_archivo as nombreArchivo,
               t.tipo_documento as tipoDocumento, i.estatus_documento, i.ruta_archivo
          from tipodocumento t inner join infodocumento i on t.id_documento=i.id_documento
          where i.id_expediente=p_idexpediente and t.ficha='1';

    return cursor_ficha;
    close cursor_ficha;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_foto_ficha(p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 228 (class 1255 OID 34921)
-- Name: f_buscar_indice_datosadicional(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_indice_datosadicional(p_idtipodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select *
         from dato_adicional t
         where t.id_documento = p_idtipodocumento
         order by t.id_dato_adicional;

    return cursor_dato;
    close cursor_dato;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_indice_datosadicional(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 229 (class 1255 OID 34922)
-- Name: f_buscar_indices(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_indices(p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_arg refcursor;
  begin

    open cursor_arg for
       select * from indices i where id_categoria=p_idcategoria order by i.id_indice;
    return cursor_arg;
    close cursor_arg;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_indices(p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 230 (class 1255 OID 34923)
-- Name: f_buscar_infodocumento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_infodocumento(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
     cursor_info refcursor;
  begin

  if p_idinfodocumento > 0 then

    open cursor_info for
    select * from infodocumento where id_infodocumento=p_idinfodocumento;

  elsif p_iddocumento > 0 then

    open cursor_info for
    select * from infodocumento i inner join tipodocumento t on i.id_documento=t.id_documento
    where i.id_documento=p_iddocumento and i.id_expediente=p_idexpediente
    order by numero_documento, version;

  end if;
  return cursor_info;
  close cursor_info;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_infodocumento(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 232 (class 1255 OID 34924)
-- Name: f_buscar_infodocumento(character varying, character varying, integer, character, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_infodocumento(p_idexpediente character varying, p_ids_documento character varying, p_estatusdoc integer, p_redigitalizo character, p_estatusaprobado integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_infodoc refcursor;
    query character varying;
  begin

   if p_estatusaprobado = 1 then
   
      --open cursor_infodoc for
        query:='select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where i.id_expediente='''||p_idexpediente||'''
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in ('||p_ids_documento||')
                 and (i.estatus_documento='||p_estatusdoc||' or i.estatus_documento='||p_estatusaprobado||')
                 and i.re_digitalizado='''||p_redigitalizo||'''
           order by t.tipo_documento, i.numero_documento, i.version desc';
   else

     --open cursor_infodoc for
        query:='select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where e.expediente='''||p_idexpediente||'''
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in ('||p_ids_documento||')
                 and i.estatus_documento='||p_estatusdoc||' and i.re_digitalizado='''||p_redigitalizo||'''
           order by t.tipo_documento, i.numero_documento, i.version desc';

   end if;

    OPEN cursor_infodoc FOR execute query;
   
    return cursor_infodoc;
    close cursor_infodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_infodocumento(p_idexpediente character varying, p_ids_documento character varying, p_estatusdoc integer, p_redigitalizo character, p_estatusaprobado integer) OWNER TO postgres;

--
-- TOC entry 233 (class 1255 OID 34925)
-- Name: f_buscar_informaciondoc(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_informaciondoc(p_idsdocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

  cursor_infdoc refcursor;

  begin

    open cursor_infdoc for
         select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expedientes e on i.id_expediente=e.expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;
            /*select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expediente e on i.id_expediente=e.id_expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;*/

       return cursor_infdoc;
       close cursor_infdoc;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_informaciondoc(p_idsdocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 293 (class 1255 OID 34926)
-- Name: f_buscar_lib_cat_perfil(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_lib_cat_perfil(p_usuario character varying, p_perfil character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
        select p.id_usuario usuario, r.rol,
               p.id_libreria, l.libreria, el.estatus status_lib,
               p.id_categoria, c.categoria, ec.estatus status_cat
           from perfil p
                inner join rol r on p.id_rol=r.id_rol
                inner join libreria l on p.id_libreria=l.id_libreria
                inner join estatus el on l.id_estatus=el.id_estatus
                inner join categoria c on p.id_categoria=c.id_categoria
                inner join estatus ec on c.id_estatus=ec.id_estatus
           where p.id_usuario=p_usuario and r.rol=p_perfil
           order by l.libreria, c.categoria;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_lib_cat_perfil(p_usuario character varying, p_perfil character varying) OWNER TO postgres;

--
-- TOC entry 234 (class 1255 OID 34927)
-- Name: f_buscar_libreria(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_libreria(p_idlibreria integer, p_libreria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_lib refcursor;
  begin

    if p_idlibreria > 0 then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.id_libreria=p_idlibreria
             order by l.libreria asc;

    elsif p_libreria is not null then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.libreria=p_libreria
             order by l.libreria asc;

    else

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             order by l.libreria asc;

    end if;
    return cursor_lib;
    close cursor_lib;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_libreria(p_idlibreria integer, p_libreria character varying) OWNER TO postgres;

--
-- TOC entry 235 (class 1255 OID 34928)
-- Name: f_buscar_libreriascategorias(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_libreriascategorias() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_libcat refcursor;
  begin

    open cursor_libcat for
       select l.id_libreria, l.libreria descLibreria, c.id_categoria, c.categoria descCategoria,
              r.id_rol, r.rol descRol, e.id_estatus, e.estatus desEstatus
          from libreria l inner join categoria c on l.id_libreria=c.id_libreria
                          inner join estatus e
                                 on (l.id_estatus=e.id_estatus and c.id_estatus=e.id_estatus),
                          rol r
          where e.id_estatus=1
          order by l.libreria, c.categoria;

    return cursor_libcat;
    close cursor_libcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_libreriascategorias() OWNER TO postgres;

--
-- TOC entry 236 (class 1255 OID 34929)
-- Name: f_buscar_no_fabrica(date, date, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_no_fabrica(p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expedeinte character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;

  begin

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select i.*, e.valor, e.fecha_indice, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion between CAST (p_fechadesde AS DATE)
           and CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select i.*, e.valor, e.fecha_indice, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion >= CAST (p_fechadesde AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select i.*, e.valor, e.fecha_indice, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion <= CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_expedeinte is not null then

         open cursor_user for
            select i.*, e.valor, e.fecha_indice, e.expediente
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
             where d.id_expediente = p_expedeinte
               and d.estatus_documento=p_estatusdocumento
               and i.id_categoria=p_idcategoria
               order by i.id_indice;
    else

      open cursor_user for
        select i.*, e.valor, e.fecha_indice, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_no_fabrica(p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expedeinte character varying) OWNER TO postgres;

--
-- TOC entry 237 (class 1255 OID 34930)
-- Name: f_buscar_perfil(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_perfil(p_idusuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_perfil refcursor;
  begin

    open cursor_perfil for
        select distinct u.nombre, u.apellido, u.id_usuario, e.estatus estatus_usuario,
               f.fabrica pertenece, r.rol,
               l.id_libreria, l.libreria,
               c.id_categoria, c.categoria
            from usuario u full join perfil p on u.id_usuario=p.id_usuario
                           full join estatus e on u.id_estatus=e.id_estatus
                           full join fabrica f on u.id_usuario=f.usuario
                           full join rol r on p.id_rol=r.id_rol
                           full join libreria l on p.id_libreria=l.id_libreria
                           full join categoria c on p.id_categoria=c.id_categoria
             where u.id_usuario=p_idusuario
             order by l.libreria, c.categoria;


    return cursor_perfil;
    close cursor_perfil;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_perfil(p_idusuario character varying) OWNER TO postgres;

--
-- TOC entry 239 (class 1255 OID 34931)
-- Name: f_buscar_subcategoria(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_subcategoria(p_idcategoria integer, p_idsubcategoria integer, p_subcategoria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_subcat refcursor;
  begin

    if p_idcategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_categoria=p_idcategoria
          order by s.subcategoria;

    elsif p_idsubcategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_subcategoria=p_idsubcategoria
          order by s.subcategoria;

    elsif p_subcategoria is not null then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.subcategoria=p_subcategoria
          order by s.subcategoria;

    else

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
             order by s.subcategoria;
    end if;
    return cursor_subcat;
    close cursor_subcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_subcategoria(p_idcategoria integer, p_idsubcategoria integer, p_subcategoria character varying) OWNER TO postgres;

--
-- TOC entry 240 (class 1255 OID 34932)
-- Name: f_buscar_tipo_documento(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_tipo_documento(p_idssucategorias integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    curor_doc refcursor;
  begin
    

    open curor_doc for
     select t.*, e.*
       from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
       where t.id_subcategoria in (p_idssucategorias) and e.id_estatus=1;
       --where t.id_subcategoria = p_idssucategorias and e.id_estatus=1;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_tipo_documento(p_idssucategorias integer) OWNER TO postgres;

--
-- TOC entry 241 (class 1255 OID 34933)
-- Name: f_buscar_tipo_documento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_tipo_documento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumemto character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_tipodoc refcursor;
  begin

    if p_idCategoria > 0 and p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria and t.id_subcategoria=p_idsubcategoria
         order by t.tipo_documento;

    elsif p_idCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria order by t.tipo_documento;

    elsif p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_subcategoria=p_idsubcategoria order by t.tipo_documento;

    elsif p_tipodocumemto is not null then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.tipo_documento=p_tipodocumemto order by t.tipo_documento;

    else

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
            order by t.tipo_documento;

    end if;
    return cursor_tipodoc;
    close cursor_tipodoc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_tipo_documento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumemto character varying) OWNER TO postgres;

--
-- TOC entry 242 (class 1255 OID 34934)
-- Name: f_buscar_ultima_version(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_ultima_version(p_iddocumento integer, p_idexpediente character varying, p_numerodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_ver refcursor;
  begin

    open cursor_ver for
      select max(i.version) as version
        from infodocumento i
        where i.id_documento=p_iddocumento
              and i.id_expediente=p_idexpediente
              and i.numero_documento=p_numerodocumento
              and i.estatus_documento<>2;

  return cursor_ver;
  close cursor_ver;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_ultima_version(p_iddocumento integer, p_idexpediente character varying, p_numerodocumento integer) OWNER TO postgres;

--
-- TOC entry 243 (class 1255 OID 34935)
-- Name: f_buscar_ultimo_numero(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_ultimo_numero(p_iddocumento integer, p_idexpediente character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
    numero_doc integer;
  begin

   numero_doc := -1;
   
   select max(numero_documento) as numeroDocumento into numero_doc from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente;

   return numero_doc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_ultimo_numero(p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 244 (class 1255 OID 34936)
-- Name: f_buscar_usuario_fabrica(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_usuario_fabrica(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_fab refcursor;
  begin

    open cursor_fab for
       select usuario, fabrica from fabrica f where f.usuario=p_usuario;


    return cursor_fab;
    close cursor_fab;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_usuario_fabrica(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 245 (class 1255 OID 34937)
-- Name: f_buscar_valor_dato_adicional(integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_valor_dato_adicional(p_idocumento integer, p_idexpediente character varying, p_numerodoc integer, p_version integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select *
           from dato_adicional d
             inner join valor_dato_adicional v on d.id_dato_adicional=v.id_dato_adicional
           where d.id_documento=p_idocumento
                and v.expediente=p_idexpediente
                and v.numero=p_numerodoc
                and v.version=p_version;

  return cursor_dato;
  close cursor_dato;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_valor_dato_adicional(p_idocumento integer, p_idexpediente character varying, p_numerodoc integer, p_version integer) OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 34938)
-- Name: f_buscar_valor_datoadicional(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_valor_datoadicional(p_idindicedato integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice_adicional
             from lista_desplegables l inner join dato_adicional a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idindicedato
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_valor_datoadicional(p_idindicedato integer) OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 34939)
-- Name: f_comprobar_foto_ficha(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_foto_ficha(p_idtipodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_foto refcursor;
  begin

    open cursor_foto for
       select ficha from tipodocumento where id_documento=p_idtipodocumento;

    return cursor_foto;
    close cursor_foto;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_foto_ficha(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 34940)
-- Name: f_comprobar_nombre_archivo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_nombre_archivo(p_idinfodocumento integer, p_idexpediente character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
  DECLARE
    nombre varchar(255);
  begin

   nombre := null;
   
   select nombre_archivo into nombre from infodocumento
   where id_infodocumento= p_idinfodocumento and id_expediente=p_idexpediente;

   return nombre;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_nombre_archivo(p_idinfodocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 34941)
-- Name: f_comprobar_numero_documento(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_numero_documento(p_iddocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_numdoc refcursor;
  begin

   open cursor_numdoc for
   select numero_documento  from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente
   order by numero_documento;

   return cursor_numdoc;
   close cursor_numdoc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_numero_documento(p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 34942)
-- Name: f_crear_sesion(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_crear_sesion(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
       select distinct r.rol desc_rol, e.estatus estatus_usuario, u.id_usuario id_user,
              u.nombre, u.apellido, u.cedula, u.id_estatus
           from usuario u inner join estatus e on u.id_estatus=e.id_estatus
                inner join perfil p on p.id_usuario=u.id_usuario
                inner join rol r on r.id_rol=p.id_rol
           where u.id_usuario=p_usuario;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_crear_sesion(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 34943)
-- Name: f_eliminar_archivo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_eliminar_archivo(p_idinfodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

    cursor_eliminar refcursor;
  begin

   open cursor_eliminar for
        select t.ruta_archivo, t.nombre_archivo from infodocumento t where t.id_infodocumento=p_idinfodocumento;

    return cursor_eliminar;
    close cursor_eliminar;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_eliminar_archivo(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 34944)
-- Name: f_foliatura_buscar_expediente(character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_foliatura_buscar_expediente(p_idexpediente character varying, p_idlibreria integer, p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_folio refcursor;
  begin
  
    open cursor_folio for
        select i.id_expediente, l.id_libreria, c.id_categoria, s.id_subcategoria,
               t.id_documento, i.id_infodocumento, t.tipo_documento as documento,
               i.paginas as cantidadPAginas 
     from infodocumento i 
         inner join tipodocumento t on i.id_documento=t.id_documento
         inner join subcategoria s on s.id_subcategoria=t.id_subcategoria
         inner join categoria c on c.id_categoria=t.id_categoria
         inner join libreria l on l.id_libreria=c.id_libreria
        where i.id_expediente=p_idexpediente and i.estatus_documento=1 and
              l.id_libreria=p_idlibreria and c.id_categoria=p_idcategoria
        order by t.id_documento, i.numero_documento desc, i.version desc;

    return cursor_folio;
    close cursor_folio;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_foliatura_buscar_expediente(p_idexpediente character varying, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 34945)
-- Name: f_informacion_tabla(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_informacion_tabla(p_tabla character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_tabla refcursor;
  begin

    open cursor_tabla for
      SELECT s.OWNER, s.TABLE_NAME, s.COLUMN_NAME, s.DATA_TYPE
          FROM all_tab_columns s
      WHERE
      --owner = 'GESTORDOCUMENTAL' and
      table_name=p_tabla;
      return cursor_tabla;
      close cursor_tabla;
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_informacion_tabla(p_tabla character varying) OWNER TO postgres;

--
-- TOC entry 254 (class 1255 OID 34946)
-- Name: f_modificar_indices(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_modificar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_flag character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    curor_indice refcursor;
  begin
    
    if p_flag = '0' then
    
      open curor_indice for
         select expediente from expedientes where expediente=p_idexpedientenuevo;
      return curor_indice;
      close curor_indice;
      
    elsif p_flag = '1' then
      
      delete from expedientes where expediente=p_idexpedienteviejo;

      update infodocumento set id_expediente=p_idexpedientenuevo where id_expediente=p_idexpedienteviejo;
      return curor_indice;
    end if;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_modificar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_flag character varying) OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 34947)
-- Name: f_usuarios(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_usuarios() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin

    open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.sexo,
              e.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

       return cursor_user;
       close cursor_user;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_usuarios() OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 34948)
-- Name: f_verificar_usuario(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_verificar_usuario(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin

   if p_usuario is not null then

    open cursor_user for
      select u.id_usuario, u.nombre, u.apellido, u.password passUser, e.id_estatus, e.estatus, c.*
      from usuario u inner join estatus e on u.id_estatus=e.id_estatus,
      configuracion c
      where u.id_usuario = p_usuario;

   else

     open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

   end if;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.f_verificar_usuario(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 34949)
-- Name: p_actualiza_codigo_combo(integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_codigo_combo(p_codigocombo integer, p_flag character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    if p_flag = '0' then

      update indices set codigo = p_codigocombo where id_indice = p_codigocombo;

    elsif p_flag = '1' then

      update dato_adicional  set codigo = p_codigocombo where id_dato_adicional = p_codigocombo;

    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_codigo_combo(p_codigocombo integer, p_flag character) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 34950)
-- Name: p_actualiza_nombre_archivo(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_nombre_archivo(p_nombrearchivo character varying, p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update infodocumento set nombre_archivo=p_nombrearchivo
        where id_infodocumento=p_idinfodocumento;
		
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_nombre_archivo(p_nombrearchivo character varying, p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 34951)
-- Name: p_actualiza_numero_da(integer, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_numero_da(p_idvalor integer, p_numerodocumento integer, p_version integer, p_expediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin
  
     update valor_dato_adicional
       set numero = p_numerodocumento
       where id_valor = p_idvalor
         and expediente = p_expediente
         and version=p_version;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_numero_da(p_idvalor integer, p_numerodocumento integer, p_version integer, p_expediente character varying) OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 34952)
-- Name: p_actualiza_td_foto(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_td_foto(p_idtipodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update tipodocumento set ficha='1' where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_td_foto(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 34953)
-- Name: p_actualizar_categorias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_categorias(p_idcategoria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update categoria set id_estatus=p_idestatus where id_categoria=p_idcategoria;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_categorias(p_idcategoria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 238 (class 1255 OID 34954)
-- Name: p_actualizar_datos_combo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_datos_combo(p_idcombo integer, p_dato character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update lista_desplegables
      set descripcion = p_dato
    where id_lista = p_idcombo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_datos_combo(p_idcombo integer, p_dato character varying) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 34955)
-- Name: p_actualizar_indices(integer, integer, character varying, character varying, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_indices(p_idindices integer, p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin
    update indices
       set id_categoria = p_idcategoria,
           indice = p_indice,
           tipo = p_tipodato,
           codigo = p_codigo,
           clave = p_clave
     where id_indice = p_idindices;
	 
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_indices(p_idindices integer, p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 34956)
-- Name: p_actualizar_indices(character varying, character varying, integer, character varying, date, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin

    if (p_idexpedienteviejo = p_idexpedientenuevo) then

      update expedientes
         set valor = p_valor,
             fecha_indice = p_fechaindice
       where id_indice = p_idindice
         and expediente = p_idexpedienteviejo
         and id_libreria = p_idlibreria
         and id_categoria = p_idcategoria;

    else
        insert into expedientes
          (expediente, id_indice, valor, id_libreria, id_categoria)
        values
          (p_idexpedientenuevo, p_idindice, p_valor, p_idlibreria, p_idcategoria);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 34957)
-- Name: p_actualizar_infodocumento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_infodocumento(p_numerodoc integer, p_idinfodocumento integer, p_idexpediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update infodocumento set numero_documento=p_numerodoc
    where id_infodocumento=p_idinfodocumento and id_expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_infodocumento(p_numerodoc integer, p_idinfodocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 34958)
-- Name: p_actualizar_librerias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_librerias(p_idlibreria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update libreria set id_estatus=p_idestatus where id_libreria=p_idlibreria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_librerias(p_idlibreria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 34959)
-- Name: p_actualizar_subcategorias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_subcategorias(p_idsubcategoria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update subcategoria set id_estatus=p_idestatus where id_subcategoria=p_idsubcategoria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_subcategorias(p_idsubcategoria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 34960)
-- Name: p_actualizar_tipodocumento(integer, integer, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_tipodocumento(p_idtipodocumento integer, p_idestatus integer, p_vencimiento character, p_datoadicional character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update tipodocumento set id_estatus=p_idestatus, vencimiento=p_vencimiento,
                        dato_adicional=p_datoadicional
   where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_tipodocumento(p_idtipodocumento integer, p_idestatus integer, p_vencimiento character, p_datoadicional character) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 34961)
-- Name: p_agrega_usuario(character varying, character varying, character varying, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agrega_usuario(p_idusuario character varying, p_nombre character varying, p_apellido character varying, p_cedula character varying, p_sexo character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into usuario
      (id_usuario, nombre, apellido, cedula, sexo, id_estatus)
    values
      (p_idusuario, p_nombre, p_apellido, p_cedula, p_sexo, 1);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agrega_usuario(p_idusuario character varying, p_nombre character varying, p_apellido character varying, p_cedula character varying, p_sexo character) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 34962)
-- Name: p_agregar_categoria(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_categoria(p_idlibreria integer, p_categoria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into categoria
      (id_libreria, categoria, id_estatus)
    values
      (p_idlibreria, p_categoria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;  
  end;$$;


ALTER FUNCTION public.p_agregar_categoria(p_idlibreria integer, p_categoria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 34963)
-- Name: p_agregar_configuracion(character varying, character varying, integer, character varying, character varying, character, character, character, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_configuracion(p_nombreservidor character varying, p_nombrebasedato character varying, p_puertobasedato integer, p_usuariobasedato character varying, p_passbasedato character varying, p_calidad character, p_foliatura character, p_ficha character, p_fabrica character, p_elimina character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update configuracion
       set calidad = p_calidad,
           foliatura = p_foliatura,
           server_name = p_nombreservidor,
           database_name = p_nombrebasedato,
           port = p_puertobasedato,
           userbd = p_usuariobasedato,
           password = p_passbasedato,
           ficha = p_ficha,
           fabrica = p_fabrica,
           elimina = p_elimina
     where id_configuracion=1;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_configuracion(p_nombreservidor character varying, p_nombrebasedato character varying, p_puertobasedato integer, p_usuariobasedato character varying, p_passbasedato character varying, p_calidad character, p_foliatura character, p_ficha character, p_fabrica character, p_elimina character) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 34964)
-- Name: p_agregar_datos_combo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_datos_combo(p_codigoindice integer, p_dato character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_codigoindice, p_dato);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_datos_combo(p_codigoindice integer, p_dato character varying) OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 34965)
-- Name: p_agregar_datos_combo_da(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_datos_combo_da(p_iddatoadiciona integer, p_datocombo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin


    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_iddatoadiciona, p_datocombo);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_datos_combo_da(p_iddatoadiciona integer, p_datocombo character varying) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 34966)
-- Name: p_agregar_fabrica(character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_fabrica(p_idusuario character varying, p_pertenece character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into fabrica(usuario, fabrica)
           values(p_idusuario, p_pertenece);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;	   
  end;$$;


ALTER FUNCTION public.p_agregar_fabrica(p_idusuario character varying, p_pertenece character) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 34967)
-- Name: p_agregar_foliaturas(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_foliaturas(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying, p_pagina integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into foliatura
      (id_infodocumento, id_documento, id_expediente, pagina)
    values
      (p_idinfodocumento, p_iddocumento, p_idexpediente, p_pagina);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_foliaturas(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying, p_pagina integer) OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 34968)
-- Name: p_agregar_indices(integer, character varying, character varying, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_indices(p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into indices
      (id_categoria, indice, tipo, codigo, clave)
    values
      (p_idcategoria, p_indice, p_tipodato, p_codigo, p_clave);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_indices(p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) OWNER TO postgres;

--
-- TOC entry 213 (class 1255 OID 34969)
-- Name: p_agregar_libreria(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_libreria(p_libreria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into libreria(libreria, id_estatus)
                values(p_libreria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;			
  end;$$;


ALTER FUNCTION public.p_agregar_libreria(p_libreria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 34970)
-- Name: p_agregar_perfil(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_perfil(p_idlibreria integer, p_idcategoria integer, p_idusuario character varying, p_idrol integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    if p_idcategoria = 0 and p_idlibreria = 0 then

      insert into perfil(id_usuario, id_rol)
                  values(p_idusuario, p_idrol);

    elsif p_idcategoria = 0 then

      insert into perfil(id_libreria, id_usuario, id_rol)
                  values(p_idlibreria, p_idusuario, p_idrol);
    else

      insert into perfil(id_libreria, id_categoria, id_usuario, id_rol)
                  values(p_idlibreria, p_idcategoria, p_idusuario, p_idrol);
    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_perfil(p_idlibreria integer, p_idcategoria integer, p_idusuario character varying, p_idrol integer) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 34971)
-- Name: p_agregar_registro_archivo(character varying, character varying, integer, character varying, integer, integer, character varying, integer, date, character varying, character varying, integer, integer, character, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_registro_archivo(p_accion character varying, p_nombrearchivo character varying, p_iddocumento integer, p_rutaarchivo character varying, p_cantpaginas integer, p_version integer, p_idexpediente character varying, p_numerodoc integer, p_fechavencimiento date, p_datoadicional character varying, p_usuario character varying, p_idinfodocumento integer, p_estatus integer, p_redigitalizo character, p_formato character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
   p_idinfodoc integer;
  begin

   
   p_idinfodoc := 0;


   if p_accion = 'versionar' then

     insert into infodocumento(id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, 
                               version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado)
                 values(p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo,
                        p_formato, p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus,
                        p_redigitalizo);
	
	 select lastval() into p_idinfodoc;

     if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion, usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, now(), p_usuario, p_datoadicional);
      else
        rollback;
      end if;

    elsif p_accion = 'reemplazar' then

      p_idinfodoc := -1;
      update infodocumento set nombre_archivo=p_nombrearchivo, ruta_archivo=p_rutaarchivo, paginas=p_cantpaginas, 
                               version=p_version, id_expediente=p_idexpediente, numero_documento=p_numerodoc, 
                               fecha_vencimiento=p_fechavencimiento, formato=p_formato, estatus_documento=p_estatus
      where id_infodocumento=p_idinfodocumento;

      update datos_infodocumento set fecha_digitalizacion = now(),
                                     usuario_digitalizo = p_usuario,
                                     dato_adicional = p_datoadicional
       where id_infodocumento=p_idinfodocumento;

    elsif p_accion = 'Guardar' then
       insert into infodocumento(id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, 
                                version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado)
                  values(p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo, p_formato, 
			 p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus, 0);
	
	 select lastval() into p_idinfodoc;

      if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion, usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, now(), p_usuario, p_datoadicional);

         if p_redigitalizo = '1' then
           update infodocumento set re_digitalizado=p_redigitalizo
              where id_infodocumento=p_idinfodocumento;
         end if;
      else
        rollback;
      end if;

    end if;

   return p_idinfodoc;
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
   end;$$;


ALTER FUNCTION public.p_agregar_registro_archivo(p_accion character varying, p_nombrearchivo character varying, p_iddocumento integer, p_rutaarchivo character varying, p_cantpaginas integer, p_version integer, p_idexpediente character varying, p_numerodoc integer, p_fechavencimiento date, p_datoadicional character varying, p_usuario character varying, p_idinfodocumento integer, p_estatus integer, p_redigitalizo character, p_formato character varying) OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 34972)
-- Name: p_agregar_subcategoria(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_subcategoria(p_idcategoria integer, p_subcategoria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into subcategoria
      (id_categoria, subcategoria, id_estatus)
    values
      (p_idcategoria, p_subcategoria, p_idestatus);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_subcategoria(p_idcategoria integer, p_subcategoria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 34973)
-- Name: p_agregar_tipodocumento(integer, integer, character varying, integer, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_tipodocumento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumento character varying, p_idestatus integer, p_vencimiento character, p_datoadicional character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into tipodocumento
      (id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional)
    values
      (p_idcategoria, p_idsubcategoria, p_tipodocumento, p_idestatus, p_vencimiento, p_datoadicional);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_tipodocumento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumento character varying, p_idestatus integer, p_vencimiento character, p_datoadicional character) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 34974)
-- Name: p_aprobar_documento(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_aprobar_documento(p_idinfodocumento integer, p_usuario character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
    estatu integer;
    idInfoDoc integer;
begin

    update infodocumento set estatus_documento=1 where id_infodocumento=p_idinfodocumento;
    --commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;

    if estatu > 0 then
    
      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;
    
      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_aprobacion = now(),
               usuario_aprobacion = p_usuario
         where id_infodocumento = p_idinfodocumento;
      else
        insert into datos_infodocumento(id_infodocumento, fecha_aprobacion, usuario_aprobacion)
                  values(p_idinfodocumento, now(), p_usuario);
      end if;
      
    else
    
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      --commit;
      
    end if;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %', SQLERRM;
 end;$$;


ALTER FUNCTION public.p_aprobar_documento(p_idinfodocumento integer, p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 34975)
-- Name: p_eliminar_expediente(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_expediente(p_idexpediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    delete from expedientes where expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_expediente(p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 34976)
-- Name: p_eliminar_infodocumento(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_infodocumento(p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  
  begin

    
    delete from datos_infodocumento where id_infodocumento = p_idinfodocumento;
    delete from infodocumento where id_infodocumento = p_idinfodocumento;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_infodocumento(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 283 (class 1255 OID 34977)
-- Name: p_eliminar_perfil(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_perfil(p_idusuario character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   delete from perfil where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_perfil(p_idusuario character varying) OWNER TO postgres;

--
-- TOC entry 284 (class 1255 OID 34978)
-- Name: p_eliminar_registro_archivo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_registro_archivo(p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   delete from datos_infodocumento where id_infodocumento=p_idinfodocumento;
   delete from infodocumento where id_infodocumento=p_idinfodocumento;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_registro_archivo(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 285 (class 1255 OID 34979)
-- Name: p_eliminar_valordatadic(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_valordatadic(p_idvalor integer, p_versiondoc integer, p_numerodoc integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin
  
    delete from valor_dato_adicional
     where id_valor = p_idvalor and version=p_versiondoc and numero=p_numerodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_valordatadic(p_idvalor integer, p_versiondoc integer, p_numerodoc integer) OWNER TO postgres;

--
-- TOC entry 286 (class 1255 OID 34980)
-- Name: p_guarda_valor_dato_adicional(integer, integer, character varying, integer, integer, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guarda_valor_dato_adicional(p_iddatoadicional integer, p_idvalor integer, p_valor character varying, p_numerodocumento integer, p_version integer, p_expediente character varying, p_flag character) RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin


    if p_flag = '0' then

      update valor_dato_adicional
         set valor = p_valor
       where id_valor = p_idvalor
         and numero = p_numerodocumento
         and version = p_version
         and expediente = p_expediente;

    elsif p_flag = '1' then

     insert into valor_dato_adicional
        (id_dato_adicional, valor, numero, version, expediente)
      values
        (p_iddatoadicional, p_valor, p_numerodocumento, p_version, p_expediente);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guarda_valor_dato_adicional(p_iddatoadicional integer, p_idvalor integer, p_valor character varying, p_numerodocumento integer, p_version integer, p_expediente character varying, p_flag character) OWNER TO postgres;

--
-- TOC entry 287 (class 1255 OID 34981)
-- Name: p_guardar_expediente(character varying, integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into expedientes
      (expediente, id_indice, valor, id_libreria, id_categoria)
    values
      (p_idexpediente, p_idindice, p_valor, p_idlibreria, p_idcategoria);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 288 (class 1255 OID 34982)
-- Name: p_guardar_expediente(character varying, integer, character varying, date, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into expedientes
      (expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria)
    values
      (p_idexpediente, p_idindice, p_valor, p_fechaindice, p_idlibreria, p_idcategoria);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_fechaindice date, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 34983)
-- Name: p_guardar_indice_datoadicional(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_indice_datoadicional(p_idtipodocumento integer, p_datoadicional character varying, p_tipo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into dato_adicional
      (indice_adicional, tipo, id_documento)
    values
      (p_datoadicional, p_tipo, p_idtipodocumento);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_indice_datoadicional(p_idtipodocumento integer, p_datoadicional character varying, p_tipo character varying) OWNER TO postgres;

--
-- TOC entry 290 (class 1255 OID 34984)
-- Name: p_modificar_usuario(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_modificar_usuario(p_idusuario character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update usuario set id_estatus=p_idestatus where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_modificar_usuario(p_idusuario character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 291 (class 1255 OID 34985)
-- Name: p_rechazar_documento(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_rechazar_documento(p_idinfodocumento integer, p_usuario character varying, p_causa character varying, p_motivo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
    estatu integer;
    idInfoDoc integer;
  begin

    update infodocumento set estatus_documento=2 where id_infodocumento= p_idinfodocumento;
    --commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;


    if estatu > 0 then

      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;

      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_rechazo = now(),
               usuario_rechazo = p_usuario,
               motivo_rechazo = p_motivo,
               causa_rechazo = p_causa
         where id_infodocumento = p_idinfodocumento;
      else

        insert into datos_infodocumento(id_infodocumento, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo)
                  values(p_idinfodocumento, now(), p_usuario, p_motivo, p_causa);
      end if;
    else
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      --commit;

    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %', SQLERRM;
  end;$$;


ALTER FUNCTION public.p_rechazar_documento(p_idinfodocumento integer, p_usuario character varying, p_causa character varying, p_motivo character varying) OWNER TO postgres;

--
-- TOC entry 292 (class 1255 OID 34986)
-- Name: p_traza_elimina_documento(character varying, integer, integer, integer, integer, integer, date, date, date, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_traza_elimina_documento(p_idexpedeinte character varying, p_idlibreria integer, p_idcategoria integer, p_idsubcategoria integer, p_iddocumento integer, p_numerodoc integer, p_fechavencimiento date, p_fechadigitalizo date, p_fecharechazo date, p_cantpaginas integer, p_versiondoc integer, p_usuarioelimino character varying, p_usuariodigitalizo character varying, p_usuariorechazo character varying, p_causaelimino character varying, p_motivoelimino character varying, p_causarechazo character varying, p_motivorechazo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
  
    iddoceliminado integer;

  begin

    insert into documento_eliminado
      (id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino)
    values
      (p_idexpedeinte, p_idlibreria, p_idcategoria, p_idsubcategoria, p_iddocumento, p_numerodoc, p_versiondoc, p_cantpaginas, p_fechavencimiento, now(), p_usuarioelimino);

    select lastval() into iddoceliminado;

    if iddoceliminado > 0 then

	 insert into datos_infodocumento
	(id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino)
      values
        (iddoceliminado, p_fechadigitalizo, p_usuariodigitalizo, p_fecharechazo, p_usuariorechazo, p_motivorechazo, p_causarechazo, now(), p_usuarioelimino, p_motivoelimino, p_causaelimino);
 
    else
      rollback;
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %',  SQLERRM;
  end;$$;


ALTER FUNCTION public.p_traza_elimina_documento(p_idexpedeinte character varying, p_idlibreria integer, p_idcategoria integer, p_idsubcategoria integer, p_iddocumento integer, p_numerodoc integer, p_fechavencimiento date, p_fechadigitalizo date, p_fecharechazo date, p_cantpaginas integer, p_versiondoc integer, p_usuarioelimino character varying, p_usuariodigitalizo character varying, p_usuariorechazo character varying, p_causaelimino character varying, p_motivoelimino character varying, p_causarechazo character varying, p_motivorechazo character varying) OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 34990)
-- Name: sq_categroria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_categroria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_categroria OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 35018)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    id_categoria integer DEFAULT nextval('sq_categroria'::regclass) NOT NULL,
    id_libreria integer NOT NULL,
    categoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 35022)
-- Name: causa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE causa (
    id_causa integer NOT NULL,
    causa character varying(150) NOT NULL
);


ALTER TABLE public.causa OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 35025)
-- Name: configuracion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE configuracion (
    id_configuracion integer NOT NULL,
    calidad character(1),
    ruta_temporal character varying(50),
    archivo_tif character varying(50),
    archivo_cod character varying(50),
    log character varying(50),
    foliatura character(1),
    server_name character varying(50),
    database_name character varying(50),
    port integer,
    userbd character varying(50),
    password character varying(50),
    ficha character(1),
    fabrica character(1),
    elimina character(1)
);


ALTER TABLE public.configuracion OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 34992)
-- Name: sq_dato_adicional; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_dato_adicional
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_dato_adicional OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 35028)
-- Name: dato_adicional; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dato_adicional (
    id_dato_adicional integer DEFAULT nextval('sq_dato_adicional'::regclass) NOT NULL,
    indice_adicional character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    id_documento integer NOT NULL,
    codigo integer
);


ALTER TABLE public.dato_adicional OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 34994)
-- Name: sq_datos_infodocumento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_datos_infodocumento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_datos_infodocumento OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 35032)
-- Name: datos_infodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE datos_infodocumento (
    id_datos integer DEFAULT nextval('sq_datos_infodocumento'::regclass) NOT NULL,
    id_infodocumento integer,
    id_doc_eliminado integer,
    fecha_digitalizacion date,
    usuario_digitalizo character varying(30),
    fecha_aprobacion date,
    usuario_aprobacion character varying(30),
    fecha_rechazo date,
    usuario_rechazo character varying(30),
    motivo_rechazo character varying(300),
    causa_rechazo character varying(300),
    fecha_eliminado date,
    usuario_elimino character varying(30),
    motivo_elimino character varying(300),
    causa_elimino character varying(300),
    dato_adicional character varying(30)
);


ALTER TABLE public.datos_infodocumento OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 34996)
-- Name: sq_documento_eliminado; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_documento_eliminado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_documento_eliminado OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 35039)
-- Name: documento_eliminado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documento_eliminado (
    id_doc_eliminado integer DEFAULT nextval('sq_documento_eliminado'::regclass) NOT NULL,
    id_expediente character varying(50) NOT NULL,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    id_documento integer NOT NULL,
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    fecha_eliminado date NOT NULL,
    usuario_elimino character varying(30) NOT NULL
);


ALTER TABLE public.documento_eliminado OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 35043)
-- Name: estatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estatus (
    id_estatus integer NOT NULL,
    estatus character varying(100) NOT NULL
);


ALTER TABLE public.estatus OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 35046)
-- Name: estatus_documento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estatus_documento (
    id_estatus_documento integer NOT NULL,
    estatus_documento character varying(20) NOT NULL
);


ALTER TABLE public.estatus_documento OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 34998)
-- Name: sq_expediente; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_expediente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_expediente OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 35049)
-- Name: expedientes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE expedientes (
    id_expedientes integer DEFAULT nextval('sq_expediente'::regclass) NOT NULL,
    expediente character varying(250) NOT NULL,
    id_indice integer NOT NULL,
    valor character varying(250),
    fecha_indice date,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL
);


ALTER TABLE public.expedientes OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 35056)
-- Name: fabrica; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fabrica (
    usuario character varying(50) NOT NULL,
    fabrica character(1) NOT NULL
);


ALTER TABLE public.fabrica OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 35000)
-- Name: sq_foliatura; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_foliatura
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_foliatura OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 35059)
-- Name: foliatura; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE foliatura (
    id_foliatura integer DEFAULT nextval('sq_foliatura'::regclass) NOT NULL,
    id_infodocumento integer NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50) NOT NULL,
    pagina integer NOT NULL
);


ALTER TABLE public.foliatura OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 35002)
-- Name: sq_indices; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_indices
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_indices OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 35063)
-- Name: indices; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE indices (
    id_indice integer DEFAULT nextval('sq_indices'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    indice character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    codigo integer NOT NULL,
    clave character(1)
);


ALTER TABLE public.indices OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 35004)
-- Name: sq_infodocumento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_infodocumento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_infodocumento OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 35067)
-- Name: infodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE infodocumento (
    id_infodocumento integer DEFAULT nextval('sq_infodocumento'::regclass) NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50),
    nombre_archivo character varying(1000),
    ruta_archivo character varying(1000),
    formato character varying(4),
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    estatus_documento integer NOT NULL,
    re_digitalizado character(1) NOT NULL
);


ALTER TABLE public.infodocumento OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 35006)
-- Name: sq_libreria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_libreria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_libreria OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 35074)
-- Name: libreria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria (
    id_libreria integer DEFAULT nextval('sq_libreria'::regclass) NOT NULL,
    libreria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE public.libreria OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 35008)
-- Name: sq_combo; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_combo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_combo OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 35078)
-- Name: lista_desplegables; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lista_desplegables (
    id_lista integer DEFAULT nextval('sq_combo'::regclass) NOT NULL,
    codigo_indice integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE public.lista_desplegables OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 35010)
-- Name: sq_perfil; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_perfil
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_perfil OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 35082)
-- Name: perfil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE perfil (
    id_perfil integer DEFAULT nextval('sq_perfil'::regclass) NOT NULL,
    id_libreria integer,
    id_categoria integer,
    id_usuario character varying(50) NOT NULL,
    id_rol integer NOT NULL
);


ALTER TABLE public.perfil OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 35350)
-- Name: reporte; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE reporte (
    id_categoria integer,
    expediente character varying(250),
    "número_rif" character varying(250),
    "nombre_o_razón_social" character varying(250),
    tipo_persona character varying(250),
    "número_registro" integer
);


ALTER TABLE public.reporte OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 35086)
-- Name: rol; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rol (
    id_rol integer NOT NULL,
    rol character varying(50) NOT NULL
);


ALTER TABLE public.rol OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 35012)
-- Name: sq_subcategroria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_subcategroria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_subcategroria OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 35014)
-- Name: sq_tipo_documento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_tipo_documento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_tipo_documento OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 35016)
-- Name: sq_valor_dato_adicional; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_valor_dato_adicional
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sq_valor_dato_adicional OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 35089)
-- Name: subcategoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subcategoria (
    id_subcategoria integer DEFAULT nextval('sq_subcategroria'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    subcategoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE public.subcategoria OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 35093)
-- Name: tipodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipodocumento (
    id_documento integer DEFAULT nextval('sq_tipo_documento'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    tipo_documento character varying(200) NOT NULL,
    id_estatus integer NOT NULL,
    vencimiento character(1),
    dato_adicional character(1),
    ficha character(1)
);


ALTER TABLE public.tipodocumento OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 35097)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    id_usuario character varying(50) NOT NULL,
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    cedula character varying(50),
    sexo character(1),
    id_estatus integer NOT NULL,
    password character varying(16)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 35100)
-- Name: valor_dato_adicional; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE valor_dato_adicional (
    id_valor integer DEFAULT nextval('sq_valor_dato_adicional'::regclass) NOT NULL,
    id_dato_adicional integer NOT NULL,
    valor character varying(250) NOT NULL,
    numero integer NOT NULL,
    version integer NOT NULL,
    expediente character varying(250) NOT NULL
);


ALTER TABLE public.valor_dato_adicional OWNER TO postgres;

--
-- TOC entry 3075 (class 0 OID 35018)
-- Dependencies: 184
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (21, 21, '01.-Expedientes de Resumen de Conformidad', 1);
INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (41, 41, '01.-  EXPEDIENTES  DE  DRAW  BACK', 1);
INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (61, 61, 'CATEGORÍA:01.- PRUEBA - EXPEDIENTES DE AGENTES DEADUANA  (AA)', 1);
INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (81, 61, 'CATEGORÍA:02.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS PERSONAS NATURALESBAJO RELACIÓN DE DEPENDENCIA  (AA-PN-BRD)', 1);
INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (101, 61, '03.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS CAPACITADOS ADUANEROS  (AA-CA)', 1);
INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (121, 61, 'CATEGORÍA:03.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS CAPACITADOS ADUANEROS  (AA-CA)', 1);
INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (1, 1, 'CategoriaPrueba', 1);
INSERT INTO categoria (id_categoria, id_libreria, categoria, id_estatus) VALUES (141, 1, 'CategoriaPrueba2', 1);


--
-- TOC entry 3076 (class 0 OID 35022)
-- Dependencies: 185
-- Data for Name: causa; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO causa (id_causa, causa) VALUES (1, 'Mala Calidad en la Imagen');
INSERT INTO causa (id_causa, causa) VALUES (2, 'Mala tipificación del Documento');
INSERT INTO causa (id_causa, causa) VALUES (3, 'Los Índices no concuerdan con el Documento');
INSERT INTO causa (id_causa, causa) VALUES (4, 'Mala Orientación del Documento');
INSERT INTO causa (id_causa, causa) VALUES (5, 'Visualización Nula del Documento');
INSERT INTO causa (id_causa, causa) VALUES (6, 'Falla Técnica del Sistema');
INSERT INTO causa (id_causa, causa) VALUES (7, 'Eliminacion de Documento');


--
-- TOC entry 3077 (class 0 OID 35025)
-- Dependencies: 186
-- Data for Name: configuracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO configuracion (id_configuracion, calidad, ruta_temporal, archivo_tif, archivo_cod, log, foliatura, server_name, database_name, port, userbd, password, ficha, fabrica, elimina) VALUES (1, '1', 'temp', 'documento.tiff', 'codificado.cod', '/lib/log4j-config.properties', '1', '192.168.0.142', 'dw4j', 5432, 'cG9zdGdyZXM=', 'ZGV2ZWxjb20=', '0', '1', '0');


--
-- TOC entry 3078 (class 0 OID 35028)
-- Dependencies: 187
-- Data for Name: dato_adicional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1, 'Dato Texto', 'TEXTO', 1, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (2, 'Dato Numero', 'NUMERO', 1, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (3, 'Dato Fecha', 'FECHA', 1, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (5, 'Dato Area', 'AREA', 1, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (4, 'Dato Combo', 'COMBO', 1, 4);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (21, 'Fecha de Recepción', 'FECHA', 61, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (41, 'Fecha de Recepción', 'FECHA', 62, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (61, 'Fecha de Declaración', 'FECHA', 63, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (81, 'Fecha de Emisión', 'FECHA', 64, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (101, 'Fecha de Emisión', 'FECHA', 65, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (121, 'Fecha de Emisión', 'FECHA', 66, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (141, 'Fecha de Emisión', 'FECHA', 67, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (161, 'Fecha de Notificación', 'FECHA', 68, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (181, 'Fecha de Recepción', 'FECHA', 69, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (201, 'Fecha de Recepción', 'FECHA', 70, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (221, 'Fecha de Recepción', 'FECHA', 71, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (241, 'Fecha de Recepción', 'FECHA', 72, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (261, 'Fecha de Recepción', 'FECHA', 73, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (281, 'Fecha de Recepción', 'FECHA', 74, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (301, 'Fecha de Notificación', 'FECHA', 75, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (321, 'Fecha de Recepción', 'FECHA', 76, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (341, 'Fecha de Emisión', 'FECHA', 77, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (361, 'Descripción', 'TEXTO', 78, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (381, 'Número', 'NUMERO', 101, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (401, 'Fecha', 'FECHA', 121, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (402, 'Número', 'NUMERO', 121, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (421, 'Fecha', 'FECHA', 141, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (422, 'Número', 'NUMERO', 141, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (441, 'Número', 'NUMERO', 202, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (481, 'Número', 'FECHA', 204, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (501, 'Observaciones', 'AREA', 205, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (521, 'Número', 'FECHA', 221, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (522, 'Ano', 'NUMERO', 221, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (541, 'Número', 'NUMERO', 222, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (561, 'Fecha', 'FECHA', 223, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (562, 'Número', 'NUMERO', 223, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (581, 'Fecha', 'FECHA', 224, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (582, 'Número', 'NUMERO', 224, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (601, 'Número', 'NUMERO', 242, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (621, 'Número', 'NUMERO', 243, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (641, 'Observaciones', 'AREA', 244, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (661, 'Número', 'FECHA', 261, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (662, 'Ano', 'NUMERO', 261, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (681, 'Número', 'NUMERO', 262, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (701, 'Número', 'NUMERO', 264, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (721, 'Número', 'NUMERO', 265, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (741, 'Observaciones', 'AREA', 266, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (761, 'Aduana', 'FECHA', 281, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (762, 'Número', 'NUMERO', 281, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (781, 'Aduana', 'FECHA', 282, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (782, 'Número', 'NUMERO', 282, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (801, 'Aduana', 'FECHA', 283, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (802, 'Número', 'NUMERO', 283, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (821, 'Aduana', 'FECHA', 284, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (822, 'Número', 'NUMERO', 284, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (841, 'Aduana', 'FECHA', 285, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (842, 'Número', 'NUMERO', 285, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (861, 'Aduana', 'FECHA', 286, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (862, 'Número', 'NUMERO', 286, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (881, 'Observaciones', 'AREA', 287, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (382, 'Ano', 'COMBO', 101, 382);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (403, 'Ano', 'COMBO', 121, 403);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (423, 'Ano', 'COMBO', 141, 423);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (442, 'Ano', 'COMBO', 202, 442);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (461, 'Número', 'COMBO', 203, 461);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (462, 'Ano', 'COMBO', 203, 462);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (482, 'Ano', 'COMBO', 204, 482);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (483, 'Aduana', 'COMBO', 204, 483);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (523, 'Aduana', 'COMBO', 221, 523);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (542, 'Ano', 'COMBO', 222, 542);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (563, 'Ano', 'COMBO', 223, 563);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (583, 'Ano', 'COMBO', 224, 583);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (602, 'Ano', 'COMBO', 242, 602);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (622, 'Ano', 'COMBO', 243, 622);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (663, 'Aduana', 'COMBO', 261, 663);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (682, 'Ano', 'COMBO', 262, 682);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (702, 'Ano', 'COMBO', 264, 702);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (722, 'Ano', 'COMBO', 265, 722);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (763, 'Ano', 'COMBO', 281, 763);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (783, 'Ano', 'COMBO', 282, 783);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (803, 'Ano', 'COMBO', 283, 803);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (823, 'Ano', 'COMBO', 284, 823);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (843, 'Ano', 'COMBO', 285, 843);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (863, 'Ano', 'COMBO', 286, 863);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (901, '.-Aduana', 'COMBO', 281, 901);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (921, '.-Aduana', 'COMBO', 282, 921);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (941, '.-Aduana', 'COMBO', 283, 941);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (961, '.-Aduana', 'COMBO', 284, 961);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (981, '.-Aduana', 'COMBO', 285, 981);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1001, '.-Aduana', 'COMBO', 286, 1001);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1021, 'Número', 'NUMERO', 321, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1041, 'Número', 'NUMERO', 325, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1061, 'Número', 'NUMERO', 326, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1081, 'Número', 'NUMERO', 327, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1101, 'Observaciones', 'AREA', 329, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1121, 'Número RIF', 'TEXTO', 341, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1122, 'Fecha Incorporación', 'FECHA', 341, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1141, 'Número RIF', 'TEXTO', 361, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1142, 'Fecha Incorporación', 'FECHA', 361, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1161, 'Número RIF', 'TEXTO', 381, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1162, 'Fecha Desincorporación', 'FECHA', 381, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1022, 'Ano', 'COMBO', 321, 1022);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1042, 'Ano', 'COMBO', 325, 1042);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1062, 'Ano', 'COMBO', 326, 1062);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1082, 'Ano', 'COMBO', 327, 1082);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1083, 'Aduana', 'COMBO', 327, 1083);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1123, 'Aduana', 'COMBO', 341, 1123);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1143, 'Aduana', 'COMBO', 361, 1143);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1163, 'Aduana', 'COMBO', 381, 1163);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1181, 'Número RIF', 'TEXTO', 421, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1182, 'Fecha Inclusión', 'FECHA', 421, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1201, 'Número RIF', 'TEXTO', 441, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1202, 'Fecha Exclusión', 'FECHA', 441, NULL);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1183, 'Aduana', 'COMBO', 421, 1183);
INSERT INTO dato_adicional (id_dato_adicional, indice_adicional, tipo, id_documento, codigo) VALUES (1203, 'Aduana', 'COMBO', 441, 1203);


--
-- TOC entry 3079 (class 0 OID 35032)
-- Dependencies: 188
-- Data for Name: datos_infodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1, 1, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (21, 21, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (41, 41, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (61, 61, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (81, 81, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (101, 101, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (121, 121, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (141, 141, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (161, 161, NULL, '2014-10-13', 'eescalona', '2014-10-13', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3921, 3281, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3861, 3221, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO INVERTIDO, DEBE SER SER REEMPLAZADO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3981, 3341, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (201, 201, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (401, 401, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO CON MALA CALIDAD', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (441, 441, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (461, 461, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (481, 481, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO VOLTEADO DEBE SER ELMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (501, 501, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (621, 621, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (641, 641, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO CON PESIMA CALIDAD. DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (521, 521, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO ERRONEO. DEBE SER ELMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (541, 541, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (581, 581, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (221, 221, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO ERRONEO. DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (241, 241, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO VENCIDO, DEBE SER VERSIONADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (421, 421, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (261, 261, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO INVERTIDO, DEBE SER REEMPLAZADO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (281, 281, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (301, 301, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (321, 321, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO CON INFORMACION ADICIONAL ERRONEA', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (341, 341, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (361, 361, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (381, 381, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (801, 801, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (861, 861, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (841, 841, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (781, 781, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'VERSIONAR DOCUMENTO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (721, 721, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (761, 761, NULL, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (881, NULL, 1, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'DOCUMENTO INVERTIDO. DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', '2014-10-17', 'tgarrido', 'Prueba de eliminacion', 'Mala Orientación del Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (901, NULL, 21, '2014-10-16', 'nilda_blanco', NULL, NULL, '2014-10-16', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', '2014-10-17', 'tgarrido', 'Eliminacion de documento', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (821, 821, NULL, '2014-10-16', 'nilda_blanco', '2014-10-16', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (941, 901, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (961, 921, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1021, 981, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1041, 1001, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1901, 1861, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1121, 1081, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1141, 1101, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1161, 1121, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1701, 1661, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1301, 1261, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO MAL TIPIFICADO, DEBE SER REEMPLAZADO', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1361, 1321, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1401, 1361, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1421, 1381, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1441, 1401, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1461, 1421, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1501, 1461, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1521, 1481, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1561, 1521, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1581, 1541, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1601, 1561, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1641, 1601, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1941, 1901, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2001, 1961, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1721, 1681, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1741, 1701, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1961, 1921, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1781, 1741, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1821, 1781, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1841, 1801, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1861, 1821, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2061, 2021, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2081, 2041, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2101, 2061, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2121, 2081, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2021, 1981, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4201, NULL, 681, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'prueba', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4241, NULL, 721, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'Para eliminar', 'Los Índices no concuerdan con el Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2301, 2261, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2321, 2281, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2381, 2341, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2401, 2361, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO MAL VISUALIZADO DEBE SER REEMPLAZADO', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2421, 2381, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DE OTRO EXPEDIENTE ', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2281, 2241, NULL, '2014-10-20', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2441, 2401, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2461, 2421, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2481, 2441, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2361, 2321, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO POR PESAR MAS DE LO ESTABLECIDO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2601, NULL, 41, '2014-10-20', 'eescalona', NULL, NULL, '2014-10-20', 'eescalona', 'eliminar ', 'Mala Orientación del Documento', '2014-10-20', 'eescalona', 'eliminado', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (981, 941, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1001, 961, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2521, 2481, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2541, 2501, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO POR PESIMA VISUALIZACION', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1181, 1141, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3881, 3241, NULL, '2014-10-21', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1281, 1241, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1321, 1281, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'INFORMACION ADICIONAL ERRONEA. DEBE SER REEMPLAZADO', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1341, 1301, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO INVERTIDO, DEBE SER REEMPLAZADO', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1381, 1341, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO Y MODIFICADO', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1541, 1501, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1621, 1581, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Mala Calidad en la Imagen', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (1661, 1621, NULL, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2801, NULL, 61, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '1.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2821, NULL, 81, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '2.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Mala Calidad en la Imagen', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2841, NULL, 101, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Visualización Nula del Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2861, NULL, 121, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '3.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2881, NULL, 141, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '8.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2901, NULL, 161, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', '9.DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2921, NULL, 181, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO DEFINITIVAMENTE', 'Visualización Nula del Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2661, 2601, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2681, 2621, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2721, 2661, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2701, 2641, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2741, 2681, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2761, 2701, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2621, 2561, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2781, 2721, NULL, '2014-10-20', 'nilda_blanco', '2014-10-20', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2641, 2581, NULL, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER REEMPLAZADO', 'Mala tipificación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2561, 2521, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', 'MALA CALIDAD DE LA IMAGEN', 'Visualización Nula del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2581, 2541, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3941, 3301, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4001, 3361, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4041, NULL, 661, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Rechazo para Eliminación', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'Eliminacion por pruebas', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4161, 3481, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4281, 3541, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3221, 3021, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3201, 3001, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3241, NULL, 201, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO MUY PESADO. DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO POR PESAR MAS DE LO ESTABLECIDO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (2041, 2001, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3261, NULL, 221, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3281, NULL, 241, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3301, NULL, 261, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3321, NULL, 281, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3341, NULL, 301, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3361, NULL, 321, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3381, NULL, 341, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3401, NULL, 361, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3421, NULL, 381, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3441, NULL, 401, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-20', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3461, NULL, 421, '2014-10-20', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'ELIMINAR DOCUMENTO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3481, NULL, 441, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3501, NULL, 461, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3521, NULL, 481, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3541, NULL, 501, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3581, NULL, 521, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3601, NULL, 541, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3621, NULL, 561, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3821, 3181, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3641, NULL, 581, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3661, NULL, 601, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3681, NULL, 621, '2014-10-21', 'nilda_blanco', NULL, NULL, '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', '2014-10-21', 'nilda_blanco', 'DOCUMENTO DEBE SER ELIMINADO', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3561, 3041, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3721, 3081, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3741, 3101, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3761, 3121, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3781, 3141, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'rechazo por prueba', 'Mala Orientación del Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3841, 3201, NULL, '2014-10-21', 'nilda_blanco', '2014-10-21', 'nilda_blanco', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3901, 3261, NULL, '2014-10-21', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (3961, 3321, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4021, NULL, 641, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', '
Rechazo para redigitalizacion en pdf', 'Falla Técnica del Sistema', '2014-10-21', 'javier_plaza', 'Eliminado por pruebas', 'Eliminacion de Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4061, 3381, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4141, 3461, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Los Índices no concuerdan con el Documento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4181, 3501, NULL, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4221, NULL, 701, '2014-10-21', 'javier_plaza', NULL, NULL, '2014-10-21', 'javier_plaza', 'Para eliminar', 'Eliminacion de Documento', '2014-10-21', 'javier_plaza', 'prueba', 'Los Índices no concuerdan con el Documento', NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4261, 3521, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento (id_datos, id_infodocumento, id_doc_eliminado, fecha_digitalizacion, usuario_digitalizo, fecha_aprobacion, usuario_aprobacion, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional) VALUES (4301, 3561, NULL, '2014-10-21', 'javier_plaza', '2014-10-21', 'javier_plaza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3080 (class 0 OID 35039)
-- Dependencies: 189
-- Data for Name: documento_eliminado; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (1, '18110507', 41, 41, 42, 67, 1, 0, 0, NULL, '2014-10-17', 'tgarrido');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (21, '18331751', 41, 41, 42, 68, 1, 0, 0, NULL, '2014-10-17', 'tgarrido');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (41, '01', 1, 1, 1, 1, 4, 0, 0, '2014-10-31', '2014-10-20', 'eescalona');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (61, '17718114', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (81, '17718114', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (101, '17718114', 61, 61, 81, 205, 8, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (121, '17718114', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (141, '17718114', 61, 61, 81, 205, 5, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (161, '17718114', 61, 61, 81, 205, 5, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (181, '17718114', 61, 61, 81, 205, 5, 0, 0, NULL, '2014-10-20', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (201, '16415223', 61, 61, 81, 203, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (221, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (241, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (261, '16415223', 61, 61, 81, 205, 4, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (281, '16415223', 61, 61, 81, 205, 6, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (301, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (321, '16415223', 61, 61, 81, 205, 4, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (341, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (361, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (381, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (401, '16415223', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (421, '16415223', 61, 61, 101, 244, 1, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (441, '16415223', 61, 61, 141, 281, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (461, '16415223', 61, 61, 141, 281, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (481, '16415223', 61, 61, 141, 281, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (501, '16415223', 61, 61, 141, 287, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (521, '16415223', 61, 61, 141, 281, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (541, '16415223', 61, 61, 141, 287, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (561, '16415223', 61, 61, 141, 287, 4, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (581, '16415223', 61, 61, 141, 287, 3, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (601, '16415223', 61, 61, 141, 287, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (621, '16415223', 61, 61, 141, 287, 2, 0, 0, NULL, '2014-10-21', 'nilda_blanco');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (641, 'G-1234567-8', 61, 61, 81, 202, 1, 0, 0, NULL, '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (661, 'G-1234567-8', 61, 61, 81, 121, 1, 0, 0, '2010-12-31', '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (681, 'G-1234567-8', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (701, 'G-1234567-8', 61, 61, 81, 205, 2, 0, 0, NULL, '2014-10-21', 'javier_plaza');
INSERT INTO documento_eliminado (id_doc_eliminado, id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino) VALUES (721, 'G-1234567-8', 61, 61, 81, 205, 1, 0, 0, NULL, '2014-10-21', 'javier_plaza');


--
-- TOC entry 3081 (class 0 OID 35043)
-- Dependencies: 190
-- Data for Name: estatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estatus (id_estatus, estatus) VALUES (1, 'Activo');
INSERT INTO estatus (id_estatus, estatus) VALUES (2, 'Inactivo');


--
-- TOC entry 3082 (class 0 OID 35046)
-- Dependencies: 191
-- Data for Name: estatus_documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estatus_documento (id_estatus_documento, estatus_documento) VALUES (0, 'Pendiente');
INSERT INTO estatus_documento (id_estatus_documento, estatus_documento) VALUES (1, 'Aprobado');
INSERT INTO estatus_documento (id_estatus_documento, estatus_documento) VALUES (2, 'Rechazado');


--
-- TOC entry 3083 (class 0 OID 35049)
-- Dependencies: 192
-- Data for Name: expedientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (21, '18331750', 21, '18331750', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (22, '18331750', 22, NULL, '2014-03-10', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (23, '18331750', 23, 'V-18331750', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (24, '18331750', 24, 'Nilda', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (25, '18331750', 25, 'Blanco', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (26, '18331750', 26, NULL, '2013-11-12', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (27, '18331750', 27, 'A3-1-1', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (28, '18331750', 28, 'Recibido', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (41, '18110507', 21, '18110507', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (42, '18110507', 22, NULL, '2013-11-12', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (43, '18110507', 23, 'J-18110507', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (44, '18110507', 24, 'Kenny Ramirez', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (45, '18110507', 25, '18110507', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (61, '18110507', 21, '18110507', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (62, '18110507', 22, NULL, '2013-11-12', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (63, '18110507', 23, 'J-18110507', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (64, '18110507', 24, 'Kenny Ramirez', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (65, '18110507', 25, '18110507', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (66, '18110507', 26, NULL, '2013-06-02', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (67, '18110507', 27, '', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (68, '18110507', 28, '', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (81, '18331751', 21, '18331751', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (82, '18331751', 22, NULL, '1987-03-10', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (83, '18331751', 23, 'J-18331751', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (84, '18331751', 24, 'SAMIR ALVAREZ', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (85, '18331751', 25, '121188', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (86, '18331751', 26, NULL, '1998-02-06', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (87, '18331751', 27, 'A5-1-1', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (88, '18331751', 28, 'Recibido', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (1, '01', 1, '01', NULL, 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (2, '01', 2, '123456', NULL, 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (3, '01', 3, NULL, '2014-10-13', 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (4, '01', 4, 'Dato1', NULL, 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (5, '01', 5, 'direccion prueba actualizar', NULL, 1, 1);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (101, '17714118', 21, '17714118', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (102, '17714118', 22, NULL, '2014-10-17', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (103, '17714118', 23, 'V-17714118', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (104, '17714118', 24, 'ALI SUAREZ', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (105, '17714118', 25, '17714118', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (106, '17714118', 26, NULL, '2014-09-17', 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (107, '17714118', 27, 'A1-1-1', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (108, '17714118', 28, 'Recibido', NULL, 41, 41);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (121, '17718114', 41, '17718114', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (122, '17718114', 42, 'ALI SUAREZ', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (123, '17718114', 43, 'Firma Personal', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (124, '17718114', 44, '17718114', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (125, '17718114', 45, '17718114', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (126, '17718114', 46, NULL, '2014-10-17', 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (127, '17718114', 47, '1', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (128, '17718114', 48, NULL, '2014-10-17', 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (129, '17718114', 49, 'BOCA DE GRITA', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (130, '17718114', 50, 'Activo', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (141, '16415223', 81, '16415223', NULL, 61, 121);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (142, '16415223', 82, 'DAVID MEDINA', NULL, 61, 121);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (143, '16415223', 83, '16415223', NULL, 61, 121);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (144, '16415223', 84, NULL, '1987-05-24', 61, 121);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (145, '16415223', 85, 'PTO. CABELLO', NULL, 61, 121);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (146, '16415223', 86, 'Activo', NULL, 61, 121);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (181, '4567890', 41, '4567890', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (182, '4567890', 42, 'PILAR GARCIA', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (183, '4567890', 43, 'Persona Jurídica', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (184, '4567890', 44, '4567890', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (185, '4567890', 45, '4567890', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (186, '4567890', 46, NULL, '1990-11-08', 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (187, '4567890', 47, '0811', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (188, '4567890', 48, NULL, '1991-10-12', 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (189, '4567890', 49, 'PTO. PALMARITO', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (190, '4567890', 50, 'Activo', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (201, 'G-1234567-8', 41, 'G-1234567-8', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (202, 'G-1234567-8', 42, 'Corporacion Venezolana de Guayana CVG', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (203, 'G-1234567-8', 43, 'Persona Jurídica', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (204, 'G-1234567-8', 44, '123456', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (205, 'G-1234567-8', 45, '2008-1452', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (206, 'G-1234567-8', 46, NULL, '2008-12-12', 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (207, 'G-1234567-8', 47, '5268', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (208, 'G-1234567-8', 48, NULL, '2008-10-30', 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (209, 'G-1234567-8', 49, 'LA GUAIRA', NULL, 61, 61);
INSERT INTO expedientes (id_expedientes, expediente, id_indice, valor, fecha_indice, id_libreria, id_categoria) VALUES (210, 'G-1234567-8', 50, 'Activo', NULL, 61, 61);


--
-- TOC entry 3084 (class 0 OID 35056)
-- Dependencies: 193
-- Data for Name: fabrica; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO fabrica (usuario, fabrica) VALUES ('eescalona', '0');
INSERT INTO fabrica (usuario, fabrica) VALUES ('nilda_blanco', '0');
INSERT INTO fabrica (usuario, fabrica) VALUES ('tgarrido', '0');
INSERT INTO fabrica (usuario, fabrica) VALUES ('javier_plaza', '0');


--
-- TOC entry 3085 (class 0 OID 35059)
-- Dependencies: 194
-- Data for Name: foliatura; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (261, 3041, 101, 'G-1234567-8', 1);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (262, 3081, 141, 'G-1234567-8', 2);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (263, 3081, 141, 'G-1234567-8', 3);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (264, 3081, 141, 'G-1234567-8', 4);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (265, 3081, 141, 'G-1234567-8', 5);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (266, 3081, 141, 'G-1234567-8', 6);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (267, 3321, 161, 'G-1234567-8', 7);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (268, 3281, 161, 'G-1234567-8', 8);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (269, 3281, 161, 'G-1234567-8', 9);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (270, 3281, 161, 'G-1234567-8', 10);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (271, 3281, 161, 'G-1234567-8', 11);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (272, 3281, 161, 'G-1234567-8', 12);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (273, 3281, 161, 'G-1234567-8', 13);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (274, 3281, 161, 'G-1234567-8', 14);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (275, 3281, 161, 'G-1234567-8', 15);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (276, 3281, 161, 'G-1234567-8', 16);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (277, 3281, 161, 'G-1234567-8', 17);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (278, 3101, 161, 'G-1234567-8', 18);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (279, 3301, 181, 'G-1234567-8', 19);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (280, 3121, 181, 'G-1234567-8', 20);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (281, 3381, 201, 'G-1234567-8', 21);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (282, 3361, 203, 'G-1234567-8', 22);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (283, 3561, 205, 'G-1234567-8', 23);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (284, 3561, 205, 'G-1234567-8', 24);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (285, 3541, 205, 'G-1234567-8', 25);
INSERT INTO foliatura (id_foliatura, id_infodocumento, id_documento, id_expediente, pagina) VALUES (286, 3521, 205, 'G-1234567-8', 26);


--
-- TOC entry 3086 (class 0 OID 35063)
-- Dependencies: 195
-- Data for Name: indices; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (1, 1, 'Indice Primario Texto', 'TEXTO', 0, 'Y');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (2, 1, 'Indice Segundo Numero', 'NUMERO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (3, 1, 'Indice Tercero Fecha', 'FECHA', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (5, 1, 'Indice Quinto Area', 'AREA', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (4, 1, 'Indice Cuarto Combo', 'COMBO', 4, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (21, 41, 'Número de Solicitud', 'NUMERO', 0, 'Y');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (22, 41, 'Fecha de Solicitud', 'FECHA', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (23, 41, 'Número de RIF', 'TEXTO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (24, 41, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (25, 41, 'Número de Declaración', 'TEXTO', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (26, 41, 'Fecha de Declaración', 'FECHA', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (27, 41, 'Ubicación Física', 'COMBO', 27, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (28, 41, 'Estatus del Expediente', 'COMBO', 28, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (41, 61, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (42, 61, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (44, 61, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (45, 61, 'Número Providencia Administrativa', 'TEXTO', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (46, 61, 'Fecha Providencia Administrativa', 'FECHA', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (47, 61, 'Número Gaceta Oficial', 'TEXTO', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (48, 61, 'Fecha Gaceta Oficial', 'FECHA', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (43, 61, 'Tipo Persona', 'COMBO', 43, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (49, 61, 'Aduana', 'COMBO', 49, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (50, 61, 'Estatus de Expediente', 'COMBO', 50, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (61, 81, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (62, 81, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (63, 81, 'Número Registro', 'NUMERO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (64, 81, 'Número Providencia Administrativa', 'TEXTO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (65, 81, 'Fecha Providencia Administrativa', 'FECHA', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (66, 81, 'Número Gaceta Oficial', 'TEXTO', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (67, 81, 'Fecha Gaceta Oficial', 'FECHA', 0, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (68, 81, 'Aduana', 'COMBO', 68, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (69, 81, 'Estatus de Expediente', 'COMBO', 69, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (81, 121, 'Número RIF', 'TEXTO', 0, 'Y');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (82, 121, 'Nombre o Razón Social', 'TEXTO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (83, 121, 'Número Registro', 'TEXTO', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (84, 121, 'Fecha Registro', 'FECHA', 0, 'S');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (85, 121, 'Aduana', 'COMBO', 85, ' ');
INSERT INTO indices (id_indice, id_categoria, indice, tipo, codigo, clave) VALUES (86, 121, 'Estatus de Expediente', 'COMBO', 86, ' ');


--
-- TOC entry 3087 (class 0 OID 35067)
-- Dependencies: 196
-- Data for Name: infodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (281, 68, '18331750', '923e87e5be075189f373b21e0f12cb2fe3796ae838835da0b6f6ea37bcf8bcb7-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (61, 1, '01', '2cba044b496b2d7d0c77a42d1d7b7c6f7f39f8317fbdb1988ef4c628eba02591-1', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 1, 1, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (81, 1, '01', '025cac2b0de928a6c51cdbdba840d26843ec517d68b6edd3015b3edc9a11367b-1', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'jpg', 2, 1, 1, '2014-11-30', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (101, 1, '01', 'ad783cd17bf6a8bf3a8f84b9ed92affb38b3eff8baf56627478ec76a704e9b52-1', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 3, 1, 1, '2014-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (301, 72, '18331750', '1275797809c15f8cd3aaed9564c9ddc334ed066df378efacc9b924ec161e7639-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 3, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (321, 75, '18331750', '8d72d315a88575960facaf51d43e644acaf1a3dfb505ffed0d024130f58c5cfa-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (121, 1, '01', '0f3f8b833be1a515dc723845994419414c56ff4ce4aaf9573aa5dff913df997a-2', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'jpg', 1, 2, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (141, 1, '01', 'ec919513da4e72e0bf9b497dc2be347b0f28b5d49b3020afeecd95b4009adf4c-2', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 2, 2, 1, '2014-11-30', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (161, 1, '01', '4042a708b14bbcc656ba9539e175573cbd4c9ab730f5513206b999ec0d90d1fb-2', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 3, 2, 1, '2014-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (341, 75, '18331750', '8d72d315a88575960facaf51d43e644a3dd48ab31d016ffcbf3314df2b3cb9ce-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (21, 1, '01', 'a739cf3f0c217c270b0b7298068ba2913c59dc048e8850243be8079a5c74d079-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'pdf', 2, 0, 1, '2014-11-30', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (41, 1, '01', 'a739cf3f0c217c270b0b7298068ba2913416a75f4cea9109507cacd8e2f2aefc-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'jpg', 3, 0, 1, '2014-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (621, 63, '18110507', 'ea3d85914a75a95b5b0f8358eab9665385fc37b18c57097425b52fc7afbb6969-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (401, 62, '18331750', '2d6c9e9c8a10cd696d6fb223cd60ef79816b112c6105b3ebd537828a39af4818-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (241, 64, '18331750', 'e8e536b925e0a050beb32316faba4945f340f1b1f65b6df5b5e3f94d95b11daf-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (421, 65, '18331750', '01c2e6e78cdd8d1e88fbec4a604418b0e0c641195b27425bb056ac56f8953d24-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (361, 76, '18331750', 'f20f8281ae998114905a8616249ed22452720e003547c70561bf5e03b95aa99f-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (381, 78, '18331750', '752fe60b9e0543e7b8d25a6f6bd2952700ec53c4682d36f5c4359f4ae7bd7ba1-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (481, 61, '18110507', '35f16ae87de3d7a1ecd64d826ee4cb789461cce28ebe3e76fb4b931c35a169b0-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (441, 62, '18331750', '2d6c9e9c8a10cd696d6fb223cd60ef7915d4e891d784977cacbfcbb00c48f133-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (261, 66, '18331750', '1c91f57ac50c00b2c34506f86f5d7a1db1a59b315fc9a3002ce38bbe070ec3f5-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 6, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (461, 62, '18331750', '2d6c9e9c8a10cd696d6fb223cd60ef790353ab4cbed5beae847a7ff6e220b5cf-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1, 1, '01', 'a739cf3f0c217c270b0b7298068ba291c4ca4238a0b923820dcc509a6f75849b-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 1, 0, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (501, 62, '18110507', '2d6c9e9c8a10cd696d6fb223cd60ef795b69b9cb83065d403869739ae7f0995e-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (541, 66, '18110507', '1c91f57ac50c00b2c34506f86f5d7a1d16c222aa19898e5058938167c8ab6c57-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3261, 2, '01', 'ebded8a31c5c3b36bad010a310bd57b795177e528f8d6c7c28a5473fd5a471b6-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 2, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (581, 68, '18110507', '923e87e5be075189f373b21e0f12cb2fc6e19e830859f2cb9f7c8f8cacb8d2a6-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (221, 63, '18331750', 'ea3d85914a75a95b5b0f8358eab96653060ad92489947d410d897474079c1477-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'pdf', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3361, 203, 'G-1234567-8', '3e85182069d5d9dd7b0b81f8fc708b9a8d9766a69b764fefc12f56739424d136-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (521, 64, '18110507', 'e8e536b925e0a050beb32316faba494507563a3fe3bbe7e3ba84431ad9d055af-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3461, 205, 'G-1234567-8', '752fe60b9e0543e7b8d25a6f6bd295277e8d7e5ccbddfd9576be61e3ab86aa73-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (201, 61, '18331750', '35f16ae87de3d7a1ecd64d826ee4cb78757b505cfd34c64c85ca5b5690ee5293-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1101, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd29527c6bff625bdb0393992c9d4db0c6bbe45-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3181, 242, '4567890', '9a327d1072fabff1a9057bca9db13bd7f4aa0dd960521e045ae2f20621fb4ee9-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3481, 205, 'G-1234567-8', '752fe60b9e0543e7b8d25a6f6bd295273fb04953d95a94367bb133f862402bce-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 2, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (821, 61, '18110507', '6a1be40c9d6854b9e84a50f724cb11594558dbb6f6f8bb2e16d03b85bde76e2c-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3281, 161, 'G-1234567-8', 'b072911f7517dd93353ab854a3177e7360a70bb05b08d6cd95deb3bdb750dce8-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 1, 10, '2015-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (801, 62, '18110507', '5a1434c2341b284becace05845972f321905aedab9bf2477edc068a355bba31a-1', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (861, 63, '18110507', '31f44729b4943d0f6e088cb6d23fcf8bf9a40a4780f5e1306c46f1c8daecee3b-1', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 2, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (841, 66, '18110507', 'ad2f01434ba5e1cc6884a69b05c850ba02a32ad2669e6fe298e607fe7cc0e1a0-1', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 1, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (641, 63, '18110507', 'ea3d85914a75a95b5b0f8358eab9665367e103b0761e60683e83c559be18d40c-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 3, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (781, 65, '18331751', '01c2e6e78cdd8d1e88fbec4a604418b07143d7fbadfa4693b9eec507d9d37443-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3381, 201, 'G-1234567-8', '268bc283be64fb07fa6b9c605f4f7df76e16656a6ee1de7232164767ccfa7920-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, '2010-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (721, 77, '18331751', 'a7b10532accb7d13ff37f70cf4484bedaba3b6fd5d186d28e06ff97135cade7f-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (761, 78, '18331751', '752fe60b9e0543e7b8d25a6f6bd2952788ae6372cfdc5df69a976e893f4d554b-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (901, 62, '17714118', '2d6c9e9c8a10cd696d6fb223cd60ef79892c91e0a653ba19df81a90f89d99bcd-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1121, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd295273a15c7d0bbe60300a39f76f8a5ba6896-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (921, 64, '17714118', 'e8e536b925e0a050beb32316faba4945430c3626b879b4005d41b8a46172e0c0-0', 'd109a0688d56be4ba9570e989ffb0b25/253ecfa490231fd15d725830874d63cd/393ee7400b96f96b2d751605fda325d9', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1141, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd29527f7f580e11d00a75814d2ded41fe8e8fe-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 4, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3541, 205, 'G-1234567-8', '89e7f1f4ce4043ba1ff352de83e0b07cc5f5c23be1b71adb51ea9dc8e9d444a8-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (961, 121, '17718114', 'ef857acb97c2e362b608f99c0255bb0cd707329bece455a462b58ce00d1194c9-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2015-07-21', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (981, 181, '17718114', 'f6b121340c85b55db6c672e33c283aae287e03db1d99e0ec2edb90d079e142f3-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2015-06-01', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1001, 202, '17718114', '2604c1897bbc3f7e6c7be05be59db36ab8c37e33defde51cf91e1e03e51657da-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1081, 205, '17718114', '752fe60b9e0543e7b8d25a6f6bd2952736a16a2505369e0c922b6ea7a23a56d2-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1261, 224, '17718114', '450bed356087d626e24a049b3f35defd17326d10d511828f6b34fa6d751739e2-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '2016-06-01', 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1281, 241, '17718114', '4c6c2287ced2e3a3243029b0fcc925e3d94e18a8adb4cc0f623f7a83b1ac75b4-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '2014-11-11', 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1301, 242, '17718114', '9a327d1072fabff1a9057bca9db13bd72df45244f09369e16ea3f9117ca45157-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1321, 242, '17718114', '9a327d1072fabff1a9057bca9db13bd7f9be311e65d81a9ad8150a60844bb94c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 2, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1341, 244, '17718114', 'c964af849361c451de597f91ff444a6a33ebd5b07dc7e407752fe773eed20635-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1361, 262, '17718114', '659747fd917f32c719b276ecea951508cf9a242b70f45317ffd281241fa66502-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1241, 221, '17718114', 'e95efe46ecfc71e51d415f00d36a21381c65cef3dfd1e00c0b03923a1c591db4-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1421, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b9aa42b31882ec039965f3c4923ce901b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1461, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b95151403b0db4f75bfd8da0b393af853-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3201, 244, '4567890', 'c964af849361c451de597f91ff444a6a24ec8468b67314c2013d215b77034476-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1481, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b4e8412ad48562e3c9934f45c3e144d48-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 3, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1501, 266, '17718114', 'a2299896b628c1798e85d68a8bde231b5cbdfd0dfa22a3fca7266376887f549b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 4, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1521, 281, '17718114', '0b6093e91d6c565882bd1af19c7b388f253f7b5d921338af34da817c00f42753-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2001, 265, '16415223', '4776d05c920ee1c2eedd53625de72b86d0fb963ff976f9c37fc81fe03c21ea7b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '2003-02-15', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1541, 284, '17718114', '6a16ad0007dca405d7cce200f23f237d1373b284bc381890049e92d324f56de0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1561, 285, '17718114', '922f79842924cbd161fa93420b178793b132ecc1609bfcf302615847c1caa69a-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1861, 101, '16415223', 'e42de1fa95baac471eecbf8f1f312138f9d1152547c0bde01830b7e8bd60024c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1601, 287, '17718114', 'fbc4b2a492ed2e54929eb69312301c1ec559da2ba967eb820766939a658022c8-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1621, 287, '17718114', 'fbc4b2a492ed2e54929eb69312301c1e4462bf0ddbe0d0da40e1e828ebebeb11-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 2, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1581, 286, '17718114', 'f315c06bf0c4da671672bd0b0651ee4e88a199611ac2b85bd3f76e8ee7e55650-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, '2020-08-15', 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1901, 141, '16415223', '5742286f3c6907872e67f5c203d4e5b1d54e99a6c03704e95e6965532dec148b-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2003-03-11', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1781, 221, '16415223', 'e95efe46ecfc71e51d415f00d36a21388b6a80c3cf2cbd5f967063618dc54f39-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1701, 181, '16415223', 'f6b121340c85b55db6c672e33c283aae15231a7ce4ba789d13b722cc5c955834-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '1988-11-11', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1921, 202, '16415223', '2604c1897bbc3f7e6c7be05be59db36a9f6992966d4c363ea0162a056cb45fe5-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3301, 181, 'G-1234567-8', 'c02e7d715d0b7654044a8aabce2df4ff8217bb4e7fa0541e0f5e04fea764ab91-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 1, 1, '2016-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1801, 223, '16415223', '9ac235aa477ee62c435f98721264a307cd14821dab219ea06e2fd1a2df2e3582-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '1998-11-11', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1821, 242, '16415223', '9a327d1072fabff1a9057bca9db13bd7596dedf4498e258e4bdc9fd70df9a859-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2081, 287, '16415223', 'c32c51a2aeff86ad96dc15998e3fbaeb0070d23b06b1486a538c0eaa45dd167a-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1981, 263, '16415223', '96aebd65e843ad4144b098af44775a3fb3b4d2dbedc99fe843fd3dedb02f086f-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2021, 281, '16415223', '809c822955c062ce078df7b3f1393f7d05a5cf06982ba7892ed2a6d38fe832d6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1961, 261, '16415223', 'e8be9944201791ffd0a128644a2c61a5f106b7f99d2cb30c3db1c3cc0fde9ccb-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1681, 161, '16415223', 'ea26359f89d921345bc68fec7606720af50a6c02a3fc5a3a5d4d9391f05f3efc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 5, '2012-12-21', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1741, 204, '16415223', 'c66dc80afdea51b798b3913974cc38dfb3b43aeeacb258365cc69cdaf42a68af-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1661, 121, '16415223', 'ef857acb97c2e362b608f99c0255bb0c7d12b66d3df6af8d429c1a357d8b9e1a-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, '2006-06-05', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3501, 205, 'G-1234567-8', '752fe60b9e0543e7b8d25a6f6bd29527d494020ff8ec181ef98ed97ac3f25453-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 3, 0, 1, NULL, 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2041, 283, '16415223', 'b9c894170eb9f7aaa6b77733b05c6e4f89885ff2c83a10305ee08bd507c1049c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2061, 285, '16415223', '18eb0a7b230aa5d9ab5b5fb13905f9ac52dbb0686f8bd0c0c757acf716e28ec0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1401, 265, '17718114', '465717bdf0c25516f05ce1c5516ff00b9701a1c165dd9420816bfec5edd6c2b1-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '2018-06-01', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2361, 262, '4567890', '2a79282c361091cea02cd7338c6eab48794288f252f45d35735a13853e605939-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2321, 242, '4567890', '9a327d1072fabff1a9057bca9db13bd7761c7920f470038d4c8a619c79eddd62-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 8, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2421, 266, '4567890', '433f9dfc3feead72372eba69f6a3954ee139c454239bfde741e893edb46a06cc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 2, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2441, 266, '4567890', 'a2299896b628c1798e85d68a8bde231b7a68443f5c80d181c42967cd71612af1-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 3, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3221, 285, '4567890', '922f79842924cbd161fa93420b178793f5c150afbfbcef941def203e85cf40bc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2241, 81, '4567890', '8e7b805b563975a276e133302eca5205b6e32320fa6bc5a588b90183b95dc028-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/f605f837d7dac58aad33dc3af98f5a35', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2481, 266, '4567890', '611058b8ca5c96bf67b019961346ef196b5754d737784b51ec5075c0dc437bf0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 4, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2381, 264, '4567890', '03ec2b6213936f48f9f7372b95811fa57b66b4fd401a271a1c7224027ce111bc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '2008-08-11', 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2501, 266, '4567890', 'd51487b435be314159e60e23ace11eb73f998e713a6e02287c374fd26835d87e-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 5, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2581, 287, '4567890', '1833a2b1b440cf5ce04d7ee615c46959309a8e73b2cdb95fc1affa8845504e87-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2521, 282, '4567890', '927a4fe639f66b1988f8f5112ce380274a1590df1d5968d41b855005bb8b67bf-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'jpg', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3321, 161, 'G-1234567-8', 'b072911f7517dd93353ab854a3177e733cfbdf468f0a03187f6cee51a25e5e9a-2', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 1, 2, 1, '2015-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2561, 286, '4567890', 'e0022deb6b3d9c2d53345d51361162d159eb5dd36914c29b299c84b7ddaf08ec-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, '2012-02-15', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2261, 222, '4567890', '51dca8005775aa6d89befed929c203e5bbaa9d6a1445eac881750bea6053f564-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (1381, 264, '17718114', 'b9f7659765fe892bb14f3024ebddcca5d82118376df344b0010f53909b961db3-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, '1998-11-07', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2281, 224, '4567890', 'cf1c8bf91a6ad9dd39478ddfcd3721b60f46c64b74a6c964c674853a89796c8e-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, '2014-10-20', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2341, 243, '4567890', '478ad03cf0312a4d3369d8021de357f4c8dfece5cc68249206e4690fc4737a8d-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2401, 266, '4567890', 'd78ab2eb6d6a440131b2b4f50fa0645f959ef477884b6ac2241b19ee4fb776ae-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/3106caa99d22d194081614f9a3f32e5c', 'tif', 1, 0, 1, NULL, 2, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2541, 284, '4567890', '717bae51b1fd658caf09c8cc66eb4000e92d74ccacdc984afa0c517ad0d557a6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3561, 205, 'G-1234567-8', '878d781b05dff873e5d23694c564e5a2414a7497190eaef6b5d75d5a6a11afcf-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 3, 0, 2, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (941, 101, '17718114', 'e42de1fa95baac471eecbf8f1f31213892262bf907af914b95a0fc33c3f33bf6-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2621, 224, '17718114', '81c816e0d7a4211b9ac37a6fbccc08f4cc70903297fe1e25537ae50aea186306-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'pdf', 1, 0, 5, '2016-06-01', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2661, 242, '17718114', 'f0c1b362c6f7d85a80980dde61a83a4f2417dc8af8570f274e6775d4d60496da-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2681, 242, '17718114', '9b8984eefdbb4804bc0336262ff4e06edcda54e29207294d8e7e1b537338b1c0-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'jpg', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2641, 241, '17718114', '39bcf6e4c11c908fd882a4053944d583f21e255f89e0f258accbe4e984eef486-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'jpg', 1, 0, 1, '2014-11-11', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2701, 244, '17718114', '4919a4f143e7f22fc5fe2ab433020a0bdf0e09d6f25a15a815563df9827f48fa-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2721, 286, '17718114', 'ab042d83580b0e3e42a8756ad535d9a3362387494f6be6613daea643a7706a42-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'pdf', 1, 0, 5, '2022-08-14', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (2601, 221, '17718114', '5d2b7dc2dc014d82f67171986970465ee02e27e04fdff967ba7d76fb24b8069d-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3041, 101, 'G-1234567-8', '09ecb0e7a4c9b3db9a6bb80a425ee1669922f5774d88b203c4ec0fdd26616899-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3081, 141, 'G-1234567-8', '5742286f3c6907872e67f5c203d4e5b1ce60ff163cab97029cc727e20e0fc3a7-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 5, '2013-05-12', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3101, 161, 'G-1234567-8', 'ea26359f89d921345bc68fec7606720a62f91ce9b820a491ee78c108636db089-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, '2015-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3121, 181, 'G-1234567-8', 'f6b121340c85b55db6c672e33c283aae097e26b2ffb0339458b55da17425a71f-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'jpg', 1, 0, 1, '2016-12-31', 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3021, 222, '16415223', '3e26498adf21c45b43d3ae923a5fda639cb9ed4f35cf7c2f295cc2bc6f732a84-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/4eba2a4de43844c3b23a8c2cb5d648c3', 'tif', 1, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3241, 2, '01', 'ebded8a31c5c3b36bad010a310bd57b7a64bd53139f71961c5c31a9af03d775e-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/d674548a51a2767017944f01c18d3f39', 'tif', 1, 0, 1, NULL, 0, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3341, 284, '4567890', '5304fd501ca14fb9d180333c34fe0072acf06cdd9c744f969958e1f085554c8b-1', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 1, 1, 1, NULL, 0, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3141, 201, 'G-1234567-8', 'f6baea765e649a9b9a4fa0166f02a5ea92a08bf918f44ccd961477be30023da1-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'tif', 1, 0, 3, '2010-12-31', 2, '1');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3001, 281, '16415223', '749db0019bb38996c1e4c16f01139927908c9a564a86426585b29f5335b619bc-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/14a72dc7818fb0ac280c4228985ae046', 'tif', 2, 0, 1, NULL, 1, '0');
INSERT INTO infodocumento (id_infodocumento, id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado) VALUES (3521, 205, 'G-1234567-8', '009692bbb41115390632242c3db6b9af98afdcc1ebd85daa0f1749c5e56b9d8c-0', 'f7398c04288a51524315308dd0633288/f0c95ff049ee3e4bee3193805831eecd/8a02b77b46ed30e3cb92475af885c74a', 'pdf', 1, 0, 1, NULL, 1, '0');


--
-- TOC entry 3088 (class 0 OID 35074)
-- Dependencies: 197
-- Data for Name: libreria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO libreria (id_libreria, libreria, id_estatus) VALUES (21, '01.-Gerencia de Regímenes Aduaneros', 1);
INSERT INTO libreria (id_libreria, libreria, id_estatus) VALUES (41, '02.-  REGÍMENES ADUANEROS – DEPARTAMENTO DE DRAW BACK', 1);
INSERT INTO libreria (id_libreria, libreria, id_estatus) VALUES (61, 'LIBRERÍA:03.- PRUEBA - REGÍMENES ADUANEROS – DIVISIÓN AUXILIARES ADUANEROS', 1);
INSERT INTO libreria (id_libreria, libreria, id_estatus) VALUES (81, '03.- PRUEBA - EXPEDIENTES DE AGENTES DE ADUANAS CAPACITADOS ADUANEROS  (AA-CA)', 1);
INSERT INTO libreria (id_libreria, libreria, id_estatus) VALUES (1, 'LibreriaPrueba', 1);


--
-- TOC entry 3089 (class 0 OID 35078)
-- Dependencies: 198
-- Data for Name: lista_desplegables; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (1, 4, 'Valor1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (2, 4, 'Valor2');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (3, 4, 'Valor3');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (4, 4, 'Valor4');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (5, 4, 'Valor5');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (21, 4, 'Dato1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (22, 4, 'Dato2');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (23, 4, 'Dato3');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (24, 4, 'Dato4');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (25, 4, 'Dato5');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (41, 27, 'A1-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (42, 27, 'A2-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (43, 27, 'A3-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (44, 27, 'A4-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (45, 27, 'A5-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (46, 27, 'A6-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (47, 27, 'A7-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (48, 27, 'A8-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (49, 27, 'A9-1-1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (61, 28, 'Recibido');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (62, 28, 'Extemporáneo');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (63, 28, 'Por Análisis');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (64, 28, 'Por Recaudos');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (65, 28, 'Perención');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (66, 28, 'Improcendente');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (67, 28, 'Liquidado');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (81, 43, 'Firma Personal');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (82, 43, 'Persona Jurídica');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (101, 49, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (102, 49, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (103, 49, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (104, 49, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (105, 49, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (106, 49, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (107, 49, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (108, 49, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (121, 50, 'Activo');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (122, 50, 'Inactivo');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (123, 50, 'Decaído');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (124, 50, 'Revocado');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (125, 50, 'Suspendido');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (126, 50, 'Cesado');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (141, 382, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (142, 382, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (143, 382, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (144, 382, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (145, 382, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (146, 382, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (161, 403, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (162, 403, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (163, 403, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (164, 403, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (165, 403, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (166, 403, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (181, 423, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (182, 423, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (183, 423, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (184, 423, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (185, 423, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (186, 423, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (201, 442, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (202, 442, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (203, 442, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (204, 442, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (205, 442, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (206, 442, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (221, 461, '1');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (222, 461, '2');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (223, 461, '3');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (224, 461, '4');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (225, 461, '5');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (241, 462, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (242, 462, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (243, 462, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (244, 462, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (245, 462, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (246, 462, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (261, 482, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (262, 482, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (263, 482, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (264, 482, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (265, 482, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (266, 482, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (281, 483, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (282, 483, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (283, 483, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (284, 483, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (285, 483, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (286, 483, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (287, 483, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (288, 483, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (301, 523, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (302, 523, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (303, 523, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (304, 523, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (305, 523, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (306, 523, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (307, 523, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (308, 523, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (321, 542, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (322, 542, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (323, 542, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (324, 542, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (325, 542, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (326, 542, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (341, 563, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (342, 563, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (343, 563, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (344, 563, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (345, 563, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (346, 563, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (361, 583, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (362, 583, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (363, 583, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (364, 583, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (365, 583, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (366, 583, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (381, 602, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (382, 602, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (383, 602, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (384, 602, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (385, 602, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (386, 602, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (401, 622, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (402, 622, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (403, 622, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (404, 622, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (405, 622, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (406, 622, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (421, 663, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (422, 663, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (423, 663, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (424, 663, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (425, 663, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (426, 663, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (427, 663, 'BOCA DE GRITA ');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (428, 663, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (441, 682, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (442, 682, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (443, 682, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (444, 682, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (445, 682, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (446, 682, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (461, 702, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (462, 702, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (463, 702, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (464, 702, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (465, 702, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (466, 702, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (481, 722, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (482, 722, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (483, 722, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (484, 722, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (485, 722, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (486, 722, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (501, 763, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (502, 763, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (503, 763, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (504, 763, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (505, 763, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (506, 763, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (521, 783, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (522, 783, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (523, 783, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (524, 783, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (525, 783, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (526, 783, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (541, 803, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (542, 803, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (543, 803, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (544, 803, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (545, 803, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (546, 803, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (561, 823, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (562, 823, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (563, 823, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (564, 823, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (565, 823, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (566, 823, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (581, 843, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (582, 843, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (583, 843, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (584, 843, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (585, 843, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (586, 843, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (601, 863, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (602, 863, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (603, 863, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (604, 863, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (605, 863, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (606, 863, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (621, 901, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (622, 901, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (623, 901, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (624, 901, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (625, 901, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (626, 901, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (627, 901, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (628, 901, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (641, 921, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (642, 921, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (643, 921, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (644, 921, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (645, 921, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (646, 921, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (647, 921, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (648, 921, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (661, 941, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (662, 941, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (663, 941, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (664, 941, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (665, 941, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (666, 941, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (667, 941, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (668, 941, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (681, 961, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (682, 961, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (683, 961, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (684, 961, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (685, 961, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (686, 961, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (687, 961, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (688, 961, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (701, 981, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (702, 981, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (703, 981, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (704, 981, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (705, 981, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (706, 981, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (707, 981, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (708, 981, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (721, 1001, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (722, 1001, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (723, 1001, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (724, 1001, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (725, 1001, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (726, 1001, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (727, 1001, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (728, 1001, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (741, 68, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (742, 68, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (743, 68, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (744, 68, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (745, 68, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (746, 68, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (747, 68, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (748, 68, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (761, 69, 'Activo');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (762, 69, 'Inactivo');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (763, 69, 'Decaído');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (764, 69, 'Revocado');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (765, 69, 'Suspendido');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (766, 69, 'Cesado');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (781, 1022, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (782, 1022, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (783, 1022, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (784, 1022, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (785, 1022, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (786, 1022, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (801, 1042, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (802, 1042, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (803, 1042, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (804, 1042, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (805, 1042, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (806, 1042, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (821, 1062, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (822, 1062, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (823, 1062, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (824, 1062, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (825, 1062, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (826, 1062, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (841, 1082, '2015');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (842, 1082, '2014');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (843, 1082, '2013');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (844, 1082, '2012');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (845, 1082, '2011');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (846, 1082, '2010');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (861, 1083, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (862, 1083, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (863, 1083, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (864, 1083, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (865, 1083, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (866, 1083, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (867, 1083, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (868, 1083, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (881, 1123, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (882, 1123, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (883, 1123, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (884, 1123, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (885, 1123, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (886, 1123, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (887, 1123, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (888, 1123, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (901, 1143, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (902, 1143, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (903, 1143, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (904, 1143, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (905, 1143, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (906, 1143, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (907, 1143, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (908, 1143, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (921, 1163, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (922, 1163, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (923, 1163, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (924, 1163, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (925, 1163, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (926, 1163, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (927, 1163, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (928, 1163, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (941, 1183, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (942, 1183, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (943, 1183, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (944, 1183, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (945, 1183, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (946, 1183, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (947, 1183, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (948, 1183, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (961, 1203, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (962, 1203, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (963, 1203, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (964, 1203, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (965, 1203, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (966, 1203, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (967, 1203, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (968, 1203, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (981, 85, 'LA GUAIRA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (982, 85, 'SAN CRISTOBAL');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (983, 85, 'PTO. AYACUCHO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (984, 85, 'PTO. CABELLO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (985, 85, 'EL GUAMACHE');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (986, 85, 'GUANTA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (987, 85, 'BOCA DE GRITA');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (988, 85, 'PTO. PALMARITO');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (1001, 86, 'Activo');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (1002, 86, 'Inactivo');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (1003, 86, 'Decaído');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (1004, 86, 'Revocado');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (1005, 86, 'Suspendido');
INSERT INTO lista_desplegables (id_lista, codigo_indice, descripcion) VALUES (1006, 86, 'Cesado');


--
-- TOC entry 3090 (class 0 OID 35082)
-- Dependencies: 199
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (1, NULL, NULL, 'dw4jconf', 7);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (2, NULL, NULL, 'dw4jconf', 8);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (43, 1, 1, 'eescalona', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (44, 1, 1, 'eescalona', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (45, 1, 1, 'eescalona', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (46, 1, 1, 'eescalona', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (47, 1, 1, 'eescalona', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (48, 1, 1, 'eescalona', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (49, NULL, NULL, 'eescalona', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (50, 41, 41, 'eescalona', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (51, 41, 41, 'eescalona', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (52, 41, 41, 'eescalona', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (53, 41, 41, 'eescalona', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (54, 41, 41, 'eescalona', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (55, 41, 41, 'eescalona', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (63, NULL, NULL, 'tgarrido', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (64, 41, 41, 'tgarrido', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (65, 41, 41, 'tgarrido', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (66, 41, 41, 'tgarrido', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (67, 41, 41, 'tgarrido', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (68, 41, 41, 'tgarrido', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (69, 41, 41, 'tgarrido', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (83, 41, 41, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (84, 41, 41, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (85, 41, 41, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (86, 41, 41, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (87, 41, 41, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (88, 41, 41, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (89, NULL, NULL, 'nilda_blanco', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (90, 61, 61, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (91, 61, 81, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (92, 61, 121, 'nilda_blanco', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (93, 61, 61, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (94, 61, 81, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (95, 61, 121, 'nilda_blanco', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (96, 61, 61, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (97, 61, 81, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (98, 61, 121, 'nilda_blanco', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (99, 61, 61, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (100, 61, 81, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (101, 61, 121, 'nilda_blanco', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (102, 61, 61, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (103, 61, 81, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (104, 61, 121, 'nilda_blanco', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (105, 61, 61, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (106, 61, 81, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (107, 61, 121, 'nilda_blanco', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (163, 61, 61, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (164, 61, 61, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (165, 61, 61, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (166, 61, 61, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (167, 61, 61, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (168, 61, 61, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (169, 61, 81, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (170, 61, 81, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (171, 61, 81, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (172, 61, 81, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (173, 61, 81, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (174, 61, 81, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (175, 61, 121, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (176, 61, 121, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (177, 61, 121, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (178, 61, 121, 'javier_plaza', 9);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (179, 61, 121, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (180, 61, 121, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (181, NULL, NULL, 'javier_plaza', 1);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (182, 1, 1, 'javier_plaza', 4);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (183, 1, 1, 'javier_plaza', 5);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (184, 1, 1, 'javier_plaza', 3);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (185, 1, 1, 'javier_plaza', 2);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (186, 1, 1, 'javier_plaza', 6);
INSERT INTO perfil (id_perfil, id_libreria, id_categoria, id_usuario, id_rol) VALUES (187, 1, 1, 'javier_plaza', 9);


--
-- TOC entry 3096 (class 0 OID 35350)
-- Dependencies: 205
-- Data for Name: reporte; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO reporte (id_categoria, expediente, "número_rif", "nombre_o_razón_social", tipo_persona, "número_registro") VALUES (61, '16415223', '16415223', 'DAVID MEDINA', 'Firma Personal', 16415223);
INSERT INTO reporte (id_categoria, expediente, "número_rif", "nombre_o_razón_social", tipo_persona, "número_registro") VALUES (61, '17718114', '17718114', 'ALI SUAREZ', 'Firma Personal', 17718114);
INSERT INTO reporte (id_categoria, expediente, "número_rif", "nombre_o_razón_social", tipo_persona, "número_registro") VALUES (61, '4567890', '4567890', 'PILAR GARCIA', 'Persona Jurídica', 4567890);
INSERT INTO reporte (id_categoria, expediente, "número_rif", "nombre_o_razón_social", tipo_persona, "número_registro") VALUES (61, 'G-1234567-8', 'G-1234567-8', 'Corporacion Venezolana de Guayana CVG', 'Persona Jurídica', 123456);


--
-- TOC entry 3091 (class 0 OID 35086)
-- Dependencies: 200
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rol (id_rol, rol) VALUES (1, 'ADMINISTRADOR');
INSERT INTO rol (id_rol, rol) VALUES (2, 'APROBADOR');
INSERT INTO rol (id_rol, rol) VALUES (3, 'DIGITALIZADOR');
INSERT INTO rol (id_rol, rol) VALUES (4, 'CONSULTAR');
INSERT INTO rol (id_rol, rol) VALUES (5, 'IMPRIMIR');
INSERT INTO rol (id_rol, rol) VALUES (6, 'REPORTES');
INSERT INTO rol (id_rol, rol) VALUES (7, 'CONFIGURADOR');
INSERT INTO rol (id_rol, rol) VALUES (8, 'MANTENIMIENTO');
INSERT INTO rol (id_rol, rol) VALUES (9, 'ELIMINAR');


--
-- TOC entry 3105 (class 0 OID 0)
-- Dependencies: 170
-- Name: sq_categroria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_categroria', 160, true);


--
-- TOC entry 3106 (class 0 OID 0)
-- Dependencies: 179
-- Name: sq_combo; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_combo', 1020, true);


--
-- TOC entry 3107 (class 0 OID 0)
-- Dependencies: 171
-- Name: sq_dato_adicional; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_dato_adicional', 1220, true);


--
-- TOC entry 3108 (class 0 OID 0)
-- Dependencies: 172
-- Name: sq_datos_infodocumento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_datos_infodocumento', 4320, true);


--
-- TOC entry 3109 (class 0 OID 0)
-- Dependencies: 173
-- Name: sq_documento_eliminado; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_documento_eliminado', 740, true);


--
-- TOC entry 3110 (class 0 OID 0)
-- Dependencies: 174
-- Name: sq_expediente; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_expediente', 220, true);


--
-- TOC entry 3111 (class 0 OID 0)
-- Dependencies: 175
-- Name: sq_foliatura; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_foliatura', 300, true);


--
-- TOC entry 3112 (class 0 OID 0)
-- Dependencies: 176
-- Name: sq_indices; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_indices', 100, true);


--
-- TOC entry 3113 (class 0 OID 0)
-- Dependencies: 177
-- Name: sq_infodocumento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_infodocumento', 3580, true);


--
-- TOC entry 3114 (class 0 OID 0)
-- Dependencies: 178
-- Name: sq_libreria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_libreria', 100, true);


--
-- TOC entry 3115 (class 0 OID 0)
-- Dependencies: 180
-- Name: sq_perfil; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_perfil', 202, true);


--
-- TOC entry 3116 (class 0 OID 0)
-- Dependencies: 181
-- Name: sq_subcategroria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_subcategroria', 220, true);


--
-- TOC entry 3117 (class 0 OID 0)
-- Dependencies: 182
-- Name: sq_tipo_documento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_tipo_documento', 460, true);


--
-- TOC entry 3118 (class 0 OID 0)
-- Dependencies: 183
-- Name: sq_valor_dato_adicional; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_valor_dato_adicional', 3040, true);


--
-- TOC entry 3092 (class 0 OID 35089)
-- Dependencies: 201
-- Data for Name: subcategoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (21, 21, '01-Resumen de Conformidad', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (41, 41, '00.- RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (42, 41, '01.- DOCUMENTOS  DE DRAW  BACK', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (61, 61, 'SUB CATEGORÍA:00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (81, 61, 'SUB CATEGORÍA:01.-  PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (101, 61, 'SUB CATEGORÍA:02.-  PRUEBA - ACTUALIZACIÓN', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (121, 61, 'SUB CATEGORÍA:03.-  PRUEBA - NOTIFICACIÓN CESE DEFINITIVO', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (141, 61, 'SUB CATEGORÍA:10.-  PRUEBA - CLAVES SISTEMAS AUTOMATIZADOS', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (161, 81, 'SUB CATEGORÍA:00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (162, 81, 'SUB CATEGORÍA:01.-  PRUEBA - REGISTRO', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (163, 81, 'SUB CATEGORÍA:02.-  PRUEBA - INCORPORACIÓN', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (164, 81, 'SUB CATEGORÍA:03.-  PRUEBA - TRANSFERENCIA', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (165, 81, 'SUB CATEGORÍA:04.-  PRUEBA - DESINCORPORACIÓN', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (181, 121, '00.-  PRUEBA - RESUMEN DEL EXPEDIENTE', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (182, 121, '01.-  PRUEBA - INCLUSIÓN', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (183, 121, '02.-  PRUEBA - EXCLUSIÓN', 1);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (201, 1, 'SubCategoriaPrueba2', 2);
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (1, 1, 'SubCategoriaPrueba', 1);


--
-- TOC entry 3093 (class 0 OID 35093)
-- Dependencies: 202
-- Data for Name: tipodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (21, 21, 21, '1.01.-Resumen de Conformidad', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (41, 41, 41, '0.01.- Relación de Documentos', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (61, 41, 42, '1.01.- Constancia de Recepción de Documentos', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (62, 41, 42, '1.02.- Solicitud de Draw Back', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (63, 41, 42, '1.03.- Declaración de Exportación  (DUA – Forma D)', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (64, 41, 42, '1.04.- Factura Comercial', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (65, 41, 42, '1.05.- Documento de Transporte  BL', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (66, 41, 42, '1.06.- Documento de Transporte  CPIC/MCI', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (67, 41, 42, '1.07.- Documento de Transporte  GA', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (68, 41, 42, '1.08.- Solicitud de Recaudos Ingreso', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (69, 41, 42, '1.09.- Certificación de Venta de Divisas', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (70, 41, 42, '1.10.- Desglose de Venta de Divisas (Valor FOB)', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (71, 41, 42, '1.11.- Carta Bajo Fe de Juramento Modalidad', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (72, 41, 42, '1.12.- Relación Insumo Producto (RIP)', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (73, 41, 42, '1.13.- Descripción Literal y Grafica del Proceso Productivo (DLG)', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (74, 41, 42, '1.14.- Relación de Insumos Importados', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (75, 41, 42, '1.15.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (76, 41, 42, '1.16.- Corrección de Datos Exportación', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (77, 41, 42, '1.17.- Formulario “Forma 16”', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (78, 41, 42, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (81, 61, 61, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (101, 61, 81, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (121, 61, 81, '1.02.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (141, 61, 81, '1.03.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (161, 61, 81, '1.04.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (181, 61, 81, '1.16.- Registro de Información Fiscal (RIF) - Socios', 1, '1', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (201, 61, 81, '1.17.- Registro de Información Fiscal (RIF) - Administradores', 1, '1', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (202, 61, 81, '1.21.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (203, 61, 81, '1.22.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (204, 61, 81, '1.23.- Providencia Administrativa', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (205, 61, 81, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (221, 61, 101, '2.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (222, 61, 101, '2.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (223, 61, 101, '2.03.- Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (224, 61, 101, '2.04.- Modificaciones del Documento Constitutivo', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (241, 61, 101, '2.05.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (242, 61, 101, '2.13.- Solicitud de Recaudos', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (243, 61, 101, '2.14.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (244, 61, 101, '2.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (261, 61, 121, '3.01.- Oficio', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (262, 61, 121, '3.02.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (263, 61, 121, '3.03.- Modificaciones del Documento Constitutivo', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (264, 61, 121, '3.04.- Solicitud de Recaudos', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (265, 61, 121, '3.05.- Alcances', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (266, 61, 121, '3.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (281, 61, 141, '10.01.- Acceso', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (282, 61, 141, '10.02.- Reseteo', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (283, 61, 141, '10.03.- Usuarios Adicionales', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (284, 61, 141, '10.04.- Bloqueo Temporal', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (285, 61, 141, '10.05.- Bloqueo Definitivo', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (286, 61, 141, '10.06.- I-Aduana ó I-Seniat', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (287, 61, 141, '10.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (301, 81, 161, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (321, 81, 162, '1.01.- Solicitud', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (322, 81, 162, '1.02.- Cédula de Identidad', 1, '1', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (323, 81, 162, '1.07.- Impuesto Sobre La Renta (ISLR)', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (324, 81, 162, '1.08.- Registro de Información Fiscal (RIF)', 1, '1', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (325, 81, 162, '1.09.- Solicitud de Recaudos', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (326, 81, 162, '1.10.- Alcances', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (327, 81, 162, '1.11.- Providencia Administrativa', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (328, 81, 162, '1.12.- Timbres Fiscales', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (329, 81, 162, '1.99.- Otros', 1, '0', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (341, 81, 163, '2.01.- Incorporación', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (361, 81, 164, '3.01.- Transferencia', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (381, 81, 165, '4.01.- Desincorporación', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (401, 121, 181, '0.01.- Índice de Foliatura', 1, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (421, 121, 182, '1.01.- Inclusión', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (441, 121, 183, '2.01.- Exclusión', 1, '1', '1', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (1, 1, 1, 'TipoDocumentoDatosCompleta', 2, '0', '0', NULL);
INSERT INTO tipodocumento (id_documento, id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional, ficha) VALUES (2, 1, 1, 'TipodocumentoSimple', 2, '0', '0', NULL);


--
-- TOC entry 3094 (class 0 OID 35097)
-- Dependencies: 203
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario (id_usuario, nombre, apellido, cedula, sexo, id_estatus, password) VALUES ('dw4jconf', 'Usuario', 'Configurador', NULL, NULL, 1, 'RGV2ZWxjb21HRA==');
INSERT INTO usuario (id_usuario, nombre, apellido, cedula, sexo, id_estatus, password) VALUES ('eescalona', 'Escalona', 'Erick', '1040', NULL, 1, NULL);
INSERT INTO usuario (id_usuario, nombre, apellido, cedula, sexo, id_estatus, password) VALUES ('nilda_blanco', 'Blanco', 'Nilda', NULL, NULL, 1, NULL);
INSERT INTO usuario (id_usuario, nombre, apellido, cedula, sexo, id_estatus, password) VALUES ('tgarrido', 'garrido', 'thais', NULL, NULL, 1, NULL);
INSERT INTO usuario (id_usuario, nombre, apellido, cedula, sexo, id_estatus, password) VALUES ('javier_plaza', 'Plaza', 'Javier', NULL, NULL, 1, NULL);


--
-- TOC entry 3095 (class 0 OID 35100)
-- Dependencies: 204
-- Data for Name: valor_dato_adicional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1201, 581, '02/06/2010', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1202, 582, '10', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1203, 583, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1181, 521, '15/10/2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1182, 522, '2000', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1241, 601, '1919', 2, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1242, 602, '2013', 2, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (121, 1, 'documento jpg version 2', 1, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (122, 2, '1234132', 1, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (123, 3, '10/10/2014', 1, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (124, 4, 'Valor1', 1, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (125, 5, 'asdfgh', 1, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (141, 1, 'documento tif version 2', 2, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (142, 2, '332452', 2, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (143, 3, '17/10/2014', 2, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (144, 4, 'Dato4', 2, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (145, 5, 'asdfg', 2, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (161, 1, 'documento tif version 2', 3, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (162, 2, '131234', 3, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (163, 3, '10/10/2014', 3, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (164, 4, 'Dato2', 3, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (165, 5, 'asdfg', 3, 2, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (181, 21, '10/01/2013', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (201, 61, '12/11/1999', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (221, 81, '02/06/2010', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (241, 121, '22/03/2013', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (261, 161, '21/10/2013', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (281, 241, '12/11/1988', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (301, 301, '12/06/2011', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (321, 301, '10/03/2012', 2, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (341, 321, '02/06/2010', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (361, 361, 'DOC NO CLASIFICADOS', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (381, 41, '12/08/1998', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (401, 101, '08/08/1998', 1, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (421, 41, '02/07/1999', 2, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (441, 41, '13/05/2013', 3, 0, '18331750');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (481, 41, '12/09/2009', 1, 0, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (501, 81, '15/08/2008', 1, 0, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (521, 121, '22/02/2012', 1, 0, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1221, 601, '12', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (561, 161, '22/02/2002', 1, 0, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1222, 602, '2013', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (601, 61, '01/01/2001', 2, 0, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (621, 61, '11/10/2009', 3, 0, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2861, 841, '14/10/2014', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1261, 641, 'REPRUEBA ', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1021, 501, 'PRUEBA 4', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (701, 341, '13/12/2011', 1, 0, '18331751');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1041, 501, 'PRUEBA 5', 2, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (741, 361, 'DOC NO CLASIFICADOS', 1, 0, '18331751');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (761, 101, '21/12/2012', 1, 0, '18331751');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (781, 41, '10/02/2002', 1, 1, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (801, 121, '12/11/2011', 1, 1, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (821, 61, '12/11/1995', 2, 1, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (461, 21, '11/02/2003', 1, 0, '18110507');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1061, 501, 'PRUEBA 6', 3, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1081, 501, 'PRUEBA 7', 4, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2862, 842, '14', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2863, 843, '2014', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2864, 981, 'GUANTA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (861, 41, '17/08/2014', 1, 0, '17714118');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (881, 81, '15/07/2007', 1, 0, '17714118');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (901, 381, '17', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (902, 382, '2010', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (921, 401, '15/10/2013', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (922, 402, '1', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (923, 403, '2011', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (941, 441, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (942, 442, '2013', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2981, 501, 'PDF para redigitalizar', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1281, 681, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1282, 682, '2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1301, 701, '22', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1302, 702, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1321, 721, '18', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1322, 722, '2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1341, 741, 'PRUEBA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1381, 741, 'PRUEBA 1', 2, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1401, 741, 'PRUEBA 2', 3, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1421, 741, 'PRUEBA 3', 4, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1441, 761, '15/10/2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1442, 762, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1443, 763, '2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1444, 901, 'SAN CRISTOBAL', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1461, 821, '12/11/1998', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1462, 822, '12', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1463, 823, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1464, 961, 'LA GUAIRA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1481, 841, '04/10/2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1482, 842, '15', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (21, 1, 'documento pdf', 2, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2, 2, '123', 1, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (22, 2, '2142', 2, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (23, 3, '25/10/2014', 2, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (25, 5, 'area', 2, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (61, 1, 'documento pdf version 1', 1, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (62, 2, '2324214', 1, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (63, 3, '25/10/2014', 1, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (65, 5, 'qwerty', 1, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (24, 4, 'Dato5', 2, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (41, 1, 'documento jpg', 3, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (42, 2, '4535432', 3, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (43, 3, '24/12/2014', 3, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (45, 5, 'area', 3, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (81, 1, 'documento jpg version 1', 2, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (82, 2, '1232143', 2, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (83, 3, '18/10/2014', 2, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (84, 4, 'Dato4', 2, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (44, 4, 'Dato5', 3, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (101, 1, 'documento pdf version 1', 3, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (102, 2, '4242', 3, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (103, 3, '25/10/2014', 3, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (104, 4, 'Dato3', 3, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1483, 843, '2015', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1484, 981, 'PTO. PALMARITO', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1521, 881, 'PRUEBA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1541, 881, 'PRUEBA 1', 2, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1183, 523, 'GUANTA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1501, 861, '03/10/2014', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1502, 862, '1122', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1503, 863, '2012', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1504, 1001, 'BOCA DE GRITA', 1, 0, '17718114');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1581, 401, '15/11/2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1582, 402, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1583, 403, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2221, 701, '1413', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2222, 702, '2013', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1621, 481, '04/10/2014', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1622, 482, '2014', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1623, 483, 'PTO. CABELLO', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2241, 741, 'PRUEBA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2261, 741, 'PRUEBA 1', 2, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2321, 741, 'PRUEBA 3', 4, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2341, 741, 'PRUEBA 4', 5, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2361, 781, '12/12/2012', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1661, 521, '15/10/2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1662, 522, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1663, 523, 'EL GUAMACHE', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1681, 561, '15/11/2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1682, 562, '15', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1683, 563, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1701, 601, '225', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1702, 602, '2013', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2362, 782, '12121', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1741, 381, '1919', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1742, 382, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2363, 783, '2012', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2364, 921, 'SAN CRISTOBAL', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2381, 821, '12/10/2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2382, 822, '21010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2383, 823, '2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1781, 421, '12/10/2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1782, 422, '1213', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1783, 423, '2013', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1801, 441, '4518', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1802, 442, '2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2384, 961, 'GUANTA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1841, 661, '15/10/2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1842, 662, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1843, 663, 'PTO. AYACUCHO', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2401, 861, '15/10/2008', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2402, 862, '1510', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2403, 863, '2011', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2404, 1001, 'PTO. PALMARITO', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2421, 881, 'PRUEBA', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2881, 821, '02/10/2014', 1, 1, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2882, 822, '1545', 1, 1, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2883, 823, '2014', 1, 1, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2884, 961, 'BOCA DE GRITA', 1, 1, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (3001, 501, 'JPG para redigitalizar', 2, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2161, 601, '1821', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2162, 602, '2011', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2281, 741, 'PRUEBA 2', 3, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1, 1, 'documento tiff', 1, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (3, 3, '13/10/2014', 1, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (4, 4, 'Dato1', 1, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (5, 5, 'area', 1, 0, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (64, 4, 'Valor1', 1, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (85, 5, 'qwerty', 2, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (105, 5, 'qwerty', 3, 1, '01');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1881, 761, '12/12/2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1882, 762, '201212', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1941, 881, 'PRUEBA 1', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2701, 761, '21/10/2010', 2, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2101, 541, '1312', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2102, 542, '2011', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2121, 581, '21/10/2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2122, 582, '2121', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1901, 801, '15/09/2009', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1902, 802, '2009', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1903, 803, '2011', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1904, 941, 'GUANTA', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1921, 841, '12/11/2007', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1922, 842, '121188', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1923, 843, '2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1924, 981, 'PTO. AYACUCHO', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2123, 583, '2010', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2181, 621, '121188', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2182, 622, '2015', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2201, 681, '0426', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2202, 682, '2011', 1, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2721, 541, '12', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2722, 542, '2015', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1861, 721, '0216', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1862, 722, '2014', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2901, 461, '3', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2902, 462, '2013', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (3021, 501, 'TIFF para redigitalizar', 3, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1883, 763, '2012', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (1884, 901, 'EL GUAMACHE', 1, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2702, 762, '5', 2, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2703, 763, '2015', 2, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2704, 901, 'PTO. PALMARITO', 2, 0, '16415223');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2741, 381, '123456', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2742, 382, '2011', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2781, 421, '05/05/2011', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2782, 422, '156348', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2783, 423, '2011', 1, 0, 'G-1234567-8');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2821, 601, '1235', 2, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2822, 602, '2014', 2, 0, '4567890');
INSERT INTO valor_dato_adicional (id_valor, id_dato_adicional, valor, numero, version, expediente) VALUES (2841, 641, 'PRUEBA', 1, 0, '4567890');


--
-- TOC entry 2896 (class 2606 OID 35108)
-- Name: categoria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pk PRIMARY KEY (id_categoria);


--
-- TOC entry 2898 (class 2606 OID 35110)
-- Name: causa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY causa
    ADD CONSTRAINT causa_pk PRIMARY KEY (id_causa);


--
-- TOC entry 2906 (class 2606 OID 35112)
-- Name: documento_eliminado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documento_eliminado
    ADD CONSTRAINT documento_eliminado_pk PRIMARY KEY (id_doc_eliminado);


--
-- TOC entry 2910 (class 2606 OID 35114)
-- Name: estatus_documento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estatus_documento
    ADD CONSTRAINT estatus_documento_pk PRIMARY KEY (id_estatus_documento);


--
-- TOC entry 2908 (class 2606 OID 35116)
-- Name: estatus_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estatus
    ADD CONSTRAINT estatus_pk PRIMARY KEY (id_estatus);


--
-- TOC entry 2916 (class 2606 OID 35118)
-- Name: foliatura_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY foliatura
    ADD CONSTRAINT foliatura_pk PRIMARY KEY (id_foliatura);


--
-- TOC entry 2922 (class 2606 OID 35120)
-- Name: libreria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria
    ADD CONSTRAINT libreria_pk PRIMARY KEY (id_libreria);


--
-- TOC entry 2926 (class 2606 OID 35122)
-- Name: perfil_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pk PRIMARY KEY (id_perfil);


--
-- TOC entry 2924 (class 2606 OID 35124)
-- Name: pk_combo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lista_desplegables
    ADD CONSTRAINT pk_combo PRIMARY KEY (id_lista);


--
-- TOC entry 2900 (class 2606 OID 35126)
-- Name: pk_configuracion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY configuracion
    ADD CONSTRAINT pk_configuracion PRIMARY KEY (id_configuracion);


--
-- TOC entry 2902 (class 2606 OID 35128)
-- Name: pk_dato_adicional; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT pk_dato_adicional PRIMARY KEY (id_dato_adicional);


--
-- TOC entry 2904 (class 2606 OID 35130)
-- Name: pk_datos_infodocumento; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT pk_datos_infodocumento PRIMARY KEY (id_datos);


--
-- TOC entry 2912 (class 2606 OID 35132)
-- Name: pk_expedientes; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT pk_expedientes PRIMARY KEY (id_expedientes);


--
-- TOC entry 2914 (class 2606 OID 35134)
-- Name: pk_fabrica; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fabrica
    ADD CONSTRAINT pk_fabrica PRIMARY KEY (usuario);


--
-- TOC entry 2918 (class 2606 OID 35136)
-- Name: pk_indice; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT pk_indice PRIMARY KEY (id_indice);


--
-- TOC entry 2920 (class 2606 OID 35138)
-- Name: pk_infordocumento; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT pk_infordocumento PRIMARY KEY (id_infodocumento);


--
-- TOC entry 2936 (class 2606 OID 35140)
-- Name: pk_valor_dato_adicional; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT pk_valor_dato_adicional PRIMARY KEY (id_valor);


--
-- TOC entry 2928 (class 2606 OID 35142)
-- Name: rol_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pk PRIMARY KEY (id_rol);


--
-- TOC entry 2930 (class 2606 OID 35144)
-- Name: subcategoria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subcategoria
    ADD CONSTRAINT subcategoria_pk PRIMARY KEY (id_subcategoria);


--
-- TOC entry 2932 (class 2606 OID 35146)
-- Name: tipodocumento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT tipodocumento_pk PRIMARY KEY (id_documento);


--
-- TOC entry 2934 (class 2606 OID 35148)
-- Name: usuario_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id_usuario);


--
-- TOC entry 2937 (class 2606 OID 35149)
-- Name: fk_dato_adicional_tipodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT fk_dato_adicional_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


--
-- TOC entry 2938 (class 2606 OID 35154)
-- Name: fk_datoc_infodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datoc_infodoc FOREIGN KEY (id_infodocumento) REFERENCES infodocumento(id_infodocumento);


--
-- TOC entry 2939 (class 2606 OID 35159)
-- Name: fk_datos_doc_eliminado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datos_doc_eliminado FOREIGN KEY (id_doc_eliminado) REFERENCES documento_eliminado(id_doc_eliminado);


--
-- TOC entry 2940 (class 2606 OID 35164)
-- Name: fk_expedientes_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2941 (class 2606 OID 35169)
-- Name: fk_expedientes_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


--
-- TOC entry 2942 (class 2606 OID 35174)
-- Name: fk_fabrica_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fabrica
    ADD CONSTRAINT fk_fabrica_usuario FOREIGN KEY (usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2943 (class 2606 OID 35179)
-- Name: fk_indice_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT fk_indice_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2944 (class 2606 OID 35184)
-- Name: fk_infodoc_statusdoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_statusdoc FOREIGN KEY (estatus_documento) REFERENCES estatus_documento(id_estatus_documento);


--
-- TOC entry 2945 (class 2606 OID 35189)
-- Name: fk_infodoc_tipodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


--
-- TOC entry 2946 (class 2606 OID 35194)
-- Name: fk_perfil_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2947 (class 2606 OID 35199)
-- Name: fk_perfil_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


--
-- TOC entry 2948 (class 2606 OID 35204)
-- Name: fk_perfil_rol; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_rol FOREIGN KEY (id_rol) REFERENCES rol(id_rol);


--
-- TOC entry 2949 (class 2606 OID 35209)
-- Name: fk_perfil_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_usuario FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2950 (class 2606 OID 35214)
-- Name: fk_tipodoc_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2951 (class 2606 OID 35219)
-- Name: fk_tipodoc_subcategoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_subcategoria FOREIGN KEY (id_subcategoria) REFERENCES subcategoria(id_subcategoria);


--
-- TOC entry 2952 (class 2606 OID 35224)
-- Name: fk_usuario_estatus; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fk_usuario_estatus FOREIGN KEY (id_estatus) REFERENCES estatus(id_estatus);


--
-- TOC entry 2953 (class 2606 OID 35229)
-- Name: kf_valor_indice_da; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT kf_valor_indice_da FOREIGN KEY (id_dato_adicional) REFERENCES dato_adicional(id_dato_adicional);


--
-- TOC entry 3103 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-10-22 15:21:05

--
-- PostgreSQL database dump complete
--

