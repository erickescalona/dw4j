/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * VerDocumento.java
 *
 * Created on 02/05/2012, 11:14:38 AM
 */

package com.develcom.verdocumento;

import com.develcom.documento.Bufer;
import com.develcom.documento.InfoDocumento;
import com.develcom.dto.Expediente;
import com.develcom.dto.ManejoSesion;
import com.develcom.log.Traza;
import com.develcom.tools.ImagePanel;
import com.develcom.tools.Imagenes;
import com.develcom.tools.Impresor;
import com.develcom.tools.ToolsFile;
import com.develcom.tools.VerificaPerfil;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import ve.com.develcom.archivo.GestionArchivos;


/**
 *
 * @author develcom
 */
public class VerDocumento extends javax.swing.JFrame {

    /**
     * Lista de bufer de imagenes
     */
    private List<BufferedImage> buferImagenes;
    /**
     * Lista de informacion del
     * documento
     */
    private List<InfoDocumento> infoDocumentos;
    private double scalaImagen = 1;
    /**
     * escribe trazas en el log
     */
    private Traza traza = new Traza(VerDocumento.class);
    /**
     * objecto de informacion
     * del documento
     */
    private InfoDocumento infoDocumento;
    private Expediente expediente;
    private ToolsFile toolsTiff = new ToolsFile();



    public VerDocumento(InfoDocumento infoDocumento) {
        this.infoDocumento=infoDocumento;
        this.expediente=ManejoSesion.getExpediente();
        //setBackground(new Color(224,239,255));
        initComponents();
        setTitle("Expediente "+ManejoSesion.getExpediente().getIdExpediente());
        //setForeground(new Color(224,239,255));
        //panelVencimiento.setVisible(false);
        jdchVencimiento.setVisible(false);
        jdchVencimiento.setEnabled(false);

        fechaVencimiento.setVisible(false);

        jbImprimir.setEnabled(false);

        setLocationRelativeTo(null);
        //CentraVentanas.centrar(this);

        jlMensaje1.setText("Libreria: "+ManejoSesion.getExpediente().getLibreria());
        jlMensaje2.setText("Categoria: " +ManejoSesion.getExpediente().getCategoria());
        jlMensaje3.setText("SubCategoria: "+ManejoSesion.getExpediente().getSubCategoria());
//        jlMensaje1.setText(expediente.getTipoDocumento());
        jlMensaje1.setText(expediente.getLibreria());
        jlMensaje2.setText(expediente.getCategoria());
        jlMensaje3.setText(expediente.getSubCategoria());

        new VerificaPerfil(this).verificarPerfil();
        mostrarDocumentos();
        setVisible(true);
    }


    

    /**
     * Busca y muestra el documento
     * previamente digitalizado
     */
    private void mostrarDocumentos() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        int contDocAprob = 0;
        String ruta="", archivo="";
        //String buffer;
        BufferedReader leer;
        PrintWriter escribir;
        //ToolsFile toolsTiff = new ToolsFile();
        EjecutaEspera hilo = new EjecutaEspera();
        Bufer buffer;
        //byte[] buffer;
        OutputStream escribiendo;

        try {

            hilo.start();

            int idInfoDoc = infoDocumento.getIdInfoDocumento();
            int idDoc = infoDocumento.getIdDocumento();
            int version = infoDocumento.getVersion();
            int numDoc = infoDocumento.getNumeroDocumento();
            int idCat = ManejoSesion.getExpediente().getIdCategoria();
            int idSubCat = ManejoSesion.getExpediente().getIdSubCategoria();
            String idExpediente = ManejoSesion.getExpediente().getIdExpediente();
            traza.trace("buscando el documento idDocumento "+idDoc+
                    " idCategoria "+idCat+
                    " idSubCategoria "+idSubCat+
                    " idExpediente "+idExpediente+
                    " numero documento "+numDoc+
                    " idInfoDocumento "+idInfoDoc
                    , Level.INFO);
            
            infoDocumentos = new GestionArchivos().buscarImagenDocumentos(idInfoDoc, idDoc, idCat, idSubCat, version, numDoc, idExpediente);

            if(!infoDocumentos.isEmpty()){
                
                    File fileCod = new File(toolsTiff.getArchivoCodificado());
                    if(toolsTiff.getDirTemporal().exists()){
                        File[] files = toolsTiff.getDirTemporal().listFiles();
                        for(File f : files){
                            if(f.delete()){
                                traza.trace("eliminado archivo "+f.getName(), Level.INFO);
                            }else{
                                traza.trace("problemas al eliminar el archivo "+f.getName(), Level.WARN);
                            }
                        }
                    }

                    for(InfoDocumento id : infoDocumentos){

                        if(id.getEstatusDocumento().equalsIgnoreCase("aprobado")){

                            traza.trace("mostrar archivo en modulo consultar y documentos aprobados", Level.INFO);

                            if(id.getFechaVencimiento()!=null){

                                XMLGregorianCalendar xmlCalendar = id.getFechaVencimiento();
                                GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();

                                fechaVencimiento.setText("Fecha de Vencimiento "+sdf.format(rechaVencimiento.getTime()));
                                fechaVencimiento.setVisible(true);
                                //jdchVencimiento.setCalendar(rechaVencimiento);
                                //panelVencimiento.setVisible(true);
                                //jdchVencimiento.setVisible(true);
                                //jdchVencimiento.setEnabled(false);

                                ruta = id.getRutaArchivo();
                                archivo = id.getNombreArchivo();
                                version = id.getVersion();
                                break;

                            }else{
                                ruta = id.getRutaArchivo();
                                archivo = id.getNombreArchivo();
                                version = id.getVersion();
                                break;
                            }
                        }
                    }

                    jlMensaje4.setText("Tipo de Documento: "+infoDocumento.getTipoDocumento()+" \"ULTIMA VERSION: "+version+"\"");

                    traza.trace("buscando el archivo ", Level.INFO);
                    traza.trace("ruta a buscar "+ruta, Level.INFO);
                    traza.trace("archivo a buscar "+archivo, Level.INFO);
                    traza.trace("version del documento a mostrar "+version, Level.INFO);                    

                    buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);
                    
                    traza.trace("buffer "+buffer, Level.INFO);
                    escribiendo = new FileOutputStream(fileCod);
                    escribiendo.write(buffer.getBufer());
                    escribiendo.flush();
                    escribiendo.close();


//                    leer = new BufferedReader(new StringReader(buffer));
//                    escribir = new PrintWriter(new BufferedWriter(new FileWriter(fileCod)));
//
//                    while ((buffer = leer.readLine()) != null) {
//                        escribir.println(buffer);
//                    }
//                    escribir.close();

                    if(fileCod.exists()){
                        toolsTiff.decodificar();
                        //new ToolsFile().decodificar();
                        buferImagenes = toolsTiff.open(toolsTiff.getArchivoTif());
                        //imagenesTiff = new ToolsTiff().abrir(toolsTiff.getArchivoTif());
                        int size = buferImagenes.size();

                        for (int i = 0; i < size; i++) {
                            ImagePanel imageTab = new ImagePanel();
                            JScrollPane sp = new JScrollPane(imageTab);
                            sp.getVerticalScrollBar().setUnitIncrement(100);
                            sp.getHorizontalScrollBar().setUnitIncrement(100);
                            imageTab.setImage(buferImagenes.get(i));
                            imageTab.repaint();
                            panelImagen.addTab("Pag." + (i + 1), new ImageIcon("/ve/gob/mcti/gui/imagenes/toolbarButtonGraphics/general/Properties16"), sp);

                        }

                        for(InfoDocumento infoDoc : infoDocumentos){
                            if(infoDoc.getEstatusDocumento().equalsIgnoreCase("aprobado")){
                                cboVersion.addItem(infoDoc.getVersion());
                                contDocAprob++;
                            }
                        }

                        if(infoDocumentos.size()==1 || contDocAprob==1){
                            cboVersion.setEnabled(false);
                            btnAbrir.setEnabled(false);
                        }
                    }
            }else{

                throw new Exception("Documento o Archivo no encontrado");
            }

            hilo.interrupt();
            hilo.terminar();


        } catch (SOAPFaultException ex) {
            traza.trace("Error general al buscar el archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(new JFrame(), "Error general al buscar el archivo\n" + ex.getMessage(), "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
        } catch (SOAPException ex) {
            traza.trace("Error general al buscar el archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(new JFrame(), "Error general al buscar el archivo\n" + ex.getMessage(), "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {

            hilo.interrupt();
            hilo.terminar();

            traza.trace("Error al buscar el archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(new JFrame(), "Error al buscar el archivo\n" + ex.getMessage(), "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            //this.dispose();
            System.exit(1);

        }catch (Exception ex) {
            
            hilo.interrupt();
            hilo.terminar();

            traza.trace("Error general al buscar el archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(new JFrame(), "Error general al buscar el archivo\n" + ex.getMessage(), "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            //this.dispose();
            System.exit(1);
        }
    }

    /**
     * Muestra la version seleccionada
     * del documento
     * @param ruta
     * Ruta donde se aloja el archivo
     */
    private void verVersion(String ruta) {
        traza.trace("ruta del archivo a mostrar "+ruta, Level.INFO);
        buferImagenes = null;
        try {
            int i;
            panelImagen.removeAll();
            buferImagenes = new ToolsFile().open(ruta);
            //imagenesTiff = new ToolsTiff().abrir(ruta);
            for (i = 0; i < buferImagenes.size(); i++) {
                ImagePanel imageTab = new ImagePanel();
                JScrollPane sp = new JScrollPane(imageTab);
                sp.getVerticalScrollBar().setUnitIncrement(100);
                sp.getHorizontalScrollBar().setUnitIncrement(100);
                imageTab.setImage(buferImagenes.get(i));
                panelImagen.addTab("Pag." + (i + 1), new ImageIcon("/ve/gob/mcti/gui/imagenes/toolbarButtonGraphics/general/Properties16"), sp);
            }

        } catch (Exception ex) {
            traza.trace("problemas al carga la version del documento", Level.ERROR, ex);
            JOptionPane.showMessageDialog(this,"problemas al carga la version del documento\n"+ ex.getMessage(),  "Alerta...", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Busca la version seleccionada
     * del documento
     * @param version
     * La version seleccionada
     */
    private void getVersion(int version) {

        String ruta, archivo;
        //String buffer;
        Bufer buffer;
        //byte[] buffer;
        BufferedReader leer;
        PrintWriter escribir;
        OutputStream escribiendo = null;
        //ToolsFile toolsTiff = new ToolsFile();

        traza.trace("version a buscar "+version, Level.INFO);

        try{
            for(InfoDocumento infoDoc : infoDocumentos){
                if(infoDoc.getVersion()==version){

                    File fileCod = new File(toolsTiff.getArchivoCodificado());
                    if(toolsTiff.getDirTemporal().exists()){
                        File[] files = toolsTiff.getDirTemporal().listFiles();
                        for(File f : files){
                            if(f.delete()){
                                traza.trace("eliminado archivo "+f.getName(), Level.INFO);
                            }else{
                                traza.trace("problemas al eliminar el archivo "+f.getName(), Level.WARN);
                            }
                        }
                    }


                    if(infoDoc.getFechaVencimiento()!=null){

                        XMLGregorianCalendar xmlCalendar = infoDoc.getFechaVencimiento();
                        GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                        jdchVencimiento.setCalendar(rechaVencimiento);
                        //panelVencimiento.setVisible(true);
                        jdchVencimiento.setVisible(true);
                        jdchVencimiento.setEnabled(false);

                    }

                    ruta = infoDoc.getRutaArchivo();
                    archivo = infoDoc.getNombreArchivo();

                    buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);
                    
                    escribiendo = new FileOutputStream(fileCod);
                    escribiendo.write(buffer.getBufer());
                    escribiendo.flush();
                    escribiendo.close();


//                    leer = new BufferedReader(new StringReader(buffer));
//                    escribir = new PrintWriter(new BufferedWriter(new FileWriter(fileCod)));
//
//                    while ((buffer = leer.readLine()) != null) {
//                        escribir.println(buffer);
//                    }
//                    escribir.close();


                    if(fileCod.exists()){
                        new ToolsFile().decodificar();

                        verVersion(toolsTiff.getArchivoTif());
                    }

                }
            }
        } catch (SOAPFaultException ex) {
            traza.trace("Error general al buscar la version del archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(new JFrame(), "Error general al buscar el archivo\n" + ex.getMessage(), "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
        } catch (SOAPException ex) {
            traza.trace("Error general al buscar la version del archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(new JFrame(), "Error general al buscar el archivo\n" + ex.getMessage(), "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            traza.trace("Error al buscar la version del archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error al buscar la version del archivo\n" + ex.getMessage(), "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
        }

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelImagen = new javax.swing.JTabbedPane();
        jlMensaje1 = new javax.swing.JLabel();
        jlMensaje2 = new javax.swing.JLabel();
        jlMensaje3 = new javax.swing.JLabel();
        jlMensaje4 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jbImprimir = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        cboVersion = new javax.swing.JComboBox();
        btnAbrir = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        jbZommOut = new javax.swing.JButton();
        jbZommIn = new javax.swing.JButton();
        jbRotar = new javax.swing.JButton();
        jbPagIzq = new javax.swing.JButton();
        jbPagDer = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        jButtonCerrar = new javax.swing.JButton();
        fechaVencimiento = new javax.swing.JLabel();
        jdchVencimiento = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(224, 239, 255));
        setForeground(new java.awt.Color(224, 239, 255));
        setResizable(false);

        panelImagen.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panelImagen.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        panelImagen.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);

        jlMensaje1.setFont(new java.awt.Font("DejaVu Sans", 0, 10)); // NOI18N
        jlMensaje1.setText("jLabel1");
        jlMensaje1.setAutoscrolls(true);

        jlMensaje2.setFont(new java.awt.Font("DejaVu Sans", 0, 10)); // NOI18N
        jlMensaje2.setText("jLabel1");
        jlMensaje2.setAutoscrolls(true);

        jlMensaje3.setFont(new java.awt.Font("DejaVu Sans", 0, 10)); // NOI18N
        jlMensaje3.setText("jLabel1");
        jlMensaje3.setAutoscrolls(true);

        jlMensaje4.setFont(new java.awt.Font("DejaVu Sans", 0, 10)); // NOI18N
        jlMensaje4.setText("jLabel1");
        jlMensaje4.setAutoscrolls(true);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jbImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/printer.png"))); // NOI18N
        jbImprimir.setToolTipText("Imprimir");
        jbImprimir.setFocusable(false);
        jbImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbImprimir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbImprimirActionPerformed(evt);
            }
        });
        jToolBar1.add(jbImprimir);
        jToolBar1.add(jSeparator4);

        cboVersion.setToolTipText("Seleccione la version del Documento");
        cboVersion.setDoubleBuffered(true);
        cboVersion.setName(""); // NOI18N
        cboVersion.setPreferredSize(new java.awt.Dimension(122, 25));
        cboVersion.setVerifyInputWhenFocusTarget(false);
        jToolBar1.add(cboVersion);

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/Open24.gif"))); // NOI18N
        btnAbrir.setToolTipText("Abrir");
        btnAbrir.setFocusable(false);
        btnAbrir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAbrir.setPreferredSize(new java.awt.Dimension(59, 25));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAbrir);
        jToolBar1.add(jSeparator2);

        jbZommOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/ZoomOut24.gif"))); // NOI18N
        jbZommOut.setMnemonic('-');
        jbZommOut.setToolTipText("Disminuir Imagen");
        jbZommOut.setPreferredSize(new java.awt.Dimension(50, 40));
        jbZommOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbZommOutActionPerformed(evt);
            }
        });
        jToolBar1.add(jbZommOut);

        jbZommIn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/ZoomIn24.gif"))); // NOI18N
        jbZommIn.setMnemonic('+');
        jbZommIn.setToolTipText("Aumentar Imagen");
        jbZommIn.setPreferredSize(new java.awt.Dimension(50, 40));
        jbZommIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbZommInActionPerformed(evt);
            }
        });
        jToolBar1.add(jbZommIn);

        jbRotar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/arrow_rotate_anticlockwise.png"))); // NOI18N
        jbRotar.setMnemonic('r');
        jbRotar.setToolTipText("Girar el Documento");
        jbRotar.setPreferredSize(new java.awt.Dimension(50, 40));
        jbRotar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbRotarActionPerformed(evt);
            }
        });
        jToolBar1.add(jbRotar);

        jbPagIzq.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/arrow_left.png"))); // NOI18N
        jbPagIzq.setMnemonic('b');
        jbPagIzq.setToolTipText("Pag.Anterior");
        jbPagIzq.setPreferredSize(new java.awt.Dimension(50, 40));
        jbPagIzq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPagIzqActionPerformed(evt);
            }
        });
        jToolBar1.add(jbPagIzq);

        jbPagDer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/arrow_right.png"))); // NOI18N
        jbPagDer.setMnemonic('n');
        jbPagDer.setToolTipText("Pag. Siguiente");
        jbPagDer.setPreferredSize(new java.awt.Dimension(50, 40));
        jbPagDer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPagDerActionPerformed(evt);
            }
        });
        jToolBar1.add(jbPagDer);
        jToolBar1.add(jSeparator3);

        jButtonCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/action_stop.gif"))); // NOI18N
        jButtonCerrar.setToolTipText("Cerrar");
        jButtonCerrar.setPreferredSize(new java.awt.Dimension(100, 40));
        jButtonCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCerrarActionPerformed(evt);
            }
        });

        fechaVencimiento.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlMensaje4, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
            .addComponent(jlMensaje3, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
            .addComponent(jlMensaje2, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
            .addComponent(jlMensaje1, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdchVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(fechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(panelImagen, javax.swing.GroupLayout.DEFAULT_SIZE, 955, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fechaVencimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jdchVencimiento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelImagen, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlMensaje1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlMensaje2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlMensaje3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlMensaje4)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbZommOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbZommOutActionPerformed
        scalaImagen=scalaImagen*0.8;
        ImagePanel imageTab = new ImagePanel() ;
        imageTab.setImage(Imagenes.scale(scalaImagen,(BufferedImage)buferImagenes.get(panelImagen.getSelectedIndex())));
        JScrollPane sp = new JScrollPane(imageTab);
        sp.getVerticalScrollBar().setUnitIncrement(100);
        sp.getHorizontalScrollBar().setUnitIncrement(100);
        panelImagen.setComponentAt(panelImagen.getSelectedIndex(),sp);
        panelImagen.repaint();
}//GEN-LAST:event_jbZommOutActionPerformed

    private void jbZommInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbZommInActionPerformed
        scalaImagen=scalaImagen*1.2;
        ImagePanel imageTab = new ImagePanel() ;
        imageTab.setImage(Imagenes.scale(scalaImagen,(BufferedImage)buferImagenes.get(panelImagen.getSelectedIndex())));
        JScrollPane sp = new JScrollPane(imageTab);
        sp.getVerticalScrollBar().setUnitIncrement(100);
        sp.getHorizontalScrollBar().setUnitIncrement(100);
        panelImagen.setComponentAt(panelImagen.getSelectedIndex(),sp);
        panelImagen.repaint();
}//GEN-LAST:event_jbZommInActionPerformed

    private void jbRotarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbRotarActionPerformed
        ImagePanel imageTab = new ImagePanel() ;
        buferImagenes.set(panelImagen.getSelectedIndex(),Imagenes.rotate((BufferedImage)buferImagenes.get(panelImagen.getSelectedIndex()),90));
        imageTab.setImage(Imagenes.scale(scalaImagen,(BufferedImage)buferImagenes.get(panelImagen.getSelectedIndex())));
        JScrollPane sp = new JScrollPane(imageTab);
        sp.getVerticalScrollBar().setUnitIncrement(100);
        sp.getHorizontalScrollBar().setUnitIncrement(100);
        panelImagen.setComponentAt(panelImagen.getSelectedIndex(),sp);
        panelImagen.repaint();
}//GEN-LAST:event_jbRotarActionPerformed

    private void jbPagIzqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbPagIzqActionPerformed
        try{
            if(panelImagen.getSelectedIndex()-1>=0){
                panelImagen.setSelectedIndex(panelImagen.getSelectedIndex()-1);
            }
        }catch (IndexOutOfBoundsException ex){
            traza.trace("error al rotar ir a la pagina izquierda", Level.ERROR, ex);
        }
}//GEN-LAST:event_jbPagIzqActionPerformed

    private void jbPagDerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbPagDerActionPerformed
        try{
            panelImagen.setSelectedIndex(panelImagen.getSelectedIndex()+1);
        }catch (IndexOutOfBoundsException ex){
            traza.trace("error al ir a la pagina derecha", Level.ERROR, ex);
        }
}//GEN-LAST:event_jbPagDerActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
        getVersion(Integer.parseInt(cboVersion.getSelectedItem().toString()));
}//GEN-LAST:event_btnAbrirActionPerformed

    private void jButtonCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCerrarActionPerformed
        
        File fileCod = new File(toolsTiff.getArchivoCodificado());
        if(toolsTiff.getDirTemporal().exists()){
            File[] files = toolsTiff.getDirTemporal().listFiles();
            for(File f : files){
                if(f.delete()){
                    traza.trace("eliminado archivo "+f.getName(), Level.INFO);
                }else{
                    traza.trace("problemas al eliminar el archivo "+f.getName(), Level.WARN);
                }
            }
        }
        
        
        
        dispose();
        Runtime.getRuntime().gc();
        Runtime.getRuntime().exit(0);
//        System.gc();
//        System.exit(0);
        //        //Principal.desktop.removeAll();
        //        if(Constantes.ACCION.equalsIgnoreCase("CAPTURAR")){
        //            //Principal.creaEditaExpediente.show();
        //            Principal.creaEditaExpediente = new CreaEditaExpediente();
        //            Principal.desktop.add(Principal.creaEditaExpediente);
        //            Principal.desktop.repaint();
        //        }else if(Constantes.ACCION.equalsIgnoreCase("CONSULTAR")){
        //            Expediente expe = Principal.resultadoExpediente.getExpe();
        //            Principal.resultadoExpediente.show();
        //            Principal.desktop.repaint();
        //        }
}//GEN-LAST:event_jButtonCerrarActionPerformed

    private void jbImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbImprimirActionPerformed

        Impresor impresor = new Impresor();
        for(int i=0; i<panelImagen.getTabCount(); i++){
            JScrollPane sp=(JScrollPane)panelImagen.getComponentAt(i);
            ImagePanel ip=(ImagePanel)sp.getViewport().getView();
            impresor.append(ip);
        }
        impresor.print();
}//GEN-LAST:event_jbImprimirActionPerformed

//    public static void main(String[] args){
//        new VerDocumento(null);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JComboBox cboVersion;
    private javax.swing.JLabel fechaVencimiento;
    private javax.swing.JButton jButtonCerrar;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton jbImprimir;
    private javax.swing.JButton jbPagDer;
    private javax.swing.JButton jbPagIzq;
    private javax.swing.JButton jbRotar;
    private javax.swing.JButton jbZommIn;
    private javax.swing.JButton jbZommOut;
    private com.toedter.calendar.JDateChooser jdchVencimiento;
    private javax.swing.JLabel jlMensaje1;
    private javax.swing.JLabel jlMensaje2;
    private javax.swing.JLabel jlMensaje3;
    private javax.swing.JLabel jlMensaje4;
    private javax.swing.JTabbedPane panelImagen;
    // End of variables declaration//GEN-END:variables

    public JButton getJbImprimir() {
        return jbImprimir;
    }

}
