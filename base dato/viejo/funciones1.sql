  /**
    funcion que busca la categorias segun el parametro dado
  **/
  CREATE OR REPLACE FUNCTION f_buscar_categoria(p_idcategoria in integer, p_idlibreria in integer, p_categoria in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_cat refcursor;
  begin


    if p_idCategoria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_categoria=p_idcategoria
          order by c.categoria;

    elsif p_idLibreria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_libreria=p_idlibreria
          order by c.categoria;

    elsif p_categoria is not null then
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.categoria=p_categoria
          order by c.categoria;
    else
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
           order by c.categoria;
    end if;
    return cursor_cat;
    close cursor_cat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
    funcion que busca las subCategoria segun el parametro dado
  **/
  CREATE OR REPLACE FUNCTION f_buscar_subcategoria(p_idcategoria in integer, p_idsubcategoria in integer, p_subcategoria in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_subcat refcursor;
  begin

    if p_idCategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_categoria=p_idcategoria
          order by s.subcategoria;

    elsif p_idSubCategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_subcategoria=p_idsubcategoria
          order by s.subcategoria;

    elsif p_subCategoria is not null then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.subcategoria=p_subcategoria
          order by s.subcategoria;

    else

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
             order by s.subcategoria;
    end if;
    return cursor_subcat;
    close cursor_subcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
    funcion que busca los tipos de documentos segun el parametro dado
  **/
  CREATE OR REPLACE FUNCTION f_buscar_tipo_documento(p_idcategoria in integer, p_idsubcategoria in integer, p_tipodocumemto in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_tipodoc refcursor;
  begin

    if (p_idCategoria > 0) and (p_idSubCategoria > 0) then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria and t.id_subcategoria=p_idsubcategoria
         order by t.tipo_documento;

    elsif p_idCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria order by t.tipo_documento;

    elsif p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_subcategoria=p_idsubcategoria order by t.tipo_documento;

    elsif p_tipodocumemto is not null then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.tipo_documento=p_tipodocumemto order by t.tipo_documento;

    else

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
            order by t.tipo_documento;

    end if;
    return cursor_tipodoc;
    close cursor_tipodoc;



  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
    funcion que busca todos los argumetos del id de la
    categoria dada
  **/
  CREATE OR REPLACE FUNCTION f_buscar_indices(p_idcategoria in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_arg refcursor;
  begin

    open cursor_arg for
       select * from indices i where id_categoria=p_idcategoria order by i.id_indice;
    return cursor_arg;
    close cursor_arg;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca todas las librerias con su categoria
     para la configuracion del usuarios
  **/
  CREATE OR REPLACE FUNCTION f_buscar_libreriascategorias() returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_libcat refcursor;
  begin

    open cursor_libcat for
       select l.id_libreria, l.libreria descLibreria, c.id_categoria, c.categoria descCategoria,
              r.id_rol, r.rol descRol, e.id_estatus, e.estatus desEstatus
          from libreria l inner join categoria c on l.id_libreria=c.id_libreria
                          inner join estatus e
                                 on (l.id_estatus=e.id_estatus and c.id_estatus=e.id_estatus),
                          rol r
          where e.id_estatus=1
          order by l.libreria, c.categoria;

    return cursor_libcat;
    close cursor_libcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca el actual perfil del usuario
     para su reconfiguracion
  **/
  CREATE OR REPLACE FUNCTION f_buscar_perfil(p_idusuario in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_perfil refcursor;
  begin

    open cursor_perfil for
        select distinct u.nombre, u.apellido, u.id_usuario, e.estatus estatus_usuario,
               f.fabrica pertenece, r.rol, 
               l.id_libreria, l.libreria,
               c.id_categoria, c.categoria
            from usuario u full join perfil p on u.id_usuario=p.id_usuario
                           full join estatus e on u.id_estatus=e.id_estatus
                           full join fabrica f on u.id_usuario=f.usuario
                           full join rol r on p.id_rol=r.id_rol
                           full join libreria l on p.id_libreria=l.id_libreria
                           full join categoria c on p.id_categoria=c.id_categoria
             where u.id_usuario=p_idusuario
             order by l.libreria, c.categoria;


    return cursor_perfil;
    close cursor_perfil;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca solo los estatus
     disponibles
  **/
  CREATE OR REPLACE FUNCTION f_buscar_estatus(p_idestatus in integer, p_estatus in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_est refcursor;
  begin

    if p_idestatus > 0 then
      open cursor_est for
         select id_estatus, estatus from estatus where id_estatus=p_idestatus;
    elsif p_estatus is not null then
      open cursor_est for
         select id_estatus, estatus from estatus where estatus=p_estatus;
    end if;

    return cursor_est;
    close cursor_est;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca solo librerias
  **/
  CREATE OR REPLACE FUNCTION f_buscar_libreria(p_idlibreria in integer, p_libreria in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_lib refcursor;
  begin

    if p_idlibreria > 0 then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.id_libreria=p_idlibreria
             order by l.libreria asc;

    elsif p_libreria is not null then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.libreria=p_libreria
             order by l.libreria asc;

    else

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             order by l.libreria asc;

    end if;
    return cursor_lib;
    close cursor_lib;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca los datos de un
     combo en especifico
  **/
  CREATE OR REPLACE FUNCTION f_buscar_datos_combo(p_idcodigoindice in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice
             from lista_desplegables l inner join indices a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idcodigoindice
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     permite comprobar si el tipo de documento
     es para la foto del expediente
  **/
  CREATE OR REPLACE FUNCTION f_comprobar_foto_ficha(p_idtipodocumento in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_foto refcursor;
  begin

    open cursor_foto for
       select ficha from tipodocumento where id_documento=p_idtipodocumento;

    return cursor_foto;
    close cursor_foto;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     busca los indices de los datos adicionales
     del tipo de documento
  **/
  CREATE OR REPLACE FUNCTION f_buscar_indice_datosadicional(p_idtipodocumento in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select *
         from dato_adicional t
         where t.id_documento = p_idtipodocumento
         order by t.id_dato_adicional;

    return cursor_dato;
    close cursor_dato;



  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     busca los valores de los datos adicionales
     segun el codigo del indice
  **/
  CREATE OR REPLACE FUNCTION f_buscar_valor_datoadicional(p_idindicedato in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice_adicional
             from lista_desplegables l inner join dato_adicional a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idindicedato
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;
  
  /**
     busca el usuario en la tabla de fabrica
  **/
  CREATE OR REPLACE FUNCTION f_buscar_usuario_fabrica(p_usuario in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_fab refcursor;
  begin

    open cursor_fab for
       select usuario, fabrica from fabrica f where f.usuario=p_usuario;


    return cursor_fab;
    close cursor_fab;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;



  /**
     procedimiento para modificar el
     estatus del usuario
  **/
  CREATE OR REPLACE FUNCTION p_modificar_usuario(p_idusuario in varchar, p_idestatus in integer) RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update usuario set id_estatus=p_idestatus where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que elimina el actual
     perfil del usuario
  **/
  CREATE OR REPLACE FUNCTION p_eliminar_perfil(p_idusuario in varchar)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   delete from perfil where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procediemiento que modifica el
     estatus de la libreria
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_librerias(p_idlibreria in integer, p_idestatus in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update libreria set id_estatus=p_idestatus where id_libreria=p_idlibreria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimiento que modifica el
     estatus de la categoria
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_categorias(p_idcategoria in integer, p_idestatus in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update categoria set id_estatus=p_idestatus where id_categoria=p_idcategoria;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que modifica el
     estatus de la subcategoria
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_subcategorias(p_idsubcategoria in integer, p_idestatus in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update subcategoria set id_estatus=p_idestatus where id_subcategoria=p_idsubcategoria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimiento que modifica el estatus,
     si necesita o no la fecha de vencimiento
     y si lleva dato adicional el tipo de
     documento
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_tipodocumento(p_idtipodocumento in integer, p_idestatus in integer, p_vencimiento in char, p_datoadicional in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update tipodocumento set id_estatus=p_idestatus, vencimiento=p_vencimiento,
                        dato_adicional=p_datoadicional
   where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que modifica los datos
     de un combo
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_datos_combo(p_idcombo in integer, p_dato in varchar)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update lista_desplegables
      set descripcion = p_dato
    where id_lista = p_idcombo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento para indocar el tipo de
     documento es para la foto del expediente
  **/
  CREATE OR REPLACE FUNCTION p_actualiza_td_foto(p_idtipodocumento in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update tipodocumento set ficha='1' where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que actualiza los indices
     de los expedientes
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_indices(p_idindices in integer, p_idcategoria in integer,
                                 p_indice in varchar, p_tipodato in varchar,
                                 p_codigo in integer, p_clave in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin
    update indices
       set id_categoria = p_idcategoria,
           indice = p_indice,
           tipo = p_tipodato,
           codigo = p_codigo,
           clave = p_clave
     where id_indice = p_idindices;
	 
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento para agregar un nuevo
     usuario al gestor documental
  **/
  CREATE OR REPLACE FUNCTION p_agrega_usuario(p_idusuario in varchar, p_nombre in varchar,
                             p_apellido in varchar, p_cedula in varchar,
                             p_sexo in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into usuario
      (id_usuario, nombre, apellido, cedula, sexo, id_estatus)
    values
      (p_idusuario, p_nombre, p_apellido, p_cedula, p_sexo, 1);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que agrega perfil del nuevo
     del usuario
  **/
  CREATE OR REPLACE FUNCTION p_agregar_perfil(p_idlibreria in integer, p_idcategoria in integer, p_idusuario in varchar, p_idrol in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    if p_idcategoria = 0 and p_idlibreria = 0 then

      insert into perfil(id_usuario, id_rol)
                  values(p_idusuario, p_idrol);

    elsif p_idcategoria = 0 then

      insert into perfil(id_libreria, id_usuario, id_rol)
                  values(p_idlibreria, p_idusuario, p_idrol);
    else

      insert into perfil(id_libreria, id_categoria, id_usuario, id_rol)
                  values(p_idlibreria, p_idcategoria, p_idusuario, p_idrol);
    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procediemiento que agrega un usuario para
     identificarlo si pertenece a la fabrica de
     digitalizacion o no
  **/
  CREATE OR REPLACE FUNCTION p_agregar_fabrica(p_idusuario in varchar, p_pertenece in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into fabrica(usuario, fabrica)
           values(p_idusuario, p_pertenece);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;	   
  end;$_$;

  /**
     procedimiento que agrega nuevas librerias
  **/
  CREATE OR REPLACE FUNCTION p_agregar_libreria(p_libreria in varchar, p_idestatus in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into libreria(libreria, id_estatus)
                values(p_libreria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;			
  end;$_$;

  /**
     procedimiento que agrega nuevas categorias
     asociado a el id de la libreria
  **/
  CREATE OR REPLACE FUNCTION p_agregar_categoria(p_idlibreria in integer, p_categoria in varchar, p_idestatus in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into categoria
      (id_libreria, categoria, id_estatus)
    values
      (p_idlibreria, p_categoria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;  
  end;$_$;


  /**
     procedimiento que agrega nuevas subcategorias
     asociado a el id de la categoria
  **/
  CREATE OR REPLACE FUNCTION p_agregar_subcategoria(p_idcategoria in integer, p_subcategoria in varchar, p_idestatus in integer)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into subcategoria
      (id_categoria, subcategoria, id_estatus)
    values
      (p_idcategoria, p_subcategoria, p_idestatus);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimiento que agregar nuevos tipos de documentos
     asociado a el id de la categoria y subcategoria
  **/
  CREATE OR REPLACE FUNCTION p_agregar_tipodocumento(p_idcategoria in integer, p_idsubcategoria in integer, p_tipodocumento in varchar, p_idestatus in integer, p_vencimiento char, p_datoadicional in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into tipodocumento
      (id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional)
    values
      (p_idcategoria, p_idsubcategoria, p_tipodocumento, p_idestatus, p_vencimiento, p_datoadicional);


  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que agrega los argumento
     para el expediente
  **/
  CREATE OR REPLACE FUNCTION p_agregar_indices(p_idcategoria in integer, p_indice in varchar,
                              p_tipodato in varchar, p_codigo in integer,
                              p_clave in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into indices
      (id_categoria, indice, tipo, codigo, clave)
    values
      (p_idcategoria, p_indice, p_tipodato, p_codigo, p_clave);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimiento que agrega los datos
     de un combo en especifico
  **/
  CREATE OR REPLACE FUNCTION p_agregar_datos_combo(p_codigoindice in integer, p_dato in varchar)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_codigoindice, p_dato);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

    /**
     procedimiento que agrega valores a los combos
     de los datos adicionales
  **/
  CREATE OR REPLACE FUNCTION p_agregar_datos_combo_da(p_iddatoadiciona in integer, p_datocombo in varchar)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin



    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_iddatoadiciona, p_datocombo);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


    /**
     procedimiento para agregar los indices
     de datos adicionales de un tipo de
     documento
  **/
  CREATE OR REPLACE FUNCTION p_guardar_indice_datoadicional(p_idtipodocumento in integer,
                                           p_datoadicional in varchar,
                                           p_tipo in varchar)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into dato_adicional
      (indice_adicional, tipo, id_documento)
    values
      (p_datoadicional, p_tipo, p_idtipodocumento);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     guarda los valor de los datos adicionales
  **/
  CREATE OR REPLACE FUNCTION p_guarda_valor_dato_adicional(p_iddatoadicional in integer, p_idvalor in integer,
                                          p_valor in varchar, p_numerodocumento in integer,
                                          p_version in integer, p_expediente in varchar,
                                          p_flag in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$
  begin



    if p_flag = 0 then

      update valor_dato_adicional
         set valor = p_valor
       where id_valor = p_idvalor
         and numero = p_numerodocumento
         and version = p_version
         and expediente = p_expediente;

    elsif p_flag = 1 then

     insert into valor_dato_adicional
        (id_dato_adicional, valor, numero, version, expediente)
      values
        (p_iddatoadicional, p_valor, p_numerodocumento, p_version, p_expediente);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     guarda los valor de los datos adicionales
  **/
  CREATE OR REPLACE FUNCTION p_actualiza_codigo_combo(p_codigocombo in integer, p_flag in char)  RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    if p_flag = '0' then

      update indices set codigo = p_codigocombo where id_indice = p_codigocombo;

    elsif p_flag = '1' then

      update dato_adicional  set codigo = p_codigocombo where id_dato_adicional = p_codigocombo;

    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;



  -- Author  : DEVELCOM
  -- Created : 21/02/2014 10:05:37 a.m.
  -- Purpose : inicio de sesion

  /** funcion que comprueba el usuario en base de dato
      si la variable usuario es null devuelve todos los
      usuario en base de dato
  **/
  CREATE OR REPLACE FUNCTION f_verificar_usuario(p_usuario in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_user refcursor;
  begin

   if p_usuario is not null then

    open cursor_user for
      select u.id_usuario, u.nombre, u.apellido, u.password passUser, e.id_estatus, e.estatus, c.*
      from usuario u inner join estatus e on u.id_estatus=e.id_estatus,
      configuracion c
      where u.id_usuario = p_usuario;

   else

     open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

   end if;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
    funcion que busca todos los usuarios para
    el autocompletar en el inicio de sesion
  **/
  CREATE OR REPLACE FUNCTION f_usuarios() returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_user refcursor;
  begin

    open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.sexo,
              e.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

       return cursor_user;
       close cursor_user;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
    funcion para crear la sesion del usuario segun su
    rol y perfil en el sistema
  **/
  CREATE OR REPLACE FUNCTION f_crear_sesion(p_usuario in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
       select distinct r.rol desc_rol, e.estatus estatus_usuario, u.id_usuario id_user,
              u.nombre, u.apellido, u.cedula, u.id_estatus
           from usuario u inner join estatus e on u.id_estatus=e.id_estatus
                inner join perfil p on p.id_usuario=u.id_usuario
                inner join rol r on r.id_rol=p.id_rol
           where u.id_usuario=p_usuario;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
    funcion que busca las librerias y categorias
    segun el perfil del usuario
  **/
  CREATE OR REPLACE FUNCTION f_buscar_lib_cat_perfil(p_usuario in varchar, p_perfil in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
        select p.id_usuario usuario, r.rol,
               p.id_libreria, l.libreria, el.estatus status_lib,
               p.id_categoria, c.categoria, ec.estatus status_cat
           from perfil p 
                inner join rol r on p.id_rol=r.id_rol
                inner join libreria l on p.id_libreria=l.id_libreria
                inner join estatus el on l.id_estatus=el.id_estatus
                inner join categoria c on p.id_categoria=c.id_categoria
                inner join estatus ec on c.id_estatus=ec.id_estatus
           where p.id_usuario=p_usuario and r.rol=p_perfil
           order by l.libreria;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que busca los usuarios que
     pertencen a la fabrica y los que no
     pertenecen a la misma
  **/
  CREATE OR REPLACE FUNCTION f_buscar_no_fabrica(p_fechadesde in date, p_fechahasta in date,
                               p_estatusdocumento integer, p_idcategoria in integer,
                               p_expedeinte in varchar)
                               returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_user refcursor;

  begin

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion between to_date(p_fechadesde, 'dd/MM/yyyy')
           and to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion >= to_date(p_fechadesde, 'dd/MM/yyyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion <= to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_expedeinte is not null then

         open cursor_user for
            select i.*, e.valor, e.expediente
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
             where d.id_expediente = p_expedeinte
               and d.estatus_documento=p_estatusdocumento
               and i.id_categoria=p_idcategoria
               order by i.id_indice;
    else

      open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca los usuarios que
     pertencen a la fabrica y los que no
     pertenecen a la misma
  **/
  CREATE OR REPLACE FUNCTION f_buscar_fabrica(p_usuario in varchar, p_fechadesde in date,
                            p_fechahasta in date, p_estatusdocumento integer,
                            p_idcategoria in integer, p_expediente in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_user refcursor;
    sfabrica char(1);
  begin

    select t.fabrica into sfabrica from fabrica t where t.usuario=p_usuario;

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion between to_date(p_fechadesde, 'dd/MM/yyyy')
           and to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion >= to_date(p_fechadesde, 'dd/MM/yyyy')
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion <= to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_expediente is not null then

         open cursor_user for
            select distinct e.expediente, e.valor, i.*, f.fabrica
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
               inner join fabrica f on f.usuario=di.usuario_digitalizo
               inner join usuario u on u.id_usuario=f.usuario
             where d.id_expediente = p_expediente
               and d.estatus_documento=p_estatusdocumento
               and f.fabrica=sfabrica
               and i.id_categoria=p_idcategoria
               order by e.expediente, i.id_indice;
    else

      open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimento que actualiza el estado
     de un tipo de documento
  **/
  CREATE OR REPLACE FUNCTION p_aprobar_documento(p_idinfodocumento in integer, p_usuario in varchar) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$
  DECLARE
  
    fecha_actual date;
    estatu integer;
    idInfoDoc integer;
	
  begin
  

    select sysdate into fecha_actual from dual;

    update infodocumento set estatus_documento=1 where id_infodocumento= p_idinfodocumento;
    commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;

    if estatu > 0 then

      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;

      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_aprobacion = fecha_actual,
               usuario_aprobacion = p_usuario
         where id_infodocumento = p_idinfodocumento;
      else
        insert into datos_infodocumento(id_infodocumento, fecha_aprobacion, usuario_aprobacion)
                  values(p_idinfodocumento, fecha_actual, p_usuario);
      end if;
    else
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      commit;
    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca las causas de rechazos
  **/
  CREATE OR REPLACE FUNCTION f_buscar_causas_rechazo() returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_causa refcursor;
  begin

    open cursor_causa for
      select id_causa, causa from causa;

    return cursor_causa;
    close cursor_causa;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimento que actualiza el estado
     de un tipo de documento de pendiente
     a rechazado
  **/
  CREATE OR REPLACE FUNCTION p_rechazar_documento(p_idinfodocumento in integer, p_usuario in varchar, p_causa in varchar, p_motivo in varchar) RETURNS void
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    fecha_actual date;
    estatu integer;
    idInfoDoc integer;
  begin

    select sysdate into fecha_actual from dual;

    update infodocumento set estatus_documento=2 where id_infodocumento= p_idinfodocumento;
    commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;


    if estatu > 0 then

      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;

      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_rechazo = fecha_actual,
               usuario_rechazo = p_usuario,
               motivo_rechazo = p_motivo,
               causa_rechazo = p_causa
         where id_infodocumento = p_idinfodocumento;
      else

        insert into datos_infodocumento(id_infodocumento, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo)
                  values(p_idinfodocumento, fecha_actual, p_usuario, p_motivo, p_causa);
      end if;
    else
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      commit;

    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;



  /**
     funcion que busca todos los datos de in tipo de
     documento
  **/
  CREATE OR REPLACE FUNCTION f_buscar_infodocumento(p_idexpediente in varchar, p_ids_documento in integer,
                                  p_estatusdoc in integer, p_redigitalizo in char,
                                  p_estatusaprobado in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_infodoc refcursor;
  begin

   if p_estatusaprobado = 1 then
   
      open cursor_infodoc for
        select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where i.id_expediente=p_idexpediente
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in (p_ids_documento)
                 and (i.estatus_documento=p_estatusdoc or i.estatus_documento=p_estatusaprobado)
                 and i.re_digitalizado=p_redigitalizo
           order by t.tipo_documento, i.numero_documento, i.version desc;
   else

     open cursor_infodoc for
        select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where e.expediente=p_idexpediente
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in (p_ids_documento)
                 and i.estatus_documento=p_estatusdoc and i.re_digitalizado=p_redigitalizo
           order by t.tipo_documento, i.numero_documento, i.version desc;

   end if;
    return cursor_infodoc;
    close cursor_infodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que busca el puntero del
     archivo fisico para ser eliminado
  **/
  CREATE OR REPLACE FUNCTION f_eliminar_archivo(p_idinfodocumento in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE

    cursor_eliminar refcursor;
  begin

   open cursor_eliminar for
        select t.ruta_archivo, t.nombre_archivo from infodocumento t where t.id_infodocumento=p_idinfodocumento;

    return cursor_eliminar;
    close cursor_eliminar;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimiento que gestiona el agregar los datos
     de un tipo de documento, que puede ser insert o
     update
  **/
  CREATE OR REPLACE FUNCTION p_agregar_registro_archivo(p_accion in varchar, p_nombrearchivo in varchar,
                                       p_iddocumento in integer, p_rutaarchivo in varchar,
                                       p_cantpaginas in integer, p_version in integer,
                                       p_idexpediente in varchar, p_numerodoc in integer,
                                       p_fechavencimiento in date, p_datoadicional in varchar,
                                       p_usuario in varchar, p_idinfodocumento in integer,
                                       p_estatus in integer, p_redigitalizo in char,
                                       p_formato in varchar) 
   RETURNS integer
    LANGUAGE plpgsql
    AS $_$
  DECLARE
   p_idinfodoc integer;
   fecha_actual date;
  begin

   
   p_idinfodoc := 0;

   select sysdate into fecha_actual from dual;

   if p_accion = 'versionar' then

     insert into infodocumento(id_documento, id_expediente, nombre_archivo, ruta_archivo,
                               formato, numero_documento, version, paginas, fecha_vencimiento,
                               estatus_documento, re_digitalizado)
                 values(p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo,
                        p_formato, p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus,
                        p_redigitalizo);
	
	 select lastval() into p_idinfodoc;

     if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion,
                                        usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, fecha_actual,
                           p_usuario, p_datoadicional);
      else
        rollback;
      end if;

    elsif p_accion = 'reemplazar' then

      p_idinfodoc := -1;
      update infodocumento set nombre_archivo=p_nombrearchivo, ruta_archivo=p_rutaarchivo,
                               paginas=p_cantpaginas, version=p_version, id_expediente=p_idexpediente,
                               numero_documento=p_numerodoc, fecha_vencimiento=p_fechavencimiento,
                               formato=p_formato, estatus_documento=p_estatus
      where id_infodocumento=p_idinfodocumento;

      update datos_infodocumento set fecha_digitalizacion = fecha_actual,
                                     usuario_digitalizo = p_usuario,
                                     dato_adicional = p_datoadicional
       where id_infodocumento=p_idinfodocumento;

    elsif p_accion = 'Guardar' then
       insert into infodocumento(id_documento, id_expediente, nombre_archivo,
                                ruta_archivo, formato, numero_documento, version, paginas,
                                fecha_vencimiento, estatus_documento, re_digitalizado)
                  values(p_iddocumento, p_idexpediente, p_nombrearchivo,
                         p_rutaarchivo, p_formato, p_numerodoc, p_version, p_cantpaginas,
                         p_fechavencimiento, p_estatus, 0);
	
	 select lastval() into p_idinfodoc;

      if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion,
                                        usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, fecha_actual, p_usuario, p_datoadicional);

         if p_redigitalizo = 1 then
           update infodocumento t set t.re_digitalizado=p_redigitalizo
              where t.id_infodocumento=p_idinfodocumento;
         end if;
      else
        rollback;
      end if;

    end if;

   return p_idinfodoc;
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
   end;$_$;


   /**
     procedimiento para actulizar el nombre del
     archivo despues de codificarlo
  **/
  CREATE OR REPLACE FUNCTION p_actualiza_nombre_archivo(p_nombrearchivo in varchar, p_idinfodocumento in integer) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   update infodocumento set nombre_archivo=p_nombrearchivo
        where id_infodocumento=p_idinfodocumento;
		
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que busca el ultimo numero del tipo
     de documento
  **/
  CREATE OR REPLACE FUNCTION f_buscar_ultimo_numero(p_iddocumento in integer, p_idexpediente in varchar) returns integer 
   
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    numero_doc integer;
  begin

   numero_doc := 0;
   
   select max(numero_documento) as numeroDocumento into numero_doc from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente;

   return numero_doc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que verifica si el consecutivo del
     numero del documento ya esta registrado
  **/
  CREATE OR REPLACE FUNCTION f_comprobar_numero_documento(p_iddocumento in integer, p_idexpediente in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_numdoc refcursor;
  begin

   open cursor_numdoc for
   select numero_documento  from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente
   order by numero_documento;

   return cursor_numdoc;
   close cursor_numdoc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que comprueba si el nombre del archiv
     esta registrado en la base de datos
  **/
  CREATE OR REPLACE FUNCTION f_comprobar_nombre_archivo(p_idinfodocumento in integer, p_idexpediente in varchar) returns varchar 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    nombre varchar(255);
  begin

   nombre := null;
   
   select nombre_archivo into nombre from infodocumento
   where id_infodocumento= p_idinfodocumento and id_expediente=p_idexpediente;

   return nombre;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcio que busca los datos necesario para luego
     buscar el fisico del tipo de documento
  **/
  CREATE OR REPLACE FUNCTION f_buscar_fisico_documento(p_iddocumento in integer, p_numerodoc in integer, p_idexpediente in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_doc refcursor;
  begin

    open cursor_doc for
        select i.*, c.id_categoria, s.id_subcategoria, t.tipo_documento tipoDoc,
               e.estatus_documento estatusArchivo
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                                inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
                                inner join tipodocumento t on i.id_documento=t.id_documento
                                inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
                                inner join categoria c on s.id_categoria=c.id_categoria
           where i.id_documento=p_iddocumento and i.id_expediente=p_idexpediente
                 and i.numero_documento=p_numerodoc and i.estatus_documento<>2
           order by i.version desc;

  return cursor_doc;
  close cursor_doc;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca la ultima version
     del documento
  **/
  CREATE OR REPLACE FUNCTION f_buscar_ultima_version(p_iddocumento in integer, p_idexpediente in varchar,
                                   p_numerodocumento in integer)
                                   returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_ver refcursor;
  begin

    open cursor_ver for
      select max(i.version) as version
        from infodocumento i
        where i.id_documento=p_iddocumento
              and i.id_expediente=p_idexpediente
              and i.numero_documento=p_numerodocumento
              and i.estatus_documento<>2;

  return cursor_ver;
  close cursor_ver;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca la informacion de
     cada dato adicional segun el tipo
     y numero de documento
  **/
  CREATE OR REPLACE FUNCTION f_buscar_valor_dato_adicional(p_idocumento in integer, p_idexpediente in varchar,
                                         p_numerodoc in integer, p_version in integer)
                                         returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select * 
           from dato_adicional d 
             inner join valor_dato_adicional v on d.id_dato_adicional=v.id_dato_adicional
           where d.id_documento=p_idocumento
                and v.expediente=p_idexpediente
                and v.numero=p_numerodoc
                and v.version=p_version;

  return cursor_dato;
  close cursor_dato;



  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento para elimiar un registro
     de la tabla infodocumento al resturar
     un error en el guardado de un documento
  **/
  CREATE OR REPLACE FUNCTION p_eliminar_registro_archivo(p_idinfodocumento in integer) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

   delete from datos_infodocumento where id_infodocumento=p_idinfodocumento;
   delete from infodocumento where id_infodocumento=p_idinfodocumento;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que busca la informacion necesaria
     del documento a eliminar
  **/
  CREATE OR REPLACE FUNCTION f_buscar_infodocumento(p_idinfodocumento in integer, p_iddocumento in integer,
                                  p_idexpediente in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
     cursor_info refcursor;
  begin

  if p_idinfodocumento > 0 then

    open cursor_info for
    select * from infodocumento where id_infodocumento=p_idinfodocumento;

  elsif p_iddocumento > 0 then

    open cursor_info for
    select * from infodocumento
    where id_documento=p_iddocumento and id_expediente=p_idexpediente
    order by numero_documento, version;

  end if;
  return cursor_info;
  close cursor_info;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimineto para eliminar el registro
     de la informacion del tipo de documento
  **/
  CREATE OR REPLACE FUNCTION p_eliminar_infodocumento(p_idinfodocumento in integer) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$
  
  begin

    
    delete from datos_infodocumento where id_infodocumento = p_idinfodocumento;
    delete from infodocumento where id_infodocumento = p_idinfodocumento;


  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que actualiza la numeracion
     del tipo de documento
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_infodocumento(p_numerodoc in integer, p_idinfodocumento in integer,
                                       p_idexpediente in varchar) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    update infodocumento set numero_documento=p_numerodoc
    where id_infodocumento=p_idinfodocumento and id_expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     procedimiento que registra la traza del
     documento que se elimino
  **/
  CREATE OR REPLACE FUNCTION p_traza_elimina_documento(p_idexpedeinte in varchar, p_idlibreria in integer,
                                      p_idcategoria in integer, p_idsubcategoria in integer,
                                      p_iddocumento in integer, p_numerodoc in integer,
                                      p_fechavencimiento in date, p_cantpaginas in integer,
                                      p_versiondoc in integer, p_datoadicional in varchar,
                                      p_usuarioelimino in varchar, p_usuariodigitalizo in varchar,
                                      p_usuariorechazo in varchar, p_causaelimino in varchar,
                                      p_motivoelimino in varchar, p_causarechazo in varchar,
                                      p_motivorechazo in varchar) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    fecha_actual date;
    iddoceliminado integer;

  begin

    
    select sysdate into fecha_actual from dual;

    insert into documento_eliminado
      (id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino)
    values
      (p_idexpedeinte, p_idlibreria, p_idcategoria, p_idsubcategoria, p_iddocumento, p_numerodoc, p_versiondoc, p_cantpaginas, p_fechavencimiento, fecha_actual, p_usuarioelimino);

    select sq_documento_eliminado.currval into iddoceliminado from dual;

    if iddoceliminado > 0 then

      insert into datos_infodocumento
        (id_doc_eliminado, usuario_digitalizo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional)
      values
        (iddoceliminado, p_usuariodigitalizo, p_usuariorechazo, p_motivorechazo, p_causarechazo, fecha_actual, p_usuarioelimino, p_motivoelimino, p_causaelimino, p_datoadicional);
    else
      rollback;
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que busca la informacion necesaria
     para eliminar tipos de documentos
  **/
  CREATE OR REPLACE FUNCTION f_buscar_informaciondoc(p_idsdocumento in integer, p_idexpediente in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE

  cursor_infdoc refcursor;

  begin

    open cursor_infdoc for
         select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expedientes e on i.id_expediente=e.expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;
            /*select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expediente e on i.id_expediente=e.id_expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;*/

       return cursor_infdoc;
       close cursor_infdoc;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que busca todos los datos
     de un tipo de documento a eliminar
  **/
  CREATE OR REPLACE FUNCTION f_buscar_datosdoc(p_idinfodocumento in integer, p_versiondoc in integer,
                             p_idexpediente in varchar, p_numerodoc in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE

    cursor_doc refcursor;
  begin

    open cursor_doc for
      select i.*, d.*, c.id_categoria, s.id_subcategoria,
             t.tipo_documento tipoDoc, e.estatus_documento estatusArchivo
          from infodocumento i 
          inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
          inner join tipodocumento t on i.id_documento=t.id_documento
          inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
          inner join categoria c on s.id_categoria=c.id_categoria
          inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
      where i.id_infodocumento=p_idinfodocumento and i.id_expediente=p_idexpediente
            and i.version=p_versiondoc and i.numero_documento=p_numerodoc
            and i.estatus_documento=2;

     return cursor_doc;
     close cursor_doc;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  
  /**
     procedimiento que elimina los datos
     adicionales del documento
  **/
CREATE OR REPLACE FUNCTION p_eliminar_valordatadic(p_idvalor in integer, p_versiondoc in integer, p_numerodoc in integer)
  RETURNS void 
    LANGUAGE plpgsql
    AS $_$
    
  begin
  
    delete from valor_dato_adicional
     where id_valor = p_idvalor and version=p_versiondoc and numero=p_numerodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;
  
  /**
     procedimiento que actuliza los numeros
     del documento asociado
  **/
CREATE OR REPLACE FUNCTION p_actualiza_numero_da(p_idvalor in integer,p_numerodocumento in integer, p_version in integer, p_expediente in character varying)
  RETURNS void
    LANGUAGE plpgsql
    AS $_$
    
  begin
  
     update valor_dato_adicional
       set numero = p_numerodocumento
       where id_valor = p_idvalor
         and expediente = p_expediente
         and version=p_version;
		 
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;



  /**
    funcion para buscar y comprobar que exite el expediente
    en base de dato
  **/
  CREATE OR REPLACE FUNCTION f_buscar_expediente(p_expediente in varchar, p_idlibreria in integer, p_idcategoria in integer, p_flag in char) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_expe refcursor;
  begin

   if (p_flag = 1) then

      open cursor_expe for
         select e.expediente, e.id_libreria, e.id_categoria, e.valor, a.id_indice,
                c.categoria, l.libreria, a.indice, a.tipo, a.codigo, a.clave
             from expedientes e inner join indices a
                     on (e.id_categoria=a.id_categoria and e.id_indice=a.id_indice)
                  inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.id_libreria
              where e.expediente=p_expediente and a.id_categoria=p_idcategoria
                and e.id_libreria=p_idlibreria
              order by a.id_indice;
    else
      open cursor_expe for
         select e.*, c.categoria, l.libreria
             from expedientes e inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.libreria
             where e.expediente=p_expediente;
    end if;
    return cursor_expe;
    close cursor_expe;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
    funcion que busca los dato de una tabla, o sea,
    nombre de columna y su tipo de dato
  **/
  CREATE OR REPLACE FUNCTION f_informacion_tabla(p_tabla in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_tabla refcursor;
  begin

    open cursor_tabla for
      SELECT s.OWNER, s.TABLE_NAME, s.COLUMN_NAME, s.DATA_TYPE
          FROM all_tab_columns s
      WHERE
      --owner = 'GESTORDOCUMENTAL' and
      table_name=p_tabla;
      return cursor_tabla;
      close cursor_tabla;
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


   /**
    procedimiento que guarda un nuevo expediente
  **/
  CREATE OR REPLACE FUNCTION p_guardar_expediente(p_idexpediente in varchar, p_idindice in integer,
                                 p_valor in varchar, p_idlibreria in integer,
                                 p_idcategoria in integer) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into expedientes
      (expediente, id_indice, valor, id_libreria, id_categoria)
    values
      (p_idexpediente, p_idindice, p_valor, p_idlibreria, p_idcategoria);


  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
    procedimiento para eliminar el expediente
  **/
  CREATE OR REPLACE FUNCTION p_eliminar_expediente(p_idexpediente in varchar) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    delete from expedientes where expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimeinto que actualiza los
     indices de un expediente
  **/
  CREATE OR REPLACE FUNCTION p_actualizar_indices(p_idexpedientenuevo in varchar,
                                 p_idexpedienteviejo in varchar, p_idindice in integer,
                                 p_valor in varchar, p_idlibreria in integer,
                                 p_idcategoria in integer) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$
    
  begin

    if (p_idexpedienteviejo = p_idexpedientenuevo) then

      update expedientes
         set valor = p_valor
       where id_indice = p_idindice
         and id_libreria = p_idlibreria
         and id_categoria = p_idcategoria;

    else
        insert into expedientes
          (expediente, id_indice, valor, id_libreria, id_categoria)
        values
          (p_idexpedientenuevo, p_idindice, p_valor, p_idlibreria, p_idcategoria);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimeinto que actualiza los
     indices de un expediente
  **/
  CREATE OR REPLACE FUNCTION f_modificar_indices(p_idexpedientenuevo in varchar,
                                 p_idexpedienteviejo in varchar, p_flag in varchar) returns refcursor 
   
    LANGUAGE plpgsql
    AS $_$
 DECLARE
    curor_indice refcursor;
  begin
    
    if p_flag = '0' then
    
      open curor_indice for
         select expediente from expedientes where expediente=p_idexpedientenuevo;
      return curor_indice;
      close curor_indice;
      
    elsif p_flag = '1' then
      
      delete from expedientes where expediente=p_idexpedienteviejo;

      update infodocumento set id_expediente=p_idexpedientenuevo where id_expediente=p_idexpedienteviejo;
      return curor_indice;
    end if;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;                              

  /**
     funcion que busca los tipos de documentos
     segun los identificadores de las subcategorias
  **/
  CREATE OR REPLACE FUNCTION f_buscar_tipo_documento(p_idssucategorias in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
 DECLARE
    curor_doc refcursor;
  begin
    

    open curor_doc for
     select t.*, e.*
       from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
       where t.id_subcategoria in (p_idssucategorias) and e.id_estatus=1;
       --where t.id_subcategoria = p_idssucategorias and e.id_estatus=1;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     funcion que busca la informacion
     para la ficha del expediente
  **/
  CREATE OR REPLACE FUNCTION f_buscar_foto_ficha(p_idexpediente in varchar) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
 DECLARE
    cursor_ficha refcursor;
  begin

    open cursor_ficha for
        select t.id_documento, i.id_infodocumento, i.nombre_archivo as nombreArchivo,
               t.tipo_documento as tipoDocumento, i.estatus_documento, i.ruta_archivo
          from tipodocumento t inner join infodocumento i on t.id_documento=i.id_documento
          where i.id_expediente=p_idexpediente and t.ficha='1';

    return cursor_ficha;
    close cursor_ficha;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;


  /**
     funcion que busca la informacion necesaria
     para eliminar tipos de documentos
  **/
  CREATE OR REPLACE FUNCTION f_foliatura_buscar_expediente(p_idexpediente in varchar, p_idlibreria in integer, p_idcategoria in integer) returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_folio refcursor;
  begin
  
    open cursor_folio for
        select i.id_expediente, l.id_libreria, c.id_categoria, s.id_subcategoria,
               t.id_documento, i.id_infodocumento, t.tipo_documento as documento,
               i.paginas as cantidadPAginas 
     from infodocumento i 
         inner join tipodocumento t on i.id_documento=t.id_documento
         inner join subcategoria s on s.id_subcategoria=t.id_subcategoria
         inner join categoria c on c.id_categoria=t.id_categoria
         inner join libreria l on l.id_libreria=c.id_libreria
        where i.id_expediente=p_idexpediente and i.estatus_documento=1 and
              l.id_libreria=p_idlibreria and c.id_categoria=p_idcategoria
        order by t.id_documento, i.numero_documento desc, i.version desc;

    return cursor_folio;
    close cursor_folio;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
    procedimiento para agregar el orden de la
    foliatura del expediente
  **/
  CREATE OR REPLACE FUNCTION p_agregar_foliaturas(p_idinfodocumento in integer, p_iddocumento in integer, p_idexpediente in varchar, p_pagina in integer) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    insert into foliatura
      (id_infodocumento, id_documento, id_expediente, pagina)
    values
      (p_idinfodocumento, p_iddocumento, p_idexpediente, p_pagina);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;



  /**
     funcion que busca la configuracion
     del gestor documental
  **/
  CREATE OR REPLACE FUNCTION f_buscar_configuracion() returns refcursor 
    LANGUAGE plpgsql
    AS $_$
  DECLARE
    cursor_conf refcursor;
  begin

    open cursor_conf for
      select * from configuracion;

    return cursor_conf;
    close cursor_conf;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;

  /**
     procedimiento que actualiza la
     nueva configuracion
  **/
  CREATE OR REPLACE FUNCTION p_agregar_configuracion(p_nombreservidor in varchar, p_nombrebasedato in varchar,
                                    p_puertobasedato in integer, p_usuariobasedato in varchar,
                                    p_passbasedato in varchar, p_calidad in char,
                                    p_foliatura in char, p_ficha in char,
                                    p_fabrica in char, p_elimina in char) 
   RETURNS void
    LANGUAGE plpgsql
    AS $_$

  begin

    update configuracion
       set calidad = p_calidad,
           foliatura = p_foliatura,
           server_name = p_nombreservidor,
           database_name = p_nombrebasedato,
           port = p_puertobasedato,
           userbd = p_usuariobasedato,
           password = p_passbasedato,
           ficha = p_ficha,
           fabrica = p_fabrica,
           elimina = p_elimina
     where id_configuracion=1;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Fall� la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$_$;
  
