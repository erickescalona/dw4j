/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.menu;

import com.develcom.autentica.Configuracion;
import com.develcom.dao.Expediente;
import com.develcom.paginas.reportes.tools.ConstruyeConsultas;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.util.List;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author develcom
 */
public class MenuPrincipal extends SelectorComposer<Component> {

    @Wire
    private Menu mnExpediente;
    @Wire
    private Menuitem mnDigitalizar;
    @Wire
    private Menu mnCalidad;
    @Wire
    private Menuitem mnCalidadDoc;
    @Wire
    private Menuitem mnActualizar;
    @Wire
    private Menuitem mnEliminar;
    @Wire
    private Menuitem mnConsultar;

    @Wire
    private Menu mnReporte;
    @Wire
    private Menuitem mnVencerse;
    @Wire
    private Menuitem mnVencidos;
    @Wire
    private Menuitem mnRechazados;
    @Wire
    private Menuitem mnPendAprobar;
    @Wire
    private Menuitem mnCausaRechazo;
    @Wire
    private Menuitem mnDigiRechAproPend;
    @Wire
    private Menuitem mnCrecimiento;
    @Wire
    private Menuitem mnIndexados;
    @Wire
    private Menuitem mnCantidadDoc;
    @Wire
    private Menuitem mnDetalleDoc;
    @Wire
    private Menuitem mnEstatusExp;
    @Wire
    private Menuitem mnEliminadoDoc;

    @Wire
    private Menu mnAdministrar;
    @Wire
    private Menuitem mnUsuarios;
    @Wire
    private Menuitem mnLibreria;
    @Wire
    private Menuitem mnCategoria;
    @Wire
    private Menuitem mnSubCategoria;
    @Wire
    private Menuitem mnTipoDocumento;
    @Wire
    private Menuitem mnIndices;
    @Wire
    private Menuitem mnFotoFicha;
    @Wire
    private Menuitem mnDatosAdicionales;
    @Wire
    private Menuitem mnComboIndice;
    @Wire
    private Menuitem mnComboDatosAd;

    @Wire
    private Menu mnMantenimiento;
    @Wire
    private Menuitem mnGestor;

    private static final long serialVersionUID = 4453437185863200875L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(MenuPrincipal.class);
    private Session session;
    private Configuracion configuracion;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {
        session = herramientas.crearSesion();
        List<String> rolUsuario;

        try {

            if (session != null) {

                ocultarMenues();

                expediente = (Expediente) session.getAttribute("expediente");
                configuracion = (Configuracion) session.getAttribute("configuracion");
                rolUsuario = expediente.getRolUsuario();

                for (String rol : rolUsuario) {
                    if (rol.equalsIgnoreCase(Constantes.CONFIGURADOR)) {
                        mnAdministrar.setVisible(true);
                    } else if (rol.equalsIgnoreCase(Constantes.DIGITALIZADOR)) {
                        mnExpediente.setVisible(true);
                        mnDigitalizar.setVisible(true);
                    } else if (rol.equalsIgnoreCase(Constantes.CONSULTAR)) {
                        mnExpediente.setVisible(true);
                        mnConsultar.setVisible(true);
                    } else if (rol.equalsIgnoreCase(Constantes.APROBADOR)) {
                        mnExpediente.setVisible(true);
                        if (!configuracion.isCalidadActivo()) {
                            mnCalidad.setVisible(false);
                        } else {
                            mnCalidad.setVisible(true);
                        }
                    } else if (rol.equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
                        mnAdministrar.setVisible(true);
                    } else if (rol.equalsIgnoreCase(Constantes.REPORTES)) {
                        mnReporte.setVisible(true);
                        if (!configuracion.isCalidadActivo()) {
                            mnRechazados.setVisible(false);
                            mnPendAprobar.setVisible(false);
                            mnCausaRechazo.setVisible(false);
                            mnDigiRechAproPend.setVisible(false);
                        }
                    } else if (rol.equalsIgnoreCase(Constantes.MANTENIMIENTO)) {
                        mnMantenimiento.setVisible(true);
                    } else if (rol.equalsIgnoreCase(Constantes.ELIMINAR)) {
                        mnEliminar.setVisible(true);
                    }
                }
            }

        } catch (Exception e) {
            traza.trace("problemas con el menu principal", Level.ERROR, e);
            herramientas.error("Problemas con el Menu Principal", e);
        }
    }

    private void ocultarMenues() {

        mnExpediente.setVisible(false);
        mnReporte.setVisible(false);
        mnAdministrar.setVisible(false);
        mnMantenimiento.setVisible(false);

        mnDigitalizar.setVisible(false);
        mnCalidad.setVisible(false);
        mnConsultar.setVisible(false);
        mnEliminar.setVisible(false);

    }

    @Listen("onClick = #mnDigitalizar")
    public void getMnDigitalizar() {
        Constantes.ACCION = "CAPTURAR";
        Constantes.ROL = "DIGITALIZADOR";
        mostrarLibreria();
    }

    @Listen("onClick = #mnCalidadDoc")
    public void getMnCalidadDoc() {
        Constantes.ACCION = "APROBAR";
        Constantes.ROL = "APROBADOR";
        mostrarLibreria();
    }

    @Listen("onClick = #mnActualizar")
    public void getMnActualizar() {
        Constantes.ACCION = "ACTUALIZAR_INDICES";
        Constantes.ROL = "APROBADOR";
        mostrarLibreria();
    }

    @Listen("onClick = #mnEliminar")
    public void getMnEliminar() {

        if (configuracion.isElimina()) {

            Constantes.ACCION = "ELIMINAR";
            Constantes.ROL = "ELIMINAR";
            mostrarLibreria();
        } else {
            Messagebox.show("Modulo de Eliminacion de Documentos no esta Activo", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @Listen("onClick = #mnConsultar")
    public void getMnConsultar() {
        Constantes.ACCION = "CONSULTAR";
        Constantes.ROL = "CONSULTAR";
        mostrarLibreria();
    }

    @Listen("onClick = #mnVencerse")
    public void getMnVencerse() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.DOCUMENTO_A_VENCERSE);
        expediente.setNombreReporte("Reporte " + mnVencerse.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnVencidos")
    public void getMnVencidos() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.DOCUMENTO_VENCIDOS);
        expediente.setNombreReporte("Reporte " + mnVencidos.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnRechazados")
    public void getMnRechazados() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.DOCUMENTOS_RECHAZADOS);
        expediente.setNombreReporte("Reporte " + mnRechazados.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnPendAprobar")
    public void getMnPendAprobar() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.DOCUMENTO_PENDIENTE_POR_APROBAR);
        expediente.setNombreReporte("Reporte " + mnPendAprobar.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnCausaRechazo")
    public void getMnCausaRechazo() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.CAUSA_DE_RECHAZO);
        expediente.setNombreReporte("Reporte " + mnCausaRechazo.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnDigiRechAproPend")
    public void getMnDigiRechAproPend() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.DOCUMENTO_INDEXADOS_RECHAZADO_APROBADO_PENDIENTE);
        expediente.setNombreReporte("Reporte " + mnDigiRechAproPend.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnCrecimiento")
    public void getMnCrecimiento() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.CRECIMIENTO_INTERMENSUAL_DE_DOCUMENTOS);
        expediente.setNombreReporte("Reporte " + mnCrecimiento.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnCantidadDoc")
    public void getMnCantidadDoc() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.CANTIDAD_DOCUMENTO_INDEXADOS_POR_INDEXADOR);
        expediente.setNombreReporte("Reporte " + mnCantidadDoc.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnDetalleDoc")
    public void getMnDetalleDoc() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.DETALLE_DOCUMENTO_INDEXADOS_POR_INDEXADOR);
        expediente.setNombreReporte("Reporte " + mnDetalleDoc.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reporteDetalle.zul");
    }

    @Listen("onClick = #mnEstatusExp")
    public void getMnEstatusExp() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.EXPEDIENTE_ESTATUS);
        expediente.setNombreReporte("Reporte " + mnEstatusExp.getLabel());
        session.setAttribute("expediente", expediente);
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnEliminadoDoc")
    public void getMnEliminadoDoc() {
        Constantes.ACCION = "REPORTES";
        Constantes.ROL = "REPORTES";
        expediente.setReporte(ConstruyeConsultas.DOCUMENTO_ELIMINADO);
        session.setAttribute("expediente", expediente);
        expediente.setNombreReporte("Reporte " + mnEliminadoDoc.getLabel());
        herramientas.navegarPagina("reportes.zul");
    }

    @Listen("onClick = #mnUsuarios")
    public void getMnUsuarios() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administracionUsuarios.zul");
    }

    @Listen("onClick = #mnLibreria")
    public void getMnLibreria() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarLibrerias.zul");
    }

    @Listen("onClick = #mnCategoria")
    public void getMnCategoria() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarCategorias.zul");
    }

    @Listen("onClick = #mnSubCategoria")
    public void getMnSubCategoria() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarSubCategorias.zul");
    }

    @Listen("onClick = #mnTipoDocumento")
    public void getMnTipoDocumento() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarTipoDocumento.zul");
    }

    @Listen("onClick = #mnIndices")
    public void getMnIndices() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarIndices.zul");
    }

    @Listen("onClick = #mnFotoFicha")
    public void getMnFotoFicha() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarFicha.zul");
    }

    @Listen("onClick = #mnDatosAdicionales")
    public void getMnDatosAdicionales() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarDatosAdicionales.zul");
    }

    @Listen("onClick = #mnComboIndice")
    public void getMnComboIndice() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarComboIndice.zul");
    }

    @Listen("onClick = #mnComboDatosAd")
    public void getMnComboDatosAd() {
        Constantes.ACCION = "ADMINISTRAR";
        Constantes.ROL = "ADMINISTRADOR";
        herramientas.navegarPagina("administrarComboDatoAdicional.zul");
    }

    @Listen("onClick = #mnGestor")
    public void getMnGestor() {
        Constantes.ACCION = "MANTENIMIENTO";
        Constantes.ROL = "MANTENIMIENTO";
        herramientas.navegarPagina("mantenimiento.zul");
    }

    private void mostrarLibreria() {
        herramientas.navegarPagina("libreria.zul");
//        Window window = (Window) Executions.createComponents("/libreria.zul", null, null);
//        window.doModal();
    }

    @Listen("onClick = #mnSalir")
    public void salir() {

        try {
            System.runFinalization();
            System.gc();
            if (session != null) {
                herramientas.cerrarSesion();
                herramientas.navegarPagina("index.zul");
            } else {
                herramientas.navegarPagina("index.zul");
            }
        } catch (Exception ex) {
            traza.trace("error al salir", Level.ERROR, ex);
        }
    }
}
