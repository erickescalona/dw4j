--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-09-30 13:48:47

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 33250)
-- Name: subcategoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subcategoria (
    id_subcategoria integer DEFAULT nextval('sq_subcategroria'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    subcategoria character varying(200) NOT NULL,
    id_estatus character varying(20) NOT NULL
);


ALTER TABLE public.subcategoria OWNER TO postgres;

--
-- TOC entry 2963 (class 0 OID 33250)
-- Dependencies: 187
-- Data for Name: subcategoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (1, 1, 'SubCategoria1', '1');
INSERT INTO subcategoria (id_subcategoria, id_categoria, subcategoria, id_estatus) VALUES (2, 1, 'SubCategoria2', '1');


--
-- TOC entry 2855 (class 2606 OID 33255)
-- Name: subcategoria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subcategoria
    ADD CONSTRAINT subcategoria_pk PRIMARY KEY (id_subcategoria);


-- Completed on 2014-09-30 13:48:48

--
-- PostgreSQL database dump complete
--

