/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.Libreria;
import com.develcom.administracion.Perfil;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.trazas.Traza;
import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;

/**
 *
 * @author develcom
 */
public class AdministraCategoria extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibrerias;

    @Wire
    private Listbox lstCategoriaExistentes;

    @Wire
    private Textbox txtCategoria;

    @Wire
    private Radio rdActivo;

    @Wire
    private Radio rdInactivo;

    @Wire
    private Listbox lstCategoriaNuevas;

    private static final long serialVersionUID = 6596018296138461422L;

    private List<Categoria> listaCategorias;
    private List<Libreria> listaLibrerias;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraCategoria.class);
    private Session session;
    private ToolsFiles toolsFile = new ToolsFiles();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }

    }

    private void llenarLibreria() {

        Comboitem item;

        try {

            cmbLibrerias.getItems().clear();

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {
                    if (lib.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(lib.getIdLibreria());
                        item.setLabel(lib.getDescripcion());
                        item.setParent(cmbLibrerias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Librerias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onSelect = #cmbLibrerias")
    public void llenarTablaCategoriaExistentes() {

        int idLibreria;
        Listitem item;
        Listcell cell;

        try {

            lstCategoriaExistentes.getItems().clear();

            idLibreria = cmbLibrerias.getSelectedItem().getValue();
            listaCategorias = new AdministracionBusqueda().buscarCategoria("", idLibreria, 0);

            if (!listaCategorias.isEmpty()) {
                for (Categoria cat : listaCategorias) {
                    item = new Listitem();

                    cell = new Listcell(cat.getCategoria());
                    item.appendChild(cell);

                    cell = new Listcell(cat.getEstatus());
                    item.appendChild(cell);

                    item.setParent(lstCategoriaExistentes);
                }
            } else {
                herramientas.warn("La Libreria no posee Categorias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onClick = #btnAgregar")
    public void agregarCategoria() {

        String libreria, estatus = "", categoria;
        boolean banLista = true, banTabla = true;
        List<Listitem> categoriaNuevas = new ArrayList<>();
        Listitem item;
        Listcell cell;

        try {

            libreria = cmbLibrerias.getSelectedItem().getLabel();

            if (rdActivo.isSelected()) {
                estatus = rdActivo.getLabel();
            } else if (rdInactivo.isSelected()) {
                estatus = rdInactivo.getLabel();
            }

            if (!libreria.equalsIgnoreCase("")) {

                categoria = txtCategoria.getText();

                if (!categoria.equalsIgnoreCase("")) {

                    for (Categoria cat : listaCategorias) {
                        String ca = cat.getCategoria();
                        if (ca.equalsIgnoreCase(categoria)) {
                            banLista = false;
                            break;
                        }
                    }

                    categoriaNuevas = lstCategoriaNuevas.getItems();

                    for (Listitem libex : categoriaNuevas) {
                        List<Listcell> cells = libex.getChildren();
                        String lib = cells.get(0).getLabel();
                        if (lib.equalsIgnoreCase(libreria)) {
                            banTabla = false;
                            break;
                        }
                    }

                    if (banTabla) {
                        if (banLista) {

                            item = new Listitem();

                            cell = new Listcell(libreria);
                            item.appendChild(cell);

                            cell = new Listcell(categoria);
                            item.appendChild(cell);

                            cell = new Listcell(estatus);
                            item.appendChild(cell);

                            item.setParent(lstCategoriaNuevas);

                            txtCategoria.setValue("");
                            txtCategoria.setDisabled(false);

                        } else {

                            int n = Messagebox.show("Ya la categoria se encuentra en la lista de existentes \n ¿desea modificarlo?",
                                    "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                                    new org.zkoss.zk.ui.event.EventListener() {

                                        @Override
                                        public void onEvent(Event evt) throws Exception {
                                        }

                                    });

                            if (n == Messagebox.YES) {

                                item = new Listitem();

                                cell = new Listcell(libreria);
                                item.appendChild(cell);

                                cell = new Listcell(categoria);
                                item.appendChild(cell);

                                cell = new Listcell(estatus);
                                item.appendChild(cell);

                                item.setParent(lstCategoriaNuevas);

                                txtCategoria.setValue("");
                                txtCategoria.setDisabled(false);

                            } else {
                                txtCategoria.setValue("");
                                txtCategoria.setDisabled(false);
                            }
                        }
                    } else {
                        herramientas.warn("Ya la Categoria se encuentra en la lista");
                    }

                } else {
                    herramientas.warn("Debe colocar un nombre de la categoria");
                }
            } else {
                herramientas.warn("Debe seleccionar una libreria");
            }
        } catch (Exception e) {
            traza.trace("problemas al agregar la libreria", Level.ERROR, e);
            herramientas.error("Problemas al agregar la Libreria", e);
        }
    }

    @Listen("onClick = #btnCambiarEstatus")
    public void cambiarEstatus() {

        List<Listcell> cells = new ArrayList<>();
        Listitem item;

        item = lstCategoriaExistentes.getSelectedItem();
        cells = item.getChildren();

        txtCategoria.setValue(cells.get(0).getLabel());
        txtCategoria.setDisabled(true);
    }

    @Listen("onClick = #btnGuardar")
    public void guardarCategorias() {

        List<Perfil> categorias = new ArrayList<>();
        Perfil categoria;
        Categoria cat;
        Libreria lib;
        List<Listitem> items;
        List<Listcell> cells;
        List categ = new ArrayList();
        boolean resp, ban = true;;
        int contCat = 0;

        try {

            int n = Messagebox.show("Seguro que desea guardar las Categorias",
                    "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event evt) throws Exception {
                        }

                    });

            if (n == Messagebox.YES) {

                items = lstCategoriaNuevas.getItems();

                if (!items.isEmpty()) {
                    for (Listitem item : items) {
                        cells = item.getChildren();

                        categoria = new Perfil();
                        cat = new Categoria();
                        lib = new Libreria();

                        lib.setDescripcion(cells.get(0).getLabel());
                        lib.setEstatus("1");
                        for (Libreria li : listaLibrerias) {
                            if (li.getDescripcion().equalsIgnoreCase(lib.getDescripcion())) {
                                lib.setIdLibreria(li.getIdLibreria());
                                break;
                            }
                        }

                        cat.setCategoria(cells.get(1).getLabel());

                        if (cells.get(2).getLabel().equalsIgnoreCase(rdActivo.getLabel())) {
                            cat.setEstatus("1");
                        } else if (cells.get(2).getLabel().equalsIgnoreCase(rdInactivo.getLabel())) {
                            cat.setEstatus("2");
                        }

                        for (Categoria cate : listaCategorias) {
                            if (cat.getCategoria().equalsIgnoreCase(cate.getCategoria())) {
                                cat.setIdCategoria(cate.getIdCategoria());
                            }
                        }

                        if (verificarCategorias(cat.getCategoria(), cat.getEstatus())) {
                            contCat++;
                            categ.add(cat.getCategoria());
                        }

                        categoria.setLibreria(lib);
                        categoria.setCategoria(cat);

                        categorias.add(categoria);
                    }

                    if (contCat == 0) {

                        resp = new AdministracionAgregar().insertarCategoria(categorias);

                        if (ban) {
                            if (resp) {
                                herramientas.info("Categorias guardadas con exito");
                            } else {
                                herramientas.warn("Problemas al guardar las Categorias");
                            }
                        }
                        limpiarComponentes();
                    } else {
                        herramientas.warn("algunas categorias ya existen \n" + categ + "\nno se procede a guardar");
                        ban = false;
                    }
                } else {
                    herramientas.info("La tabla de Categorias esta vacia");
                }

            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }

    }

    /**
     * Comprueba si existe la categoria en la base de datos
     *
     * @param categoria La categoria
     * @return Verdadero si existe, falso en caso contrario
     */
    private boolean verificarCategorias(String categoria, String estatus) {
        boolean resp = false;
        List<Categoria> categorias;

        try {

            if (estatus.equalsIgnoreCase("1")) {
                estatus = Constantes.ACTIVO;
            } else {
                estatus = Constantes.INACTIVO;
            }

            categorias = new AdministracionBusqueda().buscarCategoria(categoria, 0, 0);

            for (Categoria c : categorias) {
                String cate = c.getCategoria();

                if (cate.equalsIgnoreCase(categoria)) {
                    resp = true;
                    for (Categoria cat : listaCategorias) {
                        String categ = cat.getCategoria();

                        if (categ.equalsIgnoreCase(categoria)) {
                            if (!cat.getEstatus().equalsIgnoreCase(estatus)) {
                                resp = false;
                            }
                        }
                    }
                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
        return resp;
    }

    private void limpiarComponentes() {
        txtCategoria.setText("");
        rdActivo.setSelected(false);
        rdInactivo.setSelected(false);
        lstCategoriaExistentes.getItems().clear();
        lstCategoriaNuevas.getItems().clear();
        cmbLibrerias.setText("");
        llenarLibreria();
    }
}
