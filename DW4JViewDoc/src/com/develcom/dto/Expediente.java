/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.develcom.dto;

/**
 *
 * @author develcom
 */
public class Expediente {


    private String idExpediente;

    private int idLibreria;
    private String libreria;

    private int idCategoria;
    private String categoria;

    private int idSubCategoria;
    private String subCategoria;   
    private int totalSubCategoria;

    private int idTipoDocumento;
    private String tipoDocumento;

    private int idInfoDocumento;
    private String nombreArchivo;


    private String subcategorias;
    private String id_subcategorias;

    public String getId_subcategorias() {
        return id_subcategorias;
    }

    public void setId_subcategorias(String id_subcategorias) {
        this.id_subcategorias = id_subcategorias;
    }

    public String getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategorias(String subcategorias) {
        this.subcategorias = subcategorias;
    }
    

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(String idExpediente) {
        this.idExpediente = idExpediente;
    }

    public int getIdInfoDocumento() {
        return idInfoDocumento;
    }

    public void setIdInfoDocumento(int idInfoDocumento) {
        this.idInfoDocumento = idInfoDocumento;
    }

    public int getIdLibreria() {
        return idLibreria;
    }

    public void setIdLibreria(int idLibreria) {
        this.idLibreria = idLibreria;
    }

    public int getIdSubCategoria() {
        return idSubCategoria;
    }

    public void setIdSubCategoria(int idSubCategoria) {
        this.idSubCategoria = idSubCategoria;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getLibreria() {
        return libreria;
    }

    public void setLibreria(String libreria) {
        this.libreria = libreria;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(String subCategoria) {
        this.subCategoria = subCategoria;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getTotalSubCategoria() {
        return totalSubCategoria;
    }

    public void setTotalSubCategoria(int totalSubCategoria) {
        this.totalSubCategoria = totalSubCategoria;
    }
}
