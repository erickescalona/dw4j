/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.captura.applet;

import java.awt.HeadlessException;
import javax.swing.JApplet;
import uk.co.mmscomputing.device.sane.applet.SaneAppletExample;
import uk.co.mmscomputing.device.twain.applet.TwainAppletExample;

/**
 *
 * @author develcom
 */
public class Principal extends JApplet {

    private static String OS = System.getProperty("os.name").toLowerCase();
    private static String OSArch = System.getProperty("os.arch").toLowerCase();
    private static String OSVersion = System.getProperty("os.version").toLowerCase();
    private static final long serialVersionUID = -7735691172143683326L;

    public Principal() throws HeadlessException {
    //public static void main(String[] args) {
    

        System.out.print("Sistema operativo: ");
        System.out.println(OS);

        if (isWindows()) {
            System.out.println("Es un Windows");
            TwainAppletExample tae = new TwainAppletExample("AppDW4JWeb", null);
        } else if (isMac()) {
            System.out.println("Es un Mac");
        } else if (isUnix()) {
            System.out.println("Es un Unix/Linux");
            SaneAppletExample sae = new SaneAppletExample("AppDW4JWeb", null);
        } else if (isSolaris()) {
            System.out.println("Es Solaris");
        } else {
            System.out.println("Sistema operativo no reconocido!!");
        }
        System.out.println("Version: " + OSVersion);
        System.out.println("Aquitectura: " + OSArch);
    }

    public static boolean isWindows() {
        return (OS.contains("win"));
    }

    public static boolean isMac() {
        return (OS.contains("mac"));
    }

    public static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
    }

    public static boolean isSolaris() {
        return (OS.contains("sunos"));
    }
}
