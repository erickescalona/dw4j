/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.Libreria;
import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import ve.com.develcom.administracion.AdministracionBusqueda;
import ve.com.develcom.administracion.AdministracionModifica;

/**
 *
 * @author develcom
 */
public class AdministraFicha extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibrerias;

    @Wire
    private Combobox cmbCategorias;

    @Wire
    private Combobox cmbSubCategorias;

    @Wire
    private Combobox cmbTipoDocumento;

    private static final long serialVersionUID = -7267038525103674231L;
    private List<Libreria> listaLibrerias;
    private List<Categoria> listaCategorias;
    private List<SubCategoria> listaSubCategorias;
    private List<TipoDocumento> listaTipoDocumentos;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraFicha.class);
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }
    }

    private void llenarLibreria() {

        Comboitem item;

        try {

            cmbLibrerias.getItems().clear();

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            item = new Comboitem();
            item.setValue("");
            item.setLabel("");
            item.setParent(cmbLibrerias);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {
                    if (lib.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(lib.getIdLibreria());
                        item.setLabel(lib.getDescripcion());
                        item.setParent(cmbLibrerias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Librerias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onSelect = #cmbLibrerias")
    public void llenarCategoria() {

        Comboitem item;
        int idLibreria;

        try {

            cmbCategorias.setText("");
            cmbSubCategorias.setText("");
            cmbTipoDocumento.setText("");
            cmbCategorias.getItems().clear();

            idLibreria = cmbLibrerias.getSelectedItem().getValue();
            listaCategorias = new AdministracionBusqueda().buscarCategoria("", idLibreria, 0);

            item = new Comboitem();
            item.setValue("");
            item.setLabel("");
            item.setParent(cmbCategorias);

            if (!listaCategorias.isEmpty()) {
                for (Categoria cat : listaCategorias) {
                    if (cat.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(cat.getIdCategoria());
                        item.setLabel(cat.getCategoria());
                        item.setParent(cmbCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Categorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onSelect = #cmbCategorias")
    public void llenarSubCategoria() {

        Comboitem item;
        int idCategoria;

        try {

            cmbSubCategorias.setText("");
            cmbTipoDocumento.setText("");
            cmbSubCategorias.getItems().clear();

            idCategoria = cmbCategorias.getSelectedItem().getValue();
            listaSubCategorias = new AdministracionBusqueda().buscarSubCategoria("", idCategoria, 0);

            if (!listaSubCategorias.isEmpty()) {
                for (SubCategoria subCat : listaSubCategorias) {
                    if (subCat.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(subCat.getIdSubCategoria());
                        item.setLabel(subCat.getSubCategoria());
                        item.setParent(cmbSubCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las SubCategorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las subcategorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las subcategorias", ex);
        }
    }

    @Listen("onSelect = #cmbSubCategorias")
    public void llenarTipoDocumento() {

        Comboitem item;
        int idcategoria, idsubcategoria;

        try {

            cmbTipoDocumento.setText("");
            cmbTipoDocumento.getItems().clear();

            idcategoria = cmbCategorias.getSelectedItem().getValue();
            idsubcategoria = cmbSubCategorias.getSelectedItem().getValue();
            listaTipoDocumentos = new AdministracionBusqueda().buscarTipoDocumento("", idcategoria, idsubcategoria);

            if (!listaTipoDocumentos.isEmpty()) {
                for (TipoDocumento td : listaTipoDocumentos) {
                    if (td.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(td.getIdTipoDocumento());
                        item.setLabel(td.getTipoDocumento());
                        item.setParent(cmbTipoDocumento);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar los Tipos de Documentos (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando los tipo de documento", Level.ERROR, ex);
            herramientas.error("Problemas buscando los Tipos de Documentos", ex);
        }
    }

    @Listen("onClick = #btnGuardar")
    public void configurarFicha() {

        String nombre;
        boolean resp;
        int idTipoDoc = 0;

        try {

            nombre = cmbTipoDocumento.getSelectedItem().getLabel();
            idTipoDoc = cmbTipoDocumento.getSelectedItem().getValue();

            if (!nombre.equalsIgnoreCase("")) {

                resp = new AdministracionModifica().agregaTipoDocumentoFoto(idTipoDoc);

                if (resp) {
                    herramientas.info("Datos Adicionales agregados con exito");
                } else {
                    herramientas.warn("Problemas al agregar los Datos Adicionales");
                }
                limpiarComponentes();
            }
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al configurar la ficha", Level.ERROR, ex);
            herramientas.error("Problemas al configurar la ficha", ex);
        }
    }

    private void limpiarComponentes() {

        cmbLibrerias.setText("");
        cmbCategorias.setText("");
        cmbSubCategorias.setText("");
        cmbTipoDocumento.setText("");
        llenarLibreria();
    }

}
