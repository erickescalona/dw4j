/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.calidad;

import com.develcom.dao.Expediente;
import com.develcom.dao.ManejoSesion;
import com.develcom.elimina.Categoria;
import com.develcom.elimina.EliminaDocumento_Type;
import com.develcom.elimina.InfoDocumento;
import com.develcom.elimina.Libreria;
import com.develcom.elimina.SubCategoria;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox;
import ve.com.develcom.elimina.QuitaDocumento;

/**
 *
 * @author develcom
 */
public class RechazaDocumento extends SelectorComposer<Component> {

    @Wire
    private Window winRechaza;

    @Wire
    private Combobox cbmCausa;

    @Wire
    private Textbox txtMotivo;

    @Wire
    private Caption capMotivo;

    @Wire
    private Label lblCausa;

    private static final long serialVersionUID = 5073534393262569715L;
    private InfoDocumento infoDocumento;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(RechazaDocumento.class);
    private Session session;
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            expediente = (Expediente) session.getAttribute("expediente");

            llenarComboCausas();

            if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {
                capMotivo.setLabel("Motivo para rechazar");
                lblCausa.setValue("Causa para rechazar");
                winRechaza.setTitle("Rechazar Documentos");
            } else if (Constantes.ACCION.equalsIgnoreCase("ELIMINAR")) {
                capMotivo.setLabel("Motivo de la eliminación");
                lblCausa.setValue("Causa de la eliminación");
                winRechaza.setTitle("Eliminar Documentos");
            }

        }

    }

    private void llenarComboCausas() {

        List<String> causas;
        Comboitem item;
        try {
            causas = new ve.com.develcom.aprueba.CalidadDocumento().encontrarCausasRechazo();

            for (String causa : causas) {
                item = new Comboitem();
                item.setValue(causa);
                item.setLabel(causa);
                item.setParent(cbmCausa);
            }

        } catch (SOAPException ex) {
            traza.trace("error soap en el webservice", Level.ERROR, ex);
        } catch (SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }

    }

    @Listen("onClick = #btnAceptar")
    public void aceptar() {

        String causa, motivo, usuario, nombre, datos;
        int n = 0;
        boolean resultado;

        try {

            if (Constantes.ACCION.equalsIgnoreCase("ELIMINAR")) {
                n = Messagebox.show("Seguro que desea eliminar el documento " + expediente.getTipoDocumento()
                        + " version " + expediente.getVersionDocumento(),
                        "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                        new EventListener() {

                            @Override
                            public void onEvent(Event evt) throws Exception {
                            }
                        });
            } else {
                n = Messagebox.show("Seguro que desea rechazar el documento " + expediente.getTipoDocumento()
                        + " version " + expediente.getVersionDocumento()
                        + " \n(" + expediente.getIdInfoDocumento() + ")",
                        "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                        new EventListener() {

                            @Override
                            public void onEvent(Event evt) throws Exception {
                            }
                        });
            }

            if (n == Messagebox.YES) {

                causa = cbmCausa.getSelectedItem().getValue();
                motivo = txtMotivo.getValue();
                usuario = session.getAttribute("login").toString();

                if (!causa.equalsIgnoreCase("")) {
                    if (!motivo.equalsIgnoreCase("")) {

                        if (Constantes.ACCION.equalsIgnoreCase("ELIMINAR")) {

                            Libreria libreria = new Libreria();
                            Categoria categoria = new Categoria();
                            SubCategoria subCategoria = new SubCategoria();
                            EliminaDocumento_Type ed = new EliminaDocumento_Type();

                            libreria.setIdLibreria(ManejoSesion.getExpediente().getIdLibreria());
                            libreria.setDescripcion(ManejoSesion.getExpediente().getLibreria());
                            ed.setLibreria(libreria);

                            categoria.setIdCategoria(ManejoSesion.getExpediente().getIdCategoria());
                            categoria.setCategoria(ManejoSesion.getExpediente().getCategoria());
                            ed.setCategoria(categoria);

                            subCategoria.setIdSubCategoria(ManejoSesion.getExpediente().getIdSubCategoria());
                            subCategoria.setSubCategoria(ManejoSesion.getExpediente().getSubCategoria());
                            ed.setSubCategoria(subCategoria);

                            ed.setCausaElimino(causa);
                            ed.setMotivoElimino(motivo);
                            ed.setIdExpediente(ManejoSesion.getExpediente().getIdExpediente());
                            ed.setUsuarioElimino(ManejoSesion.getLogin());
                            ed.setInfoDocumento(infoDocumento);

                            resultado = new QuitaDocumento().eliminarTipoDocumento(ed);
                            
                            expediente.setResultado(resultado);
                            session.setAttribute("expediente", expediente);

                            if (resultado) {
                                herramientas.info("Documento eliminado con exito");
                                cerrar();
                            } else {
                                herramientas.warn("Problemas al eliminar el documento");
                            }

                        } else {

                            resultado = new ve.com.develcom.aprueba.CalidadDocumento().rechazarDoc(expediente.getIdInfoDocumento(), usuario, causa, motivo);
                            
                            expediente.setResultado(resultado);
                            session.setAttribute("expediente", expediente);
                            
                            if (resultado) {
                                herramientas.info("Documento rechazado con exito");
                                cerrar();
                            } else {
                                herramientas.warn("Problemas al rechazar el documento");
                            }
                        }

                    } else {
                        herramientas.warn("Debe agregar un motivo");
                    }
                } else {
                    herramientas.warn("Debe elejir una causa");
                }

            }

        } catch (SOAPException | SOAPFaultException | WrongValueException ex) {
            traza.trace("error al rechazar/eliminar el documento", Level.ERROR, ex);
            herramientas.error("Problemas al rechazar/eliminar el documento", ex);
        }
    }

    @Listen("onClick = #btnCerrar")
    public void cerrar() {
        winRechaza.setVisible(false);
        winRechaza.detach();
    }
}
