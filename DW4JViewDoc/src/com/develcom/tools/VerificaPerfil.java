/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.develcom.tools;

import com.develcom.autentica.Perfil;
import com.develcom.dto.ManejoSesion;
import com.develcom.log.Traza;
import com.develcom.verdocumento.VerDocumento;
import com.develcom.verdocumento.VerImagenes;
import java.util.List;
import javax.xml.soap.SOAPException;
import org.apache.log4j.Level;
import ve.com.develcom.sesion.IniciaSesion;

/**
 *
 * @author develcom
 */
public class VerificaPerfil {

    private VerDocumento verDocumento;
    private VerImagenes verImagenes;
    private Traza traza = new Traza(VerificaPerfil.class);

    public VerificaPerfil(VerDocumento verDocumento) {
        this.verDocumento = verDocumento;
    }

    public VerificaPerfil(VerImagenes verImagenes) {
        this.verImagenes = verImagenes;
    }




    public void verificarPerfil(){
        try {

            String login = ManejoSesion.getLogin();

            List<Perfil> perfiles = new IniciaSesion().buscarLibCatPerfil(login, Constantes.IMPRIMIR);

            int idCat = ManejoSesion.getExpediente().getIdCategoria();

            traza.trace("verificando el perfil del usuario " + login + " para imprimir " + " para el idCategoira " + idCat + " - " + ManejoSesion.getExpediente().getCategoria(), Level.INFO);

            for (Perfil perfil : perfiles) {
                int idCate = perfil.getCategoria().getIdCategoria();
                traza.trace("idCategoria buscada " + idCate + " - " + perfil.getCategoria().getCategoria(), Level.INFO);
                if (idCate == idCat) {
                    traza.trace("el usuario " + login + " puede imprimir", Level.INFO);
                    
                    if(verDocumento!=null){
                        verDocumento.getJbImprimir().setEnabled(true);
                    }else if(verImagenes!=null){
                        verImagenes.getJbImprimir().setEnabled(true);
                    }

                    break;
                }
            }

        } catch (SOAPException ex) {
            traza.trace("problemas al buscar las libreria y categorias segun perfil", Level.ERROR, ex);
        }
    }




}
