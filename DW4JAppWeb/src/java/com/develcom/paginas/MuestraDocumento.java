/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas;

import com.develcom.dao.Expediente;
import com.develcom.documento.Bufer;
import com.develcom.documento.DatoAdicional;
import com.develcom.documento.InfoDocumento;
import com.develcom.excepcion.DW4JDesktopExcepcion;
import com.develcom.expediente.Indice;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.cryto.EncriptaDesencripta;
import com.develcom.tools.trazas.Traza;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import ve.com.develcom.archivo.GestionArchivos;
import ve.com.develcom.expediente.BuscaIndice;
import ve.com.develcom.expediente.GestionDocumentos;

/**
 *
 * @author develcom
 */
public class MuestraDocumento extends SelectorComposer<Component> {

    @Wire
    private Groupbox grpDatosAdicionales;

    @Wire
    private Listbox datosAdicionales;

    @Wire
    private Listbox indices;

    @Wire
    private Listbox informacion;

    @Wire
    private Iframe documento;

    @Wire
    private Window winDocumento;

    @Wire
    private Caption capPanel;

    @Wire
    private Combobox cmbVersion;

    @Wire
    private Label fechaVencimiento;

    @Wire
    private Button btnAbrir;

    @Wire
    private Button btnGuardar;

    private String accion;
    private InfoDocumento infoDocumento;
    private int cantidadPaginas;
    private List<InfoDocumento> infoDocumentos;
    private static final long serialVersionUID = -6173880695156134950L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(MuestraDocumento.class);
    private Session session;
    private ToolsFiles toolsFile = new ToolsFiles();
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {
        File archivo;
        session = herramientas.crearSesion();

        if (session != null) {

            expediente = (Expediente) session.getAttribute("expediente");
            accion = expediente.getAccion();
            infoDocumento = (InfoDocumento) session.getAttribute("infoDocumento");

            try {
                if (accion == null) {
                    accion = "";
                }
            } catch (NullPointerException e) {
                accion = "";
            }

            btnAbrir.setVisible(false);
            cmbVersion.setVisible(false);
            fechaVencimiento.setVisible(false);

            traza.trace("accion " + accion, Level.INFO);

            if (Constantes.ACCION.equalsIgnoreCase("CONSULTAR")) {
                btnGuardar.setVisible(false);
            }

            mostrarInformacion();

            capPanel.setLabel("Indices Expediente: " + expediente.getIdExpediente());
            construirIndices();

            if (accion.equalsIgnoreCase("vizualizar")) {
                btnGuardar.setVisible(false);
                vizualizarDocumentos();
                mostrarDatosAdicionales(false);

            } else {

                mostrarDatosAdicionales(false);

                archivo = (File) session.getAttribute("archivo");
                mostarDocumento(archivo);
            }

        } else {
            herramientas.warn("Problemas con la Sesión");
            herramientas.navegarPagina("index.zul");
        }

    }

    private void vizualizarDocumentos() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String ruta = "", archivo = "", idExpediente;
        OutputStream outputStream;
        Bufer buffer;
        int cont = 0, idInfoDocumento, idDoc, version, numDoc, idCat, idSubCat;
        File doc, fileCod;
        Comboitem item;

        try {

            idInfoDocumento = infoDocumento.getIdInfoDocumento();
            idDoc = infoDocumento.getIdDocumento();
            version = infoDocumento.getVersion();
            numDoc = infoDocumento.getNumeroDocumento();
            idCat = expediente.getIdCategoria();
            idSubCat = expediente.getIdSubCategoria();
            idExpediente = expediente.getIdExpediente();
            traza.trace("buscando el documento idDocumento " + idDoc + " idCategoria " + idCat + " idSubCategoria " + idSubCat + " idExpediente " + idExpediente, Level.INFO);

            System.gc();
            infoDocumentos = new GestionArchivos().buscarImagenDocumentos(idInfoDocumento, idDoc, idCat, idSubCat, version, numDoc, idExpediente);

            if (infoDocumentos.size() > 0) {

                if (toolsFile.getDirTemporal().exists()) {

                    File[] files = toolsFile.getDirTemporal().listFiles();
                    for (File f : files) {
                        if (f.delete()) {
                            traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                        } else {
                            traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                            f.deleteOnExit();
                        }
                    }
                }

                for (InfoDocumento id : infoDocumentos) {

                    if (version > 0) {
                        if (id.getVersion() == version) {
                            if (id.getFechaVencimiento() != null) {

                                XMLGregorianCalendar xmlCalendar = id.getFechaVencimiento();
                                GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                                fechaVencimiento.setValue("Fecha de Vencimiento: " + sdf.format(rechaVencimiento.getTime()));
                                fechaVencimiento.setVisible(true);

                                ruta = id.getRutaArchivo();
                                archivo = id.getNombreArchivo();
                                version = id.getVersion();
                                break;

                            } else {
                                ruta = id.getRutaArchivo();
                                archivo = id.getNombreArchivo();
                                version = id.getVersion();
                                break;
                            }
                        }
                    } else {
                        if (id.getFechaVencimiento() != null) {

                            XMLGregorianCalendar xmlCalendar = id.getFechaVencimiento();
                            GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                            fechaVencimiento.setValue("Fecha de Vencimiento: " + sdf.format(rechaVencimiento.getTime()));
                            fechaVencimiento.setVisible(true);

                            ruta = id.getRutaArchivo();
                            archivo = id.getNombreArchivo();
                            version = id.getVersion();
                            break;

                        } else {
                            ruta = id.getRutaArchivo();
                            archivo = id.getNombreArchivo();
                            version = id.getVersion();
                            break;
                        }
                    }

                }

                for (InfoDocumento infoDoc : infoDocumentos) {

                    item = new Comboitem();
                    item.setLabel(String.valueOf(infoDoc.getVersion()));
                    item.setValue(infoDoc.getVersion());
                    item.setParent(cmbVersion);
                    cont++;
                }

                if (infoDocumentos.size() == 1 || cont == 1) {
                    cmbVersion.setVisible(false);
                    btnAbrir.setVisible(false);
                } else {
                    cmbVersion.setVisible(true);
                    btnAbrir.setVisible(true);
                }

                traza.trace("ruta a buscar " + ruta, Level.INFO);
                traza.trace("archivo a buscar " + archivo, Level.INFO);
                traza.trace("version del documento a mostrar " + version, Level.INFO);

                //jlMensaje4.setText("Tipo de Documento: "+infoDocumento.getTipoDocumento()+" \"ULTIMA VERSION: "+version+"\"");
                if (archivo == null) {
                    throw new Exception("Falta información del documento");
                }
                //busca el archivo fisico del tipo de documento
                buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);

                if (!buffer.isExiste()) {
                    throw new Exception("Problemas al buscar el fisico del documento");
                }
                fileCod = new File(toolsFile.getArchivoCodificado());
                outputStream = new FileOutputStream(fileCod);
                outputStream.write(buffer.getBufer());
                outputStream.flush();
                outputStream.close();

                doc = new File("documento" + Constantes.CONTADOR++ + "." + infoDocumento.getFormato());

                toolsFile.decodificar(doc.getName());

                mostarDocumento(new File(toolsFile.getTempPath(), doc.getName()));

            } else {
                throw new IOException("Documento o Archivo no encontrado");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            herramientas.error("Problemas con los Servicios Web", ex);
            traza.trace("problemas webswervice", Level.ERROR, ex);
        } catch (FileNotFoundException ex) {
            herramientas.error("Archivo no encontrado", ex);
            traza.trace("problemas archivo no encontrado", Level.ERROR, ex);
        } catch (IOException ex) {
            herramientas.error("Problemas al crear el documento", ex);
            traza.trace("problemas i/o", Level.ERROR, ex);
        } catch (Exception ex) {
            herramientas.error("Problema al visualizar el documento", ex);
            traza.trace("problemas general", Level.ERROR, ex);
        }
    }

    @Listen("onClick = #btnAbrir")
    public void getVersion() {
        int version = cmbVersion.getSelectedItem().getValue();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String ruta, archivo;
        OutputStream outputStream;
        Bufer buffer;
        File doc, fileCod;

        try {
            for (InfoDocumento infoDoc : infoDocumentos) {

                if (infoDoc.getVersion() == version) {

                    documento.setContent(null);
                    expediente.setVersionDocumento(infoDoc.getVersion());
                    expediente.setNumeroDocumento(infoDoc.getNumeroDocumento());
//                    expediente.setIdTipoDocumento(infoDoc.getIdDocumento());

                    if (toolsFile.getDirTemporal().exists()) {
                        File[] files = toolsFile.getDirTemporal().listFiles();
                        for (File f : files) {
                            try {
                                boolean del = f.delete();
                                if (del) {
                                    traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                                } else {
                                    traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                                }
                            } catch (Exception e) {
                                traza.trace("problemas al eliminar el archivo " + f.getName(), Level.ERROR, e);
                            }
                        }
                    }

                    if (infoDoc.getFechaVencimiento() != null) {

                        XMLGregorianCalendar xmlCalendar = infoDoc.getFechaVencimiento();
                        GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                        fechaVencimiento.setValue("Fecha de Vencimiento: " + sdf.format(rechaVencimiento.getTime()));
                        fechaVencimiento.setVisible(true);

                        ruta = infoDoc.getRutaArchivo();
                        archivo = infoDoc.getNombreArchivo();
                        version = infoDoc.getVersion();

                    } else {
                        ruta = infoDoc.getRutaArchivo();
                        archivo = infoDoc.getNombreArchivo();
                        version = infoDoc.getVersion();
                    }

                    traza.trace("ruta a buscar " + ruta, Level.INFO);
                    traza.trace("archivo a buscar " + archivo, Level.INFO);
                    traza.trace("version del documento a mostrar " + version, Level.INFO);

                    //jlMensaje4.setText("Tipo de Documento: "+infoDocumento.getTipoDocumento()+" \"ULTIMA VERSION: "+version+"\"");
                    if (archivo == null) {
                        throw new Exception("Falta información del documento");
                    }
                    //busca el archivo fisico del tipo de documento
                    buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);

                    if (!buffer.isExiste()) {
                        throw new Exception("Problemas al buscar el fisico del documento");
                    }
                    fileCod = new File(toolsFile.getArchivoCodificado());
                    outputStream = new FileOutputStream(fileCod);
                    outputStream.write(buffer.getBufer());
                    outputStream.flush();
                    outputStream.close();

                    doc = new File("documento" + Constantes.CONTADOR++ + "." + infoDoc.getFormato());

                    toolsFile.decodificar(doc.getName());

                    mostrarDatosAdicionales(true);
                    mostarDocumento(new File(toolsFile.getTempPath(), doc.getName()));
                }
            }
        } catch (SOAPException | SOAPFaultException ex) {
            herramientas.error("Problemas con los Servicios Web", ex);
            traza.trace("problemas webswervice", Level.ERROR, ex);
        } catch (FileNotFoundException ex) {
            herramientas.error("Archivo no encontrado", ex);
            traza.trace("problemas archivo no encontrado", Level.ERROR, ex);
        } catch (IOException ex) {
            herramientas.error("Problemas al crear el documento", ex);
            traza.trace("problemas i/o", Level.ERROR, ex);
        } catch (Exception ex) {
            herramientas.error("Problema general al visualizar la version del documento", ex);
            traza.trace("problemas general", Level.ERROR, ex);
        }
    }

    private void mostrarInformacion() {
        Listitem datos;

        datos = new Listitem();
        datos.appendChild(new Listcell("Expediente: " + expediente.getIdExpediente()));
        datos.setParent(informacion);

        datos = new Listitem();
        datos.appendChild(new Listcell("Libreria: " + expediente.getLibreria()));
        datos.setParent(informacion);

        datos = new Listitem();
        datos.appendChild(new Listcell("Categoria: " + expediente.getCategoria()));
        datos.setParent(informacion);

        datos = new Listitem();
        datos.appendChild(new Listcell("SubCategoria: " + expediente.getSubCategoria()));
        datos.setParent(informacion);

        datos = new Listitem();
        datos.appendChild(new Listcell("Tipo de Documento: " + expediente.getTipoDocumento()));
        datos.setParent(informacion);

    }

    private void mostrarDatosAdicionales(boolean vers) {
        Listitem datos;
        List<DatoAdicional> lsDatosAdicionales = new ArrayList<>();
        int numeroDocumento, version;

        try {

            if (session != null) {

                if (infoDocumento.isTipoDocDatoAdicional()) {
                    
                    datosAdicionales.getChildren().clear();

                    if (!vers) {
                        lsDatosAdicionales = infoDocumento.getLsDatosAdicionales();
                    }

                    numeroDocumento = expediente.getNumeroDocumento();
                    version = expediente.getVersionDocumento();

                    if (lsDatosAdicionales.isEmpty()) {
                        lsDatosAdicionales = new GestionDocumentos().encontrarValorDatoAdicional(expediente.getIdTipoDocumento(), expediente.getIdExpediente(), numeroDocumento, version);
                    }

                    if (!lsDatosAdicionales.isEmpty()) {

                        for (DatoAdicional da : lsDatosAdicionales) {
                            datos = new Listitem();
                            datos.appendChild(new Listcell(da.getIndiceDatoAdicional() + ": " + da.getValor()));
                            datos.setParent(datosAdicionales);
                        }
                    }

                } else {
                    grpDatosAdicionales.setVisible(false);
                }

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas mostrando los datos adicionales", Level.ERROR, ex);
        }
    }

    private void construirIndices() {

        List<Indice> listaIndic;
        Listitem datos;
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        try {

            if (session != null) {

                listaIndic = new BuscaIndice().buscaDatosIndice(expediente.getIdExpediente(), expediente.getIdLibreria(), expediente.getIdCategoria());

                for (Indice ind : listaIndic) {
//                    barraEstado.setStatus("Agregando el indice " + ind.getIndice() + ": " + ind.getValor());
                    datos = new Listitem();
                    datos.appendChild(new Listcell(ind.getIndice() + ": " + ind.getValor()));
                    datos.setParent(indices);
                }

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException | SOAPFaultException e) {
            traza.trace("Problemas con los indices del expediente " + expediente.getIdExpediente(), Level.ERROR, e);
            herramientas.error("Problemas con los indices del expediente " + expediente.getIdExpediente(), e);
        }
    }

    private void mostarDocumento(File file) {
        AMedia media;
        ByteArrayInputStream is;
        FileInputStream fs;
        byte[] buffer;
        String formato;

        try {

            if (session != null) {

                formato = file.getName().substring(file.getName().indexOf(".") + 1);

                if (formato.equalsIgnoreCase("pdf")) {

                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else if ((formato.equalsIgnoreCase("jpg")) || (formato.equalsIgnoreCase("jpge"))) {

                    file = toolsFile.guardarArchivoJPGtoPDF(file);
                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else if ((formato.equalsIgnoreCase("tif")) || (formato.equalsIgnoreCase("tiff"))) {

                    file = toolsFile.guardarArchivoTIFFtoPDF(file);
                    cantidadPaginas = toolsFile.getCantidadPaginas();
                    buffer = new byte[(int) file.length()];
                    fs = new FileInputStream(file);
                    fs.read(buffer);
                    fs.close();
                    is = new ByteArrayInputStream(buffer);

                    media = new AMedia("documento", "pdf", "application/pdf", is);
                    documento.setContent(media);

                } else {
                    herramientas.warn("Hubo un problema en la conversión del documento, se regresará");
                }

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (Exception e) {
            traza.trace("problemas al mostrar el documento", Level.ERROR, e);
        }
    }

    @Listen("onClick = #btnGuardar")
    public void guardar() {
        File archivo;
        ArrayList<BufferedImage> imagenesTiff;
        boolean resp = false;
        InfoDocumento infoDoc;
        ToolsFiles toolsTiff = new ToolsFiles();
        String dirTipoDoc, dirCat, dirSubCat, ruta, lib, idExpEncriptado, fileName, exito;
        Matcher mat;
        Pattern pat;
        int idDoc, r;
        InputStream leyendo = null;
        byte[] bu;
        Bufer buffer = new Bufer();
        pat = Pattern.compile(" ");

        String acc = accion, msg;

        try {
            traza.trace("enviando el archivo", Level.INFO);

            acc = acc.toLowerCase();
            char[] cs = acc.toCharArray();
            char ch = cs[0];
            cs[0] = Character.toUpperCase(ch);
            acc = String.valueOf(cs);

            if (session != null) {

                msg = "Desea " + acc + " \n" + expediente.getTipoDocumento() + " \nen el expediente " + expediente.getIdExpediente();
                if (accion.equalsIgnoreCase("versionar")) {
                    msg = "Desea " + acc + " \n" + expediente.getTipoDocumento() + "\n version " + infoDocumento.getVersion() + " \nen el expediente " + expediente.getIdExpediente();
                }

                r = Messagebox.show(msg, "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                        new EventListener() {
                            @Override
                            public void onEvent(Event evt) throws Exception {
                                //evt.getName().equalsIgnoreCase(Messagebox.ON_YES);
                            }
                        });

                if (r == Messagebox.YES) {

                    lib = expediente.getLibreria();
                    dirCat = expediente.getCategoria();
                    dirSubCat = expediente.getSubCategoria();
                    dirTipoDoc = expediente.getTipoDocumento();
                    idDoc = expediente.getIdTipoDocumento();
                    idExpEncriptado = expediente.getIdExpediente();

                    mat = pat.matcher(lib);
                    lib = mat.replaceAll("");

                    mat = pat.matcher(dirCat);
                    dirCat = mat.replaceAll("");

                    mat = pat.matcher(dirSubCat);
                    dirSubCat = mat.replaceAll("");

                    mat = pat.matcher(dirTipoDoc);
                    dirTipoDoc = mat.replaceAll("");

                    lib = EncriptaDesencripta.encripta(lib);
                    dirCat = EncriptaDesencripta.encripta(dirCat);
                    dirSubCat = EncriptaDesencripta.encripta(dirSubCat);
                    dirTipoDoc = EncriptaDesencripta.encripta(dirTipoDoc);
                    //idExpEncriptado = EncriptaDesencripta.encripta(idExpEncriptado);
                    ruta = lib + "/" + dirCat + "/" + dirSubCat;

                    leyendo = new FileInputStream(toolsTiff.getArchivoCodificado());
                    traza.trace("posible tamaño del archivo a transferir " + (leyendo.available() / 1024000), Level.INFO);
                    bu = new byte[leyendo.available()];
                    leyendo.read(bu);
                    leyendo.close();
                    buffer.setBufer(bu);

                    infoDocumento.setNombreArchivo(dirTipoDoc);
                    infoDocumento.setRutaArchivo(ruta);
                    infoDocumento.setCantPaginas(cantidadPaginas);

                    traza.trace("fecha vencimiento " + infoDocumento.getFechaVencimiento(), Level.INFO);
                    exito = new GestionArchivos().almacenaArchivo(buffer, accion, infoDocumento, expediente.getIdExpediente(), session.getAttribute("login").toString());

                    if (exito.equalsIgnoreCase("exito")) {
                        traza.trace("se guardo el documento con exito", Level.INFO);
                        herramientas.info("Documento generado satisfactoriamente");
                        resp = true;
                        expediente.setResultado(true);
                        session.setAttribute("expediente", expediente);
                        cerrar();
                    } else {
                        traza.trace("problemas al guardar el archivo objecto \n respuesta del webservices " + exito, Level.INFO);
                        herramientas.warn("Problemas al guardar el documento, contacte al administrador\n" + exito);
                        throw new DW4JDesktopExcepcion("problemas al guardar el archivo objecto");
                    }
                }
            }

        } catch (OutOfMemoryError ex) {
            traza.trace("Error de memoria ", Level.ERROR, ex);
            herramientas.error("Error\n" + ex.getMessage(), ex);
        } catch (FileNotFoundException ex) {
            traza.trace("Error archivo: ", Level.ERROR, ex);
            herramientas.error("Error archivo: \n" + ex.getMessage(), ex);
        } catch (IOException ex) {
            traza.trace("Error lectura archivo: ", Level.ERROR, ex);
            herramientas.error("Error lectura archivo: \n" + ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            traza.trace("Error puntero nulo ", Level.ERROR, ex);
            herramientas.error("Error al guardar el archivo\nReintente de nuevo\n" + ex.getMessage(), ex);
        } catch (DW4JDesktopExcepcion ex) {
            traza.trace("Error ", Level.ERROR, ex);
//            JOptionPane.showMessageDialog(this, ex.getMessage(), "Error General", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("Error general: ", Level.ERROR, ex);
            herramientas.error("Error General", ex);
        } finally {
            try {
                leyendo.close();
            } catch (IOException ex) {
                traza.trace("error al cerrar el archivo", Level.ERROR, ex);
            }
        }

    }

    @Listen("onClick = #btnCerrar")
    public void cerrar() {
        session.removeAttribute("infoDocumento");
        session.removeAttribute("archivo");
        winDocumento.setVisible(false);
        winDocumento.detach();
    }

}
