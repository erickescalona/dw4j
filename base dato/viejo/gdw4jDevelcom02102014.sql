--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-10-02 15:06:41

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 205 (class 3079 OID 12617)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3096 (class 0 OID 0)
-- Dependencies: 205
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 226 (class 1255 OID 34509)
-- Name: f_buscar_categoria(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_categoria(p_idcategoria integer, p_idlibreria integer, p_categoria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cat refcursor;
  begin


    if p_idCategoria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_categoria=p_idcategoria
          order by c.categoria;

    elsif p_idLibreria > 0 then

        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.id_libreria=p_idlibreria
          order by c.categoria;

    elsif p_categoria is not null then
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
          where c.categoria=p_categoria
          order by c.categoria;
    else
        open cursor_cat for
          select c.id_categoria, c.id_libreria, c.categoria, c.id_estatus, e.estatus
              from categoria c inner join estatus e on c.id_estatus=e.id_estatus
           order by c.categoria;
    end if;
    return cursor_cat;
    close cursor_cat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_categoria(p_idcategoria integer, p_idlibreria integer, p_categoria character varying) OWNER TO postgres;

--
-- TOC entry 256 (class 1255 OID 34551)
-- Name: f_buscar_causas_rechazo(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_causas_rechazo() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_causa refcursor;
  begin

    open cursor_causa for
      select id_causa, causa from causa;

    return cursor_causa;
    close cursor_causa;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_causas_rechazo() OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 34580)
-- Name: f_buscar_configuracion(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_configuracion() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_conf refcursor;
  begin

    open cursor_conf for
      select * from configuracion;

    return cursor_conf;
    close cursor_conf;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_configuracion() OWNER TO postgres;

--
-- TOC entry 232 (class 1255 OID 34517)
-- Name: f_buscar_datos_combo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_datos_combo(p_idcodigoindice integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice
             from lista_desplegables l inner join indices a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idcodigoindice
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_datos_combo(p_idcodigoindice integer) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 34569)
-- Name: f_buscar_datosdoc(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_datosdoc(p_idinfodocumento integer, p_versiondoc integer, p_idexpediente character varying, p_numerodoc integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

    cursor_doc refcursor;
  begin

    open cursor_doc for
      select i.*, d.*, c.id_categoria, s.id_subcategoria,
             t.tipo_documento tipoDoc, e.estatus_documento estatusArchivo
          from infodocumento i
          inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
          inner join tipodocumento t on i.id_documento=t.id_documento
          inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
          inner join categoria c on s.id_categoria=c.id_categoria
          inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
      where i.id_infodocumento=p_idinfodocumento and i.id_expediente=p_idexpediente
            and i.version=p_versiondoc and i.numero_documento=p_numerodoc
            and i.estatus_documento=2;

     return cursor_doc;
     close cursor_doc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_datosdoc(p_idinfodocumento integer, p_versiondoc integer, p_idexpediente character varying, p_numerodoc integer) OWNER TO postgres;

--
-- TOC entry 231 (class 1255 OID 34515)
-- Name: f_buscar_estatus(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_estatus(p_idestatus integer, p_estatus character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_est refcursor;
  begin

    if p_idestatus > 0 then
      open cursor_est for
         select id_estatus, estatus from estatus where id_estatus=p_idestatus;
    elsif p_estatus is not null then
      open cursor_est for
         select id_estatus, estatus from estatus where estatus=p_estatus;
    end if;

    return cursor_est;
    close cursor_est;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_estatus(p_idestatus integer, p_estatus character varying) OWNER TO postgres;

--
-- TOC entry 288 (class 1255 OID 34570)
-- Name: f_buscar_expediente(character varying, integer, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_expediente(p_expediente character varying, p_idlibreria integer, p_idcategoria integer, p_flag character) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_expe refcursor;
  begin

   if (p_flag = '1') then

      open cursor_expe for
         select e.expediente, e.id_libreria, e.id_categoria, e.valor, a.id_indice,
                c.categoria, l.libreria, a.indice, a.tipo, a.codigo, a.clave
             from expedientes e inner join indices a
                     on (e.id_categoria=a.id_categoria and e.id_indice=a.id_indice)
                  inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.id_libreria
              where e.expediente=p_expediente and a.id_categoria=p_idcategoria
                and e.id_libreria=p_idlibreria
              order by a.id_indice;
    else
      open cursor_expe for
         select e.*, c.categoria, l.libreria
             from expedientes e inner join categoria c on e.id_categoria=c.id_categoria
                  inner join libreria l on e.id_libreria=l.libreria
             where e.expediente=p_expediente;
    end if;
    return cursor_expe;
    close cursor_expe;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_expediente(p_expediente character varying, p_idlibreria integer, p_idcategoria integer, p_flag character) OWNER TO postgres;

--
-- TOC entry 255 (class 1255 OID 34549)
-- Name: f_buscar_fabrica(character varying, date, date, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_fabrica(p_usuario character varying, p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
    sfabrica char(1);
  begin

    select t.fabrica into sfabrica from fabrica t where t.usuario=p_usuario;

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion between CAST (p_fechadesde AS DATE)
           and CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion >= CAST (p_fechadesde AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where di.fecha_digitalizacion <= CAST (p_fechahasta AS DATE)
           and d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;

    elsif p_expediente is not null then

         open cursor_user for
            select distinct e.expediente, e.valor, i.*, f.fabrica
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
               inner join fabrica f on f.usuario=di.usuario_digitalizo
               inner join usuario u on u.id_usuario=f.usuario
             where d.id_expediente = p_expediente
               and d.estatus_documento=p_estatusdocumento
               and f.fabrica=sfabrica
               and i.id_categoria=p_idcategoria
               order by e.expediente, i.id_indice;
    else

      open cursor_user for
        select distinct e.expediente, e.valor, i.*, f.fabrica
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
           inner join fabrica f on f.usuario=di.usuario_digitalizo
           inner join usuario u on u.id_usuario=f.usuario
         where d.estatus_documento=p_estatusdocumento
           and f.fabrica=sfabrica
           and i.id_categoria=p_idcategoria
           order by e.expediente, i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_fabrica(p_usuario character varying, p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expediente character varying) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 34560)
-- Name: f_buscar_fisico_documento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_fisico_documento(p_iddocumento integer, p_numerodoc integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_doc refcursor;
  begin

    open cursor_doc for
        select i.*, c.id_categoria, s.id_subcategoria, t.tipo_documento tipoDoc,
               e.estatus_documento estatusArchivo
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                                inner join estatus_documento e on i.estatus_documento=e.id_estatus_documento
                                inner join tipodocumento t on i.id_documento=t.id_documento
                                inner join subcategoria s on t.id_subcategoria=s.id_subcategoria
                                inner join categoria c on s.id_categoria=c.id_categoria
           where i.id_documento=p_iddocumento and i.id_expediente=p_idexpediente
                 and i.numero_documento=p_numerodoc and i.estatus_documento<>2
           order by i.version desc;

  return cursor_doc;
  close cursor_doc;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_fisico_documento(p_iddocumento integer, p_numerodoc integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 34577)
-- Name: f_buscar_foto_ficha(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_foto_ficha(p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    cursor_ficha refcursor;
  begin

    open cursor_ficha for
        select t.id_documento, i.id_infodocumento, i.nombre_archivo as nombreArchivo,
               t.tipo_documento as tipoDocumento, i.estatus_documento, i.ruta_archivo
          from tipodocumento t inner join infodocumento i on t.id_documento=i.id_documento
          where i.id_expediente=p_idexpediente and t.ficha='1';

    return cursor_ficha;
    close cursor_ficha;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_foto_ficha(p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 225 (class 1255 OID 34519)
-- Name: f_buscar_indice_datosadicional(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_indice_datosadicional(p_idtipodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select *
         from dato_adicional t
         where t.id_documento = p_idtipodocumento
         order by t.id_dato_adicional;

    return cursor_dato;
    close cursor_dato;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_indice_datosadicional(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 228 (class 1255 OID 34512)
-- Name: f_buscar_indices(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_indices(p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_arg refcursor;
  begin

    open cursor_arg for
       select * from indices i where id_categoria=p_idcategoria order by i.id_indice;
    return cursor_arg;
    close cursor_arg;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_indices(p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 34564)
-- Name: f_buscar_infodocumento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_infodocumento(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
     cursor_info refcursor;
  begin

  if p_idinfodocumento > 0 then

    open cursor_info for
    select * from infodocumento where id_infodocumento=p_idinfodocumento;

  elsif p_iddocumento > 0 then

    open cursor_info for
    select * from infodocumento
    where id_documento=p_iddocumento and id_expediente=p_idexpediente
    order by numero_documento, version;

  end if;
  return cursor_info;
  close cursor_info;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_infodocumento(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 286 (class 1255 OID 34673)
-- Name: f_buscar_infodocumento(character varying, character varying, integer, character, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_infodocumento(p_idexpediente character varying, p_ids_documento character varying, p_estatusdoc integer, p_redigitalizo character, p_estatusaprobado integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_infodoc refcursor;
    query character varying;
  begin

   if p_estatusaprobado = 1 then
   
      --open cursor_infodoc for
        query:='select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where i.id_expediente='''||p_idexpediente||'''
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in ('||p_ids_documento||')
                 and (i.estatus_documento='||p_estatusdoc||' or i.estatus_documento='||p_estatusaprobado||')
                 and i.re_digitalizado='''||p_redigitalizo||'''
           order by t.tipo_documento, i.numero_documento, i.version desc';
   else

     --open cursor_infodoc for
        query:='select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento,
               i.nombre_archivo, i.ruta_archivo, i.paginas, i.version, i.formato, i.id_expediente,
               i.numero_documento, i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion,
               i.estatus_documento idStatus, d.usuario_digitalizo, d.fecha_aprobacion, d.usuario_aprobacion,
               d.fecha_rechazo, d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
               s.estatus_documento, t.dato_adicional as datipodoc
           from infodocumento i inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
                inner join tipodocumento t on i.id_documento=t.id_documento
                inner join expedientes e on e.expediente=i.id_expediente
                inner join estatus_documento s on s.id_estatus_documento=i.estatus_documento
           where e.expediente='''||p_idexpediente||'''
                 --and i.id_documento=p_ids_documento
                 and i.id_documento in ('||p_ids_documento||')
                 and i.estatus_documento='||p_estatusdoc||' and i.re_digitalizado='''||p_redigitalizo||'''
           order by t.tipo_documento, i.numero_documento, i.version desc';

   end if;

    OPEN cursor_infodoc FOR execute query;
   
    return cursor_infodoc;
    close cursor_infodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_infodocumento(p_idexpediente character varying, p_ids_documento character varying, p_estatusdoc integer, p_redigitalizo character, p_estatusaprobado integer) OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 34568)
-- Name: f_buscar_informaciondoc(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_informaciondoc(p_idsdocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

  cursor_infdoc refcursor;

  begin

    open cursor_infdoc for
         select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expedientes e on i.id_expediente=e.expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;
            /*select distinct i.id_infodocumento, t.tipo_documento tipoDoc, t.id_documento, i.nombre_archivo,
              i.ruta_archivo, i.paginas, i.formato, i.version, i.id_expediente, i.numero_documento,
              i.fecha_vencimiento, d.dato_adicional, d.fecha_digitalizacion, d.usuario_digitalizo,
              i.estatus_documento, d.fecha_aprobacion, d.usuario_aprobacion, d.fecha_rechazo,
              d.usuario_rechazo, d.motivo_rechazo, d.causa_rechazo, i.re_digitalizado,
              s.estatus_documento
            from infodocumento i
            inner join datos_infodocumento d on i.id_infodocumento=d.id_infodocumento
            inner join tipodocumento t on i.id_documento=t.id_documento
            inner join expediente e on i.id_expediente=e.id_expediente
            inner join estatus_documento s on i.estatus_documento=s.id_estatus_documento
            --where e.id_expediente=p_idexpediente and i.id_documento in (p_idsdocumento)
            where i.id_expediente=p_idexpediente and i.id_documento=p_idsdocumento
                  and i.estatus_documento=2 and i.re_digitalizado=0;*/

       return cursor_infdoc;
       close cursor_infdoc;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_informaciondoc(p_idsdocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 34547)
-- Name: f_buscar_lib_cat_perfil(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_lib_cat_perfil(p_usuario character varying, p_perfil character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
        select p.id_usuario usuario, r.rol,
               p.id_libreria, l.libreria, el.estatus status_lib,
               p.id_categoria, c.categoria, ec.estatus status_cat
           from perfil p
                inner join rol r on p.id_rol=r.id_rol
                inner join libreria l on p.id_libreria=l.id_libreria
                inner join estatus el on l.id_estatus=el.id_estatus
                inner join categoria c on p.id_categoria=c.id_categoria
                inner join estatus ec on c.id_estatus=ec.id_estatus
           where p.id_usuario=p_usuario and r.rol=p_perfil
           order by l.libreria;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_lib_cat_perfil(p_usuario character varying, p_perfil character varying) OWNER TO postgres;

--
-- TOC entry 285 (class 1255 OID 34516)
-- Name: f_buscar_libreria(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_libreria(p_idlibreria integer, p_libreria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_lib refcursor;
  begin

    if p_idlibreria > 0 then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.id_libreria=p_idlibreria
             order by l.libreria asc;

    elsif p_libreria is not null then

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             where l.libreria=p_libreria
             order by l.libreria asc;

    else

      open cursor_lib for
      select l.id_libreria, l.libreria, l.id_estatus, e.estatus status
             from libreria l inner join estatus e on l.id_estatus=e.id_estatus
             order by l.libreria asc;

    end if;
    return cursor_lib;
    close cursor_lib;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_libreria(p_idlibreria integer, p_libreria character varying) OWNER TO postgres;

--
-- TOC entry 229 (class 1255 OID 34513)
-- Name: f_buscar_libreriascategorias(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_libreriascategorias() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_libcat refcursor;
  begin

    open cursor_libcat for
       select l.id_libreria, l.libreria descLibreria, c.id_categoria, c.categoria descCategoria,
              r.id_rol, r.rol descRol, e.id_estatus, e.estatus desEstatus
          from libreria l inner join categoria c on l.id_libreria=c.id_libreria
                          inner join estatus e
                                 on (l.id_estatus=e.id_estatus and c.id_estatus=e.id_estatus),
                          rol r
          where e.id_estatus=1
          order by l.libreria, c.categoria;

    return cursor_libcat;
    close cursor_libcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_libreriascategorias() OWNER TO postgres;

--
-- TOC entry 254 (class 1255 OID 34548)
-- Name: f_buscar_no_fabrica(date, date, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_no_fabrica(p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expedeinte character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;

  begin

    if p_fechadesde is not null and p_fechahasta is not null then

      open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion between to_date(p_fechadesde, 'dd/MM/yyyy')
           and to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechadesde is not null then

        open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion >= to_date(p_fechadesde, 'dd/MM/yyyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_fechahasta is not null then

        open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where di.fecha_digitalizacion <= to_date(p_fechahasta, 'dd/MM/yyy')
           and d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;

    elsif p_expedeinte is not null then

         open cursor_user for
            select i.*, e.valor, e.expediente
               from infodocumento d inner join expedientes e
                  on d.id_expediente=e.expediente
               inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
               inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
             where d.id_expediente = p_expedeinte
               and d.estatus_documento=p_estatusdocumento
               and i.id_categoria=p_idcategoria
               order by i.id_indice;
    else

      open cursor_user for
        select i.*, e.valor, e.expediente
           from infodocumento d inner join expedientes e
              on d.id_expediente=e.expediente
           inner join indices i on (i.id_indice=e.id_indice and i.id_categoria=e.id_categoria)
           inner join datos_infodocumento di on di.id_infodocumento=d.id_infodocumento
         where d.estatus_documento=p_estatusdocumento
           and i.id_categoria=p_idcategoria
           order by i.id_indice;
    end if;

    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_no_fabrica(p_fechadesde date, p_fechahasta date, p_estatusdocumento integer, p_idcategoria integer, p_expedeinte character varying) OWNER TO postgres;

--
-- TOC entry 230 (class 1255 OID 34514)
-- Name: f_buscar_perfil(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_perfil(p_idusuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_perfil refcursor;
  begin

    open cursor_perfil for
        select distinct u.nombre, u.apellido, u.id_usuario, e.estatus estatus_usuario,
               f.fabrica pertenece, r.rol,
               l.id_libreria, l.libreria,
               c.id_categoria, c.categoria
            from usuario u full join perfil p on u.id_usuario=p.id_usuario
                           full join estatus e on u.id_estatus=e.id_estatus
                           full join fabrica f on u.id_usuario=f.usuario
                           full join rol r on p.id_rol=r.id_rol
                           full join libreria l on p.id_libreria=l.id_libreria
                           full join categoria c on p.id_categoria=c.id_categoria
             where u.id_usuario=p_idusuario
             order by l.libreria, c.categoria;


    return cursor_perfil;
    close cursor_perfil;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_perfil(p_idusuario character varying) OWNER TO postgres;

--
-- TOC entry 287 (class 1255 OID 34586)
-- Name: f_buscar_subcategoria(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_subcategoria(p_idcategoria integer, p_idsubcategoria integer, p_subcategoria character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_subcat refcursor;
  begin

    if p_idcategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_categoria=p_idcategoria
          order by s.subcategoria;

    elsif p_idsubcategoria > 0 then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.id_subcategoria=p_idsubcategoria
          order by s.subcategoria;

    elsif p_subcategoria is not null then

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
          where s.subcategoria=p_subcategoria
          order by s.subcategoria;

    else

      open cursor_subcat for
          select s.id_subcategoria, s.id_categoria, s.subcategoria, s.id_estatus, e.estatus
             from subcategoria s inner join estatus e on s.id_estatus=e.id_estatus
             order by s.subcategoria;
    end if;
    return cursor_subcat;
    close cursor_subcat;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_subcategoria(p_idcategoria integer, p_idsubcategoria integer, p_subcategoria character varying) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 34576)
-- Name: f_buscar_tipo_documento(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_tipo_documento(p_idssucategorias integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    curor_doc refcursor;
  begin
    

    open curor_doc for
     select t.*, e.*
       from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
       where t.id_subcategoria in (p_idssucategorias) and e.id_estatus=1;
       --where t.id_subcategoria = p_idssucategorias and e.id_estatus=1;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_tipo_documento(p_idssucategorias integer) OWNER TO postgres;

--
-- TOC entry 233 (class 1255 OID 34511)
-- Name: f_buscar_tipo_documento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_tipo_documento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumemto character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_tipodoc refcursor;
  begin

    if p_idCategoria > 0 and p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria and t.id_subcategoria=p_idsubcategoria
         order by t.tipo_documento;

    elsif p_idCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_categoria=p_idcategoria order by t.tipo_documento;

    elsif p_idSubCategoria > 0 then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.id_subcategoria=p_idsubcategoria order by t.tipo_documento;

    elsif p_tipodocumemto is not null then

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
         where t.tipo_documento=p_tipodocumemto order by t.tipo_documento;

    else

      open cursor_tipodoc for
         select t.id_documento, t.id_categoria, t.id_subcategoria, t.tipo_documento,
                t.id_estatus, t.vencimiento, t.dato_adicional, t.ficha, e.estatus
            from tipodocumento t inner join estatus e on t.id_estatus=e.id_estatus
            order by t.tipo_documento;

    end if;
    return cursor_tipodoc;
    close cursor_tipodoc;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_tipo_documento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumemto character varying) OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 34561)
-- Name: f_buscar_ultima_version(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_ultima_version(p_iddocumento integer, p_idexpediente character varying, p_numerodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_ver refcursor;
  begin

    open cursor_ver for
      select max(i.version) as version
        from infodocumento i
        where i.id_documento=p_iddocumento
              and i.id_expediente=p_idexpediente
              and i.numero_documento=p_numerodocumento
              and i.estatus_documento<>2;

  return cursor_ver;
  close cursor_ver;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_ultima_version(p_iddocumento integer, p_idexpediente character varying, p_numerodocumento integer) OWNER TO postgres;

--
-- TOC entry 284 (class 1255 OID 34557)
-- Name: f_buscar_ultimo_numero(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_ultimo_numero(p_iddocumento integer, p_idexpediente character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
    numero_doc integer;
  begin

   numero_doc := -1;
   
   select max(numero_documento) as numeroDocumento into numero_doc from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente;

   return numero_doc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_ultimo_numero(p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 218 (class 1255 OID 34521)
-- Name: f_buscar_usuario_fabrica(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_usuario_fabrica(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_fab refcursor;
  begin

    open cursor_fab for
       select usuario, fabrica from fabrica f where f.usuario=p_usuario;


    return cursor_fab;
    close cursor_fab;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_usuario_fabrica(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 34562)
-- Name: f_buscar_valor_dato_adicional(integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_valor_dato_adicional(p_idocumento integer, p_idexpediente character varying, p_numerodoc integer, p_version integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_dato refcursor;
  begin

    open cursor_dato for
       select *
           from dato_adicional d
             inner join valor_dato_adicional v on d.id_dato_adicional=v.id_dato_adicional
           where d.id_documento=p_idocumento
                and v.expediente=p_idexpediente
                and v.numero=p_numerodoc
                and v.version=p_version;

  return cursor_dato;
  close cursor_dato;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_valor_dato_adicional(p_idocumento integer, p_idexpediente character varying, p_numerodoc integer, p_version integer) OWNER TO postgres;

--
-- TOC entry 227 (class 1255 OID 34520)
-- Name: f_buscar_valor_datoadicional(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_buscar_valor_datoadicional(p_idindicedato integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_cbo refcursor;
  begin

    open cursor_cbo for
       select l.id_lista, l.codigo_indice, l.descripcion, a.indice_adicional
             from lista_desplegables l inner join dato_adicional a on l.codigo_indice=a.codigo
       where l.codigo_indice=p_idindicedato
       order by l.descripcion;


    return cursor_cbo;
    close cursor_cbo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_buscar_valor_datoadicional(p_idindicedato integer) OWNER TO postgres;

--
-- TOC entry 223 (class 1255 OID 34518)
-- Name: f_comprobar_foto_ficha(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_foto_ficha(p_idtipodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_foto refcursor;
  begin

    open cursor_foto for
       select ficha from tipodocumento where id_documento=p_idtipodocumento;

    return cursor_foto;
    close cursor_foto;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_foto_ficha(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 34559)
-- Name: f_comprobar_nombre_archivo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_nombre_archivo(p_idinfodocumento integer, p_idexpediente character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
  DECLARE
    nombre varchar(255);
  begin

   nombre := null;
   
   select nombre_archivo into nombre from infodocumento
   where id_infodocumento= p_idinfodocumento and id_expediente=p_idexpediente;

   return nombre;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_nombre_archivo(p_idinfodocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 34558)
-- Name: f_comprobar_numero_documento(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_comprobar_numero_documento(p_iddocumento integer, p_idexpediente character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_numdoc refcursor;
  begin

   open cursor_numdoc for
   select numero_documento  from infodocumento
   where id_documento=p_iddocumento and id_expediente=p_idexpediente
   order by numero_documento;

   return cursor_numdoc;
   close cursor_numdoc;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_comprobar_numero_documento(p_iddocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 34546)
-- Name: f_crear_sesion(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_crear_sesion(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin
   
    open cursor_user for
       select distinct r.rol desc_rol, e.estatus estatus_usuario, u.id_usuario id_user,
              u.nombre, u.apellido, u.cedula, u.id_estatus
           from usuario u inner join estatus e on u.id_estatus=e.id_estatus
                inner join perfil p on p.id_usuario=u.id_usuario
                inner join rol r on r.id_rol=p.id_rol
           where u.id_usuario=p_usuario;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_crear_sesion(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 257 (class 1255 OID 34554)
-- Name: f_eliminar_archivo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_eliminar_archivo(p_idinfodocumento integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE

    cursor_eliminar refcursor;
  begin

   open cursor_eliminar for
        select t.ruta_archivo, t.nombre_archivo from infodocumento t where t.id_infodocumento=p_idinfodocumento;

    return cursor_eliminar;
    close cursor_eliminar;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_eliminar_archivo(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 34578)
-- Name: f_foliatura_buscar_expediente(character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_foliatura_buscar_expediente(p_idexpediente character varying, p_idlibreria integer, p_idcategoria integer) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_folio refcursor;
  begin
  
    open cursor_folio for
        select i.id_expediente, l.id_libreria, c.id_categoria, s.id_subcategoria,
               t.id_documento, i.id_infodocumento, t.tipo_documento as documento,
               i.paginas as cantidadPAginas 
     from infodocumento i 
         inner join tipodocumento t on i.id_documento=t.id_documento
         inner join subcategoria s on s.id_subcategoria=t.id_subcategoria
         inner join categoria c on c.id_categoria=t.id_categoria
         inner join libreria l on l.id_libreria=c.id_libreria
        where i.id_expediente=p_idexpediente and i.estatus_documento=1 and
              l.id_libreria=p_idlibreria and c.id_categoria=p_idcategoria
        order by t.id_documento, i.numero_documento desc, i.version desc;

    return cursor_folio;
    close cursor_folio;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_foliatura_buscar_expediente(p_idexpediente character varying, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 34571)
-- Name: f_informacion_tabla(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_informacion_tabla(p_tabla character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_tabla refcursor;
  begin

    open cursor_tabla for
      SELECT s.OWNER, s.TABLE_NAME, s.COLUMN_NAME, s.DATA_TYPE
          FROM all_tab_columns s
      WHERE
      --owner = 'GESTORDOCUMENTAL' and
      table_name=p_tabla;
      return cursor_tabla;
      close cursor_tabla;
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_informacion_tabla(p_tabla character varying) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 34575)
-- Name: f_modificar_indices(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_modificar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_flag character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
 DECLARE
    curor_indice refcursor;
  begin
    
    if p_flag = '0' then
    
      open curor_indice for
         select expediente from expedientes where expediente=p_idexpedientenuevo;
      return curor_indice;
      close curor_indice;
      
    elsif p_flag = '1' then
      
      delete from expedientes where expediente=p_idexpedienteviejo;

      update infodocumento set id_expediente=p_idexpedientenuevo where id_expediente=p_idexpedienteviejo;
      return curor_indice;
    end if;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_modificar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_flag character varying) OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 34545)
-- Name: f_usuarios(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_usuarios() RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin

    open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.sexo,
              e.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

       return cursor_user;
       close cursor_user;
	   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_usuarios() OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 34544)
-- Name: f_verificar_usuario(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION f_verificar_usuario(p_usuario character varying) RETURNS refcursor
    LANGUAGE plpgsql
    AS $$
  DECLARE
    cursor_user refcursor;
  begin

   if p_usuario is not null then

    open cursor_user for
      select u.id_usuario, u.nombre, u.apellido, u.password passUser, e.id_estatus, e.estatus, c.*
      from usuario u inner join estatus e on u.id_estatus=e.id_estatus,
      configuracion c
      where u.id_usuario = p_usuario;

   else

     open cursor_user for
       select u.id_usuario, u.nombre, u.apellido, u.cedula, u.id_estatus, e.estatus
       from usuario u inner join estatus e on u.id_estatus=e.id_estatus
       order by u.id_usuario;

   end if;
    return cursor_user;
    close cursor_user;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.f_verificar_usuario(p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 249 (class 1255 OID 34543)
-- Name: p_actualiza_codigo_combo(integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_codigo_combo(p_codigocombo integer, p_flag character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    if p_flag = '0' then

      update indices set codigo = p_codigocombo where id_indice = p_codigocombo;

    elsif p_flag = '1' then

      update dato_adicional  set codigo = p_codigocombo where id_dato_adicional = p_codigocombo;

    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_codigo_combo(p_codigocombo integer, p_flag character) OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 34556)
-- Name: p_actualiza_nombre_archivo(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_nombre_archivo(p_nombrearchivo character varying, p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update infodocumento set nombre_archivo=p_nombrearchivo
        where id_infodocumento=p_idinfodocumento;
		
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_nombre_archivo(p_nombrearchivo character varying, p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 34582)
-- Name: p_actualiza_numero_da(integer, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_numero_da(p_idvalor integer, p_numerodocumento integer, p_version integer, p_expediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin
  
     update valor_dato_adicional
       set numero = p_numerodocumento
       where id_valor = p_idvalor
         and expediente = p_expediente
         and version=p_version;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_numero_da(p_idvalor integer, p_numerodocumento integer, p_version integer, p_expediente character varying) OWNER TO postgres;

--
-- TOC entry 236 (class 1255 OID 34529)
-- Name: p_actualiza_td_foto(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualiza_td_foto(p_idtipodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update tipodocumento set ficha='1' where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualiza_td_foto(p_idtipodocumento integer) OWNER TO postgres;

--
-- TOC entry 222 (class 1255 OID 34525)
-- Name: p_actualizar_categorias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_categorias(p_idcategoria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update categoria set id_estatus=p_idestatus where id_categoria=p_idcategoria;
  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_categorias(p_idcategoria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 235 (class 1255 OID 34528)
-- Name: p_actualizar_datos_combo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_datos_combo(p_idcombo integer, p_dato character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update lista_desplegables
      set descripcion = p_dato
    where id_lista = p_idcombo;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_datos_combo(p_idcombo integer, p_dato character varying) OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 34574)
-- Name: p_actualizar_indices(character varying, character varying, integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin

    if (p_idexpedienteviejo = p_idexpedientenuevo) then

      update expedientes
         set valor = p_valor
       where id_indice = p_idindice
         and id_libreria = p_idlibreria
         and id_categoria = p_idcategoria;

    else
        insert into expedientes
          (expediente, id_indice, valor, id_libreria, id_categoria)
        values
          (p_idexpedientenuevo, p_idindice, p_valor, p_idlibreria, p_idcategoria);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_indices(p_idexpedientenuevo character varying, p_idexpedienteviejo character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 237 (class 1255 OID 34530)
-- Name: p_actualizar_indices(integer, integer, character varying, character varying, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_indices(p_idindices integer, p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin
    update indices
       set id_categoria = p_idcategoria,
           indice = p_indice,
           tipo = p_tipodato,
           codigo = p_codigo,
           clave = p_clave
     where id_indice = p_idindices;
	 
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_indices(p_idindices integer, p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 34566)
-- Name: p_actualizar_infodocumento(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_infodocumento(p_numerodoc integer, p_idinfodocumento integer, p_idexpediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update infodocumento set numero_documento=p_numerodoc
    where id_infodocumento=p_idinfodocumento and id_expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_infodocumento(p_numerodoc integer, p_idinfodocumento integer, p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 221 (class 1255 OID 34524)
-- Name: p_actualizar_librerias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_librerias(p_idlibreria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update libreria set id_estatus=p_idestatus where id_libreria=p_idlibreria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_librerias(p_idlibreria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 224 (class 1255 OID 34526)
-- Name: p_actualizar_subcategorias(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_subcategorias(p_idsubcategoria integer, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update subcategoria set id_estatus=p_idestatus where id_subcategoria=p_idsubcategoria;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_subcategorias(p_idsubcategoria integer, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 234 (class 1255 OID 34527)
-- Name: p_actualizar_tipodocumento(integer, integer, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_actualizar_tipodocumento(p_idtipodocumento integer, p_idestatus integer, p_vencimiento character, p_datoadicional character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update tipodocumento set id_estatus=p_idestatus, vencimiento=p_vencimiento,
                        dato_adicional=p_datoadicional
   where id_documento=p_idtipodocumento;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_actualizar_tipodocumento(p_idtipodocumento integer, p_idestatus integer, p_vencimiento character, p_datoadicional character) OWNER TO postgres;

--
-- TOC entry 238 (class 1255 OID 34531)
-- Name: p_agrega_usuario(character varying, character varying, character varying, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agrega_usuario(p_idusuario character varying, p_nombre character varying, p_apellido character varying, p_cedula character varying, p_sexo character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into usuario
      (id_usuario, nombre, apellido, cedula, sexo, id_estatus)
    values
      (p_idusuario, p_nombre, p_apellido, p_cedula, p_sexo, 1);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agrega_usuario(p_idusuario character varying, p_nombre character varying, p_apellido character varying, p_cedula character varying, p_sexo character) OWNER TO postgres;

--
-- TOC entry 242 (class 1255 OID 34535)
-- Name: p_agregar_categoria(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_categoria(p_idlibreria integer, p_categoria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into categoria
      (id_libreria, categoria, id_estatus)
    values
      (p_idlibreria, p_categoria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;  
  end;$$;


ALTER FUNCTION public.p_agregar_categoria(p_idlibreria integer, p_categoria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 34581)
-- Name: p_agregar_configuracion(character varying, character varying, integer, character varying, character varying, character, character, character, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_configuracion(p_nombreservidor character varying, p_nombrebasedato character varying, p_puertobasedato integer, p_usuariobasedato character varying, p_passbasedato character varying, p_calidad character, p_foliatura character, p_ficha character, p_fabrica character, p_elimina character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    update configuracion
       set calidad = p_calidad,
           foliatura = p_foliatura,
           server_name = p_nombreservidor,
           database_name = p_nombrebasedato,
           port = p_puertobasedato,
           userbd = p_usuariobasedato,
           password = p_passbasedato,
           ficha = p_ficha,
           fabrica = p_fabrica,
           elimina = p_elimina
     where id_configuracion=1;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_configuracion(p_nombreservidor character varying, p_nombrebasedato character varying, p_puertobasedato integer, p_usuariobasedato character varying, p_passbasedato character varying, p_calidad character, p_foliatura character, p_ficha character, p_fabrica character, p_elimina character) OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 34539)
-- Name: p_agregar_datos_combo(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_datos_combo(p_codigoindice integer, p_dato character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_codigoindice, p_dato);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_datos_combo(p_codigoindice integer, p_dato character varying) OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 34540)
-- Name: p_agregar_datos_combo_da(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_datos_combo_da(p_iddatoadiciona integer, p_datocombo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin


    insert into lista_desplegables
      (codigo_indice, descripcion)
    values
      (p_iddatoadiciona, p_datocombo);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_datos_combo_da(p_iddatoadiciona integer, p_datocombo character varying) OWNER TO postgres;

--
-- TOC entry 240 (class 1255 OID 34533)
-- Name: p_agregar_fabrica(character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_fabrica(p_idusuario character varying, p_pertenece character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into fabrica(usuario, fabrica)
           values(p_idusuario, p_pertenece);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;	   
  end;$$;


ALTER FUNCTION public.p_agregar_fabrica(p_idusuario character varying, p_pertenece character) OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 34579)
-- Name: p_agregar_foliaturas(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_foliaturas(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying, p_pagina integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into foliatura
      (id_infodocumento, id_documento, id_expediente, pagina)
    values
      (p_idinfodocumento, p_iddocumento, p_idexpediente, p_pagina);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_foliaturas(p_idinfodocumento integer, p_iddocumento integer, p_idexpediente character varying, p_pagina integer) OWNER TO postgres;

--
-- TOC entry 245 (class 1255 OID 34538)
-- Name: p_agregar_indices(integer, character varying, character varying, integer, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_indices(p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into indices
      (id_categoria, indice, tipo, codigo, clave)
    values
      (p_idcategoria, p_indice, p_tipodato, p_codigo, p_clave);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_indices(p_idcategoria integer, p_indice character varying, p_tipodato character varying, p_codigo integer, p_clave character) OWNER TO postgres;

--
-- TOC entry 241 (class 1255 OID 34534)
-- Name: p_agregar_libreria(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_libreria(p_libreria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into libreria(libreria, id_estatus)
                values(p_libreria, p_idestatus);
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;			
  end;$$;


ALTER FUNCTION public.p_agregar_libreria(p_libreria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 239 (class 1255 OID 34532)
-- Name: p_agregar_perfil(integer, integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_perfil(p_idlibreria integer, p_idcategoria integer, p_idusuario character varying, p_idrol integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    if p_idcategoria = 0 and p_idlibreria = 0 then

      insert into perfil(id_usuario, id_rol)
                  values(p_idusuario, p_idrol);

    elsif p_idcategoria = 0 then

      insert into perfil(id_libreria, id_usuario, id_rol)
                  values(p_idlibreria, p_idusuario, p_idrol);
    else

      insert into perfil(id_libreria, id_categoria, id_usuario, id_rol)
                  values(p_idlibreria, p_idcategoria, p_idusuario, p_idrol);
    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_perfil(p_idlibreria integer, p_idcategoria integer, p_idusuario character varying, p_idrol integer) OWNER TO postgres;

--
-- TOC entry 290 (class 1255 OID 34555)
-- Name: p_agregar_registro_archivo(character varying, character varying, integer, character varying, integer, integer, character varying, integer, date, character varying, character varying, integer, integer, character, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_registro_archivo(p_accion character varying, p_nombrearchivo character varying, p_iddocumento integer, p_rutaarchivo character varying, p_cantpaginas integer, p_version integer, p_idexpediente character varying, p_numerodoc integer, p_fechavencimiento date, p_datoadicional character varying, p_usuario character varying, p_idinfodocumento integer, p_estatus integer, p_redigitalizo character, p_formato character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE
   p_idinfodoc integer;
   --fecha_actual date;
  begin

   
   p_idinfodoc := 0;

   --select sysdate into fecha_actual from dual;

   if p_accion = 'versionar' then

     insert into infodocumento(id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, 
                               version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado)
                 values(p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo,
                        p_formato, p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus,
                        p_redigitalizo);
	
	 select lastval() into p_idinfodoc;

     if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion, usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, now(), p_usuario, p_datoadicional);
      else
        rollback;
      end if;

    elsif p_accion = 'reemplazar' then

      p_idinfodoc := -1;
      update infodocumento set nombre_archivo=p_nombrearchivo, ruta_archivo=p_rutaarchivo, paginas=p_cantpaginas, 
                               version=p_version, id_expediente=p_idexpediente, numero_documento=p_numerodoc, 
                               fecha_vencimiento=p_fechavencimiento, formato=p_formato, estatus_documento=p_estatus
      where id_infodocumento=p_idinfodocumento;

      update datos_infodocumento set fecha_digitalizacion = now(),
                                     usuario_digitalizo = p_usuario,
                                     dato_adicional = p_datoadicional
       where id_infodocumento=p_idinfodocumento;

    elsif p_accion = 'Guardar' then
       insert into infodocumento(id_documento, id_expediente, nombre_archivo, ruta_archivo, formato, numero_documento, 
                                version, paginas, fecha_vencimiento, estatus_documento, re_digitalizado)
                  values(p_iddocumento, p_idexpediente, p_nombrearchivo, p_rutaarchivo, p_formato, 
			 p_numerodoc, p_version, p_cantpaginas, p_fechavencimiento, p_estatus, 0);
	
	 select lastval() into p_idinfodoc;

      if p_idinfodoc > 0 then
        insert into datos_infodocumento(id_infodocumento, fecha_digitalizacion, usuario_digitalizo, dato_adicional)
                    values(p_idinfodoc, now(), p_usuario, p_datoadicional);

         if p_redigitalizo = '1' then
           update infodocumento t set t.re_digitalizado=p_redigitalizo
              where t.id_infodocumento=p_idinfodocumento;
         end if;
      else
        rollback;
      end if;

    end if;

   return p_idinfodoc;
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
   end;$$;


ALTER FUNCTION public.p_agregar_registro_archivo(p_accion character varying, p_nombrearchivo character varying, p_iddocumento integer, p_rutaarchivo character varying, p_cantpaginas integer, p_version integer, p_idexpediente character varying, p_numerodoc integer, p_fechavencimiento date, p_datoadicional character varying, p_usuario character varying, p_idinfodocumento integer, p_estatus integer, p_redigitalizo character, p_formato character varying) OWNER TO postgres;

--
-- TOC entry 243 (class 1255 OID 34536)
-- Name: p_agregar_subcategoria(integer, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_subcategoria(p_idcategoria integer, p_subcategoria character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into subcategoria
      (id_categoria, subcategoria, id_estatus)
    values
      (p_idcategoria, p_subcategoria, p_idestatus);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_subcategoria(p_idcategoria integer, p_subcategoria character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 244 (class 1255 OID 34537)
-- Name: p_agregar_tipodocumento(integer, integer, character varying, integer, character, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_agregar_tipodocumento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumento character varying, p_idestatus integer, p_vencimiento character, p_datoadicional character) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into tipodocumento
      (id_categoria, id_subcategoria, tipo_documento, id_estatus, vencimiento, dato_adicional)
    values
      (p_idcategoria, p_idsubcategoria, p_tipodocumento, p_idestatus, p_vencimiento, p_datoadicional);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_agregar_tipodocumento(p_idcategoria integer, p_idsubcategoria integer, p_tipodocumento character varying, p_idestatus integer, p_vencimiento character, p_datoadicional character) OWNER TO postgres;

--
-- TOC entry 292 (class 1255 OID 34679)
-- Name: p_aprobar_documento(integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_aprobar_documento(p_idinfodocumento integer, p_usuario character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
    estatu integer;
    idInfoDoc integer;
begin

    update infodocumento set estatus_documento=1 where id_infodocumento=p_idinfodocumento;
    --commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;

    if estatu > 0 then
    
      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;
    
      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_aprobacion = now(),
               usuario_aprobacion = p_usuario
         where id_infodocumento = p_idinfodocumento;
      else
        insert into datos_infodocumento(id_infodocumento, fecha_aprobacion, usuario_aprobacion)
                  values(p_idinfodocumento, now(), p_usuario);
      end if;
      
    else
    
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      --commit;
      
    end if;
    
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'El error fue: %', SQLERRM;
 end;$$;


ALTER FUNCTION public.p_aprobar_documento(p_idinfodocumento integer, p_usuario character varying) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 34573)
-- Name: p_eliminar_expediente(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_expediente(p_idexpediente character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    delete from expedientes where expediente=p_idexpediente;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_expediente(p_idexpediente character varying) OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 34565)
-- Name: p_eliminar_infodocumento(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_infodocumento(p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
  
  begin

    
    delete from datos_infodocumento where id_infodocumento = p_idinfodocumento;
    delete from infodocumento where id_infodocumento = p_idinfodocumento;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_infodocumento(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 220 (class 1255 OID 34523)
-- Name: p_eliminar_perfil(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_perfil(p_idusuario character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   delete from perfil where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_perfil(p_idusuario character varying) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 34563)
-- Name: p_eliminar_registro_archivo(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_registro_archivo(p_idinfodocumento integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   delete from datos_infodocumento where id_infodocumento=p_idinfodocumento;
   delete from infodocumento where id_infodocumento=p_idinfodocumento;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_registro_archivo(p_idinfodocumento integer) OWNER TO postgres;

--
-- TOC entry 283 (class 1255 OID 34584)
-- Name: p_eliminar_valordatadic(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_eliminar_valordatadic(p_idvalor integer, p_versiondoc integer, p_numerodoc integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    
  begin
  
    delete from valor_dato_adicional
     where id_valor = p_idvalor and version=p_versiondoc and numero=p_numerodoc;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_eliminar_valordatadic(p_idvalor integer, p_versiondoc integer, p_numerodoc integer) OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 34542)
-- Name: p_guarda_valor_dato_adicional(integer, integer, character varying, integer, integer, character varying, character); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guarda_valor_dato_adicional(p_iddatoadicional integer, p_idvalor integer, p_valor character varying, p_numerodocumento integer, p_version integer, p_expediente character varying, p_flag character) RETURNS void
    LANGUAGE plpgsql
    AS $$
  begin


    if p_flag = '0' then

      update valor_dato_adicional
         set valor = p_valor
       where id_valor = p_idvalor
         and numero = p_numerodocumento
         and version = p_version
         and expediente = p_expediente;

    elsif p_flag = '1' then

     insert into valor_dato_adicional
        (id_dato_adicional, valor, numero, version, expediente)
      values
        (p_iddatoadicional, p_valor, p_numerodocumento, p_version, p_expediente);
        
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guarda_valor_dato_adicional(p_iddatoadicional integer, p_idvalor integer, p_valor character varying, p_numerodocumento integer, p_version integer, p_expediente character varying, p_flag character) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 34572)
-- Name: p_guardar_expediente(character varying, integer, character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into expedientes
      (expediente, id_indice, valor, id_libreria, id_categoria)
    values
      (p_idexpediente, p_idindice, p_valor, p_idlibreria, p_idcategoria);

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_expediente(p_idexpediente character varying, p_idindice integer, p_valor character varying, p_idlibreria integer, p_idcategoria integer) OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 34541)
-- Name: p_guardar_indice_datoadicional(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_guardar_indice_datoadicional(p_idtipodocumento integer, p_datoadicional character varying, p_tipo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

    insert into dato_adicional
      (indice_adicional, tipo, id_documento)
    values
      (p_datoadicional, p_tipo, p_idtipodocumento);
	  
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_guardar_indice_datoadicional(p_idtipodocumento integer, p_datoadicional character varying, p_tipo character varying) OWNER TO postgres;

--
-- TOC entry 219 (class 1255 OID 34522)
-- Name: p_modificar_usuario(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_modificar_usuario(p_idusuario character varying, p_idestatus integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

  begin

   update usuario set id_estatus=p_idestatus where id_usuario=p_idusuario;
   
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', sql, SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_modificar_usuario(p_idusuario character varying, p_idestatus integer) OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 34552)
-- Name: p_rechazar_documento(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_rechazar_documento(p_idinfodocumento integer, p_usuario character varying, p_causa character varying, p_motivo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
    estatu integer;
    idInfoDoc integer;
  begin

    update infodocumento set estatus_documento=2 where id_infodocumento= p_idinfodocumento;
    --commit;

    select estatus_documento into estatu from infodocumento where id_infodocumento=p_idinfodocumento;


    if estatu > 0 then

      select t.id_infodocumento into idInfoDoc from datos_infodocumento t where t.id_infodocumento=p_idinfodocumento;

      if idInfoDoc = p_idinfodocumento then
        update datos_infodocumento
           set fecha_rechazo = now(),
               usuario_rechazo = p_usuario,
               motivo_rechazo = p_motivo,
               causa_rechazo = p_causa
         where id_infodocumento = p_idinfodocumento;
      else

        insert into datos_infodocumento(id_infodocumento, fecha_rechazo, usuario_rechazo, motivo_rechazo, causa_rechazo)
                  values(p_idinfodocumento, now(), p_usuario, p_motivo, p_causa);
      end if;
    else
      update infodocumento set estatus_documento=0 where id_infodocumento= p_idinfodocumento;
      --commit;

    end if;
	
  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %', SQLERRM;
  end;$$;


ALTER FUNCTION public.p_rechazar_documento(p_idinfodocumento integer, p_usuario character varying, p_causa character varying, p_motivo character varying) OWNER TO postgres;

--
-- TOC entry 291 (class 1255 OID 34567)
-- Name: p_traza_elimina_documento(character varying, integer, integer, integer, integer, integer, date, integer, integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION p_traza_elimina_documento(p_idexpedeinte character varying, p_idlibreria integer, p_idcategoria integer, p_idsubcategoria integer, p_iddocumento integer, p_numerodoc integer, p_fechavencimiento date, p_cantpaginas integer, p_versiondoc integer, p_datoadicional character varying, p_usuarioelimino character varying, p_usuariodigitalizo character varying, p_usuariorechazo character varying, p_causaelimino character varying, p_motivoelimino character varying, p_causarechazo character varying, p_motivorechazo character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
  DECLARE
    --fecha_actual date;
    iddoceliminado integer;

  begin

    
    --select sysdate into fecha_actual from dual;

    insert into documento_eliminado
      (id_expediente, id_libreria, id_categoria, id_subcategoria, id_documento, numero_documento, version, paginas, fecha_vencimiento, fecha_eliminado, usuario_elimino)
    values
      (p_idexpedeinte, p_idlibreria, p_idcategoria, p_idsubcategoria, p_iddocumento, p_numerodoc, p_versiondoc, p_cantpaginas, p_fechavencimiento, now(), p_usuarioelimino);

    select sq_documento_eliminado.currval into iddoceliminado from dual;

    if iddoceliminado > 0 then

      insert into datos_infodocumento
        (id_doc_eliminado, usuario_digitalizo, usuario_rechazo, motivo_rechazo, causa_rechazo, fecha_eliminado, usuario_elimino, motivo_elimino, causa_elimino, dato_adicional)
      values
        (iddoceliminado, p_usuariodigitalizo, p_usuariorechazo, p_motivorechazo, p_causarechazo, now(), p_usuarioelimino, p_motivoelimino, p_causaelimino, p_datoadicional);
    else
      rollback;
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        RAISE EXCEPTION 'Falló la orden SQL: %. El error fue: %', SQLERRM, SQLERRM;
  end;$$;


ALTER FUNCTION public.p_traza_elimina_documento(p_idexpedeinte character varying, p_idlibreria integer, p_idcategoria integer, p_idsubcategoria integer, p_iddocumento integer, p_numerodoc integer, p_fechavencimiento date, p_cantpaginas integer, p_versiondoc integer, p_datoadicional character varying, p_usuarioelimino character varying, p_usuariodigitalizo character varying, p_usuariorechazo character varying, p_causaelimino character varying, p_motivoelimino character varying, p_causarechazo character varying, p_motivorechazo character varying) OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 33206)
-- Name: sq_categroria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_categroria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_categroria OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 33234)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    id_categoria integer DEFAULT nextval('sq_categroria'::regclass) NOT NULL,
    id_libreria integer NOT NULL,
    categoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 33240)
-- Name: causa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE causa (
    id_causa integer NOT NULL,
    causa character varying(150) NOT NULL
);


ALTER TABLE public.causa OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 33245)
-- Name: configuracion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE configuracion (
    id_configuracion integer NOT NULL,
    calidad character(1),
    ruta_temporal character varying(50),
    archivo_tif character varying(50),
    archivo_cod character varying(50),
    log character varying(50),
    foliatura character(1),
    server_name character varying(50),
    database_name character varying(50),
    port integer,
    userbd character varying(50),
    password character varying(50),
    ficha character(1),
    fabrica character(1),
    elimina character(1)
);


ALTER TABLE public.configuracion OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 33210)
-- Name: sq_dato_adicional; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_dato_adicional
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_dato_adicional OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 34609)
-- Name: dato_adicional; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dato_adicional (
    id_dato_adicional integer DEFAULT nextval('sq_dato_adicional'::regclass) NOT NULL,
    indice_adicional character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    id_documento integer NOT NULL,
    codigo integer
);


ALTER TABLE public.dato_adicional OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 33212)
-- Name: sq_datos_infodocumento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_datos_infodocumento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_datos_infodocumento OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 34653)
-- Name: datos_infodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE datos_infodocumento (
    id_datos integer DEFAULT nextval('sq_datos_infodocumento'::regclass) NOT NULL,
    id_infodocumento integer,
    id_doc_eliminado integer,
    fecha_digitalizacion date,
    usuario_digitalizo character varying(30),
    fecha_aprobacion date,
    usuario_aprobacion character varying(30),
    fecha_rechazo date,
    usuario_rechazo character varying(30),
    motivo_rechazo character varying(300),
    causa_rechazo character varying(300),
    fecha_eliminado date,
    usuario_elimino character varying(30),
    motivo_elimino character varying(300),
    causa_elimino character varying(300),
    dato_adicional character varying(30)
);


ALTER TABLE public.datos_infodocumento OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 33214)
-- Name: sq_documento_eliminado; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_documento_eliminado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_documento_eliminado OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 33283)
-- Name: documento_eliminado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documento_eliminado (
    id_doc_eliminado integer DEFAULT nextval('sq_documento_eliminado'::regclass) NOT NULL,
    id_expediente character varying(50) NOT NULL,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    id_documento integer NOT NULL,
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    fecha_eliminado date NOT NULL,
    usuario_elimino character varying(30) NOT NULL
);


ALTER TABLE public.documento_eliminado OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 33332)
-- Name: estatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estatus (
    id_estatus integer NOT NULL,
    estatus character varying(100) NOT NULL
);


ALTER TABLE public.estatus OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 33289)
-- Name: estatus_documento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estatus_documento (
    id_estatus_documento integer NOT NULL,
    estatus_documento character varying(20) NOT NULL
);


ALTER TABLE public.estatus_documento OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 33216)
-- Name: sq_expediente; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_expediente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_expediente OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 33343)
-- Name: expedientes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE expedientes (
    id_expedientes integer DEFAULT nextval('sq_expediente'::regclass) NOT NULL,
    expediente character varying(250) NOT NULL,
    id_indice integer NOT NULL,
    valor character varying(250),
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL
);


ALTER TABLE public.expedientes OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 33372)
-- Name: fabrica; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fabrica (
    usuario character varying(50) NOT NULL,
    fabrica character(1) NOT NULL
);


ALTER TABLE public.fabrica OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 33218)
-- Name: sq_foliatura; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_foliatura
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_foliatura OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 33382)
-- Name: foliatura; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE foliatura (
    id_foliatura integer DEFAULT nextval('sq_foliatura'::regclass) NOT NULL,
    id_infodocumento integer NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50) NOT NULL,
    pagina integer NOT NULL
);


ALTER TABLE public.foliatura OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 33220)
-- Name: sq_indices; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_indices
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_indices OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 33388)
-- Name: indices; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE indices (
    id_indice integer DEFAULT nextval('sq_indices'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    indice character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    codigo integer NOT NULL,
    clave character(1)
);


ALTER TABLE public.indices OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 33222)
-- Name: sq_infodocumento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_infodocumento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_infodocumento OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 34634)
-- Name: infodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE infodocumento (
    id_infodocumento integer DEFAULT nextval('sq_infodocumento'::regclass) NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50),
    nombre_archivo character varying(1000),
    ruta_archivo character varying(1000),
    formato character varying(4),
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    estatus_documento integer NOT NULL,
    re_digitalizado character(1) NOT NULL
);


ALTER TABLE public.infodocumento OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 33224)
-- Name: sq_libreria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_libreria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_libreria OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 33337)
-- Name: libreria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libreria (
    id_libreria integer DEFAULT nextval('sq_libreria'::regclass) NOT NULL,
    libreria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE public.libreria OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 33208)
-- Name: sq_combo; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_combo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_combo OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 33399)
-- Name: lista_desplegables; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lista_desplegables (
    id_lista integer DEFAULT nextval('sq_combo'::regclass) NOT NULL,
    codigo_indice integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE public.lista_desplegables OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 33226)
-- Name: sq_perfil; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_perfil
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_perfil OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 33410)
-- Name: perfil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE perfil (
    id_perfil integer DEFAULT nextval('sq_perfil'::regclass) NOT NULL,
    id_libreria integer,
    id_categoria integer,
    id_usuario character varying(50) NOT NULL,
    id_rol integer NOT NULL
);


ALTER TABLE public.perfil OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 33405)
-- Name: rol; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rol (
    id_rol integer NOT NULL,
    rol character varying(50) NOT NULL
);


ALTER TABLE public.rol OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 33228)
-- Name: sq_subcategroria; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_subcategroria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_subcategroria OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 33230)
-- Name: sq_tipo_documento; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_tipo_documento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_tipo_documento OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 33232)
-- Name: sq_valor_dato_adicional; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_valor_dato_adicional
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 20;


ALTER TABLE public.sq_valor_dato_adicional OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 34587)
-- Name: subcategoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subcategoria (
    id_subcategoria integer DEFAULT nextval('sq_subcategroria'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    subcategoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


ALTER TABLE public.subcategoria OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 34593)
-- Name: tipodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipodocumento (
    id_documento integer DEFAULT nextval('sq_tipo_documento'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    tipo_documento character varying(200) NOT NULL,
    id_estatus integer NOT NULL,
    vencimiento character(1),
    dato_adicional character(1),
    ficha character(1)
);


ALTER TABLE public.tipodocumento OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 33362)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    id_usuario character varying(50) NOT NULL,
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    cedula character varying(50),
    sexo character(1),
    id_estatus integer NOT NULL,
    password character varying(16)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 34620)
-- Name: valor_dato_adicional; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE valor_dato_adicional (
    id_valor integer DEFAULT nextval('sq_valor_dato_adicional'::regclass) NOT NULL,
    id_dato_adicional integer NOT NULL,
    valor character varying(250) NOT NULL,
    numero integer NOT NULL,
    version integer NOT NULL,
    expediente character varying(250) NOT NULL
);


ALTER TABLE public.valor_dato_adicional OWNER TO postgres;

--
-- TOC entry 3068 (class 0 OID 33234)
-- Dependencies: 184
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categoria VALUES (1, 1, 'CategoriaPrueba', 1);


--
-- TOC entry 3069 (class 0 OID 33240)
-- Dependencies: 185
-- Data for Name: causa; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO causa VALUES (1, 'Mala Calidad en la Imagen');
INSERT INTO causa VALUES (2, 'Mala tipificación del Documento');
INSERT INTO causa VALUES (3, 'Los Índices no concuerdan con el Documento');
INSERT INTO causa VALUES (4, 'Mala Orientación del Documento');
INSERT INTO causa VALUES (5, 'Visualización Nula del Documento');
INSERT INTO causa VALUES (6, 'Falla Técnica del Sistema');
INSERT INTO causa VALUES (7, 'Eliminacion de Documento');


--
-- TOC entry 3070 (class 0 OID 33245)
-- Dependencies: 186
-- Data for Name: configuracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO configuracion VALUES (1, '1', 'temp', 'documento.tiff', 'codificado.cod', '/lib/log4j-config.properties', '1', '192.168.0.142', 'dw4j', 5432, 'cG9zdGdyZXM=', 'RGV2ZWxjb20wMQ==', '1', '1', '1');


--
-- TOC entry 3085 (class 0 OID 34609)
-- Dependencies: 201
-- Data for Name: dato_adicional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO dato_adicional VALUES (1, 'DatoTexto', 'TEXTO', 1, NULL);
INSERT INTO dato_adicional VALUES (2, 'DatoNumero', 'NUMERO', 1, NULL);
INSERT INTO dato_adicional VALUES (3, 'DatoFecha', 'FECHA', 1, NULL);
INSERT INTO dato_adicional VALUES (5, 'DatoArea', 'AREA', 1, NULL);
INSERT INTO dato_adicional VALUES (4, 'DatoCombo', 'COMBO', 1, 4);
INSERT INTO dato_adicional VALUES (22, 'DatoTexto', 'TEXTO', 23, NULL);
INSERT INTO dato_adicional VALUES (21, 'DatoCombo1', 'COMBO', 23, 21);
INSERT INTO dato_adicional VALUES (23, 'DatoCombo2', 'COMBO', 23, 23);


--
-- TOC entry 3088 (class 0 OID 34653)
-- Dependencies: 204
-- Data for Name: datos_infodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO datos_infodocumento VALUES (81, 81, NULL, '2014-10-02', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (101, 101, NULL, '2014-10-02', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (121, 121, NULL, '2014-10-02', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (21, 21, NULL, '2014-10-02', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (41, 41, NULL, '2014-10-02', 'eescalona', '2014-10-02', 'eescalona', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO datos_infodocumento VALUES (61, 61, NULL, '2014-10-02', 'eescalona', NULL, NULL, '2014-10-02', 'eescalona', 'prueba de rechazo', 'Falla Técnica del Sistema', NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3071 (class 0 OID 33283)
-- Dependencies: 187
-- Data for Name: documento_eliminado; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3073 (class 0 OID 33332)
-- Dependencies: 189
-- Data for Name: estatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estatus VALUES (1, 'Activo');
INSERT INTO estatus VALUES (2, 'Inactivo');


--
-- TOC entry 3072 (class 0 OID 33289)
-- Dependencies: 188
-- Data for Name: estatus_documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estatus_documento VALUES (0, 'Pendiente');
INSERT INTO estatus_documento VALUES (1, 'Aprobado');
INSERT INTO estatus_documento VALUES (2, 'Rechazado');


--
-- TOC entry 3075 (class 0 OID 33343)
-- Dependencies: 191
-- Data for Name: expedientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO expedientes VALUES (1, '01', 21, '01', 1, 1);
INSERT INTO expedientes VALUES (2, '01', 22, '123456', 1, 1);
INSERT INTO expedientes VALUES (4, '01', 24, 'Valor1', 1, 1);
INSERT INTO expedientes VALUES (3, '01', 23, '02/10/2014', 1, 1);
INSERT INTO expedientes VALUES (5, '01', 25, 'direccion casa', 1, 1);


--
-- TOC entry 3077 (class 0 OID 33372)
-- Dependencies: 193
-- Data for Name: fabrica; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO fabrica VALUES ('eescalona', '0');


--
-- TOC entry 3078 (class 0 OID 33382)
-- Dependencies: 194
-- Data for Name: foliatura; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3079 (class 0 OID 33388)
-- Dependencies: 195
-- Data for Name: indices; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO indices VALUES (21, 1, 'IndiceTexto', 'TEXTO', 0, 'Y');
INSERT INTO indices VALUES (22, 1, 'IndiceNumero', 'NUMERO', 0, 'S');
INSERT INTO indices VALUES (23, 1, 'IndiceFecha', 'FECHA', 0, 'S');
INSERT INTO indices VALUES (25, 1, 'IndiceArea', 'AREA', 0, ' ');
INSERT INTO indices VALUES (24, 1, 'IndiceCombo', 'COMBO', 24, 'S');


--
-- TOC entry 3087 (class 0 OID 34634)
-- Dependencies: 203
-- Data for Name: infodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO infodocumento VALUES (81, 2, '01', '7f7a9c3624bd57588173d9ea1f81f8d943ec517d68b6edd3015b3edc9a11367b-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/0ddc2802b9bbaac437a90f53fb33a02b', 'tiff', 1, 0, 4, '2014-11-26', 0, '0');
INSERT INTO infodocumento VALUES (101, 2, '01', '7f7a9c3624bd57588173d9ea1f81f8d938b3eff8baf56627478ec76a704e9b52-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/0ddc2802b9bbaac437a90f53fb33a02b', 'pdf', 2, 0, 8, '2014-10-16', 0, '0');
INSERT INTO infodocumento VALUES (121, 2, '01', '7f7a9c3624bd57588173d9ea1f81f8d94c56ff4ce4aaf9573aa5dff913df997a-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/0ddc2802b9bbaac437a90f53fb33a02b', 'jpg', 3, 0, 1, '2014-10-27', 0, '0');
INSERT INTO infodocumento VALUES (21, 1, '01', '30cb93ad88f984a5aa46cc18fc0468c63c59dc048e8850243be8079a5c74d079-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/0ddc2802b9bbaac437a90f53fb33a02b', 'tif', 1, 0, 1, '2014-10-31', 0, '0');
INSERT INTO infodocumento VALUES (41, 1, '01', 'd20809a2c90847d096fd7f50a45675a43416a75f4cea9109507cacd8e2f2aefc-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/0ddc2802b9bbaac437a90f53fb33a02b', 'tif', 2, 0, 1, '2014-10-31', 1, '0');
INSERT INTO infodocumento VALUES (61, 1, '01', 'd20809a2c90847d096fd7f50a45675a47f39f8317fbdb1988ef4c628eba02591-0', 'ab08295cea58b40dadba72e25f78b96e/e826d74eaf2294ad1eb78654e2c3449c/0ddc2802b9bbaac437a90f53fb33a02b', 'tif', 3, 0, 1, '2014-12-17', 2, '0');


--
-- TOC entry 3074 (class 0 OID 33337)
-- Dependencies: 190
-- Data for Name: libreria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO libreria VALUES (1, 'LibreriaPrueba', 1);


--
-- TOC entry 3080 (class 0 OID 33399)
-- Dependencies: 196
-- Data for Name: lista_desplegables; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO lista_desplegables VALUES (1, 24, 'Valor1');
INSERT INTO lista_desplegables VALUES (2, 24, 'Valor2');
INSERT INTO lista_desplegables VALUES (3, 24, 'Valor3');
INSERT INTO lista_desplegables VALUES (4, 24, 'Valor4');
INSERT INTO lista_desplegables VALUES (5, 24, 'Valor5');
INSERT INTO lista_desplegables VALUES (21, 4, 'S1T1Dato1');
INSERT INTO lista_desplegables VALUES (22, 4, 'S1T1Dato2');
INSERT INTO lista_desplegables VALUES (23, 4, 'S1T1Dato3');
INSERT INTO lista_desplegables VALUES (24, 4, 'S1T1Dato4');
INSERT INTO lista_desplegables VALUES (25, 4, 'S1T1Dato5');
INSERT INTO lista_desplegables VALUES (41, 21, 'S2T3Combo1Dato1');
INSERT INTO lista_desplegables VALUES (42, 21, 'S2T3Combo1Dato2');
INSERT INTO lista_desplegables VALUES (43, 21, 'S2T3Combo1Dato3');
INSERT INTO lista_desplegables VALUES (44, 21, 'S2T3Combo1Dato4');
INSERT INTO lista_desplegables VALUES (45, 21, 'S2T3Combo1Dato5');
INSERT INTO lista_desplegables VALUES (61, 23, 'S2T3Combo2Dato1');
INSERT INTO lista_desplegables VALUES (62, 23, 'S2T3Combo1Dato2');
INSERT INTO lista_desplegables VALUES (63, 23, 'S2T3Combo1Dato3');
INSERT INTO lista_desplegables VALUES (64, 23, 'S2T3Combo1Dato4');
INSERT INTO lista_desplegables VALUES (65, 23, 'S2T3Combo1Dato5');


--
-- TOC entry 3082 (class 0 OID 33410)
-- Dependencies: 198
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil VALUES (1, NULL, NULL, 'dw4jconf', 7);
INSERT INTO perfil VALUES (2, NULL, NULL, 'dw4jconf', 8);
INSERT INTO perfil VALUES (63, NULL, NULL, 'eescalona', 1);
INSERT INTO perfil VALUES (64, 1, 1, 'eescalona', 4);
INSERT INTO perfil VALUES (65, 1, 1, 'eescalona', 5);
INSERT INTO perfil VALUES (66, 1, 1, 'eescalona', 3);
INSERT INTO perfil VALUES (67, 1, 1, 'eescalona', 2);
INSERT INTO perfil VALUES (68, 1, 1, 'eescalona', 6);
INSERT INTO perfil VALUES (69, 1, 1, 'eescalona', 9);


--
-- TOC entry 3081 (class 0 OID 33405)
-- Dependencies: 197
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rol VALUES (1, 'ADMINISTRADOR');
INSERT INTO rol VALUES (2, 'APROBADOR');
INSERT INTO rol VALUES (3, 'DIGITALIZADOR');
INSERT INTO rol VALUES (4, 'CONSULTAR');
INSERT INTO rol VALUES (5, 'IMPRIMIR');
INSERT INTO rol VALUES (6, 'REPORTES');
INSERT INTO rol VALUES (7, 'CONFIGURADOR');
INSERT INTO rol VALUES (8, 'MANTENIMIENTO');
INSERT INTO rol VALUES (9, 'ELIMINAR');


--
-- TOC entry 3097 (class 0 OID 0)
-- Dependencies: 170
-- Name: sq_categroria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_categroria', 20, true);


--
-- TOC entry 3098 (class 0 OID 0)
-- Dependencies: 171
-- Name: sq_combo; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_combo', 80, true);


--
-- TOC entry 3099 (class 0 OID 0)
-- Dependencies: 172
-- Name: sq_dato_adicional; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_dato_adicional', 40, true);


--
-- TOC entry 3100 (class 0 OID 0)
-- Dependencies: 173
-- Name: sq_datos_infodocumento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_datos_infodocumento', 140, true);


--
-- TOC entry 3101 (class 0 OID 0)
-- Dependencies: 174
-- Name: sq_documento_eliminado; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_documento_eliminado', 1, false);


--
-- TOC entry 3102 (class 0 OID 0)
-- Dependencies: 175
-- Name: sq_expediente; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_expediente', 20, true);


--
-- TOC entry 3103 (class 0 OID 0)
-- Dependencies: 176
-- Name: sq_foliatura; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_foliatura', 1, false);


--
-- TOC entry 3104 (class 0 OID 0)
-- Dependencies: 177
-- Name: sq_indices; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_indices', 40, true);


--
-- TOC entry 3105 (class 0 OID 0)
-- Dependencies: 178
-- Name: sq_infodocumento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_infodocumento', 140, true);


--
-- TOC entry 3106 (class 0 OID 0)
-- Dependencies: 179
-- Name: sq_libreria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_libreria', 20, true);


--
-- TOC entry 3107 (class 0 OID 0)
-- Dependencies: 180
-- Name: sq_perfil; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_perfil', 82, true);


--
-- TOC entry 3108 (class 0 OID 0)
-- Dependencies: 181
-- Name: sq_subcategroria; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_subcategroria', 20, true);


--
-- TOC entry 3109 (class 0 OID 0)
-- Dependencies: 182
-- Name: sq_tipo_documento; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_tipo_documento', 40, true);


--
-- TOC entry 3110 (class 0 OID 0)
-- Dependencies: 183
-- Name: sq_valor_dato_adicional; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_valor_dato_adicional', 60, true);


--
-- TOC entry 3083 (class 0 OID 34587)
-- Dependencies: 199
-- Data for Name: subcategoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO subcategoria VALUES (1, 1, 'SubCategoria1', 1);
INSERT INTO subcategoria VALUES (2, 1, 'SubCategoria2', 1);


--
-- TOC entry 3084 (class 0 OID 34593)
-- Dependencies: 200
-- Data for Name: tipodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipodocumento VALUES (1, 1, 1, 'SubCat1TipoDocumento1', 1, '1', '1', NULL);
INSERT INTO tipodocumento VALUES (2, 1, 1, 'SubCat1TipoDocumento2', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (3, 1, 1, 'SubCat1TipoDocumento3', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (21, 1, 2, 'SubCat2TipoDocumento1', 1, '0', '0', NULL);
INSERT INTO tipodocumento VALUES (22, 1, 2, 'SubCat2TipoDocumento2', 1, '1', '0', NULL);
INSERT INTO tipodocumento VALUES (23, 1, 2, 'SubCat2TipoDocumento3', 1, '0', '1', NULL);


--
-- TOC entry 3076 (class 0 OID 33362)
-- Dependencies: 192
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario VALUES ('dw4jconf', 'Usuario', 'Configurador', NULL, NULL, 1, 'RGV2ZWxjb21HRA==');
INSERT INTO usuario VALUES ('eescalona', 'Escalona', 'Erick', '1040', NULL, 1, NULL);


--
-- TOC entry 3086 (class 0 OID 34620)
-- Dependencies: 202
-- Data for Name: valor_dato_adicional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO valor_dato_adicional VALUES (21, 1, 'ytrewq', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (22, 2, '654321', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (23, 3, '30/11/2014', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (24, 4, 'S1T1Dato1', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (25, 5, 'dato adicional tipo documental 1', 2, 0, '01');
INSERT INTO valor_dato_adicional VALUES (41, 1, 'asdfgh', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (42, 2, '798456', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (43, 3, '19/11/2014', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (44, 4, 'S1T1Dato2', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (45, 5, 'dato adicionales tipo documental 1', 3, 0, '01');
INSERT INTO valor_dato_adicional VALUES (1, 1, 'qwerty', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (2, 2, '123456', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (3, 3, '30/10/2014', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (4, 4, 'S1T1Dato3', 1, 0, '01');
INSERT INTO valor_dato_adicional VALUES (5, 5, 'dato adicionales tipo documental 1, reemplazado', 1, 0, '01');


--
-- TOC entry 2889 (class 2606 OID 33239)
-- Name: categoria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pk PRIMARY KEY (id_categoria);


--
-- TOC entry 2891 (class 2606 OID 33244)
-- Name: causa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY causa
    ADD CONSTRAINT causa_pk PRIMARY KEY (id_causa);


--
-- TOC entry 2895 (class 2606 OID 33288)
-- Name: documento_eliminado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documento_eliminado
    ADD CONSTRAINT documento_eliminado_pk PRIMARY KEY (id_doc_eliminado);


--
-- TOC entry 2897 (class 2606 OID 33293)
-- Name: estatus_documento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estatus_documento
    ADD CONSTRAINT estatus_documento_pk PRIMARY KEY (id_estatus_documento);


--
-- TOC entry 2899 (class 2606 OID 33336)
-- Name: estatus_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estatus
    ADD CONSTRAINT estatus_pk PRIMARY KEY (id_estatus);


--
-- TOC entry 2909 (class 2606 OID 33387)
-- Name: foliatura_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY foliatura
    ADD CONSTRAINT foliatura_pk PRIMARY KEY (id_foliatura);


--
-- TOC entry 2901 (class 2606 OID 33342)
-- Name: libreria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libreria
    ADD CONSTRAINT libreria_pk PRIMARY KEY (id_libreria);


--
-- TOC entry 2917 (class 2606 OID 33415)
-- Name: perfil_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pk PRIMARY KEY (id_perfil);


--
-- TOC entry 2913 (class 2606 OID 33404)
-- Name: pk_combo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lista_desplegables
    ADD CONSTRAINT pk_combo PRIMARY KEY (id_lista);


--
-- TOC entry 2893 (class 2606 OID 33249)
-- Name: pk_configuracion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY configuracion
    ADD CONSTRAINT pk_configuracion PRIMARY KEY (id_configuracion);


--
-- TOC entry 2923 (class 2606 OID 34614)
-- Name: pk_dato_adicional; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT pk_dato_adicional PRIMARY KEY (id_dato_adicional);


--
-- TOC entry 2929 (class 2606 OID 34661)
-- Name: pk_datos_infodocumento; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT pk_datos_infodocumento PRIMARY KEY (id_datos);


--
-- TOC entry 2903 (class 2606 OID 33351)
-- Name: pk_expedientes; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT pk_expedientes PRIMARY KEY (id_expedientes);


--
-- TOC entry 2907 (class 2606 OID 33376)
-- Name: pk_fabrica; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fabrica
    ADD CONSTRAINT pk_fabrica PRIMARY KEY (usuario);


--
-- TOC entry 2911 (class 2606 OID 33393)
-- Name: pk_indice; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT pk_indice PRIMARY KEY (id_indice);


--
-- TOC entry 2927 (class 2606 OID 34642)
-- Name: pk_infordocumento; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT pk_infordocumento PRIMARY KEY (id_infodocumento);


--
-- TOC entry 2925 (class 2606 OID 34628)
-- Name: pk_valor_dato_adicional; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT pk_valor_dato_adicional PRIMARY KEY (id_valor);


--
-- TOC entry 2915 (class 2606 OID 33409)
-- Name: rol_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pk PRIMARY KEY (id_rol);


--
-- TOC entry 2919 (class 2606 OID 34592)
-- Name: subcategoria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subcategoria
    ADD CONSTRAINT subcategoria_pk PRIMARY KEY (id_subcategoria);


--
-- TOC entry 2921 (class 2606 OID 34598)
-- Name: tipodocumento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT tipodocumento_pk PRIMARY KEY (id_documento);


--
-- TOC entry 2905 (class 2606 OID 33366)
-- Name: usuario_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id_usuario);


--
-- TOC entry 2941 (class 2606 OID 34615)
-- Name: fk_dato_adicional_tipodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT fk_dato_adicional_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


--
-- TOC entry 2945 (class 2606 OID 34662)
-- Name: fk_datoc_infodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datoc_infodoc FOREIGN KEY (id_infodocumento) REFERENCES infodocumento(id_infodocumento);


--
-- TOC entry 2946 (class 2606 OID 34667)
-- Name: fk_datos_doc_eliminado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datos_doc_eliminado FOREIGN KEY (id_doc_eliminado) REFERENCES documento_eliminado(id_doc_eliminado);


--
-- TOC entry 2930 (class 2606 OID 33352)
-- Name: fk_expedientes_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2931 (class 2606 OID 33357)
-- Name: fk_expedientes_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


--
-- TOC entry 2933 (class 2606 OID 33377)
-- Name: fk_fabrica_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fabrica
    ADD CONSTRAINT fk_fabrica_usuario FOREIGN KEY (usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2934 (class 2606 OID 33394)
-- Name: fk_indice_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT fk_indice_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2943 (class 2606 OID 34643)
-- Name: fk_infodoc_statusdoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_statusdoc FOREIGN KEY (estatus_documento) REFERENCES estatus_documento(id_estatus_documento);


--
-- TOC entry 2944 (class 2606 OID 34648)
-- Name: fk_infodoc_tipodoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


--
-- TOC entry 2935 (class 2606 OID 33416)
-- Name: fk_perfil_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2936 (class 2606 OID 33421)
-- Name: fk_perfil_libreria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


--
-- TOC entry 2937 (class 2606 OID 33426)
-- Name: fk_perfil_rol; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_rol FOREIGN KEY (id_rol) REFERENCES rol(id_rol);


--
-- TOC entry 2938 (class 2606 OID 33431)
-- Name: fk_perfil_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_usuario FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2939 (class 2606 OID 34599)
-- Name: fk_tipodoc_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


--
-- TOC entry 2940 (class 2606 OID 34604)
-- Name: fk_tipodoc_subcategoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_subcategoria FOREIGN KEY (id_subcategoria) REFERENCES subcategoria(id_subcategoria);


--
-- TOC entry 2932 (class 2606 OID 33367)
-- Name: fk_usuario_estatus; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fk_usuario_estatus FOREIGN KEY (id_estatus) REFERENCES estatus(id_estatus);


--
-- TOC entry 2942 (class 2606 OID 34629)
-- Name: kf_valor_indice_da; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT kf_valor_indice_da FOREIGN KEY (id_dato_adicional) REFERENCES dato_adicional(id_dato_adicional);


--
-- TOC entry 3095 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-10-02 15:06:42

--
-- PostgreSQL database dump complete
--

