/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas;

import com.develcom.autentica.Sesion;
import com.develcom.autentica.Usuario;
import com.develcom.dao.Expediente;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import com.develcom.tools.Propiedades;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.excepcion.DW4JException;
import com.develcom.tools.trazas.Traza;
import java.io.File;
import java.net.ConnectException;
import java.util.List;
import java.util.Properties;
import javax.xml.soap.SOAPException;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import ve.com.develcom.sesion.IniciaSesion;
import ve.com.develcom.tools.Configuracion;

/**
 *
 * @author develcom
 */
public class Login extends SelectorComposer<Component> {

    private static final long serialVersionUID = -107498885186176708L;

    @Wire
    private Combobox usuario;

    @Wire
    private Textbox pass;

    private Herramientas herramientas = new Herramientas();
    Traza traza = new Traza(Login.class);
    private Properties propiedades;
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {
        List<Usuario> users;

        try {

            Configuracion.setWeb(true);
            propiedades = Propiedades.cargarPropiedades();

//            Properties env = System.getProperties();
//
//            Enumeration pro = env.propertyNames();
//            Enumeration prop = env.elements();
//
//            while (pro.hasMoreElements() && prop.hasMoreElements()) {
//                traza.trace("propiedad " + pro.nextElement().toString() + " valor " + prop.nextElement().toString(), Level.INFO);
//            }
            users = new IniciaSesion().autocomplete();

            Comboitem item;

            for (Usuario user : users) {
                item = new Comboitem();
                item.setValue(user.getIdUsuario());
                item.setLabel(user.getIdUsuario());
                item.setParent(usuario);
            }

        } catch (SOAPException | ConnectException ex) {
            traza.trace("Problemas en el inicio de sesion", Level.ERROR, ex);
//            herramientas.error("Problemas en el inicio de sesion\nComuniquese con el administrador del Sistema\n", ex);
            Window win = herramientas.crearVentanaModal("error.zul");
            List<Component> child = win.getChildren();
            for (Component c : child) {
                if (c instanceof Label) {
                    Label msg = (Label) c;
                    msg.setValue("Problemas en el inicio de sesion\nComuniquese con el administrador del Sistema\n" + ex.getMessage());
                }
            }
            win.doModal();
        } catch (SOAPFaultException ex) {
            traza.trace("Problemas en el inicio de sesion", Level.ERROR, ex);
//            herramientas.error("Problemas en el inicio de sesion\nComuniquese con el administrador del Sistema\n", ex);
            Window win = herramientas.crearVentanaModal("error.zul");
            List<Component> child = win.getChildren();
            for (Component c : child) {
                if (c instanceof Label) {
                    Label msg = (Label) c;
                    msg.setValue("Problemas en el inicio de sesion\nComuniquese con el administrador del Sistema\n" + ex.getMessage());
                }
            }
            win.doModal();
        } catch (WebServiceException ex) {
            traza.trace("Problemas en el inicio de sesion", Level.ERROR, ex);
//            herramientas.error("Problemas en el inicio de sesion\nComuniquese con el administrador del Sistema\n", ex);
            Window win = herramientas.crearVentanaModal("error.zul");
            List<Component> child = win.getChildren();
            for (Component c : child) {
                if (c instanceof Label) {
                    Label msg = (Label) c;
                    msg.setValue("Problemas en el inicio de sesion\nComuniquese con el administrador del Sistema\n" + ex.getMessage());
                }
            }
            win.doModal();
        }

    }

    @Listen("onClick = #sesion")
    public void iniciarSesion() {
        com.develcom.autentica.Configuracion configuracion;
        String sesionID, navegador;
        Sesion verifica;
        List<Sesion> sesion;
        //String userdir = System.getProperty("user.dir");
        int tiempoSesion;

        try {
            Expediente expediente = new Expediente();

            if (!pass.getValue().equalsIgnoreCase("") && !usuario.getValue().equalsIgnoreCase("")) {

                traza.trace("ingresando el usuario " + usuario.getValue(), Level.INFO);

                traza.trace("validando el usuario", Level.INFO);

                verifica = new IniciaSesion().iniciarSesion(usuario.getValue(), pass.getValue());

                if (!verifica.getVerificar().equalsIgnoreCase("ldap")) {
                    if (!verifica.getVerificar().equalsIgnoreCase("basedato")) {
                        if (!verifica.getVerificar().equalsIgnoreCase("inactivo")) {

                            if (verifica.getVerificar().equalsIgnoreCase("exito")) {

                                configuracion = verifica.getConfiguracion();
                                traza.trace("creando la sesion del usuario", Level.INFO);

                                sesion = new IniciaSesion().armarSesion(usuario.getValue());
                                //ManejoSesion.setSesion(sesion);

                                if ((sesion != null) && (sesion.size() >= 1)) {
                                    //ManejoSesion.setLogin(sesion.get(0).getIdUsuario());

                                    traza.trace("id de la sesion webService " + sesion.get(0).getIdSession(), Level.INFO);
                                    traza.trace("fecha y hora de inicio de la sesion " + sesion.get(0).getFechaHora(), Level.INFO);
                                    traza.trace("estado del usuario " + sesion.get(0).getEstatusUsuario(), Level.INFO);

                                    if (sesion.get(0).getEstatusUsuario().equalsIgnoreCase(Constantes.ACTIVO)) {

                                        session = herramientas.crearSesion();

                                        for (Sesion s : sesion) {
                                            expediente.agregaRolUsuario(s.getRolUsuario().getRol());
                                        }

                                        session.setAttribute("expediente", expediente);

                                        sesionID = herramientas.getID();
                                        navegador = herramientas.getNavegador();

                                        traza.trace("navegador " + navegador, Level.INFO);
                                        traza.trace("id sesion del usuario " + usuario + " " + sesionID, Level.INFO);
                                        traza.trace("directorio " + session.getWebApp().getDirectory(), Level.INFO);

                                        session.setAttribute("configuracion", configuracion);
                                        session.setAttribute("login", usuario.getValue());
                                        session.setAttribute("sesionID", sesionID);
                                        session.setAttribute("propiedades", propiedades);
                                        tiempoSesion = Integer.parseInt(propiedades.getProperty("timeSession"));
                                        session.setMaxInactiveInterval(tiempoSesion);
                                        session.setAttribute("navegador", navegador);

                                        if (new ToolsFiles().getDirTemporal().exists()) {

                                            File[] files = new ToolsFiles().getDirTemporal().listFiles();
                                            for (File f : files) {
                                                if (f.delete()) {
                                                    traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                                                } else {
                                                    traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                                                    f.deleteOnExit();
                                                }
                                            }
                                        }

                                        herramientas.navegarPagina("principal.zul");

                                    } else {
                                        herramientas.info("Usuario Inactivo");
                                        usuario.setText("");
                                        pass.setText("");
                                    }

                                } else {
                                    herramientas.info("Problemas para crear la sesion \ncomuniquese con el administrador del sistema");
                                    usuario.setText("");
                                    pass.setText("");
                                }

                            } else {
                                herramientas.info(verifica.getVerificar() + "\ncomuniquese con el administrador del sistema\n");
                                usuario.setText("");
                                pass.setText("");
                            }

                        } else {
                            herramientas.info("Usuario esta inactivo para el Sistema Gestion Documental\ncomuniquese con el administrador del sistema\n" + verifica.getRespuesta());
                            usuario.setText("");
                            pass.setText("");
                        }

                    } else {
                        herramientas.info("Usuario no registrado en el Sistema Gestion Documental\ncomuniquese con el administrador del sistema\n" + verifica.getRespuesta());
                        usuario.setText("");
                        pass.setText("");
                    }
                } else {
                    herramientas.info(verifica.getRespuesta() + "\ncomuniquese con el administrador del sistema");
                    usuario.setText("");
                    pass.setText("");
                }
            } else {
                herramientas.info("Debe INGRESAR USUARIO y CONTRASEÑA");
                usuario.setText("");
                pass.setText("");
            }
        } catch (WrongValueException | SOAPException | SOAPFaultException | NumberFormatException | DW4JException ex) {
            herramientas.error("Error en el inicio de sesion ", ex);
            traza.trace("error en el inicio de sesion", Level.ERROR, ex);
        }

    }

    @Listen("onOK = #pass")
    public void aceptar() {
        iniciarSesion();
    }

}
