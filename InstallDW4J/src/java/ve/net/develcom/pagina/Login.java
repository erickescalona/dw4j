/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ve.net.develcom.pagina;

import com.develcom.autentica.Sesion;
import com.develcom.autentica.Usuario;
import java.net.ConnectException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Textbox;
import ve.com.develcom.sesion.IniciaSesion;
import ve.com.develcom.tools.Configuracion;
import ve.net.develcom.dao.Expediente;
import ve.net.develcom.log.Traza;
import ve.net.develcom.tool.Constantes;
import ve.net.develcom.tool.Herramientas;
import ve.net.develcom.tool.Propiedades;

/**
 *
 * @author develcom
 */
public class Login extends SelectorComposer<Component> {

    private static final long serialVersionUID = -1910267193876326538L;

    private String navegador;
    @Wire
    private Combobox usuario;
    @Wire
    private Textbox contrasenia;
    private Traza traza = new Traza(Login.class);
    private Session session;
    private Properties propiedades;
    private Herramientas herramientas = new Herramientas();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    public void iniciar() {
        List<Usuario> users;
        try {
            Configuracion.setWeb(true);
            propiedades = new Propiedades().cargarPropiedades();

            users = new IniciaSesion().autocomplete();

            Comboitem item;

            for (Usuario user : users) {
                item = new Comboitem();
                item.setValue(user.getIdUsuario());
                item.setLabel(user.getIdUsuario());
                item.setParent(usuario);
            }

            navegador = herramientas.navegador();

            int ff = navegador.indexOf("Firefox");
            int ie = navegador.indexOf("MSIE");
            int chro = navegador.indexOf("Chrome");

            if (ff != -1) {
                navegador = navegador.substring(ff, (ff + 7));
            } else if (ie != -1) {
                navegador = navegador.substring(ie, (ie + 4));
            } else if (chro != -1) {
                navegador = navegador.substring(chro, (chro + 6));
            }
            traza.trace("navegador " + navegador, Level.INFO);
        } catch (SOAPException | SOAPFaultException | ConnectException ex) {
            herramientas.showError("Problemas en el inicio de sesion\nComuniquese con el administrador del Sistema\n", ex);
        }
    }

    //@Command
    @Listen("onClick = #iniciar")
    public void iniciarSesion() {
        String resp = "", sesionID, user, pass;
        Sesion verifica;
        List<Sesion> sesion;
        boolean consulta = false, ficha;
        //String userdir = System.getProperty("user.dir");
        int tiempoSesion;

        try {
            Expediente expediente = new Expediente();

            user = usuario.getSelectedItem().getValue().toString();
            pass = contrasenia.getValue();

            if (!pass.equalsIgnoreCase("") && !user.equalsIgnoreCase("")) {

                traza.trace("ingresando el usuario " + usuario, Level.INFO);

                traza.trace("validando el usuario", Level.INFO);

                verifica = new IniciaSesion().iniciarSesion(user, pass);

                if (!verifica.getVerificar().equalsIgnoreCase("ldap")) {
                    if (!verifica.getVerificar().equalsIgnoreCase("basedato")) {
                        if (!verifica.getVerificar().equalsIgnoreCase("inactivo")) {

                            if (verifica.getVerificar().equalsIgnoreCase("exito")) {

                                traza.trace("creando la sesion del usuario", Level.INFO);

                                sesion = new IniciaSesion().armarSesion(user);
                                //ManejoSesion.setSesion(sesion);

                                if ((sesion != null) && (sesion.size() >= 1)) {
                                    //ManejoSesion.setLogin(sesion.get(0).getIdUsuario());

                                    traza.trace("id de la sesion webService " + sesion.get(0).getIdSession(), Level.INFO);
                                    traza.trace("fecha y hora de inicio de la sesion " + sesion.get(0).getFechaHora(), Level.INFO);

                                    if (sesion.get(0).getEstatusUsuario().equalsIgnoreCase(Constantes.ACTIVO)) {

                                        session = herramientas.crearSesion();

                                        for (Sesion s : sesion) {

                                            if (s.getRolUsuario().getRol().equalsIgnoreCase(Constantes.ADMINISTRADOR)) {
                                                consulta = true;
                                                break;
                                            } else {
                                                consulta = false;
                                            }
                                        }
                                        for (Sesion s : sesion) {
                                            user = s.getIdUsuario();
                                            if (s.getRolUsuario().getRol().equalsIgnoreCase(Constantes.IMPRIMIR)) {
                                                expediente.setRolUsuario(Constantes.IMPRIMIR);
                                                session.setAttribute("expediente", expediente);
                                                break;
                                            }
//                                            else{
//                                                session.setAttribute("expediente", expediente);
//                                            }
                                        }
                                        traza.trace("consulta " + consulta, Level.INFO);
                                        if (consulta) {

//                                            sesionID = session.getId();
                                            ficha = verifica.getConfiguracion().isFicha();

//                                            traza.trace("id sesion del usuario " + usuario + " " + sesionID, Level.INFO);
                                            traza.trace("configuracion ficha " + ficha, Level.INFO);

                                            session.setAttribute("ficha", ficha);
                                            session.setAttribute("login", usuario);
//                                            session.setAttribute("sesionID", sesionID);
                                            session.setAttribute("propiedades", propiedades);
                                            tiempoSesion = Integer.parseInt(propiedades.getProperty("timeSession"));
                                            session.setMaxInactiveInterval(tiempoSesion);
                                            session.setAttribute("navegador", navegador);

//                                            traza.trace("id de la sesion aplicacion web " + sesionID, Level.INFO);
                                            herramientas.pageRedirection("install.zul");

                                        } else {
                                            herramientas.showWarn("El usuario " + usuario + " no tiene rol de CONSULTAR");
                                            traza.trace("El usuario " + usuario + " no tiene rol de CONSULTAR", Level.INFO);
                                        }
                                    } else {
                                        herramientas.showWarn("Usuario Inactivo");
                                        //throw new Exception("Usuario Inactivo");
                                    }

                                } else {
                                    herramientas.showWarn("Problemas para crear la sesion \ncomuniquese con el administrador del sistema");
                                    //throw new Exception("Problemas para crear la crearSesion \ncomuniquese con el administrador del sistema");
                                }

                            } else {
                                herramientas.showWarn(verifica.getVerificar() + "\ncomuniquese con el administrador del sistema\n");
                                //throw new Exception(verifica.getVerificar() + "\ncomuniquese con el administrador del sistema\n");
                            }

                        } else {
                            herramientas.showWarn("Usuario esta inactivo para el Sistema Gestion Documental\ncomuniquese con el administrador del sistema\n" + verifica.getRespuesta());
                            //throw new Exception("Usuario esta inactivo para el Sistema Gestion Documental\ncomuniquese con el administrador del sistema\n" + verifica.getRespuesta());
                        }

                    } else {
                        herramientas.showWarn("Usuario no registrado en el Sistema Gestion Documental\ncomuniquese con el administrador del sistema\n" + verifica.getRespuesta());
                        //throw new Exception("Usuario no registrado en el Sistema Gestion Documental\ncomuniquese con el administrador del sistema\n" + verifica.getRespuesta());
                    }
                } else {
                    herramientas.showWarn(verifica.getRespuesta() + "\ncomuniquese con el administrador del sistema");
                    //throw new Exception(verifica.getRespuesta() + "\ncomuniquese con el administrador del sistema");
                }
            } else {
                herramientas.showWarn("Debe INGRESAR USUARIO y CONTRASEÑA");
                //throw new Exception("Debe INGRESAR USUARIO y CONTRASEÑA");
            }
        } catch (Exception ex) {
            herramientas.showError("", ex);
            traza.trace("error en el inicio de sesion", Level.ERROR, ex);
        }

    }

}
