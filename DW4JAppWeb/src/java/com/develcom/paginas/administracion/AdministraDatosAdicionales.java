/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.DatoAdicional;
import com.develcom.administracion.Libreria;
import com.develcom.administracion.SubCategoria;
import com.develcom.administracion.TipoDocumento;
import com.develcom.excepcion.DW4JDesktopExcepcion;
import com.develcom.paginas.menu.MenuPrincipal;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;

/**
 *
 * @author develcom
 */
public class AdministraDatosAdicionales extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibrerias;

    @Wire
    private Combobox cmbCategorias;

    @Wire
    private Combobox cmbSubCategorias;

    @Wire
    private Combobox cmbTipoDocumento;

    @Wire
    private Listbox lstDatosAdicionalesExistentes;

    @Wire
    private Textbox txtNombreDatoAdicional;

    @Wire
    private Combobox cmbTipoDato;

    @Wire
    private Listbox lstDatosAdicionalesNuevos;

    private static final long serialVersionUID = -4263119134392998193L;
    private List<Libreria> listaLibrerias;
    private List<Categoria> listaCategorias;
    private List<SubCategoria> listaSubCategorias;
    private List<TipoDocumento> listaTipoDocumentos;
    private List<DatoAdicional> lsDatosAdicionalesExistente;
    private int idTipoDocumento;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraDatosAdicionales.class);
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }
    }

    private void llenarLibreria() {

        Comboitem item;

        try {

            cmbLibrerias.getItems().clear();

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {
                    if (lib.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(lib.getIdLibreria());
                        item.setLabel(lib.getDescripcion());
                        item.setParent(cmbLibrerias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Librerias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onSelect = #cmbLibrerias")
    public void llenarCategoria() {

        Comboitem item;
        int idLibreria;

        try {

            cmbCategorias.setText("");
            cmbSubCategorias.setText("");
            cmbTipoDocumento.setText("");
            cmbCategorias.getItems().clear();

            idLibreria = cmbLibrerias.getSelectedItem().getValue();
            listaCategorias = new AdministracionBusqueda().buscarCategoria("", idLibreria, 0);

            if (!listaCategorias.isEmpty()) {
                for (Categoria cat : listaCategorias) {
                    if (cat.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(cat.getIdCategoria());
                        item.setLabel(cat.getCategoria());
                        item.setParent(cmbCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Categorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onSelect = #cmbCategorias")
    public void llenarSubCategoria() {

        Comboitem item;
        int idCategoria;

        try {

            cmbSubCategorias.setText("");
            cmbTipoDocumento.setText("");
            cmbSubCategorias.getItems().clear();

            idCategoria = cmbCategorias.getSelectedItem().getValue();
            listaSubCategorias = new AdministracionBusqueda().buscarSubCategoria("", idCategoria, 0);

            if (!listaSubCategorias.isEmpty()) {
                for (SubCategoria subCat : listaSubCategorias) {
                    if (subCat.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(subCat.getIdSubCategoria());
                        item.setLabel(subCat.getSubCategoria());
                        item.setParent(cmbSubCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las SubCategorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las subcategorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las subcategorias", ex);
        }
    }

    @Listen("onSelect = #cmbSubCategorias")
    public void llenarTipoDocumento() {

        Comboitem item;
        int idcategoria, idsubcategoria;
        int noHay = 0;

        try {

            cmbTipoDocumento.setText("");
            cmbTipoDocumento.getItems().clear();

            idcategoria = cmbCategorias.getSelectedItem().getValue();
            idsubcategoria = cmbSubCategorias.getSelectedItem().getValue();
            listaTipoDocumentos = new AdministracionBusqueda().buscarTipoDocumento("", idcategoria, idsubcategoria);

            if (!listaTipoDocumentos.isEmpty()) {
                for (TipoDocumento td : listaTipoDocumentos) {
                    if (td.getEstatus().equalsIgnoreCase("Activo")) {
                        if (td.getDatoAdicional().equalsIgnoreCase("1")) {
                            item = new Comboitem();
                            item.setValue(td.getIdTipoDocumento());
                            item.setLabel(td.getTipoDocumento());
                            item.setParent(cmbTipoDocumento);
                        } else {
                            noHay++;
                        }
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar los Tipos de Documentos (lista vacia)");
            }

            if (noHay == listaTipoDocumentos.size()) {
                herramientas.warn("La SubCategoria " + cmbSubCategorias.getSelectedItem().getLabel() + " no tiene tipos de Documentos con Datos Adicionales");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando los tipos de documentos", Level.ERROR, ex);
            herramientas.error("Problemas buscando los Tipos de Documentos", ex);
        }
    }

    @Listen("onSelect = #cmbTipoDocumento")
    public void llenarDatosAdicionalesExistentes() {

        Listitem item;
        Listcell cell;

        try {

            lstDatosAdicionalesExistentes.getItems().clear();

            idTipoDocumento = cmbTipoDocumento.getSelectedItem().getValue();

            lsDatosAdicionalesExistente = new AdministracionBusqueda().buscarIndDatoAdicional(idTipoDocumento);

            if (!lsDatosAdicionalesExistente.isEmpty()) {
                for (DatoAdicional da : lsDatosAdicionalesExistente) {

                    item = new Listitem();

                    cell = new Listcell(da.getIndiceDatoAdicional());
                    item.appendChild(cell);

                    cell = new Listcell(da.getTipo());
                    item.appendChild(cell);

                    item.setParent(lstDatosAdicionalesExistentes);
                }
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando los datos adicionales", Level.ERROR, ex);
            herramientas.error("Problemas buscando los datos adicionales", ex);
        }
    }

    @Listen("onClick = #btnAgregar")
    public void agregarDatosAdicionalesNuevos() {
        String datoadicionalNuevo, tipo;
        boolean banLista = true;
        String libreria, categoria, subCategoria, tipoDocumento;
        Listitem item;
        Listcell cell;

        try {

            libreria = cmbLibrerias.getSelectedItem().getLabel();
            categoria = cmbCategorias.getSelectedItem().getLabel();
            subCategoria = cmbSubCategorias.getSelectedItem().getLabel();
            tipoDocumento = cmbTipoDocumento.getSelectedItem().getLabel();
            datoadicionalNuevo = txtNombreDatoAdicional.getValue();
            tipo = cmbTipoDato.getSelectedItem().getLabel();

            if (!libreria.equalsIgnoreCase("")) {
                if (!categoria.equalsIgnoreCase("")) {
                    if (!subCategoria.equalsIgnoreCase("")) {
                        if (!tipoDocumento.equalsIgnoreCase("")) {
                            if (!datoadicionalNuevo.equalsIgnoreCase("")) {

                                item = new Listitem();

                                cell = new Listcell(datoadicionalNuevo);
                                item.appendChild(cell);

                                cell = new Listcell(tipo);
                                item.appendChild(cell);

                                for (DatoAdicional arg : lsDatosAdicionalesExistente) {
                                    String argu = arg.getIndiceDatoAdicional();
                                    if (argu.equalsIgnoreCase(datoadicionalNuevo)) {
                                        banLista = false;
                                        break;
                                    } else {
                                        banLista = true;
                                    }
                                }

                                if (banLista) {
                                    item.setParent(lstDatosAdicionalesNuevos);
                                    txtNombreDatoAdicional.setText("");
                                    cmbTipoDato.setText("");
                                } else {
                                    herramientas.warn("El Dato Adicional ya existe");
                                }
                            }
                        } else {
                            throw new DW4JDesktopExcepcion("Debe seleccionar un tipo de documento");
                        }
                    } else {
                        throw new DW4JDesktopExcepcion("Debe seleccionar una subcategoria");
                    }
                } else {
                    throw new DW4JDesktopExcepcion("Debe seleccionar una categoria");
                }
            } else {
                throw new DW4JDesktopExcepcion("Debe seleccionar una libreria");
            }
        } catch (DW4JDesktopExcepcion e) {
            traza.trace("", Level.ERROR, e);
            herramientas.error("", e);
        } catch (Exception e) {
            traza.trace("error generla agregando el nuevo dato adicional", Level.ERROR, e);
            herramientas.error("Error generla agregando el nuevo dato adicional", e);
        }
    }

    @Listen("onClick = #btnGuardar")
    public void guardarDatosAdicionales() {

        List<DatoAdicional> lsDatoAdicional = new ArrayList<>();
        DatoAdicional da;
        List<Listitem> items;
        List<Listcell> cells;
        boolean resp;

        try {

            items = lstDatosAdicionalesNuevos.getItems();

            for (Listitem item : items) {
                cells = item.getChildren();

                da = new DatoAdicional();
                da.setIdTipoDocumento(idTipoDocumento);
                da.setIndiceDatoAdicional(cells.get(0).getLabel());
                da.setTipo(cells.get(1).getLabel());

                traza.trace("agregando dato adicional " + da.getIndiceDatoAdicional() + " tipo " + da.getTipo(), Level.INFO);

                lsDatoAdicional.add(da);
            }

            resp = new AdministracionAgregar().insertarIndiceDatoAdicional(lsDatoAdicional);

            if (resp) {
                herramientas.info("Datos Adicionales agregados con exito");
            } else {
                herramientas.warn("Problemas al agregar los Datos Adicionales");
            }
            limpiarComponentes();

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al agregar los valores", Level.ERROR, ex);
            herramientas.error("Problemas al agregar los valores", ex);
        }
    }

    private void limpiarComponentes() {

        cmbLibrerias.setText("");
        cmbCategorias.setText("");
        cmbSubCategorias.setText("");
        cmbTipoDocumento.setText("");
        cmbTipoDato.setText("");
        lstDatosAdicionalesExistentes.getItems().clear();
        lstDatosAdicionalesNuevos.getItems().clear();
        llenarLibreria();
    }

}
