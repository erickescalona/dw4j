/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.Fabrica;
import com.develcom.administracion.Libreria;
import com.develcom.administracion.Perfil;
import com.develcom.administracion.Rol;
import com.develcom.administracion.Sesion;
import com.develcom.autentica.Configuracion;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;
import ve.com.develcom.administracion.AdministracionModifica;
import ve.com.develcom.tools.Traza;

/**
 *
 * @author develcom
 */
public class AdministraUsuario extends SelectorComposer<Component> {

    @Wire
    private Window winAdminUsuarios;

    @Wire
    private Textbox txtUsuario;

    @Wire
    private Combobox cmbLibreria;

    @Wire
    private Label lblMensajeUser;

    @Wire
    private Listbox lstCategorias;

    @Wire
    private Label lblMensaje;

    @Wire
    private Checkbox chkDigitalizador;

    @Wire
    private Checkbox chkImprimir;

    @Wire
    private Checkbox chkReportes;

    @Wire
    private Checkbox chkEliminar;

    @Wire
    private Checkbox chkAdministrador;

    @Wire
    private Checkbox chkConsultar;

    @Wire
    private Checkbox chkAprobador;

    @Wire
    private Radio rdActivo;

    @Wire
    private Radio rdInactivo;

    @Wire
    private Checkbox chkPerteneceFabrica;

//    @Wire
//    private Grid grdConfiguracion;
    @Wire
    private Listbox lstPerfil;

    @Wire
    private Label lblEstatus;

    @Wire
    private Button btnGuardar;

    @Wire
    private Button btnAgregar;

    @Wire
    private Button btnEliminarFilas;

    private boolean existe = false;
    private boolean fabrica;
    private boolean elimina;
    private Configuracion configuracion;
    private String usuarioBuscado;
    private List<Sesion> perfilActual;
    private String estatusActualUsuario = "";
    private List<Perfil> listaPerfil;
    private static final long serialVersionUID = 478075162513654390L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraUsuario.class);
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            configuracion = (Configuracion) session.getAttribute("configuracion");

            fabrica = configuracion.isFabrica();
            elimina = configuracion.isElimina();

            llenarLibreria();
            inactivarComponentes();

        } else {
            herramientas.warn("Sesión vencida");
            herramientas.navegarPagina("index.zul");
        }

    }

    private void llenarLibreria() {
        Comboitem item;
        String tmp = "";

        try {
            listaPerfil = new AdministracionBusqueda().buscarLibCat();

            if (!listaPerfil.isEmpty()) {
                for (Perfil perfil : listaPerfil) {
                    if (!tmp.equalsIgnoreCase(perfil.getLibreria().getDescripcion())) {
                        tmp = perfil.getLibreria().getDescripcion();
                        item = new Comboitem();
                        item.setValue(perfil.getLibreria().getIdLibreria());
                        item.setLabel(perfil.getLibreria().getDescripcion());
                        item.setParent(cmbLibreria);
                    }
                }
            }

        } catch (SOAPException ex) {
            traza.trace("error soap en el webservice", Level.ERROR, ex);
        } catch (SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onClick = #btnComprobarUsuario")
    public void comprobarUsuario() {

//        Rows rows;
        List<Listitem> items;

        try {

            if (session != null) {

                items = lstPerfil.getItems();
//                rows = grdConfiguracion.getRows();
                usuarioBuscado = txtUsuario.getValue();

                Sesion sesion = new AdministracionBusqueda().compruebaUsuario(usuarioBuscado);

                estatusActualUsuario = sesion.getEstatusUsuario();
                traza.trace("estatus usuario " + estatusActualUsuario, Level.INFO);

                if (estatusActualUsuario == null) {
                    estatusActualUsuario = "";
                }

                if (!items.isEmpty()) {

                    int n = Messagebox.show("Configuracion actual no se guardo\n¿Desea continuar?",
                            "Alerta", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                            new EventListener() {

                                @Override
                                public void onEvent(Event evt) throws Exception {

                                }
                            });

                    if (n == Messagebox.YES) {

                        if (!usuarioBuscado.equalsIgnoreCase("dw4jconf")) {

                            traza.trace("comprobar usuario: " + sesion.getRespuesta(), Level.INFO);

                            if (sesion.getVerificar().equalsIgnoreCase("exito")) {
                                if (llenarTabla()) {
                                    if (!sesion.getIdUsuario().equalsIgnoreCase(txtUsuario.getValue())) {
                                        btnGuardar.setLabel("Modificar");
                                        btnEliminarFilas.setVisible(true);
                                        existe = true;
                                        activarComponentes();
                                    } else {
                                        lblMensajeUser.setClass("letra-negrita");;
                                        lblMensajeUser.setValue("El usuario " + sesion.getIdUsuario() + " ya inicio sesion, los cambios son efectivo de inmediato excepto con el perfil de administrador");
                                        btnGuardar.setLabel("Modificar");
                                        btnEliminarFilas.setVisible(true);
                                        existe = true;
                                        activarComponentes();
                                    }

                                } else {
                                    herramientas.warn("Problemas al llenar la tabla perfil del usuario " + usuarioBuscado);
                                }
                            } else if (sesion.getVerificar().equalsIgnoreCase("basedato")) {

                                existe = false;
                                traza.trace(sesion.getRespuesta(), Level.INFO);
                                lblMensajeUser.setClass("letra-negrita");;
                                lblMensajeUser.setValue(sesion.getRespuesta() + "Proceda a configurarlo para registrarlo en el sistema");
                                activarComponentes();
                                btnGuardar.setLabel("Guardar");
                                btnEliminarFilas.setVisible(false);

                            } else if (sesion.getVerificar().equalsIgnoreCase("ldap")) {

                                existe = false;
                                lblMensajeUser.setClass("letra-roja");
                                lblMensajeUser.setValue(sesion.getRespuesta());
                                inactivarComponentes();
                            }

                        } else {
                            herramientas.warn("El usuario " + usuarioBuscado + " no se puede configurar");
                        }
                    }

                } else {

                    if (!usuarioBuscado.equalsIgnoreCase("dw4jconf")) {

                        traza.trace("comprobar usuario: " + sesion.getRespuesta(), Level.INFO);

                        if (sesion.getVerificar().equalsIgnoreCase("exito")) {
                            if (llenarTabla()) {
                                if (!sesion.getIdUsuario().equalsIgnoreCase(txtUsuario.getValue())) {
                                    btnGuardar.setLabel("Modificar");
                                    btnEliminarFilas.setVisible(true);
                                    existe = true;
                                    activarComponentes();
                                } else {
//                                    lblMensajeUser.setForeground(Color.black);
                                    lblMensajeUser.setValue("El usuario " + sesion.getIdUsuario() + " ya inicio sesion, los cambios son efectivo de inmediato excepto con el perfil de administrador");
                                    btnGuardar.setLabel("Modificar");
                                    btnEliminarFilas.setVisible(true);
                                    existe = true;
                                    activarComponentes();
                                }

                            } else {
                                herramientas.warn("Problemas al llenar la tabla perfil del usuario " + usuarioBuscado);
                            }
                        } else if (sesion.getVerificar().equalsIgnoreCase("basedato")) {

                            existe = false;
                            traza.trace(sesion.getRespuesta(), Level.INFO);
//                            lblMensajeUser.setForeground(Color.black);
                            lblMensajeUser.setValue(sesion.getRespuesta() + "Proceda a configurarlo para registrarlo en el sistema");
                            activarComponentes();
                            btnGuardar.setLabel("Guardar");
                            btnEliminarFilas.setVisible(false);

                        } else if (sesion.getVerificar().equalsIgnoreCase("ldap")) {

                            existe = false;
//                            lblMensajeUser.setForeground(Color.red);
                            lblMensajeUser.setValue(sesion.getRespuesta());
                            inactivarComponentes();
                        }

                    } else {
                        herramientas.warn("El usuario " + usuarioBuscado + " no se puede configurar");
                    }
                }

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException ex) {
            traza.trace("error soap en el webservice", Level.ERROR, ex);
        } catch (SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    private boolean llenarTabla() {

        String fabric = "";
        boolean resp = false;

        Listitem item;
        Listcell cell;

        try {

            if (session != null) {

//                rows = grdConfiguracion.getRows();
                perfilActual = new AdministracionBusqueda().buscandoPerfil(txtUsuario.getText());

                traza.trace("tamaño lista perfil del usuario " + usuarioBuscado + " " + perfilActual.size(), Level.INFO);

                if (!perfilActual.isEmpty()) {
                    resp = true;

                    if (fabrica) {
                        if (perfilActual.get(0).getFabrica().isPertenece()) {
                            chkPerteneceFabrica.setChecked(true);
                            fabric = "SI";
                        } else {
                            chkPerteneceFabrica.setChecked(false);
                            fabric = "NO";
                        }

                    }

                    for (Sesion ses : perfilActual) {

                        item = new Listitem();

                        try {

                            if (ses.getLibreria() != null) {

                                cell = new Listcell(ses.getIdUsuario());
                                item.appendChild(cell);

                                cell = new Listcell(ses.getRolUsuario().getRol());
                                item.appendChild(cell);

                                cell = new Listcell(ses.getLibreria().getDescripcion());
                                item.appendChild(cell);

                                cell = new Listcell(ses.getCategoria().getCategoria());
                                item.appendChild(cell);

                                item.setParent(lstPerfil);
                                
                            }
                            traza.trace("agregando la fila " + ses.getIdUsuario() + " "
                                    + ses.getRolUsuario().getRol() + " "
                                    + ses.getLibreria().getDescripcion() + " "
                                    + ses.getCategoria().getCategoria(), Level.INFO);

                        } catch (NullPointerException e) {

                            cell = new Listcell(ses.getIdUsuario());
                            item.appendChild(cell);

                            cell = new Listcell(ses.getRolUsuario().getRol());
                            item.appendChild(cell);

                            cell = new Listcell("");
                            item.appendChild(cell);

                            cell = new Listcell("");
                            item.appendChild(cell);

                            item.setParent(lstPerfil);

                            traza.trace("agregando la fila " + ses.getIdUsuario() + " "
                                    + ses.getRolUsuario().getRol(), Level.INFO);

                        }
                    }

                    if (fabrica) {
                        lblMensaje.setValue("Usuario: " + txtUsuario.getValue() + " ya registrado su estatus es: " + estatusActualUsuario + " y pertenece a la FABRICA: " + fabric.toUpperCase());
                    } else {
                        lblMensaje.setValue("Usuario: " + txtUsuario.getValue() + " ya registrado su estatus es: " + estatusActualUsuario);
                    }
                }

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException ex) {
            traza.trace("error soap en el webservice", Level.ERROR, ex);
        } catch (SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
        return resp;

    }

    @Listen("onSelect = #cmbLibreria")
    public void llenarCategorias() {
        Listitem datos;
        Listcell cell;
        int lib;
        String cat = "";

        if (session != null) {

            lstCategorias.getItems().clear();

            lib = cmbLibreria.getSelectedItem().getValue();

            traza.trace("id libreria seleccionada " + lib, Level.INFO);

            for (Perfil lc : listaPerfil) {
                if (lib == lc.getLibreria().getIdLibreria()) {
                    if (!cat.equalsIgnoreCase(lc.getCategoria().getCategoria())) {
                        cat = lc.getCategoria().getCategoria();
                        datos = new Listitem();
                        datos.setValue(lc.getCategoria().getIdCategoria());
                        datos.setLabel(lc.getCategoria().getCategoria());
                        datos.setParent(lstCategorias);
                    }
                }
            }
        } else {
            herramientas.warn("Sesión vencida");
            herramientas.navegarPagina("index.zul");
        }
    }

    @Listen("onClick = #btnAgregar")
    public void agregarConfiguracion() {

        int i, j, l, contAd = 0;
        String libreria, status = "", cat, rol, tiene = "";
        List<Listitem> categoriasSeleccionas = new ArrayList<>();
        Set<Listitem> categorias;
        List<String> perfiles = new ArrayList<>();
        String user;
        boolean verPerfil = true, agrega = true, admin = true, other = false;
        Rows rows;
        Row fila;
        Label label;
        List<Listitem> items;
        Listitem item;
        Listcell cell;

        try {

            if (session != null) {

                libreria = cmbLibreria.getSelectedItem().getLabel();

                categorias = lstCategorias.getSelectedItems();
                categoriasSeleccionas.addAll(categorias);

                user = txtUsuario.getText();

                traza.trace("libreria seleccionada " + libreria, Level.INFO);
                traza.trace("usuario a configurar " + libreria, Level.INFO);

                if (rdActivo.isSelected()) {
                    status = "Activo";
                } else if (rdInactivo.isSelected()) {
                    status = "Inactivo";
                }

                traza.trace("actual estatus del usuario " + estatusActualUsuario, Level.INFO);
                traza.trace("nuevo estatus del usuario " + status, Level.INFO);

                if (!estatusActualUsuario.equalsIgnoreCase(status) || estatusActualUsuario.equalsIgnoreCase("")) {
                    lblEstatus.setValue(status);
                    agrega = false;
                } else {
                    if (!lblEstatus.getValue().equalsIgnoreCase(status) && !lblEstatus.getValue().equalsIgnoreCase("")) {
                        lblEstatus.setValue("");
                        agrega = false;
                    }
                }

                if (chkAdministrador.isChecked()) {
                    perfiles.add(chkAdministrador.getLabel());
                    admin = false;
                }

                if (chkConsultar.isChecked()) {
                    perfiles.add(chkConsultar.getLabel());
                    other = true;
                }

                if (chkImprimir.isChecked()) {
                    perfiles.add(chkImprimir.getLabel());
                    other = true;
                }

                if (chkDigitalizador.isChecked()) {
                    perfiles.add(chkDigitalizador.getLabel());
                    other = true;
                }

                if (chkAprobador.isChecked()) {
                    perfiles.add(chkAprobador.getLabel());
                    other = true;
                }

                if (chkReportes.isChecked()) {
                    perfiles.add(chkReportes.getLabel());
                    other = true;
                }

                if (chkEliminar.isChecked()) {
                    perfiles.add(chkEliminar.getLabel());
                    other = true;
                }

                agrega = !perfiles.isEmpty();

                items = lstPerfil.getItems();

                if (agrega) {

                    if (admin && other) {

                        if (!user.equalsIgnoreCase("")) {
                            if (!libreria.equalsIgnoreCase("")) {
                                if (categoriasSeleccionas.size() > 0) {

                                    if (!items.isEmpty()) {

                                        try {

                                            for (i = 0; i < items.size(); i++) {

                                                List<Listcell> cells = items.get(i).getChildren();

                                                cat = cells.get(2).toString();
                                                rol = cells.get(3).toString();

                                                if (verPerfil) {
                                                    for (l = 0; l < perfiles.size(); l++) {

                                                        String per = (String) perfiles.get(l);

                                                        for (j = 0; j < categoriasSeleccionas.size(); j++) {

                                                            String cate = categoriasSeleccionas.get(j).getValue().toString();

                                                            if ((cate.equalsIgnoreCase(cat))
                                                                    && (per.equalsIgnoreCase(rol))) {
                                                                verPerfil = false;
                                                                tiene = cat + " y " + rol;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        } catch (NullPointerException e) {
                                        }
                                    }

                                    if (verPerfil) {

                                        for (i = 0; i < perfiles.size(); i++) {

                                            item = new Listitem();
                                            String per = (String) perfiles.get(i);

                                            for (j = 0; j < categoriasSeleccionas.size(); j++) {

                                                if (per.equalsIgnoreCase(chkAdministrador.getLabel())) {
                                                    if (contAd == 0) {

                                                        cell = new Listcell(user);
                                                        item.appendChild(cell);

                                                        cell = new Listcell(per.toUpperCase());
                                                        item.appendChild(cell);

                                                        item.setParent(lstPerfil);
                                                    }
                                                    contAd++;
                                                    break;
                                                } else {

                                                    cell = new Listcell(user);
                                                    item.appendChild(cell);

                                                    cell = new Listcell(per.toUpperCase());
                                                    item.appendChild(cell);

                                                    cell = new Listcell(libreria);
                                                    item.appendChild(cell);

                                                    cell = new Listcell(categoriasSeleccionas.get(j).getValue().toString());
                                                    item.appendChild(cell);

                                                    item.setParent(lstPerfil);
                                                }
                                            }
                                        }
                                        limpiar();
                                    } else {
                                        herramientas.warn("Ya el usuario tiene ese perfil " + tiene);
                                    }
                                } else {
                                    herramientas.warn("Debe seleccionar categorias");
                                }
                            } else {
                                herramientas.warn("Debe seleccionar una libreria");
                            }
                        } else {
                            herramientas.warn("Debe colocar un usuario");
                        }
                    } else {
                        item = new Listitem();

                        cell = new Listcell(user);
                        item.appendChild(cell);

                        cell = new Listcell(chkAdministrador.getLabel());
                        item.appendChild(cell);

                        item.setParent(lstPerfil);
                        limpiar();
                    }
                }

            } else {
                herramientas.warn("Sesión vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (Exception e) {
            traza.trace("problemas al agregar la configuracion", Level.ERROR, e);
            herramientas.error("Problemas al agregar la configuracion", e);
        }

    }

    private void limpiar() {

        rdActivo.setSelected(false);
        rdInactivo.setSelected(false);

        cmbLibreria.getSelectedItem().setValue("");

        chkAdministrador.setChecked(false);
        chkConsultar.setChecked(false);
        chkImprimir.setChecked(false);
        chkAprobador.setChecked(false);
        chkDigitalizador.setChecked(false);
        chkReportes.setChecked(false);
        chkEliminar.setChecked(false);
    }

    @Listen("onClick = #btnEliminarFilas")
    public void eliminarFilas() {

        Set<Listitem> itemSel;
        List<Listitem> listitems = new ArrayList<>();
//        Iterator it;

        try {

            itemSel = lstPerfil.getSelectedItems();
            listitems.addAll(itemSel);
//            it = itemSel.iterator();

            for (Listitem listitem : listitems) {
                lstPerfil.removeChild(listitem);
            }

//            while (it.hasNext()) {
//                Listitem item = (Listitem) it.next();
//                lstPerfil.removeChild(item);
//            }
            lstPerfil.renderAll();

        } catch (Exception e) {
            traza.trace("problemas eliminado la fila", Level.ERROR, e);
            herramientas.error("Problemas eliminando el perfil", e);
        }

    }

    @Listen("onClick = #btnGuardar")
    public void guardarPerfil() {

        List<Perfil> perfiles = new ArrayList<>();
        Perfil perfil;
        Categoria categoria;
        Libreria libreria;
        Fabrica fabric;
        Rol rol;
        boolean idPerfil;
        List<Listitem> listitems;
        List<Listcell> cells;

        try {

            listitems = lstPerfil.getItems();

            for (Listitem item : listitems) {

                cells = item.getChildren();

                perfil = new Perfil();
                libreria = new Libreria();
                fabric = new Fabrica();
                categoria = new Categoria();
                rol = new Rol();

                perfil.setUsuario(usuarioBuscado);

                try {
                    rol.setRol(cells.get(1).getLabel());
                    for (Perfil p1 : listaPerfil) {
                        if (rol.getRol().equalsIgnoreCase(p1.getRol().getRol())) {
                            rol.setIdRol(p1.getRol().getIdRol());
                            perfil.setRol(rol);
                            traza.trace("rol " + rol.getRol(), Level.INFO);
                            traza.trace("id rol " + rol.getIdRol(), Level.INFO);
                            break;
                        }
                    }
                } catch (Exception e) {
                }

                try {
                    libreria.setDescripcion(cells.get(2).getLabel());
                    for (Perfil p1 : listaPerfil) {
                        if (p1.getLibreria().getDescripcion().equalsIgnoreCase(libreria.getDescripcion())) {
                            libreria.setIdLibreria(p1.getLibreria().getIdLibreria());
                            perfil.setLibreria(libreria);
                            traza.trace("libreria " + libreria.getDescripcion(), Level.INFO);
                            traza.trace("id libreria " + libreria.getIdLibreria(), Level.INFO);
                            break;
                        }
                    }
                } catch (Exception e) {
                }

                try {
                    categoria.setCategoria(cells.get(3).getLabel());
                    for (Perfil p1 : listaPerfil) {
                        if (p1.getCategoria().getCategoria().equalsIgnoreCase(categoria.getCategoria())) {
                            categoria.setIdCategoria(p1.getCategoria().getIdCategoria());
                            perfil.setCategoria(categoria);
                            traza.trace("categoria " + categoria.getCategoria(), Level.INFO);
                            traza.trace("id categoria " + categoria.getIdCategoria(), Level.INFO);
                            break;
                        }
                    }
                } catch (Exception e) {
                }

                if (lblEstatus.getValue().equalsIgnoreCase(rdActivo.getLabel())) {
                    perfil.setEstatus(Constantes.ESTATUS_ACTIVO);
                } else if (lblEstatus.getValue().equalsIgnoreCase(rdInactivo.getLabel())) {
                    perfil.setEstatus(Constantes.ESTATUS_INACTIVO);
                }
                traza.trace("id estatus " + perfil.getEstatus(), Level.INFO);

                if (this.fabrica) {
                    if (chkPerteneceFabrica.isChecked()) {
                        fabric.setPertenece(true);
                        fabric.setUsuario(usuarioBuscado);
                        perfil.setFabrica(fabric);
                    } else {
                        fabric.setPertenece(false);
                        fabric.setUsuario(usuarioBuscado);
                        perfil.setFabrica(fabric);
                    }
                } else {
                    fabric.setPertenece(false);
                    fabric.setUsuario(usuarioBuscado);
                    perfil.setFabrica(fabric);
                }

                perfiles.add(perfil);

            }

            if (existe) {
                idPerfil = new AdministracionModifica().modificandoPerfil(perfiles);
            } else {
                idPerfil = new AdministracionAgregar().agregandoPerfil(perfiles);

            }

            if (idPerfil) {
                herramientas.info("Usuario " + usuarioBuscado + " configurado con exito");
                limpiarComponentes();
                inactivarComponentes();
            } else {
                herramientas.warn("Problemas al configurar el usuario " + usuarioBuscado);
                limpiarComponentes();
                inactivarComponentes();
            }

        } catch (SOAPException ex) {
            traza.trace("error soap en el webservice", Level.ERROR, ex);
        } catch (SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }

    }

    private void limpiarComponentes() {

        txtUsuario.setValue("");
        chkAdministrador.setChecked(false);
        chkAprobador.setChecked(false);
        chkConsultar.setChecked(false);
        chkDigitalizador.setChecked(false);
        chkEliminar.setChecked(false);
        chkImprimir.setChecked(false);
        chkPerteneceFabrica.setChecked(false);
        chkReportes.setChecked(false);
        rdActivo.setChecked(false);
        rdInactivo.setChecked(false);
        lstCategorias.getItems().clear();
        lstPerfil.getItems().clear();
        cmbLibreria.getChildren().clear();
//        grdConfiguracion.getRows().getChildren().clear();
    }

    private void inactivarComponentes() {

        cmbLibreria.setDisabled(true);
        chkDigitalizador.setDisabled(true);
        chkAprobador.setDisabled(true);
        chkReportes.setDisabled(true);
        chkAdministrador.setDisabled(true);
        chkConsultar.setDisabled(true);
        chkImprimir.setDisabled(true);

        lstCategorias.setDisabled(true);

        btnGuardar.setDisabled(false);
        btnAgregar.setDisabled(false);

        rdInactivo.setDisabled(false);
        rdActivo.setDisabled(false);
        chkPerteneceFabrica.setDisabled(true);
        chkEliminar.setDisabled(true);
    }

    private void activarComponentes() {

        cmbLibreria.setDisabled(false);
        chkDigitalizador.setDisabled(false);
        chkAprobador.setDisabled(false);
        chkReportes.setDisabled(false);
        chkAdministrador.setDisabled(false);
        chkConsultar.setDisabled(false);
        chkImprimir.setDisabled(false);

        lstCategorias.setDisabled(false);

        btnGuardar.setDisabled(false);
        btnAgregar.setDisabled(false);

        rdInactivo.setDisabled(false);
        rdActivo.setDisabled(false);
        chkPerteneceFabrica.setDisabled(false);
        chkEliminar.setDisabled(false);
    }

}
