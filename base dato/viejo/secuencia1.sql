create sequence SQ_CATEGRORIA
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_COMBO
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_DATO_ADICIONAL
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_DATOS_INFODOCUMENTO
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_DOCUMENTO_ELIMINADO
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_EXPEDIENTE
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_FOLIATURA
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;

/*
create sequence SQ_INCREMENTO
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;*/


create sequence SQ_INDICES
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_INFODOCUMENTO
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_LIBRERIA
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_PERFIL
minvalue 1
maxvalue 9223372036854775807
start with 3
increment by 1
cache 20;


create sequence SQ_SUBCATEGRORIA
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_TIPO_DOCUMENTO
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;


create sequence SQ_VALOR_DATO_ADICIONAL
minvalue 1
maxvalue 9223372036854775807
start with 1
increment by 1
cache 20;