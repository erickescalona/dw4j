/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.tools.excepcion;

/**
 *
 * @author develcom
 */
//public class DW4JException extends Throwable {
public class DW4JException extends Exception {

    private static final long serialVersionUID = 3006796833273886980L;

    public DW4JException(String message) {
        super(message);
    }

    public DW4JException(String message, Throwable cause) {
        super(message, cause);
    }

    public DW4JException(Throwable cause) {
        super(cause);
    }

    public DW4JException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
