CREATE TABLE categoria (
    id_categoria integer DEFAULT nextval('sq_categroria'::regclass) NOT NULL,
    id_libreria integer NOT NULL,
    categoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


CREATE TABLE causa (
    id_causa integer NOT NULL,
    causa character varying(150) NOT NULL
);


CREATE TABLE configuracion (
    id_configuracion integer NOT NULL,
    calidad character(1),
    ruta_temporal character varying(50),
    archivo_tif character varying(50),
    archivo_cod character varying(50),
    log character varying(50),
    foliatura character(1),
    server_name character varying(50),
    database_name character varying(50),
    port integer,
    userbd character varying(50),
    password character varying(50),
    ficha character(1),
    fabrica character(1),
    elimina character(1)
);


CREATE TABLE dato_adicional (
    id_dato_adicional integer DEFAULT nextval('sq_dato_adicional'::regclass) NOT NULL,
    indice_adicional character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    id_documento integer NOT NULL,
    codigo integer
);


CREATE TABLE datos_infodocumento (
    id_datos integer DEFAULT nextval('sq_datos_infodocumento'::regclass) NOT NULL,
    id_infodocumento integer,
    id_doc_eliminado integer,
    fecha_digitalizacion date,
    usuario_digitalizo character varying(30),
    fecha_aprobacion date,
    usuario_aprobacion character varying(30),
    fecha_rechazo date,
    usuario_rechazo character varying(30),
    motivo_rechazo character varying(300),
    causa_rechazo character varying(300),
    fecha_eliminado date,
    usuario_elimino character varying(30),
    motivo_elimino character varying(300),
    causa_elimino character varying(300),
    dato_adicional character varying(30)
);


CREATE TABLE documento_eliminado (
    id_doc_eliminado integer DEFAULT nextval('sq_documento_eliminado'::regclass) NOT NULL,
    id_expediente character varying(50) NOT NULL,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    id_documento integer NOT NULL,
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    fecha_eliminado date NOT NULL,
    usuario_elimino character varying(30) NOT NULL
);


CREATE TABLE estatus (
    id_estatus integer NOT NULL,
    estatus character varying(100) NOT NULL
);


CREATE TABLE estatus_documento (
    id_estatus_documento integer NOT NULL,
    estatus_documento character varying(20) NOT NULL
);


CREATE TABLE expedientes (
    id_expedientes integer DEFAULT nextval('sq_expediente'::regclass) NOT NULL,
    expediente character varying(250) NOT NULL,
    id_indice integer NOT NULL,
    valor character varying(250),
    fecha_indice date,
    id_libreria integer NOT NULL,
    id_categoria integer NOT NULL
);


CREATE TABLE fabrica (
    usuario character varying(50) NOT NULL,
    fabrica character(1) NOT NULL
);


CREATE TABLE foliatura (
    id_foliatura integer DEFAULT nextval('sq_foliatura'::regclass) NOT NULL,
    id_infodocumento integer NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50) NOT NULL,
    pagina integer NOT NULL
);


CREATE TABLE indices (
    id_indice integer DEFAULT nextval('sq_indices'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    indice character varying(250) NOT NULL,
    tipo character varying(50) NOT NULL,
    codigo integer NOT NULL,
    clave character(1)
);


CREATE TABLE infodocumento (
    id_infodocumento integer DEFAULT nextval('sq_infodocumento'::regclass) NOT NULL,
    id_documento integer NOT NULL,
    id_expediente character varying(50),
    nombre_archivo character varying(1000),
    ruta_archivo character varying(1000),
    formato character varying(4),
    numero_documento integer NOT NULL,
    version integer NOT NULL,
    paginas integer NOT NULL,
    fecha_vencimiento date,
    estatus_documento integer NOT NULL,
    re_digitalizado character(1) NOT NULL
);


CREATE TABLE libreria (
    id_libreria integer DEFAULT nextval('sq_libreria'::regclass) NOT NULL,
    libreria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


CREATE TABLE lista_desplegables (
    id_lista integer DEFAULT nextval('sq_combo'::regclass) NOT NULL,
    codigo_indice integer NOT NULL,
    descripcion character varying(200)
);


CREATE TABLE perfil (
    id_perfil integer DEFAULT nextval('sq_perfil'::regclass) NOT NULL,
    id_libreria integer,
    id_categoria integer,
    id_usuario character varying(50) NOT NULL,
    id_rol integer NOT NULL
);


CREATE TABLE rol (
    id_rol integer NOT NULL,
    rol character varying(50) NOT NULL
);


CREATE TABLE subcategoria (
    id_subcategoria integer DEFAULT nextval('sq_subcategroria'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    subcategoria character varying(200) NOT NULL,
    id_estatus integer NOT NULL
);


CREATE TABLE tipodocumento (
    id_documento integer DEFAULT nextval('sq_tipo_documento'::regclass) NOT NULL,
    id_categoria integer NOT NULL,
    id_subcategoria integer NOT NULL,
    tipo_documento character varying(200) NOT NULL,
    id_estatus integer NOT NULL,
    vencimiento character(1),
    dato_adicional character(1),
    ficha character(1)
);


CREATE TABLE usuario (
    id_usuario character varying(50) NOT NULL,
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    cedula character varying(50),
    sexo character(1),
    id_estatus integer NOT NULL,
    password character varying(16)
);


CREATE TABLE valor_dato_adicional (
    id_valor integer DEFAULT nextval('sq_valor_dato_adicional'::regclass) NOT NULL,
    id_dato_adicional integer NOT NULL,
    valor character varying(250) NOT NULL,
    numero integer NOT NULL,
    version integer NOT NULL,
    expediente character varying(250) NOT NULL
);


ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pk PRIMARY KEY (id_categoria);


ALTER TABLE ONLY causa
    ADD CONSTRAINT causa_pk PRIMARY KEY (id_causa);


ALTER TABLE ONLY documento_eliminado
    ADD CONSTRAINT documento_eliminado_pk PRIMARY KEY (id_doc_eliminado);


ALTER TABLE ONLY estatus_documento
    ADD CONSTRAINT estatus_documento_pk PRIMARY KEY (id_estatus_documento);


ALTER TABLE ONLY estatus
    ADD CONSTRAINT estatus_pk PRIMARY KEY (id_estatus);


ALTER TABLE ONLY foliatura
    ADD CONSTRAINT foliatura_pk PRIMARY KEY (id_foliatura);


ALTER TABLE ONLY libreria
    ADD CONSTRAINT libreria_pk PRIMARY KEY (id_libreria);


ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pk PRIMARY KEY (id_perfil);


ALTER TABLE ONLY lista_desplegables
    ADD CONSTRAINT pk_combo PRIMARY KEY (id_lista);


ALTER TABLE ONLY configuracion
    ADD CONSTRAINT pk_configuracion PRIMARY KEY (id_configuracion);


ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT pk_dato_adicional PRIMARY KEY (id_dato_adicional);


ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT pk_datos_infodocumento PRIMARY KEY (id_datos);


ALTER TABLE ONLY expedientes
    ADD CONSTRAINT pk_expedientes PRIMARY KEY (id_expedientes);


ALTER TABLE ONLY fabrica
    ADD CONSTRAINT pk_fabrica PRIMARY KEY (usuario);


ALTER TABLE ONLY indices
    ADD CONSTRAINT pk_indice PRIMARY KEY (id_indice);


ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT pk_infordocumento PRIMARY KEY (id_infodocumento);


ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT pk_valor_dato_adicional PRIMARY KEY (id_valor);


ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pk PRIMARY KEY (id_rol);


ALTER TABLE ONLY subcategoria
    ADD CONSTRAINT subcategoria_pk PRIMARY KEY (id_subcategoria);


ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT tipodocumento_pk PRIMARY KEY (id_documento);


ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (id_usuario);


ALTER TABLE ONLY dato_adicional
    ADD CONSTRAINT fk_dato_adicional_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datoc_infodoc FOREIGN KEY (id_infodocumento) REFERENCES infodocumento(id_infodocumento);


ALTER TABLE ONLY datos_infodocumento
    ADD CONSTRAINT fk_datos_doc_eliminado FOREIGN KEY (id_doc_eliminado) REFERENCES documento_eliminado(id_doc_eliminado);


ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


ALTER TABLE ONLY expedientes
    ADD CONSTRAINT fk_expedientes_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


ALTER TABLE ONLY fabrica
    ADD CONSTRAINT fk_fabrica_usuario FOREIGN KEY (usuario) REFERENCES usuario(id_usuario);


ALTER TABLE ONLY indices
    ADD CONSTRAINT fk_indice_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_statusdoc FOREIGN KEY (estatus_documento) REFERENCES estatus_documento(id_estatus_documento);


ALTER TABLE ONLY infodocumento
    ADD CONSTRAINT fk_infodoc_tipodoc FOREIGN KEY (id_documento) REFERENCES tipodocumento(id_documento);


ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_libreria FOREIGN KEY (id_libreria) REFERENCES libreria(id_libreria);


ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_rol FOREIGN KEY (id_rol) REFERENCES rol(id_rol);


ALTER TABLE ONLY perfil
    ADD CONSTRAINT fk_perfil_usuario FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_categoria FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);


ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT fk_tipodoc_subcategoria FOREIGN KEY (id_subcategoria) REFERENCES subcategoria(id_subcategoria);


ALTER TABLE ONLY usuario
    ADD CONSTRAINT fk_usuario_estatus FOREIGN KEY (id_estatus) REFERENCES estatus(id_estatus);


ALTER TABLE ONLY valor_dato_adicional
    ADD CONSTRAINT kf_valor_indice_da FOREIGN KEY (id_dato_adicional) REFERENCES dato_adicional(id_dato_adicional);
