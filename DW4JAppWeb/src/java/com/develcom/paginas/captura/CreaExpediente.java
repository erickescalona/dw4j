/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.captura;

import com.develcom.administracion.Combo;
import com.develcom.administracion.Indices;
import com.develcom.dao.Expediente;
import com.develcom.expediente.Expedientes;
import com.develcom.expediente.Indice;
import com.develcom.tools.Herramientas;
import com.develcom.tools.ToolsFiles;
import com.develcom.tools.excepcion.DW4JException;
import com.develcom.tools.trazas.Traza;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import ve.com.develcom.expediente.BuscaIndice;
import ve.com.develcom.expediente.GestionExpediente;
import ve.com.develcom.expediente.LLenarListaDesplegable;

/**
 *
 * @author develcom
 */
public class CreaExpediente extends SelectorComposer<Component> {

    @Wire
    private Window winNuevoExpediente;

    @Wire
    private Grid panelIndices;

    @Wire
    private Label lblLibreria;

    @Wire
    private Label lblCategoria;

    @Wire
    private Label lblSubCategoria;

    private List<Indices> listaIndices;
    private List<Indice> indices = new ArrayList<Indice>();
    private static final long serialVersionUID = 6474383397574901009L;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(CreaExpediente.class);
    private Session session;
    private ToolsFiles toolsFile = new ToolsFiles();
    private Expediente expediente;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            expediente = (Expediente) session.getAttribute("expediente");
            
            lblLibreria.setValue("Libreria: "+expediente.getLibreria());
            lblLibreria.setTooltiptext(expediente.getLibreria());
            lblCategoria.setValue("Categoria: "+expediente.getCategoria());
            lblCategoria.setTooltiptext(expediente.getCategoria());
            lblSubCategoria.setValue("SubCategoria: "+expediente.getSubCategoria());
            lblSubCategoria.setTooltiptext(expediente.getSubCategoria());
            
            crearFormularioIndices();

        }

    }

    /**
     * Genera un formulario de los indices para un nuevo expediente
     */
    private void crearFormularioIndices() {

        Indice indice;
        Rows rows = null;
        Row row = null;
        Label label;
        Textbox textbox;
        Combobox combobox;
        Datebox datebox;
        int cont = 2, idCategoria, sizeList, i;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        try {

            if (session != null) {

                idCategoria = expediente.getIdCategoria();
                listaIndices = new BuscaIndice().buscarIndice(idCategoria);
                sizeList = listaIndices.size();
                
                
                rows = panelIndices.getRows();

                for (Indices ind : listaIndices) {

                    indice = new Indice();

                    if (cont == 2) {
                        cont = 0;
                        row = new Row();
                    }

                    if (ind.getTipo().equalsIgnoreCase("NUMERO")) {

                        traza.trace("id etiqueta numero " + ind.getIndice().replace(" ", "_"), Level.INFO);

                        label = new Label(ind.getIndice());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndice().replace(" ", "_"));

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                        indice.setClave(ind.getClave());
                        indice.setCodigo(ind.getCodigo());
                        indice.setIdCategoria(idCategoria);
                        indice.setIdIndice(ind.getIdIndice());
                        indice.setIndice(ind.getIndice());
                        indice.setTipo(ind.getTipo());
                        indice.setUpdateIndices(false);
                        indices.add(indice);
                        
                        if (ind.getClave().equalsIgnoreCase("y")) {
                            textbox.setValue(expediente.getIdExpediente());
                            textbox.setDisabled(true);
                        }

                    } else if (ind.getTipo().equalsIgnoreCase("TEXTO")) {

                        traza.trace("id etiqueta texto " + ind.getIndice().replace(" ", "_"), Level.INFO);

                        label = new Label(ind.getIndice());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndice().replace(" ", "_"));

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                        indice.setClave(ind.getClave());
                        indice.setCodigo(ind.getCodigo());
                        indice.setIdCategoria(idCategoria);
                        indice.setIdIndice(ind.getIdIndice());
                        indice.setIndice(ind.getIndice());
                        indice.setTipo(ind.getTipo());
                        indice.setUpdateIndices(false);
                        indices.add(indice);
                        
                        if (ind.getClave().equalsIgnoreCase("y")) {
                            textbox.setValue(expediente.getIdExpediente());
                            textbox.setDisabled(true);
                        }

                    } else if (ind.getTipo().equalsIgnoreCase("AREA")) {

                        traza.trace("id etiqueta area " + ind.getIndice().replace(" ", "_"), Level.INFO);

                        label = new Label(ind.getIndice());

                        textbox = new Textbox();
                        textbox.setId(ind.getIndice().replace(" ", "_"));
                        textbox.setRows(6);

                        row.appendChild(label);
                        row.appendChild(textbox);

                        rows.appendChild(row);

                        cont++;

                        indice.setClave(ind.getClave());
                        indice.setCodigo(ind.getCodigo());
                        indice.setIdCategoria(idCategoria);
                        indice.setIdIndice(ind.getIdIndice());
                        indice.setIndice(ind.getIndice());
                        indice.setTipo(ind.getTipo());
                        indice.setUpdateIndices(false);
                        indices.add(indice);
                        
                        if (ind.getClave().equalsIgnoreCase("y")) {
                            textbox.setValue(expediente.getIdExpediente());
                            textbox.setDisabled(true);
                        }

                    } else if (ind.getTipo().equalsIgnoreCase("COMBO")) {

                        traza.trace("id etiqueta combo " + ind.getIndice().replace(" ", "_"), Level.INFO);
                        Comboitem item;

                        label = new Label(ind.getIndice());

                        combobox = new Combobox();
                        combobox.setId(ind.getIndice().replace(" ", "_"));

                        List<Combo> datosCombo = new LLenarListaDesplegable().buscarData(ind.getCodigo(), false);

                        for (Combo combo : datosCombo) {
                            item = new Comboitem();
                            item.setValue(combo.getIdCombo());
                            item.setLabel(combo.getDatoCombo());
                            item.setParent(combobox);
                        }

                        row.appendChild(label);
                        row.appendChild(combobox);

                        rows.appendChild(row);

                        cont++;

                        indice.setClave(ind.getClave());
                        indice.setCodigo(ind.getCodigo());
                        indice.setIdCategoria(idCategoria);
                        indice.setIdIndice(ind.getIdIndice());
                        indice.setIndice(ind.getIndice());
                        indice.setTipo(ind.getTipo());
                        indice.setUpdateIndices(false);
                        indices.add(indice);
                        
                        if (ind.getClave().equalsIgnoreCase("y")) {
                            combobox.getSelectedItem().setValue(expediente.getIdExpediente());
                            combobox.setDisabled(true);
                        }

                    } else if (ind.getTipo().equalsIgnoreCase("FECHA")) {

                        traza.trace("id etiqueta fecha " + ind.getIndice().replace(" ", "_"), Level.INFO);

                        Label labelDesde = new Label(ind.getIndice().replace(" ", "_"));
                        datebox = new Datebox();
                        datebox.setId(ind.getIndice().replace(" ", "_"));
                        datebox.setFormat("dd/MM/yyyy");

                        row.appendChild(labelDesde);
                        row.appendChild(datebox);

                        rows.appendChild(row);

                        cont++;

                        indice.setClave(ind.getClave());
                        indice.setCodigo(ind.getCodigo());
                        indice.setIdCategoria(idCategoria);
                        indice.setIdIndice(ind.getIdIndice());
                        indice.setIndice(ind.getIndice());
                        indice.setTipo(ind.getTipo());
                        indice.setUpdateIndices(false);
                        indices.add(indice);
                        
                        if (ind.getClave().equalsIgnoreCase("y")) {
                            datebox.setValue(sdf.parse(expediente.getIdExpediente()));
                            datebox.setDisabled(true);
                        }
                    }

                }

            } else {
                herramientas.warn("Sesion vencida");
                herramientas.navegarPagina("index.zul");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas creando los datos adicionales", Level.ERROR, ex);
        } catch (ParseException ex) {
            traza.trace("problemas en convertir la fecha", Level.ERROR, ex);
        }

    }

    @Listen("onClick = #btnGuardar")
    public void guardarIndices() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Expedientes expeWS = new Expedientes();
        int idLibreria, idCategoria;
        boolean exito;

        try {

            if (session != null) {

                idCategoria = expediente.getIdCategoria();
                idLibreria = expediente.getIdLibreria();

                for (int i = 0; i < indices.size(); i++) {

                    Indice indice = indices.get(i);

                    if (panelIndices.getFellowIfAny(indice.getIndice().replace(" ", "_")) instanceof Textbox) {

                        Textbox textbox = (Textbox) panelIndices.getFellowIfAny(indice.getIndice().replace(" ", "_"));

                        if ((indice.getClave().equalsIgnoreCase("y")) || (indice.getClave().equalsIgnoreCase("s")) || (indice.getClave().equalsIgnoreCase("o"))) {

                            if (textbox.getValue() != null || !textbox.getValue().equalsIgnoreCase("")) {

                                indice.setValor(textbox.getValue());

                            } else {
                                String arg = indice.getIndice().replace("_", " ");
                                arg = arg.toLowerCase();
                                char[] cs = arg.toCharArray();
                                char ch = cs[0];
                                cs[0] = Character.toUpperCase(ch);
                                arg = String.valueOf(cs);

                                throw new DW4JException("El " + arg + " no debe estar vacio");
                            }
                        } else {
                            indice.setValor(textbox.getValue());
                        }

                        traza.trace("Indice" + indice.getIndice() + " valor " + textbox.getValue(), Level.INFO);

                    } else if (panelIndices.getFellowIfAny(indice.getIndice().replace(" ", "_")) instanceof Combobox) {

                        Combobox combobox = (Combobox) panelIndices.getFellowIfAny(indice.getIndice().replace(" ", "_"));

                        if ((indice.getClave().equalsIgnoreCase("y")) || (indice.getClave().equalsIgnoreCase("s")) || (indice.getClave().equalsIgnoreCase("o"))) {

                            if (combobox.getValue() != null || !combobox.getValue().equalsIgnoreCase("")) {

                                indice.setValor(combobox.getSelectedItem().getValue());

                            } else {
                                String arg = indice.getIndice().replace("_", " ");
                                arg = arg.toLowerCase();
                                char[] cs = arg.toCharArray();
                                char ch = cs[0];
                                cs[0] = Character.toUpperCase(ch);
                                arg = String.valueOf(cs);

                                throw new DW4JException("El " + arg + " no debe estar vacio");
                            }
                        } else {
                            indice.setValor(combobox.getSelectedItem().getValue());
                        }

                        traza.trace("Indice " + indice.getIndice() + " valor " + combobox.getValue(), Level.INFO);

                    } else if (panelIndices.getFellowIfAny(indice.getIndice().replace(" ", "_")) instanceof Datebox) {

                        Datebox datebox = (Datebox) panelIndices.getFellowIfAny(indice.getIndice().replace(" ", "_"));

                        if ((indice.getClave().equalsIgnoreCase("y")) || (indice.getClave().equalsIgnoreCase("s")) || (indice.getClave().equalsIgnoreCase("o"))) {

                            if (datebox.getValue() != null) {

                                indice.setValor(sdf.format(datebox.getValue()));

                            } else {
                                String arg = indice.getIndice().replace("_", " ");
                                arg = arg.toLowerCase();
                                char[] cs = arg.toCharArray();
                                char ch = cs[0];
                                cs[0] = Character.toUpperCase(ch);
                                arg = String.valueOf(cs);

                                throw new DW4JException("El " + arg + " no debe estar vacio");
                            }
                        } else {
                            indice.setValor(sdf.format(datebox.getValue()));
                        }

                        traza.trace("Indice " + indice.getIndice() + " valor " + datebox.getValue(), Level.INFO);

                    }
                    expeWS.getIndices().add(indice);

                }

                expeWS.setExpediente(expediente.getIdExpediente());
                expeWS.setIdLibreria(idLibreria);
                expeWS.setIdCategoria(idCategoria);

                exito = new GestionExpediente().archivarExpediente(expeWS);

                if (exito) {
                    expediente.setResultado(true);
                    session.setAttribute("expediente", expediente);
                    winNuevoExpediente.setVisible(false);
                    winNuevoExpediente.detach();

                } else {
                    expediente.setResultado(false);
                    session.setAttribute("expediente", expediente);
                    herramientas.warn("Problema al guardar el Expediente");
                }

            } else {
                herramientas.info("Sesión finalizada");
                herramientas.navegarPagina("index.zul");
            }

        } catch (DW4JException e) {
            herramientas.warn(e.getMessage());
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al guardar el expediente", Level.ERROR, ex);
            herramientas.error("Problemas al guardar el expediente", ex);
        }

    }

    @Listen("onClick = #btnCerrar")
    public void cerrar() {
        winNuevoExpediente.setVisible(false);
        winNuevoExpediente.detach();
    }

}
