/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * VerDocumentoPDF.java
 *
 * Created on 06-sep-2012, 9:14:30
 */
package com.develcom.verdocumento;

import com.develcom.documento.Bufer;
import com.develcom.documento.InfoDocumento;
import com.develcom.dto.Expediente;
import com.develcom.dto.ManejoSesion;
import com.develcom.expediente.Indice;
import com.develcom.log.Traza;
import com.develcom.tools.Constantes;
import com.develcom.tools.ToolsFile;
import com.develcom.tools.pdf.Acciones;
import com.develcom.tools.pdf.PreparaPagina;
import com.develcom.tools.pdf.ThumbAction;
import com.sun.pdfview.OutlineNode;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PagePanel;
import com.sun.pdfview.ThumbPanel;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import ve.com.develcom.archivo.GestionArchivos;

/**
 *
 * @author develcom
 */
public class VerDocumentoPDF extends javax.swing.JFrame {
//public class VerDocumentoPDF extends javax.swing.JPanel {

    private static final long serialVersionUID = 1652300882199048256L;
//public class VerDocumentoPDF extends JFrame {
    /**
     * Total de paginas digitalizadas
     */
    private int cantidadPaginas = 0;
    /**
     * lista de indices dinamicos
     */
    private List<Indice> indices = new ArrayList<Indice>();
    /**
     * accion a tomar (versionar o reemplazar)
     */
    private String accion;
    /**
     * objecto con la informacion del documento
     */
    private InfoDocumento infoDocumento;
    private List<InfoDocumento> infoDocumentos;
    private Expediente expediente;
    private Traza traza = new Traza(VerDocumentoPDF.class);
    private Acciones acciones = new Acciones(this);
    private String nombreDocumento;
    private int paginaActual = -1;
    private PagePanel panelPagina;
    //private PagePanel panelPagina;
    private PDFFile archivoPDF;
    private OutlineNode outline = null;
    private ThumbPanel thumbs;// = new ThumbPanel(null);
    private JDialog olf;
    private JScrollPane thumbsScroll;// = new JScrollPane(thumbs, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    private PreparaPagina pagePrep;
    private int versionSeleccionada;
    private ToolsFile toolsTiff = new ToolsFile();
    private File filePDF;

    /**
     * Creates new form VerDocumentoPDF
     */
    public VerDocumentoPDF(InfoDocumento infoDocumento) {
        this.expediente = ManejoSesion.getExpediente();
        this.infoDocumento = infoDocumento;
        iniciar();
    }

    private void iniciar() {
        panelPagina = new PagePanel();
        panelPagina.addKeyListener(acciones);

        initComponents();

        jlTipoDocumento.setText(expediente.getTipoDocumento());
        jlLibreria.setText(expediente.getLibreria());
        jlCategoria.setText(expediente.getCategoria());
        jlSubCategoria.setText(expediente.getSubCategoria());
        traza.trace("accion " + accion, Level.INFO);

        jlFechaVencimiento.setVisible(false);

        zommOut.setVisible(false);
        zommIn.setVisible(false);

        split.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new ThumbAction(this));
        split.setOneTouchExpandable(true);
        thumbs = new ThumbPanel(null);
        thumbsScroll = new JScrollPane(thumbs, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        split.setDividerLocation((int) thumbs.getPreferredSize().width + (int) thumbsScroll.getVerticalScrollBar().getWidth() + 4);
        split.setRightComponent(panelPagina);
        split.setLeftComponent(thumbsScroll);

        setLocationRelativeTo(null);

//        crearObjetos();

//        if(flag){
//            guardar(file);
//        }else{
//            mostrar();
//        }

        visualizar();

        nombreDocumento = expediente.getTipoDocumento();
        setTitle(nombreDocumento);
        traza.trace("nombre del documento " + nombreDocumento, Level.INFO);


    }

    private void visualizar() {
        EjecutaEspera hilo = new EjecutaEspera();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        String ruta = "", archivo = "";//, buffer;
        BufferedReader leer;
        PrintWriter escribir;

        int contDocAprob = 0, contDocPend = 0;
        OutputStream escribiendo;
        Bufer buffer;
        //byte[] buffer;


        int idInfoDocumento = infoDocumento.getIdInfoDocumento();
        int idDoc = infoDocumento.getIdDocumento();
        int version = infoDocumento.getVersion();
        int numDoc = infoDocumento.getNumeroDocumento();
        int idCat = ManejoSesion.getExpediente().getIdCategoria();
        int idSubCat = ManejoSesion.getExpediente().getIdSubCategoria();
        String idExpediente = ManejoSesion.getExpediente().getIdExpediente();
        traza.trace("buscando el documento idDocumento " + idDoc + " idCategoria " + idCat + " idSubCategoria " + idSubCat + " idExpediente " + idExpediente, Level.INFO);

        try {
            
            infoDocumentos = new GestionArchivos().buscarImagenDocumentos(idInfoDocumento, idDoc, idCat, idSubCat, version, numDoc, idExpediente);
            hilo.start();

            if (infoDocumentos.size() > 0) {

                File fileCod = new File(toolsTiff.getArchivoCodificado());
                if (toolsTiff.getDirTemporal().exists()) {
                    File[] files = toolsTiff.getDirTemporal().listFiles();
                    for (File f : files) {
                        if (f.delete()) {
                            traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                        } else {
                            traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                        }
                    }
                }

                for (InfoDocumento id : infoDocumentos) {

                    if (id.getFechaVencimiento() != null) {

                        XMLGregorianCalendar xmlCalendar = id.getFechaVencimiento();
                        GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                        jlFechaVencimiento.setText("Fecha de vencimiento: " + sdf.format(rechaVencimiento.getTime()));
                        jlFechaVencimiento.setVisible(true);

                        ruta = id.getRutaArchivo();
                        archivo = id.getNombreArchivo();
                        version = id.getVersion();
                        break;

                    } else {
                        ruta = id.getRutaArchivo();
                        archivo = id.getNombreArchivo();
                        version = id.getVersion();
                        break;
                    }
                }
                traza.trace("ruta a buscar " + ruta, Level.INFO);
                traza.trace("archivo a buscar " + archivo, Level.INFO);
                traza.trace("version del documento a mostrar " + version, Level.INFO);

                //jlMensaje4.setText("Tipo de Documento: "+infoDocumento.getTipoDocumento()+" \"ULTIMA VERSION: "+version+"\"");


                //busca el archivo fisico del tipo de documento
                buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);

                escribiendo = new FileOutputStream(fileCod);
                escribiendo.write(buffer.getBufer());
                escribiendo.flush();
                escribiendo.close();

//                leer = new BufferedReader(new StringReader(buffer));
//                escribir = new PrintWriter(new BufferedWriter(new FileWriter(fileCod)));
//
//                while ((buffer = leer.readLine()) != null) {
//                    escribir.println(buffer);
//                }
//                escribir.close();

                filePDF = new File("documento." + infoDocumento.getFormato());

                new ToolsFile().decodificar(filePDF.getName());

                openFile(new File(toolsTiff.getTempPath() + filePDF));

                for (InfoDocumento infoDoc : infoDocumentos) {

                    if (Constantes.ACCION.equalsIgnoreCase("CONSULTAR")) {

                        if (infoDoc.getEstatusDocumento().equalsIgnoreCase("aprobado")) {
                            cboVersion.addItem(infoDoc.getVersion());
                            contDocAprob++;
                        }

                    } else if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {

                        if (infoDoc.getEstatusDocumento().equalsIgnoreCase("pendiente")) {
                            cboVersion.addItem(infoDoc.getVersion());
                            contDocPend++;
                        }

                    } else if (Constantes.ACCION.equalsIgnoreCase("CAPTURAR")) {
                        cboVersion.addItem(infoDoc.getVersion());
                    }
                }

                if (infoDocumentos.size() == 1 || contDocAprob == 1 || contDocPend == 1) {
                    cboVersion.setEnabled(false);
                    btnAbrir.setEnabled(false);
                }
                setVisible(true);

            }
            hilo.interrupt();
            hilo.terminar();
        } catch (SOAPException ex) {
            hilo.interrupt();
            hilo.terminar();
            traza.trace("error al buscar el archivo " + ex.getMessage(), Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();

//            if(Constantes.ACCION.equalsIgnoreCase("CAPTURAR")){
//                CreaEditaExpediente cee = new CreaEditaExpediente();
//                Principal.desktop.add(cee);
//            }else if(Constantes.ACCION.equalsIgnoreCase("CONSULTAR")){
//                ResultadoExpediente re = new ResultadoExpediente();
//                Principal.desktop.add(re);
//                Principal.desktop.repaint();
//            }
        } catch (FileNotFoundException ex) {
            hilo.interrupt();
            hilo.terminar();
            traza.trace("Error al buscar el archivo", Level.ERROR, ex);
            this.dispose();
            JOptionPane.showMessageDialog(this, "Error al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
//            if(Constantes.ACCION.equalsIgnoreCase("CAPTURAR")){
//                CreaEditaExpediente cee = new CreaEditaExpediente();
//                Principal.desktop.add(cee);
//                Principal.desktop.repaint();
//            }else if(Constantes.ACCION.equalsIgnoreCase("CONSULTAR")){
//                ResultadoExpediente re = new ResultadoExpediente();
//                Principal.desktop.add(re);
//            }
        } catch (SOAPFaultException e) {
            hilo.interrupt();
            hilo.terminar();
            traza.trace("error al buscar el archivo " + e.getMessage(), Level.ERROR, e);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();
//            if(Constantes.ACCION.equalsIgnoreCase("CAPTURAR")){
//                CreaEditaExpediente cee = new CreaEditaExpediente();
//                Principal.desktop.add(cee);
//                Principal.desktop.repaint();
//            }else if(Constantes.ACCION.equalsIgnoreCase("CONSULTAR")){
//                ResultadoExpediente re = new ResultadoExpediente();
//                Principal.desktop.add(re);
//            }
        } catch (Exception ex) {
            hilo.interrupt();
            hilo.terminar();
            traza.trace("Error general al buscar el archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();
//            if(Constantes.ACCION.equalsIgnoreCase("CAPTURAR")){
//                CreaEditaExpediente cee = new CreaEditaExpediente();
//                Principal.desktop.add(cee);
//                Principal.desktop.repaint();
//            }else if(Constantes.ACCION.equalsIgnoreCase("CONSULTAR")){
//                ResultadoExpediente re = new ResultadoExpediente();
//                Principal.desktop.add(re);
//            }
        }

    }

    /**
     * Crea los indices dinamicos en el formulario
     */
//    private void crearObjetos() {
//        try {
//
//            traza.trace("creando indices dinamicos", Level.INFO);
//            GridBagConstraints constraints = new GridBagConstraints();
//            UtilidadVentanas uv = new UtilidadVentanas(this);
//
//            constraints.gridx=0;
//            constraints.gridy=0;
//            constraints.gridwidth = 1;
//            constraints.gridheight = 1;
//
//            panelIndices.setLayout(new FlowLayout(FlowLayout.LEFT));
//            //panelIndices.add(uv.crearObjetos(expediente));
//            panelIndices.add(uv.mostrarIndices(expediente));
//
//        } catch (Exception e) {
//            traza.trace("error al crear los indices dinamicos", Level.ERROR, e);
//        }
//
//    }
    /**
     * <p>Open a specific pdf file. Creates a DocumentInfo from the file, and
     * opens that.</p>
     *
     * <p><b>Note:</b> Mapping the file locks the file until the PDFFile is
     * closed.</p>
     *
     * @param file the file to open
     * @throws IOException
     */
    private void openFile(File file) {
        traza.trace("abriendo el archivo " + file.getName(), Level.INFO);
        try {

            // first open the file for random access
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            // extract a file channel
            FileChannel channel = raf.getChannel();
            // now memory-map a byte-buffer
            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            openPDFByteBuffer(buf, file.getPath(), file.getName());

        } catch (FileNotFoundException ex) {
            traza.trace("archivo no encontrado ", Level.ERROR, ex);
        } catch (IOException ex) {
            traza.trace("error al leer el archivo ", Level.ERROR, ex);
        }
    }

    /**
     * open the ByteBuffer data as a PDFFile and start to process it.
     *
     * @param buf
     * @param path
     */
    private void openPDFByteBuffer(ByteBuffer buf, String path, String name) {

        traza.trace("creando el archivo " + name + " con el bufer " + buf, Level.INFO);

        traza.trace("archivoPDF " + archivoPDF, Level.INFO);
        // create a PDFFile from the data
        //PDFFile newfile = null;
        try {
            // set up our document
            archivoPDF = new PDFFile(buf);
            //newfile = new PDFFile(buf);
        } catch (IOException ioe) {

            traza.trace("no parece ser un archivo pdf", Level.ERROR, ioe);

            JOptionPane.showMessageDialog(split, "no parece ser un archivo pdf \n" + ioe.getMessage(), "Error al abrir el archivo ", JOptionPane.ERROR_MESSAGE);

            return;
        }
        traza.trace("archivoPDF " + archivoPDF, Level.INFO);

        // Now that we're reasonably sure this document is real, close the
        // old one.
        //doClose();

        // set up our document
        //archivoPDF = newfile;;

        // set up the thumbnails
        //if (doThumb) {
        thumbs = new ThumbPanel(archivoPDF);
        thumbs.addPageChangeListener(acciones);
        thumbsScroll.getViewport().setView(thumbs);
        thumbsScroll.getViewport().setBackground(Color.gray);
        //}

        //setEnabling();
        //panelPagina.showPage(null);

        cantidadPaginas = archivoPDF.getNumPages();

        // display page 1.
        forceGotoPage(0);

        // if the PDF has an outline, display it.
        try {
            outline = archivoPDF.getOutline();
        } catch (IOException ioe) {
        }

        traza.trace("outline " + outline, Level.INFO);
        if (outline != null) {
            if (outline.getChildCount() > 0) {
                olf = new JDialog(new JFrame(), "Outline");
                olf.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                olf.setLocation(this.getLocation());
                JTree jt = new JTree(outline);
                jt.setRootVisible(false);
                jt.addTreeSelectionListener(acciones);
                JScrollPane jsp = new JScrollPane(jt);
                olf.getContentPane().add(jsp);
                olf.pack();
                olf.setVisible(true);
            } else {
                if (olf != null) {
                    olf.setVisible(false);
                    olf = null;
                }
            }
        }
    }

    /**
     * Changes the displayed page.
     *
     * @param pagenum the page to display
     */
    public void forceGotoPage(int pagenum) {

        traza.trace("cantidad de paginas del archivo pdf " + cantidadPaginas, Level.INFO);


        if (pagenum <= 0) {
            pagenum = 0;
        } else if (pagenum >= archivoPDF.getNumPages()) {
            pagenum = archivoPDF.getNumPages() - 1;
            //System.out.println("pagenum "+pagenum);
        }

        paginaActual = pagenum;
        traza.trace("pagina actual " + paginaActual, Level.INFO);

        // update the page text field
        pagina.setText(String.valueOf(paginaActual + 1));

        traza.trace("moviendose a la pagina " + (pagenum + 1), Level.INFO);


        PDFPage pg = archivoPDF.getPage(pagenum + 1);
        panelPagina.showPage(pg);
        panelPagina.requestFocus();

        traza.trace("actual pagina " + panelPagina.getPage(), Level.INFO);

        thumbs.pageShown(pagenum);


        //split.updateUI();

        // stop any previous page prepper, and start a new one
        traza.trace("pagePrep " + pagePrep, Level.INFO);
        if (pagePrep != null) {
            pagePrep.quit();
        }
        pagePrep = new PreparaPagina(pagenum, this);
        //pagePrep.run();
        pagePrep.start();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barra = new javax.swing.JToolBar();
        jPanel1 = new javax.swing.JPanel();
        primero = new javax.swing.JButton();
        previo = new javax.swing.JButton();
        pagina = new javax.swing.JTextField();
        siguiente = new javax.swing.JButton();
        ultimo = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        cancelar = new javax.swing.JButton();
        zommIn = new javax.swing.JButton();
        zommOut = new javax.swing.JButton();
        jbImprimir = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        cboVersion = new javax.swing.JComboBox();
        jlFechaVencimiento = new javax.swing.JLabel();
        split = new javax.swing.JSplitPane();
        jlLibreria = new javax.swing.JLabel();
        jlCategoria = new javax.swing.JLabel();
        jlSubCategoria = new javax.swing.JLabel();
        jlTipoDocumento = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(224, 239, 255));

        barra.setFloatable(false);

        primero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/pdf/first.gif"))); // NOI18N
        primero.setToolTipText("Primero");
        primero.setFocusable(false);
        primero.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        primero.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        primero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                primeroActionPerformed(evt);
            }
        });

        previo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/pdf/prev.gif"))); // NOI18N
        previo.setToolTipText("Anterior");
        previo.setFocusable(false);
        previo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        previo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        previo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                previoActionPerformed(evt);
            }
        });

        siguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/pdf/next.gif"))); // NOI18N
        siguiente.setToolTipText("Siguiente");
        siguiente.setFocusable(false);
        siguiente.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        siguiente.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        siguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                siguienteActionPerformed(evt);
            }
        });

        ultimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/pdf/last.gif"))); // NOI18N
        ultimo.setToolTipText("Ultimo");
        ultimo.setFocusable(false);
        ultimo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ultimo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ultimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ultimoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(primero)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(previo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pagina, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(siguiente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ultimo))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ultimo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(pagina, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(primero, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(previo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(siguiente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        barra.add(jPanel1);

        cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/Knob Cancel1.png"))); // NOI18N
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });

        zommIn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/ZoomIn24.gif"))); // NOI18N
        zommIn.setMnemonic('+');
        zommIn.setToolTipText("Aumentar Imagen");
        zommIn.setPreferredSize(new java.awt.Dimension(50, 40));
        zommIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zommInActionPerformed(evt);
            }
        });

        zommOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/ZoomOut24.gif"))); // NOI18N
        zommOut.setMnemonic('-');
        zommOut.setToolTipText("Disminuir Imagen");
        zommOut.setPreferredSize(new java.awt.Dimension(50, 40));
        zommOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zommOutActionPerformed(evt);
            }
        });

        jbImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/printer.png"))); // NOI18N
        jbImprimir.setToolTipText("Imprimir");
        jbImprimir.setFocusable(false);
        jbImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbImprimir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbImprimirActionPerformed(evt);
            }
        });

        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/develcom/imagenes/develcom/Open24.gif"))); // NOI18N
        btnAbrir.setToolTipText("Abrir");
        btnAbrir.setFocusable(false);
        btnAbrir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAbrir.setPreferredSize(new java.awt.Dimension(59, 25));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });

        cboVersion.setFont(new java.awt.Font("DejaVu Sans", 0, 12)); // NOI18N
        cboVersion.setToolTipText("Seleccione la versión del documento que desea ver");
        cboVersion.setDoubleBuffered(true);
        cboVersion.setName(""); // NOI18N
        cboVersion.setPreferredSize(new java.awt.Dimension(122, 25));
        cboVersion.setVerifyInputWhenFocusTarget(false);

        jlFechaVencimiento.setText("fecha Vencimiento");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jbImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(cboVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAbrir, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(zommOut, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zommIn, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(jlFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlFechaVencimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(zommIn, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                        .addComponent(zommOut, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                        .addComponent(jbImprimir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cancelar, 0, 0, Short.MAX_VALUE))
                    .addComponent(btnAbrir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        barra.add(jPanel3);

        jlLibreria.setText("Libreria");

        jlCategoria.setText("Categoria");

        jlSubCategoria.setText("SubCategoria");

        jlTipoDocumento.setText("Tipo de Documento");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(barra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlLibreria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlCategoria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlSubCategoria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlTipoDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(split))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(barra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(split, javax.swing.GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlLibreria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlCategoria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlSubCategoria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlTipoDocumento)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void primeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_primeroActionPerformed
        acciones.doFirst();
    }//GEN-LAST:event_primeroActionPerformed

    private void previoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_previoActionPerformed
        acciones.doPrev();
    }//GEN-LAST:event_previoActionPerformed

    private void siguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_siguienteActionPerformed
        acciones.doNext();
    }//GEN-LAST:event_siguienteActionPerformed

    private void ultimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ultimoActionPerformed
        acciones.doLast();
    }//GEN-LAST:event_ultimoActionPerformed

    private void jbImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbImprimirActionPerformed
        acciones.doPrint();
    }//GEN-LAST:event_jbImprimirActionPerformed

    private void zommOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zommOutActionPerformed
        acciones.doZoomTool();
    }//GEN-LAST:event_zommOutActionPerformed

    private void zommInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zommInActionPerformed
        acciones.doFitInWindow();
    }//GEN-LAST:event_zommInActionPerformed

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed


        File fileCod = new File(toolsTiff.getArchivoCodificado());
        if (toolsTiff.getDirTemporal().exists()) {
            File[] files = toolsTiff.getDirTemporal().listFiles();
            for (File f : files) {
                if (f.delete()) {
                    traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                } else {
                    traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                }
            }
        }


        this.dispose();
        Runtime.getRuntime().gc();
        Runtime.getRuntime().exit(0);
    }//GEN-LAST:event_cancelarActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed

        versionSeleccionada = Integer.parseInt(cboVersion.getSelectedItem().toString());
        getVersion(versionSeleccionada);
//        version(versionSeleccionada);
}//GEN-LAST:event_btnAbrirActionPerformed

//    private void version(int version){
//        final MostrarProceso proceso = new MostrarProceso("Buscando la version "+version+" del documento");
//        proceso.start();
//
//        new Thread(new Runnable() {
//
//            public void run() {
//                getVersion(versionSeleccionada);
//                traza.trace("mostrando la ventana", Level.INFO);
//
//                proceso.detener();
//            }
//        }).start();
//
//    }
    /**
     * Busca la version seleccionada del documento
     *
     * @param version La version seleccionada
     */
    private void getVersion(int version) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String ruta, archivo;
        //String buffer;
        Bufer buffer;
        //byte[] buffer;
        BufferedReader leer;
        PrintWriter escribir;
        OutputStream escribiendo;
//        ToolsTiff toolsTiff = new ToolsTiff();

        traza.trace("version a buscar " + version, Level.INFO);

        try {
            for (InfoDocumento infoDoc : infoDocumentos) {
                if (infoDoc.getVersion() == version) {

                    File fileCod = new File(toolsTiff.getArchivoCodificado());
                    if (toolsTiff.getDirTemporal().exists()) {
                        File[] files = toolsTiff.getDirTemporal().listFiles();
                        for (File f : files) {
                            if (f.delete()) {
                                traza.trace("eliminado archivo " + f.getName(), Level.INFO);
                            } else {
                                traza.trace("problemas al eliminar el archivo " + f.getName(), Level.WARN);
                            }
                        }
                    }


                    if (infoDoc.getFechaVencimiento() != null) {

                        XMLGregorianCalendar xmlCalendar = infoDoc.getFechaVencimiento();
                        GregorianCalendar rechaVencimiento = xmlCalendar.toGregorianCalendar();
                        jlFechaVencimiento.setText("Fecha de vencimiento: " + sdf.format(rechaVencimiento.getTime()));
                        jlFechaVencimiento.setVisible(true);

                    }

                    ruta = infoDoc.getRutaArchivo();
                    archivo = infoDoc.getNombreArchivo();
                    version = infoDoc.getVersion();


                    ruta = infoDoc.getRutaArchivo();
                    archivo = infoDoc.getNombreArchivo();
                    buffer = new GestionArchivos().buscandoArchivo(ruta, archivo);

                    escribiendo = new FileOutputStream(fileCod);
                    escribiendo.write(buffer.getBufer());
                    escribiendo.flush();
                    escribiendo.close();

//                    leer = new BufferedReader(new StringReader(buffer));
//                    escribir = new PrintWriter(new BufferedWriter(new FileWriter(fileCod)));
//
//                    while ((buffer = leer.readLine()) != null) {
//                        escribir.println(buffer);
//                    }
//                    escribir.close();

                    new ToolsFile().decodificar("archivoPDF.pdf");

                    openFile(new File(toolsTiff.getTempPath() + "archivoPDF.pdf"));
                }
            }
        } catch (SOAPException ex) {
            traza.trace("error al buscar el archivo " + ex.getMessage(), Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error...", JOptionPane.DEFAULT_OPTION + JOptionPane.ERROR_MESSAGE);
            this.dispose();
        } catch (IOException ex) {
            traza.trace("Error al buscar la version del archivo", Level.ERROR, ex);
            JOptionPane.showMessageDialog(this, "Error al buscar la version del archivo\n" + ex.getMessage(), "Error...", JOptionPane.ERROR_MESSAGE);
        } catch (SOAPFaultException e) {
            traza.trace("error al buscar el archivo " + e.getMessage(), Level.ERROR, e);
            JOptionPane.showMessageDialog(this, "Error general al buscar el archivo.\n Documento o Archivo no encontrado", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToolBar barra;
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton cancelar;
    private javax.swing.JComboBox cboVersion;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton jbImprimir;
    private javax.swing.JLabel jlCategoria;
    private javax.swing.JLabel jlFechaVencimiento;
    private javax.swing.JLabel jlLibreria;
    private javax.swing.JLabel jlSubCategoria;
    private javax.swing.JLabel jlTipoDocumento;
    private javax.swing.JTextField pagina;
    private javax.swing.JButton previo;
    private javax.swing.JButton primero;
    private javax.swing.JButton siguiente;
    private javax.swing.JSplitPane split;
    private javax.swing.JButton ultimo;
    private javax.swing.JButton zommIn;
    private javax.swing.JButton zommOut;
    // End of variables declaration//GEN-END:variables

    public PagePanel getPanelPagina() {
        return panelPagina;
    }

    public ThumbPanel getThumbs() {
        return thumbs;
    }

    public JScrollPane getThumbsScroll() {
        return thumbsScroll;
    }

    public JSplitPane getSplit() {
        return split;
    }

    public PDFFile getArchivoPDF() {
        return archivoPDF;
    }

    public int getPaginaActual() {
        return paginaActual;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public JTextField getPagina() {
        return pagina;
    }

    public void setIndices(List<Indice> indices) {
        this.indices = indices;
    }
}
