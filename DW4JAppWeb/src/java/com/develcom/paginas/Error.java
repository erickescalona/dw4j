/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Window;

/**
 *
 * @author develcom
 */
public class Error extends SelectorComposer<Component> {
    
    @Wire
    private Label lblMensaje;
    
    @Wire
    private Window winError;
    

    private static final long serialVersionUID = -4704376135009407479L;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        
    }
    
    
    @Listen("onClick = #btnOK")
    public void cerrar(){
        winError.setVisible(false);
        winError.detach();
    }

    public Label getLblMensaje() {
        return lblMensaje;
    }

    public void setLblMensaje(Label lblMensaje) {
        this.lblMensaje = lblMensaje;
    }
    
    

}
