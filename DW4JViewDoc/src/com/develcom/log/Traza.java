/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.develcom.log;

import java.io.File;
import java.io.IOException;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
/**
 * Permite crear y escribir
 * trazas en el log
 * @author develcom
 */
public class Traza {

    private String appPath = System.getProperties().getProperty("user.dir");
    private String so = System.getProperty("os.name");
    public static final String RUTALOCAL = "visorDoc";
    /**
     * Atributo principar
     * del log4j para
     * escribir las trazas
     * segun su nivel
     */
    private static Logger logger;
    private String archivo;
    private String nombreClase;
    private DailyRollingFileAppender fileAppender;
    private ConsoleAppender console;

    /**
     * Constructor, inicializa
     * el atributo logger con
     * la clase el cual se escribiran
     * las diferentes trazas en el
     * log
     * @param clase
     * La clase al cual
     * se escribiran las
     * trazas en el log
     */
    public Traza(Class clase) {
        nombreClase=clase.getSimpleName();
        logger = Logger.getLogger(clase);
        
        String home = System.getProperty("user.home");
        File ruta = null;
        
        String win = so.substring(0, 6);
        int index = so.indexOf("windows");
        boolean ok = so.startsWith("Windows");
        
        
        //if(win.equalsIgnoreCase("windows")){
        if(ok){
            ruta = new File("c:\\"+RUTALOCAL+"\\logViewDoc");
            this.archivo = "c:\\"+RUTALOCAL+"\\logViewDoc\\verdoc.log";
        }else if(so.equalsIgnoreCase("linux")){
            ruta = new File(home+"/"+RUTALOCAL+"/logViewDoc");
            this.archivo = ruta+"/verdoc.log";
        }

            
        try {
            if (!ruta.exists()){
                if(ruta.mkdirs()){

                }
            }

            PatternLayout patron = new PatternLayout();
//            this.archivo = ruta+"/verdoc.log";
            patron.setConversionPattern("[%5p] (%d{dd/MM/yyyy HH:mm:ss,SSS} %5p) - %m%n");


            this.console = new ConsoleAppender(patron);
            this.console.activateOptions();
            this.console.setImmediateFlush(true);
            this.console.setTarget("System.out");

            this.fileAppender = new DailyRollingFileAppender(patron, this.archivo, "'.'yyyyMMdd");

            logger.removeAllAppenders();
            logger.addAppender(this.fileAppender);
            logger.addAppender(this.console);
        } catch (IOException ex)  {
            ex.printStackTrace();
        } catch(Exception e){
            e.printStackTrace();
        }

        

    }


    /**
     * Permite escribir el mensaje
     * de traza en el log segun su
     * nivel.
     * http://logging.apache.org/log4j/1.2/manual.html
     * @param mensaje
     * El mensaje que se
     * escribira
     * @param nivel
     * El nivel del log
     * @param ex
     * La excepcion que
     * se generó
     */
    public void trace(String mensaje, Level nivel, Throwable ex) {

        try {

            //PropertyConfigurator.configure(buscarProperties());


            if (nivel.equals(Level.INFO)) {
                logger.info("(verDocWeb) "+nombreClase+" - "+mensaje, ex);
            }
            if (nivel.equals(Level.DEBUG)) {
                logger.debug("(verDocWeb) "+nombreClase+" - "+mensaje, ex);
            }
            if (nivel.equals(Level.WARN)) {
                logger.warn("(verDocWeb) "+nombreClase+" - "+mensaje, ex);
            }
            if (nivel.equals(Level.ERROR)) {
                logger.error("(verDocWeb) "+nombreClase+" - "+mensaje, ex);
            }
            if (nivel.equals(Level.FATAL)) {
                logger.fatal("(verDocWeb) "+nombreClase+" - "+mensaje, ex);
            }
        } catch (Exception e) {
            //LogLog.error("Error al escribir la traza" +ex.toString());
            ex.printStackTrace();
        }
    }

    /**
     * Permite escribir el mensaje
     * de traza en el log segun su
     * nivel.
     * http://logging.apache.org/log4j/1.2/manual.html
     * @param mensaje
     * El mensaje que se
     * escribira
     * @param nivel
     * El nivel del log
     */
    public void trace(String mensaje, Level nivel) {

        try {

            //PropertyConfigurator.configure(buscarProperties());


            if (nivel.equals(Level.INFO)) {
                logger.info("(verDocWeb) "+nombreClase+" - "+mensaje);
            }
            if (nivel.equals(Level.DEBUG)) {
                logger.debug("(verDocWeb) "+nombreClase+" - "+mensaje);
            }
            if (nivel.equals(Level.WARN)) {
                logger.warn("(verDocWeb) "+nombreClase+" - "+mensaje);
            }
            if (nivel.equals(Level.ERROR)) {
                logger.error("(verDocWeb) "+nombreClase+" - "+mensaje);
            }
            if (nivel.equals(Level.FATAL)) {
                logger.fatal("(verDocWeb) "+nombreClase+" - "+mensaje);
            }
        } catch (Exception e) {
            //LogLog.error("Error al escribir la traza" +ex.toString());
            e.printStackTrace();
        }
    }

    public static Logger getLogger() {
        return logger;
    }


//    /**
//     * Busca el archivo
//     * de propiedades
//     * de log4j
//     * @return
//     * Un objecto con las
//     * propiedades del
//     * log4j
//     */
//    private Properties buscarProperties(){
//        String appPath = System.getProperties().getProperty("user.dir");
//        //String properties = ManejoSesion.getPropedades().getProperty("log");
//        //String fileProperties = appPath+properties;
//
//        Properties prop = new Properties();
//        InputStream ips;
//        try {
//            //ips=new FileInputStream(fileProperties);
//            prop.load(new FileInputStream(new File(appPath+"/lib/dw4jdesktop.properties")));
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return prop;
//    }

}
