/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ve.net.develcom.pagina;

import java.io.InputStream;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Filedownload;
import ve.net.develcom.log.Traza;
import ve.net.develcom.tool.Herramientas;

/**
 *
 * @author develcom
 */
public class Install extends SelectorComposer<Component>{
    
    private static final long serialVersionUID = 7323502310733392364L;
    private Traza traza = new Traza(Install.class);
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }
    
    @Listen("onClick = #download")
    public void download(){
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        InputStream in = cl.getResourceAsStream("../../resources/AppDesktop.zip");
        try {
            Filedownload.save(in, "application/zip", "AppDesktop.zip");
        } catch (Exception ex) {
            traza.trace("archivo no encontrado", org.apache.log4j.Level.ERROR, ex);
        }
    }
    
    @Listen("onClick = #exit")
    public void exit(){
        Herramientas tool = new Herramientas();
        tool.pageRedirection("index.zul");
    }
    
}
