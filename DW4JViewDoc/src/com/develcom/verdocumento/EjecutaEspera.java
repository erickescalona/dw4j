/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.develcom.verdocumento;

/**
 *
 * @author develcom
 */
public class EjecutaEspera extends Thread{


    private Espera esperar = new Espera(new javax.swing.JFrame(), true);

    @Override
    public void run() {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //Espera dialog = new Espera(new javax.swing.JFrame(), true);
                esperar.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                esperar.setVisible(true);
            }
        });



        //esperar.setVisible(true);
    }

    public void terminar(){
        esperar.dispose();
    }

}
