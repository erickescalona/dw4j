/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas.administracion;

import com.develcom.administracion.Categoria;
import com.develcom.administracion.Combo;
import com.develcom.administracion.DatoAdicional;
import com.develcom.administracion.Indices;
import com.develcom.administracion.Libreria;
import com.develcom.tools.Herramientas;
import com.develcom.tools.trazas.Traza;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import ve.com.develcom.administracion.AdministracionAgregar;
import ve.com.develcom.administracion.AdministracionBusqueda;

/**
 *
 * @author develcom
 */
public class AdministraComboIndice extends SelectorComposer<Component> {

    @Wire
    private Combobox cmbLibrerias;

    @Wire
    private Combobox cmbCategorias;

    @Wire
    private Combobox cmbIndice;

    @Wire
    private Listbox lstIndicesExistentes;

    @Wire
    private Textbox txtIndice;

    @Wire
    private Listbox lstInicesNuevos;

    private static final long serialVersionUID = 361977072928709086L;
    private List<Libreria> listaLibrerias;
    private List<Categoria> listaCategorias;
    private List<Indices> listaIndices;
    private List<Combo> listaValoresExistentes;
    private boolean existe;
    private HashMap<String, Integer> hashMapValortExistentes = new HashMap<>();
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(AdministraComboIndice.class);
    private Session session;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {

            llenarLibreria();

        } else {
            herramientas.info("Sesión Vencida");
            herramientas.navegarPagina("index.zul");
        }
    }

    private void llenarLibreria() {

        Comboitem item;

        try {

            cmbLibrerias.getItems().clear();

            listaLibrerias = new AdministracionBusqueda().buscarLibreria("", 0);

            item = new Comboitem();
            item.setValue("");
            item.setLabel("");
            item.setParent(cmbLibrerias);

            if (!listaLibrerias.isEmpty()) {
                for (Libreria lib : listaLibrerias) {
                    if (lib.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(lib.getIdLibreria());
                        item.setLabel(lib.getDescripcion());
                        item.setParent(cmbLibrerias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Librerias");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("error soapfault en el webservice", Level.ERROR, ex);
        }
    }

    @Listen("onSelect = #cmbLibrerias")
    public void llenarCategoria() {

        Comboitem item;
        int idLibreria;

        try {

            cmbCategorias.setText("");
            cmbCategorias.getItems().clear();

            idLibreria = cmbLibrerias.getSelectedItem().getValue();
            listaCategorias = new AdministracionBusqueda().buscarCategoria("", idLibreria, 0);

            item = new Comboitem();
            item.setValue("");
            item.setLabel("");
            item.setParent(cmbCategorias);

            if (!listaCategorias.isEmpty()) {
                for (Categoria cat : listaCategorias) {
                    if (cat.getEstatus().equalsIgnoreCase("Activo")) {
                        item = new Comboitem();
                        item.setValue(cat.getIdCategoria());
                        item.setLabel(cat.getCategoria());
                        item.setParent(cmbCategorias);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar las Categorias (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando las categorias", Level.ERROR, ex);
            herramientas.error("Problemas buscando las categorias", ex);
        }
    }

    @Listen("onSelect = #cmbCategorias")
    public void llenarIndices() {

        Comboitem item;
        int idcategoria;

        try {

            idcategoria = cmbCategorias.getSelectedItem().getValue();

            listaIndices = new AdministracionBusqueda().buscandoIndices(idcategoria);

            if (!listaIndices.isEmpty()) {
                for (Indices ind : listaIndices) {
                    if (ind.getTipo().equalsIgnoreCase("COMBO")) {
                        item = new Comboitem();
                        item.setValue(ind.getIdIndice());
                        item.setLabel(ind.getIndice());
                        item.setParent(cmbIndice);
                    }
                }
            } else {
                herramientas.warn("Problemas al buscar los Indices del \nDato Adicional para la lista desplegable (lista vacia)");
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando los indices", Level.ERROR, ex);
            herramientas.error("Problemas buscando los Indices", ex);
        }
    }

    @Listen("onSelect = #cmbIndice")
    public void llenarValoresExistentes() {

        Listitem item;
        int idIndice;

        try {

            lstIndicesExistentes.getItems().clear();
            
            idIndice = cmbIndice.getSelectedItem().getValue();

            listaValoresExistentes = new AdministracionBusqueda().buscandoDatosCombo(idIndice, false);

            if (!listaValoresExistentes.isEmpty()) {
                existe = true;
                for (Combo valor : listaValoresExistentes) {
                    item = new Listitem(valor.getDatoCombo(), valor.getIdCombo());
                    item.setParent(lstIndicesExistentes);
                }
            } else {
                existe = false;
            }

        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas buscando los indices", Level.ERROR, ex);
            herramientas.error("Problemas buscando los Indices", ex);
        }

    }

    @Listen("onClick = #btnModifica")
    public void modificarValor() {
        String valor = lstIndicesExistentes.getSelectedItem().getLabel();
        txtIndice.setValue(valor);
    }

    @Listen("onClick = #btnAgregar")
    public void agregarValoresNuevos() {

        int idIndice;
        String valor, indice, libreria, categoria, tmp;
        boolean banLista = true, banTabla = true;
        List<Listitem> valoresNuevos;
        Listitem item;
        Listcell cell;

        try {

            libreria = cmbLibrerias.getSelectedItem().getLabel();
            categoria = cmbCategorias.getSelectedItem().getLabel();
            indice = cmbIndice.getSelectedItem().getLabel();
            valor = txtIndice.getValue();

            if (!libreria.equalsIgnoreCase("")) {
                if (!categoria.equalsIgnoreCase("")) {
                    if (!indice.equalsIgnoreCase("")) {
                        if (!valor.equalsIgnoreCase("")) {
                            if (existe) {

                                try {

                                    tmp = lstIndicesExistentes.getSelectedItem().getLabel();
                                    for (Combo cbo : listaValoresExistentes) {
                                        if (cbo.getDatoCombo().equalsIgnoreCase(tmp)) {
                                            hashMapValortExistentes.put(valor, cbo.getIdCombo());
                                            break;
                                        }
                                    }
                                    lstIndicesExistentes.clearSelection();
                                } catch (NullPointerException e) {

                                    for (Combo tipoDoc : listaValoresExistentes) {
                                        String td = tipoDoc.getDatoCombo().trim();
                                        if (td.equalsIgnoreCase(valor)) {
                                            banLista = false;
                                            break;
                                        } else {
                                            banLista = true;
                                        }
                                    }
                                }
                            }

                            valoresNuevos = lstInicesNuevos.getItems();

                            for (Listitem libex : valoresNuevos) {
                                List<Listcell> cells = libex.getChildren();
                                String val = cells.get(2).getLabel();
                                if (val.equalsIgnoreCase(valor)) {
                                    banTabla = false;
                                    break;
                                }
                            }

                            if (banLista) {
                                if (banTabla) {
                                    for (Indices arg : listaIndices) {
                                        if (arg.getIndice().equalsIgnoreCase(indice)) {
                                            idIndice = arg.getIdIndice();

                                            item = new Listitem();

                                            cell = new Listcell(Integer.toString(idIndice));
                                            item.appendChild(cell);

                                            cell = new Listcell(indice);
                                            item.appendChild(cell);

                                            cell = new Listcell(valor);
                                            item.appendChild(cell);

                                            item.setParent(lstInicesNuevos);
                                            txtIndice.setText("");
                                            break;
                                        }
                                    }
                                } else {
                                    herramientas.warn("Ya el dato se encuentra en la tabla");
                                }
                            } else {
                                herramientas.warn("Ya el dato se encuentra en la lista");
                            }
                        } else {
                            herramientas.warn("Debe colocar un valor");
                        }
                    } else {
                        herramientas.warn("Debe seleccionar un Indice");
                    }

                } else {
                    herramientas.warn("Debe seleccionar una Categoria");
                }
            } else {
                herramientas.warn("Debe seleccionar una Libreria");
            }

        } catch (Exception e) {
            traza.trace("problemas al agregar el valor del combo", Level.ERROR, e);
            herramientas.error("Problemas al agregar el valor de la lista desplegabla", e);
        }
    }

    @Listen("onClick = #btnGuardar")
    public void guardarValor() {

        List<Combo> combos = new ArrayList<>();
        Combo combo;
        boolean resp;
        List<Listitem> items;
        List<Listcell> cells;

        try {

            int n = Messagebox.show("Seguro que desea guardar los valores de la lista desplegable",
                    "Alerta...", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {

                        @Override
                        public void onEvent(Event evt) throws Exception {
                        }

                    });

            if (n == Messagebox.YES) {

                items = lstInicesNuevos.getItems();

                if (!items.isEmpty()) {
                    for (Listitem item : items) {
                        cells = item.getChildren();
                        combo = new Combo();

                        combo.setCodigoIndice(Integer.parseInt(cells.get(0).getLabel()));

                        combo.setIndice(cells.get(1).getLabel());

                        combo.setDatoCombo(cells.get(2).getLabel());

                        try {
                            combo.setIdCombo(hashMapValortExistentes.get(combo.getDatoCombo()));
                        } catch (NullPointerException e) {
                        }

                        combos.add(combo);
                    }

                    resp = new AdministracionAgregar().agregandoListaDesplegables(combos, false);

                    if (resp) {
                        herramientas.info("Valores guardados con exito");
                    } else {
                        herramientas.warn("Problemas al guardar los Valores");
                    }
                    limpiarComponentes();
                }
            }
        } catch (SOAPException | SOAPFaultException ex) {
            traza.trace("problemas al guardar los valores", Level.ERROR, ex);
            herramientas.error("Problemas al guardar los valores", ex);
        }
    }

    private void limpiarComponentes() {
        txtIndice.setText("");
        lstIndicesExistentes.getItems().clear();
        lstInicesNuevos.getItems().clear();
        cmbLibrerias.setText("");
        cmbCategorias.setText("");
        cmbIndice.setText("");
        cmbLibrerias.getItems().clear();
        cmbCategorias.getItems().clear();
        cmbIndice.getItems().clear();
        llenarLibreria();
    }

}
