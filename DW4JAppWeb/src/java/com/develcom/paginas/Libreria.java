/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.develcom.paginas;

import com.develcom.autentica.Perfil;
import com.develcom.dao.Expediente;
import com.develcom.tools.Constantes;
import com.develcom.tools.Herramientas;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.log4j.Level;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Window;
import ve.com.develcom.sesion.IniciaSesion;
import ve.com.develcom.tools.Traza;

/**
 *
 * @author develcom
 */
public class Libreria extends SelectorComposer<Component> {

    private static final long serialVersionUID = 3817067128899146387L;

    @Wire
    private Window winLibreria;

    @Wire
    private Combobox cmbLibreria;

    @Wire
    private Combobox cmbCategoria;

    private List<Perfil> perfiles;
    private Herramientas herramientas = new Herramientas();
    private Traza traza = new Traza(Libreria.class);
    private Session session;
    private String login;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        iniciar();
    }

    private void iniciar() {

        session = herramientas.crearSesion();

        if (session != null) {
            login = session.getAttribute("login").toString();

            if (Constantes.ACCION.equalsIgnoreCase("CAPTURAR")) {
                winLibreria.setTitle("Digitaliza");
            } else if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {
                winLibreria.setTitle("Calidad");
            } else if (Constantes.ACCION.equalsIgnoreCase("ACTUALIZAR_INDICES")) {
                winLibreria.setTitle("Indices");
            } else if (Constantes.ACCION.equalsIgnoreCase("ELIMINAR")) {
                winLibreria.setTitle("Elimina");
            } else if (Constantes.ACCION.equalsIgnoreCase("CONSULTAR")) {
                winLibreria.setTitle("Consulta");
            } else if (Constantes.ACCION.equalsIgnoreCase("REPORTES")) {
                winLibreria.setTitle("Reportes");
            }

            llenarComboLibrerias();
        }
    }

    private void llenarComboLibrerias() {

        Comboitem item;

        try {
            cmbCategoria.setDisabled(true);

            if (session != null) {

                traza.trace("buscando las libreria del usuario " + login + " con el rol " + Constantes.ROL, Level.INFO);
                perfiles = new IniciaSesion().buscarLibCatPerfil(login, Constantes.ROL);

                traza.trace("llenando lista de librerias", Level.INFO);
                String lib = "";

                for (Perfil perfil : perfiles) {
                    String des = perfil.getLibreria().getDescripcion();

                    if (!lib.equalsIgnoreCase(des)) {
                        lib = perfil.getLibreria().getDescripcion();

                        if (perfil.getLibreria().getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {
                            traza.trace("libreria agregada " + lib, Level.INFO);
                            item = new Comboitem();
                            item.setValue(perfil.getLibreria().getIdLibreria());
                            item.setLabel(lib);
                            item.setParent(cmbLibreria);
                        }
                    }
                }

            } else {
                herramientas.warn("Problemas con el objecto Sesion");
                herramientas.navegarPagina("index.zul");
            }
        } catch (SOAPException | SOAPFaultException e) {
            herramientas.error("Error al llenar la lista de librerias ", e);
            traza.trace("error llenando el combo libreria", Level.ERROR, e);
        }
    }

    @Listen("onSelect = #cmbLibreria")
    public void llenarComboCategoria() {
        int lib;
        String cat = "";
        Comboitem item;

        traza.trace("libreria selecionada para llenar combo categoria " + cmbLibreria.getValue(), Level.INFO);

        try {

            cmbLibreria.setTooltiptext(cmbLibreria.getSelectedItem().getLabel());
            
            cmbCategoria.getItems().clear();

            if (session != null) {

//                if (libreria != null) {
                lib = cmbLibreria.getSelectedItem().getValue();

                traza.trace("llenando lista de categorias", Level.INFO);
                traza.trace("id libreria seleccionada " + lib, Level.INFO);

                for (Perfil cate : perfiles) {
                    String categ = cate.getCategoria().getCategoria();

                    if (cate.getLibreria().getIdLibreria() == lib) {
                        if (!cat.equalsIgnoreCase(categ)) {
                            cat = cate.getCategoria().getCategoria();

                            if (cate.getCategoria().getEstatus().equalsIgnoreCase(Constantes.ACTIVO)) {
                                traza.trace("categoria agregada " + cat, Level.INFO);
                                item = new Comboitem();
                                item.setValue(cate.getCategoria().getIdCategoria());
                                item.setLabel(cat);
                                item.setParent(cmbCategoria);
                            }
                        }
                    }
                }
                cmbCategoria.setDisabled(false);
//                }
            } else {
                herramientas.warn("Problemas con el objecto Sesion");
                herramientas.navegarPagina("index.zul");
            }

        } catch (Exception e) {
            herramientas.error("Problemas al llenar la lista de categorias", e);
            traza.trace("error al llenar lista de categoria", Level.ERROR, e);
        }
    }
        
    @Listen("onSelect = #cmbCategoria")
    public void infoCategoria(){
        cmbCategoria.setTooltiptext(cmbCategoria.getSelectedItem().getLabel());
    }

    @Listen("onClick = #btnAceptar")
    public void aceptar() {

        String libr, cate;
        int lib, cat;
        Expediente expediente;

        try {

            expediente = (Expediente) session.getAttribute("expediente");

            libr = cmbLibreria.getSelectedItem().getValue().toString();
            cate = cmbCategoria.getSelectedItem().getValue().toString();

            traza.trace("libreria selecionada " + libr, Level.INFO);
            traza.trace("categoria selecionada " + cate, Level.INFO);

            if (!libr.equalsIgnoreCase("")) {
                if (!cate.equalsIgnoreCase("")) {

                    lib = cmbLibreria.getSelectedItem().getValue();
                    cat = cmbCategoria.getSelectedItem().getValue();

                    for (Perfil perfil : perfiles) {

                        if (perfil.getLibreria().getIdLibreria() == lib) {
                            expediente.setIdLibreria(lib);
                            expediente.setLibreria(perfil.getLibreria().getDescripcion());
                        }
                        if (perfil.getCategoria().getIdCategoria() == cat) {
                            expediente.setIdCategoria(cat);
                            expediente.setCategoria(perfil.getCategoria().getCategoria());
                        }
                    }

                    session.setAttribute("expediente", expediente);

                    if (Constantes.ACCION.equalsIgnoreCase("CAPTURAR")) {
                        herramientas.navegarPagina("digitaliza.zul");
                    } else if (Constantes.ACCION.equalsIgnoreCase("CONSULTAR")) {
                        herramientas.navegarPagina("consulta.zul");
                    } else if (Constantes.ACCION.equalsIgnoreCase("APROBAR")) {
                        herramientas.navegarPagina("buscaExpediente.zul");
                    } else if (Constantes.ACCION.equalsIgnoreCase("ACTUALIZAR_INDICES")) {
                        herramientas.navegarPagina("consultarIndices.zul");
                    } else if (Constantes.ACCION.equalsIgnoreCase("ELIMINAR")) {
                        herramientas.navegarPagina("eliminaDocumento.zul");
                    }

                    traza.trace("seleccion la libreria " + expediente.getLibreria() + " id " + expediente.getIdLibreria(), Level.INFO);
                    traza.trace("seleccion la categoria " + expediente.getCategoria() + " id " + expediente.getIdCategoria(), Level.INFO);
                } else {
                    herramientas.warn("Debe seleccionar una categoria");
                }
            } else {
                herramientas.warn("Debe seleccionar una libreria");
            }
        } catch (NumberFormatException ex) {
            herramientas.error("", ex);
            traza.trace("error en la conversion del id", Level.ERROR, ex);
        } catch (WrongValueException ex) {
            herramientas.error("", ex);
            traza.trace("Error al aceptar", Level.ERROR, ex);
        }
    }
}
