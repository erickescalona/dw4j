/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.develcom.tools;

import com.develcom.log.Traza;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import org.apache.log4j.Level;

/**
 *
 * @author develcom
 */
public class Impresor {

    private Traza traza = new Traza(Impresor.class);

    private PrinterJob    pj;
    private PageFormat    pf;
    private Book          bk;
    

    public Impresor(){
      pj=PrinterJob.getPrinterJob();

      pf = pj.defaultPage();
      Paper p=pf.getPaper();
      p.setImageableArea(0.0,0.0,p.getWidth(),p.getHeight());
      pf.setPaper(p);
      pf = pj.validatePage(pf);

      bk=new Book();
    }

    public void append(ImagePanel image){
      bk.append(image, pf);
    }

    public void print(){
      pj.setPageable(bk);
      if(pj.printDialog()){
        try{
          pj.print();
        }catch (Exception e){
          traza.trace("error durante la impresion", Level.ERROR, e);
          traza.trace("9\b"+getClass().getName()+".print:\n\t"+e.getMessage(), Level.ERROR);
        }
      }
    }

}
